#!/bin/bash

#
# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
#

while getopts 'n:v:p:i:t:' flag; do
    # shellcheck disable=SC2220
    case "$flag" in
        n) NAMESPACE=$OPTARG;;
        v) VALUES=$OPTARG;;
        p) HELM_PATH=$OPTARG;;
        i) APPLICATION=$OPTARG;;
        t) TAG=$OPTARG;;
    esac
done
shift $(($OPTIND - 1));

echo "Deploying $APPLICATION:$TAG with config file $VALUES and path $HELM_PATH to namespace $NAMESPACE"

DANGEROUS_USER_MNEMONIC=`echo $1 | sed 's/-/ /g'`
ACCESS_TOKEN=`echo $2 | sed 's/-/ /g'`
CREATOR_ADDRESS=`echo $3 | sed 's/-/ /g'`

# Switch to project if it exists or create a new one
oc project "$NAMESPACE"
# Upgrade or install
helm upgrade --namespace "$NAMESPACE" -f $VALUES -i $APPLICATION $HELM_PATH --set "$DANGEROUS_USER_MNEMONIC" --set "$2" --set "$3"
# Ensure image stream picks up the new docker image right away
oc import-image $APPLICATION:$TAG
