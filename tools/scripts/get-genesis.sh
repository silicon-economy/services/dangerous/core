#!/usr/bin/env sh

#
# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
#

# Remove old `genesis.go`
rm ./apps/blockchain/x/businesslogic/types/genesis.go
# Remove snippets directory if it exists
rm -rf ./snippets
# Clone snippet into `./snippets` directory
git clone ${EXAMPLE_GENESIS_SSH_URL} ./snippets
# Move `genesis.go` to target blockchain directory
mv ./snippets/genesis.go ./apps/blockchain/x/businesslogic/types/genesis.go
# Remove snippets directory if it exists
rm -rf ./snippets
# Prevent file from being added to changelist
git update-index --skip-worktree ./apps/blockchain/x/businesslogic/types/genesis.go

