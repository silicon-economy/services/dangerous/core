/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';
import * as fs from 'fs';
import { AppModule } from '../../../apps/transport-document-gateway/src/app.module';
import { Config } from '../../../apps/transport-document-gateway/src/config/config';

describe('Generate Transport-Document-Gateway Swagger', () => {
  let app: NestFastifyApplication;

  beforeAll(async () => {
    jest.spyOn(AppModule.prototype, 'onApplicationBootstrap').mockImplementation();
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = module.createNestApplication(new FastifyAdapter());
  });

  afterAll(async () => {
    await app.close();
  });

  it('should generate swagger spec', () => {
    const config: Pick<OpenAPIObject, 'openapi' | 'info' | 'servers' | 'security' | 'tags' | 'externalDocs'> =
      new DocumentBuilder()
        .setTitle(Config.APPLICATION_TITLE)
        .setDescription(Config.APPLICATION_DESCRIPTION)
        .setVersion(Config.APPLICATION_VERSION)
        .addBasicAuth()
        .build();

    const document: OpenAPIObject = SwaggerModule.createDocument(app, config);
    fs.writeFileSync(
      './documentation/generated/swagger/transport-document-gateway_swagger.json',
      JSON.stringify(document)
    );
  });
});
