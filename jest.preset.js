/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

const nxPreset = require('@nx/jest/preset').default;

// Global jest configuration.
// The use of these options inside of project jest.config.js is deprecated!
nxPreset.coverageReporters = ['lcov', 'text', 'text-summary'];
nxPreset.collectCoverage = true;

module.exports = { ...nxPreset };
