/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: [
    '@nrwl/nx',
    'license-header',
    '@typescript-eslint',
    '@delagen/deprecation',
    'import',
    'jsdoc',
    'disable',
    'eslint-plugin-tsdoc',
  ],
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      extends: [
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'plugin:eslint-plugin-eslint-comments/recommended',
        'plugin:jsdoc/recommended',
        'plugin:import/recommended',
        'prettier',
        'plugin:@nrwl/nx/typescript',
      ],
      parserOptions: {
        sourceType: 'module',
        project: ['./tsconfig.eslint.json'],
        tsconfigRootDir: __dirname,
      },
      rules: {
        'eslint-comments/no-unlimited-disable': 'off',
        'eslint-comments/disable-enable-pair': ['error', { allowWholeFile: true }],
        '@typescript-eslint/explicit-function-return-type': 'warn',
        '@delagen/deprecation/deprecation': 'error',
        // Needs to be disabled for using jest tests
        '@typescript-eslint/unbound-method': 'off',
        // eslint-plugin-jsdoc rules
        'jsdoc/require-example': 'off',
        'jsdoc/require-param-type': 'off',
        'jsdoc/require-returns-type': 'off',
        'jsdoc/check-syntax': 'error',
        'jsdoc/require-jsdoc': [
          'warn',
          {
            contexts: [
              // Exclude Angular methods, getter and setter
              'MethodDefinition:not([kind="constructor"],' +
                '[kind="get"],' +
                '[kind="set"],' +
                '[key.name=/(ngAfterContentChecked|ngAfterContentInit|ngAfterViewChecked|ngAfterViewInit|ngDoBootstrap|ngDoCheck|ngOnChanges|ngOnDestroy|ngOnInit)/])',
              // Include interfaces.
              // 'TSInterfaceDeclaration',
              // Include enums.
              'TSEnumDeclaration',
            ],
            checkConstructors: false,
            // Do not create empty jsdoc automatically
            enableFixer: false,
          },
        ],
        // eslint-plugin-tsdoc rules
        'tsdoc/syntax': 'error',
      },
    },
    // Disable plugins for specific file name patterns
    {
      files: ['main.ts', '*.module.ts', '*.spec.ts', '*.dto.ts', '*.enum.ts'],
      settings: {
        'disable/plugins': ['jsdoc'],
      },
    },
    // Disable '@typescript-eslint/explicit-function-return-type' rule for specific file name patterns
    {
      files: ['main.ts', '*.module.ts', '*.spec.ts', '*.dto.ts', '*.enum.ts', 'config.ts'],
      rules: {
        '@typescript-eslint/explicit-function-return-type': 'off',
      },
    },
    {
      files: ['*.ts', '*.tsx', '*.js', '*.jsx'],
      rules: {
        'license-header/header': ['error', './tools/license-header.js'],
      },
    },
    {
      files: ['*.js', '*.jsx'],
      extends: ['plugin:@nrwl/nx/javascript'],
      rules: {},
    },
    {
      files: ['*.spec.ts', '*.spec.tsx', '*.spec.js', '*.spec.jsx'],
      env: {
        jest: true,
      },
    },
  ],
  processor: 'disable/disable',
};
