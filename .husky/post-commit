#!/usr/bin/env sh

#
# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
#

. "$(dirname -- "$0")/_/husky.sh"

#######################################
# Add LICENSE file in each microservice's subdirectory.
# Globals:
#   None
# Arguments:
#   None
#######################################
add_license_to_subdirectories(){
  for f in apps/*; do
    if [ -d "$f" ]; then
      # Copy only when the source LICENSE file is updated or when the destination LICENSE file is missing
      cp -u LICENSE $f/
      git add $f/LICENSE
    fi
  done
}

#######################################
# Add license header to blockchain files.
# Globals:
#   None
# Arguments:
#   None
#######################################
add_blockchain_license_header(){
  # Check if go package is installed on the client machine, if not, the package is installed automatically
  command -v addlicense >/dev/null 2>&1 || { printf "Go package 'https://github.com/google/addlicense' is \
  needed and\nis going to be installed. Please make sure that your go-path is\nset correctly!"; \
  go install github.com/google/addlicense@latest; }
  # Add license header to all blockchain files, except for various patterns
  printf "Adding blockchain license headers in progress\n"
  addlicense -v -f tools/license-header.go \
  -ignore "**/helm/**" \
  -ignore "**/.gitlab-ci.yml" \
  -ignore "**/Dockerfile" apps/blockchain
  printf "Adding blockchain license headers finished\n\n"
}

#######################################
# Update swagger documentation for both gateways in
# directory '<projectRoot>/documentation/generated/swagger/'.
# Globals:
#   None
# Arguments:
#   None
#######################################
update_gateway_swagger_documentation(){
  nx run tools:generate_dangerous-goods-gateway_swagger-json
  nx run tools:generate_transport-document-gateway_swagger-json
}

#######################################
# Execute various updates to project files, if a `.commit` file is present
# and add changed files due to husky execution to the previous commit.
#
# This is done to allow the use of `git commit -n -m '<commit-message>'`
# without executing this post-commit hook, because the `-n` does not affect the
# execution of it.
# Globals:
#   None
# Arguments:
#   None
#######################################
amend_changes_to_previous_commit(){
  echo
  if [ -e .commit ]
      then
      rm .commit
      add_license_to_subdirectories
      add_blockchain_license_header
      update_gateway_swagger_documentation
      git add .
      git commit --amend -C HEAD --no-verify
  fi
  exit
}

amend_changes_to_previous_commit
