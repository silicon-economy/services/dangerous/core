/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

const NodePolyfillPlugin = require('node-polyfill-webpack-plugin');

module.exports = (x) => {
  const result = {
    ...x,
    plugins: [new NodePolyfillPlugin()],
    target: 'node',
    resolve: {
      extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'], // other stuff
      fallback: {
        fs: false,
        path: require.resolve('path-browserify'),
        mqtt: false,
        ioredis: false,
        nats: false,
        kafkajs: false,
        '@grpc/proto-loader': false,
        '@grpc/grpc-js': false,
        '@nestjs/websockets/socket-module': false,
        'class-transformer/storage': false,
      },
    },
    ignoreWarnings: [
      function ignoreSourcemapsloaderWarnings(warning) {
        return (
          warning.module &&
          warning.module.resource.includes('node_modules') &&
          warning.details &&
          warning.details.includes('source-map-loader')
        );
      },
    ],
  };
  result.output.filename = '[name].js';

  return result;
};
