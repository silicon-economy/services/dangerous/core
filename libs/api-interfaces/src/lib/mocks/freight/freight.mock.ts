/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { FreightIdsIncluded } from '@core/api-interfaces/lib/dtos/frontend/transport-document/freight-ids-included';
import {
  PackagingGroup,
  PackagingUnit,
  TransportCategory,
  TransportLabel,
  TunnelRestrictionCode,
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

export const mockFreightIdsIncluded: FreightIdsIncluded = {
  additionalInformation: 'Beförderung ohne Freistellung nach ADR 1.1.3.6',
  transportationInstructions: 'Bitte Beförderungseinheit nach der Entladung reinigen.',
  totalTransportPoints: 3330,
  orders: [
    {
      id: 'b2541d16-5ce6-4156-8a40-1320a6e103aa',
      status: undefined,
      logEntries: [],
      orderPositions: [
        {
          package: 'Fässer aus Kunststoff (H) abnehmbarer Deckel',
          packagingCode: '1H2',
          quantity: 1,
          unit: PackagingUnit.L,
          individualAmount: 60,
          polluting: false,
          transportPoints: 180,
          totalAmount: 60,
          id: 31,
          deviceId: '0',
          status: '',
          logEntries: [],
          dangerousGood: {
            unNumber: '1090',
            casNumber: '67-64-1',
            description: 'ACETON',
            label1: TransportLabel._3,
            label2: TransportLabel._6_1,
            label3: TransportLabel._,
            packingGroup: PackagingGroup.II,
            tunnelRestrictionCode: TunnelRestrictionCode['(D/E)'],
            transportCategory: TransportCategory['_2'],
          },
        },
        {
          package: 'Kanister aus Kunststoff (H) abnehmbarer Deckel',
          packagingCode: '3H2',
          quantity: 1,
          unit: PackagingUnit.L,
          individualAmount: 50,
          polluting: false,
          transportPoints: 150,
          totalAmount: 50,
          id: 33,
          deviceId: '0',
          status: '',
          logEntries: [],
          dangerousGood: {
            unNumber: '1230',
            casNumber: '67-56-1',
            description: 'METHANOL',
            label1: TransportLabel._3,
            label2: TransportLabel._6_1,
            label3: TransportLabel._,
            packingGroup: PackagingGroup.II,
            tunnelRestrictionCode: TunnelRestrictionCode['(D/E)'],
            transportCategory: TransportCategory['_2'],
          },
        },
      ],
      consignee: {
        name: 'Funke AG - Lacke und Farben',
        address: {
          street: 'Columbiadamm',
          number: '194',
          postalCode: '10965',
          city: 'Berlin',
          country: 'Deutschland',
        },
        contact: {
          name: 'Dr. Martina Zünd',
          phone: '0049 741 852 063 9',
          mail: 'Zuend@Funke.de',
          department: 'Abt. 4A',
        },
      },
    },
    {
      id: '55abcb19-d605-48ee-8f0b-82a78259936f',
      status: undefined,
      logEntries: [],
      orderPositions: [
        {
          package: 'Großpackmittel (IBC) aus starrer Kunststoff (H) für flüssige Stoffe, mit baulicher Ausrüstung',
          packagingCode: '31H1',
          quantity: 1,
          unit: PackagingUnit.L,
          individualAmount: 1000,
          polluting: false,
          transportPoints: 3000,
          totalAmount: 1000,
          id: 35,
          deviceId: '0',
          status: '',
          logEntries: [],
          dangerousGood: {
            unNumber: '3105',
            casNumber: '37187-22-7',
            description: 'ORGANISCHES PEROXID TYP D, FLÜSSIG, N.A.G., (ENTHÄLT ACETYLACETONPEROXID)',
            label1: TransportLabel._3,
            label2: TransportLabel._6_1,
            label3: TransportLabel._,
            packingGroup: PackagingGroup.II,
            tunnelRestrictionCode: TunnelRestrictionCode['(D/E)'],
            transportCategory: TransportCategory['_2'],
          },
        },
      ],
      consignee: {
        name: 'Gefahrstoffvertriebsgesellschaft Huber GmbH',
        address: {
          street: 'Meraner Str.',
          number: '47',
          postalCode: '83024',
          city: 'Rosenheim',
          country: 'Deutschland',
        },
        contact: {
          name: 'Alois Huber',
          phone: '0049 098 765 432 1',
          mail: 'alois.huber@geschaeftsfuehrung.GVG-Huber.de',
          department: 'GL',
        },
      },
    },
  ],
};
