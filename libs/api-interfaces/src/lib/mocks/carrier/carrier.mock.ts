/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Carrier } from '@core/api-interfaces/lib/dtos/frontend';

export const mockCarrier: Carrier = new Carrier(
  'dangerous - Gefahrgutlogistik mit Blockchain',
  'Benjamin Diehard',
  'RO-AB123'
);
