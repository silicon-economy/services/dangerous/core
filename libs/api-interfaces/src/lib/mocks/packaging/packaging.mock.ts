/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Packaging } from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingTypes/packaging';

export const mockPackaging: Packaging = {
  category: 'test',
  code: 'test',
  kind: '',
  material: 'test',
  typesOfMaterial: 'A',
};
