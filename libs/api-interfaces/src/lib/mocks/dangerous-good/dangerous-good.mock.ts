/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DangerousGood } from '@core/api-interfaces/lib/dtos/frontend';
import {
  PackagingGroup,
  TransportCategory,
  TransportLabel,
  TunnelRestrictionCode,
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

export const mockDangerousGood: DangerousGood = {
  unNumber: '1090',
  casNumber: '67-64-1',
  description: 'ACETON',
  label1: TransportLabel._3,
  label2: TransportLabel._6_1,
  label3: TransportLabel._,
  packingGroup: PackagingGroup.II,
  tunnelRestrictionCode: TunnelRestrictionCode['(D/E)'],
  transportCategory: TransportCategory['_2'],
};
