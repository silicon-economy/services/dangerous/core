/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { LogEntry } from '@core/api-interfaces/lib/dtos/frontend/transport-document/log-entry';
import { TransportDocumentStatus } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

export const mockLogEntries = [
  new LogEntry(TransportDocumentStatus.created, 1692962291680, '', ''),
  new LogEntry(TransportDocumentStatus.accepted, 1692962291681, '', ''),
  new LogEntry(TransportDocumentStatus.transport, 1692962291682, '', ''),
  new LogEntry(TransportDocumentStatus.transport_denied, 1692962291683, '', ''),
];
