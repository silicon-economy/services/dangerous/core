/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import {
  PackagingGroup,
  PackagingUnit,
  TransportCategory,
  TransportLabel,
  TunnelRestrictionCode,
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

export const mockOrderPositionWithId: OrderPositionWithId = {
  package: 'Großpackmittel (IBC) aus starrer Kunststoff (H) für flüssige Stoffe, mit äußerer Umhüllung aus Stahl (A)',
  packagingCode: '31HA1',
  quantity: 1,
  unit: PackagingUnit.L,
  individualAmount: 1000,
  polluting: false,
  transportPoints: 3000,
  totalAmount: 1000,
  id: 1,
  deviceId: '1',
  status: '',
  logEntries: [],
  dangerousGood: {
    unNumber: '1230',
    casNumber: '67-56-1',
    description: 'METHANOL',
    label1: TransportLabel._3,
    label2: TransportLabel._6_1,
    label3: TransportLabel._,
    packingGroup: PackagingGroup.II,
    tunnelRestrictionCode: TunnelRestrictionCode['(C/D)'],
    transportCategory: TransportCategory._0,
  },
};
