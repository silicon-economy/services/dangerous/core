/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { mockOrderPositionWithId } from '@core/api-interfaces/lib/mocks/order-position/orderPosition.mock';

export const mockOrderWithId: OrderWithId = {
  id: '1',
  consignee: {
    name: 'Gefahrgutverteilzentrum',
    address: {
      city: 'Dortmund',
      street: 'Chemiestraße',
      number: '22A',
      postalCode: '44227',
      country: 'Deutschland',
    },
    contact: {
      name: 'Daggi',
      phone: '0231 5467-321',
      mail: 'daggi@example.com',
      department: 'Testabteilung',
    },
  },
  logEntries: [],
  status: undefined,
  orderPositions: [mockOrderPositionWithId, mockOrderPositionWithId, mockOrderPositionWithId],
};
