/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CreateGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend';
import {
  PackagingGroup,
  PackagingUnit,
  TransportCategory,
  TransportLabel,
  TunnelRestrictionCode,
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

export const mockCreateTransportDocumentRequestDto: CreateGoodsDataDto = {
  id: 'BP000004-2023825',
  consignor: {
    name: 'Chemical Industries Germany Inc.',
    address: {
      street: 'Ressestraße',
      number: '50',
      postalCode: '45894',
      city: 'Gelsenkirchen',
      country: 'Deutschland',
    },
    contact: {
      name: 'Fritz Hazard',
      phone: '0049 123 456 789 0',
      mail: 'fritz.hazard@ChemIndInc.de',
      department: 'OE 3 II',
    },
  },
  freight: {
    additionalInformation: 'Beförderung ohne Freistellung nach ADR 1.1.3.6',
    transportationInstructions: 'Bitte Beförderungseinheit nach der Entladung reinigen.',
    totalTransportPoints: 3330,
    orders: [
      {
        orderPositions: [
          {
            package: 'Fässer aus Kunststoff (H) abnehmbarer Deckel',
            packagingCode: '1H2',
            quantity: 1,
            unit: PackagingUnit.L,
            individualAmount: 60,
            polluting: false,
            transportPoints: 180,
            totalAmount: 60,
            dangerousGood: {
              unNumber: '1090',
              casNumber: '67-64-1',
              description: 'ACETON',
              label1: TransportLabel._3,
              label2: TransportLabel._6_1,
              label3: TransportLabel._,
              packingGroup: PackagingGroup.II,
              tunnelRestrictionCode: TunnelRestrictionCode['(D/E)'],
              transportCategory: TransportCategory['_2'],
            },
          },
          {
            package: 'Kanister aus Kunststoff (H) abnehmbarer Deckel',
            packagingCode: '3H2',
            quantity: 1,
            unit: PackagingUnit.L,
            individualAmount: 50,
            polluting: false,
            transportPoints: 150,
            totalAmount: 50,
            dangerousGood: {
              unNumber: '1230',
              casNumber: '67-56-1',
              description: 'METHANOL',
              label1: TransportLabel._3,
              label2: TransportLabel._6_1,
              label3: TransportLabel._,
              packingGroup: PackagingGroup.II,
              tunnelRestrictionCode: TunnelRestrictionCode['(D/E)'],
              transportCategory: TransportCategory['_2'],
            },
          },
        ],
        consignee: {
          name: 'Funke AG - Lacke und Farben',
          address: {
            street: 'Columbiadamm',
            number: '194',
            postalCode: '10965',
            city: 'Berlin',
            country: 'Deutschland',
          },
          contact: {
            name: 'Dr. Martina Zünd',
            phone: '0049 741 852 063 9',
            mail: 'Zuend@Funke.de',
            department: 'Abt. 4A',
          },
        },
      },
      {
        orderPositions: [
          {
            package: 'Großpackmittel (IBC) aus starrer Kunststoff (H) für flüssige Stoffe, mit baulicher Ausrüstung',
            packagingCode: '31H1',
            quantity: 1,
            unit: PackagingUnit.L,
            individualAmount: 1000,
            polluting: false,
            transportPoints: 3000,
            totalAmount: 1000,
            dangerousGood: {
              unNumber: '3105',
              casNumber: '37187-22-7',
              description: 'ORGANISCHES PEROXID TYP D, FLÜSSIG, N.A.G., (ENTHÄLT ACETYLACETONPEROXID)',
              label1: TransportLabel._3,
              label2: TransportLabel._6_1,
              label3: TransportLabel._,
              packingGroup: PackagingGroup.II,
              tunnelRestrictionCode: TunnelRestrictionCode['(D/E)'],
              transportCategory: TransportCategory['_2'],
            },
          },
        ],
        consignee: {
          name: 'Gefahrstoffvertriebsgesellschaft Huber GmbH',
          address: {
            street: 'Meraner Str.',
            number: '47',
            postalCode: '83024',
            city: 'Rosenheim',
            country: 'Deutschland',
          },
          contact: {
            name: 'Alois Huber',
            phone: '0049 098 765 432 1',
            mail: 'alois.huber@geschaeftsfuehrung.GVG-Huber.de',
            department: 'GL',
          },
        },
      },
    ],
  },
  carrier: {
    name: 'Gefahrgutlogistik',
    driver: 'Max Mustermann',
    licensePlate: 'RO-AB123',
  },
};
