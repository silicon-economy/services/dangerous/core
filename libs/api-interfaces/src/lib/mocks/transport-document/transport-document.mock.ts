/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { PackagingUnit } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/packaging-unit.enum';
import { OrderPositionStatus } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/status.enum';
import { TransportLabel } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/transport-label.enum';
import { PackagingGroup } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/packaging-group.enum';
import { TunnelRestrictionCode } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/tunnel-restriction-code.enum';
import { TransportCategory } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/transport-category.enum';
import { TransportVehicleCriteria } from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/transport-vehicle-criteria';
import {
  OrderStatus,
  TransportDocumentStatus,
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

// Created
export const mockTransportDocumentCreated: SaveGoodsDataDto = {
  id: 'BP000001-20210819',
  consignor: {
    name: 'Chemical Industries Germany Inc.',
    address: {
      street: 'Ressestraße',
      number: '50',
      postalCode: '45894',
      city: 'Gelsenkirchen',
      country: 'Deutschland',
    },
    contact: {
      name: 'Fritz Hazard',
      phone: '0049 123 456 789 0',
      mail: 'fritz.hazard@ChemIndInc.de',
      department: 'OE 3 II',
    },
  },
  freight: {
    additionalInformation: 'Beförderung ohne Freistellung nach ADR 1.1.3.6',
    transportationInstructions: 'Beachtung der höchstzulässigen anwendbaren Stapellast gem. ADR 6.5.2.2.2',
    totalTransportPoints: 3000,
    orders: [
      {
        id: '0e7d3e92-f59b-4985-a61b-4267270cf57a',
        status: OrderStatus._,
        logEntries: [],
        orderPositions: [
          {
            package:
              'Großpackmittel (IBC) aus starrer Kunststoff (H) für flüssige Stoffe, mit äußerer Umhüllung aus Stahl (A)',
            packagingCode: '31HA1',
            quantity: 1,
            unit: PackagingUnit.L,
            individualAmount: 1000,
            polluting: false,
            transportPoints: 3000,
            totalAmount: 1000,
            id: 1,
            deviceId: '1',
            status: '',
            logEntries: [],
            dangerousGood: {
              unNumber: '1230',
              casNumber: '67-56-1',
              description: 'METHANOL',
              label1: TransportLabel._3,
              label2: TransportLabel._6_1,
              label3: TransportLabel._,
              packingGroup: PackagingGroup.II,
              tunnelRestrictionCode: TunnelRestrictionCode['(C/D)'],
              transportCategory: TransportCategory._0,
            },
          },
        ],
        consignee: {
          name: 'Funke AG - Lacke und Farben',
          address: {
            street: 'Columbiadamm',
            number: '194',
            postalCode: '10965',
            city: 'Berlin',
            country: 'Deutschland',
          },
          contact: {
            name: 'Dr. Martina Zünd',
            phone: '0049 741 852 063 9',
            mail: 'Zuend@Funke.de',
            department: 'Abt. 4A',
          },
        },
      },
    ],
  },
  carrier: {
    name: 'Gefahrgutlogistik',
    driver: 'Mustermann',
    licensePlate: 'DR-EI333',
  },
  logEntries: [
    {
      status: TransportDocumentStatus.created,
      date: 1629372434571,
      author: '',
      description: '',
    },
  ],
  status: TransportDocumentStatus.created,
  createdDate: 1629372434571,
  lastUpdate: 1629372601559,
};

// Accepted
const mockTransportDocumentAcceptedPrepare: SaveGoodsDataDto = structuredClone(mockTransportDocumentCreated);
mockTransportDocumentAcceptedPrepare.status = TransportDocumentStatus.accepted;
mockTransportDocumentAcceptedPrepare.logEntries.push({
  status: TransportDocumentStatus.accepted,
  date: 1629372458465,
  author: '',
  description: '',
  acceptanceCriteria: {
    carrierCheckCriteria: {
      acceptConsignor: true,
      acceptConsignees: [true],
      acceptFreight: true,
      acceptAdditionalInformation: true,
      acceptTransportationInstructions: true,
    },
    comment: '',
  },
});
export const mockTransportDocumentAccepted: SaveGoodsDataDto = mockTransportDocumentAcceptedPrepare;

// Released
const mockTransportDocumentReleasedPrepare: SaveGoodsDataDto = structuredClone(mockTransportDocumentAccepted);
mockTransportDocumentReleasedPrepare.status = TransportDocumentStatus.released;
mockTransportDocumentReleasedPrepare.logEntries.push({
  status: TransportDocumentStatus.released,
  date: 1629372468326,
  author: '',
  description: '',
});
export const mockTransportDocumentReleased: SaveGoodsDataDto = mockTransportDocumentReleasedPrepare;

// Vehicle accepted
const mockTransportDocumentVehicleAcceptedPrepare: SaveGoodsDataDto = structuredClone(mockTransportDocumentReleased);
mockTransportDocumentVehicleAcceptedPrepare.status = TransportDocumentStatus.vehicle_accepted;
mockTransportDocumentVehicleAcceptedPrepare.logEntries.push({
  status: TransportDocumentStatus.vehicle_accepted,
  date: 1702046788344,
  author: '',
  description: '',
  acceptanceCriteria: {
    licencePlate: 'RO-AB123',
    acceptVehicleCondition: true,
    acceptVehicleSafetyEquipment: true,
    driverName: 'Max Mustermann',
    acceptCarrierInformation: true,
    acceptCarrierSafetyEquipment: true,
    comment: '',
  } as TransportVehicleCriteria,
});
export const mockTransportDocumentVehicleAccepted: SaveGoodsDataDto = mockTransportDocumentVehicleAcceptedPrepare;

// Transport
const mockTransportDocumentTransportPrepare: SaveGoodsDataDto = structuredClone(mockTransportDocumentVehicleAccepted);
mockTransportDocumentTransportPrepare.status = TransportDocumentStatus.transport;
mockTransportDocumentTransportPrepare.logEntries.push({
  status: TransportDocumentStatus.transport,
  date: 1629372601559,
  author: 'Bernd Beförderer',
  description: '',
});
mockTransportDocumentTransportPrepare.freight.orders[0].orderPositions[0].status =
  OrderPositionStatus.visual_inspection_carrier_accepted;
mockTransportDocumentTransportPrepare.freight.orders[0].orderPositions[0].logEntries.push({
  status: OrderPositionStatus.accepted_by_carrier,
  date: 1629372601559,
  author: 'Bernd Beförderer',
  description: '',
  acceptanceCriteria: {
    comment: '',
    visualInspectionCarrierCriteria: {
      acceptTransportability: true,
      acceptLabeling: true,
    },
  },
});

export const mockTransportDocumentTransport: SaveGoodsDataDto = mockTransportDocumentTransportPrepare;
