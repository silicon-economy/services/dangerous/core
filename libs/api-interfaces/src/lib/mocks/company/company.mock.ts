/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Address, Company, ContactPerson } from '@core/api-interfaces/lib/dtos/frontend';

const mockAddress: Address = new Address('Ressestraße', '9', '83209', 'Prien am Chiemsee', 'Germany');
const mockContactPerson: ContactPerson = new ContactPerson(
  'Max Mustermann',
  '49 5985 03809',
  'mustermann@mail.com',
  '16'
);
export const mockCompany: Company = new Company('TDL', mockAddress, mockContactPerson);
