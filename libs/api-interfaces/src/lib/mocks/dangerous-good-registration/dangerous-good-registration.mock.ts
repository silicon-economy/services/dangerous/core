/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { SaveDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-dangerous-good-registration.dto';
import {
  PackagingGroup,
  PackagingUnit,
  TransportCategory,
  TransportLabel,
  TunnelRestrictionCode,
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

export const mockDangerousGoodRegistration: SaveDangerousGoodRegistrationDto = {
  creator: 'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
  id: 'DGR000001-2023828',
  createdDate: 1693229293427,
  lastUpdate: 1693229293427,
  consignor: {
    name: 'Chemical Industries Germany Inc.',
    address: {
      street: 'Ressestraße',
      number: '50',
      postalCode: '45894',
      city: 'Gelsenkirchen',
      country: 'Deutschland',
    },
    contact: {
      name: 'Fritz Hazard',
      phone: '0049 123 456 789 0',
      mail: 'fritz.hazard@ChemIndInc.de',
      department: 'OE 3 II',
    },
  },
  freight: {
    additionalInformation: 'Beförderung ohne Freistellung nach ADR 1.1.3.6',
    transportationInstructions: 'Bitte Beförderungseinheit nach der Entladung reinigen.',
    totalTransportPoints: 3330,
    orders: [
      {
        id: '0b2c8319-ce08-4dea-bd6b-de09e605d12c',
        status: undefined,
        logEntries: [],
        orderPositions: [
          {
            package: 'Fässer aus Kunststoff (H) abnehmbarer Deckel',
            packagingCode: '1H2',
            quantity: 1,
            unit: PackagingUnit.L,
            individualAmount: 60,
            polluting: false,
            transportPoints: 180,
            totalAmount: 60,
            id: 29,
            deviceId: '0',
            status: '',
            logEntries: [],
            dangerousGood: {
              unNumber: '1090',
              casNumber: '67-64-1',
              description: 'ACETON',
              label1: TransportLabel._3,
              label2: TransportLabel._,
              label3: TransportLabel._,
              packingGroup: PackagingGroup.II,
              tunnelRestrictionCode: TunnelRestrictionCode['(E)'],
              transportCategory: TransportCategory._2,
            },
          },
        ],
        consignee: {
          name: 'Funke AG - Lacke und Farben',
          address: {
            street: 'Columbiadamm',
            number: '194',
            postalCode: '10965',
            city: 'Berlin',
            country: 'Deutschland',
          },
          contact: {
            name: 'Dr. Martina Zünd',
            phone: '0049 741 852 063 9',
            mail: 'Zuend@Funke.de',
            department: 'Abt. 4A',
          },
        },
      },
    ],
  },
};
