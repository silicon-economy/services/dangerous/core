/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AmqpService } from './service/amqp.service';

@Module({ providers: [AmqpService], exports: [AmqpService] })
export class AmqpModule {
  static register(queue: string) {
    return ClientsModule.registerAsync([
      {
        name: queue,
        imports: [ConfigModule],
        inject: [ConfigService],
        // eslint-disable-next-line @typescript-eslint/require-await
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.RMQ,
          options: {
            urls: [configService.get('amqpUrl')] as string[],
            queue: configService.get<string>('queuePrefix') + queue,
            queueOptions: {
              durable: true,
            },
          },
        }),
      },
    ]);
  }
}
