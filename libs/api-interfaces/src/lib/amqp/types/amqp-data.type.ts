/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { SaveGoodsDataDto } from '../../dtos/data-management/transport-document/save-goods-data.dto';
import { AcceptDangerousGoodRegistrationDto, SaveDangerousGoodRegistrationDto } from '../../dtos/data-management';
import { UpdateVisualInspectionCarrierDto } from '../../dtos/data-management/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import { UpdateVisualInspectionConsigneeDto } from '../../dtos/data-management/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';

export type PermittedSendDataTypes =
  | void
  | string
  | number
  | SaveGoodsDataDto
  | SaveDangerousGoodRegistrationDto
  | UpdateVisualInspectionCarrierDto
  | UpdateVisualInspectionConsigneeDto
  | AcceptDangerousGoodRegistrationDto;
