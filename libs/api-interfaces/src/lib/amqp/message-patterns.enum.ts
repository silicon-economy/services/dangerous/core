/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

// Dangerous Goods Gateway

/**
 * Dangerous goods lookup amqp message patterns
 */
export enum MessagePatternsDangerousGoodLookup {
  GET = 'dangerous/dangerous-good-lookup/inputstring',
}

/**
 * Packaging lookup amqp message patterns
 */
export enum MessagePatternsPackagingLookup {
  GET = 'dangerous/packaging-lookup',
  GET_BY_CODE = 'dangerous/packaging-lookup/code',
}

/**
 * Business logic amqp message patterns
 */
export enum MessagePatternsBusinessLogic {
  CALCULATE_EXEMPTION = 'dangerous/businesslogic/adr',
}

// Transport Document Gateway

/**
 * Data management (transport document) amqp message patterns
 */
export enum MessagePatternsDataManagement {
  GET_TRANSPORT_DOCUMENT = 'dangerous/data-management/get',
  GET_TRANSPORT_DOCUMENT_FOR_CONSIGNEE = 'dangerous/data-management/get/for-consignee',
  GET_ALL_TRANSPORT_DOCUMENTS = 'dangerous/data-management/get-all',
  GET_ALL_TRANSPORT_DOCUMENTS_FOR_CONSIGNEE = 'dangerous/data-management/get-all/for-consignee',
  CREATE_TRANSPORT_DOCUMENT = 'dangerous/data-management/create',
  CREATE_AND_RELEASE_TRANSPORT_DOCUMENT = 'dangerous/data-management/create-and-release',
  UPDATE_TRANSPORT_DOCUMENT = 'dangerous/data-management/update',
  DELETE_TRANSPORT_DOCUMENT_FROM_WORLD_STATE = 'dangerous/data-management/delete',
  CARRIER_CHECK = 'dangerous/data-management/carrier-check',
  RELEASE_TRANSPORT_DOCUMENT = 'dangerous/data-management/release',
  VEHICLE_INSPECTION = 'dangerous/data-management/vehicle-inspection',
  GET_TRANSPORT_DOCUMENT_ID_BY_ORDER_POSITION_ID = 'dangerous/data-management/transport-document/id-by-order-position-id',
  UPDATE_VISUAL_INSPECTION_CARRIER = 'dangerous/data-management/transport-document/update-visual-inspection-carrier',
  UPDATE_VISUAL_INSPECTION_CONSIGNEE = 'dangerous/data-management/transport-document/update-visual-inspection-consignee',
  CONFIRM_CARRIER = 'dangerous/data-management/confirm-carrier',
  ACCEPT_DANGEROUS_GOOD_REGISTRATION = 'dangerous/data-management/transport-document/accept-dgr',
}

/**
 * Data management (dangerous goods registration) amqp message patterns
 */
export enum MessagePatternsDangerousGoodRegistration {
  GET_ALL_DANGEROUS_GOOD_REGISTRATIONS = 'dangerous/data-management/dangerous-good-registration/get-all',
  GET_DANGEROUS_GOOD_REGISTRATION = 'dangerous/data-management/dangerous-good-registration/get',
  CREATE_DANGEROUS_GOOD_REGISTRATION = 'dangerous/data-management/dangerous-good-registration/create',
  UPDATE_DANGEROUS_GOOD_REGISTRATION = 'dangerous/data-management/dangerous-good-registration/update',
  DELETE_DANGEROUS_GOOD_REGISTRATION = 'dangerous/data-management/dangerous-good-registration/delete',
}

/**
 * Media management link patterns
 */
export enum MessagePatternsMediaManagement {
  GET_PDF_BY_TRANSPORT_DOCUMENT_ID = 'dangerous/media-management/transport-document-id',
}

/**
 * Sensing puck link patterns
 */
export enum MessagePatternsTendermintSensingPuck {
  GET_PUCK_DATA_BY_JOB_ID = 'dangerous/tendermint/sensing-puck/get-puck-data-by-job-id',
  GET_ALL_PUCK_DATA = 'dangerous/tendermint/sensing-puck/get-all-puck-data',
  GET_ALL_PAST_EVENTS = 'dangerous/tendermint/sensing-puck/get-all-past-events',
  UPLOAD_SENSING_PUCK_DATA = 'dangerous/tendermint/sensing-puck/upload-sensing-puck-data',
}

// Tendermint Service

/**
 * Tendermint service (dangerous goods registration) amqp message patterns
 */
export enum MessagePatternsTendermintDangerousGoodRegistration {
  GET_ALL_DANGEROUS_GOOD_REGISTRATIONS = 'dangerous/tendermint/dangerous-good-registration/get-all',
  GET_DANGEROUS_GOOD_REGISTRATION = 'dangerous/tendermint/dangerous-good-registration/get',
  CREATE_DANGEROUS_GOOD_REGISTRATION = 'dangerous/tendermint/dangerous-good-registration/create',
  UPDATE_DANGEROUS_GOOD_REGISTRATION = 'dangerous/tendermint/dangerous-good-registration/update',
  DELETE_DANGEROUS_GOOD_REGISTRATION = 'dangerous/tendermint/dangerous-good-registration/delete',
}

/**
 * Tendermint service (transport document) amqp message patterns
 */
export enum MessagePatternsTendermintTransportDocument {
  GET_ALL_TRANSPORT_DOCUMENTS = 'dangerous/tendermint/transport-document/get-all',
  GET_TRANSPORT_DOCUMENT = 'dangerous/tendermint/transport-document/get',
  CREATE_TRANSPORT_DOCUMENT = 'dangerous/tendermint/transport-document/create',
  UPDATE_TRANSPORT_DOCUMENT = 'dangerous/tendermint/transport-document/update',
  DELETE_TRANSPORT_DOCUMENT = 'dangerous/tendermint/transport-document/delete',
  GET_TRANSPORT_DOCUMENT_ID_BY_ORDER_POSITION_ID = 'dangerous/tendermint/transport-document/id-by-order-position-id',
  UPDATE_VISUAL_INSPECTION_CARRIER = 'dangerous/tendermint/transport-document/update-visual-inspection-carrier',
  UPDATE_VISUAL_INSPECTION_CONSIGNEE = 'dangerous/tendermint/transport-document/update-visual-inspection-consignee',
  ACCEPT_DANGEROUS_GOOD_REGISTRATION = 'dangerous/tendermint/transport-document/accept-dgr',
}
