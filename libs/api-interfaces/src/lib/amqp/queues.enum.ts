/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Amqp queue names
 */
export enum Queues {
  DANGEROUS_LOOKUP = 'dangerousLookup',
  BUSINESS_LOGIC = 'businessLogic',
  DATA_MANAGEMENT = 'dataManagement',
  TENDERMINT_SERVICE = 'tendermintService',
  MEDIA_MANAGEMENT = 'mediaManagement',
}
