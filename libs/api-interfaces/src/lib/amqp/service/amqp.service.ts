/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';
import { ClientProxy } from '@nestjs/microservices';
import { Queues } from '../queues.enum';
import { PermittedSendDataTypes } from '../types/amqp-data.type';

@Injectable()
export class AmqpService {
  constructor(@Inject(Queues.TENDERMINT_SERVICE) public readonly client: ClientProxy) {}

  /**
   * Auxiliary function for using `this.client.send()` in return for easier jest testing.
   *
   * @param amqpMessagePattern - Amqp message pattern
   * @param data - Data
   * @param responseType - Response type
   * @returns Depends on usage of this function
   */
  async sendToAmqpBroker(
    amqpMessagePattern: string,
    data: PermittedSendDataTypes,
    responseType?: unknown
  ): Promise<typeof responseType> {
    return firstValueFrom(this.client.send<typeof responseType>(amqpMessagePattern, data), { defaultValue: '' });
  }
}
