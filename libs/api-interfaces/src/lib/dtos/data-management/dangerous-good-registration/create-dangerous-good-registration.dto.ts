/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmptyObject, IsOptional, IsString, ValidateNested } from 'class-validator';
import { Company } from '../transport-document/transport-document/company';
import { Freight } from '../transport-document/transport-document/freight';
import { FreightRegistration } from '../transport-document/transport-document/freight-registration';

/**
 * Some attributes are marked as optional due to the status of development!
 */
export class CreateDangerousGoodRegistrationDto {
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Company)
  @ApiProperty({})
  consignor: Company;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => FreightRegistration)
  @ApiProperty({})
  freight: Freight;

  @IsOptional()
  @IsString()
  @ApiProperty({})
  createdBy?: string;
}
