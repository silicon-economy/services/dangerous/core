/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmptyObject, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';
import { CreateDangerousGoodRegistrationDto } from './create-dangerous-good-registration.dto';
import { FreightIdsIncluded } from '../transport-document/transport-document/freight-ids-included';
import { Company } from '../transport-document/transport-document/company';
import { Freight } from '../transport-document/transport-document/freight';

export class SaveDangerousGoodRegistrationDto extends CreateDangerousGoodRegistrationDto {
  @IsString()
  @ApiProperty()
  id: string;

  @IsString()
  creator?: string;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Company)
  @ApiProperty({})
  consignor: Company;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Freight)
  @ApiProperty({})
  freight: FreightIdsIncluded;

  @IsNumber()
  @ApiProperty()
  createdDate: number;

  @IsOptional()
  @IsNumber()
  lastUpdate?: number;
}
