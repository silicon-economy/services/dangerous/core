/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsNotEmptyObject, ValidateNested } from 'class-validator';
import { SaveDangerousGoodRegistrationDto } from './save-dangerous-good-registration.dto';

export class QueryGetDangerousGoodRegistrationResponse {
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => SaveDangerousGoodRegistrationDto)
  dangerousGoodRegistration: SaveDangerousGoodRegistrationDto;
}

export class QueryAllDangerousGoodRegistrationResponse {
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => SaveDangerousGoodRegistrationDto)
  dangerousGoodRegistrations: SaveDangerousGoodRegistrationDto[];
}
