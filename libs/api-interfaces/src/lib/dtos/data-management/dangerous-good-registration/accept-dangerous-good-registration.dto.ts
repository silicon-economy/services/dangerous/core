/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {CalculateExemptionResponseDto} from "../../business-logic/response/business-logic.response.dto";
import {Carrier} from "../transport-document/transport-document/carrier";
import {IsNotEmptyObject, IsOptional, IsString, ValidateNested} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";
import {Type} from "class-transformer";

export class AcceptDangerousGoodRegistrationDto {
  @IsOptional()
  @IsString()
  @ApiProperty()
  transportDocumentId?: string;

  @IsString()
  @ApiProperty()
  dangerousGoodRegistrationId: string;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Carrier)
  @ApiProperty({})
  carrier: Carrier;

  @IsString()
  @ApiProperty()
  transportationInstructions: string;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Number)
  @ApiProperty({ type: [Number] })
  acceptedOrderPositionIds: number[];

  newExemptionValues?: CalculateExemptionResponseDto;
}
