/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface AlertDto {
  deviceId: number;
  jobId: number;
  temperature: number;
  upperTemperatureThreshold: number;
  lowerTemperatureThreshold: number;
  humidity: number;
  upperHumidityThreshold: number;
  lowerHumidityThreshold: number;
  timestamp: number;
}
