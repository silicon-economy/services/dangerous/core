/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmptyObject, IsString, ValidateNested } from 'class-validator';
import {DeviceDataDto} from "./device-data.dto";
import {FirmwareDescriptor} from "../../blockchain/deviceJobData.dto";

export class UploadSensingPuckDataDto {
  @IsString()
  creator: string;

  @ApiProperty()
  jobId: number;

  @ApiProperty()
  deviceId: number;

  @ApiProperty()
  protocolVersion: number;

  @ApiProperty()
  firmwareDescriptor: FirmwareDescriptor;

  @ApiProperty()
  comTimestamp: number;

  @ApiProperty()
  lastComCause: string;

  @IsNotEmptyObject()
  @ApiProperty({ type: [DeviceDataDto] })
  @ValidateNested({ each: true })
  @Type(() => DeviceDataDto)
  data: DeviceDataDto[];
}
