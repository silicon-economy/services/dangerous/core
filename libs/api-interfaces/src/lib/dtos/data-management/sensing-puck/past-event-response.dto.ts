/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { PastEvent } from "../../blockchain/past_event";



export class PastEventResponse {
  PastEvent: PastEvent[];
  pagination: null;
}
