/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsNotEmptyObject, IsOptional, IsString, ValidateNested } from 'class-validator';
import { Company } from './transport-document/company';
import { Carrier } from './transport-document/carrier';
import { Freight } from './transport-document/freight';
import { ApiProperty } from '@nestjs/swagger';

/**
 * Some attributes are marked as optional due to the status of development!
 */
export class CreateGoodsDataDto {
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Company)
  @ApiProperty({})
  consignor: Company;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Freight)
  @ApiProperty({})
  freight: Freight;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Carrier)
  @ApiProperty({})
  carrier: Carrier;

  @IsOptional()
  @IsString()
  @ApiProperty({})
  createdBy?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({})
  id?: string;
}
