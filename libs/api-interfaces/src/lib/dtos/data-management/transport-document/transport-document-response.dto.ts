/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Type} from 'class-transformer';
import {IsNotEmptyObject, IsString, ValidateNested} from 'class-validator';
import {SaveGoodsDataDto} from "./save-goods-data.dto";


export class QueryGetTransportDocumentResponse {
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => SaveGoodsDataDto)
  transportDocument: SaveGoodsDataDto;
}

export class QueryAllTransportDocumentResponse {
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => SaveGoodsDataDto)
  transportDocument: SaveGoodsDataDto[];
}

export class QueryGetTransportDocumentIdByOrderPositionIdResponse {
  @IsString()
  transportDocumentId: string;
}
