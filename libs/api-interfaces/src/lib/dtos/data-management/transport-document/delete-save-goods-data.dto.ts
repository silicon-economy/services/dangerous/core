/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

/**
 * Some attributes are marked as optional due to the status of development!
 */
export class DeleteGoodsDataDto {
  @IsString()
  @ApiProperty()
  id: string;

  @IsOptional()
  @IsString()
  creator?: string;
}
