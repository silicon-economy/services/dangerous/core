/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TransportVehicleCriteria} from "./transport-document/acceptance-criteria/transport-vehicle-criteria";


export class TransportVehicleInspectionRequestDto {
  documentId: string;
  transportVehicleCriteria: TransportVehicleCriteria;
}
