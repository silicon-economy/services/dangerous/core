/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ApiProperty } from '@nestjs/swagger';
import {IsBoolean, IsNumber, IsOptional, IsString} from 'class-validator';

/**
 * Some attributes are marked as optional due to the status of development!
 */
export class OrderPositionVisualInspectionCarrierDto {
  @IsNumber()
  @ApiProperty()
  orderPositionId: number;

  @IsBoolean()
  @ApiProperty()
  acceptTransportability: boolean;

  @IsBoolean()
  @ApiProperty()
  acceptLabeling: boolean;

  @IsString()
  @IsOptional()
  @ApiProperty()
  comment?: string;
}
