/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsString, ValidateNested } from 'class-validator';
import { OrderVisualInspectionCarrierDto } from './order-visual-inspection-carrier.dto';

/**
 * Some attributes are marked as optional due to the status of development!
 */
export class UpdateVisualInspectionCarrierDto {
  @IsString()
  @ApiProperty()
  transportDocumentId: string;

  // @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => OrderVisualInspectionCarrierDto)
  @ApiProperty({ type: [OrderVisualInspectionCarrierDto] })
  orders: OrderVisualInspectionCarrierDto[];
}
