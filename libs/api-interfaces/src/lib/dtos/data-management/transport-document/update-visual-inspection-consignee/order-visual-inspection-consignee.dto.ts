/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsString, ValidateNested } from 'class-validator';
import {OrderPositionVisualInspectionConsigneeDto} from "./order-position-visual-inspection-consignee.dto";
import {ApiProperty} from "@nestjs/swagger";


/**
 * Some attributes are marked as optional due to the status of development!
 */
export class OrderVisualInspectionConsigneeDto {
  @IsString()
  @ApiProperty()
  orderId: string;

  // @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => OrderPositionVisualInspectionConsigneeDto)
  @ApiProperty({ type: [OrderPositionVisualInspectionConsigneeDto] })
  orderPositions: OrderPositionVisualInspectionConsigneeDto[];
}
