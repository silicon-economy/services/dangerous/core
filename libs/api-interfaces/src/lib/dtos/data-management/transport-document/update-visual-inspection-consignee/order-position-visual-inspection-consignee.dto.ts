/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {IsBoolean, IsNumber, IsOptional, IsString} from 'class-validator';
import {ApiProperty} from "@nestjs/swagger";

/**
 * Some attributes are marked as optional due to the status of development!
 */
export class OrderPositionVisualInspectionConsigneeDto {
  @IsNumber()
  @ApiProperty()
  orderPositionId: number;

  @IsBoolean()
  @ApiProperty()
  acceptQuantity: boolean;

  @IsBoolean()
  @ApiProperty()
  acceptIntactness: boolean;

  @IsString()
  @IsOptional()
  @ApiProperty()
  comment?: string;
}
