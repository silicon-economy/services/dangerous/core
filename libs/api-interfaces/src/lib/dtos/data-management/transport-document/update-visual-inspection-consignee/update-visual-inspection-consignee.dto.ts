/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsString, ValidateNested } from 'class-validator';
import {OrderVisualInspectionConsigneeDto} from "./order-visual-inspection-consignee.dto";
import {ApiProperty} from "@nestjs/swagger";


/**
 * Some attributes are marked as optional due to the status of development!
 */
export class UpdateVisualInspectionConsigneeDto {
  @IsString()
  @ApiProperty()
  transportDocumentId: string;

  // @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => OrderVisualInspectionConsigneeDto)
  @ApiProperty({ type: [OrderVisualInspectionConsigneeDto] })
  order: OrderVisualInspectionConsigneeDto;
}
