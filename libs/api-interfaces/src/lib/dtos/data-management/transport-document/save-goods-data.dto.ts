/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsArray, IsEnum, IsNotEmptyObject, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';
import { TransportDocumentStatus } from './transport-document/enums/status.enum';
import { Company } from './transport-document/company';
import { Carrier } from './transport-document/carrier';
import { FreightIdsIncluded } from './transport-document/freight-ids-included';
import { LogEntry } from './transport-document/log-entry';
import { CreateGoodsDataDto } from './create-goods-data.dto';
import { ApiProperty } from '@nestjs/swagger';

/**
 * Some attributes are marked as optional due to the status of development!
 */
export class SaveGoodsDataDto extends CreateGoodsDataDto {
  constructor(
    id: string,
    consignor: Company,
    carrier: Carrier,
    freight: FreightIdsIncluded,
    logEntries: LogEntry[],
    status: string,
    createdDate: number,
    lastUpdate: number
  ) {
    super();
    this.id = id;
    this.consignor = consignor;
    this.carrier = carrier;
    this.freight = freight;
    this.logEntries = logEntries;
    this.status = status;
    this.createdDate = createdDate;
    this.lastUpdate = lastUpdate;
  }

  @IsString()
  @ApiProperty()
  id: string;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Company)
  @ApiProperty({})
  consignor: Company;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Carrier)
  @ApiProperty({})
  carrier: Carrier;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => FreightIdsIncluded)
  @ApiProperty({ type: FreightIdsIncluded })
  freight: FreightIdsIncluded;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => LogEntry)
  @ApiProperty({ type: [LogEntry] })
  logEntries: LogEntry[];

  @IsEnum(TransportDocumentStatus)
  @ApiProperty({ enum: TransportDocumentStatus })
  status: string;

  @IsNumber()
  @ApiProperty()
  createdDate: number;

  @IsOptional()
  @IsNumber()
  @ApiProperty({})
  lastUpdate?: number;
}
