/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {CarrierCheckCriteria} from "./transport-document/acceptance-criteria/carrier-check-criteria";


export class CarrierCheckRequestDto {
  documentId: string;
  carrierCheckCriteria: CarrierCheckCriteria;
}
