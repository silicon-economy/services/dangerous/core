/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { IsString } from 'class-validator';
import {ApiProperty} from "@nestjs/swagger";

export class Address {
  @IsString()
  @ApiProperty({})
  street: string;

  @IsString()
  @ApiProperty({})
  number: string;

  @IsString()
  @ApiProperty({})
  postalCode: string;

  @IsString()
  @ApiProperty({})
  city: string;

  @IsString()
  @ApiProperty({})
  country: string;
}
