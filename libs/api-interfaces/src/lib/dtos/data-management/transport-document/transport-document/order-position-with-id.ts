/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsArray, IsBoolean, IsEnum, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';
import { TransportDocumentStatus } from './enums/status.enum';
import { PackagingUnit } from './enums/packaging-unit.enum';
import { DangerousGood } from './dangerous-good';
import { LogEntry } from './log-entry';
import { ApiProperty } from '@nestjs/swagger';

export class OrderPositionWithId {
  constructor(
    dangerousGood: DangerousGood,
    packageType: string,
    packagingCode: string,
    quantity: number,
    unit: PackagingUnit,
    individualAmount: number,
    polluting: boolean,
    transportPoints: number,
    totalAmount: number,
    id: number,
    deviceId?: string,
    logEntries?: LogEntry[],
    status?: string
  ) {
    this.dangerousGood = dangerousGood;
    this.package = packageType;
    this.packagingCode = packagingCode;
    this.quantity = quantity;
    this.unit = unit;
    this.individualAmount = individualAmount;
    this.polluting = polluting;
    this.transportPoints = transportPoints;
    this.totalAmount = totalAmount;
    this.id = id;
    this.deviceId = deviceId;
    this.logEntries = logEntries;
    this.status = status;
  }

  @ValidateNested({ each: true })
  @Type(() => DangerousGood)
  @ApiProperty({})
  dangerousGood: DangerousGood;

  @IsString()
  @ApiProperty({})
  package: string;

  @IsString()
  @ApiProperty({})
  packagingCode: string;

  @IsNumber()
  @ApiProperty({})
  quantity: number;

  @IsEnum(PackagingUnit)
  @ApiProperty({ enum: PackagingUnit })
  unit: PackagingUnit;

  @IsNumber()
  @ApiProperty({})
  individualAmount: number;

  @IsBoolean()
  @ApiProperty({})
  polluting: boolean;

  @IsNumber()
  @ApiProperty({})
  transportPoints: number;

  @IsNumber()
  @ApiProperty({})
  totalAmount: number;

  @IsNumber()
  @ApiProperty({})
  id: number;

  @IsNumber()
  @ApiProperty({})
  @IsOptional()
  deviceId?: string;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => LogEntry)
  @ApiProperty({ type: [LogEntry] })
  @IsOptional()
  logEntries: LogEntry[];

  @IsEnum(TransportDocumentStatus)
  @ApiProperty({ enum: TransportDocumentStatus })
  @IsOptional()
  status: string;
}
