/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { IsString } from 'class-validator';
import {ApiProperty} from "@nestjs/swagger";

export class Carrier {
  @IsString()
  @ApiProperty({})
  name: string;

  @IsString()
  @ApiProperty({})
  driver: string;

  @IsString()
  @ApiProperty({})
  licensePlate: string;
}
