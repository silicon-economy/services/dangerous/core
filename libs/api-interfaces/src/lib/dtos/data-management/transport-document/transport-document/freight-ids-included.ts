/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsArray, IsEnum, IsNumber, IsString, ValidateNested } from 'class-validator';
import { AdditionalInformation } from './enums/additional-information.enum';
import { OrderWithId } from './order-with-id';
import { ApiProperty } from '@nestjs/swagger';

export class FreightIdsIncluded {
  constructor(
    orders: OrderWithId[],
    additionalInformation: AdditionalInformation | string,
    transportationInstructions: string,
    totalTransportPoints: number
  ) {
    this.orders = orders;
    this.additionalInformation = additionalInformation;
    this.transportationInstructions = transportationInstructions;
    this.totalTransportPoints = totalTransportPoints;
  }

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => OrderWithId)
  @ApiProperty({ type: [OrderWithId] })
  orders: OrderWithId[];

  @IsEnum(AdditionalInformation)
  @ApiProperty({ enum: AdditionalInformation })
  additionalInformation: AdditionalInformation | string;

  @IsString()
  @ApiProperty({})
  transportationInstructions: string;

  @IsNumber()
  @ApiProperty({})
  totalTransportPoints: number;
}
