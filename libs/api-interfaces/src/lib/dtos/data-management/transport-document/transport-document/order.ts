/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsArray, IsNotEmptyObject, ValidateNested } from 'class-validator';
import { Company } from './company';
import { OrderPosition } from './order-position';
import { ApiProperty } from '@nestjs/swagger';

export class Order {
  constructor(consignee: Company, orderPositions: OrderPosition[]) {
    this.consignee = consignee;
    this.orderPositions = orderPositions;
  }

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Company)
  @ApiProperty({ type: Company })
  consignee: Company;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => OrderPosition)
  @ApiProperty({ type: [OrderPosition] })
  orderPositions: OrderPosition[];
}
