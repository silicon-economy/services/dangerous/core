/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsNotEmptyObject, IsString, ValidateNested } from 'class-validator';
import {Address} from "./address";
import {ContactPerson} from "./contact-person";
import {ApiProperty} from "@nestjs/swagger";


export class Company {
  @IsString()
  @ApiProperty({})
  name: string;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Address)
  @ApiProperty({})
  address: Address;

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => ContactPerson)
  @ApiProperty({})
  contact: ContactPerson;
}
