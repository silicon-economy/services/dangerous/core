/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { IsArray, IsBoolean, IsOptional, IsString } from 'class-validator';

/**
 * Model for saving what group positions were accepted by the carrier
 */
export class CarrierCheckCriteria {
  @IsOptional()
  @IsString()
  comment?: string;

  /**
   * Group position for consignor
   */
  @IsBoolean()
  acceptConsignor: boolean;

  /**
   * Group position for consignee
   */
  @IsArray()
  @IsBoolean({ each: true })
  acceptConsignees: boolean[];

  /**
   * Group position for: load, quantity, packagingCode, totalAmount, totalTransportPoints
   */
  @IsBoolean()
  acceptFreight: boolean;

  /**
   * Group position for transportation instructions
   */
  @IsBoolean()
  acceptTransportationInstructions: boolean;

  /**
   * Group position for additional information
   */
  @IsBoolean()
  acceptAdditionalInformation: boolean;
}
