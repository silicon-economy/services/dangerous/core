/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsArray, IsEnum, IsNotEmptyObject, IsOptional, IsString, IsUUID, ValidateNested } from 'class-validator';
import { TransportDocumentStatus } from './enums/status.enum';
import { OrderPositionWithId } from './order-position-with-id';
import { Company } from './company';
import { LogEntry } from './log-entry';
import { ApiProperty } from '@nestjs/swagger';

export class OrderWithId {
  constructor(
    consignee: Company,
    orderPositions: OrderPositionWithId[],
    id: string,
    logEntries?: LogEntry[],
    status?: string
  ) {
    this.consignee = consignee;
    this.orderPositions = orderPositions;
    this.id = id;
    this.logEntries = logEntries;
    this.status = status;
  }

  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => Company)
  @ApiProperty({ type: Company })
  consignee: Company;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => OrderPositionWithId)
  @ApiProperty({ type: [OrderPositionWithId] })
  orderPositions: OrderPositionWithId[];

  @IsString()
  @IsUUID()
  @ApiProperty({})
  id: string;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => LogEntry)
  @ApiProperty({ type: [LogEntry] })
  @IsOptional()
  logEntries: LogEntry[];

  @IsEnum(TransportDocumentStatus)
  @ApiProperty({ enum: TransportDocumentStatus })
  @IsOptional()
  status: string;
}
