/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsEnum, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';
import { TransportDocumentStatus } from './enums/status.enum';
import { ApiProperty } from '@nestjs/swagger';
import { AcceptanceCriteria } from './acceptance-criteria/acceptance-criteria';

export class LogEntry {
  constructor(
    status: string,
    date: number,
    author: string,
    description?: string,
    acceptanceCriteria?: AcceptanceCriteria
  ) {
    this.status = status;
    this.date = date;
    this.author = author;
    this.description = description;
    this.acceptanceCriteria = acceptanceCriteria;
  }

  @IsEnum(TransportDocumentStatus)
  @ApiProperty({ enum: TransportDocumentStatus })
  status: string;

  @IsNumber()
  @ApiProperty()
  date: number;

  @IsOptional()
  @IsString()
  @ApiProperty({})
  author?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({})
  description?: string;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => AcceptanceCriteria)
  @ApiProperty({})
  acceptanceCriteria?: AcceptanceCriteria;
}
