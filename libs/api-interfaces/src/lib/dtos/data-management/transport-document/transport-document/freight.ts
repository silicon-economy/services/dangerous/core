/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsArray, IsEnum, IsNumber, IsString, ValidateNested } from 'class-validator';
import { AdditionalInformation } from './enums/additional-information.enum';
import { Order } from './order';
import { ApiProperty } from '@nestjs/swagger';

export class Freight {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => Order)
  @ApiProperty({ type: [Order] })
  orders: Order[];

  @IsEnum(AdditionalInformation)
  @ApiProperty({ enum: AdditionalInformation })
  additionalInformation: AdditionalInformation | string;

  @IsString()
  @ApiProperty({})
  transportationInstructions: string;

  @IsNumber()
  @ApiProperty({})
  totalTransportPoints: number;
}
