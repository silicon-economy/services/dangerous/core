/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { IsBoolean, IsOptional, IsString } from 'class-validator';

/**
 * Model for saving what group positions were accepted by the carrier
 */
export class TransportVehicleCriteria {
  @IsOptional()
  @IsString()
  comment?: string;

  @IsString()
  licencePlate: string;

  @IsBoolean()
  acceptVehicleCondition: boolean;

  @IsBoolean()
  acceptVehicleSafetyEquipment: boolean;

  @IsString()
  driverName: string;

  @IsBoolean()
  acceptCarrierInformation: boolean;

  @IsBoolean()
  acceptCarrierSafetyEquipment: boolean;
}
