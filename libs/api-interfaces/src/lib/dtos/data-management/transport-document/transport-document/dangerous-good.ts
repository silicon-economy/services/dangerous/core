/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { IsEnum, IsOptional, IsString } from 'class-validator';
import {TransportLabel} from "./enums/transport-label.enum";
import {PackagingGroup} from "./enums/packaging-group.enum";
import {TunnelRestrictionCode} from "./enums/tunnel-restriction-code.enum";
import {TransportCategory} from "./enums/transport-category.enum";
import {ApiProperty} from "@nestjs/swagger";


export class DangerousGood {
  @IsString()
  @ApiProperty({})
  unNumber: string;

  @IsString()
  @ApiProperty()
  casNumber: string;

  @IsString()
  @ApiProperty({})
  description: string;

  @IsEnum(TransportLabel)
  @ApiProperty({ enum: TransportLabel })
  label1: TransportLabel;

  @IsOptional()
  @IsEnum(TransportLabel)
  @ApiProperty({ enum: TransportLabel })
  label2: TransportLabel;

  @IsOptional()
  @IsEnum(TransportLabel)
  @ApiProperty({ enum: TransportLabel })
  label3: TransportLabel;

  @IsEnum(PackagingGroup)
  @ApiProperty({ enum: PackagingGroup })
  packingGroup: PackagingGroup;

  @IsEnum(TunnelRestrictionCode)
  @ApiProperty({ enum: TunnelRestrictionCode })
  tunnelRestrictionCode: TunnelRestrictionCode;

  @IsEnum(TransportCategory)
  @ApiProperty({ enum: TransportCategory })
  transportCategory: TransportCategory;
}
