/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from 'class-transformer';
import { IsBoolean, IsEnum, IsNotEmptyObject, IsNumber, IsString, ValidateNested } from 'class-validator';
import {PackagingUnit} from "./enums/packaging-unit.enum";
import {DangerousGood} from "./dangerous-good";
import {ApiProperty} from "@nestjs/swagger";


export class OrderPosition {
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => DangerousGood)
  @ApiProperty({})
  dangerousGood: DangerousGood;

  @IsString()
  @ApiProperty({})
  package: string;

  @IsString()
  @ApiProperty({})
  packagingCode: string;

  @IsNumber()
  @ApiProperty({})
  quantity: number;

  @IsEnum(PackagingUnit)
  @ApiProperty({ enum: PackagingUnit })
  unit: PackagingUnit;

  @IsNumber()
  @ApiProperty({})
  individualAmount: number;

  @IsBoolean()
  @ApiProperty({})
  polluting: boolean;

  @IsNumber()
  @ApiProperty({})
  transportPoints: number;

  @IsNumber()
  @ApiProperty({})
  totalAmount: number;
}
