/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { IsBoolean } from 'class-validator';

export class VisualInspectionCarrierCriteria {
  @IsBoolean()
  acceptTransportability: boolean;

  @IsBoolean()
  acceptLabeling: boolean;
}
