/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { CompanyDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/company.dto';

export class CreateUserDto {
  username: string;
  password: string;
  roles: UserRole[];
  company: CompanyDto;

  constructor(username: string, password: string, roles: UserRole[], company: CompanyDto) {
    this.username = username;
    this.password = password;
    this.roles = roles;
    this.company = company;
  }
}
