/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class ContactPersonDto {
  name: string;
  phone: string;
  mail: string;
  department: string;

  constructor(name: string, phone: string, mail: string, department: string) {
    this.name = name;
    this.phone = phone;
    this.mail = mail;
    this.department = department;
  }
}
