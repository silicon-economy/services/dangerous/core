/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { UserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/user.dto';

export class JwtDto extends UserDto {
  username: string;
  roles: UserRole[];
  iat: number;

  constructor(username: string, roles: UserRole[], user: UserDto, iat?: number) {
    super(user.id, user.company);
    this.username = username;
    this.roles = roles;
    this.iat = iat;
  }
}
