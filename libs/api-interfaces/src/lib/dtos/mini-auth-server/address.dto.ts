/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class AddressDto {
  street: string;
  number: string;
  postalCode: string;
  city: string;
  country: string;

  constructor(street: string, number: string, postalCode: string, city: string, country: string) {
    this.street = street;
    this.number = number;
    this.postalCode = postalCode;
    this.city = city;
    this.country = country;
  }
}
