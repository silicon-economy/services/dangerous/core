/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AddressDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/address.dto';
import { ContactPersonDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/contact-person.dto';

export class CompanyDto {
  name: string;
  address: AddressDto;
  contact: ContactPersonDto;

  constructor(name: string, address: AddressDto, contact: ContactPersonDto) {
    this.name = name;
    this.address = address;
    this.contact = contact;
  }
}
