/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CompanyDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/company.dto';

export class UserDto {
  id: string;
  company: CompanyDto;

  constructor(id: string, company: CompanyDto) {
    this.id = id;
    this.company = company;
  }
}
