/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company, Freight } from '@core/api-interfaces/lib/dtos/frontend';

export class CreateDangerousGoodRegistrationDto {
  consignor: Company;
  freight: Freight;
  createdBy?: string;

  constructor(consignor: Company, freight: Freight, createdBy?: string) {
    this.consignor = consignor;
    this.freight = freight;
    this.createdBy = createdBy;
  }
}
