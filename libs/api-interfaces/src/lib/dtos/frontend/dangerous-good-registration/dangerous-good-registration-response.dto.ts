/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { SaveDangerousGoodRegistrationDto } from './save-dangerous-good-registration.dto';

export class QueryGetDangerousGoodRegistrationResponse {
  dangerousGoodRegistration: SaveDangerousGoodRegistrationDto;

  constructor(dangerousGoodRegistration: SaveDangerousGoodRegistrationDto) {
    this.dangerousGoodRegistration = dangerousGoodRegistration;
  }
}

export class QueryAllDangerousGoodRegistrationResponse {
  dangerousGoodRegistrations: SaveDangerousGoodRegistrationDto[];
}
