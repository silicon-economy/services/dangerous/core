/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CalculateExemptionResponseDto } from '@core/api-interfaces';
import { Carrier } from '@core/api-interfaces/lib/dtos/frontend';

export class AcceptDangerousGoodRegistrationDto {
  transportDocumentId?: string;
  dangerousGoodRegistrationId: string;
  carrier: Carrier;
  transportationInstructions: string;
  acceptedOrderPositionIds: number[];
  newExemptionValues?: CalculateExemptionResponseDto;


  constructor()
  constructor(
    dangerousGoodRegistrationId: string,
    carrier: Carrier,
    transportationInstructions: string,
    acceptedOrderPositionIds: number[],
    transportDocumentId?: string,
    newExemptionValues?: CalculateExemptionResponseDto
  )
  constructor(
    dangerousGoodRegistrationId?: string,
    carrier?: Carrier,
    transportationInstructions?: string,
    acceptedOrderPositionIds?: number[],
    transportDocumentId?: string,
    newExemptionValues?: CalculateExemptionResponseDto
  ) {
    this.transportDocumentId = transportDocumentId;
    this.dangerousGoodRegistrationId = dangerousGoodRegistrationId;
    this.carrier = carrier;
    this.transportationInstructions = transportationInstructions;
    this.acceptedOrderPositionIds = acceptedOrderPositionIds;
    this.newExemptionValues = newExemptionValues;
  }
}
