/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CreateDangerousGoodRegistrationDto } from './create-dangerous-good-registration.dto';
import { Company } from '@core/api-interfaces/lib/dtos/frontend';
import { FreightIdsIncluded } from '@core/api-interfaces/lib/dtos/frontend/transport-document/freight-ids-included';

export class SaveDangerousGoodRegistrationDto extends CreateDangerousGoodRegistrationDto {
  constructor(
    id: string,
    creator: string,
    consignor: Company,
    freight: FreightIdsIncluded,
    createdDate: number,
    lastUpdate: number
  ) {
    super(consignor, freight, creator);
    this.id = id;
    this.creator = creator;
    this.consignor = consignor;
    this.freight = freight;
    this.createdDate = createdDate;
    this.lastUpdate = lastUpdate;
  }

  id: string;
  creator?: string;
  consignor: Company;
  freight: FreightIdsIncluded;
  createdDate: number;
  lastUpdate?: number;
}
