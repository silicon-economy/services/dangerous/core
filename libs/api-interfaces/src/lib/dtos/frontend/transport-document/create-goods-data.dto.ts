/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company } from "./company";
import { Freight } from "./freight";
import { Carrier } from "./carrier";

export class CreateGoodsDataDto{
  consignor: Company;
  freight: Freight;
  carrier: Carrier;
  createdBy?: string;
  id?: string;

  constructor(consignor: Company, freight: Freight, carrier: Carrier, createdBy?: string, id?: string) {
    this.consignor = consignor;
    this.freight = freight;
    this.carrier = carrier;
    this.createdBy = createdBy;
    this.id = id;
  }
}
