/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Order } from './order';
import { AdditionalInformation } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/additional-information.enum';

export class Freight {
  orders: Order[];
  additionalInformation: AdditionalInformation | string;
  transportationInstructions: string;
  totalTransportPoints: number;

  constructor(
    orders: Order[],
    additionalInformation: AdditionalInformation | string,
    transportationInstructions: string,
    totalTransportPoints: number
  ) {
    this.orders = orders;
    this.additionalInformation = additionalInformation;
    this.transportationInstructions = transportationInstructions;
    this.totalTransportPoints = totalTransportPoints;
  }
}
