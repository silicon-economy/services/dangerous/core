/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DangerousGood } from "../../data-management/transport-document/transport-document/dangerous-good";
import { PackagingUnit } from "../../data-management/transport-document/transport-document/enums/packaging-unit.enum";

export class OrderPosition {
  dangerousGood: DangerousGood;
  package: string;
  packagingCode: string;
  quantity: number;
  unit: PackagingUnit;
  individualAmount: number;
  polluting: boolean;
  transportPoints: number;
  totalAmount: number;

  constructor()
  constructor(dangerousGood: DangerousGood, packageOrder: string, packagingCode: string, quantity: number, unit: PackagingUnit, individualAmount: number, polluting: boolean, transportPoints: number, totalAmount: number)
  constructor(dangerousGood?: DangerousGood, packageOrder?: string, packagingCode?: string, quantity?: number, unit?: PackagingUnit, individualAmount?: number, polluting?: boolean, transportPoints?: number, totalAmount?: number) {
    this.dangerousGood = dangerousGood;
    this.package = packageOrder;
    this.packagingCode = packagingCode;
    this.quantity = quantity;
    this.unit = unit;
    this.individualAmount = individualAmount;
    this.polluting = polluting;
    this.transportPoints = transportPoints;
    this.totalAmount = totalAmount;
  }
}
