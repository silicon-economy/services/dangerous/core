/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company } from './company';
import { LogEntry } from './log-entry';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import { OrderStatus } from "@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums";

export class OrderWithId {
  constructor(
    consignee: Company,
    orderPositions: OrderPositionWithId[],
    id: string,
    logEntries?: LogEntry[],
    status?: OrderStatus
  ) {
    this.consignee = consignee;
    this.orderPositions = orderPositions;
    this.id = id;
    this.logEntries = logEntries;
    this.status = status;
  }

  consignee: Company;
  orderPositions: OrderPositionWithId[];
  id: string;
  logEntries: LogEntry[];
  status: OrderStatus;
}
