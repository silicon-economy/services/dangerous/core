/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { PackagingUnit } from './enums/packaging-unit.enum';
import { DangerousGood } from './dangerous-good';
import { LogEntry } from './log-entry';

export class OrderPositionWithId {
  constructor(
    dangerousGood: DangerousGood,
    packageType: string,
    packagingCode: string,
    quantity: number,
    unit: PackagingUnit,
    individualAmount: number,
    polluting: boolean,
    transportPoints: number,
    totalAmount: number,
    id: number,
    deviceId?: string,
    logEntries?: LogEntry[],
    status?: string
  ) {
    this.dangerousGood = dangerousGood;
    this.package = packageType;
    this.packagingCode = packagingCode;
    this.quantity = quantity;
    this.unit = unit;
    this.individualAmount = individualAmount;
    this.polluting = polluting;
    this.transportPoints = transportPoints;
    this.totalAmount = totalAmount;
    this.id = id;
    this.deviceId = deviceId;
    this.logEntries = logEntries;
    this.status = status;
  }

  dangerousGood: DangerousGood;
  package: string;
  packagingCode: string;
  quantity: number;
  unit: PackagingUnit;
  individualAmount: number;
  polluting: boolean;
  transportPoints: number;
  totalAmount: number;
  id: number;
  deviceId?: string;
  logEntries: LogEntry[];
  status: string;
}
