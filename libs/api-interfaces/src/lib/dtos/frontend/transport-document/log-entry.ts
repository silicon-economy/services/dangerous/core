/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AcceptanceCriteria } from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/acceptance-criteria';
import {
  OrderPositionStatus,
  OrderStatus,
  TransportDocumentStatus,
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

export class LogEntry {
  constructor(
    status: TransportDocumentStatus | OrderStatus | OrderPositionStatus,
    date: number,
    author: string,
    description?: string,
    acceptanceCriteria?: AcceptanceCriteria
  ) {
    this.status = status;
    this.date = date;
    this.author = author;
    this.description = description;
    this.acceptanceCriteria = acceptanceCriteria;
  }

  status: TransportDocumentStatus | OrderStatus | OrderPositionStatus;
  date: number;
  author?: string;
  description?: string;
  acceptanceCriteria?: AcceptanceCriteria;
}
