/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { AdditionalInformation } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/additional-information.enum';

export class FreightIdsIncluded {
  constructor()
  constructor(
    orders: OrderWithId[],
    additionalInformation: AdditionalInformation | string,
    transportationInstructions: string,
    totalTransportPoints: number
  )
  constructor(
    orders?: OrderWithId[],
    additionalInformation?: AdditionalInformation | string,
    transportationInstructions?: string,
    totalTransportPoints?: number
  ){
    this.orders = orders;
    this.additionalInformation = additionalInformation;
    this.transportationInstructions = transportationInstructions;
    this.totalTransportPoints = totalTransportPoints;
  }

  orders: OrderWithId[];
  additionalInformation: AdditionalInformation | string;
  transportationInstructions: string;
  totalTransportPoints: number;
}
