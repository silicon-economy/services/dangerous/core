/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Address } from "./address";
import { ContactPerson } from "./contact-person";

export class Company {
  name: string;
  address: Address | undefined;
  contact: ContactPerson | undefined;

  constructor(name: string, address?: Address, contact?: ContactPerson) {
    this.name = name;
    this.address = address;
    this.contact = contact;
  }

  /**
   * This Method serves the purpose of avoiding undefined values at the initialization of new consignees
   *
   * @returns Company an empty company object
   */
  static createEmptyCompany(): Company {
    return new Company('', new Address('', '', '', '', ''), new ContactPerson('', '', '', ''));
  }
}
