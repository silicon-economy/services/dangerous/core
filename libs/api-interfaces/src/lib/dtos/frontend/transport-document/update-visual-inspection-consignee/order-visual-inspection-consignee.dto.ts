/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { OrderPositionVisualInspectionConsigneeDto } from './order-position-visual-inspection-consignee.dto';

export class OrderVisualInspectionConsigneeDto {
  constructor(orderId: string, orderPositions: OrderPositionVisualInspectionConsigneeDto[]) {
    this.orderId = orderId;
    this.orderPositions = orderPositions;
  }

  orderId: string;
  orderPositions: OrderPositionVisualInspectionConsigneeDto[];
}
