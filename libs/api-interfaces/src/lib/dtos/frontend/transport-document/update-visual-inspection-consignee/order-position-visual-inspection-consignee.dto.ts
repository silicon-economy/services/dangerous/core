/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class OrderPositionVisualInspectionConsigneeDto {
  constructor(orderPositionId: number, acceptQuantity: boolean, acceptIntactness: boolean, comment: string) {
    this.orderPositionId = orderPositionId;
    this.acceptQuantity = acceptQuantity;
    this.acceptIntactness = acceptIntactness;
    this.comment = comment;
  }

  orderPositionId: number;
  acceptQuantity: boolean;
  acceptIntactness: boolean;
  comment: string;
}
