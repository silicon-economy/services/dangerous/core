/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AcceptanceCriteria } from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/acceptance-criteria';

export class VisualInspectionConsigneeCriteria extends AcceptanceCriteria {
  acceptQuantity: boolean;
  acceptIntactness: boolean;

  constructor(acceptQuantity: boolean, acceptIntactness: boolean, comment?: string) {
    super(comment);
    this.acceptQuantity = acceptQuantity;
    this.acceptIntactness = acceptIntactness;
  }
}
