/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AcceptanceCriteria } from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/acceptance-criteria';

export class VisualInspectionCarrierCriteria extends AcceptanceCriteria {
  acceptTransportability: boolean;
  acceptLabeling: boolean;

  constructor(acceptTransportability: boolean, acceptLabeling: boolean, comment?: string) {
    super(comment);
    this.acceptTransportability = acceptTransportability;
    this.acceptLabeling = acceptLabeling;
  }
}
