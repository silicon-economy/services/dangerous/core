/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Model for saving what group positions were accepted by the carrier
 */
export class CarrierCheckCriteria {
  /**
   * Group position for consignor
   */
  acceptConsignor: boolean;

  /**
   * Group position for consignee
   */
  acceptConsignees: boolean[];

  /**
   * Group position for: load, quantity, packagingCode, totalAmount, totalTransportPoints
   */
  acceptFreight: boolean;

  /**
   * Group position for transportation instructions
   */
  acceptTransportationInstructions: boolean;

  /**
   * Group position for additional information
   */
  acceptAdditionalInformation: boolean;

  comment?: string;

  constructor(
    acceptConsignor: boolean,
    acceptConsignees: boolean[],
    acceptFreight: boolean,
    acceptTransportationInstructions: boolean,
    acceptAdditionalInformation: boolean,
    comment?: string
  ) {
    this.acceptConsignor = acceptConsignor;
    this.acceptConsignees = acceptConsignees;
    this.acceptFreight = acceptFreight;
    this.acceptTransportationInstructions = acceptTransportationInstructions;
    this.acceptAdditionalInformation = acceptAdditionalInformation;
    this.comment = comment;
  }
}
