/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CarrierCheckCriteria } from './carrier-check-criteria';
import { TransportVehicleCriteria } from './transport-vehicle-criteria';
import { VisualInspectionCarrierCriteria } from './visual-inspection-carrier-criteria';
import { VisualInspectionConsigneeCriteria } from './visual-inspection-consignee-criteria';

/**
 * Model for saving what group positions were accepted by the carrier
 */
export class AcceptanceCriteria {
  comment?: string;
  carrierCheckCriteria?: CarrierCheckCriteria;
  transportVehicleCriteria?: TransportVehicleCriteria;
  visualInspectionCarrierCriteria?: VisualInspectionCarrierCriteria;
  visualInspectionConsigneeCriteria?: VisualInspectionConsigneeCriteria;

  constructor(
    comment?: string,
    carrierCheckCriteria?: CarrierCheckCriteria,
    transportVehicleCriteria?: TransportVehicleCriteria,
    visualInspectionCarrierCriteria?: VisualInspectionCarrierCriteria,
    visualInspectionConsigneeCriteria?: VisualInspectionConsigneeCriteria
  ) {
    this.comment = comment;
    this.carrierCheckCriteria = carrierCheckCriteria;
    this.transportVehicleCriteria = transportVehicleCriteria;
    this.visualInspectionCarrierCriteria = visualInspectionCarrierCriteria;
    this.visualInspectionConsigneeCriteria = visualInspectionConsigneeCriteria;
  }
}
