/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class TransportVehicleCriteria {
  constructor(
    licencePlate: string,
    acceptVehicleCondition: boolean,
    acceptVehicleSafetyEquipment: boolean,
    driverName: string,
    acceptCarrierInformation: boolean,
    acceptCarrierSafetyEquipment: boolean,
    comment?: string
  ) {
    this.licencePlate = licencePlate;
    this.acceptVehicleCondition = acceptVehicleCondition;
    this.acceptVehicleSafetyEquipment = acceptVehicleSafetyEquipment;
    this.driverName = driverName;
    this.acceptCarrierInformation = acceptCarrierInformation;
    this.acceptCarrierSafetyEquipment = acceptCarrierSafetyEquipment;
    this.comment = comment;
  }

  licencePlate: string;
  acceptVehicleCondition: boolean;
  acceptVehicleSafetyEquipment: boolean;
  driverName: string;
  acceptCarrierInformation: boolean;
  acceptCarrierSafetyEquipment: boolean;
  comment?: string;
}
