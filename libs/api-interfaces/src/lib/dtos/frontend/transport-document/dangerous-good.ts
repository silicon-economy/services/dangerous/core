/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TransportLabel } from "../../data-management/transport-document/transport-document/enums/transport-label.enum";
import { PackagingGroup } from "../../data-management/transport-document/transport-document/enums/packaging-group.enum";
import {
  TunnelRestrictionCode
} from "../../data-management/transport-document/transport-document/enums/tunnel-restriction-code.enum";
import {
  TransportCategory
} from "../../data-management/transport-document/transport-document/enums/transport-category.enum";

export class DangerousGood {
  unNumber: string;
  casNumber: string;
  description: string;
  label1: TransportLabel;
  label2: TransportLabel;
  label3: TransportLabel;
  packingGroup: PackagingGroup;
  tunnelRestrictionCode: TunnelRestrictionCode;
  transportCategory: TransportCategory;

  constructor(unNumber: string, casNumber: string, description: string, label1: TransportLabel, label2: TransportLabel, label3: TransportLabel, packingGroup: PackagingGroup, tunnelRestrictionCode: TunnelRestrictionCode, transportCategory: TransportCategory) {
    this.unNumber = unNumber;
    this.casNumber = casNumber;
    this.description = description;
    this.label1 = label1;
    this.label2 = label2;
    this.label3 = label3;
    this.packingGroup = packingGroup;
    this.tunnelRestrictionCode = tunnelRestrictionCode;
    this.transportCategory = transportCategory;
  }
}
