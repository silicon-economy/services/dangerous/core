/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum TransportLabel {
  _1 = '1',
  _1_1 = '1.1',
  _1_2 = '1.2',
  _1_3 = '1.3',
  _1_4 = '1.4',
  _1_5 = '1.5',
  _1_6 = '1.6',
  _2_1 = '2.1',
  _2_2 = '2.2',
  _2_3 = '2.3',
  _3 = '3',
  _4_1 = '4.1',
  _4_2 = '4.2',
  _4_3 = '4.3',
  _5_1 = '5.1',
  _5_2 = '5.2',
  _6_1 = '6.1',
  _6_2 = '6.2',
  _7 = '7',
  _8 = '8',
  _9 = '9',
  _9_A = '9A',
  _ = '',
}
