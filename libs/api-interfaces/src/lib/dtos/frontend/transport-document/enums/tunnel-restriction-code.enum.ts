/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum TunnelRestrictionCode {
  '(B)',
  '(B1000C)',
  '(B/D)',
  '(B/E)',
  '(C)',
  '(C5000D)',
  '(C/D)',
  '(C/E)',
  '(D)',
  '(D/E)',
  '(E)',
  '(-)',
}
