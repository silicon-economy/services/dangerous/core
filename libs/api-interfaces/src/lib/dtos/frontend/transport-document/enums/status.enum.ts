/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum TransportDocumentStatus {
  created = 'created',
  accepted = 'accepted',
  not_accepted = 'not_accepted',
  released = 'released',
  vehicle_accepted = 'vehicle_accepted',
  vehicle_denied = 'vehicle_denied',
  transport = 'transport',
  transport_denied = 'transport_denied',
  transport_completed = 'transport_completed',
  unknown = 'unknown',
}

export enum OrderStatus {
  carrier_confirmed = 'carrier_confirmed',
  visual_inspection_consignee_accepted = 'visual_inspection_consignee_accepted',
  visual_inspection_consignee_denied = 'visual_inspection_consignee_denied',
}

export enum OrderPositionStatus {
  accepted_by_carrier = 'accepted_by_carrier',
  visual_inspection_carrier_accepted = 'visual_inspection_carrier_accepted',
  visual_inspection_carrier_denied = 'visual_inspection_carrier_denied',
  visual_inspection_consignee_accepted = 'visual_inspection_consignee_accepted',
  visual_inspection_consignee_denied = 'visual_inspection_consignee_denied',
}
