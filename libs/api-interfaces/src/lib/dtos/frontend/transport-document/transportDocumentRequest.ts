/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Carrier, Company, Freight } from '@core/api-interfaces/lib/dtos/frontend';

export class TransportDocumentRequest {
  id?: string;
  sender: Company;
  consignee?: Company;
  carrier: Carrier;
  freight?: Freight;
  status?: string;
  createdDate?: Date;
  carrierCheckDate?: Date;
  releasedDate?: Date;
  createdBy?: string;
  carrierCheckBy?: string;
  releasedBy?: string;

  constructor(
    sender: Company,
    carrier: Carrier,
    consignee: Company,
    freight?: Freight,
    status?: string,
    createdDate?: Date,
    carrierCheckDate?: Date,
    releasedDate?: Date,
    createdBy?: string,
    carrierCheckBy?: string,
    releasedBy?: string,
    id?: string
  ) {
    this.id = id;
    this.sender = sender;
    this.consignee = consignee;
    this.carrier = carrier;
    this.freight = freight;
    this.status = status;
    this.createdDate = createdDate;
    this.carrierCheckDate = carrierCheckDate;
    this.releasedDate = releasedDate;
    this.createdBy = createdBy;
    this.carrierCheckBy = carrierCheckBy;
    this.releasedBy = releasedBy;
  }
}
