/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company } from './company';
import { Carrier } from "@core/api-interfaces/lib/dtos/frontend";
import { FreightIdsIncluded } from "@core/api-interfaces/lib/dtos/frontend/transport-document/freight-ids-included";
import { LogEntry } from "@core/api-interfaces/lib/dtos/frontend/transport-document/log-entry";
import { TransportDocumentStatus } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/status.enum';

/**
 * Some attributes are marked as optional due to the status of development!
 */
export class SaveGoodsDataDto {

  constructor()
  constructor(
    id: string,
    consignor: Company,
    carrier: Carrier,
    freight: FreightIdsIncluded,
    logEntries: LogEntry[],
    status: TransportDocumentStatus,
    createdDate: number,
    lastUpdate: number
  )
  constructor(
    id?: string,
    consignor?: Company,
    carrier?: Carrier,
    freight?: FreightIdsIncluded,
    logEntries?: LogEntry[],
    status?: TransportDocumentStatus,
    createdDate?: number,
    lastUpdate?: number
  ) {
    this.id = id;
    this.consignor = consignor;
    this.carrier = carrier;
    this.freight = freight;
    this.logEntries = logEntries;
    this.status = status;
    this.createdDate = createdDate;
    this.lastUpdate = lastUpdate;
  }

  id: string;

  consignor: Company;

  carrier: Carrier;

  freight: FreightIdsIncluded;

  logEntries: LogEntry[];

  status: TransportDocumentStatus;

  createdDate: number;

  lastUpdate?: number;
}
