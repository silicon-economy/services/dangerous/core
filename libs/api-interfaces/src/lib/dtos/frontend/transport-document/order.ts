/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company } from "./company";
import { OrderPosition } from "./order-position";

export class Order {
  consignee: Company;
  orderPositions: OrderPosition[];

  constructor(consignee: Company, orderPositions: OrderPosition[]) {
    this.consignee = consignee;
    this.orderPositions = orderPositions;
  }
}
