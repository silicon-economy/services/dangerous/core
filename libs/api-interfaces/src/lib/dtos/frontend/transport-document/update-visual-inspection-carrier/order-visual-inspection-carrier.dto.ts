/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { OrderPositionVisualInspectionCarrierDto } from './order-position-visual-inspection-carrier.dto';

export class OrderVisualInspectionCarrierDto {
  constructor(orderId: string, orderPositions: OrderPositionVisualInspectionCarrierDto[]) {
    this.orderId = orderId;
    this.orderPositions = orderPositions;
  }

  orderId: string;
  orderPositions: OrderPositionVisualInspectionCarrierDto[];
}
