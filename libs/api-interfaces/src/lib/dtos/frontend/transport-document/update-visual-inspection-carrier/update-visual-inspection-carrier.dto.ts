/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { OrderVisualInspectionCarrierDto } from './order-visual-inspection-carrier.dto';

export class UpdateVisualInspectionCarrierDto {
  constructor(transportDocumentId: string, orders: OrderVisualInspectionCarrierDto[]) {
    this.transportDocumentId = transportDocumentId;
    this.orders = orders;
  }

  transportDocumentId: string;
  orders: OrderVisualInspectionCarrierDto[];
}
