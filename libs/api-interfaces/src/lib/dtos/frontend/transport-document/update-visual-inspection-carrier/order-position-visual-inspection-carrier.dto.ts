/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class OrderPositionVisualInspectionCarrierDto {
  constructor(orderPositionId: number, acceptTransportability: boolean, acceptLabeling: boolean, comment: string) {
    this.orderPositionId = orderPositionId;
    this.acceptTransportability = acceptTransportability;
    this.acceptLabeling = acceptLabeling;
    this.comment = comment;
  }

  orderPositionId: number;
  acceptTransportability: boolean;
  acceptLabeling: boolean;
  comment: string;
}
