/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { PackagingMaterialType } from './packagingTypes/packaging-material-type.type';
import { PackagingKind } from './packagingTypes/packaging-kind.type';
import { PackagingMaterial } from './packagingTypes/packaging-material.type';

export class PackagingFilter {
  constructor(
    query: string,
    typesOfMaterial?: PackagingMaterialType,
    kind?: PackagingKind,
    category?: string,
    material?: PackagingMaterial
  ) {
    this.query = query;
    this.typesOfMaterial = typesOfMaterial;
    this.kind = kind;
    this.category = category;
    this.material = material;
  }

  query: string;
  typesOfMaterial?: PackagingMaterialType;
  kind?: PackagingKind;
  category?: string;
  material?: PackagingMaterial;
}
