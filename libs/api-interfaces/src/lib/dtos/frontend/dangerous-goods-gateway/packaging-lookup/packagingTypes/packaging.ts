/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class Packaging {
  kind: string;
  typesOfMaterial: string;
  material: string;
  category: string;
  code: string;
}
