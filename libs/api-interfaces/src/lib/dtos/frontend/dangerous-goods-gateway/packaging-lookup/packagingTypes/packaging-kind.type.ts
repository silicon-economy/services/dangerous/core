/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export const PACKAGING_KINDS: readonly string[] = [
  'Fässer',
  'Holzfässer',
  'Kanister',
  'Kisten',
  'Säcke',
  'Kombinationsverpackungen',
  'Feinstblechverpackungen',
  'Großpackmittel (IBC)',
] as const;

export type PackagingKind = (typeof PACKAGING_KINDS)[number];
