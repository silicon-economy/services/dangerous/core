/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class DangerousGoodLookupDto {
  constructor(
    unNumber: string,
    casNumber: string,
    description: string,
    label1: string,
    label2: string,
    label3: string,
    packingGroup: string,
    tunnelRestrictionCode: string,
    transportCategory: string,
    polluting: boolean,
    limitedQuantity: number
  ) {
    this.unNumber = unNumber;
    this.casNumber = casNumber;
    this.description = description;
    this.label1 = label1;
    this.label2 = label2;
    this.label3 = label3;
    this.packingGroup = packingGroup;
    this.tunnelRestrictionCode = tunnelRestrictionCode;
    this.transportCategory = transportCategory;
    this.polluting = polluting;
    this.limitedQuantity = limitedQuantity;
  }

  unNumber: string;
  casNumber: string;
  description: string;
  label1: string;
  label2: string;
  label3: string;
  packingGroup: string;
  tunnelRestrictionCode: string;
  transportCategory: string;
  polluting: boolean;
  limitedQuantity: number;
}
