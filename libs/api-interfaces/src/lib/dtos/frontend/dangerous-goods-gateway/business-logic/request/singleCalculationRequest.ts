/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class SingleCalculationRequest {
  inputString: string;
  unit: string;
  quantity: number;
  individualAmount: number;

  constructor(inputString: string, unit: string, quantity: number, individualAmount: number) {
    this.inputString = inputString;
    this.unit = unit;
    this.quantity = quantity;
    this.individualAmount = individualAmount;
  }
}
