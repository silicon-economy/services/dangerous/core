/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TransportCategoryPointsResponseWrapper } from './transportCategoryPointsResponseWrapper';

export class TransportCategoryPoints {
  consignees: TransportCategoryPointsResponseWrapper[];
  totalTransportCategoryPoints: number;
  exemptionStatus: string;

  constructor(
    consignees: TransportCategoryPointsResponseWrapper[],
    totalTransportCategoryPoints: number,
    exemptionStatus: string
  ) {
    this.consignees = consignees;
    this.totalTransportCategoryPoints = totalTransportCategoryPoints;
    this.exemptionStatus = exemptionStatus;
  }
}
