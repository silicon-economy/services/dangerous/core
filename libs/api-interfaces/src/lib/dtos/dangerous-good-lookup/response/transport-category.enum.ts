/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum TransportCategory {
  'transportCategory_1' = 50,
  'transportCategory_2' = 3,
  'transportCategory_3' = 1,
  'transportCategory_4' = 0,
}

export const transportCategoryMap = new Map<string, string>([
  ['1', 'transportCategory_1'],
  ['2', 'transportCategory_2'],
  ['3', 'transportCategory_3'],
  ['4', 'transportCategory_4'],
]);
