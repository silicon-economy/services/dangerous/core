/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ApiProperty } from '@nestjs/swagger';

export class DangerousGoodLookupResponseDto {
  @ApiProperty()
  unNumber: string;

  @ApiProperty()
  casNumber: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  label1: string;

  @ApiProperty()
  label2: string;

  @ApiProperty()
  label3: string;

  @ApiProperty()
  packingGroup: string;

  @ApiProperty()
  tunnelRestrictionCode: string;

  @ApiProperty()
  transportCategory: string;

  @ApiProperty()
  polluting: boolean;

  @ApiProperty()
  limitedQuantity: number;
}
