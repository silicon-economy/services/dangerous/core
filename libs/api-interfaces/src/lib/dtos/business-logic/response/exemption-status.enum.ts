/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum ExemptionStatus {
  transportWithoutExemption = 'Beförderung ohne Freistellung nach ADR 1.1.3.6',
  transportWith1kp = 'Beförderung nach ADR 1.1.3.6',
  transportWithLq = 'Beförderung von Limited Quantities (ADR 3.4)',
  transportWith1kpAndLq = 'Beförderung nach ADR 1.1.3.6 und ADR 3.4 (Limited Quantities)',
}
