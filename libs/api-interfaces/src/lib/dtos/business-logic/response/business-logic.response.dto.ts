/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { IsEnum } from 'class-validator';
import { ExemptionStatus } from './exemption-status.enum';

export class CalculateExemptionResponseDto {
  constructor(consignees: ConsigneeResponse[], totalTransportCategoryPoints: number, exemptionStatus: string) {
    this.consignees = consignees;
    this.totalTransportCategoryPoints = totalTransportCategoryPoints;
    this.exemptionStatus = exemptionStatus;
  }

  consignees: ConsigneeResponse[];
  totalTransportCategoryPoints: number;

  @IsEnum(ExemptionStatus)
  exemptionStatus: string;
}

export class ConsigneeResponse {
  constructor(responses: SingleThousandPointValue[]) {
    this.responses = responses;
  }

  responses: SingleThousandPointValue[];
}

export class SingleThousandPointValue {
  constructor(unNumber: string, totalAmount: number, transportCategoryPoints: number, exemptionStatus: string) {
    this.unNumber = unNumber;
    this.totalAmount = totalAmount;
    this.transportCategoryPoints = transportCategoryPoints;
    this.exemptionStatus = exemptionStatus;
  }

  unNumber: string;
  totalAmount: number;
  transportCategoryPoints: number;

  @IsEnum(ExemptionStatus)
  exemptionStatus: string;
}
