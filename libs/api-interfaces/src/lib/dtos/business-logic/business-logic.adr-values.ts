/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class AdrValues {
  constructor(adr34: boolean, adr1136: boolean, withoutExemption?: boolean) {
    this.adr34 = adr34;
    this.adr1136 = adr1136;
    this.withoutExemption = withoutExemption;
  }

  adr34: boolean;
  adr1136: boolean;
  withoutExemption: boolean;
}
