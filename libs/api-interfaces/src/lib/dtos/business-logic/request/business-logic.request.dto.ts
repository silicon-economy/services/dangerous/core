/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ApiProperty } from '@nestjs/swagger';
import { PackagingUnit } from '../../data-management/transport-document/transport-document/enums/packaging-unit.enum';

export class CalculateExemptionRequestDto {
  @ApiProperty({
    description: 'The consignees of the hazardous goods.',
    default: [{ requests: [{ inputString: '1090', unit: 'L', quantity: 42, individualAmount: 10 }] }],
    required: true,
  })
  consignees: ConsigneeRequest[];
}

export class ConsigneeRequest {
  @ApiProperty({
    description: 'The hazardous goods which are going to be delivered to the consignees.',
    default: [{ inputString: '1090', unit: 'L', quantity: 42, individualAmount: 10 }],
    required: true,
  })
  requests: SingleRequest[];
}
export class SingleRequest {
  @ApiProperty({ description: 'The UN-Number of the hazardous good.', default: '1090', required: true })
  inputString: string;

  @ApiProperty({ description: 'The unit in which the quantity is given.', default: 'L', required: true })
  unit: PackagingUnit;

  @ApiProperty({ description: 'The quantity being delivered.', default: 42, required: true })
  quantity: number;

  @ApiProperty({ description: 'The individual amount being delivered.', default: 10, required: true })
  individualAmount: number;
}
