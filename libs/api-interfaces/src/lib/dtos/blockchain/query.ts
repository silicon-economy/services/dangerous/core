/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable */
import { Reader, util, configure, Writer } from 'protobufjs/minimal';
import * as Long from 'long';
import { TransportDocument } from '../blockchain/transport_document';
import { PageRequest, PageResponse } from '../cosmos/base/query/v1beta1/pagination';
import { MsgDeviceData, DeviceJobData } from '../blockchain/device_job_data';
import { DangerousGoodRegistration } from '../blockchain/dangerous_good_registration';
import { PastEvent } from '../blockchain/past_event';

export const protobufPackage =
  'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic';

export interface QueryGetTransportDocumentRequest {
  id: string;
}

export interface QueryGetTransportDocumentResponse {
  transportDocument: TransportDocument | undefined;
}

export interface QueryAllTransportDocumentRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllTransportDocumentResponse {
  transportDocument: TransportDocument[];
  pagination: PageResponse | undefined;
}

export interface QueryGetDeviceJobDataRequest {
  id: number;
}

export interface QueryGetDeviceJobDataResponse {
  deviceData: MsgDeviceData[];
}

export interface QueryAllDeviceJobDataRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllDeviceJobDataResponse {
  deviceJobData: DeviceJobData[];
  pagination: PageResponse | undefined;
}

export interface QueryGetTransportDocumentIdByOrderPositionIdRequest {
  orderPositionId: string;
}

export interface QueryGetTransportDocumentIdByOrderPositionIdResponse {
  transportDocumentId: string;
}

export interface QueryGetDangerousGoodRegistrationRequest {
  id: string;
}

export interface QueryGetDangerousGoodRegistrationResponse {
  dangerousGoodRegistration: DangerousGoodRegistration | undefined;
}

export interface QueryAllDangerousGoodRegistrationRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllDangerousGoodRegistrationResponse {
  dangerousGoodRegistration: DangerousGoodRegistration[];
  pagination: PageResponse | undefined;
}

export interface QueryGetPastEventRequest {
  id: number;
}

export interface QueryGetPastEventResponse {
  PastEvent: PastEvent | undefined;
}

export interface QueryAllPastEventRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllPastEventResponse {
  PastEvent: PastEvent[];
  pagination: PageResponse | undefined;
}

const baseQueryGetTransportDocumentRequest: object = { id: '' };

export const QueryGetTransportDocumentRequest = {
  encode(message: QueryGetTransportDocumentRequest, writer: Writer = Writer.create()): Writer {
    if (message.id !== '') {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetTransportDocumentRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetTransportDocumentRequest } as QueryGetTransportDocumentRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTransportDocumentRequest {
    const message = { ...baseQueryGetTransportDocumentRequest } as QueryGetTransportDocumentRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = '';
    }
    return message;
  },

  toJSON(message: QueryGetTransportDocumentRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryGetTransportDocumentRequest>): QueryGetTransportDocumentRequest {
    const message = { ...baseQueryGetTransportDocumentRequest } as QueryGetTransportDocumentRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = '';
    }
    return message;
  },
};

const baseQueryGetTransportDocumentResponse: object = {};

export const QueryGetTransportDocumentResponse = {
  encode(message: QueryGetTransportDocumentResponse, writer: Writer = Writer.create()): Writer {
    if (message.transportDocument !== undefined) {
      TransportDocument.encode(message.transportDocument, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetTransportDocumentResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetTransportDocumentResponse } as QueryGetTransportDocumentResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.transportDocument = TransportDocument.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTransportDocumentResponse {
    const message = { ...baseQueryGetTransportDocumentResponse } as QueryGetTransportDocumentResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromJSON(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },

  toJSON(message: QueryGetTransportDocumentResponse): unknown {
    const obj: any = {};
    message.transportDocument !== undefined &&
      (obj.transportDocument = message.transportDocument
        ? TransportDocument.toJSON(message.transportDocument)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryGetTransportDocumentResponse>): QueryGetTransportDocumentResponse {
    const message = { ...baseQueryGetTransportDocumentResponse } as QueryGetTransportDocumentResponse;
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      message.transportDocument = TransportDocument.fromPartial(object.transportDocument);
    } else {
      message.transportDocument = undefined;
    }
    return message;
  },
};

const baseQueryAllTransportDocumentRequest: object = {};

export const QueryAllTransportDocumentRequest = {
  encode(message: QueryAllTransportDocumentRequest, writer: Writer = Writer.create()): Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllTransportDocumentRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllTransportDocumentRequest } as QueryAllTransportDocumentRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTransportDocumentRequest {
    const message = { ...baseQueryAllTransportDocumentRequest } as QueryAllTransportDocumentRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllTransportDocumentRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined &&
      (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryAllTransportDocumentRequest>): QueryAllTransportDocumentRequest {
    const message = { ...baseQueryAllTransportDocumentRequest } as QueryAllTransportDocumentRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

const baseQueryAllTransportDocumentResponse: object = {};

export const QueryAllTransportDocumentResponse = {
  encode(message: QueryAllTransportDocumentResponse, writer: Writer = Writer.create()): Writer {
    for (const v of message.transportDocument) {
      TransportDocument.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllTransportDocumentResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllTransportDocumentResponse } as QueryAllTransportDocumentResponse;
    message.transportDocument = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.transportDocument.push(TransportDocument.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTransportDocumentResponse {
    const message = { ...baseQueryAllTransportDocumentResponse } as QueryAllTransportDocumentResponse;
    message.transportDocument = [];
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      for (const e of object.transportDocument) {
        message.transportDocument.push(TransportDocument.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllTransportDocumentResponse): unknown {
    const obj: any = {};
    if (message.transportDocument) {
      obj.transportDocument = message.transportDocument.map((e) => (e ? TransportDocument.toJSON(e) : undefined));
    } else {
      obj.transportDocument = [];
    }
    message.pagination !== undefined &&
      (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryAllTransportDocumentResponse>): QueryAllTransportDocumentResponse {
    const message = { ...baseQueryAllTransportDocumentResponse } as QueryAllTransportDocumentResponse;
    message.transportDocument = [];
    if (object.transportDocument !== undefined && object.transportDocument !== null) {
      for (const e of object.transportDocument) {
        message.transportDocument.push(TransportDocument.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

const baseQueryGetDeviceJobDataRequest: object = { id: 0 };

export const QueryGetDeviceJobDataRequest = {
  encode(message: QueryGetDeviceJobDataRequest, writer: Writer = Writer.create()): Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetDeviceJobDataRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetDeviceJobDataRequest } as QueryGetDeviceJobDataRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetDeviceJobDataRequest {
    const message = { ...baseQueryGetDeviceJobDataRequest } as QueryGetDeviceJobDataRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: QueryGetDeviceJobDataRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryGetDeviceJobDataRequest>): QueryGetDeviceJobDataRequest {
    const message = { ...baseQueryGetDeviceJobDataRequest } as QueryGetDeviceJobDataRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseQueryGetDeviceJobDataResponse: object = {};

export const QueryGetDeviceJobDataResponse = {
  encode(message: QueryGetDeviceJobDataResponse, writer: Writer = Writer.create()): Writer {
    for (const v of message.deviceData) {
      MsgDeviceData.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetDeviceJobDataResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetDeviceJobDataResponse } as QueryGetDeviceJobDataResponse;
    message.deviceData = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.deviceData.push(MsgDeviceData.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetDeviceJobDataResponse {
    const message = { ...baseQueryGetDeviceJobDataResponse } as QueryGetDeviceJobDataResponse;
    message.deviceData = [];
    if (object.deviceData !== undefined && object.deviceData !== null) {
      for (const e of object.deviceData) {
        message.deviceData.push(MsgDeviceData.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: QueryGetDeviceJobDataResponse): unknown {
    const obj: any = {};
    if (message.deviceData) {
      obj.deviceData = message.deviceData.map((e) => (e ? MsgDeviceData.toJSON(e) : undefined));
    } else {
      obj.deviceData = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<QueryGetDeviceJobDataResponse>): QueryGetDeviceJobDataResponse {
    const message = { ...baseQueryGetDeviceJobDataResponse } as QueryGetDeviceJobDataResponse;
    message.deviceData = [];
    if (object.deviceData !== undefined && object.deviceData !== null) {
      for (const e of object.deviceData) {
        message.deviceData.push(MsgDeviceData.fromPartial(e));
      }
    }
    return message;
  },
};

const baseQueryAllDeviceJobDataRequest: object = {};

export const QueryAllDeviceJobDataRequest = {
  encode(message: QueryAllDeviceJobDataRequest, writer: Writer = Writer.create()): Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllDeviceJobDataRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllDeviceJobDataRequest } as QueryAllDeviceJobDataRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllDeviceJobDataRequest {
    const message = { ...baseQueryAllDeviceJobDataRequest } as QueryAllDeviceJobDataRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllDeviceJobDataRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined &&
      (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryAllDeviceJobDataRequest>): QueryAllDeviceJobDataRequest {
    const message = { ...baseQueryAllDeviceJobDataRequest } as QueryAllDeviceJobDataRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

const baseQueryAllDeviceJobDataResponse: object = {};

export const QueryAllDeviceJobDataResponse = {
  encode(message: QueryAllDeviceJobDataResponse, writer: Writer = Writer.create()): Writer {
    for (const v of message.deviceJobData) {
      DeviceJobData.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllDeviceJobDataResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllDeviceJobDataResponse } as QueryAllDeviceJobDataResponse;
    message.deviceJobData = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.deviceJobData.push(DeviceJobData.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllDeviceJobDataResponse {
    const message = { ...baseQueryAllDeviceJobDataResponse } as QueryAllDeviceJobDataResponse;
    message.deviceJobData = [];
    if (object.deviceJobData !== undefined && object.deviceJobData !== null) {
      for (const e of object.deviceJobData) {
        message.deviceJobData.push(DeviceJobData.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllDeviceJobDataResponse): unknown {
    const obj: any = {};
    if (message.deviceJobData) {
      obj.deviceJobData = message.deviceJobData.map((e) => (e ? DeviceJobData.toJSON(e) : undefined));
    } else {
      obj.deviceJobData = [];
    }
    message.pagination !== undefined &&
      (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryAllDeviceJobDataResponse>): QueryAllDeviceJobDataResponse {
    const message = { ...baseQueryAllDeviceJobDataResponse } as QueryAllDeviceJobDataResponse;
    message.deviceJobData = [];
    if (object.deviceJobData !== undefined && object.deviceJobData !== null) {
      for (const e of object.deviceJobData) {
        message.deviceJobData.push(DeviceJobData.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

const baseQueryGetTransportDocumentIdByOrderPositionIdRequest: object = { orderPositionId: '' };

export const QueryGetTransportDocumentIdByOrderPositionIdRequest = {
  encode(message: QueryGetTransportDocumentIdByOrderPositionIdRequest, writer: Writer = Writer.create()): Writer {
    if (message.orderPositionId !== '') {
      writer.uint32(10).string(message.orderPositionId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetTransportDocumentIdByOrderPositionIdRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryGetTransportDocumentIdByOrderPositionIdRequest,
    } as QueryGetTransportDocumentIdByOrderPositionIdRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.orderPositionId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTransportDocumentIdByOrderPositionIdRequest {
    const message = {
      ...baseQueryGetTransportDocumentIdByOrderPositionIdRequest,
    } as QueryGetTransportDocumentIdByOrderPositionIdRequest;
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = String(object.orderPositionId);
    } else {
      message.orderPositionId = '';
    }
    return message;
  },

  toJSON(message: QueryGetTransportDocumentIdByOrderPositionIdRequest): unknown {
    const obj: any = {};
    message.orderPositionId !== undefined && (obj.orderPositionId = message.orderPositionId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetTransportDocumentIdByOrderPositionIdRequest>
  ): QueryGetTransportDocumentIdByOrderPositionIdRequest {
    const message = {
      ...baseQueryGetTransportDocumentIdByOrderPositionIdRequest,
    } as QueryGetTransportDocumentIdByOrderPositionIdRequest;
    if (object.orderPositionId !== undefined && object.orderPositionId !== null) {
      message.orderPositionId = object.orderPositionId;
    } else {
      message.orderPositionId = '';
    }
    return message;
  },
};

const baseQueryGetTransportDocumentIdByOrderPositionIdResponse: object = { transportDocumentId: '' };

export const QueryGetTransportDocumentIdByOrderPositionIdResponse = {
  encode(message: QueryGetTransportDocumentIdByOrderPositionIdResponse, writer: Writer = Writer.create()): Writer {
    if (message.transportDocumentId !== '') {
      writer.uint32(10).string(message.transportDocumentId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetTransportDocumentIdByOrderPositionIdResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQueryGetTransportDocumentIdByOrderPositionIdResponse,
    } as QueryGetTransportDocumentIdByOrderPositionIdResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.transportDocumentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTransportDocumentIdByOrderPositionIdResponse {
    const message = {
      ...baseQueryGetTransportDocumentIdByOrderPositionIdResponse,
    } as QueryGetTransportDocumentIdByOrderPositionIdResponse;
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = String(object.transportDocumentId);
    } else {
      message.transportDocumentId = '';
    }
    return message;
  },

  toJSON(message: QueryGetTransportDocumentIdByOrderPositionIdResponse): unknown {
    const obj: any = {};
    message.transportDocumentId !== undefined && (obj.transportDocumentId = message.transportDocumentId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetTransportDocumentIdByOrderPositionIdResponse>
  ): QueryGetTransportDocumentIdByOrderPositionIdResponse {
    const message = {
      ...baseQueryGetTransportDocumentIdByOrderPositionIdResponse,
    } as QueryGetTransportDocumentIdByOrderPositionIdResponse;
    if (object.transportDocumentId !== undefined && object.transportDocumentId !== null) {
      message.transportDocumentId = object.transportDocumentId;
    } else {
      message.transportDocumentId = '';
    }
    return message;
  },
};

const baseQueryGetDangerousGoodRegistrationRequest: object = { id: '' };

export const QueryGetDangerousGoodRegistrationRequest = {
  encode(message: QueryGetDangerousGoodRegistrationRequest, writer: Writer = Writer.create()): Writer {
    if (message.id !== '') {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetDangerousGoodRegistrationRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetDangerousGoodRegistrationRequest } as QueryGetDangerousGoodRegistrationRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetDangerousGoodRegistrationRequest {
    const message = { ...baseQueryGetDangerousGoodRegistrationRequest } as QueryGetDangerousGoodRegistrationRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id);
    } else {
      message.id = '';
    }
    return message;
  },

  toJSON(message: QueryGetDangerousGoodRegistrationRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryGetDangerousGoodRegistrationRequest>): QueryGetDangerousGoodRegistrationRequest {
    const message = { ...baseQueryGetDangerousGoodRegistrationRequest } as QueryGetDangerousGoodRegistrationRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = '';
    }
    return message;
  },
};

const baseQueryGetDangerousGoodRegistrationResponse: object = {};

export const QueryGetDangerousGoodRegistrationResponse = {
  encode(message: QueryGetDangerousGoodRegistrationResponse, writer: Writer = Writer.create()): Writer {
    if (message.dangerousGoodRegistration !== undefined) {
      DangerousGoodRegistration.encode(message.dangerousGoodRegistration, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetDangerousGoodRegistrationResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetDangerousGoodRegistrationResponse } as QueryGetDangerousGoodRegistrationResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.dangerousGoodRegistration = DangerousGoodRegistration.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetDangerousGoodRegistrationResponse {
    const message = { ...baseQueryGetDangerousGoodRegistrationResponse } as QueryGetDangerousGoodRegistrationResponse;
    if (object.dangerousGoodRegistration !== undefined && object.dangerousGoodRegistration !== null) {
      message.dangerousGoodRegistration = DangerousGoodRegistration.fromJSON(object.dangerousGoodRegistration);
    } else {
      message.dangerousGoodRegistration = undefined;
    }
    return message;
  },

  toJSON(message: QueryGetDangerousGoodRegistrationResponse): unknown {
    const obj: any = {};
    message.dangerousGoodRegistration !== undefined &&
      (obj.dangerousGoodRegistration = message.dangerousGoodRegistration
        ? DangerousGoodRegistration.toJSON(message.dangerousGoodRegistration)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryGetDangerousGoodRegistrationResponse>
  ): QueryGetDangerousGoodRegistrationResponse {
    const message = { ...baseQueryGetDangerousGoodRegistrationResponse } as QueryGetDangerousGoodRegistrationResponse;
    if (object.dangerousGoodRegistration !== undefined && object.dangerousGoodRegistration !== null) {
      message.dangerousGoodRegistration = DangerousGoodRegistration.fromPartial(object.dangerousGoodRegistration);
    } else {
      message.dangerousGoodRegistration = undefined;
    }
    return message;
  },
};

const baseQueryAllDangerousGoodRegistrationRequest: object = {};

export const QueryAllDangerousGoodRegistrationRequest = {
  encode(message: QueryAllDangerousGoodRegistrationRequest, writer: Writer = Writer.create()): Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllDangerousGoodRegistrationRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllDangerousGoodRegistrationRequest } as QueryAllDangerousGoodRegistrationRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllDangerousGoodRegistrationRequest {
    const message = { ...baseQueryAllDangerousGoodRegistrationRequest } as QueryAllDangerousGoodRegistrationRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllDangerousGoodRegistrationRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined &&
      (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryAllDangerousGoodRegistrationRequest>): QueryAllDangerousGoodRegistrationRequest {
    const message = { ...baseQueryAllDangerousGoodRegistrationRequest } as QueryAllDangerousGoodRegistrationRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

const baseQueryAllDangerousGoodRegistrationResponse: object = {};

export const QueryAllDangerousGoodRegistrationResponse = {
  encode(message: QueryAllDangerousGoodRegistrationResponse, writer: Writer = Writer.create()): Writer {
    for (const v of message.dangerousGoodRegistration) {
      DangerousGoodRegistration.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllDangerousGoodRegistrationResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllDangerousGoodRegistrationResponse } as QueryAllDangerousGoodRegistrationResponse;
    message.dangerousGoodRegistration = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.dangerousGoodRegistration.push(DangerousGoodRegistration.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllDangerousGoodRegistrationResponse {
    const message = { ...baseQueryAllDangerousGoodRegistrationResponse } as QueryAllDangerousGoodRegistrationResponse;
    message.dangerousGoodRegistration = [];
    if (object.dangerousGoodRegistration !== undefined && object.dangerousGoodRegistration !== null) {
      for (const e of object.dangerousGoodRegistration) {
        message.dangerousGoodRegistration.push(DangerousGoodRegistration.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllDangerousGoodRegistrationResponse): unknown {
    const obj: any = {};
    if (message.dangerousGoodRegistration) {
      obj.dangerousGoodRegistration = message.dangerousGoodRegistration.map((e) =>
        e ? DangerousGoodRegistration.toJSON(e) : undefined
      );
    } else {
      obj.dangerousGoodRegistration = [];
    }
    message.pagination !== undefined &&
      (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QueryAllDangerousGoodRegistrationResponse>
  ): QueryAllDangerousGoodRegistrationResponse {
    const message = { ...baseQueryAllDangerousGoodRegistrationResponse } as QueryAllDangerousGoodRegistrationResponse;
    message.dangerousGoodRegistration = [];
    if (object.dangerousGoodRegistration !== undefined && object.dangerousGoodRegistration !== null) {
      for (const e of object.dangerousGoodRegistration) {
        message.dangerousGoodRegistration.push(DangerousGoodRegistration.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

const baseQueryGetPastEventRequest: object = { id: 0 };

export const QueryGetPastEventRequest = {
  encode(message: QueryGetPastEventRequest, writer: Writer = Writer.create()): Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetPastEventRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetPastEventRequest } as QueryGetPastEventRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetPastEventRequest {
    const message = { ...baseQueryGetPastEventRequest } as QueryGetPastEventRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: QueryGetPastEventRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryGetPastEventRequest>): QueryGetPastEventRequest {
    const message = { ...baseQueryGetPastEventRequest } as QueryGetPastEventRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseQueryGetPastEventResponse: object = {};

export const QueryGetPastEventResponse = {
  encode(message: QueryGetPastEventResponse, writer: Writer = Writer.create()): Writer {
    if (message.PastEvent !== undefined) {
      PastEvent.encode(message.PastEvent, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryGetPastEventResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryGetPastEventResponse } as QueryGetPastEventResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.PastEvent = PastEvent.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetPastEventResponse {
    const message = { ...baseQueryGetPastEventResponse } as QueryGetPastEventResponse;
    if (object.PastEvent !== undefined && object.PastEvent !== null) {
      message.PastEvent = PastEvent.fromJSON(object.PastEvent);
    } else {
      message.PastEvent = undefined;
    }
    return message;
  },

  toJSON(message: QueryGetPastEventResponse): unknown {
    const obj: any = {};
    message.PastEvent !== undefined &&
      (obj.PastEvent = message.PastEvent ? PastEvent.toJSON(message.PastEvent) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryGetPastEventResponse>): QueryGetPastEventResponse {
    const message = { ...baseQueryGetPastEventResponse } as QueryGetPastEventResponse;
    if (object.PastEvent !== undefined && object.PastEvent !== null) {
      message.PastEvent = PastEvent.fromPartial(object.PastEvent);
    } else {
      message.PastEvent = undefined;
    }
    return message;
  },
};

const baseQueryAllPastEventRequest: object = {};

export const QueryAllPastEventRequest = {
  encode(message: QueryAllPastEventRequest, writer: Writer = Writer.create()): Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllPastEventRequest {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllPastEventRequest } as QueryAllPastEventRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllPastEventRequest {
    const message = { ...baseQueryAllPastEventRequest } as QueryAllPastEventRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllPastEventRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined &&
      (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryAllPastEventRequest>): QueryAllPastEventRequest {
    const message = { ...baseQueryAllPastEventRequest } as QueryAllPastEventRequest;
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageRequest.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

const baseQueryAllPastEventResponse: object = {};

export const QueryAllPastEventResponse = {
  encode(message: QueryAllPastEventResponse, writer: Writer = Writer.create()): Writer {
    for (const v of message.PastEvent) {
      PastEvent.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): QueryAllPastEventResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQueryAllPastEventResponse } as QueryAllPastEventResponse;
    message.PastEvent = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.PastEvent.push(PastEvent.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllPastEventResponse {
    const message = { ...baseQueryAllPastEventResponse } as QueryAllPastEventResponse;
    message.PastEvent = [];
    if (object.PastEvent !== undefined && object.PastEvent !== null) {
      for (const e of object.PastEvent) {
        message.PastEvent.push(PastEvent.fromJSON(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromJSON(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },

  toJSON(message: QueryAllPastEventResponse): unknown {
    const obj: any = {};
    if (message.PastEvent) {
      obj.PastEvent = message.PastEvent.map((e) => (e ? PastEvent.toJSON(e) : undefined));
    } else {
      obj.PastEvent = [];
    }
    message.pagination !== undefined &&
      (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<QueryAllPastEventResponse>): QueryAllPastEventResponse {
    const message = { ...baseQueryAllPastEventResponse } as QueryAllPastEventResponse;
    message.PastEvent = [];
    if (object.PastEvent !== undefined && object.PastEvent !== null) {
      for (const e of object.PastEvent) {
        message.PastEvent.push(PastEvent.fromPartial(e));
      }
    }
    if (object.pagination !== undefined && object.pagination !== null) {
      message.pagination = PageResponse.fromPartial(object.pagination);
    } else {
      message.pagination = undefined;
    }
    return message;
  },
};

/** Query defines the gRPC querier service. */
export interface Query {
  /** Queries a transportDocument by id. */
  TransportDocument(request: QueryGetTransportDocumentRequest): Promise<QueryGetTransportDocumentResponse>;

  /** Queries a list of transportDocument items. */
  TransportDocumentAll(request: QueryAllTransportDocumentRequest): Promise<QueryAllTransportDocumentResponse>;

  /** Queries a deviceJobData by id. */
  DeviceJobData(request: QueryGetDeviceJobDataRequest): Promise<QueryGetDeviceJobDataResponse>;

  /** Queries a list of deviceJobData items. */
  DeviceJobDataAll(request: QueryAllDeviceJobDataRequest): Promise<QueryAllDeviceJobDataResponse>;

  /** Queries a transportDocumentIdByOrderPositionId by index. */
  TransportDocumentIdByOrderPositionId(
    request: QueryGetTransportDocumentIdByOrderPositionIdRequest
  ): Promise<QueryGetTransportDocumentIdByOrderPositionIdResponse>;

  /** Queries a DangerousGoodRegistration by index. */
  DangerousGoodRegistration(
    request: QueryGetDangerousGoodRegistrationRequest
  ): Promise<QueryGetDangerousGoodRegistrationResponse>;

  /** Queries a list of DangerousGoodRegistration items. */
  DangerousGoodRegistrationAll(
    request: QueryAllDangerousGoodRegistrationRequest
  ): Promise<QueryAllDangerousGoodRegistrationResponse>;

  /** Queries a PastEvent by id. */
  PastEvent(request: QueryGetPastEventRequest): Promise<QueryGetPastEventResponse>;

  /** Queries a list of PastEvent items. */
  PastEventAll(request: QueryAllPastEventRequest): Promise<QueryAllPastEventResponse>;
}

export class QueryClientImpl implements Query {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }

  TransportDocument(request: QueryGetTransportDocumentRequest): Promise<QueryGetTransportDocumentResponse> {
    const data = QueryGetTransportDocumentRequest.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic.Query',
      'TransportDocument',
      data
    );
    return promise.then((data) => QueryGetTransportDocumentResponse.decode(new Reader(data)));
  }

  TransportDocumentAll(request: QueryAllTransportDocumentRequest): Promise<QueryAllTransportDocumentResponse> {
    const data = QueryAllTransportDocumentRequest.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic.Query',
      'TransportDocumentAll',
      data
    );
    return promise.then((data) => QueryAllTransportDocumentResponse.decode(new Reader(data)));
  }

  DeviceJobData(request: QueryGetDeviceJobDataRequest): Promise<QueryGetDeviceJobDataResponse> {
    const data = QueryGetDeviceJobDataRequest.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic.Query',
      'DeviceJobData',
      data
    );
    return promise.then((data) => QueryGetDeviceJobDataResponse.decode(new Reader(data)));
  }

  DeviceJobDataAll(request: QueryAllDeviceJobDataRequest): Promise<QueryAllDeviceJobDataResponse> {
    const data = QueryAllDeviceJobDataRequest.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic.Query',
      'DeviceJobDataAll',
      data
    );
    return promise.then((data) => QueryAllDeviceJobDataResponse.decode(new Reader(data)));
  }

  TransportDocumentIdByOrderPositionId(
    request: QueryGetTransportDocumentIdByOrderPositionIdRequest
  ): Promise<QueryGetTransportDocumentIdByOrderPositionIdResponse> {
    const data = QueryGetTransportDocumentIdByOrderPositionIdRequest.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic.Query',
      'TransportDocumentIdByOrderPositionId',
      data
    );
    return promise.then((data) => QueryGetTransportDocumentIdByOrderPositionIdResponse.decode(new Reader(data)));
  }

  DangerousGoodRegistration(
    request: QueryGetDangerousGoodRegistrationRequest
  ): Promise<QueryGetDangerousGoodRegistrationResponse> {
    const data = QueryGetDangerousGoodRegistrationRequest.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic.Query',
      'DangerousGoodRegistration',
      data
    );
    return promise.then((data) => QueryGetDangerousGoodRegistrationResponse.decode(new Reader(data)));
  }

  DangerousGoodRegistrationAll(
    request: QueryAllDangerousGoodRegistrationRequest
  ): Promise<QueryAllDangerousGoodRegistrationResponse> {
    const data = QueryAllDangerousGoodRegistrationRequest.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic.Query',
      'DangerousGoodRegistrationAll',
      data
    );
    return promise.then((data) => QueryAllDangerousGoodRegistrationResponse.decode(new Reader(data)));
  }

  PastEvent(request: QueryGetPastEventRequest): Promise<QueryGetPastEventResponse> {
    const data = QueryGetPastEventRequest.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic.Query',
      'PastEvent',
      data
    );
    return promise.then((data) => QueryGetPastEventResponse.decode(new Reader(data)));
  }

  PastEventAll(request: QueryAllPastEventRequest): Promise<QueryAllPastEventResponse> {
    const data = QueryAllPastEventRequest.encode(request).finish();
    const promise = this.rpc.request(
      'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic.Query',
      'PastEventAll',
      data
    );
    return promise.then((data) => QueryAllPastEventResponse.decode(new Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

if (true) {
  util.Long = Long as any;
  configure();
}
