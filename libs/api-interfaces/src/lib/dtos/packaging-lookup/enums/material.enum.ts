/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum Material {
  'Stahl',
  'Aluminium',
  'Naturholz',
  'Sperrholz',
  'Holzfaserwerkstoff',
  'Pappe',
  'Kunststoff',
  'Kombination mit einem Kunststoff-Innenbehälter',
  'Textilgewebe',
  'Papier, mehrlagig',
  'Metall, außer Stahl oder Aluminium',
  'Porzellan, Glas oder Steinzeug',
  'Kunststoffgefäß',
  'Kunststoffgewebe',
}
