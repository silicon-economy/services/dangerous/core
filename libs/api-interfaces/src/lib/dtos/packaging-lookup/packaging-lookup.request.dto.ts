/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ApiProperty} from '@nestjs/swagger';
import {IsEnum, IsOptional} from 'class-validator';
import {TypesOfMaterial} from "./enums/types-of-material.enum";
import {PackagingKind} from "./enums/packaging-kind.enum";
import {Material} from "./enums/material.enum";

export class PackagingLookupRequestDto {
  @ApiProperty({
    description: 'The query used to filter the packaging information.',
    default: 'Fässer',
    required: false,
  })
  query: string;

  @IsOptional()
  @IsEnum(TypesOfMaterial)
  @ApiProperty({
    description: 'The type of material the packaging is made of.',
    required: false,
    enum: TypesOfMaterial,
  })
  typesOfMaterial?: TypesOfMaterial;

  @IsOptional()
  @IsEnum(PackagingKind)
  @ApiProperty({
    description: 'The kind of packaging that is being used.',
    required: false,
    enum: PackagingKind,
  })
  kind?: PackagingKind;

  @IsOptional()
  @ApiProperty({
    description: 'The category.',
    default: 'nicht abnehmbarer Deckel',
    required: false,
  })
  category?: string;

  @IsOptional()
  @IsEnum(Material)
  @ApiProperty({
    description: 'The UN-Number of the hazardous good.',
    required: false,
    enum: Material,
  })
  material?: Material;
}
