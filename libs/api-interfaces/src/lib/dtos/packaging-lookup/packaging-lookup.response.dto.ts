/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { IsEnum } from 'class-validator';
import {TypesOfMaterial} from "./enums/types-of-material.enum";
import {PackagingKind} from "./enums/packaging-kind.enum";
import {Material} from "./enums/material.enum";


export class PackagingLookupResponseDto {
  @IsEnum(PackagingKind)
  kind: PackagingKind;

  @IsEnum(TypesOfMaterial)
  typesOfMaterial: TypesOfMaterial;

  @IsEnum(Material)
  material: Material;

  category: string;

  code: string;
}
