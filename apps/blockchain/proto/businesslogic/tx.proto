// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

syntax = "proto3";
package git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic;

import "businesslogic/transport_document.proto";
import "businesslogic/device_job_data.proto";
import "businesslogic/transport_document_id_by_order_position_id.proto";
import "businesslogic/dangerous_good_registration.proto";
import "businesslogic/past_event.proto";
import "gogoproto/gogo.proto";
// this line is used by starport scaffolding # proto/tx/import

option go_package = "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types";

// Msg defines the Msg service.
service Msg {
  rpc CreateTransportDocument(MsgCreateTransportDocument) returns (MsgCreateTransportDocumentResponse);
  rpc UpdateTransportDocument(MsgUpdateTransportDocument) returns (MsgUpdateTransportDocumentResponse);
  rpc DeleteTransportDocument(MsgDeleteTransportDocument) returns (MsgDeleteTransportDocumentResponse);

  rpc AddDeviceToOrderPosition(MsgAddDeviceToOrderPosition) returns (MsgAddDeviceToOrderPositionResponse);
  rpc UploadDeviceData(MsgUploadDeviceData) returns (MsgUploadDeviceDataResponse);
  rpc RevertToGenesis(MsgRevertToGenesis) returns (MsgRevertToGenesisResponse);
  rpc FinishIoTBrokerJob(MsgFinishIoTBrokerJob) returns (MsgFinishIoTBrokerJobResponse);
  rpc RemoveDeviceFromOrderPosition(MsgRemoveDeviceFromOrderPosition) returns (MsgRemoveDeviceFromOrderPositionResponse);
  rpc CreateDangerousGoodRegistration(MsgCreateDangerousGoodRegistration) returns (MsgCreateDangerousGoodRegistrationResponse);
  rpc UpdateDangerousGoodRegistration(MsgUpdateDangerousGoodRegistration) returns (MsgUpdateDangerousGoodRegistrationResponse);
  rpc DeleteDangerousGoodRegistration(MsgDeleteDangerousGoodRegistration) returns (MsgDeleteDangerousGoodRegistrationResponse);

  rpc FetchTransportDocument(MsgFetchTransportDocument) returns (MsgFetchTransportDocumentResponse);
  rpc FetchTransportDocumentIdByOrderPositionId(MsgFetchTransportDocumentIdByOrderPositionId) returns (MsgFetchTransportDocumentIdByOrderPositionIdResponse);
  rpc FetchAllTransportDocuments(MsgFetchAllTransportDocuments) returns (MsgFetchAllTransportDocumentsResponse);

  rpc FetchDeviceJobData(MsgFetchDeviceJobData) returns (MsgFetchDeviceJobDataResponse);
  rpc FetchAllDeviceJobData(MsgFetchAllDeviceJobData) returns (MsgFetchAllDeviceJobDataResponse);

  rpc FetchDangerousGoodRegistration(MsgFetchDangerousGoodRegistration) returns (MsgFetchDangerousGoodRegistrationResponse);
  rpc FetchAllDangerousGoodRegistrations(MsgFetchAllDangerousGoodRegistrations) returns (MsgFetchAllDangerousGoodRegistrationsResponse);

  rpc FetchPastEvent(MsgFetchPastEvent) returns (MsgFetchPastEventResponse);
  rpc FetchAllPastEvents(MsgFetchAllPastEvents) returns (MsgFetchAllPastEventsResponse);

  // this line is used by starport scaffolding # proto/tx/rpc
}


message MsgCreateTransportDocument {
  string creator = 1;
  Company consignor = 2;
  Carrier carrier = 3;
  Freight freight = 4;
  repeated LogEntry logEntries = 5;
  string status = 6;
  double createdDate = 7;
  double lastUpdate = 8;
}

message MsgCreateTransportDocumentResponse {
  TransportDocument transportDocument = 1;
}

message MsgUpdateTransportDocument {
  string creator = 1;
  string id = 2;
  Company consignor = 3;
  Carrier carrier = 4;
  Freight freight = 5;
  repeated LogEntry logEntries = 6;
  string status = 7;
  double createdDate = 8;
  double lastUpdate = 9;
}

message MsgUpdateTransportDocumentResponse {
  TransportDocument transportDocument = 1;
}

message MsgDeleteTransportDocument {
  string creator = 1;
  string id = 2;
}

message MsgDeleteTransportDocumentResponse {}

message MsgUploadDeviceData {
  string creator = 1;
  uint64 jobId = 2;
  uint64 deviceId = 3;
  uint32 protocolVersion = 4;
  FirmwareDescriptor firmwareDescriptor = 5;
  uint64 comTimestamp = 6;
  string lastComCause = 7;
  repeated Data data = 8;
}

message MsgUploadDeviceDataResponse {
}

message MsgAddDeviceToOrderPosition {
  string creator = 1;
  string transportDocumentId = 2;
  string orderId = 3;
  uint64 orderPositionId = 4;
  uint64 deviceId = 5;
  uint64 timeStamp = 6;
}

message MsgAddDeviceToOrderPositionResponse {
  TransportDocument transportDocument = 1;
}

message MsgRevertToGenesis {
  string creator = 1;
}

message MsgRevertToGenesisResponse {
}

message MsgFinishIoTBrokerJob {
  string creator = 1;
  uint64 jobId = 2;
  uint64 deviceId = 3;
}

message MsgFinishIoTBrokerJobResponse {
}

message MsgRemoveDeviceFromOrderPosition {
  string creator = 1;
  uint64 orderPositionId = 2;
  uint64 timeStamp = 3;
}

message MsgRemoveDeviceFromOrderPositionResponse {
}

message MsgCreateDangerousGoodRegistration {
  string creator = 1;
  Company consignor = 2;
  Freight freight = 3;
  double createdDate = 4;
  double lastUpdate = 5;
}
message MsgCreateDangerousGoodRegistrationResponse {
  DangerousGoodRegistration dangerousGoodRegistration = 1;
}

message MsgUpdateDangerousGoodRegistration {
  string creator = 1;
  string id = 2;
  Company consignor = 3;
  Freight freight = 4;
  double createdDate = 5;
  int64 lastUpdate =  6;
}
message MsgUpdateDangerousGoodRegistrationResponse {
  DangerousGoodRegistration dangerousGoodRegistration = 1;
}

message MsgDeleteDangerousGoodRegistration {
  string creator = 1;
  string id = 2;
}
message MsgDeleteDangerousGoodRegistrationResponse {}

message MsgFetchTransportDocument {
  string creator = 1;
  string id = 2;
}

message MsgFetchTransportDocumentResponse {
  TransportDocument transportDocument = 1 [(gogoproto.nullable) = false];
}

message MsgFetchTransportDocumentIdByOrderPositionId {
  string creator = 1;
  string orderPositionId = 2;
}

message MsgFetchTransportDocumentIdByOrderPositionIdResponse {
  string transportDocumentId = 1;
}

message MsgFetchAllTransportDocuments {
  string creator = 1;
}

message MsgFetchAllTransportDocumentsResponse {
  repeated TransportDocument transportDocument = 1 [(gogoproto.nullable) = false];
}

message MsgFetchDeviceJobData {
  string creator = 1;
  uint64 id = 2;
}

message MsgFetchDeviceJobDataResponse {
  repeated MsgDeviceData deviceData = 1;
}

message MsgFetchAllDeviceJobData {
  string creator = 1;
}

message MsgFetchAllDeviceJobDataResponse {
  repeated DeviceJobData deviceJobData = 1 [(gogoproto.nullable) = false];
}

message MsgFetchDangerousGoodRegistration {
  string creator = 1;
  string id = 2;
}

message MsgFetchDangerousGoodRegistrationResponse {
  DangerousGoodRegistration dangerousGoodRegistration = 1 [(gogoproto.nullable) = false];
}

message MsgFetchAllDangerousGoodRegistrations {
  string creator = 1;
}

message MsgFetchAllDangerousGoodRegistrationsResponse {
  repeated DangerousGoodRegistration dangerousGoodRegistrations = 1 [(gogoproto.nullable) = false];
}

message MsgFetchPastEvent {
  string creator = 1;
  uint64 id = 2;
}

message MsgFetchPastEventResponse {
  PastEvent PastEvent = 1 [(gogoproto.nullable) = false];
}

message MsgFetchAllPastEvents {
  string creator = 1;
}

message MsgFetchAllPastEventsResponse {
  repeated PastEvent PastEvent = 1 [(gogoproto.nullable) = false];
}

// this line is used by starport scaffolding # proto/tx/message
