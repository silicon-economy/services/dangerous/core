# dangerousblockchain

**dangerousblockchain** is a blockchain built using Cosmos SDK and Tendermint and created
with [ignite](https://github.com/ignite/cli).

## Get started

```
ignite chain serve
```

`serve` command installs dependencies, builds, initializes, and starts your blockchain in development.

### Configure

Your blockchain can be configured with `config.yml` during development. To learn more see
the [ignite documentation](https://github.com/ignite/cli#documentation).

### Launch

To launch your blockchain live on multiple nodes use `ignite network` commands. Learn more
about [Starport Network](https://github.com/tendermint/spn).

### Web Frontend

Starport has scaffolded a Vue.js-based web app in the `vue` directory. Run the following commands to install
dependencies and start the app:

```
cd vue
npm install
npm run serve
```

The frontend app is built using the `@starport/vue` and `@starport/vuex` packages. For details, see
the [monorepo for Starport front-end development](https://github.com/tendermint/vue).

## Release

To release a new version of your blockchain, create and push a new tag with `v` prefix. A new draft release with the
configured targets will be created.

```
git tag v0.1
git push origin v0.1
```

After a draft release is created, make your final changes from the release page and publish it.

### Install

To install the recommended version of your blockchain node's binary, execute the following command on your machine:

```
curl https://get.ignite.com/cli@v0.23.0! | sudo bash
```

`org/dangerous-blockchain` should match the `username` and `repo_name` of the Github repository to which the source code
was pushed. Learn more about [the install process](https://github.com/allinbits/ignite-installer).

## Learn more

- [Starport](https://github.com/tendermint/starport)
- [Ignite CLI documentation](https://github.com/ignite/cli#documentation)
- [Cosmos SDK documentation](https://docs.cosmos.network)
- [Cosmos SDK Tutorials](https://tutorials.cosmos.network)
- [Discord](https://discord.gg/cosmosnetwork)
