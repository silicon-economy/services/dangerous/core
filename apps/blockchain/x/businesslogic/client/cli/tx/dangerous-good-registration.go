// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package tx

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/spf13/cobra"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

var _ = strconv.Itoa(0)

func CmdFetchDangerousGoodRegistration() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "fetch-dangerous-good-registration [id]",
		Short: "Broadcast message NewMsgFetchDangerousGoodRegistration",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			argId := args[0]
			clientCtx, err := client.GetClientTxContext(cmd)

			if err != nil {
				return err
			}

			msg := types.NewMsgFetchDangerousGoodRegistration(
				clientCtx.GetFromAddress().String(),
				argId,
			)

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdFetchAllDangerousGoodRegistrations() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "fetch-all-dangerous-good-registrations [id]",
		Short: "Broadcast message NewMsgFetchAllDangerousGoodRegistrations",
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientTxContext(cmd)

			if err != nil {
				return err
			}

			msg := types.NewMsgFetchAllDangerousGoodRegistrations(
				clientCtx.GetFromAddress().String(),
			)

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdAddNewEmptyDangerousGoodRegistration() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "create-dangerous-good-registration [id]",
		Short: "Create a new dangerous-good-registration",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			clientCtx, err := client.GetClientTxContext(cmd)

			if err != nil {
				return err
			}

			msg := types.NewMsgCreateDangerousGoodRegistration(
				clientCtx.GetFromAddress().String(),
				&types.Company{},
				&types.Freight{},
				0,
			)

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
