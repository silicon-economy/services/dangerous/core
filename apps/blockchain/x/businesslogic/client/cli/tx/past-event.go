// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package tx

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/spf13/cobra"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func CmdFetchPastEvent() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "fetch-past-event [id]",
		Short: "Broadcast message MsgFetchPastEvent",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			argId := args[0]
			clientCtx, err := client.GetClientTxContext(cmd)

			if err != nil {
				return err
			}

			id, err := strconv.ParseUint(argId, 10, 64)

			msg := types.NewMsgFetchPastEvent(
				clientCtx.GetFromAddress().String(),
				id,
			)

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdFetchAllPastEvents() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "fetch-all-past-events",
		Short: "Broadcast message MsgFetchAllPastEvents",
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			clientCtx, err := client.GetClientTxContext(cmd)

			if err != nil {
				return err
			}

			msg := types.NewMsgFetchAllPastEvents(
				clientCtx.GetFromAddress().String(),
			)

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
