// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package tx

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/spf13/cobra"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

var _ = strconv.Itoa(0)

func CmdFetchTransportDocument() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "fetch-transport-document [id]",
		Short: "Broadcast message NewMsgFetchTransportDocument",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			argId := args[0]
			clientCtx, err := client.GetClientTxContext(cmd)

			if err != nil {
				return err
			}

			msg := types.NewMsgFetchTransportDocumentRequest(
				clientCtx.GetFromAddress().String(),
				argId,
			)

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdFetchTransportDocumentIdByOrderPositionId() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "fetch-transport-document-id-by-order-position-id [orderPositionId]",
		Short: "Broadcast message NewMsgFetchTransportDocumentIdByOrderPositionId",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			argId := args[0]
			clientCtx, err := client.GetClientTxContext(cmd)

			if err != nil {
				return err
			}

			msg := types.NewMsgFetchTransportDocumentIdByOrderPositionId(
				clientCtx.GetFromAddress().String(),
				argId,
			)

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdFetchAllTransportDocuments() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "fetch-all-transport-documents",
		Short: "Broadcast message NewMsgFetchAllTransportDocuments",
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			clientCtx, err := client.GetClientTxContext(cmd)

			if err != nil {
				return err
			}

			msg := types.NewMsgFetchAllTransportDocuments(
				clientCtx.GetFromAddress().String(),
			)

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdAddNewEmptyTransportDocument() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "add-empty-transport-document",
		Short: "Broadcast message add-empty-transport-document",
		Args:  cobra.ExactArgs(0),
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			clientCtx, err := client.GetClientTxContext(cmd)

			if err != nil {
				return err
			}

			msg := types.NewMsgCreateTransportDocument(
				clientCtx.GetFromAddress().String(),
				&types.Company{},
				&types.Freight{},
				&types.Carrier{},
				"",
				0,
			)

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
