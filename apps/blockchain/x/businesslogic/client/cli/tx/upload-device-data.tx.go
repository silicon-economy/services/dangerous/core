// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package tx

import (
	"time"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/spf13/cobra"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func CmdUploadDeviceData() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "upload-device-data",
		Short: "Upload a new DeviceData with Alert",
		Args:  cobra.ExactArgs(0),
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientTxContext(cmd)

			if err != nil {
				return err
			}

			msg := types.NewMsgUploadDeviceData(
				clientCtx.GetFromAddress().String(),
				1,
				1,
				types.FirmwareDescriptor{
					VersionNumber: &types.VersionNumber{
						Major:         1,
						Minor:         1,
						Patch:         1,
						CommitCounter: 1,
						ReleaseFlag:   true,
					},
					CommitHash: "",
					DirtyFlad:  false,
				},
				1629460334991,
				"Alarm",
				[]*types.Data{
					{
						Temperature: 16.69,
						Humidity:    82,
						Timestamp:   time.Unix(1629460334, 0).Format(time.RFC3339),
						MotionState: "Static",
					},
				},
				1,
			)

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
