// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package tx

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/spf13/cobra"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

var _ = strconv.Itoa(0)

func CmdRemoveDeviceFromOrderPosition() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "remove-device-from-order-position [order-position-id]",
		Short: "Broadcast message removeDeviceFromOrderPosition",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			argOrderPositionId := args[0]

			clientCtx, err := client.GetClientTxContext(cmd)

			if err != nil {
				return err
			}

			orderPositionId, err := strconv.ParseUint(argOrderPositionId, 10, 64)

			if err != nil {
				return err
			}

			msg := types.NewMsgRemoveDeviceFromOrderPosition(
				clientCtx.GetFromAddress().String(),
				orderPositionId,
			)

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
