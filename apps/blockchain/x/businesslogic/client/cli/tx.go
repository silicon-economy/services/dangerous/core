// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package cli

import (
	"fmt"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/spf13/cobra"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/client/cli/tx"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// GetTxCmd returns the transaction commands for this module
func GetTxCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:                        types.ModuleName,
		Short:                      fmt.Sprintf("%s transactions subcommands", types.ModuleName),
		DisableFlagParsing:         true,
		SuggestionsMinimumDistance: 2,
		RunE:                       client.ValidateCmd,
	}

	cmd.AddCommand(tx.CmdFetchDangerousGoodRegistration())
	cmd.AddCommand(tx.CmdFetchAllDangerousGoodRegistrations())
	cmd.AddCommand(tx.CmdAddNewEmptyDangerousGoodRegistration())

	cmd.AddCommand(tx.CmdFetchTransportDocument())
	cmd.AddCommand(tx.CmdFetchTransportDocumentIdByOrderPositionId())
	cmd.AddCommand(tx.CmdFetchAllTransportDocuments())
	cmd.AddCommand(tx.CmdAddNewEmptyTransportDocument())

	cmd.AddCommand(tx.CmdFetchDeviceJobData())
	cmd.AddCommand(tx.CmdFetchAllDeviceJobData())
	cmd.AddCommand(tx.CmdFinishIoTBrokerJob())
	cmd.AddCommand(tx.CmdRemoveDeviceFromOrderPosition())
	cmd.AddCommand(tx.CmdUploadDeviceData())

	cmd.AddCommand(tx.CmdFetchPastEvent())
	cmd.AddCommand(tx.CmdFetchAllPastEvents())

	cmd.AddCommand(tx.CmdRevertToGenesis())
	// this line is used by starport scaffolding # 1

	return cmd
}
