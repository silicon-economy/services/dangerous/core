// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	keepertest "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/testutil/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func TestTransportDocumentIdByOrderPositionIdQuerySingle(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	wctx := sdk.WrapSDKContext(ctx)
	msgs := createNTransportDocumentIdByOrderPositionId(keeper, ctx, 2)
	for _, tc := range []struct {
		desc     string
		request  *types.QueryGetTransportDocumentIdByOrderPositionIdRequest
		response *types.QueryGetTransportDocumentIdByOrderPositionIdResponse
		err      error
	}{
		{
			desc: "First",
			request: &types.QueryGetTransportDocumentIdByOrderPositionIdRequest{
				OrderPositionId: msgs[0].OrderPositionId,
			},
			response: &types.QueryGetTransportDocumentIdByOrderPositionIdResponse{TransportDocumentId: msgs[0].TransportDocumentId},
		},
		{
			desc: "Second",
			request: &types.QueryGetTransportDocumentIdByOrderPositionIdRequest{
				OrderPositionId: msgs[1].OrderPositionId,
			},
			response: &types.QueryGetTransportDocumentIdByOrderPositionIdResponse{TransportDocumentId: msgs[1].TransportDocumentId},
		},
		{
			desc: "KeyNotFound",
			request: &types.QueryGetTransportDocumentIdByOrderPositionIdRequest{
				OrderPositionId: strconv.Itoa(100000),
			},
			err: status.Error(codes.InvalidArgument, "not found"),
		},
		{
			desc: "InvalidRequest",
			err:  status.Error(codes.InvalidArgument, "invalid request"),
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			response, err := keeper.TransportDocumentIdByOrderPositionId(wctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.Equal(t, tc.response, response)
			}
		})
	}
}
