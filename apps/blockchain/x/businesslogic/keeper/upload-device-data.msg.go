// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

type PuckConfig struct {
	JobID             int    `json:"jobId"`
	DeviceID          int    `json:"deviceId"`
	ProtocolVersion   int    `json:"protocolVersion"`
	JobState          string `json:"jobState"`
	ClearAlarm        bool   `json:"clearAlarm"`
	MeasurementCycles struct {
		Storage   int `json:"storage"`
		Transport int `json:"transport"`
	} `json:"measurementCycles"`
	DataAmount       int `json:"dataAmount"`
	TemperatureAlarm struct {
		UpperBound float64 `json:"upperBound"`
		LowerBound float64 `json:"lowerBound"`
	} `json:"temperatureAlarm"`
	HumidityAlarm struct {
		UpperBound float64 `json:"upperBound"`
		LowerBound float64 `json:"lowerBound"`
	} `json:"humidityAlarm"`
	LedTimings struct {
		ShowID    int `json:"showId"`
		ShowAlarm int `json:"showAlarm"`
		ShowTemp  int `json:"showTemp"`
	} `json:"ledTimings"`
}

// UploadDeviceData uploads deviceData to the IoT broker.
func (k msgServer) UploadDeviceData(
	goCtx context.Context,
	msg *types.MsgUploadDeviceData,
) (*types.MsgUploadDeviceDataResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	var deviceData = types.MsgDeviceData{
		Creator:            msg.Creator,
		ProtocolVersion:    msg.ProtocolVersion,
		JobId:              msg.JobId,
		FirmwareDescriptor: msg.FirmwareDescriptor,
		ComTimestamp:       msg.ComTimestamp,
		LastComCause:       msg.LastComCause,
		Data:               msg.Data,
		DeviceId:           msg.DeviceId,
	}

	_, found := k.GetDeviceJobData(ctx, msg.JobId)

	if deviceData.GetLastComCause() == types.LastComCauseAlarm {
		var puckConfig PuckConfig
		// Get Puck Config
		resp, err := http.Get(
			types.ApiEndpointBroker + "/" + strconv.FormatUint(
				deviceData.GetJobId(),
				10,
			),
		)
		if err != nil {
			log.Fatalln(err)
		}
		// We Read the response body on the line below.
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}
		// Convert the body to json
		if err := json.Unmarshal(body, &puckConfig); err != nil { // Parse []byte to go struct pointer
			fmt.Println("Can not unmarshal JSON")
		}

		k.AppendPastEvent(ctx, types.PastEvent{
			Id:      0,
			Creator: msg.Creator,
			EventTypeAlert: &types.EventTypeAlert{
				IsEventTypeAlert:          true,
				Creator:                   msg.Creator,
				DeviceId:                  msg.GetDeviceId(),
				JobId:                     msg.GetJobId(),
				Temperature:               msg.GetData()[0].Temperature,
				UpperTemperatureThreshold: float32(puckConfig.TemperatureAlarm.UpperBound),
				LowerTemperatureThreshold: float32(puckConfig.TemperatureAlarm.LowerBound),
				Humidity:                  msg.GetData()[0].Humidity,
				UpperHumidityThreshold:    float32(puckConfig.HumidityAlarm.UpperBound),
				LowerHumidityThreshold:    float32(puckConfig.HumidityAlarm.LowerBound),
				Timestamp:                 msg.GetData()[0].Timestamp},
		})

		err = ctx.EventManager().EmitTypedEvent(&types.EventTypeAlert{
			IsEventTypeAlert:          true,
			Creator:                   msg.Creator,
			DeviceId:                  msg.GetDeviceId(),
			JobId:                     msg.GetJobId(),
			Temperature:               msg.GetData()[0].Temperature,
			UpperTemperatureThreshold: float32(puckConfig.TemperatureAlarm.UpperBound),
			LowerTemperatureThreshold: float32(puckConfig.TemperatureAlarm.LowerBound),
			Humidity:                  msg.GetData()[0].Humidity,
			UpperHumidityThreshold:    float32(puckConfig.HumidityAlarm.UpperBound),
			LowerHumidityThreshold:    float32(puckConfig.HumidityAlarm.LowerBound),
			Timestamp:                 msg.GetData()[0].Timestamp,
		})
		if err != nil {
			return nil, err
		}
	}

	if !found {
		var deviceJobData = types.DeviceJobData{
			Creator: msg.Creator,
			Id:      msg.JobId,
			DeviceMessages: []*types.MsgDeviceData{
				&deviceData,
			},
		}
		k.AppendDeviceJobData(ctx, deviceJobData)
	} else {
		deviceJobData, _ := k.GetDeviceJobData(ctx, msg.JobId)
		deviceJobData.DeviceMessages = append(deviceJobData.DeviceMessages, &deviceData)
		k.SetDeviceJobData(ctx, deviceJobData)
	}

	return &types.MsgUploadDeviceDataResponse{}, nil
}
