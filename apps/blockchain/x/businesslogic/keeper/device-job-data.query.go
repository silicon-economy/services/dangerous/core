// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// DeviceJobDataAll returns all deviceJobData.
func (k Keeper) DeviceJobDataAll(c context.Context, req *types.QueryAllDeviceJobDataRequest) (*types.QueryAllDeviceJobDataResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var deviceJobDataArray []types.DeviceJobData
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	deviceJobDataStore := prefix.NewStore(store, types.KeyPrefix(types.DeviceJobDataKey))

	pageRes, err := query.Paginate(deviceJobDataStore, req.Pagination, func(key []byte, value []byte) error {
		var deviceJobData types.DeviceJobData
		if err := k.cdc.Unmarshal(value, &deviceJobData); err != nil {
			return err
		}

		deviceJobDataArray = append(deviceJobDataArray, deviceJobData)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllDeviceJobDataResponse{DeviceJobData: deviceJobDataArray, Pagination: pageRes}, nil
}

// DeviceJobData returns deviceJobData for a given id.
func (k Keeper) DeviceJobData(c context.Context, req *types.QueryGetDeviceJobDataRequest) (*types.QueryGetDeviceJobDataResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var deviceJobData types.DeviceJobData
	ctx := sdk.UnwrapSDKContext(c)

	deviceJobData, found := k.GetDeviceJobData(ctx, req.Id)
	if !found {
		return nil, sdkerrors.ErrKeyNotFound
	}

	return &types.QueryGetDeviceJobDataResponse{DeviceData: deviceJobData.DeviceMessages}, nil
}
