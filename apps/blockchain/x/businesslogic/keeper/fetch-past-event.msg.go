// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// FetchPastEvent returns the latest event for a given id.
func (k msgServer) FetchPastEvent(
	goCtx context.Context,
	msg *types.MsgFetchPastEvent,
) (*types.MsgFetchPastEventResponse, error) {
	var request *types.QueryGetPastEventRequest

	if msg != nil {
		request = &types.QueryGetPastEventRequest{Id: msg.Id}
	}

	document, err := k.PastEvent(goCtx, request)

	if err != nil {
		return nil, err
	}

	return &types.MsgFetchPastEventResponse{PastEvent: document.PastEvent}, nil
}
