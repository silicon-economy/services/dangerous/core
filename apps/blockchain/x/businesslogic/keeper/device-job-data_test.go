// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func CreateNDeviceJobData(keeper *keeper.Keeper, ctx sdk.Context, n int) []types.DeviceJobData {
	items := make([]types.DeviceJobData, n)
	for i := range items {
		items[i].Creator = "any"
		items[i].Id = uint64(i)
		keeper.AppendDeviceJobData(ctx, items[i])
	}
	return items
}

func TestDeviceJobDataGet(t *testing.T) {
	testKeeper, ctx := SetupKeeper(t)
	items := CreateNDeviceJobData(testKeeper, ctx, 10)
	for _, item := range items {
		deviceJobData, _ := testKeeper.GetDeviceJobData(ctx, item.Id)
		assert.Equal(t, item, deviceJobData)
	}
}

func TestDeviceJobDataExist(t *testing.T) {
	testKeeper, ctx := SetupKeeper(t)
	items := CreateNDeviceJobData(testKeeper, ctx, 10)
	for _, item := range items {
		_, found := testKeeper.GetDeviceJobData(ctx, item.Id)
		assert.True(t, found)
	}
}

func TestDeviceJobDataRemove(t *testing.T) {
	testKeeper, ctx := SetupKeeper(t)
	items := CreateNDeviceJobData(testKeeper, ctx, 10)
	for _, item := range items {
		testKeeper.RemoveDeviceJobData(ctx, item.Id)
		_, found := testKeeper.GetDeviceJobData(ctx, item.Id)
		assert.False(t, found)
	}
}

func TestDeviceJobDataGetAll(t *testing.T) {
	testKeeper, ctx := SetupKeeper(t)
	items := CreateNDeviceJobData(testKeeper, ctx, 10)
	assert.Equal(t, items, testKeeper.GetAllDeviceJobData(ctx))
}
