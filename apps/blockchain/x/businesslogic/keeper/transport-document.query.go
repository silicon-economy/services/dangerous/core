// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// TransportDocumentAll returns all transport documents stored in the blockchain.
func (k Keeper) TransportDocumentAll(
	c context.Context,
	req *types.QueryAllTransportDocumentRequest,
) (*types.QueryAllTransportDocumentResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var transportDocuments []types.TransportDocument
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	transportDocumentStore := prefix.NewStore(store, types.KeyPrefix(types.TransportDocumentKey))

	pageRes, err := query.Paginate(transportDocumentStore, req.Pagination, func(key []byte, value []byte) error {
		var transportDocument types.TransportDocument
		if err := k.cdc.Unmarshal(value, &transportDocument); err != nil {
			return err
		}

		transportDocuments = append(transportDocuments, transportDocument)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllTransportDocumentResponse{TransportDocument: transportDocuments, Pagination: pageRes}, nil
}

// TransportDocument returns a specific transport document given by its id.
func (k Keeper) TransportDocument(
	c context.Context,
	req *types.QueryGetTransportDocumentRequest,
) (*types.QueryGetTransportDocumentResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	ctx := sdk.UnwrapSDKContext(c)
	transportDocument, found := k.GetTransportDocument(ctx, req.Id)

	if !found {
		return nil, sdkerrors.ErrKeyNotFound
	}

	return &types.QueryGetTransportDocumentResponse{TransportDocument: transportDocument}, nil
}
