// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func TestFetchTransportDocumentFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	responseFromTransportDocument, errorFromTransportDocument := srv.CreateTransportDocument(
		ctx,
		&types.MsgCreateTransportDocument{
			Creator:     creator,
			Consignor:   &types.Company{},
			Carrier:     &types.Carrier{},
			Freight:     &types.Freight{},
			Status:      "shipped",
			CreatedDate: 0,
			LastUpdate:  0,
		})
	require.NoError(t, errorFromTransportDocument)

	responseFromFetch, errorFromFetch := srv.FetchTransportDocument(
		ctx,
		&types.MsgFetchTransportDocument{
			Creator: creator,
			Id:      responseFromTransportDocument.TransportDocument.Id,
		})
	require.NoError(t, errorFromFetch)
	assert.NotNil(t, responseFromFetch.TransportDocument)
	assert.Equal(t, responseFromFetch.TransportDocument.Status, responseFromTransportDocument.TransportDocument.Status)
}

func TestFetchTransportDocumentNotFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	response, err := srv.FetchTransportDocument(
		ctx,
		&types.MsgFetchTransportDocument{
			Creator: creator,
			Id:      "123",
		})
	require.Error(t, err)
	assert.Nil(t, response)
}

func TestFetchTransportDocumentIsNil(t *testing.T) {
	srv, ctx := SetupMsgServer(t)

	response, err := srv.FetchTransportDocument(
		ctx,
		nil,
	)
	require.Error(t, err)
	assert.Nil(t, response)
}
