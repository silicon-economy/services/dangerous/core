// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"

	keepertest "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/testutil/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/testutil/nullify"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func createNDangerousGoodRegistration(keeper *keeper.Keeper, ctx sdk.Context, n int) []types.DangerousGoodRegistration {
	items := make([]types.DangerousGoodRegistration, n)
	for i := range items {
		items[i].Id = strconv.Itoa(i)

		keeper.SetDangerousGoodRegistration(ctx, items[i])
	}
	return items
}

func TestDangerousGoodRegistrationGet(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	items := createNDangerousGoodRegistration(keeper, ctx, 10)
	for _, item := range items {
		rst, found := keeper.GetDangerousGoodRegistration(ctx,
			item.Id,
		)
		require.True(t, found)
		require.Equal(t,
			nullify.Fill(&item),
			nullify.Fill(&rst),
		)
	}
}
func TestDangerousGoodRegistrationRemove(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	items := createNDangerousGoodRegistration(keeper, ctx, 10)
	for _, item := range items {
		keeper.RemoveDangerousGoodRegistration(ctx,
			item.Id,
		)
		_, found := keeper.GetDangerousGoodRegistration(ctx,
			item.Id,
		)
		require.False(t, found)
	}
}

func TestDangerousGoodRegistrationGetAll(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	items := createNDangerousGoodRegistration(keeper, ctx, 10)
	require.ElementsMatch(t,
		nullify.Fill(items),
		nullify.Fill(keeper.GetAllDangerousGoodRegistration(ctx)),
	)
}
