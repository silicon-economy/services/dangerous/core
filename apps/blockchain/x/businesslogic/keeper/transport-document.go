// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

var isProduction, _ = strconv.ParseBool(os.Getenv(types.IsProduction))

// GetOrderPositionCount gets the total number of orderPostion.
func (k Keeper) GetOrderPositionCount(ctx sdk.Context) uint64 {
	var bz []byte
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.OrderPositionCountKey)
	bz = store.Get(byteKey)

	if bz == nil {
		if isProduction {
			return 10
		} else {
			return 11
		}
	}

	// Parse bytes
	return binary.BigEndian.Uint64(bz) + 2
}

// SetOrderPositionCount sets the total number of transportDocument.
func (k Keeper) SetOrderPositionCount(ctx sdk.Context, count uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.OrderPositionCountKey)
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, count)
	store.Set(byteKey, bz)
}

// GetTransportDocumentCount gets the total number of transportDocument.
func (k Keeper) GetTransportDocumentCount(ctx sdk.Context) uint64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.TransportDocumentCountKey)
	bz := store.Get(byteKey)

	// Count does not exist: no element
	if bz == nil {
		return uint64(len(k.GetAllTransportDocument(ctx)) + 1)
	}

	// Parse bytes
	return binary.BigEndian.Uint64(bz)
}

// SetTransportDocumentCount sets the total number of transportDocument.
func (k Keeper) SetTransportDocumentCount(ctx sdk.Context, count uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.TransportDocumentCountKey)
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, count)
	store.Set(byteKey, bz)
}

// AppendTransportDocument appends a transportDocument in the store with a new id and update the count.
func (k Keeper) AppendTransportDocument(
	ctx sdk.Context,
	transportDocument types.TransportDocument,
) types.TransportDocument {
	transportDocumentCount := k.GetTransportDocumentCount(ctx)
	k.SetTransportDocumentCount(ctx, transportDocumentCount+1)

	// Create the transportDocument id
	currentTime := time.Unix(int64(transportDocument.CreatedDate)/1000, 0)

	transportDocument.Id = fmt.Sprintf(
		types.TransportDocumentIdFormat,
		types.TransportDocumentIdPrefix,
		transportDocumentCount,
		currentTime.Year(),
		currentTime.Month(),
		currentTime.Day(),
	)

	// Set the IDs and save orderPositionId-to-transportDocumentId-mapping
	for _, order := range transportDocument.Freight.Orders {
		for _, orderPosition := range order.OrderPositions {
			// the id for orderPosition is a globally incremental number
			orderPositionCount := k.GetOrderPositionCount(ctx)
			orderPosition.Id = orderPositionCount
			k.SetOrderPositionCount(ctx, orderPositionCount)
			k.SetTransportDocumentIdByOrderPositionId(ctx, types.TransportDocumentIdByOrderPositionId{
				Creator:             transportDocument.Creator,
				OrderPositionId:     strconv.FormatUint(orderPosition.GetId(), 10),
				TransportDocumentId: transportDocument.GetId(),
			})
		}
	}
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TransportDocumentKey))
	appendedValue := k.cdc.MustMarshal(&transportDocument)
	store.Set(GetTransportDocumentIDBytes(transportDocument.Id), appendedValue)
	return transportDocument
}

// SetTransportDocument sets a specific transportDocument in the store.
func (k Keeper) SetTransportDocument(ctx sdk.Context, transportDocument types.TransportDocument) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TransportDocumentKey))
	b := k.cdc.MustMarshal(&transportDocument)
	store.Set(GetTransportDocumentIDBytes(transportDocument.Id), b)
}

// GetTransportDocument returns a transportDocument from its id.
func (k Keeper) GetTransportDocument(ctx sdk.Context, id string) (val types.TransportDocument, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TransportDocumentKey))
	b := store.Get(GetTransportDocumentIDBytes(id))
	if b == nil {
		return val, false
	}
	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// RemoveTransportDocument removes a transportDocument from the store.
func (k Keeper) RemoveTransportDocument(ctx sdk.Context, id string) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TransportDocumentKey))
	store.Delete(GetTransportDocumentIDBytes(id))
}

// GetAllTransportDocument returns all transportDocument.
func (k Keeper) GetAllTransportDocument(ctx sdk.Context) (list []types.TransportDocument) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TransportDocumentKey))
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.TransportDocument
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}

// GetTransportDocumentIDBytes returns the byte representation of the id.
func GetTransportDocumentIDBytes(id string) []byte {
	return []byte(id)
}

// GetTransportDocumentIDFromBytes returns id in string format from a byte array.
func GetTransportDocumentIDFromBytes(bz []byte) string {
	return string(bz)
}

// AddNewJobToIoTBroker adds a new job to the IoT broker.
func AddNewJobToIoTBroker(deviceId uint64, orderPositionId uint64) {
	// get default puck config, add deviceId and JobId
	var puckConfig = types.DefaultPuckConfiguration
	puckConfig.DeviceId = deviceId
	puckConfig.JobId = orderPositionId
	b, _ := json.Marshal(puckConfig)

	_, err := http.Post(types.ApiEndpointBroker, "application/json", bytes.NewReader(b))
	if err != nil {
		return
	}
}

var FinishPuckConfigPostVar = finishPuckConfigPost

// SendFinishPuckConfig sends a FinishPuckConfig POST request to the IoT broker.
func SendFinishPuckConfig(deviceId uint64, orderPositionId uint64) {
	// get default puck config, add deviceId and JobId
	var puckConfig = types.DefaultPuckConfiguration
	puckConfig.DeviceId = deviceId
	puckConfig.JobId = orderPositionId
	puckConfig.JobState = types.JobStateFinished
	b, _ := json.Marshal(puckConfig)

	FinishPuckConfigPostVar(puckConfig.JobId, types.ApiEndpointBroker, "application/json", bytes.NewReader(b))
}

func finishPuckConfigPost(orderPositionId uint64, url string, contentType string, body io.Reader) {
	_, err := http.Post(url, contentType, body)
	if err != nil {
		return
	}
}
