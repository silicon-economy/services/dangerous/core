// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"testing"

	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func TestTransportDocumentMsgServerCreate(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"
	for i := 0; i < 5; i++ {
		resp, err := srv.CreateTransportDocument(ctx, &types.MsgCreateTransportDocument{
			Creator:     creator,
			Consignor:   &types.Company{},
			Carrier:     &types.Carrier{},
			Freight:     &types.Freight{},
			LogEntries:  []*types.LogEntry{},
			Status:      "",
			CreatedDate: 0,
			LastUpdate:  0,
		})
		require.NoError(t, err)
		assert.NotNil(t, resp.TransportDocument.Id)
	}
}

func TestTransportDocumentMsgServerUpdate(t *testing.T) {
	creator := "A"

	for _, tc := range []struct {
		desc    string
		request *types.MsgUpdateTransportDocument
		err     error
	}{
		{
			desc:    "Completed",
			request: &types.MsgUpdateTransportDocument{Creator: creator},
		},
		{
			desc:    "IncorrectOwner",
			request: &types.MsgUpdateTransportDocument{Creator: "B"},
			err:     sdkerrors.ErrUnauthorized,
		},
		{
			desc:    "KeyNotFound",
			request: &types.MsgUpdateTransportDocument{Creator: creator, Id: "10"},
			err:     sdkerrors.ErrKeyNotFound,
		},
	} {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			srv, ctx := SetupMsgServer(t)
			response, err := srv.CreateTransportDocument(
				ctx,
				&types.MsgCreateTransportDocument{
					Creator:    creator,
					Consignor:  &types.Company{},
					Carrier:    &types.Carrier{},
					Freight:    &types.Freight{},
					LogEntries: []*types.LogEntry{},
				},
			)
			require.NoError(t, err)

			if tc.request.Id == "" {
				tc.request.Id = response.TransportDocument.Id
			}

			_, err = srv.UpdateTransportDocument(ctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestTransportDocumentMsgServerDelete(t *testing.T) {
	creator := "A"

	for _, tc := range []struct {
		desc    string
		request *types.MsgDeleteTransportDocument
		err     error
	}{
		{
			desc:    "Completed",
			request: &types.MsgDeleteTransportDocument{Creator: creator},
		},
		{
			desc:    "Unauthorized",
			request: &types.MsgDeleteTransportDocument{Creator: "B"},
			err:     sdkerrors.ErrUnauthorized,
		},
		{
			desc:    "KeyNotFound",
			request: &types.MsgDeleteTransportDocument{Creator: creator, Id: "10"},
			err:     sdkerrors.ErrKeyNotFound,
		},
	} {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			srv, ctx := SetupMsgServer(t)

			response, err := srv.CreateTransportDocument(
				ctx,
				&types.MsgCreateTransportDocument{
					Creator:    creator,
					Consignor:  &types.Company{},
					Carrier:    &types.Carrier{},
					Freight:    &types.Freight{},
					LogEntries: []*types.LogEntry{},
				},
			)

			if tc.request.Id == "" {
				tc.request.Id = response.TransportDocument.Id
			}

			require.NoError(t, err)
			_, err = srv.DeleteTransportDocument(ctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}
