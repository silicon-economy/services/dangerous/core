// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/query"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	keepertest "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/testutil/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/testutil/nullify"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func TestDangerousGoodRegistrationQuerySingle(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	wctx := sdk.WrapSDKContext(ctx)
	msgs := createNDangerousGoodRegistration(keeper, ctx, 2)
	for _, tc := range []struct {
		desc     string
		request  *types.QueryGetDangerousGoodRegistrationRequest
		response *types.QueryGetDangerousGoodRegistrationResponse
		err      error
	}{
		{
			desc: "First",
			request: &types.QueryGetDangerousGoodRegistrationRequest{
				Id: msgs[0].Id,
			},
			response: &types.QueryGetDangerousGoodRegistrationResponse{DangerousGoodRegistration: msgs[0]},
		},
		{
			desc: "Second",
			request: &types.QueryGetDangerousGoodRegistrationRequest{
				Id: msgs[1].Id,
			},
			response: &types.QueryGetDangerousGoodRegistrationResponse{DangerousGoodRegistration: msgs[1]},
		},
		{
			desc: "KeyNotFound",
			request: &types.QueryGetDangerousGoodRegistrationRequest{
				Id: strconv.Itoa(100000),
			},
			err: status.Error(codes.InvalidArgument, "not found"),
		},
		{
			desc: "InvalidRequest",
			err:  status.Error(codes.InvalidArgument, "invalid request"),
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			response, err := keeper.DangerousGoodRegistration(wctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
				require.Equal(t,
					nullify.Fill(tc.response),
					nullify.Fill(response),
				)
			}
		})
	}
}

func TestDangerousGoodRegistrationQueryPaginated(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	wctx := sdk.WrapSDKContext(ctx)
	msgs := createNDangerousGoodRegistration(keeper, ctx, 5)

	request := func(next []byte, offset, limit uint64, total bool) *types.QueryAllDangerousGoodRegistrationRequest {
		return &types.QueryAllDangerousGoodRegistrationRequest{
			Pagination: &query.PageRequest{
				Key:        next,
				Offset:     offset,
				Limit:      limit,
				CountTotal: total,
			},
		}
	}
	t.Run("ByOffset", func(t *testing.T) {
		step := 2
		for i := 0; i < len(msgs); i += step {
			resp, err := keeper.DangerousGoodRegistrationAll(wctx, request(nil, uint64(i), uint64(step), false))
			require.NoError(t, err)
			require.LessOrEqual(t, len(resp.DangerousGoodRegistration), step)
			require.Subset(t,
				nullify.Fill(msgs),
				nullify.Fill(resp.DangerousGoodRegistration),
			)
		}
	})
	t.Run("ByKey", func(t *testing.T) {
		step := 2
		var next []byte
		for i := 0; i < len(msgs); i += step {
			resp, err := keeper.DangerousGoodRegistrationAll(wctx, request(next, 0, uint64(step), false))
			require.NoError(t, err)
			require.LessOrEqual(t, len(resp.DangerousGoodRegistration), step)
			require.Subset(t,
				nullify.Fill(msgs),
				nullify.Fill(resp.DangerousGoodRegistration),
			)
			next = resp.Pagination.NextKey
		}
	})
	t.Run("Total", func(t *testing.T) {
		resp, err := keeper.DangerousGoodRegistrationAll(wctx, request(nil, 0, 0, true))
		require.NoError(t, err)
		require.Equal(t, len(msgs), int(resp.Pagination.Total))
		require.ElementsMatch(t,
			nullify.Fill(msgs),
			nullify.Fill(resp.DangerousGoodRegistration),
		)
	})
	t.Run("InvalidRequest", func(t *testing.T) {
		_, err := keeper.DangerousGoodRegistrationAll(wctx, nil)
		require.ErrorIs(t, err, status.Error(codes.InvalidArgument, "invalid request"))
	})
}
