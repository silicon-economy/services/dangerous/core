// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func CreateNTransportDocument(keeper *keeper.Keeper, ctx sdk.Context, n int) []types.TransportDocument {
	items := make([]types.TransportDocument, n)
	for i := range items {
		items[i].Creator = "any"
		items[i].Consignor = &make([]types.Company, 1)[0]
		items[i].Freight = &make([]types.Freight, 1)[0]
		items[i].Carrier = &make([]types.Carrier, 1)[0]
		var transportDocument = types.TransportDocument{
			Creator:     items[i].Creator,
			Consignor:   items[i].Consignor,
			Freight:     items[i].Freight,
			Carrier:     items[i].Carrier,
			Status:      items[i].Status,
			CreatedDate: items[i].CreatedDate,
			LastUpdate:  items[i].LastUpdate,
			LogEntries:  items[i].LogEntries,
		}
		items[i].Id = keeper.AppendTransportDocument(
			ctx,
			transportDocument,
		).Id
	}
	return items
}

func TestTransportDocumentGet(t *testing.T) {
	keeper, ctx := SetupKeeper(t)
	items := CreateNTransportDocument(keeper, ctx, 10)
	for _, item := range items {
		val, _ := keeper.GetTransportDocument(ctx, item.Id)
		assert.Equal(t, item, val)
	}
}

func TestTransportDocumentExist(t *testing.T) {
	keeper, ctx := SetupKeeper(t)
	items := CreateNTransportDocument(keeper, ctx, 10)
	for _, item := range items {
		_, found := keeper.GetTransportDocument(ctx, item.Id)
		assert.True(t, found)
	}
}

func TestTransportDocumentRemove(t *testing.T) {
	keeper, ctx := SetupKeeper(t)
	items := CreateNTransportDocument(keeper, ctx, 10)
	for _, item := range items {
		keeper.RemoveTransportDocument(ctx, item.Id)
		_, found := keeper.GetTransportDocument(ctx, item.Id)
		assert.False(t, found)
	}
}

func TestTransportDocumentGetAll(t *testing.T) {
	keeper, ctx := SetupKeeper(t)
	items := CreateNTransportDocument(keeper, ctx, 10)
	assert.Equal(t, len(items), len(keeper.GetAllTransportDocument(ctx)))
}
