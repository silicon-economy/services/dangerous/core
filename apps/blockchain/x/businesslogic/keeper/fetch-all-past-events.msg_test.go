// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func TestFetchAllPastEventsFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	_, errorFromUpload := srv.UploadDeviceData(
		ctx,
		&types.MsgUploadDeviceData{
			Creator:            "A",
			JobId:              0,
			DeviceId:           0,
			ProtocolVersion:    0,
			FirmwareDescriptor: nil,
			ComTimestamp:       0,
			LastComCause:       "",
			Data:               nil,
		})
	require.NoError(t, errorFromUpload)

	_, errorFromFetch := srv.FetchAllPastEvents(
		ctx,
		&types.MsgFetchAllPastEvents{
			Creator: creator,
		})
	require.NoError(t, errorFromFetch)
}

func TestFetchAllPastEventsNotFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	response, err := srv.FetchAllPastEvents(
		ctx,
		&types.MsgFetchAllPastEvents{
			Creator: creator,
		})

	require.NoError(t, err)
	assert.Empty(t, response)
}

func TestFetchAllPastEventsIsNil(t *testing.T) {
	srv, ctx := SetupMsgServer(t)

	response, err := srv.FetchAllPastEvents(
		ctx,
		nil,
	)

	require.Error(t, err)
	assert.Nil(t, response)
}
