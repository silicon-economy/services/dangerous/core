// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// FetchDeviceJobData returns all DeviceData for a given id.
func (k msgServer) FetchDeviceJobData(
	goCtx context.Context,
	msg *types.MsgFetchDeviceJobData,
) (*types.MsgFetchDeviceJobDataResponse, error) {
	var request *types.QueryGetDeviceJobDataRequest

	if msg != nil {
		request = &types.QueryGetDeviceJobDataRequest{Id: msg.Id}
	}

	document, err := k.DeviceJobData(goCtx, request)

	if err != nil {
		return nil, err
	}

	return &types.MsgFetchDeviceJobDataResponse{DeviceData: document.DeviceData}, nil
}
