// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// TransportDocumentIdByOrderPositionId accepts an order position id
// and returns the corresponding id of the transport document where the order position is located.
func (k Keeper) TransportDocumentIdByOrderPositionId(
	c context.Context,
	req *types.QueryGetTransportDocumentIdByOrderPositionIdRequest,
) (*types.QueryGetTransportDocumentIdByOrderPositionIdResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}
	ctx := sdk.UnwrapSDKContext(c)

	val, found := k.GetTransportDocumentIdByOrderPositionId(
		ctx,
		req.OrderPositionId,
	)
	if !found {
		return nil, status.Error(codes.InvalidArgument, "not found")
	}

	return &types.QueryGetTransportDocumentIdByOrderPositionIdResponse{
		TransportDocumentId: val.TransportDocumentId,
	}, nil
}
