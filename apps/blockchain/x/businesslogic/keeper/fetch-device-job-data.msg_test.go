// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func TestFetchDeviceJobDataFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	responseFromTransportDocument, errorFromTransportDocument := srv.CreateTransportDocument(
		ctx,
		&types.MsgCreateTransportDocument{
			Creator:   creator,
			Consignor: &types.Company{},
			Carrier:   &types.Carrier{},
			Freight: &types.Freight{
				Orders: []*types.Order{
					{
						Consignee: &types.Company{},
						OrderPositions: []*types.OrderPosition{
							{
								DangerousGood: &types.DangerousGood{},
								DeviceId:      123,
							},
						},
					},
				},
			},
			LogEntries: []*types.LogEntry{
				{
					Status:             "created",
					Date:               1629372434571,
					Author:             "",
					Description:        "",
					AcceptanceCriteria: nil,
				},
			},
		})
	require.NoError(t, errorFromTransportDocument)

	_, errorFromAddDevice := srv.AddDeviceToOrderPosition(
		ctx,
		&types.MsgAddDeviceToOrderPosition{
			Creator:             creator,
			TransportDocumentId: responseFromTransportDocument.TransportDocument.Id,
			OrderId:             responseFromTransportDocument.TransportDocument.Freight.Orders[0].Id,
			OrderPositionId:     responseFromTransportDocument.TransportDocument.Freight.Orders[0].OrderPositions[0].Id,
			DeviceId:            responseFromTransportDocument.TransportDocument.Freight.Orders[0].OrderPositions[0].DeviceId,
		})
	require.NoError(t, errorFromAddDevice)

	_, err := srv.FetchDeviceJobData(
		ctx,
		&types.MsgFetchDeviceJobData{
			Creator: creator,
			Id:      responseFromTransportDocument.TransportDocument.Freight.Orders[0].OrderPositions[0].DeviceId,
		})
	if err != nil {
		return
	}
}

func TestFetchDeviceJobDataNotFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	response, err := srv.FetchDeviceJobData(
		ctx,
		&types.MsgFetchDeviceJobData{
			Creator: creator,
			Id:      0,
		})

	require.Error(t, err)
	assert.Nil(t, response)
}

func TestFetchDeviceJobDataIsNil(t *testing.T) {
	srv, ctx := SetupMsgServer(t)

	response, err := srv.FetchDeviceJobData(
		ctx,
		nil,
	)

	require.Error(t, err)
	assert.Nil(t, response)
}
