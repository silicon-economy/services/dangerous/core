// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"encoding/binary"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// GetDeviceJobDataCount get the total number of deviceJobData
func (k Keeper) GetDeviceJobDataCount(ctx sdk.Context) uint64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.DeviceJobDataCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	return binary.BigEndian.Uint64(bz)
}

// SetDeviceJobDataCount set the total number of deviceJobData
func (k Keeper) SetDeviceJobDataCount(ctx sdk.Context, count uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.DeviceJobDataCountKey)
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, count)
	store.Set(byteKey, bz)
}

// AppendDeviceJobData appends a deviceJobData in the store with a new id and updates the count.
func (k Keeper) AppendDeviceJobData(
	ctx sdk.Context,
	deviceJobData types.DeviceJobData,
) uint64 {

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DeviceJobDataKey))
	appendedValue := k.cdc.MustMarshal(&deviceJobData)
	store.Set(GetDeviceJobDataIDBytes(deviceJobData.Id), appendedValue)

	return deviceJobData.Id
}

// SetDeviceJobData sets a specific deviceJobData in the store.
func (k Keeper) SetDeviceJobData(ctx sdk.Context, deviceJobData types.DeviceJobData) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DeviceJobDataKey))
	b := k.cdc.MustMarshal(&deviceJobData)
	store.Set(GetDeviceJobDataIDBytes(deviceJobData.Id), b)
}

// GetDeviceJobData returns a deviceJobData from its id.
func (k Keeper) GetDeviceJobData(ctx sdk.Context, id uint64) (val types.DeviceJobData, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DeviceJobDataKey))
	b := store.Get(GetDeviceJobDataIDBytes(id))
	if b == nil {
		return val, false
	}
	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// RemoveDeviceJobData removes a deviceJobData from the store.
func (k Keeper) RemoveDeviceJobData(ctx sdk.Context, id uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DeviceJobDataKey))
	store.Delete(GetDeviceJobDataIDBytes(id))
}

// GetAllDeviceJobData returns all deviceJobData. A PageRequest can be added to take use of pagination.
func (k Keeper) GetAllDeviceJobData(ctx sdk.Context) (list []types.DeviceJobData) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DeviceJobDataKey))
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.DeviceJobData
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}

// GetDeviceJobDataIDBytes returns the byte representation of the id.
func GetDeviceJobDataIDBytes(id uint64) []byte {
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, id)
	return bz
}

// GetDeviceJobDataIDFromBytes returns id in uint64 format from a byte array.
func GetDeviceJobDataIDFromBytes(bz []byte) uint64 {
	return binary.BigEndian.Uint64(bz)
}
