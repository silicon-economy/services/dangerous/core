// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// DangerousGoodRegistrationAll returns all dangerous good registrations.
// A PageRequest can be added to take use of pagination.
func (k Keeper) DangerousGoodRegistrationAll(
	c context.Context,
	req *types.QueryAllDangerousGoodRegistrationRequest,
) (*types.QueryAllDangerousGoodRegistrationResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var dangerousGoodRegistrations []types.DangerousGoodRegistration
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	dangerousGoodRegistrationStore := prefix.NewStore(store, types.KeyPrefix(types.DangerousGoodRegistrationKey))

	pageRes, err := query.Paginate(
		dangerousGoodRegistrationStore,
		req.Pagination,
		func(key []byte, value []byte) error {
			var dangerousGoodRegistration types.DangerousGoodRegistration
			if err := k.cdc.Unmarshal(value, &dangerousGoodRegistration); err != nil {
				return err
			}

			dangerousGoodRegistrations = append(dangerousGoodRegistrations, dangerousGoodRegistration)
			return nil
		},
	)

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllDangerousGoodRegistrationResponse{
		DangerousGoodRegistration: dangerousGoodRegistrations,
		Pagination:                pageRes,
	}, nil
}

// DangerousGoodRegistration returns a dangerous good registration given by its id.
func (k Keeper) DangerousGoodRegistration(
	c context.Context,
	req *types.QueryGetDangerousGoodRegistrationRequest,
) (*types.QueryGetDangerousGoodRegistrationResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}
	ctx := sdk.UnwrapSDKContext(c)

	val, found := k.GetDangerousGoodRegistration(
		ctx,
		req.Id,
	)
	if !found {
		return nil, status.Error(codes.InvalidArgument, "not found")
	}

	return &types.QueryGetDangerousGoodRegistrationResponse{DangerousGoodRegistration: val}, nil
}
