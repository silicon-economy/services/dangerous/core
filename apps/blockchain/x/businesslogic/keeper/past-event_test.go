// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"

	keepertest "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/testutil/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/testutil/nullify"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func createNPastEvent(keeper *keeper.Keeper, ctx sdk.Context, n int) []types.PastEvent {
	items := make([]types.PastEvent, n)
	for i := range items {
		items[i].EventTypeAlert = &types.EventTypeAlert{
			JobId:    uint64(i),
			DeviceId: uint64(i),
		}
		items[i].Id = keeper.AppendPastEvent(ctx, items[i])
	}
	return items
}

func TestPastEventGet(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	items := createNPastEvent(keeper, ctx, 10)
	for _, item := range items {
		got, found := keeper.GetPastEvent(ctx, item.Id)
		require.True(t, found)
		require.Equal(t,
			nullify.Fill(&item),
			nullify.Fill(&got),
		)
	}
}

func TestPastEventRemove(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	items := createNPastEvent(keeper, ctx, 10)
	for _, item := range items {
		keeper.RemovePastEvent(ctx, item.Id)
		_, found := keeper.GetPastEvent(ctx, item.Id)
		require.False(t, found)
	}
}

func TestPastEventGetAll(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	items := createNPastEvent(keeper, ctx, 10)
	require.ElementsMatch(t,
		nullify.Fill(items),
		nullify.Fill(keeper.GetAllPastEvent(ctx)),
	)
}

func TestPastEventCount(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	items := createNPastEvent(keeper, ctx, 10)
	count := uint64(len(items))
	require.Equal(t, count, keeper.GetPastEventCount(ctx))
}
