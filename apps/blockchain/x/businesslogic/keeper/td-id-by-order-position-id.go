// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// SetTransportDocumentIdByOrderPositionId sets a specific transportDocumentIdByOrderPositionId in the store from its index.
func (k Keeper) SetTransportDocumentIdByOrderPositionId(
	ctx sdk.Context,
	transportDocumentIdByOrderPositionId types.TransportDocumentIdByOrderPositionId,
) {
	store := prefix.NewStore(
		ctx.KVStore(k.storeKey),
		types.KeyPrefix(types.TransportDocumentIdByOrderPositionIdKeyPrefix),
	)
	b := k.cdc.MustMarshal(&transportDocumentIdByOrderPositionId)
	store.Set(types.TransportDocumentIdByOrderPositionIdKey(
		transportDocumentIdByOrderPositionId.OrderPositionId,
	), b)
}

// GetTransportDocumentIdByOrderPositionId accepts an order position id
// and returns the corresponding id of the transport document where the order position is located.
func (k Keeper) GetTransportDocumentIdByOrderPositionId(
	ctx sdk.Context,
	index string,

) (val types.TransportDocumentIdByOrderPositionId, found bool) {
	store := prefix.NewStore(
		ctx.KVStore(k.storeKey),
		types.KeyPrefix(types.TransportDocumentIdByOrderPositionIdKeyPrefix),
	)

	b := store.Get(types.TransportDocumentIdByOrderPositionIdKey(
		index,
	))
	if b == nil {
		return val, false
	}

	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// RemoveTransportDocumentIdByOrderPositionId removes a transportDocumentIdByOrderPositionId from the store.
func (k Keeper) RemoveTransportDocumentIdByOrderPositionId(
	ctx sdk.Context,
	index string,

) {
	store := prefix.NewStore(
		ctx.KVStore(k.storeKey),
		types.KeyPrefix(types.TransportDocumentIdByOrderPositionIdKeyPrefix),
	)
	store.Delete(types.TransportDocumentIdByOrderPositionIdKey(
		index,
	))
}

// GetAllTransportDocumentIdByOrderPositionId returns all transportDocumentIdByOrderPositionId.
func (k Keeper) GetAllTransportDocumentIdByOrderPositionId(
	ctx sdk.Context,
) (list []types.TransportDocumentIdByOrderPositionId) {
	store := prefix.NewStore(
		ctx.KVStore(k.storeKey),
		types.KeyPrefix(types.TransportDocumentIdByOrderPositionIdKeyPrefix),
	)
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.TransportDocumentIdByOrderPositionId
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}
