// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func TestFetchAllTransportDocumentsFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	_, errorFromTransportDocument1 := srv.CreateTransportDocument(
		ctx,
		&types.MsgCreateTransportDocument{
			Creator:     creator,
			Consignor:   &types.Company{},
			Carrier:     &types.Carrier{},
			Freight:     &types.Freight{},
			Status:      "shipped",
			CreatedDate: 0,
			LastUpdate:  0,
		})
	require.NoError(t, errorFromTransportDocument1)

	_, errorFromTransportDocument2 := srv.CreateTransportDocument(
		ctx,
		&types.MsgCreateTransportDocument{
			Creator:     creator,
			Consignor:   &types.Company{},
			Carrier:     &types.Carrier{},
			Freight:     &types.Freight{},
			Status:      "sold",
			CreatedDate: 1,
			LastUpdate:  1,
		})
	require.NoError(t, errorFromTransportDocument2)

	responseFromFetch, errorFromFetch := srv.FetchAllTransportDocuments(
		ctx,
		&types.MsgFetchAllTransportDocuments{
			Creator: creator,
		})
	require.NoError(t, errorFromFetch)
	assert.NotNil(t, responseFromFetch.TransportDocument)
	assert.Len(t, responseFromFetch.TransportDocument, 2)
}

func TestFetchAllTransportDocumentsNotFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	response, err := srv.FetchAllTransportDocuments(
		ctx,
		&types.MsgFetchAllTransportDocuments{
			Creator: creator,
		})

	require.NoError(t, err)
	assert.Empty(t, response)
}

func TestFetchAllTransportDocumentsIsNil(t *testing.T) {
	srv, ctx := SetupMsgServer(t)

	response, err := srv.FetchAllTransportDocuments(
		ctx,
		nil,
	)

	require.Error(t, err)
	assert.Nil(t, response)
}
