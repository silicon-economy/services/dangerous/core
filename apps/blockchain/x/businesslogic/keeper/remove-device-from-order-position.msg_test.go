// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func TestRemoveDeviceFromOrderPosition(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	// Mock function 'AddNewJobToIoTBroker'
	keeper.SendFinishPuckConfigVar = func(deviceId, orderPositionId uint64) {
		fmt.Printf("FinishPuckConfig was sent for job with job id %d \n", orderPositionId)
	}

	_, err := srv.CreateTransportDocument(ctx, &types.MsgCreateTransportDocument{
		Creator:   creator,
		Consignor: &types.Company{},
		Carrier:   &types.Carrier{},
		Freight: &types.Freight{
			Orders: []*types.Order{
				{
					Consignee: &types.Company{
						Name: "Funke AG - Lacke und Farben ",
						Address: &types.Address{
							Street:     "Columbiadamm",
							Number:     "194",
							PostalCode: "10965",
							City:       "Berlin",
							Country:    "Deutschland",
						},
						Contact: &types.ContactPerson{
							Name:       "Dr. Martina Zünd",
							Phone:      "0049 741 852 063 9",
							Mail:       "Zuend@Funke.de",
							Department: "Abt. 4A",
						},
					},
					OrderPositions: []*types.OrderPosition{
						{
							DangerousGood: &types.DangerousGood{
								UnNumber:              "1230",
								CasNumber:             "67-56-1",
								Description:           "METHANOL",
								Label1:                "3",
								Label2:                "6.1",
								Label3:                "",
								PackingGroup:          "II",
								TunnelRestrictionCode: "(D/E)",
								TransportCategory:     "2",
							},
							Package:          "Großpackmittel (IBC) aus starrer Kunststoff (H) für flüssige Stoffe, mit äußerer Umhüllung aus Stahl (A)",
							PackagingCode:    "31HA1",
							Quantity:         1,
							Unit:             "L",
							IndividualAmount: 1000,
							Polluting:        false,
							TransportPoints:  3000,
							TotalAmount:      1000,
							DeviceId:         123,
						},
					},
				},
			},
			Load:                       "",
			AdditionalInformation:      "Beförderung ohne Freistellung nach ADR 1.1.3.6",
			TransportationInstructions: "Beachtung der höchstzulässigen anwendbaren Stapellast gem. ADR 6.5.2.2.2",
			TotalTransportPoints:       3000,
		},
		LogEntries: []*types.LogEntry{
			{
				Status:             "created",
				Date:               1629372434571,
				Author:             "",
				Description:        "",
				AcceptanceCriteria: nil,
			},
			{
				Status:      "accepted",
				Date:        1629372458465,
				Author:      "",
				Description: "",
				AcceptanceCriteria: &types.AcceptanceCriteria{
					Comment: "",
					AcceptanceCriteria: &types.AcceptanceCriteria_CarrierCheckCriteria{
						CarrierCheckCriteria: &types.CarrierCheckCriteria{
							AcceptAdditionalInformation:      true,
							AcceptConsignees:                 []bool{true},
							AcceptFreight:                    true,
							AcceptConsignor:                  true,
							AcceptTransportationInstructions: true},
					},
				},
			},
			{
				Status:             "released",
				Date:               1629372468326,
				Author:             "",
				Description:        "",
				AcceptanceCriteria: nil,
			},
			{
				Status:             "transport",
				Date:               1629372601559,
				Author:             "Bernd Beförderer",
				Description:        "",
				AcceptanceCriteria: nil,
			},
		},
		Status:      "",
		CreatedDate: 0,
		LastUpdate:  0,
	})
	if err != nil {
		return
	}

	_, err = srv.RemoveDeviceFromOrderPosition(
		ctx, &types.MsgRemoveDeviceFromOrderPosition{
			Creator: creator, OrderPositionId: 11,
		},
	)

	require.NoError(t, err)
}
