// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// PastEventAll returns all pastEvent.
func (k Keeper) PastEventAll(c context.Context, req *types.QueryAllPastEventRequest) (*types.QueryAllPastEventResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var pastEvents []types.PastEvent
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	pastEventStore := prefix.NewStore(store, types.KeyPrefix(types.PastEventKey))

	pageRes, err := query.Paginate(pastEventStore, req.Pagination, func(key []byte, value []byte) error {
		var pastEvent types.PastEvent
		if err := k.cdc.Unmarshal(value, &pastEvent); err != nil {
			return err
		}

		pastEvents = append(pastEvents, pastEvent)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllPastEventResponse{PastEvent: pastEvents, Pagination: pageRes}, nil
}

// PastEvent returns a pastEvent for a given id.
func (k Keeper) PastEvent(c context.Context, req *types.QueryGetPastEventRequest) (*types.QueryGetPastEventResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	ctx := sdk.UnwrapSDKContext(c)
	pastEvent, found := k.GetPastEvent(ctx, req.Id)
	if !found {
		return nil, sdkerrors.ErrKeyNotFound
	}

	return &types.QueryGetPastEventResponse{PastEvent: pastEvent}, nil
}
