// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"encoding/binary"
	"fmt"
	"time"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// SetDangerousGoodRegistration set a specific dangerousGoodRegistration in the store from its index
func (k Keeper) SetDangerousGoodRegistration(
	ctx sdk.Context,
	dangerousGoodRegistration types.DangerousGoodRegistration,
) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DangerousGoodRegistrationKey))
	b := k.cdc.MustMarshal(&dangerousGoodRegistration)
	store.Set(GetDangerousGoodRegistrationIDBytes(dangerousGoodRegistration.Id), b)
}

// GetDangerousGoodRegistration returns a dangerousGoodRegistration from its index.
func (k Keeper) GetDangerousGoodRegistration(
	ctx sdk.Context,
	id string,

) (val types.DangerousGoodRegistration, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DangerousGoodRegistrationKey))
	b := store.Get(GetTransportDocumentIDBytes(id))
	if b == nil {
		return val, false
	}
	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// RemoveDangerousGoodRegistration removes a dangerousGoodRegistration from the store.
func (k Keeper) RemoveDangerousGoodRegistration(
	ctx sdk.Context,
	id string,

) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DangerousGoodRegistrationKey))
	store.Delete(GetDangerousGoodRegistrationIDBytes(id))
}

// GetAllDangerousGoodRegistration returns all dangerousGoodRegistration.
func (k Keeper) GetAllDangerousGoodRegistration(ctx sdk.Context) (list []types.DangerousGoodRegistration) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DangerousGoodRegistrationKey))
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.DangerousGoodRegistration
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}

// GetDangerousGoodRegistrationIDBytes returns the byte representation of the id.
func GetDangerousGoodRegistrationIDBytes(id string) []byte {
	return []byte(id)
}

// GetDangerousGoodRegistrationCount returns the number of dangerous good registrations.
func (k Keeper) GetDangerousGoodRegistrationCount(ctx sdk.Context) uint64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.DangerousGoodRegistrationCountKey)
	bz := store.Get(byteKey)

	// Count does not exist: no element
	if bz == nil {
		return uint64(len(k.GetAllDangerousGoodRegistration(ctx)) + 1)
	}

	// Parse bytes
	return binary.BigEndian.Uint64(bz)
}

// SetDangerousGoodRegistrationCount sets the total number of dangerous good registrations.
func (k Keeper) SetDangerousGoodRegistrationCount(ctx sdk.Context, count uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.DangerousGoodRegistrationCountKey)
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, count)
	store.Set(byteKey, bz)
}

// AppendDangerousGoodRegistration appends a new dangerous good registration to the KVStore.
// It generates the respective ids for the registration, its orders and its order positions.
func (k Keeper) AppendDangerousGoodRegistration(
	ctx sdk.Context,
	dangerousGoodRegistration types.DangerousGoodRegistration,
) types.DangerousGoodRegistration {
	// dangerousGoodRegistrationCount
	dangerousGoodRegistrationCount := k.GetDangerousGoodRegistrationCount(ctx)
	k.SetDangerousGoodRegistrationCount(ctx, dangerousGoodRegistrationCount+1)

	// Create the dangerousGoodRegistration id
	currentTime := time.Unix(int64(dangerousGoodRegistration.CreatedDate)/1000, 0)

	dangerousGoodRegistration.Id = fmt.Sprintf(
		types.DangerousGoodRegistrationIdFormat,
		types.DangerousGoodRegistrationIdPrefix,
		dangerousGoodRegistrationCount,
		currentTime.Year(),
		currentTime.Month(),
		currentTime.Day(),
	)

	// Set the IDs and save orderPositionId-to-transportDocumentId-mapping
	for _, order := range dangerousGoodRegistration.Freight.Orders {
		for _, orderPosition := range order.OrderPositions {
			// The id for orderPosition is a globally incremental number
			orderPositionCount := k.GetOrderPositionCount(ctx)
			orderPosition.Id = orderPositionCount
			// Update orderPositionCount
			k.SetOrderPositionCount(ctx, orderPositionCount)
		}
	}
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DangerousGoodRegistrationKey))
	appendedValue := k.cdc.MustMarshal(&dangerousGoodRegistration)
	store.Set(GetTransportDocumentIDBytes(dangerousGoodRegistration.Id), appendedValue)
	return dangerousGoodRegistration
}
