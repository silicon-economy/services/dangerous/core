// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// FetchTransportDocument returns a transport document given by its id.
func (k msgServer) FetchTransportDocument(
	goCtx context.Context,
	msg *types.MsgFetchTransportDocument,
) (*types.MsgFetchTransportDocumentResponse, error) {
	var request *types.QueryGetTransportDocumentRequest

	if msg != nil {
		request = &types.QueryGetTransportDocumentRequest{Id: msg.Id}
	}

	document, err := k.TransportDocument(goCtx, request)

	if err != nil {
		return nil, err
	}

	return &types.MsgFetchTransportDocumentResponse{TransportDocument: document.TransportDocument}, nil
}
