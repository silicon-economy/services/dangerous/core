// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"

	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// AddNewJobToIoTBrokerVar function was saved as a var, so it can be mocked in the test files-
var AddNewJobToIoTBrokerVar = AddNewJobToIoTBroker

// AddDeviceToOrderPosition links a dragon puck to a given order position
// and returns the updated transport document.
func (k msgServer) AddDeviceToOrderPosition(
	goCtx context.Context,
	msg *types.MsgAddDeviceToOrderPosition,
) (*types.MsgAddDeviceToOrderPositionResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	transportDocument, transportDocumentExists := k.GetTransportDocument(ctx, msg.TransportDocumentId)

	if !transportDocumentExists {
		return nil, sdkerrors.Wrap(
			sdkerrors.ErrKeyNotFound,
			fmt.Sprintf("key %s doesn't exist", msg.TransportDocumentId),
		)
	}
	var orderPositionFound = false
	for orderIndex := 0; orderIndex < len(transportDocument.Freight.Orders); orderIndex++ {
		if transportDocument.Freight.Orders[orderIndex].Id == msg.OrderId {
			for orderPositionIndex := 0; orderPositionIndex < len(transportDocument.Freight.Orders[orderIndex].OrderPositions); orderPositionIndex++ {
				if transportDocument.Freight.Orders[orderIndex].OrderPositions[orderPositionIndex].Id == msg.OrderPositionId {
					orderPositionFound = true
					transportDocument.Freight.Orders[orderIndex].OrderPositions[orderPositionIndex].DeviceId = msg.DeviceId
					transportDocument.LogEntries = append(transportDocument.LogEntries, &types.LogEntry{
						Status:      transportDocument.LogEntries[len(transportDocument.LogEntries)-1].Status,
						Date:        float64(msg.TimeStamp),
						Author:      types.MockAuthor,
						Description: types.PuckConnectedSuccessfullyDescriptionMessage(msg.DeviceId),
					})
					transportDocument.LastUpdate = float64(msg.TimeStamp)
				}
			}
		}
	}
	if !orderPositionFound {
		return nil, sdkerrors.Wrap(
			sdkerrors.ErrKeyNotFound,
			fmt.Sprintf("key %d doesn't exist", msg.OrderPositionId),
		)
	}

	k.SetTransportDocument(ctx, transportDocument)

	AddNewJobToIoTBrokerVar(msg.DeviceId, msg.OrderPositionId)

	return &types.MsgAddDeviceToOrderPositionResponse{TransportDocument: &transportDocument}, nil
}
