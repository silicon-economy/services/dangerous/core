// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func TestFetchAllDangerousGoodRegistrationFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	_, errorFromCreate1 := srv.CreateDangerousGoodRegistration(
		ctx,
		&types.MsgCreateDangerousGoodRegistration{
			Creator:     creator,
			Consignor:   &types.Company{},
			Freight:     &types.Freight{},
			CreatedDate: 0,
			LastUpdate:  0,
		})
	require.NoError(t, errorFromCreate1)

	_, errorFromCreate2 := srv.CreateDangerousGoodRegistration(
		ctx,
		&types.MsgCreateDangerousGoodRegistration{
			Creator:     creator,
			Consignor:   &types.Company{},
			Freight:     &types.Freight{},
			CreatedDate: 0,
			LastUpdate:  0,
		})
	require.NoError(t, errorFromCreate2)

	responseFromFetch, errorFromFetch := srv.FetchAllDangerousGoodRegistrations(
		ctx,
		&types.MsgFetchAllDangerousGoodRegistrations{
			Creator: creator,
		})
	require.NoError(t, errorFromFetch)
	assert.NotNil(t, responseFromFetch.DangerousGoodRegistrations)
	assert.Len(t, responseFromFetch.DangerousGoodRegistrations, 2)
}

func TestFetchAllDangerousGoodRegistrationNotFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	response, err := srv.FetchAllDangerousGoodRegistrations(
		ctx,
		&types.MsgFetchAllDangerousGoodRegistrations{
			Creator: creator,
		})

	require.NoError(t, err)
	assert.Empty(t, response)
}

func TestFetchAllDangerousGoodRegistrationIsNil(t *testing.T) {
	srv, ctx := SetupMsgServer(t)

	response, err := srv.FetchAllDangerousGoodRegistrations(
		ctx,
		nil,
	)

	require.Error(t, err)
	assert.Nil(t, response)
}
