// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func TestDangerousGoodRegistrationMsgServerCreate(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"
	for i := 0; i < 5; i++ {
		resp, err := srv.CreateDangerousGoodRegistration(ctx, &types.MsgCreateDangerousGoodRegistration{
			Creator:     creator,
			Consignor:   &types.Company{},
			Freight:     &types.Freight{},
			CreatedDate: 0,
			LastUpdate:  0,
		})
		require.NoError(t, err)
		assert.NotNil(t, resp.GetDangerousGoodRegistration().GetId())
	}
}

func TestDangerousGoodRegistrationMsgServerUpdate(t *testing.T) {
	creator := "A"

	for _, tc := range []struct {
		desc    string
		request *types.MsgUpdateDangerousGoodRegistration
		err     error
	}{
		{
			desc:    "Completed",
			request: &types.MsgUpdateDangerousGoodRegistration{Creator: creator},
		},
		{
			desc:    "IncorrectOwner",
			request: &types.MsgUpdateDangerousGoodRegistration{Creator: "B"},
			err:     sdkerrors.ErrUnauthorized,
		},
		{
			desc: "KeyNotFound",
			request: &types.MsgUpdateDangerousGoodRegistration{Creator: creator,
				Id: "10"},
			err: sdkerrors.ErrKeyNotFound,
		},
	} {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			srv, ctx := SetupMsgServer(t)
			response, err := srv.CreateDangerousGoodRegistration(ctx, &types.MsgCreateDangerousGoodRegistration{
				Creator:     creator,
				Consignor:   &types.Company{},
				Freight:     &types.Freight{},
				CreatedDate: 0,
				LastUpdate:  0,
			},
			)
			require.NoError(t, err)

			if tc.request.Id == "" {
				tc.request.Id = response.DangerousGoodRegistration.Id
			}

			_, err = srv.UpdateDangerousGoodRegistration(ctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestDangerousGoodRegistrationMsgServerDelete(t *testing.T) {
	creator := "A"

	for _, tc := range []struct {
		desc    string
		request *types.MsgDeleteDangerousGoodRegistration
		err     error
	}{
		{
			desc:    "Completed",
			request: &types.MsgDeleteDangerousGoodRegistration{Creator: creator},
		},
		{
			desc:    "Unauthorized",
			request: &types.MsgDeleteDangerousGoodRegistration{Creator: "B"},
			err:     sdkerrors.ErrUnauthorized,
		},
		{
			desc: "KeyNotFound",
			request: &types.MsgDeleteDangerousGoodRegistration{Creator: creator,
				Id: strconv.Itoa(100000),
			},
			err: sdkerrors.ErrKeyNotFound,
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			srv, ctx := SetupMsgServer(t)

			response, err := srv.CreateDangerousGoodRegistration(ctx, &types.MsgCreateDangerousGoodRegistration{
				Creator:     creator,
				Consignor:   &types.Company{},
				Freight:     &types.Freight{},
				CreatedDate: 0,
				LastUpdate:  0,
			})

			if tc.request.Id == "" {
				tc.request.Id = response.GetDangerousGoodRegistration().GetId()
			}
			require.NoError(t, err)
			_, err = srv.DeleteDangerousGoodRegistration(ctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}
