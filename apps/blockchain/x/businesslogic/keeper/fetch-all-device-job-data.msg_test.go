// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func TestFetchAllDeviceJobDataFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	_, err := srv.FetchAllDeviceJobData(
		ctx,
		&types.MsgFetchAllDeviceJobData{
			Creator: creator,
		})
	if err != nil {
		return
	}
}

func TestFetchAllDeviceJobDataNotFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	response, err := srv.FetchAllDeviceJobData(
		ctx,
		&types.MsgFetchAllDeviceJobData{
			Creator: creator,
		},
	)

	require.NoError(t, err)
	assert.Empty(t, response)
}

func TestFetchAllDeviceJobDataIsNil(t *testing.T) {
	srv, ctx := SetupMsgServer(t)

	response, err := srv.FetchAllDeviceJobData(
		ctx,
		nil,
	)

	require.Error(t, err)
	assert.Nil(t, response)
}
