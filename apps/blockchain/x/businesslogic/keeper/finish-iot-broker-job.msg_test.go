// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"fmt"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func TestFinishIoTBrokerJob(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	keeper.FinishPuckConfigPostVar = func(jobId uint64, url, contentType string, body io.Reader) {
		fmt.Printf("FinishPuckConfig was sent for job with job id %d \n", jobId)
	}

	resp, err := srv.FinishIoTBrokerJob(ctx, &types.MsgFinishIoTBrokerJob{
		Creator: creator, JobId: 11, DeviceId: 123,
	})
	require.NoError(t, err)
	assert.NotNil(t, resp)
}
