// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"time"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenKeeperPackage "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/keeper"
	walletKeeperPackage "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/keeper"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

// CreateDangerousGoodRegistration creates and saves a new dangerous good registration.
func (k msgServer) CreateDangerousGoodRegistration(
	goCtx context.Context,
	msg *types.MsgCreateDangerousGoodRegistration,
) (*types.MsgCreateDangerousGoodRegistrationResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	var dangerousGoodRegistration = types.DangerousGoodRegistration{
		Creator:     msg.Creator,
		Consignor:   msg.Consignor,
		Freight:     msg.Freight,
		CreatedDate: msg.CreatedDate,
		LastUpdate:  msg.LastUpdate,
	}

	response := k.AppendDangerousGoodRegistration(
		ctx,
		dangerousGoodRegistration,
	)

	// put reference into token manager

	// Create token with TransportDocument as tokenType and inside segment '0'
	tokenKeeper := tokenKeeperPackage.NewKeeper(k.cdc, k.tokenKeeper, k.memKey)
	tokenKeeper.AppendToken(
		ctx,
		msg.Creator,
		dangerousGoodRegistration.Id,
		time.Unix(int64(dangerousGoodRegistration.CreatedDate)/1000, 0).UTC().Format(time.RFC3339),
		types.DangerousGoodRegistrationTokenType,
		types.ChangeMessageCreated,
		"0",
	)

	// Create a TokenRef inside segment '0'
	walletKeeper := walletKeeperPackage.NewKeeper(k.cdc, k.walletKeeper, k.memKey)
	walletKeeper.AppendTokenRef(
		ctx,
		walletTypes.MsgCreateTokenRef{
			Creator:   msg.Creator,
			Id:        dangerousGoodRegistration.Id,
			ModuleRef: types.DangerousGoodRegistrationModuleRef,
			SegmentId: "0",
		},
	)

	return &types.MsgCreateDangerousGoodRegistrationResponse{
		DangerousGoodRegistration: &response,
	}, nil
}

// UpdateDangerousGoodRegistration updates a dangerous good registration.
func (k msgServer) UpdateDangerousGoodRegistration(
	goCtx context.Context,
	msg *types.MsgUpdateDangerousGoodRegistration,
) (*types.MsgUpdateDangerousGoodRegistrationResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	var dangerousGoodRegistration = types.DangerousGoodRegistration{
		Creator:     msg.Creator,
		Id:          msg.Id,
		Consignor:   msg.Consignor,
		Freight:     msg.Freight,
		CreatedDate: msg.CreatedDate,
		LastUpdate:  float64(msg.LastUpdate),
	}

	// Check if the value exists
	valFound, isFound := k.GetDangerousGoodRegistration(
		ctx,
		msg.Id,
	)

	if !isFound {
		return nil, sdkerrors.Wrap(sdkerrors.ErrKeyNotFound, "index not set")
	}

	// Checks if the msg creator is the same as the current owner
	if msg.Creator != valFound.Creator {
		return nil, sdkerrors.Wrap(sdkerrors.ErrUnauthorized, "incorrect owner")
	}

	k.SetDangerousGoodRegistration(ctx, dangerousGoodRegistration)
	response, _ := k.GetDangerousGoodRegistration(ctx, msg.Id)
	return &types.MsgUpdateDangerousGoodRegistrationResponse{
		DangerousGoodRegistration: &response,
	}, nil
}

// DeleteDangerousGoodRegistration removes a dangerous good registration from the world state.
func (k msgServer) DeleteDangerousGoodRegistration(
	goCtx context.Context,
	msg *types.MsgDeleteDangerousGoodRegistration,
) (*types.MsgDeleteDangerousGoodRegistrationResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Check if the value exists
	valFound, isFound := k.GetDangerousGoodRegistration(
		ctx,
		msg.Id,
	)
	if !isFound {
		return nil, sdkerrors.Wrap(sdkerrors.ErrKeyNotFound, "index not set")
	}

	// Checks if the msg creator is the same as the current owner
	if msg.Creator != valFound.Creator {
		return nil, sdkerrors.Wrap(sdkerrors.ErrUnauthorized, "incorrect owner")
	}

	k.RemoveDangerousGoodRegistration(
		ctx,
		msg.Id,
	)

	return &types.MsgDeleteDangerousGoodRegistrationResponse{}, nil
}
