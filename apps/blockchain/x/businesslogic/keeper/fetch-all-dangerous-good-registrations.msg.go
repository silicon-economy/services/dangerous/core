// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// FetchAllDangerousGoodRegistrations returns all dangerous good registrations stored in the blockchain.
func (k msgServer) FetchAllDangerousGoodRegistrations(
	goCtx context.Context,
	msg *types.MsgFetchAllDangerousGoodRegistrations,
) (*types.MsgFetchAllDangerousGoodRegistrationsResponse, error) {
	var request *types.QueryAllDangerousGoodRegistrationRequest

	if msg != nil {
		request = &types.QueryAllDangerousGoodRegistrationRequest{}
	}

	documents, err := k.DangerousGoodRegistrationAll(goCtx, request)

	if err != nil {
		return nil, err
	}

	return &types.MsgFetchAllDangerousGoodRegistrationsResponse{DangerousGoodRegistrations: documents.DangerousGoodRegistration}, nil
}
