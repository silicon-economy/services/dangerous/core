// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// RevertToGenesis reverts the world state to the state given by the genesis block.
func (k msgServer) RevertToGenesis(
	goCtx context.Context,
	msg *types.MsgRevertToGenesis,
) (*types.MsgRevertToGenesisResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// remove every current data in store
	for _, transportDocument := range k.GetAllTransportDocument(ctx) {
		k.RemoveTransportDocument(ctx, transportDocument.Id)
	}
	for _, dangerousGoodRegistration := range k.GetAllDangerousGoodRegistration(ctx) {
		k.RemoveDangerousGoodRegistration(ctx, dangerousGoodRegistration.Id)
	}

	// save all elements from default genesis into the store
	genesis := types.DefaultGenesis()

	for _, transportDocument := range genesis.TransportDocumentList {
		k.SetTransportDocument(ctx, transportDocument)
	}

	for _, dangerousGoodRegistration := range genesis.DangerousGoodRegistrationList {
		k.SetDangerousGoodRegistration(ctx, dangerousGoodRegistration)
	}

	// After setting genesis transport documents and dangerous good registrations again,
	// the counts are reset accordingly.
	k.SetTransportDocumentCount(ctx, uint64(len(k.GetAllTransportDocument(ctx))+1))
	k.SetDangerousGoodRegistrationCount(ctx, uint64(len(k.GetAllDangerousGoodRegistration(ctx))+1))

	for _, deviceJobData := range k.GetAllDeviceJobData(ctx) {
		var latestMessage = deviceJobData.DeviceMessages[len(deviceJobData.DeviceMessages)-1]
		SendFinishPuckConfig(latestMessage.DeviceId, latestMessage.JobId)
		k.RemoveDeviceJobData(ctx, deviceJobData.Id)
	}
	for _, elem := range genesis.DeviceJobDataList {
		k.AppendDeviceJobData(ctx, elem)
	}

	return &types.MsgRevertToGenesisResponse{}, nil
}
