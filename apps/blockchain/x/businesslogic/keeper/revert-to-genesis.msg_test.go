// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func Test_msgServer_RevertToGenesis(t *testing.T) {
	srv, ctx := SetupKeeper(t)
	creator := "A"
	goCtx := sdk.WrapSDKContext(ctx)

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))
	defer ts.Close()

	type fields struct {
		Keeper keeper.Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgRevertToGenesis
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgRevertToGenesisResponse
		wantErr bool
	}{
		{
			name:    "Completed",
			fields:  fields{Keeper: *srv},
			args:    args{goCtx: goCtx, msg: &types.MsgRevertToGenesis{Creator: creator}},
			want:    &types.MsgRevertToGenesisResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := keeper.NewMsgServerImpl(tt.fields.Keeper)
			got, err := k.RevertToGenesis(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.RevertToGenesis() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.RevertToGenesis() = %v, want %v", got, tt.want)
			}
		})
	}
}
