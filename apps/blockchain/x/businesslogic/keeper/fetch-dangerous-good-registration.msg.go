// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// FetchDangerousGoodRegistration returns all dangerous good registrations stored in the blockchain.
func (k msgServer) FetchDangerousGoodRegistration(
	goCtx context.Context,
	msg *types.MsgFetchDangerousGoodRegistration,
) (*types.MsgFetchDangerousGoodRegistrationResponse, error) {
	var request *types.QueryGetDangerousGoodRegistrationRequest

	if msg != nil {
		request = &types.QueryGetDangerousGoodRegistrationRequest{Id: msg.Id}
	}

	document, err := k.DangerousGoodRegistration(goCtx, request)

	if err != nil {
		return nil, err
	}

	return &types.MsgFetchDangerousGoodRegistrationResponse{DangerousGoodRegistration: document.DangerousGoodRegistration}, nil
}
