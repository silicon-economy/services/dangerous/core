// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func TestFetchTransportDocumentIdFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	responseFromTransportDocument, errorFromTransportDocument := srv.CreateTransportDocument(
		ctx,
		&types.MsgCreateTransportDocument{
			Creator:   creator,
			Consignor: &types.Company{},
			Carrier:   &types.Carrier{},
			Freight: &types.Freight{
				Orders: []*types.Order{
					{
						Consignee: &types.Company{},
						OrderPositions: []*types.OrderPosition{
							{
								DangerousGood: &types.DangerousGood{},
								DeviceId:      123,
							},
						},
					},
				},
			}})
	require.NoError(t, errorFromTransportDocument)

	responseFromFetch, errorFromFetch := srv.FetchTransportDocumentIdByOrderPositionId(
		ctx,
		&types.MsgFetchTransportDocumentIdByOrderPositionId{
			Creator:         creator,
			OrderPositionId: strconv.FormatUint(responseFromTransportDocument.TransportDocument.Freight.Orders[0].OrderPositions[0].Id, 10),
		})
	require.NoError(t, errorFromFetch)
	assert.NotNil(t, responseFromFetch.TransportDocumentId)
}

func TestFetchTransportDocumentIdNotFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	response, err := srv.FetchTransportDocumentIdByOrderPositionId(
		ctx,
		&types.MsgFetchTransportDocumentIdByOrderPositionId{
			Creator:         creator,
			OrderPositionId: "123",
		})
	require.Error(t, err)
	assert.Nil(t, response)
}

func TestFetchTransportDocumentIdIsNil(t *testing.T) {
	srv, ctx := SetupMsgServer(t)

	response, err := srv.FetchTransportDocumentIdByOrderPositionId(
		ctx,
		nil,
	)
	require.Error(t, err)
	assert.Nil(t, response)
}
