// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"
	"time"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"

	tokenKeeperPackage "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/keeper"
	walletKeeperPackage "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/keeper"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

// CreateTransportDocument creates and saves a new transport document.
func (k msgServer) CreateTransportDocument(
	goCtx context.Context,
	msg *types.MsgCreateTransportDocument,
) (*types.MsgCreateTransportDocumentResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	var transportDocument = types.TransportDocument{
		Creator:     msg.Creator,
		Consignor:   msg.Consignor,
		Freight:     msg.Freight,
		Carrier:     msg.Carrier,
		LogEntries:  msg.LogEntries,
		Status:      msg.Status,
		CreatedDate: msg.CreatedDate,
		LastUpdate:  msg.LastUpdate,
	}

	response := k.AppendTransportDocument(
		ctx,
		transportDocument,
	)

	// put reference into token manager

	// Create token with TransportDocument as tokenType and inside segment '0'
	tokenKeeper := tokenKeeperPackage.NewKeeper(k.cdc, k.tokenKeeper, k.memKey)
	tokenKeeper.AppendToken(
		ctx,
		msg.Creator,
		transportDocument.Id,
		time.Unix(int64(msg.CreatedDate)/1000, 0).UTC().Format(time.RFC3339),
		types.TransportDocumentTokenType,
		types.ChangeMessageCreated,
		"0",
	)

	// Create a TokenRef inside segment '0'
	walletKeeper := walletKeeperPackage.NewKeeper(k.cdc, k.walletKeeper, k.memKey)
	walletKeeper.AppendTokenRef(
		ctx,
		walletTypes.MsgCreateTokenRef{
			Creator:   msg.Creator,
			Id:        transportDocument.Id,
			ModuleRef: types.TransportDocumentModuleRef,
			SegmentId: "0",
		},
	)

	return &types.MsgCreateTransportDocumentResponse{
		TransportDocument: &response,
	}, nil
}

// UpdateTransportDocument updates a transport document and returns the updated version.
func (k msgServer) UpdateTransportDocument(
	goCtx context.Context,
	msg *types.MsgUpdateTransportDocument,
) (*types.MsgUpdateTransportDocumentResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	var transportDocument = types.TransportDocument{
		Creator:     msg.Creator,
		Id:          msg.Id,
		Consignor:   msg.Consignor,
		Freight:     msg.Freight,
		Carrier:     msg.Carrier,
		LogEntries:  msg.LogEntries,
		Status:      msg.Status,
		CreatedDate: msg.CreatedDate,
		LastUpdate:  msg.LastUpdate,
	}

	// Checks that the element exists
	val, found := k.GetTransportDocument(ctx, msg.Id)
	if !found {
		return nil, sdkerrors.Wrap(sdkerrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	// Checks if the msg creator is the same as the current owner
	if msg.Creator != val.Creator {
		return nil, sdkerrors.Wrap(sdkerrors.ErrUnauthorized, "incorrect owner")
	}

	k.SetTransportDocument(ctx, transportDocument)
	response, _ := k.GetTransportDocument(ctx, msg.Id)

	return &types.MsgUpdateTransportDocumentResponse{TransportDocument: &response}, nil
}

// DeleteTransportDocument removes a transport document from the world state.
func (k msgServer) DeleteTransportDocument(
	goCtx context.Context,
	msg *types.MsgDeleteTransportDocument,
) (*types.MsgDeleteTransportDocumentResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Checks that the element exists
	val, found := k.GetTransportDocument(ctx, msg.Id)
	if !found {
		return nil, sdkerrors.Wrap(sdkerrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	// Checks if the msg creator is the same as the current owner
	if msg.Creator != val.Creator {
		return nil, sdkerrors.Wrap(sdkerrors.ErrUnauthorized, "incorrect owner")
	}

	k.RemoveTransportDocument(ctx, msg.Id)

	return &types.MsgDeleteTransportDocumentResponse{}, nil
}
