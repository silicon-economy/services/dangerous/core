// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"

	keepertest "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/testutil/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func createNTransportDocumentIdByOrderPositionId(
	keeper *keeper.Keeper,
	ctx sdk.Context,
	n int,
) []types.TransportDocumentIdByOrderPositionId {
	items := make([]types.TransportDocumentIdByOrderPositionId, n)
	for i := range items {
		items[i].OrderPositionId = strconv.Itoa(i)

		keeper.SetTransportDocumentIdByOrderPositionId(ctx, items[i])
	}
	return items
}

func TestTransportDocumentIdByOrderPositionIdGet(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	items := createNTransportDocumentIdByOrderPositionId(keeper, ctx, 10)
	for _, item := range items {
		rst, found := keeper.GetTransportDocumentIdByOrderPositionId(ctx,
			item.OrderPositionId,
		)
		require.True(t, found)
		require.Equal(t, item, rst)
	}
}
func TestTransportDocumentIdByOrderPositionIdRemove(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	items := createNTransportDocumentIdByOrderPositionId(keeper, ctx, 10)
	for _, item := range items {
		keeper.RemoveTransportDocumentIdByOrderPositionId(ctx,
			item.OrderPositionId,
		)
		_, found := keeper.GetTransportDocumentIdByOrderPositionId(ctx,
			item.OrderPositionId,
		)
		require.False(t, found)
	}
}

func TestTransportDocumentIdByOrderPositionIdGetAll(t *testing.T) {
	keeper, ctx := keepertest.DangerousblockchainKeeper(t)
	items := createNTransportDocumentIdByOrderPositionId(keeper, ctx, 10)
	require.ElementsMatch(t, items, keeper.GetAllTransportDocumentIdByOrderPositionId(ctx))
}
