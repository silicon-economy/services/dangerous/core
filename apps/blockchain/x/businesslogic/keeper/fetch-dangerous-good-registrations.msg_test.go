// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// Prevent strconv unused error
var _ = strconv.IntSize

func TestFetchDangerousGoodRegistrationFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	responseFromCreate, _ := srv.CreateDangerousGoodRegistration(
		ctx,
		&types.MsgCreateDangerousGoodRegistration{
			Creator:     creator,
			Consignor:   &types.Company{},
			Freight:     &types.Freight{},
			CreatedDate: 0,
			LastUpdate:  0,
		})

	responseFromFetch, errorFromFetch := srv.FetchDangerousGoodRegistration(
		ctx,
		&types.MsgFetchDangerousGoodRegistration{
			Creator: creator,
			Id:      responseFromCreate.DangerousGoodRegistration.Id,
		})

	require.NoError(t, errorFromFetch)
	assert.NotNil(t, responseFromFetch.DangerousGoodRegistration.Id)
}

func TestFetchDangerousGoodRegistrationNotFound(t *testing.T) {
	srv, ctx := SetupMsgServer(t)
	creator := "A"

	response, err := srv.FetchDangerousGoodRegistration(
		ctx,
		&types.MsgFetchDangerousGoodRegistration{
			Creator: creator,
			Id:      "0",
		})

	require.Error(t, err)
	assert.Nil(t, response)
}

func TestFetchDangerousGoodRegistrationIsNil(t *testing.T) {
	srv, ctx := SetupMsgServer(t)

	response, err := srv.FetchDangerousGoodRegistration(
		ctx,
		nil,
	)

	require.Error(t, err)
	assert.Nil(t, response)
}
