// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// FetchAllDeviceJobData return all DeviceJobData.
func (k msgServer) FetchAllDeviceJobData(
	goCtx context.Context,
	msg *types.MsgFetchAllDeviceJobData,
) (*types.MsgFetchAllDeviceJobDataResponse, error) {
	var request *types.QueryAllDeviceJobDataRequest

	if msg != nil {
		request = &types.QueryAllDeviceJobDataRequest{}
	}

	documents, err := k.DeviceJobDataAll(goCtx, request)

	if err != nil {
		return nil, err
	}

	return &types.MsgFetchAllDeviceJobDataResponse{DeviceJobData: documents.DeviceJobData}, nil
}
