// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// FinishIoTBrokerJob finishes an IoTBrokerJob.
func (k msgServer) FinishIoTBrokerJob(
	goCtx context.Context,
	msg *types.MsgFinishIoTBrokerJob,
) (*types.MsgFinishIoTBrokerJobResponse, error) {

	SendFinishPuckConfig(msg.DeviceId, msg.JobId)

	return &types.MsgFinishIoTBrokerJobResponse{}, nil
}
