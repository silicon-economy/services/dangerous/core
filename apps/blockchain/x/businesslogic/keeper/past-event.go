// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"encoding/binary"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// GetPastEventCount get the total number of pastEvent.
func (k Keeper) GetPastEventCount(ctx sdk.Context) uint64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.PastEventCountKey)
	bz := store.Get(byteKey)

	// Count does not exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	return binary.BigEndian.Uint64(bz)
}

// SetPastEventCount sets the total number of pastEvent.
func (k Keeper) SetPastEventCount(ctx sdk.Context, count uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.PastEventCountKey)
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, count)
	store.Set(byteKey, bz)
}

// AppendPastEvent appends a pastEvent in the store with a new id and updates the count.
func (k Keeper) AppendPastEvent(
	ctx sdk.Context,
	pastEvent types.PastEvent,
) uint64 {
	deviceId := pastEvent.GetEventTypeAlert().GetDeviceId()
	jobId := pastEvent.GetEventTypeAlert().GetJobId()

	// Create the pastEvent
	pastEventCount := k.GetPastEventCount(ctx)

	// Set the ID of the appended value
	pastEvent.Id = pastEventCount

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.PastEventKey))

	allPastEvents := k.GetAllPastEvent(ctx)
	foundMatchingPastEvent := false
	for _, element := range allPastEvents {
		if element.GetEventTypeAlert().GetDeviceId() == deviceId &&
			element.GetEventTypeAlert().GetJobId() == jobId {
			foundMatchingPastEvent = true
			k.SetPastEvent(ctx, types.PastEvent{
				Id:             element.GetId(),
				Creator:        element.GetCreator(),
				EventTypeAlert: pastEvent.GetEventTypeAlert(),
			})
			break
		}

	}
	if !foundMatchingPastEvent {
		appendedValue := k.cdc.MustMarshal(&pastEvent)
		store.Set(GetPastEventIDBytes(pastEvent.Id), appendedValue)
	  k.SetPastEventCount(ctx, pastEventCount+1)
	}

	return pastEventCount
}

// SetPastEvent set a specific pastEvent in the store.
func (k Keeper) SetPastEvent(ctx sdk.Context, pastEvent types.PastEvent) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.PastEventKey))
	b := k.cdc.MustMarshal(&pastEvent)
	store.Set(GetPastEventIDBytes(pastEvent.Id), b)
}

// GetPastEvent returns a pastEvent from its id.
func (k Keeper) GetPastEvent(ctx sdk.Context, id uint64) (val types.PastEvent, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.PastEventKey))
	b := store.Get(GetPastEventIDBytes(id))
	if b == nil {
		return val, false
	}
	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// RemovePastEvent removes a pastEvent from the store.
func (k Keeper) RemovePastEvent(ctx sdk.Context, id uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.PastEventKey))
	store.Delete(GetPastEventIDBytes(id))
}

// GetAllPastEvent returns all pastEvent.
func (k Keeper) GetAllPastEvent(ctx sdk.Context) (list []types.PastEvent) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.PastEventKey))
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.PastEvent
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}

// GetPastEventIDBytes returns the byte representation of the id.
func GetPastEventIDBytes(id uint64) []byte {
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, id)
	return bz
}

// GetPastEventIDFromBytes returns id in uint64 format from a byte array.
func GetPastEventIDFromBytes(bz []byte) uint64 {
	return binary.BigEndian.Uint64(bz)
}
