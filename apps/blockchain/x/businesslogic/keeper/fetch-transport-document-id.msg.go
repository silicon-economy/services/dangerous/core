// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// FetchTransportDocumentIdByOrderPositionId accepts an order position id
// and returns the corresponding id of the transport document where the order position is located.
func (k msgServer) FetchTransportDocumentIdByOrderPositionId(
	goCtx context.Context,
	msg *types.MsgFetchTransportDocumentIdByOrderPositionId,
) (*types.MsgFetchTransportDocumentIdByOrderPositionIdResponse, error) {
	var request *types.QueryGetTransportDocumentIdByOrderPositionIdRequest

	if msg != nil {
		request = &types.QueryGetTransportDocumentIdByOrderPositionIdRequest{OrderPositionId: msg.OrderPositionId}
	}

	document, err := k.TransportDocumentIdByOrderPositionId(goCtx, request)

	if err != nil {
		return nil, err
	}

	return &types.MsgFetchTransportDocumentIdByOrderPositionIdResponse{TransportDocumentId: document.TransportDocumentId}, nil
}
