// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"
	"strconv"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

var SendFinishPuckConfigVar = SendFinishPuckConfig

// RemoveDeviceFromOrderPosition removes the link between a device and its corresponding order position.
func (k msgServer) RemoveDeviceFromOrderPosition(
	goCtx context.Context,
	msg *types.MsgRemoveDeviceFromOrderPosition,
) (*types.MsgRemoveDeviceFromOrderPositionResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	transportDocumentId, found := k.GetTransportDocumentIdByOrderPositionId(
		ctx,
		strconv.FormatUint(msg.GetOrderPositionId(), 10),
	)
	if !found {
		return nil, sdkerrors.Wrap(
			sdkerrors.ErrKeyNotFound,
			fmt.Sprintf("key %d doesn't exist", msg.GetOrderPositionId()),
		)
	}
	transportDocument, found := k.GetTransportDocument(ctx, transportDocumentId.TransportDocumentId)
	if !found {
		return nil, sdkerrors.Wrap(
			sdkerrors.ErrKeyNotFound,
			fmt.Sprintf("key %s doesn't exist", transportDocumentId.TransportDocumentId),
		)
	}
	var deviceId uint64
	for orderIndex := 0; orderIndex < len(transportDocument.GetFreight().GetOrders()); orderIndex++ {
		for orderPositionIndex := 0; orderPositionIndex < len(transportDocument.GetFreight().GetOrders()[orderIndex].GetOrderPositions()); orderPositionIndex++ {
			if transportDocument.GetFreight().GetOrders()[orderIndex].GetOrderPositions()[orderPositionIndex].GetId() == msg.GetOrderPositionId() {
				date := float64(msg.TimeStamp)
				deviceId = transportDocument.GetFreight().GetOrders()[orderIndex].GetOrderPositions()[orderPositionIndex].GetDeviceId()
				transportDocument.LogEntries = append(transportDocument.LogEntries, &types.LogEntry{
					Status:      transportDocument.LogEntries[len(transportDocument.LogEntries)-1].Status,
					Date:        date,
					Author:      types.MockAuthor,
					Description: types.PuckDisconnectedSuccessfullyDescriptionMessage(deviceId),
				})
				transportDocument.GetFreight().GetOrders()[orderIndex].GetOrderPositions()[orderPositionIndex].DeviceId = 0
				transportDocument.LastUpdate = date
			}
		}
	}
	k.SetTransportDocument(ctx, transportDocument)
	SendFinishPuckConfigVar(deviceId, msg.GetOrderPositionId())
	return &types.MsgRemoveDeviceFromOrderPositionResponse{}, nil
}
