// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// FetchAllTransportDocuments returns all transport documents stored in the blockchain.
func (k msgServer) FetchAllTransportDocuments(
	goCtx context.Context,
	msg *types.MsgFetchAllTransportDocuments,
) (*types.MsgFetchAllTransportDocumentsResponse, error) {
	var request *types.QueryAllTransportDocumentRequest

	if msg != nil {
		request = &types.QueryAllTransportDocumentRequest{}
	}

	documents, err := k.TransportDocumentAll(goCtx, request)

	if err != nil {
		return nil, err
	}

	return &types.MsgFetchAllTransportDocumentsResponse{TransportDocument: documents.TransportDocument}, nil
}
