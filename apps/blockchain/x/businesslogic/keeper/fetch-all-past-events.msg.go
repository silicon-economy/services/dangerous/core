// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// FetchAllPastEvents returns all past dragon puck events.
func (k msgServer) FetchAllPastEvents(
	goCtx context.Context,
	msg *types.MsgFetchAllPastEvents,
) (*types.MsgFetchAllPastEventsResponse, error) {
	var request *types.QueryAllPastEventRequest

	if msg != nil {
		request = &types.QueryAllPastEventRequest{}
	}

	documents, err := k.PastEventAll(goCtx, request)

	if err != nil {
		return nil, err
	}

	return &types.MsgFetchAllPastEventsResponse{PastEvent: documents.PastEvent}, nil
}
