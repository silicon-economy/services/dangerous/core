// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgFetchTransportDocumentIdByOrderPositionId{}

func NewMsgFetchTransportDocumentIdByOrderPositionId(
	creator string,
	orderPositionId string,
) *MsgFetchTransportDocumentIdByOrderPositionId {
	return &MsgFetchTransportDocumentIdByOrderPositionId{
		Creator:         creator,
		OrderPositionId: orderPositionId,
	}
}

func (msg *MsgFetchTransportDocumentIdByOrderPositionId) Route() string {
	return RouterKey
}

func (msg *MsgFetchTransportDocumentIdByOrderPositionId) Type() string {
	return "FetchTransportDocumentIdByOrderPositionId"
}

func (msg *MsgFetchTransportDocumentIdByOrderPositionId) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchTransportDocumentIdByOrderPositionId) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchTransportDocumentIdByOrderPositionId) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
