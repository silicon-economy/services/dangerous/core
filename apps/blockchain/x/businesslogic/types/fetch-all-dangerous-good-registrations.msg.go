// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgFetchAllDangerousGoodRegistrations{}

func NewMsgFetchAllDangerousGoodRegistrations(
	creator string,
) *MsgFetchAllDangerousGoodRegistrations {
	return &MsgFetchAllDangerousGoodRegistrations{
		Creator: creator,
	}
}

func (msg *MsgFetchAllDangerousGoodRegistrations) Route() string {
	return RouterKey
}

func (msg *MsgFetchAllDangerousGoodRegistrations) Type() string {
	return "FetchAllDangerousGoodRegistrations"
}

func (msg *MsgFetchAllDangerousGoodRegistrations) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchAllDangerousGoodRegistrations) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchAllDangerousGoodRegistrations) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
