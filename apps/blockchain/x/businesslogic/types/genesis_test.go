// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func TestGenesisState_Validate(t *testing.T) {
	for _, tc := range []struct {
		desc     string
		genState *types.GenesisState
		valid    bool
	}{
		{
			desc:     "default is valid",
			genState: types.DefaultGenesis(),
			valid:    true,
		},
		{
			desc: "valid genesis state",
			genState: &types.GenesisState{
				TransportDocumentIdByOrderPositionIdList: []types.TransportDocumentIdByOrderPositionId{
					{
						OrderPositionId: "0",
					},
					{
						OrderPositionId: "1",
					},
				},
				DangerousGoodRegistrationList: []types.DangerousGoodRegistration{
					{
						Id: "0",
					},
					{
						Id: "1",
					},
				},
				PastEventList: []types.PastEvent{
					{
						Id: 0,
					},
					{
						Id: 1,
					},
				},
				PastEventCount: 2,
				// this line is used by starport scaffolding # types/genesis/validField
			},
			valid: true,
		},
		{
			desc: "duplicated transportDocumentIdByOrderPositionId",
			genState: &types.GenesisState{
				TransportDocumentIdByOrderPositionIdList: []types.TransportDocumentIdByOrderPositionId{
					{
						OrderPositionId: "0",
					},
					{
						OrderPositionId: "0",
					},
				},
			},
			valid: false,
		},
		{
			desc: "duplicated dangerousGoodRegistration",
			genState: &types.GenesisState{
				DangerousGoodRegistrationList: []types.DangerousGoodRegistration{
					{
						Id: "0",
					},
					{
						Id: "0",
					},
				},
			},
			valid: false,
		},
		{
			desc: "duplicated pastEvent",
			genState: &types.GenesisState{
				PastEventList: []types.PastEvent{
					{
						Id: 0,
					},
					{
						Id: 0,
					},
				},
			},
			valid: false,
		},
		{
			desc: "invalid pastEvent count",
			genState: &types.GenesisState{
				PastEventList: []types.PastEvent{
					{
						Id: 1,
					},
				},
				PastEventCount: 0,
			},
			valid: false,
		},
		// this line is used by starport scaffolding # types/genesis/testcase
	} {
		t.Run(tc.desc, func(t *testing.T) {
			err := tc.genState.Validate()
			if tc.valid {
				require.NoError(t, err)
			} else {
				require.Error(t, err)
			}
		})
	}
}
