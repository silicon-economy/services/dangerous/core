// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

const (
	// ModuleName defines the module name
	ModuleName = "businesslogic"

	// StoreKey defines the primary module store key
	StoreKey = ModuleName

	// RouterKey is the message route for slashing
	RouterKey = ModuleName

	// QuerierRoute defines the module's query routing key
	QuerierRoute = ModuleName

	// MemStoreKey defines the in-memory store key
	MemStoreKey = "mem_dangerousblockchain"
)

func KeyPrefix(p string) []byte {
	return []byte(p)
}

const (
	TransportDocumentKey      = "TransportDocument-value-"
	TransportDocumentCountKey = "TransportDocument-count-"
	OrderPositionCountKey     = "OrderPosition-count-"
)

const (
	DangerousGoodRegistrationKey                   = "DangerousGoodRegistration-value-"
	DangerousGoodRegistrationCountKey              = "DangerousGoodRegistration-count-"
	DangerousGoodRegistrationOrderPositionCountKey = "DangerousGoodRegistrationOrderPosition-count-"
)

const (
	DeviceJobDataKey      = "DeviceJobData-value-"
	DeviceJobDataCountKey = "DeviceJobData-count-"
)

const (
	PastEventKey      = "PastEvent-value-"
	PastEventCountKey = "PastEvent-count-"
)
