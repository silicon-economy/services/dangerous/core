// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgFetchAllTransportDocuments{}

func NewMsgFetchAllTransportDocuments(
	creator string,
) *MsgFetchAllTransportDocuments {
	return &MsgFetchAllTransportDocuments{
		Creator: creator,
	}
}

func (msg *MsgFetchAllTransportDocuments) Route() string {
	return RouterKey
}

func (msg *MsgFetchAllTransportDocuments) Type() string {
	return "FetchAllTransportDocuments"
}

func (msg *MsgFetchAllTransportDocuments) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchAllTransportDocuments) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchAllTransportDocuments) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
