// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgUploadDeviceData{}

func NewMsgUploadDeviceData(
	creator string,
	jobId uint64,
	protocolVersion uint32,
	firmwareDescriptor FirmwareDescriptor,
	comTimestamp uint64,
	lastComCause string,
	data []*Data,
	deviceId uint64,
) *MsgUploadDeviceData {
	return &MsgUploadDeviceData{
		JobId:              jobId,
		Creator:            creator,
		ProtocolVersion:    protocolVersion,
		FirmwareDescriptor: &firmwareDescriptor,
		ComTimestamp:       comTimestamp,
		LastComCause:       lastComCause,
		Data:               data,
		DeviceId:           deviceId,
	}
}

func (msg *MsgUploadDeviceData) Route() string {
	return RouterKey
}

func (msg *MsgUploadDeviceData) Type() string {
	return "UploadDeviceData"
}

func (msg *MsgUploadDeviceData) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUploadDeviceData) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUploadDeviceData) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
