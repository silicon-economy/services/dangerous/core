// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateTransportDocument{}

func NewMsgCreateTransportDocument(
	creator string,
	consignor *Company,
	freight *Freight,
	carrier *Carrier,
	status string,
	createdDate float64,
) *MsgCreateTransportDocument {
	return &MsgCreateTransportDocument{
		Creator:     creator,
		Consignor:   consignor,
		Carrier:     carrier,
		Freight:     freight,
		LogEntries:  []*LogEntry{},
		Status:      status,
		CreatedDate: createdDate,
	}
}

func (msg *MsgCreateTransportDocument) Route() string {
	return RouterKey
}

func (msg *MsgCreateTransportDocument) Type() string {
	return "CreateTransportDocument"
}

func (msg *MsgCreateTransportDocument) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateTransportDocument) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateTransportDocument) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateTransportDocument{}

func NewMsgUpdateTransportDocument(
	creator string,
	id string,
	consignor *Company,
	carrier *Carrier,
	freight *Freight,
	logentries []*LogEntry,
	status string,
	createdDate float64,
	lastUpdate float64,
) *MsgUpdateTransportDocument {
	return &MsgUpdateTransportDocument{
		Id:          id,
		Creator:     creator,
		Consignor:   consignor,
		Carrier:     carrier,
		Freight:     freight,
		LogEntries:  logentries,
		Status:      status,
		CreatedDate: createdDate,
		LastUpdate:  lastUpdate,
	}
}

func (msg *MsgUpdateTransportDocument) Route() string {
	return RouterKey
}

func (msg *MsgUpdateTransportDocument) Type() string {
	return "UpdateTransportDocument"
}

func (msg *MsgUpdateTransportDocument) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateTransportDocument) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateTransportDocument) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgDeleteTransportDocument{}

func NewMsgDeleteTransportDocument(creator string, id string) *MsgDeleteTransportDocument {
	return &MsgDeleteTransportDocument{
		Id:      id,
		Creator: creator,
	}
}
func (msg *MsgDeleteTransportDocument) Route() string {
	return RouterKey
}

func (msg *MsgDeleteTransportDocument) Type() string {
	return "DeleteTransportDocument"
}

func (msg *MsgDeleteTransportDocument) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeleteTransportDocument) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeleteTransportDocument) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
