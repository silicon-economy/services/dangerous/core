// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

const (
	TypeMsgCreateDangerousGoodRegistration = "create_dangerous_good_registration"
	TypeMsgUpdateDangerousGoodRegistration = "update_dangerous_good_registration"
	TypeMsgDeleteDangerousGoodRegistration = "delete_dangerous_good_registration"
)

var _ sdk.Msg = &MsgCreateDangerousGoodRegistration{}

func NewMsgCreateDangerousGoodRegistration(
	creator string,
	consignor *Company,
	freight *Freight,
	createdDate float64,
) *MsgCreateDangerousGoodRegistration {
	return &MsgCreateDangerousGoodRegistration{
		Creator:     creator,
		Consignor:   consignor,
		Freight:     freight,
		CreatedDate: createdDate,
	}
}

func (msg *MsgCreateDangerousGoodRegistration) Route() string {
	return RouterKey
}

func (msg *MsgCreateDangerousGoodRegistration) Type() string {
	return TypeMsgCreateDangerousGoodRegistration
}

func (msg *MsgCreateDangerousGoodRegistration) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateDangerousGoodRegistration) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateDangerousGoodRegistration) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateDangerousGoodRegistration{}

func NewMsgUpdateDangerousGoodRegistration(
	creator string,
	id string,
	consignor *Company,
	freight *Freight,
	createdDate float64,
) *MsgUpdateDangerousGoodRegistration {
	return &MsgUpdateDangerousGoodRegistration{
		Id:          id,
		Creator:     creator,
		Consignor:   consignor,
		Freight:     freight,
		CreatedDate: createdDate,
	}
}

func (msg *MsgUpdateDangerousGoodRegistration) Route() string {
	return RouterKey
}

func (msg *MsgUpdateDangerousGoodRegistration) Type() string {
	return TypeMsgUpdateDangerousGoodRegistration
}

func (msg *MsgUpdateDangerousGoodRegistration) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateDangerousGoodRegistration) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateDangerousGoodRegistration) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgDeleteDangerousGoodRegistration{}

func NewMsgDeleteDangerousGoodRegistration(
	creator string,
	id string,

) *MsgDeleteDangerousGoodRegistration {
	return &MsgDeleteDangerousGoodRegistration{
		Creator: creator,
		Id:      id,
	}
}
func (msg *MsgDeleteDangerousGoodRegistration) Route() string {
	return RouterKey
}

func (msg *MsgDeleteDangerousGoodRegistration) Type() string {
	return TypeMsgDeleteDangerousGoodRegistration
}

func (msg *MsgDeleteDangerousGoodRegistration) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeleteDangerousGoodRegistration) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeleteDangerousGoodRegistration) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
