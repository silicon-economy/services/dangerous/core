// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgFetchAllDeviceJobData{}

func NewMsgFetchAllDeviceJobData(
	creator string,
) *MsgFetchAllDeviceJobData {
	return &MsgFetchAllDeviceJobData{
		Creator: creator,
	}
}

func (msg *MsgFetchAllDeviceJobData) Route() string {
	return RouterKey
}

func (msg *MsgFetchAllDeviceJobData) Type() string {
	return "FetchAllDeviceJobData"
}

func (msg *MsgFetchAllDeviceJobData) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchAllDeviceJobData) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchAllDeviceJobData) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
