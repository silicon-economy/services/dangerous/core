// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

const (
	JobStateActive   = "Active"
	JobStateFinished = "Finished"
)

var DefaultPuckConfiguration = PuckConfiguration{
	ProtocolVersion: 2,
	DataAmount:      10,
	ClearAlarm:      false,
	LedTimings: &LedTimings{
		ShowId:    12,
		ShowAlarm: 8,
		ShowTemp:  8,
	},
	MeasurementCycles: &MeasurementCycles{
		Storage:   20,
		Transport: 10,
	},
	JobState: JobStateActive,
	HumidityAlarm: &HumidityAlarm{
		LowerBound: 40,
		UpperBound: 60,
	},
	TemperatureAlarm: &TemperatureAlarm{
		LowerBound: 10,
		UpperBound: 25,
	},
	DeviceId: 0,
	JobId:    0,
}
