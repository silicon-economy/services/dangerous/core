// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"github.com/cosmos/cosmos-sdk/codec"
	cdctypes "github.com/cosmos/cosmos-sdk/codec/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/msgservice"
)

func RegisterCodec(cdc *codec.LegacyAmino) {
	cdc.RegisterConcrete(&MsgFetchTransportDocument{}, "businesslogic/FetchTransportDocument", nil)
	cdc.RegisterConcrete(&MsgFetchTransportDocumentIdByOrderPositionId{}, "businesslogic/FetchTransportDocumentIdByOrderPositionId", nil)
	cdc.RegisterConcrete(&MsgFetchAllTransportDocuments{}, "businesslogic/FetchAllTransportDocuments", nil)
	cdc.RegisterConcrete(&MsgFetchDeviceJobData{}, "businesslogic/FetchDeviceJobData", nil)
	cdc.RegisterConcrete(&MsgFetchAllDeviceJobData{}, "businesslogic/FetchAllDeviceJobData", nil)
	cdc.RegisterConcrete(&MsgFetchDangerousGoodRegistration{}, "businesslogic/FetchDangerousGoodRegistration", nil)
	cdc.RegisterConcrete(&MsgFetchAllDangerousGoodRegistrations{}, "businesslogic/FetchAllDangerousGoodRegistrations", nil)
	cdc.RegisterConcrete(&MsgFetchPastEvent{}, "businesslogic/FetchPastEvent", nil)
	cdc.RegisterConcrete(&MsgFetchAllPastEvents{}, "businesslogic/FetchAllPastEvents", nil)
	cdc.RegisterConcrete(&MsgCreateTransportDocument{}, "businesslogic/CreateTransportDocument", nil)
	cdc.RegisterConcrete(&MsgUpdateTransportDocument{}, "businesslogic/UpdateTransportDocument", nil)
	cdc.RegisterConcrete(&MsgDeleteTransportDocument{}, "businesslogic/DeleteTransportDocument", nil)
	cdc.RegisterConcrete(&MsgDeviceData{}, "businesslogic/DeviceData", nil)
	cdc.RegisterConcrete(&MsgUploadDeviceData{}, "businesslogic/UploadDeviceData", nil)
	cdc.RegisterConcrete(&MsgAddDeviceToOrderPosition{}, "businesslogic/AddDeviceToOrderPosition", nil)
	cdc.RegisterConcrete(&MsgRevertToGenesis{}, "businesslogic/RevertToGenesis", nil)
	cdc.RegisterConcrete(&MsgFinishIoTBrokerJob{}, "businesslogic/FinishIoTBrokerJob", nil)
	cdc.RegisterConcrete(&MsgRemoveDeviceFromOrderPosition{}, "businesslogic/RemoveDeviceFromOrderPosition", nil)
	cdc.RegisterConcrete(
		&MsgCreateDangerousGoodRegistration{},
		"businesslogic/CreateDangerousGoodRegistration",
		nil,
	)
	cdc.RegisterConcrete(
		&MsgUpdateDangerousGoodRegistration{},
		"businesslogic/UpdateDangerousGoodRegistration",
		nil,
	)
	cdc.RegisterConcrete(
		&MsgDeleteDangerousGoodRegistration{},
		"businesslogic/DeleteDangerousGoodRegistration",
		nil,
	)
	// this line is used by starport scaffolding # 2
}

func RegisterInterfaces(registry cdctypes.InterfaceRegistry) {
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgFetchTransportDocument{},
		&MsgFetchTransportDocumentIdByOrderPositionId{},
		&MsgFetchAllTransportDocuments{},
		&MsgCreateTransportDocument{},
		&MsgUpdateTransportDocument{},
		&MsgDeleteTransportDocument{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgFetchDeviceJobData{},
		&MsgFetchAllDeviceJobData{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgFetchPastEvent{},
		&MsgFetchAllPastEvents{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgDeviceData{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgUploadDeviceData{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgAddDeviceToOrderPosition{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgRevertToGenesis{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgFinishIoTBrokerJob{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgRemoveDeviceFromOrderPosition{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgFetchDangerousGoodRegistration{},
		&MsgFetchAllDangerousGoodRegistrations{},
		&MsgCreateDangerousGoodRegistration{},
		&MsgUpdateDangerousGoodRegistration{},
		&MsgDeleteDangerousGoodRegistration{},
	)
	// this line is used by starport scaffolding # 3

	msgservice.RegisterMsgServiceDesc(registry, &_Msg_serviceDesc)
}

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewProtoCodec(cdctypes.NewInterfaceRegistry())
)
