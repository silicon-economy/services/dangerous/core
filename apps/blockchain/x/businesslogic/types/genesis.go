// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"fmt"
)

// DefaultIndex is the default capability global index
const DefaultIndex uint64 = 1

// DefaultGenesis returns the default Capability genesis state
func DefaultGenesis() *GenesisState {
	return &GenesisState{
		TransportDocumentIdByOrderPositionIdList: []TransportDocumentIdByOrderPositionId{},
		DangerousGoodRegistrationList:            []DangerousGoodRegistration{},
		PastEventList:                            []PastEvent{},
		// this line is used by starport scaffolding # genesis/types/default
		DeviceJobDataList:     []DeviceJobData{},
		TransportDocumentList: []TransportDocument{},
	}
}

// Validate performs basic genesis state validation returning an error upon any
// failure.
func (gs GenesisState) Validate() error {
	// Check for duplicated ID in transportDocument
	transportDocumentIdMap := make(map[string]bool)
	for _, elem := range gs.TransportDocumentList {
		if _, ok := transportDocumentIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for transportDocument")
		}
		transportDocumentIdMap[elem.Id] = true
	}
	// Check for duplicated ID in deviceJobData
	deviceJobDataIdMap := make(map[uint64]bool)
	deviceJobDataCount := gs.GetDeviceJobDataCount()
	for _, elem := range gs.DeviceJobDataList {
		if _, ok := deviceJobDataIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for deviceJobData")
		}
		if elem.Id >= deviceJobDataCount {
			return fmt.Errorf("deviceJobData id should be lower or equal than the last id")
		}
		deviceJobDataIdMap[elem.Id] = true
	}
	// Check for duplicated index in transportDocumentIdByOrderPositionId
	transportDocumentIdByOrderPositionIdIndexMap := make(map[string]struct{})

	for _, elem := range gs.TransportDocumentIdByOrderPositionIdList {
		index := string(TransportDocumentIdByOrderPositionIdKey(elem.OrderPositionId))
		if _, ok := transportDocumentIdByOrderPositionIdIndexMap[index]; ok {
			return fmt.Errorf("duplicated index for transportDocumentIdByOrderPositionId")
		}
		transportDocumentIdByOrderPositionIdIndexMap[index] = struct{}{}
	}
	// Check for duplicated index in dangerousGoodRegistration
	dangerousGoodRegistrationIndexMap := make(map[string]struct{})

	for _, elem := range gs.DangerousGoodRegistrationList {
		if _, ok := dangerousGoodRegistrationIndexMap[elem.Id]; ok {
			return fmt.Errorf("duplicated index for dangerousGoodRegistration")
		}
		dangerousGoodRegistrationIndexMap[elem.Id] = struct{}{}
	}
	// Check for duplicated ID in pastEvent
	pastEventIdMap := make(map[uint64]bool)
	pastEventCount := gs.GetPastEventCount()
	for _, elem := range gs.PastEventList {
		if _, ok := pastEventIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for pastEvent")
		}
		if elem.Id >= pastEventCount {
			return fmt.Errorf("pastEvent id should be lower or equal than the last id")
		}
		pastEventIdMap[elem.Id] = true
	}
	// this line is used by starport scaffolding # genesis/types/validate

	return nil
}
