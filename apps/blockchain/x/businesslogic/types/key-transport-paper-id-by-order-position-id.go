// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import "encoding/binary"

var _ binary.ByteOrder

const (
	// TransportDocumentIdByOrderPositionIdKeyPrefix is the prefix to retrieve all TransportDocumentIdByOrderPositionId
	TransportDocumentIdByOrderPositionIdKeyPrefix = "TransportDocumentIdByOrderPositionId/value/"
)

// TransportDocumentIdByOrderPositionIdKey returns the store key to retrieve a TransportDocumentIdByOrderPositionId from the index fields
func TransportDocumentIdByOrderPositionIdKey(
	index string,
) []byte {
	var key []byte

	indexBytes := []byte(index)
	key = append(key, indexBytes...)
	key = append(key, []byte("/")...)

	return key
}
