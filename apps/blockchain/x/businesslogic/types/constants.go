// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import "strconv"

const (
	ApiEndpointBroker                  = "https://dragonpuck-connector-rest.apps.sele.iml.fraunhofer.de/v2/jobs"
	ChangeMessageCreated               = "CREATED"
	DangerousGoodRegistrationIdFormat  = "%s%06d-%d%d%d"
	DangerousGoodRegistrationIdPrefix  = "DGR"
	DangerousGoodRegistrationModuleRef = "DangerousGoodRegistration"
	DangerousGoodRegistrationTokenType = "DangerousGoodRegistration"
	IsProduction                       = "IS_PRODUCTION"
	LastComCauseAlarm                  = "Alarm"
	MockAuthor                         = "Bernd Beförderer"
	TransportDocumentIdFormat          = "%s%06d-%d%d%d"
	TransportDocumentIdPrefix          = "BP"
	TransportDocumentModuleRef         = "TransportDocument"
	TransportDocumentTokenType         = "TransportDocument"
)

func PuckConnectedSuccessfullyDescriptionMessage(deviceId uint64) string {
	return "Puck Nr." + strconv.FormatUint(
		deviceId,
		10,
	) + " wurde verknüpft."
}

func PuckDisconnectedSuccessfullyDescriptionMessage(deviceId uint64) string {
	return "Puck Nr." + strconv.FormatUint(
		deviceId,
		10,
	) + " wurde entkoppelt."
}
