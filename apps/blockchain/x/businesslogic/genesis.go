// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package businesslogic

import (
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// InitGenesis initializes the capability module's state from a provided genesis
// state.
func InitGenesis(ctx sdk.Context, k keeper.Keeper, genState types.GenesisState) {
	// Set all the transportDocument
	for _, elem := range genState.TransportDocumentList {
		k.SetTransportDocument(ctx, elem)
	}

	// Set all the deviceJobData
	for _, elem := range genState.DeviceJobDataList {
		k.SetDeviceJobData(ctx, elem)
	}

	// Set all the transportDocumentIdByOrderPositionId
	for _, elem := range genState.TransportDocumentIdByOrderPositionIdList {
		k.SetTransportDocumentIdByOrderPositionId(ctx, elem)
	}
	// Set all the dangerousGoodRegistration
	for _, elem := range genState.DangerousGoodRegistrationList {
		k.SetDangerousGoodRegistration(ctx, elem)
	}
	// Set all the pastEvent
	for _, elem := range genState.PastEventList {
		k.SetPastEvent(ctx, elem)
	}
	// Set pastEvent count
	k.SetPastEventCount(ctx, genState.PastEventCount)
	// Set deviceJobDataCount
	k.SetDeviceJobDataCount(ctx, genState.DeviceJobDataCount)
	// this line is used by starport scaffolding # genesis/module/init
}

// ExportGenesis returns the capability module's exported genesis.
func ExportGenesis(ctx sdk.Context, k keeper.Keeper) *types.GenesisState {
	genesis := types.DefaultGenesis()

	genesis.TransportDocumentList = k.GetAllTransportDocument(ctx)
	genesis.DeviceJobDataList = k.GetAllDeviceJobData(ctx)
	genesis.DeviceJobDataCount = k.GetDeviceJobDataCount(ctx)
	genesis.TransportDocumentIdByOrderPositionIdList = k.GetAllTransportDocumentIdByOrderPositionId(ctx)
	genesis.DangerousGoodRegistrationList = k.GetAllDangerousGoodRegistration(ctx)
	genesis.PastEventList = k.GetAllPastEvent(ctx)
	genesis.PastEventCount = k.GetPastEventCount(ctx)
	// this line is used by starport scaffolding # genesis/module/export

	return genesis
}
