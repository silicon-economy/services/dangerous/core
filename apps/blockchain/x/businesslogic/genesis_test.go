// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package businesslogic_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	keepertest "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/testutil/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func TestGenesis(t *testing.T) {
	genesisState := types.GenesisState{
		TransportDocumentList: []types.TransportDocument{
			{
				Id: "0",
			},
			{
				Id: "1",
			},
		},
		DeviceJobDataList: []types.DeviceJobData{
			{
				Id: 0,
			},
			{
				Id: 1,
			},
		},
		DeviceJobDataCount: 2,
		TransportDocumentIdByOrderPositionIdList: []types.TransportDocumentIdByOrderPositionId{
			{
				OrderPositionId: "0",
			},
			{
				OrderPositionId: "1",
			},
		},
		DangerousGoodRegistrationList: []types.DangerousGoodRegistration{
			{
				Id: "0",
			},
			{
				Id: "1",
			},
		},
		PastEventList: []types.PastEvent{
			{
				Id: 0,
			},
			{
				Id: 1,
			},
		},
		PastEventCount: 2,
		// this line is used by starport scaffolding # genesis/test/state
	}

	k, ctx := keepertest.DangerousblockchainKeeper(t)
	businesslogic.InitGenesis(ctx, *k, genesisState)
	got := businesslogic.ExportGenesis(ctx, *k)
	require.NotNil(t, got)

	require.Len(t, got.TransportDocumentList, len(genesisState.TransportDocumentList))
	require.Subset(t, genesisState.TransportDocumentList, got.TransportDocumentList)
	require.Len(t, got.DeviceJobDataList, len(genesisState.DeviceJobDataList))
	require.Subset(t, genesisState.DeviceJobDataList, got.DeviceJobDataList)
	require.Equal(t, genesisState.DeviceJobDataCount, got.DeviceJobDataCount)
	require.Len(
		t,
		got.TransportDocumentIdByOrderPositionIdList,
		len(genesisState.TransportDocumentIdByOrderPositionIdList),
	)
	require.Subset(
		t,
		genesisState.TransportDocumentIdByOrderPositionIdList,
		got.TransportDocumentIdByOrderPositionIdList,
	)
	require.ElementsMatch(t, genesisState.DangerousGoodRegistrationList, got.DangerousGoodRegistrationList)
	require.ElementsMatch(t, genesisState.PastEventList, got.PastEventList)
	require.Equal(t, genesisState.PastEventCount, got.PastEventCount)
	// this line is used by starport scaffolding # genesis/test/assert
}
