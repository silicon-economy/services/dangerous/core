{{- define "depRow" -}}
{{- range $i, $dep := . }}
{{ $dep.Name }} | {{ $dep.Version }} | {{ $dep.URL }} | {{ $dep.LicenceType }}
{{- end }}
{{- end -}}

Direct dependencies
{{ template "depRow" .Direct }}

{{ if .Indirect }}
Indirect dependencies
{{ template "depRow" .Indirect }}
{{ end }}