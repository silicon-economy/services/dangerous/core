// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

// Needs to be package `main` due to `go run` conventions!
package main

import (
	"embed"
	"go/build"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/html"
	"github.com/princjef/gomarkdoc"
	"github.com/princjef/gomarkdoc/lang"
	"github.com/princjef/gomarkdoc/logger"
)

//go:embed static
var Openapi embed.FS

func main() {
	// Create a renderer to output data
	out, err := gomarkdoc.NewRenderer()
	if err != nil {
		print(err)
	}

	// Get current directory
	wd, err := os.Getwd()
	if err != nil {
		print(err)
	}

	// Complement path
	wd = wd + "/x/businesslogic/"

	// Creates documentation file for each subpackage of `dangerousblockchain/`
	filepath.Walk(wd, func(path string, fileInfo fs.FileInfo, err error) error {
		if err == nil && fileInfo.IsDir() {
			buildPkg, err := build.ImportDir(path, build.ImportComment)
			if err != nil {
				print(err)
			}

			// Create a documentation package from the build representation of our
			// package.
			log := logger.New(logger.DebugLevel)
			pkg, err := lang.NewPackageFromBuild(
				log,
				buildPkg,
				lang.PackageWithUnexportedIncluded(),
				lang.PackageWithRepositoryOverrides(&lang.Repo{DefaultBranch: "main"}),
			)
			if err != nil {
				print(err)
			}

			// Write the documentation out to console.
			docs, err := out.Package(pkg)
			if err != nil {
				print(err)
			}

			fileBytes := []byte(docs)

			// Writes file as Markdown
			// err = os.WriteFile("./docs/docs.md", fileBytes, 0644)
			// if err != nil {
			// 	print(err)
			// }

			// Select css file
			opts := html.RendererOptions{Flags: html.CompletePage, CSS: "./static/docs.css"}
			renderer := html.NewRenderer(opts)

			// Writes file as HTML
			err = os.WriteFile("./gomarkdoc/"+fileInfo.Name()+".html", markdown.ToHTML(fileBytes, nil, renderer), 0644)
			if err != nil {
				print(err)
			}
		}
		return err
	})

}
