#!/bin/bash

#
# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
#
ENVFILE=${PWD}/.env
test -f $ENVFILE && source $ENVFILE || echo "Test failed"

PATH=$PATH:~/go/bin
export PATH

# turn on bash's job control
set -m

mkdir -p ~/.TokenManager

# Perform actions only if directory is actually empty
if [ -z "$(ls -A ~/.TokenManager)" ]; then
  echo "### Copy backed up files"
  cp -r /backup/. ~/.TokenManager
  chmod -R 777 ~/.TokenManager
  printf '### Backed up files copied: %s\n\n\n' "$(ls -la ~/.TokenManager)"
fi

# Start the primary process and put it in the background
TokenManagerd start --log_level "warn" &

# Start the helper process
# the my_helper_process might need to know how to wait on the
# primary process to start before it does its work and returns
sleep 20s #Wait for tendermint demon start

# Create account on demand
if TokenManagerd keys list | grep dangerous_user; then
  printf '### Account %s already exists\n\n\n' dangerous_user
else
  echo "### Create account dangerous_user"
  printf '%s\n' "$DANGEROUS_USER_MNEMONIC" | TokenManagerd keys add dangerous_user --recover || exit 1
  printf '### Account created\n\n\n'

  echo "### Send 1000 token from alice to dangerous_user"
  TokenManagerd tx bank send "$(TokenManagerd keys show alice -a)" "$(TokenManagerd keys show dangerous_user -a)" 1000token -y
  printf '### 1000 token sent\n\n\n'
fi

if TokenManagerd keys list | grep puck1; then
  printf '### Account %s already exists\n\n\n' puck1
else
  echo "### Create account puck1"
  printf '%s\n' "$PUCK1_MNEMONIC" | TokenManagerd keys add puck1 --recover || exit 1
  printf '### Account created\n\n\n'

  echo "### Send 1000 token from alice to puck1"
  TokenManagerd tx bank send "$(TokenManagerd keys show alice -a)" "$(TokenManagerd keys show puck1 -a)" 1000token -y
  printf '### 1000 token sent\n\n\n'
fi

if TokenManagerd keys list | grep puck2; then
  printf '### Account %s already exists\n\n\n' puck2
else
  echo "### Create account puck2"
  printf '%s\n' "$PUCK2_MNEMONIC" | TokenManagerd keys add puck2 --recover || exit 1
  printf '### Account created\n\n\n'

  echo "### Send 500 token from alice to puck2"
  TokenManagerd tx bank send "$(TokenManagerd keys show alice -a)" "$(TokenManagerd keys show puck2 -a)" 500token -y
  printf '### 500 token sent\n\n\n'
fi

if TokenManagerd keys list | grep puck3; then
  printf '### Account %s already exists\n\n\n' puck3
else
  echo "### Create account puck3"
  printf '%s\n' "$PUCK3_MNEMONIC" | TokenManagerd keys add puck3 --recover || exit 1
  printf '### Account created\n\n\n'

  echo "### Send 500 token from alice to puck3"
  TokenManagerd tx bank send "$(TokenManagerd keys show alice -a)" "$(TokenManagerd keys show puck3 -a)" 500token -y
  printf '### 500 token sent\n\n\n'
fi

if TokenManagerd keys list | grep puck4; then
  printf '### Account %s already exists\n\n\n' puck4
else
  echo "### Create account puck4"
  printf '%s\n' "$PUCK4_MNEMONIC" | TokenManagerd keys add puck4 --recover || exit 1
  printf '### Account created\n\n\n'

  echo "### Send 1000 token from alice to puck4"
  TokenManagerd tx bank send "$(TokenManagerd keys show alice -a)" "$(TokenManagerd keys show puck4 -a)" 1000token -y
  printf '### 1000 token sent\n\n\n'
fi



# now we bring the primary process back into the foreground
# and leave it there
fg %1
