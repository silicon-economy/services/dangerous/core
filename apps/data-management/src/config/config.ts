/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export default () => ({
  queuePrefix: process.env.DEPLOYMENT_ENVIRONMENT || '',
  amqpUrl: process.env.AMQP_URL || 'amqp://guest:guest@0.0.0.0:5672',
});
