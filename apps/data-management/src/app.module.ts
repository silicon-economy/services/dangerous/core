/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import config from '@config';
import { DataManagementModule } from '@dataManagementModule/data-management.module';
import { Queues } from '@amqp/queues.enum';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true, load: [config] }), DataManagementModule],
})
export class AppModule {
  constructor(@Inject(Queues.TENDERMINT_SERVICE) public readonly client: ClientProxy) {}

  onApplicationBootstrap() {
    void this.client.connect().then();
  }
}
