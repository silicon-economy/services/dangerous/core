/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { TransportDocumentController } from '@dataManagementModule/transport-document/controller/transport-document.controller';
import { TransportDocumentService } from '@dataManagementModule/transport-document/service/transport-document.service';
import { AmqpService } from '@amqp/service/amqp.service';
import { AmqpModule } from '@amqp/amqp.module';
import { Queues } from '@amqp/queues.enum';

describe('TransportDocumentController', () => {
  let transportDocumentController: TransportDocumentController;
  let transportDocumentService: TransportDocumentService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [TransportDocumentController],
      providers: [TransportDocumentService, AmqpService],
      imports: [
        AmqpModule.register(Queues.TENDERMINT_SERVICE),
        AmqpModule.register(Queues.BUSINESS_LOGIC),
        ConfigModule,
      ],
    }).compile();

    transportDocumentController = moduleRef.get<TransportDocumentController>(TransportDocumentController);
    transportDocumentService = moduleRef.get<TransportDocumentService>(TransportDocumentService);
  });

  it('should be defined', () => {
    expect(transportDocumentController).toBeDefined();
    expect(transportDocumentService).toBeDefined();
    expect(transportDocumentController.getTransportDocumentById).toBeDefined();
    expect(transportDocumentController.getTransportDocumentForConsigneeById).toBeDefined();
    expect(transportDocumentController.getAllTransportDocuments).toBeDefined();
    expect(transportDocumentController.getAllTransportDocumentsForConsignee).toBeDefined();
    expect(transportDocumentController.createTransportDocument).toBeDefined();
    expect(transportDocumentController.createAndReleaseTransportDocument).toBeDefined();
    expect(transportDocumentController.processCarrierCheck).toBeDefined();
    expect(transportDocumentController.releaseTransportDocument).toBeDefined();
    expect(transportDocumentController.processCarrierConfirmation).toBeDefined();
    expect(transportDocumentController.updateVisualInspectionCarrier).toBeDefined();
    expect(transportDocumentController.updateVisualInspectionConsignee).toBeDefined();
  });
});
