/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { TransportDocumentService } from '@dataManagementModule/transport-document/service/transport-document.service';
import { UpdateVisualInspectionCarrierDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import { UpdateVisualInspectionConsigneeDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { CarrierCheckRequestDto } from '@core/api-interfaces/lib/dtos/data-management';
import { TransportVehicleInspectionRequestDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-vehicle-inspection-request.dto';
import { RequestDocumentForConsignee } from '@dataManagementModule/transport-document/controller/interfaces/request.interface';
import { MessagePatternsDataManagement } from '@amqp/message-patterns.enum';
import { JwtDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto';

@Controller()
export class TransportDocumentController {
  constructor(private transportDocumentService: TransportDocumentService) {}

  /**
   * Fetch transport document by id.
   *
   * @param transportDocumentId - Transport document id
   * @returns Transport document
   */
  @MessagePattern(MessagePatternsDataManagement.GET_TRANSPORT_DOCUMENT)
  async getTransportDocumentById(transportDocumentId: string): Promise<SaveGoodsDataDto> {
    return this.transportDocumentService.parseTransportDocument(
      await this.transportDocumentService.getTransportDocument(transportDocumentId)
    );
  }

  /**
   * Fetch transport document id for consignee. The consignee will only receive information relevant for him.
   *
   * @param request - Request
   */
  @MessagePattern(MessagePatternsDataManagement.GET_TRANSPORT_DOCUMENT_FOR_CONSIGNEE)
  async getTransportDocumentForConsigneeById(request: RequestDocumentForConsignee): Promise<SaveGoodsDataDto> {
    return this.transportDocumentService.filterForConsignee(
      request.jwtDto,
      this.transportDocumentService.parseTransportDocument(
        await this.transportDocumentService.getTransportDocument(request.documentId)
      )
    );
  }

  /**
   * Fetch all transport documents.
   */
  @MessagePattern(MessagePatternsDataManagement.GET_ALL_TRANSPORT_DOCUMENTS)
  async getAllTransportDocuments(): Promise<SaveGoodsDataDto[]> {
    const transportDocuments: SaveGoodsDataDto[] = await this.transportDocumentService.getAllTransportDocuments();
    if (transportDocuments != undefined) {
      return transportDocuments.map((transportDocument) =>
        this.transportDocumentService.parseTransportDocument(transportDocument)
      );
    }
  }

  /**
   * Fetch all transport documents for consignee. The consignee will only receive information relevant for him.
   *
   * @param jwtDto - User information
   */
  @MessagePattern(MessagePatternsDataManagement.GET_ALL_TRANSPORT_DOCUMENTS_FOR_CONSIGNEE)
  async getAllTransportDocumentsForConsignee(jwtDto: JwtDto): Promise<SaveGoodsDataDto[]> {
    const transportDocuments: SaveGoodsDataDto[] = await this.transportDocumentService.getAllTransportDocuments();
    if (transportDocuments != undefined) {
      return transportDocuments
        .map((transportDocument) =>
          this.transportDocumentService.filterForConsignee(
            jwtDto,
            this.transportDocumentService.parseTransportDocument(transportDocument)
          )
        )
        .filter((transportDocument: SaveGoodsDataDto) => transportDocument != null);
    }
  }

  /**
   * Fetch a transport document by order position id.
   *
   * @param orderPositionId - Order position id
   */
  @MessagePattern(MessagePatternsDataManagement.GET_TRANSPORT_DOCUMENT_ID_BY_ORDER_POSITION_ID)
  async getTransportDocumentIdByOrderPositionId(orderPositionId: number): Promise<string> {
    return this.transportDocumentService.getTransportDocumentIdByOrderPositionId(orderPositionId);
  }

  /**
   * Creates a new transport document.
   *
   * @param document - Transport document
   */
  @MessagePattern(MessagePatternsDataManagement.CREATE_TRANSPORT_DOCUMENT)
  async createTransportDocument(document: SaveGoodsDataDto): Promise<SaveGoodsDataDto> {
    return this.transportDocumentService.saveTransportDocument(document);
  }

  /**
   * Creates and releases a new transport document.
   *
   * @param saveGoodsDataDto - Save good data dto
   */
  @MessagePattern(MessagePatternsDataManagement.CREATE_AND_RELEASE_TRANSPORT_DOCUMENT)
  async createAndReleaseTransportDocument(saveGoodsDataDto: SaveGoodsDataDto): Promise<SaveGoodsDataDto> {
    return this.transportDocumentService.parseTransportDocument(
      await this.transportDocumentService.createAndReleaseTransportDocument(saveGoodsDataDto)
    );
  }

  /**
   * Call used to deliver information of the visual inspection done by the carrier.
   *
   * @param updateVisualInspectionCarrierDto - Update visual inspection carrier dto
   */
  @MessagePattern(MessagePatternsDataManagement.UPDATE_VISUAL_INSPECTION_CARRIER)
  async updateVisualInspectionCarrier(
    updateVisualInspectionCarrierDto: UpdateVisualInspectionCarrierDto
  ): Promise<SaveGoodsDataDto> {
    return this.transportDocumentService.parseTransportDocument(
      await this.transportDocumentService.updateVisualInspectionCarrier(updateVisualInspectionCarrierDto)
    );
  }

  /**
   * Call used to deliver information of the visual inspection done by the consignee.
   *
   * @param updateVisualInspectionConsigneeDto - Update visual inspection consignee dto
   */
  @MessagePattern(MessagePatternsDataManagement.UPDATE_VISUAL_INSPECTION_CONSIGNEE)
  async updateVisualInspectionConsignee(
    updateVisualInspectionConsigneeDto: UpdateVisualInspectionConsigneeDto
  ): Promise<SaveGoodsDataDto> {
    return this.transportDocumentService.parseTransportDocument(
      await this.transportDocumentService.updateVisualInspectionConsignee(updateVisualInspectionConsigneeDto)
    );
  }

  /**
   * Update transport document.
   *
   * @param saveGoodsDataDto - Save goods data dto
   */
  @MessagePattern(MessagePatternsDataManagement.UPDATE_TRANSPORT_DOCUMENT)
  async updateTransportDocument(saveGoodsDataDto: SaveGoodsDataDto): Promise<SaveGoodsDataDto> {
    return this.transportDocumentService.parseTransportDocument(
      await this.transportDocumentService.updateTransportDocument(saveGoodsDataDto)
    );
  }

  /**
   * Delete transport document from world state.
   *
   * @param transportDocumentId - Transport document id
   */
  @MessagePattern(MessagePatternsDataManagement.DELETE_TRANSPORT_DOCUMENT_FROM_WORLD_STATE)
  async deleteTransportDocumentFromWorldState(transportDocumentId: string) {
    return await this.transportDocumentService.deleteTransportDocumentFromWorldState(transportDocumentId);
  }

  /**
   * Call used to deliver the check over the transport document, performed by the carrier.
   *
   * @param carrierCheckRequestDto - Carrier check request dto
   */
  @MessagePattern(MessagePatternsDataManagement.CARRIER_CHECK)
  async processCarrierCheck(carrierCheckRequestDto: CarrierCheckRequestDto): Promise<SaveGoodsDataDto> {
    return this.transportDocumentService.parseTransportDocument(
      await this.transportDocumentService.processCarrierCheck(carrierCheckRequestDto)
    );
  }

  /**
   * Call used for carrier confirmation at delivery arrival, performed by the consignee.
   *
   * @param request - Request
   */
  @MessagePattern(MessagePatternsDataManagement.CONFIRM_CARRIER)
  async processCarrierConfirmation(request: RequestDocumentForConsignee): Promise<SaveGoodsDataDto> {
    return this.transportDocumentService.parseTransportDocument(
      await this.transportDocumentService.processCarrierConfirmation(request.documentId, request.jwtDto)
    );
  }

  /**
   * Release transport document.
   *
   * @param transportDocumentId - Transport document id
   */
  @MessagePattern(MessagePatternsDataManagement.RELEASE_TRANSPORT_DOCUMENT)
  async releaseTransportDocument(transportDocumentId: string): Promise<SaveGoodsDataDto> {
    return this.transportDocumentService.parseTransportDocument(
      await this.transportDocumentService.releaseTransportDocument(transportDocumentId)
    );
  }

  /**
   * Process vehicle inspection, performed by the consignee.
   *
   * @param transportVehicleInspectionRequestDto - Transport vehicle inspection request dto
   */
  @MessagePattern(MessagePatternsDataManagement.VEHICLE_INSPECTION)
  async processVehicleInspection(
    transportVehicleInspectionRequestDto: TransportVehicleInspectionRequestDto
  ): Promise<SaveGoodsDataDto> {
    return this.transportDocumentService.parseTransportDocument(
      await this.transportDocumentService.processVehicleInspection(transportVehicleInspectionRequestDto)
    );
  }
}
