/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { JwtDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto';

/**
 * Interface for requesting transport documents for a given consignee.
 *
 * @param documentId - Transport document id
 * @param consigneeInformation - Consignee information
 */
export interface RequestDocumentForConsignee {
  documentId: string;
  jwtDto: JwtDto;
}
