/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  BadRequestException,
  ForbiddenException,
  Inject,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { isEqual } from 'lodash';
import hash from 'object-hash';
import * as ErrorStrings from '../../../resources/error.strings.json';
import { UpdateVisualInspectionCarrierDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import { UpdateVisualInspectionConsigneeDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';
import {
  QueryAllTransportDocumentResponse,
  QueryGetTransportDocumentIdByOrderPositionIdResponse,
  QueryGetTransportDocumentResponse,
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document-response.dto';
import {
  OrderPositionStatus,
  OrderStatus,
  TransportDocumentStatus,
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/status.enum';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order-with-id';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { LogEntry } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/log-entry';
import { Order } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order';
import { AcceptanceCriteria } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/acceptance-criteria/acceptance-criteria';
import { TransportVehicleInspectionRequestDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-vehicle-inspection-request.dto';
import { CarrierCheckRequestDto } from '@core/api-interfaces/lib/dtos/data-management';
import { OrderVisualInspectionCarrierDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-carrier/order-visual-inspection-carrier.dto';
import { OrderPositionVisualInspectionCarrierDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import { OrderVisualInspectionConsigneeDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-consignee/order-visual-inspection-consignee.dto';
import { Queues } from '@amqp/queues.enum';
import { MessagePatternsTendermintTransportDocument } from '@amqp/message-patterns.enum';
import { OrderPositionVisualInspectionConsigneeDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-consignee/order-position-visual-inspection-consignee.dto';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order-position-with-id';
import { AmqpService } from '@amqp/service/amqp.service';
import { CarrierCheckCriteria } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/acceptance-criteria/carrier-check-criteria';
import { TransportVehicleCriteria } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/acceptance-criteria/transport-vehicle-criteria';
import { JwtDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto';

@Injectable()
export class TransportDocumentService {
  private readonly logger = new Logger(TransportDocumentService.name);

  constructor(
    @Inject(Queues.TENDERMINT_SERVICE) public readonly client: ClientProxy,
    @Inject(Queues.BUSINESS_LOGIC) public readonly businessClient: ClientProxy,
    public readonly configService: ConfigService,
    public readonly amqpService: AmqpService
  ) {}

  /**
   * Returns the transport document with the id `documentId`.
   *
   * @param documentId - Transport document id
   * @returns Transport document
   */
  async getTransportDocument(documentId: string): Promise<SaveGoodsDataDto> {
    this.logger.debug('Send AQMP request to tendermintService to GET the transport document by its id');
    this.logger.verbose('transportDocumentId: ' + documentId);
    try {
      return (
        (await this.amqpService.sendToAmqpBroker(
          MessagePatternsTendermintTransportDocument.GET_TRANSPORT_DOCUMENT,
          documentId,
          QueryGetTransportDocumentResponse
        )) as QueryGetTransportDocumentResponse
      ).transportDocument;
    } catch (e) {
      this.logger.error(e);
      throw new NotFoundException(ErrorStrings.NOT_FOUND);
    }
  }

  /**
   * Returns all transport documents in descending order with 'lastUpdate' as a sortKey (i.e. most recently updated
   * transport document = first array entry).
   *
   * @returns Sorted list of all transport documents
   */
  async getAllTransportDocuments(): Promise<SaveGoodsDataDto[]> {
    this.logger.debug('Send AQMP request to tendermintService to GET all transport documents');
    try {
      const transportDocuments: SaveGoodsDataDto[] = (
        (await this.amqpService.sendToAmqpBroker(
          MessagePatternsTendermintTransportDocument.GET_ALL_TRANSPORT_DOCUMENTS,
          <null>{},
          QueryAllTransportDocumentResponse
        )) as QueryAllTransportDocumentResponse
      ).transportDocument;
      transportDocuments.sort(
        (transportDocumentA, transportDocumentB) => transportDocumentB.lastUpdate - transportDocumentA.lastUpdate
      );
      return transportDocuments;
    } catch (e) {
      this.logger.error(e);
      throw new NotFoundException('No transport document with that Id found in blockchain data storage.');
    }
  }

  /**
   * Returns the transport document id of the transport document which contains the order position with the given
   * order position id.
   *
   * @param orderPositionId - Order position id of an order position
   * @returns Returns the transport document id of the transport document which contains the order position
   */
  async getTransportDocumentIdByOrderPositionId(orderPositionId: number): Promise<string> {
    this.logger.debug(
      'Send AQMP request to tendermintService to GET the transport document by on of its orderPositionIds'
    );
    this.logger.verbose('orderPositionId: ' + orderPositionId.toString());
    try {
      return (
        (await this.amqpService.sendToAmqpBroker(
          MessagePatternsTendermintTransportDocument.GET_TRANSPORT_DOCUMENT_ID_BY_ORDER_POSITION_ID,
          orderPositionId,
          QueryGetTransportDocumentIdByOrderPositionIdResponse
        )) as QueryGetTransportDocumentIdByOrderPositionIdResponse
      ).transportDocumentId;
    } catch (e) {
      this.logger.error(e);
      throw new NotFoundException(ErrorStrings.NOT_FOUND_ORDER_POSITION_ID);
    }
  }

  /**
   * Saves a transport document to the blockchain.
   * After saving, the transport document id is set to the id given by the tendermint service.
   *
   * @param saveGoodsDataDto - A save goods data dto
   * @returns The saved transport document with its id
   */
  async saveTransportDocument(saveGoodsDataDto: SaveGoodsDataDto): Promise<SaveGoodsDataDto> {
    this.logger.debug(
      'Send AQMP request to tendermintService to SAVE the transport document by on of its orderPositionIds'
    );
    this.logger.verbose('saveGoodsDataDto: ' + JSON.stringify(saveGoodsDataDto));
    try {
      return (
        (await this.amqpService.sendToAmqpBroker(
          MessagePatternsTendermintTransportDocument.CREATE_TRANSPORT_DOCUMENT,
          saveGoodsDataDto,
          QueryGetTransportDocumentResponse
        )) as QueryGetTransportDocumentResponse
      ).transportDocument;
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(ErrorStrings.ERROR_SAVE);
    }
  }

  /**
   * Creates a transport document and updates its status to `released` immediately, adds the corresponding new log entries
   * and saves it to the blockchain.
   *
   * @param saveGoodsDataDto - Save goods data dto
   * @returns The created and released transport document
   */
  async createAndReleaseTransportDocument(saveGoodsDataDto: SaveGoodsDataDto): Promise<SaveGoodsDataDto> {
    try {
      const result: SaveGoodsDataDto = await this.saveTransportDocument(saveGoodsDataDto);
      if (result.status == TransportDocumentStatus.created) {
        this.logger.debug('Change transport document status to released');
        const logEntry = new LogEntry(TransportDocumentStatus.released, result.logEntries[0].date, undefined);
        result.status = TransportDocumentStatus.released;
        result.logEntries.push(logEntry);
        return this.updateTransportDocument(result);
      } else {
        throw new BadRequestException(ErrorStrings.ERROR_RELEASE);
      }
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(ErrorStrings.ERROR_SAVE);
    }
  }

  /**
   * Executes visual inspection (performed by the carrier) for a transport document and updates its status to `transport`, adds the corresponding
   * new log entries and saves it to the blockchain.
   *
   * @param updateVisualInspectionCarrierDto - Update order position check dto
   * @returns The transport document with status `transport`
   */
  async updateVisualInspectionCarrier(
    updateVisualInspectionCarrierDto: UpdateVisualInspectionCarrierDto
  ): Promise<SaveGoodsDataDto> {
    let allAcceptanceCriteriaTrue = true;
    let transportDocument: SaveGoodsDataDto;

    try {
      transportDocument = await this.getTransportDocument(updateVisualInspectionCarrierDto.transportDocumentId);
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(ErrorStrings.NOT_FOUND);
    }

    this.logger.debug('Start processing visual inspection');
    if (transportDocument.status == TransportDocumentStatus.vehicle_accepted) {
      const currentDate = Date.now();

      for (let orderIndex = 0; orderIndex < transportDocument.freight.orders.length; orderIndex++) {
        const currentOrder: OrderWithId = structuredClone(transportDocument.freight.orders[orderIndex]);
        let processedOrder: OrderWithId;
        const orderVisualInspectionCarrierDto: OrderVisualInspectionCarrierDto = this.getOrderCheckDtoById(
          currentOrder.id,
          updateVisualInspectionCarrierDto
        );

        [processedOrder, allAcceptanceCriteriaTrue] = this.processVisualInspectionCarrierPerOrder(
          currentOrder,
          orderVisualInspectionCarrierDto,
          allAcceptanceCriteriaTrue,
          currentDate
        );
        transportDocument.freight.orders[orderIndex] = processedOrder;
      }
      const logEntryTransport: LogEntry = this.createTransportLogEntry(allAcceptanceCriteriaTrue, currentDate);
      transportDocument.logEntries.push(logEntryTransport);
      transportDocument.status = logEntryTransport.status;
      transportDocument.lastUpdate = logEntryTransport.date;

      await this.updateTransportDocument(transportDocument);
    }
    return transportDocument;
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private processVisualInspectionCarrierPerOrder(
    order: OrderWithId,
    orderVisualInspectionCarrierDto: OrderVisualInspectionCarrierDto,
    allAcceptanceCriteriaTrue: boolean,
    currentDate: number
  ): [OrderWithId, boolean] {
    const orderToBeVisuallyInspected: OrderWithId = structuredClone(order);
    let allAcceptanceCriteriaTrueLocal: boolean = allAcceptanceCriteriaTrue;

    for (
      let orderPositionIndex = 0;
      orderPositionIndex < orderVisualInspectionCarrierDto.orderPositions.length;
      orderPositionIndex++
    ) {
      const currentOrderPosition: OrderPositionWithId = orderToBeVisuallyInspected.orderPositions[orderPositionIndex];
      const orderPositionCheckDto: OrderPositionVisualInspectionCarrierDto = this.getOrderPositionCheckDtoById(
        orderVisualInspectionCarrierDto.orderPositions[orderPositionIndex].orderPositionId,
        orderVisualInspectionCarrierDto
      );

      [orderToBeVisuallyInspected.orderPositions[orderPositionIndex], allAcceptanceCriteriaTrueLocal] =
        this.processVisualInspectionCarrierPerOrderPosition(
          orderPositionCheckDto,
          currentOrderPosition,
          allAcceptanceCriteriaTrueLocal,
          currentDate
        );
    }
    return [orderToBeVisuallyInspected, allAcceptanceCriteriaTrueLocal];
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private processVisualInspectionCarrierPerOrderPosition(
    orderPositionCheckDto: OrderPositionVisualInspectionCarrierDto,
    orderPosition: OrderPositionWithId,
    allAcceptanceCriteriaTrue: boolean,
    currentDate: number
  ): [OrderPositionWithId, boolean] {
    let allAcceptanceCriteriaTrueLocal: boolean = allAcceptanceCriteriaTrue;
    const orderPositionToBeVisuallyInspected: OrderPositionWithId = structuredClone(orderPosition);

    let orderPositionStatus = OrderPositionStatus.visual_inspection_carrier_accepted;
    if (!orderPositionCheckDto.acceptTransportability || !orderPositionCheckDto.acceptLabeling) {
      allAcceptanceCriteriaTrueLocal = false;
      orderPositionStatus = OrderPositionStatus.visual_inspection_carrier_denied;
    }
    orderPositionToBeVisuallyInspected.status = orderPositionStatus;
    orderPositionToBeVisuallyInspected.logEntries.push({
      date: currentDate,
      author: 'Bernd Beförderer',
      description: '',
      acceptanceCriteria: {
        comment: orderPositionCheckDto.comment ?? '',
        visualInspectionCarrierCriteria: {
          acceptTransportability: orderPositionCheckDto.acceptTransportability,
          acceptLabeling: orderPositionCheckDto.acceptLabeling,
        },
      },
      status: orderPositionStatus,
    } as LogEntry);
    return [orderPositionToBeVisuallyInspected, allAcceptanceCriteriaTrueLocal];
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private createTransportLogEntry(allAcceptanceCriteriaTrue: boolean, currentDate: number): LogEntry {
    this.logger.debug('Create transport log entry');
    let status: string;
    if (allAcceptanceCriteriaTrue) {
      status = TransportDocumentStatus.transport;
    } else {
      status = TransportDocumentStatus.transport_denied;
    }
    return {
      status: status,
      date: currentDate,
      author: 'Bernd Beförderer',
    } as LogEntry;
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private getOrderPositionCheckDtoById(orderPositionId: number, dto: OrderVisualInspectionCarrierDto) {
    const orderPositionCheckDtos: OrderPositionVisualInspectionCarrierDto[] = dto.orderPositions;
    for (const orderPositionCheckDto of orderPositionCheckDtos) {
      if (orderPositionId == orderPositionCheckDto.orderPositionId) {
        return orderPositionCheckDto;
      }
    }
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private getOrderCheckDtoById(orderId: string, dto: UpdateVisualInspectionCarrierDto) {
    const orderCheckDtos: OrderVisualInspectionCarrierDto[] = dto.orders;
    for (const orderCheckDto of orderCheckDtos) {
      if (orderId == orderCheckDto.orderId) {
        return orderCheckDto;
      }
    }
  }

  /**
   * Executes visual inspection check for a transport document and updates its status to `delivered`, adds the corresponding
   * new log entries and saves it to the blockchain.
   *
   * @param updateVisualInspectionConsigneeDto - Update visual inspection check dto
   * @returns The transport document with status `delivered`
   */
  async updateVisualInspectionConsignee(
    updateVisualInspectionConsigneeDto: UpdateVisualInspectionConsigneeDto
  ): Promise<SaveGoodsDataDto> {
    let allAcceptanceCriteriaTrue = true;
    let allOrdersDelivered = true;
    let transportDocument: SaveGoodsDataDto;
    const currentDate = Date.now();

    try {
      transportDocument = await this.getTransportDocument(updateVisualInspectionConsigneeDto.transportDocumentId);
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(ErrorStrings.NOT_FOUND);
    }

    this.logger.log('Start processing visual inspection');
    if (transportDocument.status == TransportDocumentStatus.transport) {
      for (let orderIndex = 0; orderIndex < transportDocument.freight.orders.length; orderIndex++) {
        const currentOrder: OrderWithId = structuredClone(transportDocument.freight.orders[orderIndex]);
        let processedOrder: OrderWithId;

        const orderVisualInspectionConsigneeDto: OrderVisualInspectionConsigneeDto =
          this.getOrderVisualInspectionConsigneeDtoById(currentOrder.id, updateVisualInspectionConsigneeDto);

        [processedOrder, allOrdersDelivered, allAcceptanceCriteriaTrue] = this.processVisualInspectionConsigneePerOrder(
          currentOrder,
          orderVisualInspectionConsigneeDto,
          allAcceptanceCriteriaTrue,
          currentDate,
          allOrdersDelivered
        );
        transportDocument.freight.orders[orderIndex] = processedOrder;
      }
      if (allOrdersDelivered) {
        const logEntry: LogEntry = this.generatedTransportCompletedLogEntry(currentDate);
        transportDocument.logEntries.push(logEntry);
        transportDocument.status = logEntry.status;
      }
      transportDocument.lastUpdate = currentDate;
      await this.updateTransportDocument(transportDocument);
      return transportDocument;
    }
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private processVisualInspectionConsigneePerOrder(
    order: OrderWithId,
    orderVisualInspectionConsigneeDto: OrderVisualInspectionConsigneeDto,
    allAcceptanceCriteriaTrue: boolean,
    currentDate: number,
    allOrdersDelivered: boolean
  ): [OrderWithId, boolean, boolean] {
    const orderToBeVisuallyInspected: OrderWithId = structuredClone(order);
    let allAcceptanceCriteriaTrueLocal: boolean = allAcceptanceCriteriaTrue;
    let allOrdersDeliveredLocal: boolean = allOrdersDelivered;

    // Set values for the order that was visually inspected
    if (
      orderToBeVisuallyInspected.status == OrderStatus.carrier_confirmed &&
      orderVisualInspectionConsigneeDto != null
    ) {
      for (
        let orderPositionIndex = 0;
        orderPositionIndex < orderVisualInspectionConsigneeDto.orderPositions.length;
        orderPositionIndex++
      ) {
        const currentOrderPosition: OrderPositionWithId = orderToBeVisuallyInspected.orderPositions[orderPositionIndex];
        const orderPositionVisualInspectionConsigneeDto: OrderPositionVisualInspectionConsigneeDto =
          this.getOrderPositionVisualInspectionConsigneeDtoById(
            orderVisualInspectionConsigneeDto.orderPositions[orderPositionIndex].orderPositionId,
            orderVisualInspectionConsigneeDto
          );

        [orderToBeVisuallyInspected.orderPositions[orderPositionIndex], allAcceptanceCriteriaTrueLocal] =
          this.processVisualInspectionConsigneePerOrderPosition(
            orderPositionVisualInspectionConsigneeDto,
            currentOrderPosition,
            allAcceptanceCriteriaTrueLocal,
            currentDate
          );
      }
      let orderStatus = OrderStatus.visual_inspection_consignee_accepted;
      if (!allAcceptanceCriteriaTrueLocal) {
        orderStatus = OrderStatus.visual_inspection_consignee_denied;
      }
      orderToBeVisuallyInspected.status = orderStatus;
      orderToBeVisuallyInspected.logEntries.push({
        date: currentDate,
        author: 'Bernd Beförderer',
        description: '',
        status: orderStatus,
      } as LogEntry);
    }
    // Check if all orders have been delivered
    if (
      orderToBeVisuallyInspected.status != OrderStatus.visual_inspection_consignee_accepted &&
      orderToBeVisuallyInspected.status != OrderStatus.visual_inspection_consignee_denied
    ) {
      allOrdersDeliveredLocal = false;
    }
    return [orderToBeVisuallyInspected, allOrdersDeliveredLocal, allAcceptanceCriteriaTrueLocal];
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private processVisualInspectionConsigneePerOrderPosition(
    orderPositionVisualInspectionConsigneeDto: OrderPositionVisualInspectionConsigneeDto,
    orderPosition: OrderPositionWithId,
    allAcceptanceCriteriaTrue: boolean,
    currentDate: number
  ): [OrderPositionWithId, boolean] {
    let allAcceptanceCriteriaTrueLocal: boolean = allAcceptanceCriteriaTrue;
    const orderPositionToBeVisuallyInspected: OrderPositionWithId = structuredClone(orderPosition);

    let orderPositionStatus = OrderPositionStatus.visual_inspection_consignee_accepted;
    if (
      !orderPositionVisualInspectionConsigneeDto.acceptQuantity ||
      !orderPositionVisualInspectionConsigneeDto.acceptIntactness
    ) {
      allAcceptanceCriteriaTrueLocal = false;
      orderPositionStatus = OrderPositionStatus.visual_inspection_consignee_denied;
    }

    orderPositionToBeVisuallyInspected.status = orderPositionStatus;

    orderPositionToBeVisuallyInspected.logEntries.push(
      this.generateOrderPositionLogEntry(currentDate, orderPositionVisualInspectionConsigneeDto, orderPositionStatus)
    );
    return [orderPositionToBeVisuallyInspected, allAcceptanceCriteriaTrueLocal];
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private generatedTransportCompletedLogEntry(currentDate: number) {
    return {
      status: TransportDocumentStatus.transport_completed,
      date: currentDate,
      author: '',
    } as LogEntry;
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private generateOrderPositionLogEntry(
    currentDate: number,
    orderPositionVisualInspectionConsigneeDto: OrderPositionVisualInspectionConsigneeDto,
    orderPositionStatus: string
  ) {
    return {
      date: currentDate,
      author: 'Bernd Beförderer',
      description: '',
      acceptanceCriteria: {
        visualInspectionConsigneeCriteria: {
          acceptIntactness: orderPositionVisualInspectionConsigneeDto.acceptIntactness,
          acceptQuantity: orderPositionVisualInspectionConsigneeDto.acceptQuantity,
        },
      },
      status: orderPositionStatus,
    } as LogEntry;
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private getOrderVisualInspectionConsigneeDtoById(orderId: string, dto: UpdateVisualInspectionConsigneeDto) {
    const orderVisualInspectionConsigneeDto: OrderVisualInspectionConsigneeDto = dto.order;
    if (orderId == orderVisualInspectionConsigneeDto.orderId) {
      return orderVisualInspectionConsigneeDto;
    }
    return null;
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private getOrderPositionVisualInspectionConsigneeDtoById(
    orderPositionId: number,
    dto: OrderVisualInspectionConsigneeDto
  ) {
    const orderPositionVisualInspectionConsigneeDtos = dto.orderPositions;
    for (const orderPositionVisualInspectionConsigneeDto of orderPositionVisualInspectionConsigneeDtos) {
      if (orderPositionId == orderPositionVisualInspectionConsigneeDto.orderPositionId) {
        return orderPositionVisualInspectionConsigneeDto;
      }
    }
    return null;
  }

  /**
   * Updates an existing transport document by providing its id. (Consider first checking if it exists and then perform
   * the update to give the user proper feedback.)
   *
   * @param saveGoodsDataDto - A save goods data dto
   * @returns The updated transport document
   */
  async updateTransportDocument(saveGoodsDataDto: SaveGoodsDataDto): Promise<SaveGoodsDataDto> {
    this.logger.debug('Send AMQP request to tendermint service to UPDATE a transport document');
    this.logger.verbose('saveGoodsDataDto: ' + JSON.stringify(saveGoodsDataDto));
    try {
      await this.getTransportDocument(saveGoodsDataDto.id);
      return (
        (await this.amqpService.sendToAmqpBroker(
          MessagePatternsTendermintTransportDocument.UPDATE_TRANSPORT_DOCUMENT,
          saveGoodsDataDto,
          QueryGetTransportDocumentResponse
        )) as QueryGetTransportDocumentResponse
      ).transportDocument;
    } catch (e) {
      this.logger.error(e);
      throw new NotFoundException(ErrorStrings.NOT_FOUND_UPDATE);
    }
  }

  /**
   * Deletes a transport document from the blockchains world state.
   *
   * @param transportDocumentId - Transport document id
   * @returns Promise<void>
   */
  async deleteTransportDocumentFromWorldState(transportDocumentId: string): Promise<void> {
    this.logger.debug('Send AMQP request to tendermint service to DELETE a transport document');
    this.logger.verbose('transportDocumentId: ' + transportDocumentId);
    let result: SaveGoodsDataDto = undefined;
    try {
      result = await this.getTransportDocument(transportDocumentId);
    } catch (e) {
      this.logger.error(e);
      throw new NotFoundException(ErrorStrings.NOT_FOUND);
    }
    if (result.status == TransportDocumentStatus.created || result.status == TransportDocumentStatus.not_accepted) {
      await this.amqpService.sendToAmqpBroker(
        MessagePatternsTendermintTransportDocument.DELETE_TRANSPORT_DOCUMENT,
        transportDocumentId
      );
    } else {
      throw new ForbiddenException(ErrorStrings.ERROR_DISABLE);
    }
  }

  /**
   * Executes a carrier check for a given transport document, updates its status, adds a new log entry and saves it to
   * the blockchain.
   *
   * @param carrierCheckRequestDto - A dto containing information about what group positions (of an already created transport document) the carrier accepts
   * @returns The updated transport document
   */
  async processCarrierCheck(carrierCheckRequestDto: CarrierCheckRequestDto): Promise<SaveGoodsDataDto> {
    const result: SaveGoodsDataDto = await this.getTransportDocument(carrierCheckRequestDto.documentId);
    this.logger.debug('Start carrier check');
    if (result.status == TransportDocumentStatus.created) {
      const logEntry = new LogEntry(undefined, undefined, undefined);
      // if any of the group positions is not accepted, set status to not accepted, else to accepted. LogEntry will be set accordingly
      if (
        !carrierCheckRequestDto.carrierCheckCriteria.acceptConsignor ||
        carrierCheckRequestDto.carrierCheckCriteria.acceptConsignees.filter((value) => !value).length > 0 ||
        !carrierCheckRequestDto.carrierCheckCriteria.acceptFreight ||
        !carrierCheckRequestDto.carrierCheckCriteria.acceptTransportationInstructions ||
        !carrierCheckRequestDto.carrierCheckCriteria.acceptAdditionalInformation
      ) {
        result.status = TransportDocumentStatus.not_accepted;
        logEntry.status = TransportDocumentStatus.not_accepted;
      } else {
        result.status = TransportDocumentStatus.accepted;
        logEntry.status = TransportDocumentStatus.accepted;
      }
      const date = new Date().getTime();
      result.lastUpdate = date;
      logEntry.date = date;

      logEntry.acceptanceCriteria = {
        comment: carrierCheckRequestDto.carrierCheckCriteria.comment,
        carrierCheckCriteria: {
          acceptAdditionalInformation: carrierCheckRequestDto.carrierCheckCriteria.acceptAdditionalInformation,
          acceptConsignees: carrierCheckRequestDto.carrierCheckCriteria.acceptConsignees,
          acceptFreight: carrierCheckRequestDto.carrierCheckCriteria.acceptFreight,
          acceptConsignor: carrierCheckRequestDto.carrierCheckCriteria.acceptConsignor,
          acceptTransportationInstructions:
            carrierCheckRequestDto.carrierCheckCriteria.acceptTransportationInstructions,
        },
      } as AcceptanceCriteria;

      result.logEntries.push(logEntry);
      return this.updateTransportDocument(result);
    } else {
      throw new BadRequestException(ErrorStrings.ERROR_CARRIERCHECK);
    }
  }

  /**
   * Process whether the consignee confirms the carrier or not.
   *
   * @param documentId - Transport document id
   * @param jwtDto - User information
   * @returns updated transport document
   */
  async processCarrierConfirmation(documentId: string, jwtDto: JwtDto): Promise<SaveGoodsDataDto> {
    const result: SaveGoodsDataDto = await this.getTransportDocument(documentId);
    this.logger.debug('Start process carrier confirmation');
    if (result.status == TransportDocumentStatus.transport) {
      const date = new Date().getTime();
      const logEntry = new LogEntry(OrderStatus.carrier_confirmed, date, undefined);

      result.lastUpdate = date;
      result.freight.orders.forEach((order: OrderWithId) => {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        if (isEqual(hash(order.consignee), hash(jwtDto.company))) {
          order.status = OrderStatus.carrier_confirmed;
          order.logEntries.push(logEntry);
        }
      });

      return this.updateTransportDocument(result);
    } else {
      throw new BadRequestException(ErrorStrings.ERROR_CARRIERCONFIRMATION);
    }
  }

  /**
   * Updates the status of a transport document to `released`, adds a new log entry and saves it to the blockchain.
   *
   * @param transportDocumentId - Transport document id
   * @returns The updated transport document
   */
  async releaseTransportDocument(transportDocumentId: string): Promise<SaveGoodsDataDto> {
    const result: SaveGoodsDataDto = await this.getTransportDocument(transportDocumentId);
    this.logger.debug('Start release transport document');
    if (result.status == TransportDocumentStatus.accepted) {
      result.status = TransportDocumentStatus.released;
      const date = new Date().getTime();
      const logEntry = new LogEntry(TransportDocumentStatus.released, date, undefined);
      result.lastUpdate = date;
      result.logEntries.push(logEntry);
      return this.updateTransportDocument(result);
    } else {
      throw new BadRequestException(ErrorStrings.ERROR_RELEASE);
    }
  }

  /**
   * Executes a vehicle inspection for a given transport document, updates its status, adds a new log entry and saves it
   * to the blockchain.
   *
   * @param transportVehicleInspectionRequestDto - A dto containing modifications of the transport document and information
   * about what group positions (of an already accepted transport document) the carrier accepts.
   * @returns The updated transport document
   */
  async processVehicleInspection(
    transportVehicleInspectionRequestDto: TransportVehicleInspectionRequestDto
  ): Promise<SaveGoodsDataDto> {
    const result: SaveGoodsDataDto = await this.getTransportDocument(transportVehicleInspectionRequestDto.documentId);
    this.logger.debug('Start process vehicle inspection');
    if (result.status == TransportDocumentStatus.released || result.status == TransportDocumentStatus.created) {
      const logEntry = new LogEntry(undefined, undefined, undefined);
      if (
        !transportVehicleInspectionRequestDto.transportVehicleCriteria.acceptCarrierInformation ||
        !transportVehicleInspectionRequestDto.transportVehicleCriteria.acceptCarrierSafetyEquipment ||
        !transportVehicleInspectionRequestDto.transportVehicleCriteria.acceptVehicleCondition ||
        !transportVehicleInspectionRequestDto.transportVehicleCriteria.acceptVehicleSafetyEquipment ||
        transportVehicleInspectionRequestDto.transportVehicleCriteria.driverName == '' ||
        transportVehicleInspectionRequestDto.transportVehicleCriteria.licencePlate == ''
      ) {
        result.status = TransportDocumentStatus.vehicle_denied;
        logEntry.status = TransportDocumentStatus.vehicle_denied;
      } else {
        result.status = TransportDocumentStatus.vehicle_accepted;
        logEntry.status = TransportDocumentStatus.vehicle_accepted;
      }
      result.carrier.driver = transportVehicleInspectionRequestDto.transportVehicleCriteria.driverName;
      result.carrier.licensePlate = transportVehicleInspectionRequestDto.transportVehicleCriteria.licencePlate;
      const date = new Date().getTime();
      result.lastUpdate = date;

      logEntry.acceptanceCriteria = {
        comment: transportVehicleInspectionRequestDto.transportVehicleCriteria.comment,
        transportVehicleCriteria: transportVehicleInspectionRequestDto.transportVehicleCriteria,
      } as AcceptanceCriteria;

      logEntry.date = date;
      result.logEntries.push(logEntry);
      return this.updateTransportDocument(result);
    } else {
      throw new BadRequestException(ErrorStrings.ERROR_VEHICLECHECK);
    }
  }

  /**
   * Filter information for consignee
   *
   * @param jwtDto - User information
   * @param saveGoodsDataDto - Save goods data dto
   * @returns Filtered save goods data dto
   */
  filterForConsignee(jwtDto: JwtDto, saveGoodsDataDto: SaveGoodsDataDto): SaveGoodsDataDto {
    const filteredOrders: OrderWithId[] = saveGoodsDataDto.freight.orders.filter((order: Order) => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      return isEqual(hash(order.consignee), hash(jwtDto.company));
    });
    if (filteredOrders.length > 0) {
      saveGoodsDataDto.freight.orders = filteredOrders;
    } else {
      return null;
    }
    return saveGoodsDataDto;
  }

  /**
   * Parses the received transport document from the blockchain into the current data model for the frontend, i.e. the
   * log entry structure, which is given due to protobuf limitations, is aligned to the current data model. Only the
   * log entries of the transport document will be modified.
   *
   * @param saveGoodsDataDto - A save goods data dto
   * @returns The parsed transport document in the current frontend data model structure
   */
  parseTransportDocument(saveGoodsDataDto: SaveGoodsDataDto): SaveGoodsDataDto {
    const transportDocumentParsed: SaveGoodsDataDto = this.buildTransportDocumentWithoutLogEntries(saveGoodsDataDto);

    for (const logEntry of saveGoodsDataDto.logEntries) {
      // Case 1: logEntry contains carrierCheckCriteria
      if (logEntry?.acceptanceCriteria?.carrierCheckCriteria != null) {
        logEntry.acceptanceCriteria.carrierCheckCriteria.comment = logEntry.acceptanceCriteria.comment;
        const logEntryParsed: LogEntry = this.parseLogEntry(logEntry, logEntry.acceptanceCriteria.carrierCheckCriteria);
        transportDocumentParsed.logEntries.push(logEntryParsed);

        // Case 2: logEntry contains transportVehicleCriteria
      } else if (logEntry?.acceptanceCriteria?.transportVehicleCriteria != null) {
        logEntry.acceptanceCriteria.transportVehicleCriteria.comment = logEntry.acceptanceCriteria.comment;
        const logEntryParsed: LogEntry = this.parseLogEntry(
          logEntry,
          logEntry.acceptanceCriteria.transportVehicleCriteria
        );
        transportDocumentParsed.logEntries.push(logEntryParsed);

        // Case 3: logEntry contains no acceptanceCriteria
      } else {
        transportDocumentParsed.logEntries.push(logEntry);
      }
    }
    return transportDocumentParsed;
  }

  /**
   * Auxiliary function for `parseTransportDocument()`. Constructs a new log entry in the current frontend data model
   * structure.
   *
   * @param logEntry - Log entry in the blockchain data model structure
   * @param acceptanceCriteria - The corresponding acceptance criteria of the log entry
   * @returns The constructed log entry
   */
  private parseLogEntry(
    logEntry: LogEntry,
    acceptanceCriteria: CarrierCheckCriteria | TransportVehicleCriteria
  ): LogEntry {
    return {
      status: logEntry.status,
      date: logEntry.date,
      author: logEntry.author,
      description: logEntry.description,
      acceptanceCriteria: acceptanceCriteria,
    };
  }

  /**
   * Auxiliary function for `parseTransportDocument()`. Builds an analogue transport document, but without its log entries.
   *
   * @param saveGoodsDataDto - A save goods data dto
   * @returns Transport document without log entries
   */
  private buildTransportDocumentWithoutLogEntries(saveGoodsDataDto: SaveGoodsDataDto): SaveGoodsDataDto {
    return {
      id: saveGoodsDataDto.id,
      consignor: saveGoodsDataDto.consignor,
      freight: saveGoodsDataDto.freight,
      carrier: saveGoodsDataDto.carrier,
      logEntries: [],
      status: saveGoodsDataDto.status,
      createdDate: saveGoodsDataDto.createdDate,
      createdBy: saveGoodsDataDto.createdBy,
      lastUpdate: saveGoodsDataDto.lastUpdate,
    };
  }
}
