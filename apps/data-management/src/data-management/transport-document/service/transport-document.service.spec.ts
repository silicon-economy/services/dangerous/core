/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  BadRequestException,
  ForbiddenException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import structuredClone from '@ungap/structured-clone';
import fs from 'fs';
import path from 'path';
import * as ErrorStrings from '../../../resources/error.strings.json';
import { DangerousGoodRegistrationService } from '@dataManagementModule/dangerous-good-registration/service/dangerous-good-registration.service';
import { TransportDocumentService } from '@dataManagementModule/transport-document/service/transport-document.service';
import { TransportDocumentController } from '@dataManagementModule/transport-document/controller/transport-document.controller';
import {
  OrderStatus,
  TransportDocumentStatus,
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/status.enum';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { LogEntry } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/log-entry';
import { CarrierCheckRequestDto } from '@core/api-interfaces/lib/dtos/data-management';
import { TransportVehicleInspectionRequestDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-vehicle-inspection-request.dto';
import { UpdateVisualInspectionCarrierDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import { UpdateVisualInspectionConsigneeDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';
import {
  AcceptanceCriteria,
  TransportVehicleCriteria,
} from '@core/api-interfaces/lib/dtos/blockchain/transport_document';
import { AmqpService } from '@amqp/service/amqp.service';
import { AmqpModule } from '@amqp/amqp.module';
import { Queues } from '@amqp/queues.enum';
import { JwtDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { UserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/user.dto';
import { CarrierCheckCriteria } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/acceptance-criteria/carrier-check-criteria';
import {
  mockTransportDocumentAccepted,
  mockTransportDocumentCreated,
  mockTransportDocumentTransport,
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';

describe('TransportDocumentService', () => {
  let transportDocumentController: TransportDocumentController;
  let transportDocumentService: TransportDocumentService;
  let transportDocumentsMock: SaveGoodsDataDto[] = JSON.parse(
    fs.readFileSync(path.resolve(__dirname, '../../../resources/testdata/dangerousGoodsList.json'), 'utf-8')
  ) as SaveGoodsDataDto[];
  const mockJwtToken: JwtDto = new JwtDto(
    undefined,
    [UserRole.consignee],
    new UserDto('funkeAg', {
      name: 'Funke AG - Lacke und Farben',
      address: {
        street: 'Columbiadamm',
        number: '194',
        postalCode: '10965',
        city: 'Berlin',
        country: 'Deutschland',
      },
      contact: {
        name: 'Dr. Martina Zünd',
        phone: '0049 741 852 063 9',
        mail: 'Zuend@Funke.de',
        department: 'Abt. 4A',
      },
    })
  );

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [TransportDocumentController],
      providers: [DangerousGoodRegistrationService, TransportDocumentService, AmqpService],
      imports: [
        AmqpModule.register(Queues.TENDERMINT_SERVICE),
        AmqpModule.register(Queues.BUSINESS_LOGIC),
        ConfigModule,
      ],
    }).compile();

    transportDocumentController = moduleRef.get<TransportDocumentController>(TransportDocumentController);
    transportDocumentService = moduleRef.get<TransportDocumentService>(TransportDocumentService);
  });

  beforeEach(() => {
    reloadTransportDocumentsMock();
  });

  it('should be defined', () => {
    expect(transportDocumentController).toBeDefined();
    expect(transportDocumentService).toBeDefined();
    expect(transportDocumentService.getTransportDocument).toBeDefined();
    expect(transportDocumentService.getAllTransportDocuments).toBeDefined();
    expect(transportDocumentService.saveTransportDocument).toBeDefined();
    expect(transportDocumentService.createAndReleaseTransportDocument).toBeDefined();
    expect(transportDocumentService.updateTransportDocument).toBeDefined();
    expect(transportDocumentService.processCarrierCheck).toBeDefined();
    expect(transportDocumentService.releaseTransportDocument).toBeDefined();
  });

  describe('createAndReleaseTransportDocument', () => {
    it('should create and release a transport document', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'saveTransportDocument')
        .mockReturnValue(transportDocumentsMock[4]);
      jest.spyOn(TransportDocumentService.prototype as any, 'updateTransportDocument').mockImplementation((a) => a);

      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      const result: SaveGoodsDataDto = structuredClone(transportDocumentsMock[4]) as SaveGoodsDataDto;
      const logEntry = new LogEntry(TransportDocumentStatus.released, result.logEntries[0].date, undefined);
      result.status = TransportDocumentStatus.released;
      result.logEntries.push(logEntry);

      expect(await transportDocumentService.createAndReleaseTransportDocument({} as SaveGoodsDataDto)).toMatchObject(
        result
      );
    });

    // eslint-disable-next-line @typescript-eslint/require-await
    it('should fail when trying to create and release a transport document', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'saveTransportDocument')
        .mockReturnValue(transportDocumentsMock[0]);
      jest.spyOn(TransportDocumentService.prototype as any, 'updateTransportDocument').mockImplementation((a) => a);

      await expect(async () => {
        await transportDocumentService.createAndReleaseTransportDocument({} as SaveGoodsDataDto);
      }).rejects.toThrow(new InternalServerErrorException(ErrorStrings.ERROR_SAVE));
    });
  });

  describe('deleteTransportDocumentFromWorldState', () => {
    it('should delete a transport document from world state', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        .mockReturnValue(transportDocumentsMock[4]);
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      jest.spyOn(AmqpService.prototype as any, 'sendToAmqpBroker').mockReturnValue({});

      expect(await transportDocumentService.deleteTransportDocumentFromWorldState('')).toBeUndefined();
    });

    it('should fail when trying to delete a transport document with status other than created or not_accepted', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        .mockReturnValue(transportDocumentsMock[0]);
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      jest.spyOn(AmqpService.prototype as any, 'sendToAmqpBroker').mockReturnValue({});

      await expect(async () => {
        await transportDocumentService.deleteTransportDocumentFromWorldState('');
      }).rejects.toThrow(new ForbiddenException(ErrorStrings.ERROR_DISABLE));
    });
  });

  describe('processCarrierCheck', () => {
    it('should process carrier check for a transport document', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[4]));

      const carrierCheckRequestDto: CarrierCheckRequestDto = {
        carrierCheckCriteria: {
          acceptAdditionalInformation: true,
          acceptConsignees: [true],
          acceptFreight: true,
          acceptConsignor: true,
          acceptTransportationInstructions: true,
          comment: '',
        },
        documentId: transportDocumentsMock[4].id,
      };
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      const result = structuredClone(transportDocumentsMock[4]) as SaveGoodsDataDto;
      const logEntry = new LogEntry(TransportDocumentStatus.accepted, new Date().getTime(), undefined, undefined, {
        comment: carrierCheckRequestDto.carrierCheckCriteria.comment,
        carrierCheckCriteria: {
          acceptAdditionalInformation: carrierCheckRequestDto.carrierCheckCriteria.acceptAdditionalInformation,
          acceptConsignees: carrierCheckRequestDto.carrierCheckCriteria.acceptConsignees,
          acceptFreight: carrierCheckRequestDto.carrierCheckCriteria.acceptFreight,
          acceptConsignor: carrierCheckRequestDto.carrierCheckCriteria.acceptConsignor,
          acceptTransportationInstructions:
            carrierCheckRequestDto.carrierCheckCriteria.acceptTransportationInstructions,
        },
      });

      result.status = TransportDocumentStatus.accepted;
      result.logEntries.push(logEntry);

      expect((await transportDocumentService.processCarrierCheck(carrierCheckRequestDto)).status).toEqual(
        result.status
      );

      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[4]));
      expect((await transportDocumentService.processCarrierCheck(carrierCheckRequestDto)).logEntries[1].status).toEqual(
        result.logEntries[1].status
      );

      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValueOnce(structuredClone(transportDocumentsMock[4]));
      expect(
        (await transportDocumentService.processCarrierCheck(carrierCheckRequestDto)).logEntries[1].acceptanceCriteria
      ).toEqual(result.logEntries[1].acceptanceCriteria);
    });

    it('should process carrier check for a transport document', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[4]));

      const carrierCheckRequestDto: CarrierCheckRequestDto = {
        carrierCheckCriteria: {
          acceptAdditionalInformation: false,
          acceptConsignees: [true],
          acceptFreight: true,
          acceptConsignor: true,
          acceptTransportationInstructions: true,
          comment: '',
        },
        documentId: transportDocumentsMock[4].id,
      };
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      const result: SaveGoodsDataDto = structuredClone(transportDocumentsMock[4]) as SaveGoodsDataDto;
      const logEntry = new LogEntry(TransportDocumentStatus.not_accepted, new Date().getTime(), undefined, undefined, {
        comment: carrierCheckRequestDto.carrierCheckCriteria.comment,
        carrierCheckCriteria: {
          acceptAdditionalInformation: carrierCheckRequestDto.carrierCheckCriteria.acceptAdditionalInformation,
          acceptConsignees: carrierCheckRequestDto.carrierCheckCriteria.acceptConsignees,
          acceptFreight: carrierCheckRequestDto.carrierCheckCriteria.acceptFreight,
          acceptConsignor: carrierCheckRequestDto.carrierCheckCriteria.acceptConsignor,
          acceptTransportationInstructions:
            carrierCheckRequestDto.carrierCheckCriteria.acceptTransportationInstructions,
        },
      });

      result.status = TransportDocumentStatus.not_accepted;
      result.logEntries.push(logEntry);

      expect((await transportDocumentService.processCarrierCheck(carrierCheckRequestDto)).status).toEqual(
        result.status
      );

      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[4]));
      expect((await transportDocumentService.processCarrierCheck(carrierCheckRequestDto)).logEntries[1].status).toEqual(
        result.logEntries[1].status
      );

      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValueOnce(structuredClone(transportDocumentsMock[4]));
      expect(
        (await transportDocumentService.processCarrierCheck(carrierCheckRequestDto)).logEntries[1].acceptanceCriteria
      ).toEqual(result.logEntries[1].acceptanceCriteria);
    });

    it('should fail when trying to process carrier check for transport document with status other than created', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        .mockReturnValue(transportDocumentsMock[0]);
      await expect(async () => {
        await transportDocumentService.processCarrierCheck({} as CarrierCheckRequestDto);
      }).rejects.toThrow(new BadRequestException(ErrorStrings.ERROR_CARRIERCHECK));
    });
  });

  describe('updateVisualInspectionCarrier', () => {
    it('should update visual inspection carrier', async () => {
      const updateVisualInspectionCarrierDto: UpdateVisualInspectionCarrierDto = {
        transportDocumentId: 'BP000006-202338',
        orders: [
          {
            orderId: '2a9779e1-e74e-4f1b-89e9-615bcfc051ff',
            orderPositions: [
              {
                orderPositionId: 11,
                acceptLabeling: true,
                acceptTransportability: true,
              },
              {
                orderPositionId: 13,
                acceptLabeling: true,
                acceptTransportability: true,
              },
            ],
          },
          {
            orderId: 'f08368f3-f050-4ff8-ab0b-4b2caabf6ad6',
            orderPositions: [
              {
                orderPositionId: 15,
                acceptLabeling: true,
                acceptTransportability: true,
              },
            ],
          },
        ],
      };

      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[5]));

      const updatedTransportDocument = await transportDocumentService.updateVisualInspectionCarrier(
        updateVisualInspectionCarrierDto
      );

      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-call
      const expectedResult: SaveGoodsDataDto = structuredClone(transportDocumentsMock[6]);
      expectedResult.freight.orders.forEach((order) => {
        order.orderPositions.forEach((orderPosition) => {
          orderPosition.logEntries[0].date = updatedTransportDocument.lastUpdate;
        });
      });

      expectedResult.lastUpdate = updatedTransportDocument.lastUpdate;
      expectedResult.logEntries[3].date = updatedTransportDocument.logEntries[3].date;

      expect(updatedTransportDocument).toEqual(expectedResult);
    });
  });

  describe('updateVisualInspectionConsignee', () => {
    it('should update visual inspection consignee', async () => {
      const updateVisualInspectionConsigneeDto: UpdateVisualInspectionConsigneeDto = {
        transportDocumentId: 'BP000006-202338',
        order: {
          orderId: '2a9779e1-e74e-4f1b-89e9-615bcfc051ff',
          orderPositions: [
            {
              orderPositionId: 11,
              acceptQuantity: true,
              acceptIntactness: true,
            },
            {
              orderPositionId: 13,
              acceptQuantity: true,
              acceptIntactness: true,
            },
          ],
        },
      };

      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[7]));

      const updatedTransportDocument = await transportDocumentService.updateVisualInspectionConsignee(
        updateVisualInspectionConsigneeDto
      );

      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-call
      const expectedResult: SaveGoodsDataDto = structuredClone(transportDocumentsMock[8]);
      expectedResult.freight.orders[0].logEntries.forEach((logEntry) => {
        logEntry.date = updatedTransportDocument.lastUpdate;
      });
      expectedResult.freight.orders[0].orderPositions.forEach((orderPosition) => {
        orderPosition.logEntries[1].date = updatedTransportDocument.lastUpdate;
      });

      expectedResult.lastUpdate = updatedTransportDocument.lastUpdate;

      expect(updatedTransportDocument).toEqual(expectedResult);
    });
  });

  describe('processCarrierConfirmation', () => {
    it('should process carrier confirmation', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[0]));
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      const result: SaveGoodsDataDto = structuredClone(transportDocumentsMock[0]) as SaveGoodsDataDto;
      const date = new Date().getTime();

      const logEntry = new LogEntry(OrderStatus.carrier_confirmed, date, undefined);
      result.freight.orders[0].status = OrderStatus.carrier_confirmed;
      result.lastUpdate = date;
      result.freight.orders[0].logEntries.push(logEntry);

      expect(
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-call
        (await transportDocumentService.processCarrierConfirmation(result.id, mockJwtToken)).freight.orders[0].status
      ).toEqual(result.freight.orders[0].status);
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[0]));
      expect(
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-call
        (await transportDocumentService.processCarrierConfirmation(result.id, mockJwtToken)).freight.orders[0]
          .logEntries[0].status
      ).toEqual(result.freight.orders[0].logEntries[0].status);
    });
  });

  describe('releaseTransportDocument', () => {
    it('should release a transport document', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[3]));

      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      const result: SaveGoodsDataDto = structuredClone(transportDocumentsMock[3]) as SaveGoodsDataDto;
      const date = new Date().getTime();
      const logEntry = new LogEntry(TransportDocumentStatus.released, date, undefined);

      result.status = TransportDocumentStatus.released;
      result.lastUpdate = date;
      result.logEntries.push(logEntry);

      expect((await transportDocumentService.releaseTransportDocument(result.id)).status).toEqual(result.status);

      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[3]));
      expect((await transportDocumentService.releaseTransportDocument(result.id)).logEntries[2].status).toEqual(
        result.logEntries[2].status
      );
    });

    it('should fail when trying to release a transport document with status other than accepted', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        .mockReturnValue(transportDocumentsMock[0]);

      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      const transportDocumentWithStatusTransport: SaveGoodsDataDto = structuredClone(
        transportDocumentsMock[0]
      ) as SaveGoodsDataDto;

      await expect(async () => {
        await transportDocumentService.releaseTransportDocument(transportDocumentWithStatusTransport.id);
      }).rejects.toThrow(new BadRequestException(ErrorStrings.ERROR_RELEASE));
    });
  });

  describe('processVehicleInspection', () => {
    it('should process vehicle inspection', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[1]));

      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      const result: SaveGoodsDataDto = structuredClone(transportDocumentsMock[1]) as SaveGoodsDataDto;

      const transportVehicleInspectionRequestDto: TransportVehicleInspectionRequestDto = {
        documentId: result.id,
        transportVehicleCriteria: {
          acceptCarrierInformation: true,
          acceptCarrierSafetyEquipment: true,
          acceptVehicleCondition: true,
          acceptVehicleSafetyEquipment: true,
          driverName: 'test driver',
          licencePlate: 'AB-CD123',
          comment: 'Test comment',
        } as TransportVehicleCriteria,
      };

      if (result.status == TransportDocumentStatus.released) {
        const logEntry = new LogEntry(undefined, undefined, undefined);
        if (
          !transportVehicleInspectionRequestDto.transportVehicleCriteria.acceptCarrierInformation ||
          !transportVehicleInspectionRequestDto.transportVehicleCriteria.acceptCarrierSafetyEquipment ||
          !transportVehicleInspectionRequestDto.transportVehicleCriteria.acceptVehicleCondition ||
          !transportVehicleInspectionRequestDto.transportVehicleCriteria.acceptVehicleSafetyEquipment ||
          transportVehicleInspectionRequestDto.transportVehicleCriteria.driverName == '' ||
          transportVehicleInspectionRequestDto.transportVehicleCriteria.licencePlate == ''
        ) {
          result.status = TransportDocumentStatus.vehicle_denied;
          logEntry.status = TransportDocumentStatus.vehicle_denied;
        } else {
          result.status = TransportDocumentStatus.vehicle_accepted;
          logEntry.status = TransportDocumentStatus.vehicle_accepted;
        }
        result.carrier.driver = transportVehicleInspectionRequestDto.transportVehicleCriteria.driverName;
        result.carrier.licensePlate = transportVehicleInspectionRequestDto.transportVehicleCriteria.licencePlate;
        const date = new Date().getTime();
        result.lastUpdate = date;

        logEntry.acceptanceCriteria = {
          comment: transportVehicleInspectionRequestDto.transportVehicleCriteria.comment,
          transportVehicleCriteria: transportVehicleInspectionRequestDto.transportVehicleCriteria,
        } as AcceptanceCriteria;

        logEntry.date = date;
        result.logEntries.push(logEntry);
      }

      expect(
        (await transportDocumentService.processVehicleInspection(transportVehicleInspectionRequestDto)).carrier
      ).toEqual(result.carrier);
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[1]));
      expect(
        (await transportDocumentService.processVehicleInspection(transportVehicleInspectionRequestDto)).status
      ).toEqual(result.status);
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[1]));
      expect(
        (await transportDocumentService.processVehicleInspection(transportVehicleInspectionRequestDto)).logEntries[3]
          .status
      ).toEqual(result.logEntries[3].status);
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        .mockReturnValue(structuredClone(transportDocumentsMock[1]));
      expect(
        (await transportDocumentService.processVehicleInspection(transportVehicleInspectionRequestDto)).logEntries[3]
          .acceptanceCriteria
      ).toEqual(result.logEntries[3].acceptanceCriteria);
    });

    it('should fail when trying to process vehicle inspection for a transport document with status other than released', async () => {
      jest
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .spyOn(TransportDocumentService.prototype as any, 'getTransportDocument')
        .mockReturnValue(transportDocumentsMock[0]);

      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      const transportDocumentWithStatusTransport: SaveGoodsDataDto = structuredClone(
        transportDocumentsMock[0]
      ) as SaveGoodsDataDto;
      const transportVehicleInspectionRequestDto: TransportVehicleInspectionRequestDto = {
        documentId: transportDocumentWithStatusTransport.id,
        transportVehicleCriteria: {
          acceptCarrierInformation: true,
          acceptCarrierSafetyEquipment: true,
          acceptVehicleCondition: true,
          acceptVehicleSafetyEquipment: true,
          driverName: 'test driver',
          licencePlate: 'AB-CD123',
          comment: 'Test comment',
        } as TransportVehicleCriteria,
      };

      await expect(async () => {
        await transportDocumentService.processVehicleInspection(transportVehicleInspectionRequestDto);
      }).rejects.toThrow(new BadRequestException(ErrorStrings.ERROR_VEHICLECHECK));
    });

    it('should filterForConsignee', () => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      const mockDocument: SaveGoodsDataDto = structuredClone(transportDocumentsMock[0]) as SaveGoodsDataDto;
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-assignment

      expect(transportDocumentService.filterForConsignee(mockJwtToken, mockDocument)).toEqual(mockDocument);
    });
  });

  describe('parseTransportDocument', (): void => {
    it('should correctly return logEntry with CarrierCheckCriteria', (): void => {
      const logEntry: LogEntry = {
        status: 'send',
        date: 1692962291681,
        author: 'Max Mustermann',
        description: '',
      };

      const carrierCheckCriteriaMock: CarrierCheckCriteria = {
        acceptConsignor: true,
        acceptConsignees: [true, true],
        acceptFreight: true,
        acceptTransportationInstructions: true,
        acceptAdditionalInformation: true,
      };

      const parseLogEntry = transportDocumentService['parseLogEntry'](logEntry, carrierCheckCriteriaMock);

      expect(parseLogEntry).toEqual({
        status: 'send',
        date: 1692962291681,
        author: 'Max Mustermann',
        description: '',
        acceptanceCriteria: carrierCheckCriteriaMock,
      });
    });

    it('should correctly return transport document without log entries', (): void => {
      const buildTransportDocumentWithoutLogEntries: SaveGoodsDataDto =
        transportDocumentService['buildTransportDocumentWithoutLogEntries'](mockTransportDocumentTransport);

      expect(buildTransportDocumentWithoutLogEntries).toEqual({
        id: mockTransportDocumentTransport.id,
        consignor: mockTransportDocumentTransport.consignor,
        freight: mockTransportDocumentTransport.freight,
        carrier: mockTransportDocumentTransport.carrier,
        logEntries: [],
        status: mockTransportDocumentTransport.status,
        createdDate: mockTransportDocumentTransport.createdDate,
        createdBy: undefined,
        lastUpdate: mockTransportDocumentTransport.lastUpdate,
      });
    });
  });

  it('should call getAllTransportDocuments', async () => {
    const sendToAmqpBrokerSpy = jest
      .spyOn(transportDocumentService['amqpService'], 'sendToAmqpBroker')
      .mockReturnValue(Promise.resolve({ transportDocument: [{ lastUpdate: 1 }, { lastUpdate: 2 }] }));
    expect(await transportDocumentService.getAllTransportDocuments()).toEqual([{ lastUpdate: 2 }, { lastUpdate: 1 }]);
    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
  });

  it('should call getAllDangerousGoodRegistrations and throw error', async () => {
    const sendToAmqpBrokerSpy = jest
      .spyOn(transportDocumentService['amqpService'], 'sendToAmqpBroker')
      .mockRejectedValue(new Error());
    await expect(async () => {
      await transportDocumentService.getAllTransportDocuments();
    }).rejects.toThrow(new NotFoundException('No transport document with that Id found in blockchain data storage.'));
    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
  });

  it('should call saveTransportDocument', async () => {
    const sendToAmqpBrokerSpy = jest
      .spyOn(transportDocumentService['amqpService'], 'sendToAmqpBroker')
      .mockReturnValue(Promise.resolve({ dangerousGoodRegistration: undefined }));
    await transportDocumentService.saveTransportDocument(mockTransportDocumentCreated);
    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
  });

  it('should call updateTransportDocument', async () => {
    const sendToAmqpBrokerSpy = jest
      .spyOn(transportDocumentService['amqpService'], 'sendToAmqpBroker')
      .mockReturnValue(Promise.resolve({ dangerousGoodRegistration: undefined }));
    const getTransportDocumentServiceSpy = jest
      .spyOn(transportDocumentService, 'getTransportDocument')
      .mockReturnValue(Promise.resolve(mockTransportDocumentAccepted));
    await transportDocumentService.updateTransportDocument(mockTransportDocumentAccepted);
    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
    expect(getTransportDocumentServiceSpy).toHaveBeenCalled();
  });

  it('should parseTransportDocument', () => {
    expect(transportDocumentService.parseTransportDocument(mockTransportDocumentAccepted)).toEqual({
      carrier: {
        driver: 'Mustermann',
        licensePlate: 'DR-EI333',
        name: 'Gefahrgutlogistik',
      },
      consignor: {
        address: {
          city: 'Gelsenkirchen',
          country: 'Deutschland',
          number: '50',
          postalCode: '45894',
          street: 'Ressestraße',
        },
        contact: {
          department: 'OE 3 II',
          mail: 'fritz.hazard@ChemIndInc.de',
          name: 'Fritz Hazard',
          phone: '0049 123 456 789 0',
        },
        name: 'Chemical Industries Germany Inc.',
      },
      createdDate: 1629372434571,
      freight: {
        additionalInformation: 'Beförderung ohne Freistellung nach ADR 1.1.3.6',
        orders: [
          {
            consignee: {
              address: {
                city: 'Berlin',
                country: 'Deutschland',
                number: '194',
                postalCode: '10965',
                street: 'Columbiadamm',
              },
              contact: {
                department: 'Abt. 4A',
                mail: 'Zuend@Funke.de',
                name: 'Dr. Martina Zünd',
                phone: '0049 741 852 063 9',
              },
              name: 'Funke AG - Lacke und Farben',
            },
            id: '0e7d3e92-f59b-4985-a61b-4267270cf57a',
            logEntries: [],
            orderPositions: [
              {
                dangerousGood: {
                  casNumber: '67-56-1',
                  description: 'METHANOL',
                  label1: '3',
                  label2: '6.1',
                  label3: '',
                  packingGroup: 1,
                  transportCategory: '0',
                  tunnelRestrictionCode: 6,
                  unNumber: '1230',
                },
                deviceId: '1',
                id: 1,
                individualAmount: 1000,
                logEntries: [],
                package:
                  'Großpackmittel (IBC) aus starrer Kunststoff (H) für flüssige Stoffe, mit äußerer Umhüllung aus Stahl (A)',
                packagingCode: '31HA1',
                polluting: false,
                quantity: 1,
                status: '',
                totalAmount: 1000,
                transportPoints: 3000,
                unit: 'L',
              },
            ],
            status: '',
          },
        ],
        totalTransportPoints: 3000,
        transportationInstructions: 'Beachtung der höchstzulässigen anwendbaren Stapellast gem. ADR 6.5.2.2.2',
      },
      id: 'BP000001-20210819',
      lastUpdate: 1629372601559,
      logEntries: [
        {
          author: '',
          date: 1629372434571,
          description: '',
          status: 'created',
        },
        {
          acceptanceCriteria: {
            acceptAdditionalInformation: true,
            acceptConsignees: [true],
            acceptConsignor: true,
            acceptFreight: true,
            acceptTransportationInstructions: true,
            comment: '',
          },
          author: '',
          date: 1629372458465,
          description: '',
          status: 'accepted',
        },
      ],
      status: 'accepted',
    });
  });

  function reloadTransportDocumentsMock() {
    transportDocumentsMock = JSON.parse(
      fs.readFileSync(path.resolve(__dirname, '../../../resources/testdata/dangerousGoodsList.json'), 'utf-8')
    ) as SaveGoodsDataDto[];
  }
});
