/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { DangerousGoodRegistrationController } from '@dataManagementModule/dangerous-good-registration/controller/dangerous-good-registration.controller';
import { DangerousGoodRegistrationService } from '@dataManagementModule/dangerous-good-registration/service/dangerous-good-registration.service';
import { TransportDocumentService } from '@dataManagementModule/transport-document/service/transport-document.service';
import { TransportDocumentController } from '@dataManagementModule/transport-document/controller/transport-document.controller';
import { AmqpModule } from '@amqp/amqp.module';
import { AmqpService } from '@amqp/service/amqp.service';
import { Queues } from '@amqp/queues.enum';

const amqpModule = AmqpModule.register(Queues.TENDERMINT_SERVICE);
const amqpModuleBusiness = AmqpModule.register(Queues.BUSINESS_LOGIC);

@Module({
  controllers: [DangerousGoodRegistrationController, TransportDocumentController],
  providers: [DangerousGoodRegistrationService, TransportDocumentService, AmqpService],
  imports: [amqpModule, amqpModuleBusiness],
  exports: [amqpModule, amqpModuleBusiness],
})
export class DataManagementModule {}
