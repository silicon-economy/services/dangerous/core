/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import * as ErrorStrings from '../../../resources/error.strings.json';
import { TransportDocumentService } from '@dataManagementModule/transport-document/service/transport-document.service';
import {
  AcceptDangerousGoodRegistrationDto,
  SaveDangerousGoodRegistrationDto,
} from '@core/api-interfaces/lib/dtos/data-management';
import {
  QueryAllDangerousGoodRegistrationResponse,
  QueryGetDangerousGoodRegistrationResponse,
} from '@core/api-interfaces/lib/dtos/data-management/dangerous-good-registration/dangerous-good-registration-response.dto';
import { CalculateExemptionRequestDto, CalculateExemptionResponseDto, SingleRequest } from '@core/api-interfaces';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order-position-with-id';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { cloneDeep, isEqual } from 'lodash';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order-with-id';
import { v4 as uuid } from 'uuid';
import {
  OrderPositionStatus,
  TransportDocumentStatus,
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/status.enum';
import { LogEntry } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/log-entry';
import { AcceptanceCriteria } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/acceptance-criteria/acceptance-criteria';
import { FreightIdsIncluded } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/freight-ids-included';
import { AmqpService } from '@amqp/service/amqp.service';
import {
  MessagePatternsBusinessLogic,
  MessagePatternsTendermintDangerousGoodRegistration,
} from '@amqp/message-patterns.enum';
import { Queues } from '@amqp/queues.enum';
import { Company } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/company';

@Injectable()
export class DangerousGoodRegistrationService {
  private readonly logger = new Logger(DangerousGoodRegistrationService.name);

  constructor(
    @Inject(Queues.BUSINESS_LOGIC) public readonly businessClient: ClientProxy,
    public readonly configService: ConfigService,
    public readonly amqpService: AmqpService,
    public readonly transportDocumentService: TransportDocumentService
  ) {}

  /**
   * Creates a transport document and updates its status to `transport` immediately, adds the corresponding new log entries
   * and saves it to the blockchain.
   *
   * @param dto - Accept dangerous good registration dto
   * @returns The created and released transport document
   */
  async acceptDangerousGoodRegistration(dto: AcceptDangerousGoodRegistrationDto): Promise<SaveGoodsDataDto> {
    this.logger.log('Start accept dangerous good registration');
    this.logger.debug('Prepare saveGoodDataDto');
    let transportDocument: SaveGoodsDataDto = {} as SaveGoodsDataDto;
    const currentDate = Date.now();
    let foundTransportDocument = true;

    const dangerousGoodRegistration: SaveDangerousGoodRegistrationDto = await this.getDangerousGoodRegistration(
      dto.dangerousGoodRegistrationId
    );

    const firstFreightOrder = dangerousGoodRegistration.freight.orders[0];
    this.logger.debug('Filter for accepted order positions');
    const acceptedOrderPositions = firstFreightOrder.orderPositions.filter((orderPosition) =>
      dto.acceptedOrderPositionIds.some((id) => id == orderPosition.id)
    );

    const requests: SingleRequest[] = [];
    this.logger.debug('Create/Push requests based on accepted order positions');
    acceptedOrderPositions.forEach((element) => {
      requests.push({
        individualAmount: element.individualAmount,
        inputString: element.dangerousGood.description,
        quantity: element.quantity,
        unit: element.unit,
      });
    });

    const businessLogicCalculateExemptionRequestDto: CalculateExemptionRequestDto = {
      consignees: [{ requests: requests }],
    };
    dto.newExemptionValues = await this.calculateExemption(businessLogicCalculateExemptionRequestDto);

    // Remove dangerous goods from dangerous goods registration if they are accepted
    const lostTransportPoints = this.extractAcceptedOrderPositionAndCalculateLostTransportPoints(
      dto,
      firstFreightOrder
    );

    try {
      transportDocument = await this.transportDocumentService.getTransportDocument(dto.transportDocumentId);
    } catch (e) {
      foundTransportDocument = false;
    }

    if (foundTransportDocument) {
      this.logger.debug('Transport document with id ' + dto.transportDocumentId + 'found');
      transportDocument = this.updateTransportDocument(
        transportDocument,
        dto,
        firstFreightOrder,
        acceptedOrderPositions
      );
    } else {
      this.logger.debug('Transport document with id ' + dto.transportDocumentId + ' does not exist');
      transportDocument = await this.createTransportDocument(
        dangerousGoodRegistration,
        acceptedOrderPositions,
        dto,
        currentDate
      );
    }

    const finalTransportDocument = await this.handleDangerousGoodRegistration(
      transportDocument,
      dangerousGoodRegistration,
      lostTransportPoints
    );
    this.logger.log('Finnish accept dangerous good registration');
    return finalTransportDocument;
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private async handleDangerousGoodRegistration(
    transportDocument: SaveGoodsDataDto,
    dangerousGoodRegistration: SaveDangerousGoodRegistrationDto,
    lostTransportPoints: number
  ) {
    const finalTransportDocument = await this.transportDocumentService.updateTransportDocument(transportDocument);

    // Update the dangerous good registration
    if (dangerousGoodRegistration.freight.orders[0].orderPositions.length == 0) {
      this.logger.debug('Delete dangerous good registration, because there are no unprocessed order positions left');
      await this.deleteDangerousGoodRegistrationFromWorldState(dangerousGoodRegistration.id);
    } else {
      this.logger.debug('Update total transport points of dangerous good registration');
      dangerousGoodRegistration.freight.totalTransportPoints -= lostTransportPoints;
      await this.updateDangerousGoodRegistration({
        id: dangerousGoodRegistration.id,
        consignor: dangerousGoodRegistration.consignor,
        freight: dangerousGoodRegistration.freight,
        createdDate: dangerousGoodRegistration.createdDate,
      } as SaveDangerousGoodRegistrationDto);
    }

    return finalTransportDocument;
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private async createTransportDocument(
    dangerousGoodRegistration: SaveDangerousGoodRegistrationDto,
    acceptedOrderPositions: OrderPositionWithId[],
    dto: AcceptDangerousGoodRegistrationDto,
    currentDate: number
  ) {
    this.logger.log('Create new transport document based off dangerous good registration');
    const author = 'Bernd Beförderer';

    const newFreight: FreightIdsIncluded = cloneDeep(dangerousGoodRegistration.freight);
    newFreight.orders[0].orderPositions = acceptedOrderPositions;
    newFreight.totalTransportPoints = dto.newExemptionValues.totalTransportCategoryPoints;
    newFreight.additionalInformation = dto.newExemptionValues.exemptionStatus;
    const transportDocument = await this.transportDocumentService.saveTransportDocument({
      consignor: dangerousGoodRegistration.consignor,
      freight: newFreight,
      carrier: dto.carrier,
      status: TransportDocumentStatus.created,
      createdDate: currentDate,
      id: '',
      logEntries: null,
    } as SaveGoodsDataDto);
    if (transportDocument.status == TransportDocumentStatus.created) {
      for (const order of transportDocument.freight.orders) {
        for (let orderPositionId = 0; orderPositionId < order.orderPositions.length; orderPositionId++) {
          this.logger.debug(
            'change status of created transport document and update the order-position ids, so they stay the same as in the dangerous good registration'
          );
          order.orderPositions[orderPositionId].id = acceptedOrderPositions[orderPositionId].id;
          order.orderPositions[orderPositionId].logEntries.push({
            status: OrderPositionStatus.accepted_by_carrier,
            date: currentDate,
            description: '',
            acceptanceCriteria: {
              comment: '',
              visualInspectionCarrierCriteria: {
                acceptTransportability: true,
                acceptLabeling: true,
              },
            } as AcceptanceCriteria,
          } as LogEntry);
        }
      }

      const logEntryCreated: LogEntry = {
        status: TransportDocumentStatus.created,
        date: currentDate,
        author: author,
      };

      const logEntryTransport: LogEntry = {
        status: TransportDocumentStatus.transport,
        date: Date.now(),
        author: author,
      };

      transportDocument.logEntries.push(logEntryCreated);
      transportDocument.logEntries.push(logEntryTransport);
      transportDocument.status = logEntryTransport.status;
      transportDocument.lastUpdate = currentDate;
    }
    this.logger.log('Finish create transport document');
    this.logger.verbose('Created transport document: ' + JSON.stringify(transportDocument));

    return transportDocument;
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private updateTransportDocument(
    transportDocument: SaveGoodsDataDto,
    dto: AcceptDangerousGoodRegistrationDto,
    firstFreightOrder: OrderWithId,
    acceptedOrderPositions: OrderPositionWithId[]
  ): SaveGoodsDataDto {
    this.logger.log('Update transport document');
    const registrationConsignee: Company = firstFreightOrder.consignee;
    const updatedTransportDocument: SaveGoodsDataDto = cloneDeep(transportDocument);
    updatedTransportDocument.freight.totalTransportPoints += dto.newExemptionValues.totalTransportCategoryPoints;
    updatedTransportDocument.freight.additionalInformation = dto.newExemptionValues.exemptionStatus;
    let foundEquivalentConsignee = false;

    // Check for each order if the consignee is identical with the one of the dangerous good registration
    // If true => Check if order position can be listed together
    // (i.e., Same dg, package, packaging code, individual amount)
    this.logger.debug(
      'Check for each order if the consignee is identical with the one of the dangerous good registration'
    );
    updatedTransportDocument.freight.orders.forEach((order: OrderWithId) => {
      if (isEqual(order.consignee, registrationConsignee)) {
        foundEquivalentConsignee = true;
        for (const acceptedOrderPosition of acceptedOrderPositions) {
          let foundEquivalentOrderPosition = false;
          foundEquivalentOrderPosition = this.checkForEquivalentOrderPosition(
            order,
            acceptedOrderPosition,
            foundEquivalentOrderPosition
          );
          // If it cannot be listed together, add it as a new order position
          if (!foundEquivalentOrderPosition) {
            order.orderPositions.push(acceptedOrderPosition);
          }
        }
      }
    });

    if (!foundEquivalentConsignee) {
      this.logger.debug('No equivalent consignee found; create new order');
      updatedTransportDocument.freight.orders.push({
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-assignment
        id: uuid(),
        consignee: firstFreightOrder.consignee,
        orderPositions: acceptedOrderPositions,
      } as OrderWithId);
    }

    return updatedTransportDocument;
  }

  private checkForEquivalentOrderPosition(
    order: OrderWithId,
    acceptedOrderPosition: OrderPositionWithId,
    foundEquivalentOrderPosition: boolean
  ) {
    for (const orderPosition of order.orderPositions) {
      if (this.isIdenticalTransportGood(acceptedOrderPosition, orderPosition)) {
        foundEquivalentOrderPosition = true;
        orderPosition.quantity = orderPosition.quantity + acceptedOrderPosition.quantity;
        orderPosition.transportPoints = orderPosition.transportPoints + acceptedOrderPosition.transportPoints;
        orderPosition.totalAmount = orderPosition.totalAmount + acceptedOrderPosition.totalAmount;
        break;
      }
    }
    return foundEquivalentOrderPosition;
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private isIdenticalTransportGood(acceptedOrderPosition: OrderPositionWithId, orderPosition: OrderPositionWithId) {
    return (
      isEqual(acceptedOrderPosition.dangerousGood, orderPosition.dangerousGood) &&
      acceptedOrderPosition.package == orderPosition.package &&
      acceptedOrderPosition.packagingCode == orderPosition.packagingCode &&
      acceptedOrderPosition.individualAmount == orderPosition.individualAmount
    );
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private extractAcceptedOrderPositionAndCalculateLostTransportPoints(
    acceptDangerousGoodRegistrationDto: AcceptDangerousGoodRegistrationDto,
    firstFreightOrder: OrderWithId
  ) {
    this.logger.debug('Extract accepted order positions and calculate lost transport points');
    let lostTransportPoints = 0;

    acceptDangerousGoodRegistrationDto.acceptedOrderPositionIds.forEach((acceptedOrderPositionId) => {
      firstFreightOrder.orderPositions.forEach((orderPosition) => {
        if (acceptedOrderPositionId == orderPosition.id) {
          lostTransportPoints += orderPosition.transportPoints;
          const index = firstFreightOrder.orderPositions.indexOf(orderPosition, 0);
          if (index > -1) {
            firstFreightOrder.orderPositions.splice(index, 1);
          }
        }
      });
    });

    return lostTransportPoints;
  }

  /**
   * Returns the dangerous good registration with the id `dangerousGoodRegistrationId`.
   *
   * @param dangerousGoodRegistrationId - Dangerous good registration id
   */
  async getDangerousGoodRegistration(dangerousGoodRegistrationId: string): Promise<SaveDangerousGoodRegistrationDto> {
    this.logger.debug('Send AQMP request to tendermintService to GET the dangerous good registration by its id');
    this.logger.verbose('dangerousGoodRegistrationId: ' + dangerousGoodRegistrationId);
    try {
      return (
        (await this.amqpService.sendToAmqpBroker(
          MessagePatternsTendermintDangerousGoodRegistration.GET_DANGEROUS_GOOD_REGISTRATION,
          dangerousGoodRegistrationId,
          QueryGetDangerousGoodRegistrationResponse
        )) as QueryGetDangerousGoodRegistrationResponse
      ).dangerousGoodRegistration;
    } catch (e) {
      this.logger.error(e);
      throw new NotFoundException(ErrorStrings.NOT_FOUND);
    }
  }

  /**
   * Returns all dangerous good registrations.
   *
   * @returns Promise\<SaveDangerousGoodRegistrationDto[]\>
   */
  async getAllDangerousGoodRegistrations(): Promise<SaveDangerousGoodRegistrationDto[]> {
    this.logger.debug('Send AQMP request to tendermintService to get all dangerous good registration');
    try {
      const dangerousGoodRegistrations: SaveDangerousGoodRegistrationDto[] = (
        (await this.amqpService.sendToAmqpBroker(
          MessagePatternsTendermintDangerousGoodRegistration.GET_ALL_DANGEROUS_GOOD_REGISTRATIONS,
          <null>{},
          QueryAllDangerousGoodRegistrationResponse
        )) as QueryAllDangerousGoodRegistrationResponse
      ).dangerousGoodRegistrations;
      dangerousGoodRegistrations.sort(
        (dangerousGoodRegistrationA, dangerousGoodRegistrationB) =>
          dangerousGoodRegistrationB.lastUpdate - dangerousGoodRegistrationA.lastUpdate
      );
      return dangerousGoodRegistrations;
    } catch (e) {
      this.logger.error(e);
      throw new NotFoundException(ErrorStrings.NOT_FOUND);
    }
  }

  /**
   * Saves a dangerous good registration to the blockchain. After saving, the dangerous good registration id is set to
   * the id given by the tendermint service.
   *
   * @param saveDangerousGoodRegistrationDto - Save dangerous good registration dto
   */
  async saveDangerousGoodRegistration(
    saveDangerousGoodRegistrationDto: SaveDangerousGoodRegistrationDto
  ): Promise<SaveDangerousGoodRegistrationDto> {
    this.logger.log('Save dangerous good registration');
    try {
      return (
        (await this.amqpService.sendToAmqpBroker(
          MessagePatternsTendermintDangerousGoodRegistration.CREATE_DANGEROUS_GOOD_REGISTRATION,
          saveDangerousGoodRegistrationDto,
          QueryGetDangerousGoodRegistrationResponse
        )) as QueryGetDangerousGoodRegistrationResponse
      ).dangerousGoodRegistration;
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(ErrorStrings.ERROR_SAVE);
    }
  }

  /**
   * Updates an existing dangerous good registration by providing its id. (Consider first checking if it exists and then
   * perform the update to give the user proper feedback.)
   *
   * @param dangerousGoodRegistrationDto - Dangerous good registration dto
   */
  async updateDangerousGoodRegistration(
    dangerousGoodRegistrationDto: SaveDangerousGoodRegistrationDto
  ): Promise<SaveDangerousGoodRegistrationDto> {
    this.logger.log('Update dangerous good registration');
    try {
      dangerousGoodRegistrationDto.lastUpdate = new Date().getTime();
      await this.getDangerousGoodRegistration(dangerousGoodRegistrationDto.id);
      return (
        (await this.amqpService.sendToAmqpBroker(
          MessagePatternsTendermintDangerousGoodRegistration.UPDATE_DANGEROUS_GOOD_REGISTRATION,
          dangerousGoodRegistrationDto,
          QueryGetDangerousGoodRegistrationResponse
        )) as QueryGetDangerousGoodRegistrationResponse
      ).dangerousGoodRegistration;
    } catch (e) {
      this.logger.error(e);
      throw new NotFoundException(ErrorStrings.NOT_FOUND_UPDATE);
    }
  }

  /**
   * Deletes a dangerous good registration from the blockchain's world state.
   *
   * @param dangerousGoodRegistrationId - Dangerous good registration id
   */
  async deleteDangerousGoodRegistrationFromWorldState(dangerousGoodRegistrationId: string) {
    this.logger.log('Delete dangerous good registration from world state');
    try {
      await this.getDangerousGoodRegistration(dangerousGoodRegistrationId);
    } catch (e) {
      this.logger.error(e);
      throw new NotFoundException(ErrorStrings.NOT_FOUND);
    }
    await this.amqpService.sendToAmqpBroker(
      MessagePatternsTendermintDangerousGoodRegistration.DELETE_DANGEROUS_GOOD_REGISTRATION,
      dangerousGoodRegistrationId
    );
  }

  /**
   * Returns thousand-point value for given adrInformation.
   *
   * @param adrInformation - ADR information
   * @returns Promise<CalculateExemptionResponseDto>
   */
  private async calculateExemption(
    adrInformation: CalculateExemptionRequestDto
  ): Promise<CalculateExemptionResponseDto> {
    this.logger.debug('Send AMQP request to business logic service to calculate exemption');
    this.logger.verbose('adrInformation: ' + JSON.stringify(adrInformation));
    return firstValueFrom(this.businessClient.send(MessagePatternsBusinessLogic.CALCULATE_EXEMPTION, adrInformation));
  }
}
