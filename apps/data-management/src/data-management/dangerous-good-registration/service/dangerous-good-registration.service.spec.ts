/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DangerousGoodRegistrationController } from '@dataManagementModule/dangerous-good-registration/controller/dangerous-good-registration.controller';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import fs from 'fs';
import path from 'path';
import { DangerousGoodRegistrationService } from '@dataManagementModule/dangerous-good-registration/service/dangerous-good-registration.service';
import { TransportDocumentService } from '@dataManagementModule/transport-document/service/transport-document.service';
import { CalculateExemptionResponseDto, ConsigneeResponse, ExemptionStatus } from '@core/api-interfaces';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { AcceptDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/data-management';
import { AmqpService } from '@amqp/service/amqp.service';
import { AmqpModule } from '@amqp/amqp.module';
import { Queues } from '@amqp/queues.enum';
import { TransportDocumentStatus } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';
import { mockDangerousGoodRegistration } from '@core/api-interfaces/lib/mocks/dangerous-good-registration/dangerous-good-registration.mock';
import { NotFoundException } from '@nestjs/common';
import { of } from 'rxjs';
import * as ErrorStrings from '../../../resources/error.strings.json';

describe('DangerousGoodRegistrationService', () => {
  let dangerousGoodRegistrationService: DangerousGoodRegistrationService;
  let transportDocumentsMock: SaveGoodsDataDto[] = JSON.parse(
    fs.readFileSync(path.resolve(__dirname, '../../../resources/testdata/dangerousGoodsList.json'), 'utf-8')
  ) as SaveGoodsDataDto[];

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [DangerousGoodRegistrationController],
      providers: [DangerousGoodRegistrationService, TransportDocumentService, AmqpService],
      imports: [
        AmqpModule.register(Queues.TENDERMINT_SERVICE),
        AmqpModule.register(Queues.BUSINESS_LOGIC),
        ConfigModule,
      ],
    }).compile();

    dangerousGoodRegistrationService = moduleRef.get<DangerousGoodRegistrationService>(
      DangerousGoodRegistrationService
    );
  });

  afterEach(() => {
    jest.clearAllMocks();
    transportDocumentsMock = JSON.parse(
      fs.readFileSync(path.resolve(__dirname, '../../../resources/testdata/dangerousGoodsList.json'), 'utf-8')
    ) as SaveGoodsDataDto[];
  });

  it('should acceptDangerousGoodRegistration', async () => {
    const mockDocument: SaveGoodsDataDto = transportDocumentsMock[0];
    const acceptDangerousGoodRegistrationDto: AcceptDangerousGoodRegistrationDto = {
      dangerousGoodRegistrationId: 'BP000005-20210819',
      acceptedOrderPositionIds: [1],
      transportationInstructions: 'Keine Transporthinweise.',
      carrier: {
        driver: 'Timothy Muleman',
        licensePlate: 'AD-R-1337',
        name: 'Mühlig Motors',
      },
    };
    jest
      .spyOn(DangerousGoodRegistrationService.prototype, 'getDangerousGoodRegistration')
      .mockReturnValueOnce(Promise.resolve(mockDocument));
    jest.spyOn(dangerousGoodRegistrationService.businessClient, 'send').mockReturnValue(
      of(
        new CalculateExemptionResponseDto(
          [
            {
              responses: [
                {
                  unNumber: mockDocument.freight.orders[0].orderPositions[0].dangerousGood.unNumber,
                  totalAmount: mockDocument.freight.orders[0].orderPositions[0].totalAmount,
                  transportCategoryPoints: mockDocument.freight.orders[0].orderPositions[0].transportPoints,
                  exemptionStatus: ExemptionStatus['Beförderung ohne Freistellung nach ADR 1.1.3.6'] as ExemptionStatus,
                },
              ],
            },
          ] as ConsigneeResponse[],
          mockDocument.freight.totalTransportPoints,
          ExemptionStatus['Beförderung ohne Freistellung nach ADR 1.1.3.6'] as ExemptionStatus
        )
      )
    );

    mockDocument.carrier = acceptDangerousGoodRegistrationDto.carrier;
    mockDocument.freight.transportationInstructions = acceptDangerousGoodRegistrationDto.transportationInstructions;

    jest
      .spyOn(AmqpService.prototype, 'sendToAmqpBroker')
      .mockReturnValue(Promise.resolve({ transportDocument: mockDocument }));

    expect(
      await dangerousGoodRegistrationService.acceptDangerousGoodRegistration(acceptDangerousGoodRegistrationDto)
    ).toEqual(mockDocument);
  });

  it('should acceptDangerousGoodRegistration with no transport document found', async () => {
    const mockDocument: SaveGoodsDataDto = transportDocumentsMock[0];
    const acceptDangerousGoodRegistrationDto: AcceptDangerousGoodRegistrationDto = {
      dangerousGoodRegistrationId: 'BP000005-20210819',
      acceptedOrderPositionIds: [1],
      transportationInstructions: 'Keine Transporthinweise.',
      carrier: {
        driver: 'Timothy Muleman',
        licensePlate: 'AD-R-1337',
        name: 'Mühlig Motors',
      },
    };
    jest
      .spyOn(DangerousGoodRegistrationService.prototype, 'getDangerousGoodRegistration')
      .mockReturnValueOnce(Promise.resolve(mockDocument));

    jest.spyOn(TransportDocumentService.prototype, 'getTransportDocument').mockRejectedValue(new Error(''));
    jest
      .spyOn(TransportDocumentService.prototype, 'saveTransportDocument')
      .mockReturnValueOnce(Promise.resolve(mockDocument));
    mockDocument.status = TransportDocumentStatus.created;
    jest
      .spyOn(TransportDocumentService.prototype, 'updateTransportDocument')
      .mockReturnValueOnce(Promise.resolve(mockDocument));

    jest.spyOn(dangerousGoodRegistrationService.businessClient, 'send').mockReturnValue(
      of(
        new CalculateExemptionResponseDto(
          [
            {
              responses: [
                {
                  unNumber: mockDocument.freight.orders[0].orderPositions[0].dangerousGood.unNumber,
                  totalAmount: mockDocument.freight.orders[0].orderPositions[0].totalAmount,
                  transportCategoryPoints: mockDocument.freight.orders[0].orderPositions[0].transportPoints,
                  exemptionStatus: ExemptionStatus['Beförderung ohne Freistellung nach ADR 1.1.3.6'] as ExemptionStatus,
                },
              ],
            },
          ] as ConsigneeResponse[],
          mockDocument.freight.totalTransportPoints,
          ExemptionStatus['Beförderung ohne Freistellung nach ADR 1.1.3.6'] as ExemptionStatus
        )
      )
    );
    mockDocument.carrier = acceptDangerousGoodRegistrationDto.carrier;
    mockDocument.freight.transportationInstructions = acceptDangerousGoodRegistrationDto.transportationInstructions;

    jest
      .spyOn(AmqpService.prototype, 'sendToAmqpBroker')
      .mockReturnValue(Promise.resolve({ transportDocument: mockDocument }));

    expect(
      await dangerousGoodRegistrationService.acceptDangerousGoodRegistration(acceptDangerousGoodRegistrationDto)
    ).toEqual(mockDocument);
  });

  it('should call getDangerousGoodRegistration', async () => {
    const sendToAmqpBrokerSpy = jest
      .spyOn(AmqpService.prototype, 'sendToAmqpBroker')
      .mockReturnValueOnce(Promise.resolve({ dangerousGoodRegistration: undefined }));
    await dangerousGoodRegistrationService.getDangerousGoodRegistration('foobar');

    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
  });

  it('should call getDangerousGoodRegistration and throw error', async () => {
    const sendToAmqpBrokerSpy = jest.spyOn(AmqpService.prototype, 'sendToAmqpBroker').mockRejectedValue(new Error());
    await expect(async () => {
      await dangerousGoodRegistrationService.getDangerousGoodRegistration('foobar');
    }).rejects.toThrow(new NotFoundException(ErrorStrings.NOT_FOUND));
    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
  });

  it('should call getAllDangerousGoodRegistrations', async () => {
    const sendToAmqpBrokerSpy = jest
      .spyOn(dangerousGoodRegistrationService['amqpService'], 'sendToAmqpBroker')
      .mockReturnValue(Promise.resolve({ dangerousGoodRegistrations: [{ lastUpdate: 1 }, { lastUpdate: 2 }] }));
    expect(await dangerousGoodRegistrationService.getAllDangerousGoodRegistrations()).toEqual([
      { lastUpdate: 2 },
      { lastUpdate: 1 },
    ]);
    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
  });

  it('should call getAllDangerousGoodRegistrations and throw error', async () => {
    jest.resetAllMocks();
    const sendToAmqpBrokerSpy = jest
      .spyOn(dangerousGoodRegistrationService['amqpService'], 'sendToAmqpBroker')
      .mockRejectedValue(new Error());
    await expect(async () => {
      await dangerousGoodRegistrationService.getAllDangerousGoodRegistrations();
    }).rejects.toThrow(new NotFoundException('No transport document with that Id found in blockchain data storage.'));
    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
  });

  it('should call saveDangerousGoodRegistration', async () => {
    const sendToAmqpBrokerSpy = jest
      .spyOn(dangerousGoodRegistrationService['amqpService'], 'sendToAmqpBroker')
      .mockReturnValue(Promise.resolve({ dangerousGoodRegistration: undefined }));
    await dangerousGoodRegistrationService.saveDangerousGoodRegistration(mockDangerousGoodRegistration);
    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
  });

  it('should call saveDangerousGoodRegistration and throw an error', async () => {
    const sendToAmqpBrokerSpy = jest
      .spyOn(dangerousGoodRegistrationService['amqpService'], 'sendToAmqpBroker')
      .mockRejectedValue(new Error());
    await expect(async () => {
      await dangerousGoodRegistrationService.saveDangerousGoodRegistration(mockDangerousGoodRegistration);
    }).rejects.toThrow(new NotFoundException('Saving failed. Check your Input.'));
    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
  });

  it('should call updateDangerousGoodRegistration', async () => {
    const sendToAmqpBrokerSpy = jest
      .spyOn(dangerousGoodRegistrationService['amqpService'], 'sendToAmqpBroker')
      .mockReturnValue(Promise.resolve({ dangerousGoodRegistration: undefined }));
    const getDangerousGoodRegistrationSpy = jest
      .spyOn(dangerousGoodRegistrationService, 'getDangerousGoodRegistration')
      .mockReturnValueOnce(Promise.resolve(mockDangerousGoodRegistration));
    await dangerousGoodRegistrationService.updateDangerousGoodRegistration(mockDangerousGoodRegistration);
    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
    expect(getDangerousGoodRegistrationSpy).toHaveBeenCalled();
  });

  it('should call updateDangerousGoodRegistration and throw an error', async () => {
    const sendToAmqpBrokerSpy = jest
      .spyOn(dangerousGoodRegistrationService['amqpService'], 'sendToAmqpBroker')
      .mockRejectedValue(new Error());
    const getDangerousGoodRegistrationSpy = jest
      .spyOn(dangerousGoodRegistrationService, 'getDangerousGoodRegistration')
      .mockReturnValueOnce(Promise.resolve(mockDangerousGoodRegistration));
    await expect(async () => {
      await dangerousGoodRegistrationService.updateDangerousGoodRegistration(mockDangerousGoodRegistration);
    }).rejects.toThrow(new NotFoundException('No transport documents with that Id found in blockchain data storage.'));
    expect(sendToAmqpBrokerSpy).toHaveBeenCalled();
    expect(getDangerousGoodRegistrationSpy).toHaveBeenCalled();
  });
});
