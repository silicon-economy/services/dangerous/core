/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { DangerousGoodRegistrationService } from '@dataManagementModule/dangerous-good-registration/service/dangerous-good-registration.service';

import {
  AcceptDangerousGoodRegistrationDto,
  SaveDangerousGoodRegistrationDto,
} from '@core/api-interfaces/lib/dtos/data-management';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { MessagePatternsDangerousGoodRegistration, MessagePatternsDataManagement } from '@amqp/message-patterns.enum';

@Controller()
export class DangerousGoodRegistrationController {
  constructor(private dangerousGoodRegistrationService: DangerousGoodRegistrationService) {}

  /**
   * Accepts a dangerous good registration.
   *
   * @param acceptDangerousGoodRegistrationDto - Accept dangerous goods registration dto
   * @returns Promise\<SaveGoodsDataDto\>
   */
  @MessagePattern(MessagePatternsDataManagement.ACCEPT_DANGEROUS_GOOD_REGISTRATION)
  async acceptDangerousGoodRegistration(
    acceptDangerousGoodRegistrationDto: AcceptDangerousGoodRegistrationDto
  ): Promise<SaveGoodsDataDto> {
    return this.dangerousGoodRegistrationService.acceptDangerousGoodRegistration(acceptDangerousGoodRegistrationDto);
  }

  // Dangerous Good Registration message patterns
  /**
   * Returns a dangerous good registration by its id.
   *
   * @param dangerousGoodRegistrationId - Dangerous good registration id
   * @returns Promise\<SaveDangerousGoodRegistrationDto\>
   */
  @MessagePattern(MessagePatternsDangerousGoodRegistration.GET_DANGEROUS_GOOD_REGISTRATION)
  async getDangerousGoodRegistrationById(
    dangerousGoodRegistrationId: string
  ): Promise<SaveDangerousGoodRegistrationDto> {
    return this.dangerousGoodRegistrationService.getDangerousGoodRegistration(dangerousGoodRegistrationId);
  }

  /**
   * Returns all dangerous good registrations.
   *
   * @returns  Promise\<SaveDangerousGoodRegistrationDto[]\>
   */
  @MessagePattern(MessagePatternsDangerousGoodRegistration.GET_ALL_DANGEROUS_GOOD_REGISTRATIONS)
  async getAllDangerousGoodRegistrations(): Promise<SaveDangerousGoodRegistrationDto[]> {
    return this.dangerousGoodRegistrationService.getAllDangerousGoodRegistrations();
  }

  /**
   * Creates a new dangerous good registration.
   *
   * @param dangerousGoodRegistrationDto - Dangerous good registration dto
   * @returns Promise\<SaveDangerousGoodRegistrationDto\>
   */
  @MessagePattern(MessagePatternsDangerousGoodRegistration.CREATE_DANGEROUS_GOOD_REGISTRATION)
  async createDangerousGoodRegistration(
    dangerousGoodRegistrationDto: SaveDangerousGoodRegistrationDto
  ): Promise<SaveDangerousGoodRegistrationDto> {
    return this.dangerousGoodRegistrationService.saveDangerousGoodRegistration(dangerousGoodRegistrationDto);
  }

  /**
   * Updates a dangerous good registration according to a given Dto.
   *
   * @param dangerousGoodRegistrationDto - Dangerous good registration dto
   * @returns Promise\<SaveDangerousGoodRegistrationDto\>
   */
  @MessagePattern(MessagePatternsDangerousGoodRegistration.UPDATE_DANGEROUS_GOOD_REGISTRATION)
  async updateDangerousGoodRegistration(
    dangerousGoodRegistrationDto: SaveDangerousGoodRegistrationDto
  ): Promise<SaveDangerousGoodRegistrationDto> {
    return this.dangerousGoodRegistrationService.updateDangerousGoodRegistration(dangerousGoodRegistrationDto);
  }

  /**
   * Removes a dangerous good registration given by its id from the blockchain's world state.
   *
   * @param dangerousGoodRegistrationId - Dangerous good registration id
   * @returns Promise\<void\>
   */
  @MessagePattern(MessagePatternsDangerousGoodRegistration.DELETE_DANGEROUS_GOOD_REGISTRATION)
  async deleteDangerousGoodRegistrationFromWorldState(dangerousGoodRegistrationId: string): Promise<void> {
    return this.dangerousGoodRegistrationService.deleteDangerousGoodRegistrationFromWorldState(
      dangerousGoodRegistrationId
    );
  }
}
