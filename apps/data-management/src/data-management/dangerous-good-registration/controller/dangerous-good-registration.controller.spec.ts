/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { DangerousGoodRegistrationService } from '@dataManagementModule/dangerous-good-registration/service/dangerous-good-registration.service';
import { DangerousGoodRegistrationController } from '@dataManagementModule/dangerous-good-registration/controller/dangerous-good-registration.controller';
import { TransportDocumentService } from '@dataManagementModule/transport-document/service/transport-document.service';
import { AmqpService } from '@amqp/service/amqp.service';
import { AmqpModule } from '@amqp/amqp.module';
import { Queues } from '@amqp/queues.enum';

describe('DangerousGoodRegistrationController', () => {
  let dangerousGoodRegistrationController: DangerousGoodRegistrationController;
  let dangerousGoodRegistrationService: DangerousGoodRegistrationService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [DangerousGoodRegistrationController],
      providers: [DangerousGoodRegistrationService, TransportDocumentService, AmqpService],
      imports: [
        AmqpModule.register(Queues.TENDERMINT_SERVICE),
        AmqpModule.register(Queues.BUSINESS_LOGIC),
        ConfigModule,
      ],
    }).compile();

    dangerousGoodRegistrationController = moduleRef.get<DangerousGoodRegistrationController>(
      DangerousGoodRegistrationController
    );
    dangerousGoodRegistrationService = moduleRef.get<DangerousGoodRegistrationService>(
      DangerousGoodRegistrationService
    );
  });

  it('should be defined', () => {
    expect(dangerousGoodRegistrationController).toBeDefined();
    expect(dangerousGoodRegistrationService).toBeDefined();
    expect(dangerousGoodRegistrationController.acceptDangerousGoodRegistration).toBeDefined();
    expect(dangerousGoodRegistrationController.getDangerousGoodRegistrationById).toBeDefined();
    expect(dangerousGoodRegistrationController.getAllDangerousGoodRegistrations).toBeDefined();
    expect(dangerousGoodRegistrationController.createDangerousGoodRegistration).toBeDefined();
    expect(dangerousGoodRegistrationController.updateDangerousGoodRegistration).toBeDefined();
    expect(dangerousGoodRegistrationController.deleteDangerousGoodRegistrationFromWorldState).toBeDefined();
  });
});
