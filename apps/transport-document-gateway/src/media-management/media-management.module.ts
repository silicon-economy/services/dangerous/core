/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { MediaManagementController } from './controller/media-management.controller';
import { MediaManagementService } from './service/media-management.service';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

const amqpModule = AmqpModule.register(Queues.MEDIA_MANAGEMENT);

@Module({
  imports: [amqpModule],
  providers: [MediaManagementService],
  controllers: [MediaManagementController],
  exports: [MediaManagementService, amqpModule],
})
export class MediaManagementModule {}
