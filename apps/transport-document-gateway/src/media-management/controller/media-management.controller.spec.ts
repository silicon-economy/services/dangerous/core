/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { MediaManagementService } from '../service/media-management.service';
import { MediaManagementController } from './media-management.controller';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';

describe('MediaManagementController', () => {
  let controller: MediaManagementController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AmqpModule.register(Queues.MEDIA_MANAGEMENT)],
      controllers: [MediaManagementController],
      providers: [MediaManagementService],
    }).compile();

    controller = module.get<MediaManagementController>(MediaManagementController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(controller.getPackagingLabelsByTransportDocumentId).toBeDefined();
  });
});
