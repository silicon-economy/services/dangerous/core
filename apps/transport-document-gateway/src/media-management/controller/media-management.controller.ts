/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller, Get, Header, Param, Res } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { MediaManagementService } from '../service/media-management.service';
import dataUriToBuffer from 'data-uri-to-buffer';

@ApiTags('Media Management')
@ApiBearerAuth('JWT-auth')
@Controller()
@ApiTags('v1')
export class MediaManagementController {
  constructor(private service: MediaManagementService) {}

  /**
   * Get packaging labels for a transport document by given id
   *
   * @param transportDocumentId - Transport Document id
   * @param response - MimeBuffer
   */
  @ApiOperation({
    summary: 'Get packaging labels for a transport document by its Id.',
    description: 'Gets packaging labels for a transport document by its Id.',
  })
  @Get('/:transportDocumentId')
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  @ApiParam({ name: 'transportDocumentId', description: 'Transport document Id' })
  @Header('Content-Type', 'application/pdf')
  @Header('Content-Disposition', 'inline; filename=hazard_label.pdf')
  async getPackagingLabelsByTransportDocumentId(
    @Param('transportDocumentId') transportDocumentId: string,
    @Res() response: Response
  ): Promise<void> {
    const pdf: ArrayBuffer = await this.service.getPackagingLabelsByTransportDocumentId(transportDocumentId);
    const buffer: dataUriToBuffer.MimeBuffer = dataUriToBuffer(Buffer.from(pdf).toString());
    response.send(buffer);
  }
}
