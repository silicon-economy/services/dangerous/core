/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import { MessagePatternsMediaManagement } from '@core/api-interfaces/lib/amqp/message-patterns.enum';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

@Injectable()
export class MediaManagementService {
  private readonly logger = new Logger(MediaManagementService.name);
  constructor(@Inject(Queues.MEDIA_MANAGEMENT) public readonly client: ClientProxy) {}

  /**
   * Gets packaging labels for a transport document by its Id.
   *
   * @param transportDocumentId - Transport document Id
   * @returns Packaging labels (Pdf)
   */
  async getPackagingLabelsByTransportDocumentId(transportDocumentId: string): Promise<ArrayBuffer> {
    this.logger.log('Get packaging labels for transport document with id ' + transportDocumentId);
    return firstValueFrom(
      this.client.send(MessagePatternsMediaManagement.GET_PDF_BY_TRANSPORT_DOCUMENT_ID, transportDocumentId)
    );
  }
}
