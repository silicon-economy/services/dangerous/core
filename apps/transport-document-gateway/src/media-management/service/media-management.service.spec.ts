/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { MediaManagementService } from './media-management.service';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';

describe('MediaManagementService', () => {
  let service: MediaManagementService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AmqpModule.register(Queues.MEDIA_MANAGEMENT)],
      providers: [MediaManagementService],
    }).compile();

    service = module.get<MediaManagementService>(MediaManagementService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(service.getPackagingLabelsByTransportDocumentId).toBeDefined();
  });
});
