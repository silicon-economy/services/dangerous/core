/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { TransportDocumentService } from '../service/transport-document.service';
import { Observable } from 'rxjs';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { CreateRequestBodyType } from '../controller/types/create-request-body-type';
import { CreateGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/create-goods-data.dto';
import { PostHeader } from '../controller/enum/post-header.enum';
import { UpdateRequestBodyType } from '../controller/types/update-request-body.type';
import { CarrierCheckCriteria } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/acceptance-criteria/carrier-check-criteria';
import { UpdateVisualInspectionCarrierDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import { UpdateVisualInspectionConsigneeDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';

import { TransportVehicleCriteria } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/acceptance-criteria/transport-vehicle-criteria';
import {
  AcceptDangerousGoodRegistrationDto,
  CarrierCheckRequestDto,
} from '@core/api-interfaces/lib/dtos/data-management';

import * as acceptDangerousGoodRegistration from '../controller/dto/dummies/accept-dangerous-good-registration.dummy';
import * as carrierCheckDtoDummy from '../controller/dto/dummies/carrier-check-dto.dummy';
import * as createGoodsDataDtoDummy from '../controller/dto/dummies/create-goods-data-dto.dummy';
import * as updateVisualInspectionCarrierDummy from '../controller/dto/dummies/update-visual-inspection-carrier.dummy';
import * as updateVisualInspectionConsigneeDummy from '../controller/dto/dummies/update-visual-inspection-consignee.dummy';
import * as vehicleInspectionDtoDummy from '../controller/dto/dummies/vehicle-inspection-dto.dummy';
import { DtoParser } from '../../../shared/dto-parser';
import { PutHeader } from '../controller/enum/put-header.enum';
import { TransportVehicleInspectionRequestDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-vehicle-inspection-request.dto';
import { JwtDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';

@Injectable()
export class TransportDocumentHandler {
  constructor(
    private transportDocumentService: TransportDocumentService,
    private dtoParser: DtoParser
  ) {}

  /**
   * Retrieves the transport document by id.
   *
   * @param transportDocumentId - The id of the transport document.
   * @param jwtDto - User information
   * @returns An Observable of type SaveGoodsDataDto.
   */
  handleGetTransportDocumentById(transportDocumentId: string, jwtDto: JwtDto): Observable<SaveGoodsDataDto> {
    let result: Observable<SaveGoodsDataDto>;
    if (jwtDto.roles.includes(UserRole.consignee)) {
      result = this.transportDocumentService.getTransportDocumentForConsignee(transportDocumentId, jwtDto);
    } else {
      result = this.transportDocumentService.getTransportDocument(transportDocumentId);
    }
    return result;
  }

  /**
   * Retrieves all transport documents.
   *
   * @param jwtDto - User information
   * @returns An Observable of an array of SaveGoodsDataDto.
   */
  handleGetAllTransportDocuments(jwtDto: JwtDto): Observable<SaveGoodsDataDto[]> {
    let result: Observable<SaveGoodsDataDto[]>;
    if (jwtDto.roles.includes(UserRole.consignee)) {
      result = this.transportDocumentService.getAllTransportDocumentsForConsignee(jwtDto);
    } else {
      result = this.transportDocumentService.getAllTransportDocuments();
    }
    return result;
  }

  /**
   * Creates a transport document.
   *
   * @param requestBody - The request body for creating the transport document.
   * @param typeOfAction - Optional. The type of action to perform.
   * @returns An Observable of type SaveGoodsDataDto.
   * @throws HttpException with status code 400 if the result is undefined.
   */
  handleCreateTransportDocument(
    requestBody: CreateRequestBodyType,
    typeOfAction?: string
  ): Observable<SaveGoodsDataDto> {
    let result: Observable<SaveGoodsDataDto>;
    switch (typeOfAction) {
      case undefined: {
        result = this.transportDocumentService.saveTransportDocument(
          this.dtoParser.parseDto(requestBody as CreateGoodsDataDto)
        );
        break;
      }
      case PostHeader[PostHeader.createAndRelease]: {
        result = this.transportDocumentService.createAndReleaseTransportDocument(
          this.dtoParser.parseDto(requestBody as CreateGoodsDataDto)
        );
        break;
      }
      case PostHeader[PostHeader.createAndTransport]: {
        if (this.isAcceptDangerousGoodRegistrationDto(requestBody)) {
          result = this.transportDocumentService.acceptDangerousGoodRegistration(requestBody);
        }
        break;
      }
      default: {
        break;
      }
    }
    if (result == undefined) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
    return result;
  }

  /**
   * Updates a transport document.
   *
   * @param requestBody - Optional. The request body for updating the transport document.
   * @param jwtDto - User information
   * @param typeOfAction - Optional. The type of action to perform.
   * @param transportDocumentId - Optional. The id of the transport document.
   * @returns An Observable of type SaveGoodsDataDto or void.
   * @throws HttpException with status code 400 if the result is undefined.
   */
  handleUpdateTransportDocument(
    requestBody?: UpdateRequestBodyType,
    jwtDto?: JwtDto,
    typeOfAction?: string,
    transportDocumentId?: string
  ): Observable<SaveGoodsDataDto | void> {
    let result: Observable<SaveGoodsDataDto | void>;
    switch (typeOfAction) {
      case undefined: {
        if (this.isCreateGoodsDataDto(requestBody)) {
          result = this.transportDocumentService.updateTransportDocument(requestBody);
        }
        break;
      }
      case PutHeader[PutHeader.carrierCheck]: {
        if (this.isCarrierCheckCriteria(requestBody)) {
          const carrierCheckRequestDto: CarrierCheckRequestDto = {
            documentId: transportDocumentId,
            carrierCheckCriteria: requestBody,
          };
          result = this.transportDocumentService.processCarrierCheck(carrierCheckRequestDto);
        }
        break;
      }
      case PutHeader[PutHeader.disable]: {
        if (transportDocumentId != undefined) {
          result = this.transportDocumentService.deleteTransportDocumentFromWorldState(transportDocumentId);
        }
        break;
      }
      case PutHeader[PutHeader.release]: {
        if (transportDocumentId != undefined) {
          result = this.transportDocumentService.releaseTransportDocument(transportDocumentId);
        }
        break;
      }
      case PutHeader[PutHeader.updateVisualInspectionCarrier]: {
        if (this.isUpdateOrderPositionCheckDto(requestBody)) {
          result = this.transportDocumentService.updateVisualInspectionCarrier(requestBody);
        }
        break;
      }
      case PutHeader[PutHeader.updateVisualInspectionConsignee]: {
        if (this.isUpdateVisualInspectionCheckDto(requestBody)) {
          result = this.transportDocumentService.updateVisualInspectionConsignee(requestBody);
        }
        break;
      }
      case PutHeader[PutHeader.vehicleInspection]: {
        if (this.isTransportVehicleCriteria(requestBody) && transportDocumentId != undefined) {
          const transportVehicleInspectionRequestDto: TransportVehicleInspectionRequestDto = {
            documentId: transportDocumentId,
            transportVehicleCriteria: requestBody,
          };
          result = this.transportDocumentService.processVehicleInspection(transportVehicleInspectionRequestDto);
        }
        break;
      }
      case PutHeader[PutHeader.confirmCarrier]: {
        if (jwtDto != undefined) {
          result = this.transportDocumentService.processCarrierConfirmation(transportDocumentId, jwtDto);
        } else {
          throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
        }
        break;
      }
      case PutHeader[PutHeader.acceptDangerousGoodRegistration]: {
        if (this.isAcceptDangerousGoodRegistrationDto(requestBody)) {
          result = this.transportDocumentService.acceptDangerousGoodRegistration(requestBody);
        }
        break;
      }
      default: {
        break;
      }
    }
    if (result == undefined) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
    return result;
  }


  /**
   * Checks if the given object is of type CarrierCheckCriteria.
   *
   * @param object - The object to check.
   * @returns True if the object is of type CarrierCheckCriteria, false otherwise.
   */
  isCarrierCheckCriteria(object: UpdateRequestBodyType): object is CarrierCheckCriteria {
    return Object.getOwnPropertyNames(carrierCheckDtoDummy.carrierCheckCriteria)
      .map((e) => {
        return e in object;
      })
      .reduce((b, c) => {
        return b && c;
      });
  }


  /**
   * Checks if the given object is of type CreateGoodsDataDto.
   *
   * @param object - The object to check.
   * @returns True if the object is of type CreateGoodsDataDto, false otherwise.
   */
  isCreateGoodsDataDto(object: UpdateRequestBodyType): object is CreateGoodsDataDto {
    return Object.getOwnPropertyNames(createGoodsDataDtoDummy.createGoodsDataDto)
      .map((e) => {
        return e in object;
      })
      .reduce((b, c) => {
        return b && c;
      });
  }


  /**
   * Checks if the given object is of type UpdateVisualInspectionCarrierDto.
   *
   * @param object - The object to check.
   * @returns True if the object is of type UpdateVisualInspectionCarrierDto, false otherwise.
   */ isUpdateOrderPositionCheckDto(object: UpdateRequestBodyType): object is UpdateVisualInspectionCarrierDto {
    return Object.getOwnPropertyNames(updateVisualInspectionCarrierDummy.updateVisualInspectionCarrierDto)
      .map((e) => {
        return e in object;
      })
      .reduce((b, c) => {
        return b && c;
      });
  }


  /**
   * Checks if the given object is of type UpdateVisualInspectionConsigneeDto.
   *
   * @param object - The object to check.
   * @returns True if the object is of type UpdateVisualInspectionConsigneeDto, false otherwise.
   */ isUpdateVisualInspectionCheckDto(object: UpdateRequestBodyType): object is UpdateVisualInspectionConsigneeDto {
    return Object.getOwnPropertyNames(updateVisualInspectionConsigneeDummy.updateVisualInspectionConsigneeDto)
      .map((e) => {
        return e in object;
      })
      .reduce((b, c) => {
        return b && c;
      });
  }


  /**
   * Checks if the given object is of type TransportVehicleCriteria.
   *
   * @param object - The object to check.
   * @returns True if the object is of type TransportVehicleCriteria, false otherwise.
   */ isTransportVehicleCriteria(object: UpdateRequestBodyType): object is TransportVehicleCriteria {
    return Object.getOwnPropertyNames(vehicleInspectionDtoDummy.vehicleInspectionDto)
      .map((e) => {
        return e in object;
      })
      .reduce((b, c) => {
        return b && c;
      });
  }


  /**
   * Checks if the given object is of type AcceptDangerousGoodRegistrationDto.
   *
   * @param object - The object to check.
   * @returns True if the object is of type AcceptDangerousGoodRegistrationDto, false otherwise.
   */ isAcceptDangerousGoodRegistrationDto(
    object: CreateRequestBodyType | UpdateRequestBodyType
  ): object is AcceptDangerousGoodRegistrationDto {
    return Object.getOwnPropertyNames(acceptDangerousGoodRegistration.acceptDangerousGoodRegistration)
      .map((e) => {
        return e in object;
      })
      .reduce((b, c) => {
        return b && c;
      });
  }
}
