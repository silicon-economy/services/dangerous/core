/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { TransportDocumentHandler } from './transport-document.handler';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { DtoParser } from '../../../shared/dto-parser';
import { TransportDocumentService } from '../service/transport-document.service';

describe('DangerousGoodRegistrationHandler', () => {
  let transportDocumentHandler: TransportDocumentHandler;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [TransportDocumentHandler, TransportDocumentService, DtoParser],
      imports: [HttpModule, ConfigModule, AmqpModule.register(Queues.DATA_MANAGEMENT)],
    }).compile();

    transportDocumentHandler = moduleRef.get<TransportDocumentHandler>(TransportDocumentHandler);
  });

  it('should be defined', () => {
    expect(transportDocumentHandler).toBeDefined();
  });
});
