/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { DtoParser } from '../../shared/dto-parser';
import { TransportDocumentController } from './controller/transport-document.controller';
import { TransportDocumentService } from './service/transport-document.service';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { TransportDocumentHandler } from './handler/transport-document.handler';

const amqpModule = AmqpModule.register(Queues.DATA_MANAGEMENT);

@Module({
  controllers: [TransportDocumentController],
  providers: [TransportDocumentHandler, TransportDocumentService, DtoParser],
  imports: [HttpModule, amqpModule, TransportDocumentModule],
  exports: [amqpModule],
})
export class TransportDocumentModule {}
