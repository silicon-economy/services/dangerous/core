/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { DtoParser } from '../../../shared/dto-parser';
import { TransportDocumentController } from '../controller/transport-document.controller';
import { TransportDocumentService } from './transport-document.service';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { TransportDocumentHandler } from '../handler/transport-document.handler';
import { JwtDto } from "@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto";
import { CreateDangerousGoodRegistrationDto } from "@core/api-interfaces/lib/dtos/data-management";
import { Order } from "@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order";
import { Freight } from "@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/freight";
import {
  CreateGoodsDataDto
} from "@core/api-interfaces/lib/dtos/data-management/transport-document/create-goods-data.dto";
import {
  mockTransportDocumentCreated
} from "@core/api-interfaces/lib/mocks/transport-document/transport-document.mock";

describe('TransportDocumentService', () => {
  let transportDocumentService: TransportDocumentService;
  let dtoParser: DtoParser;
  let amqpClientSpy;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [TransportDocumentController],
      providers: [TransportDocumentService, TransportDocumentHandler, DtoParser],
      imports: [HttpModule, ConfigModule, AmqpModule.register(Queues.DATA_MANAGEMENT)],
    }).compile();

    transportDocumentService = moduleRef.get<TransportDocumentService>(TransportDocumentService);
    dtoParser = moduleRef.get<DtoParser>(DtoParser);
    amqpClientSpy = jest.spyOn(transportDocumentService.client,'send')
  });

  it('should be defined', () => {
    expect(transportDocumentService).toBeDefined();
    expect(transportDocumentService.getTransportDocument).toBeDefined();
    expect(transportDocumentService.getTransportDocumentForConsignee).toBeDefined();
    expect(transportDocumentService.getAllTransportDocuments).toBeDefined();
    expect(transportDocumentService.getAllTransportDocumentsForConsignee).toBeDefined();
    expect(transportDocumentService.saveTransportDocument).toBeDefined();
    expect(transportDocumentService.createAndReleaseTransportDocument).toBeDefined();
    expect(transportDocumentService.deleteTransportDocumentFromWorldState).toBeDefined();
    expect(transportDocumentService.processCarrierCheck).toBeDefined();
    expect(transportDocumentService.releaseTransportDocument).toBeDefined();
    expect(transportDocumentService.processVehicleInspection).toBeDefined();
    expect(transportDocumentService.getTransportDocumentIdByOrderPositionId).toBeDefined();
    expect(transportDocumentService.processCarrierConfirmation).toBeDefined();
  });

  it('should getTransportDocument', ()=>{
    transportDocumentService.getTransportDocument('')
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should getTransportDocument for consignee', ()=>{
    transportDocumentService.getTransportDocumentForConsignee('', undefined)
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should getAllTransportDocuments', ()=>{
    transportDocumentService.getAllTransportDocuments()
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should getAllTransportDocuments for consignee', ()=>{
    transportDocumentService.getAllTransportDocumentsForConsignee(undefined)
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should saveTransportDocument', ()=>{
    transportDocumentService.saveTransportDocument(mockTransportDocumentCreated)
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should createAndReleaseTransportDocument', ()=>{
    transportDocumentService.createAndReleaseTransportDocument(mockTransportDocumentCreated)
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should deleteTransportDocumentFromWorldState', ()=>{
    transportDocumentService.deleteTransportDocumentFromWorldState('')
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should processCarrierCheck', ()=>{
    transportDocumentService.processCarrierCheck(undefined)
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should releaseTransportDocument', ()=>{
    transportDocumentService.releaseTransportDocument(undefined)
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should processVehicleInspection', ()=>{
    transportDocumentService.processVehicleInspection(undefined)
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should getTransportDocumentIdByOrderPositionId', ()=>{
    transportDocumentService.getTransportDocumentIdByOrderPositionId(undefined)
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should processCarrierConfirmation', ()=>{
    transportDocumentService.processCarrierConfirmation(undefined, undefined)
    expect(amqpClientSpy).toHaveBeenCalled()
  })
});
