/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { defaultIfEmpty, Observable } from 'rxjs';
import { MessagePatternsDataManagement } from '@core/api-interfaces/lib/amqp/message-patterns.enum';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { UpdateVisualInspectionCarrierDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import { UpdateVisualInspectionConsigneeDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';
import {
  AcceptDangerousGoodRegistrationDto,
  CarrierCheckRequestDto,
} from '@core/api-interfaces/lib/dtos/data-management';
import { CreateGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/create-goods-data.dto';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { TransportVehicleInspectionRequestDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-vehicle-inspection-request.dto';
import { JwtDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto';

@Injectable()
export class TransportDocumentService {
  private readonly logger = new Logger(TransportDocumentService.name);

  constructor(
    public readonly configService: ConfigService,
    @Inject(Queues.DATA_MANAGEMENT) public readonly client: ClientProxy
  ) {}

  /**
   * Returns the transport document with the Id `documentId`.
   *
   * @param documentId - Transport document Id
   * @returns Transport document
   */
  getTransportDocument(documentId: string): Observable<SaveGoodsDataDto> {
    this.logger.log('Get transport document with id ' + documentId);
    return this.client.send(MessagePatternsDataManagement.GET_TRANSPORT_DOCUMENT, documentId);
  }

  /**
   * Returns all transport document in descending order with 'lastUpdate' as a sortKey, i.e. most recently updated
   * transport document = first array entry).
   *
   * @returns Sorted list of all transport documents
   */
  getAllTransportDocuments(): Observable<SaveGoodsDataDto[]> {
    this.logger.log('Get all transport documents');
    return this.client.send(MessagePatternsDataManagement.GET_ALL_TRANSPORT_DOCUMENTS, {});
  }

  /**
   * Returns the parts of the transport document with the Id `documentId`, which the consignee has access to.
   *
   * @param documentId - Transport document Id
   * @param jwtDto - User information
   * @returns Transport document
   */
  getTransportDocumentForConsignee(documentId: string, jwtDto: JwtDto): Observable<SaveGoodsDataDto> {
    this.logger.log('Get transport document for consignee');
    return this.client.send(MessagePatternsDataManagement.GET_TRANSPORT_DOCUMENT_FOR_CONSIGNEE, {
      documentId: documentId,
      jwtDto: jwtDto,
    });
  }

  /**
   * Returns the parts of all transport document in descending order with 'lastUpdate' as a sortKey, i.e. most recently updated
   * transport document = first array entry), on which the consignee has access.
   *
   * @param jwtDto - User information
   * @returns Sorted list of all transport documents
   */
  getAllTransportDocumentsForConsignee(jwtDto: JwtDto): Observable<SaveGoodsDataDto[]> {
    this.logger.log('Get all transport documents for consignee');
    return this.client.send(MessagePatternsDataManagement.GET_ALL_TRANSPORT_DOCUMENTS_FOR_CONSIGNEE, jwtDto);
  }

  /**
   * Returns the transport document Id of the transport document which contains the order position with the given
   * order position Id.
   *
   * @param orderPositionId - Order position Id of an order position
   * @returns Returns the transport document Id of the transport document which contains the order position
   */
  getTransportDocumentIdByOrderPositionId(orderPositionId: number): Observable<string> {
    this.logger.log('Get transport document id by order position id');
    return this.client.send(
      MessagePatternsDataManagement.GET_TRANSPORT_DOCUMENT_ID_BY_ORDER_POSITION_ID,
      orderPositionId
    );
  }

  /**
   * Saves a transport document to the blockchain.
   * After saving, the transport document Id is set to the Id given by the tendermint service.
   *
   * @param saveGoodsDataDto - A save goods data dto
   * @returns The saved transport document with its Id
   */
  saveTransportDocument(saveGoodsDataDto: SaveGoodsDataDto): Observable<SaveGoodsDataDto> {
    this.logger.log('Create new transport document');
    return this.client.send(MessagePatternsDataManagement.CREATE_TRANSPORT_DOCUMENT, saveGoodsDataDto);
  }

  /**
   * Updates a transport document in the blockchain.
   *
   * @param createGoodsDataDto - A create goods data dto
   * @returns The updated transport document
   */
  updateTransportDocument(createGoodsDataDto: CreateGoodsDataDto): Observable<SaveGoodsDataDto> {
    this.logger.log('Update transport document');
    return this.client.send(MessagePatternsDataManagement.UPDATE_TRANSPORT_DOCUMENT, createGoodsDataDto);
  }

  /**
   * Saves a transport document with the initial status `released` and two log entries (`created` & `released`) to the blockchain.
   * After saving, the transport document Id is set to the Id given by the tendermint service.
   *
   * @param saveGoodsDataDto - A save goods data dto
   * @returns The saved transport document with the initial status `released` with its Id
   */
  createAndReleaseTransportDocument(saveGoodsDataDto: SaveGoodsDataDto): Observable<SaveGoodsDataDto> {
    this.logger.log('Create and release transport document');
    return this.client.send(MessagePatternsDataManagement.CREATE_AND_RELEASE_TRANSPORT_DOCUMENT, saveGoodsDataDto);
  }

  /**
   * Converts a dangerous good registration into a transport document.
   * If a valid transport document Id is given, it will be merged into the given transport document.
   * Otherwise it saves a transport document with the initial status `transport` and two log entries (`created` & `transport`) to the blockchain.
   * After saving, the transport document Id is set to the Id given by the tendermint service.
   *
   * @param acceptDangerousGoodRegistration - A save goods data dto
   * @returns The saved transport document with the initial status `transport` with its Id
   */
  acceptDangerousGoodRegistration(
    acceptDangerousGoodRegistration: AcceptDangerousGoodRegistrationDto
  ): Observable<SaveGoodsDataDto> {
    this.logger.log('Accept dangerous good registration');
    return this.client.send(
      MessagePatternsDataManagement.ACCEPT_DANGEROUS_GOOD_REGISTRATION,
      acceptDangerousGoodRegistration
    );
  }

  /**
   * Executes a order position check for a given transport document, updates its status, adds a new log entry and saves
   * it to the blockchain.
   *
   * @param updateVisualInspectionCarrierDto - A update order position check dto
   * @returns The updated transport document
   */
  updateVisualInspectionCarrier(
    updateVisualInspectionCarrierDto: UpdateVisualInspectionCarrierDto
  ): Observable<SaveGoodsDataDto> {
    this.logger.log('Update visual inspection carrier');
    return this.client.send(
      MessagePatternsDataManagement.UPDATE_VISUAL_INSPECTION_CARRIER,
      updateVisualInspectionCarrierDto
    );
  }

  /**
   * Executes a visual inspection check for a given transport document, updates its status, adds a new log entry and saves
   * it to the blockchain.
   *
   * @param updateVisualInspectionConsigneeDto - A update visual inspection check dto
   * @returns The updated transport document
   */
  updateVisualInspectionConsignee(
    updateVisualInspectionConsigneeDto: UpdateVisualInspectionConsigneeDto
  ): Observable<SaveGoodsDataDto> {
    this.logger.log('Update visual inspection consignee');
    return this.client.send(
      MessagePatternsDataManagement.UPDATE_VISUAL_INSPECTION_CONSIGNEE,
      updateVisualInspectionConsigneeDto
    );
  }

  /**
   * Deletes a transport document from the world state of the blockchain.
   *
   * @param transportDocumentId - Transport document id
   * @returns void
   */
  deleteTransportDocumentFromWorldState(transportDocumentId: string): Observable<void> {
    this.logger.log('Delete transport document from world state');
    return this.client
      .send(MessagePatternsDataManagement.DELETE_TRANSPORT_DOCUMENT_FROM_WORLD_STATE, transportDocumentId)
      .pipe(defaultIfEmpty(new Observable<void>())) as Observable<void>;
  }

  // --- End CRUD and Start Business Logic ---
  /**
   * Executes a carrier check for a given transport document, updates its status, adds a new log entry and saves it to
   * the blockchain.
   *
   * @param carrierCheckRequestDto - A dto containing information about what group positions (of an already created transport document) the carrier accepts
   * @returns The updated transport document
   */
  processCarrierCheck(carrierCheckRequestDto: CarrierCheckRequestDto): Observable<SaveGoodsDataDto> {
    this.logger.log('Process carrier check');
    return this.client.send(MessagePatternsDataManagement.CARRIER_CHECK, carrierCheckRequestDto);
  }

  /**
   * Updates the status of a transport document to `released`, adds a new log entry and saves it to the blockchain.
   *
   * @param transportDocumentId - Transport document Id
   * @returns The updated transport document
   */
  releaseTransportDocument(transportDocumentId: string): Observable<SaveGoodsDataDto> {
    this.logger.log('Release transport document');
    return this.client.send(MessagePatternsDataManagement.RELEASE_TRANSPORT_DOCUMENT, transportDocumentId);
  }

  /**
   * Executes a vehicle inspection for a given transport document, updates its status, adds a new log entry and saves it
   * to the blockchain.
   *
   * @param transportVehicleInspectionRequestDto - A dto containing modifications of the transport document and information
   * about what group positions (of an already accepted transport document) the carrier accepts.
   * @returns The updated transport document
   */
  processVehicleInspection(
    transportVehicleInspectionRequestDto: TransportVehicleInspectionRequestDto
  ): Observable<SaveGoodsDataDto> {
    this.logger.log('Process vehicle inspection');
    return this.client.send(MessagePatternsDataManagement.VEHICLE_INSPECTION, transportVehicleInspectionRequestDto);
  }

  /**
   * Create carrier confirmation
   *
   * @param transportDocumentId - Transport document id
   * @param jwtDto - User information
   * @returns Observable<SaveGoodsDataDto>
   */
  processCarrierConfirmation(transportDocumentId: string, jwtDto: JwtDto): Observable<SaveGoodsDataDto> {
    this.logger.log('Process carrier confirmation');
    return this.client.send(MessagePatternsDataManagement.CONFIRM_CARRIER, {
      documentId: transportDocumentId,
      jwtDto: jwtDto,
    });
  }
}
