/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TransportVehicleCriteria } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/acceptance-criteria/transport-vehicle-criteria';

export const vehicleInspectionDto: TransportVehicleCriteria = {
  comment: 'string',
  licencePlate: 'string',
  acceptVehicleCondition: true,
  acceptVehicleSafetyEquipment: true,
  driverName: 'string',
  acceptCarrierInformation: true,
  acceptCarrierSafetyEquipment: true,
};
