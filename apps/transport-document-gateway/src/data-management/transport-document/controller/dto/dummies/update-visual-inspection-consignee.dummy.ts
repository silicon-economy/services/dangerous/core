/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UpdateVisualInspectionConsigneeDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';

export const updateVisualInspectionConsigneeDto: UpdateVisualInspectionConsigneeDto = {
  transportDocumentId: 'string',
  order: {
    orderId: 'string',
    orderPositions: [
      {
        orderPositionId: 1,
        acceptQuantity: true,
        acceptIntactness: true,
        comment: 'string',
      },
    ],
  },
};
