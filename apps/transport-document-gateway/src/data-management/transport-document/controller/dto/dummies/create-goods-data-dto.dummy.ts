/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AdditionalInformation } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/additional-information.enum';
import { CreateGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/create-goods-data.dto';
import { TransportLabel } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/transport-label.enum';
import { PackagingUnit } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/packaging-unit.enum';
import { PackagingGroup } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/packaging-group.enum';
import { TunnelRestrictionCode } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/tunnel-restriction-code.enum';
import { TransportCategory } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/transport-category.enum';

export const createGoodsDataDto: CreateGoodsDataDto = {
  consignor: {
    name: 'string',
    address: {
      street: 'string',
      number: 'string',
      postalCode: 'string',
      city: 'string',
      country: 'string',
    },
    contact: {
      name: 'string',
      phone: 'string',
      mail: 'string',
      department: 'string',
    },
  },
  freight: {
    orders: [
      {
        consignee: {
          name: 'string',
          address: {
            street: 'string',
            number: 'string',
            postalCode: 'string',
            city: 'string',
            country: 'string',
          },
          contact: {
            name: 'string',
            phone: 'string',
            mail: 'string',
            department: 'string',
          },
        },
        orderPositions: [
          {
            dangerousGood: {
              unNumber: 'string',
              casNumber: 'string',
              description: 'string',
              label1: TransportLabel._1_2,
              label2: TransportLabel._1_1,
              label3: TransportLabel._1_1,
              packingGroup: PackagingGroup.I,
              tunnelRestrictionCode: TunnelRestrictionCode['(B)'],
              transportCategory: TransportCategory._0,
            },
            package: 'string',
            packagingCode: 'string',
            quantity: 0,
            unit: PackagingUnit.L,
            individualAmount: 0,
            polluting: true,
            transportPoints: 0,
            totalAmount: 0,
          },
        ],
      },
    ],
    additionalInformation: AdditionalInformation['Beförderung ohne Freistellung nach ADR 1.1.3.6'],
    transportationInstructions: 'string',
    totalTransportPoints: 0,
  },
  carrier: {
    name: 'string',
    driver: 'string',
    licensePlate: 'string',
  },
  createdBy: 'string',
};
