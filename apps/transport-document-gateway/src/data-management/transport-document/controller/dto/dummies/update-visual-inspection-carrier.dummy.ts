/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UpdateVisualInspectionCarrierDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';

export const updateVisualInspectionCarrierDto: UpdateVisualInspectionCarrierDto = {
  transportDocumentId: 'string',
  orders: [
    {
      orderId: 'string',
      orderPositions: [
        {
          orderPositionId: 1,
          acceptTransportability: true,
          acceptLabeling: true,
          comment: 'string',
        },
      ],
    },
  ],
};
