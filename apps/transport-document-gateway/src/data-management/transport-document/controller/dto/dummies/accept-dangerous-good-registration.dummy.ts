/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AcceptDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/data-management';

export const acceptDangerousGoodRegistration: AcceptDangerousGoodRegistrationDto = {
  dangerousGoodRegistrationId: '',
  acceptedOrderPositionIds: [],
  transportationInstructions: '',
  carrier: { driver: '', licensePlate: '', name: '' },
};
