/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CarrierCheckCriteria } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/acceptance-criteria/carrier-check-criteria';

export const carrierCheckCriteria: CarrierCheckCriteria = {
  comment: 'string',
  acceptConsignor: true,
  acceptConsignees: [true],
  acceptFreight: true,
  acceptTransportationInstructions: true,
  acceptAdditionalInformation: true,
};
