/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { JwtDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/user.dto';
import { CompanyDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/company.dto';
import { AddressDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/address.dto';
import { ContactPersonDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/contact-person.dto';

export interface GetRequestUser {
  user: JwtDto;
}

export const AuthUser = createParamDecorator((data, req: ExecutionContext) => {
  const getRequest: GetRequestUser = req.switchToHttp().getRequest<GetRequestUser>();
  return new JwtDto(
    getRequest.user.username,
    getRequest.user.roles,
    new UserDto(
      getRequest.user.id,
      new CompanyDto(
        getRequest.user.company.name,
        new AddressDto(
          getRequest.user.company.address.street,
          getRequest.user.company.address.number,
          getRequest.user.company.address.postalCode,
          getRequest.user.company.address.city,
          getRequest.user.company.address.country
        ),
        new ContactPersonDto(
          getRequest.user.company.contact.name,
          getRequest.user.company.contact.phone,
          getRequest.user.company.contact.mail,
          getRequest.user.company.contact.department
        )
      )
    )
  );
});
