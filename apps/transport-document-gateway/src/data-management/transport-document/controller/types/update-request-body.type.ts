/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UpdateVisualInspectionCarrierDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import { UpdateVisualInspectionConsigneeDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';
import { AcceptDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/data-management';
import { CreateGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/create-goods-data.dto';
import { CarrierCheckCriteria } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/acceptance-criteria/carrier-check-criteria';
import { TransportVehicleCriteria } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/acceptance-criteria/transport-vehicle-criteria';

export type UpdateRequestBodyType =
  | CreateGoodsDataDto
  | CarrierCheckCriteria
  | UpdateVisualInspectionCarrierDto
  | UpdateVisualInspectionConsigneeDto
  | TransportVehicleCriteria
  | AcceptDangerousGoodRegistrationDto;
