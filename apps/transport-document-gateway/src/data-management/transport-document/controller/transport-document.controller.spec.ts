/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { DtoParser } from '../../../shared/dto-parser';
import { TransportDocumentService } from '../service/transport-document.service';
import { TransportDocumentController } from './transport-document.controller';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { CreateRequestBodyType } from './types/create-request-body-type';
import { PostHeader } from './enum/post-header.enum';
import { UpdateRequestBodyType } from './types/update-request-body.type';
import { PutHeader } from './enum/put-header.enum';
import { HttpException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { TransportDocumentHandler } from '../handler/transport-document.handler';
import { JwtDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { UserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/user.dto';

describe('TransportDocumentController', () => {
  let transportDocumentController: TransportDocumentController;
  let transportDocumentService: TransportDocumentService;
  const mockJwtTokenConsignee: JwtDto = new JwtDto(
    'funkeAg',
    [UserRole.consignee],
    new UserDto('funkeAg', {
      name: 'Funke AG - Lacke und Farben',
      address: {
        street: 'Columbiadamm',
        number: '194',
        postalCode: '10965',
        city: 'Berlin',
        country: 'Deutschland',
      },
      contact: {
        name: 'Dr. Martina Zünd',
        phone: '0049 741 852 063 9',
        mail: 'Zuend@Funke.de',
        department: 'Abt. 4A',
      },
    })
  );
  const mockJwtTokenConsignor: JwtDto = new JwtDto(
    'funkeAg',
    [UserRole.consignor],
    new UserDto('funkeAg', {
      name: 'Funke AG - Lacke und Farben',
      address: {
        street: 'Columbiadamm',
        number: '194',
        postalCode: '10965',
        city: 'Berlin',
        country: 'Deutschland',
      },
      contact: {
        name: 'Dr. Martina Zünd',
        phone: '0049 741 852 063 9',
        mail: 'Zuend@Funke.de',
        department: 'Abt. 4A',
      },
    })
  );

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [TransportDocumentController],
      providers: [TransportDocumentHandler, TransportDocumentService, DtoParser],
      imports: [HttpModule, ConfigModule, AmqpModule.register(Queues.DATA_MANAGEMENT)],
    }).compile();

    transportDocumentController = moduleRef.get<TransportDocumentController>(TransportDocumentController);
    transportDocumentService = moduleRef.get<TransportDocumentService>(TransportDocumentService);
  });

  it('should be defined', () => {
    expect(transportDocumentController).toBeDefined();
    expect(transportDocumentService).toBeDefined();
    expect(transportDocumentController.getTransportDocumentById).toBeDefined();
    expect(transportDocumentController.getAllTransportDocuments).toBeDefined();
    expect(transportDocumentController.createTransportDocument).toBeDefined();
    expect(transportDocumentController.getTransportDocumentIdByOrderPositionId).toBeDefined();
  });

  describe('getTransportDocumentById', () => {
    it('should call getTransportDocumentForConsignee', () => {
      const getTransportDocumentForConsigneeSpy = jest
        .spyOn(TransportDocumentService.prototype, 'getTransportDocumentForConsignee')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      void transportDocumentController.getTransportDocumentById('', mockJwtTokenConsignee);
      expect(getTransportDocumentForConsigneeSpy).toHaveBeenCalled();
    });

    it('should call getTransportDocument', () => {
      const getTransportDocumentForConsigneeSpy = jest
        .spyOn(TransportDocumentService.prototype, 'getTransportDocument')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      void transportDocumentController.getTransportDocumentById('', mockJwtTokenConsignor);
      expect(getTransportDocumentForConsigneeSpy).toHaveBeenCalled();
    });
  });

  describe('getAllTransportDocuments', () => {
    it('should call getAllTransportDocumentsForConsignee', () => {
      const getAllTransportDocumentsForConsigneeSpy = jest
        .spyOn(TransportDocumentService.prototype, 'getAllTransportDocumentsForConsignee')
        .mockReturnValue({} as Observable<SaveGoodsDataDto[]>);
      void transportDocumentController.getAllTransportDocuments(mockJwtTokenConsignee);
      expect(getAllTransportDocumentsForConsigneeSpy).toHaveBeenCalled();
    });

    it('should call getAllTransportDocuments', () => {
      const getAllTransportDocumentsSpy = jest
        .spyOn(TransportDocumentService.prototype, 'getAllTransportDocuments')
        .mockReturnValue({} as Observable<SaveGoodsDataDto[]>);
      void transportDocumentController.getAllTransportDocuments(mockJwtTokenConsignor);
      expect(getAllTransportDocumentsSpy).toHaveBeenCalled();
    });
  });

  describe('createTransportDocument', () => {
    it('should call saveTransportDocument', () => {
      const requestBody: CreateRequestBodyType = {} as CreateRequestBodyType;

      const saveTransportDocumentSpy = jest
        .spyOn(TransportDocumentService.prototype, 'saveTransportDocument')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      jest.spyOn(DtoParser.prototype, 'parseDto').mockReturnValue({} as SaveGoodsDataDto);
      void transportDocumentController.createTransportDocument(requestBody);
      expect(saveTransportDocumentSpy).toHaveBeenCalled();
    });

    it('should call createAndReleaseTransportDocument', () => {
      const requestBody: CreateRequestBodyType = {} as CreateRequestBodyType;

      const saveTransportDocumentSpy = jest
        .spyOn(TransportDocumentService.prototype, 'createAndReleaseTransportDocument')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      jest.spyOn(DtoParser.prototype, 'parseDto').mockReturnValue({} as SaveGoodsDataDto);
      void transportDocumentController.createTransportDocument(requestBody, PostHeader[PostHeader.createAndRelease]);
      expect(saveTransportDocumentSpy).toHaveBeenCalled();
    });

    it('should call createAndReleaseTransportDocument', () => {
      const requestBody: CreateRequestBodyType = {} as CreateRequestBodyType;

      const acceptDangerousGoodRegistrationSpy = jest
        .spyOn(TransportDocumentService.prototype, 'acceptDangerousGoodRegistration')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      jest.spyOn(TransportDocumentHandler.prototype, 'isAcceptDangerousGoodRegistrationDto').mockReturnValue(true);
      void transportDocumentController.createTransportDocument(requestBody, PostHeader[PostHeader.createAndTransport]);
      expect(acceptDangerousGoodRegistrationSpy).toHaveBeenCalled();
    });
  });

  describe('updateTransportDocument', () => {
    it('should call saveTransportDocument', () => {
      const requestBody: UpdateRequestBodyType = {} as UpdateRequestBodyType;

      const updateTransportDocumentSpy = jest
        .spyOn(TransportDocumentService.prototype, 'updateTransportDocument')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      jest.spyOn(TransportDocumentHandler.prototype, 'isCreateGoodsDataDto').mockReturnValue(true);
      void transportDocumentController.updateTransportDocument(requestBody);
      expect(updateTransportDocumentSpy).toHaveBeenCalled();
    });

    it('should call processCarrierCheck', () => {
      const requestBody: UpdateRequestBodyType = {} as UpdateRequestBodyType;

      const processCarrierCheckSpy = jest
        .spyOn(TransportDocumentService.prototype, 'processCarrierCheck')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      jest.spyOn(TransportDocumentHandler.prototype, 'isCarrierCheckCriteria').mockReturnValue(true);
      void transportDocumentController.updateTransportDocument(
        requestBody,
        undefined,
        PutHeader[PutHeader.carrierCheck]
      );
      expect(processCarrierCheckSpy).toHaveBeenCalled();
    });

    it('should call deleteTransportDocumentFromWorldState', () => {
      const deleteTransportDocumentFromWorldStateSpy = jest
        .spyOn(TransportDocumentService.prototype, 'deleteTransportDocumentFromWorldState')
        .mockReturnValue({} as Observable<void>);
      void transportDocumentController.updateTransportDocument(
        undefined,
        undefined,
        PutHeader[PutHeader.disable],
        '123'
      );
      expect(deleteTransportDocumentFromWorldStateSpy).toHaveBeenCalled();
    });

    it('should call releaseTransportDocument', () => {
      const releaseTransportDocumentSpy = jest
        .spyOn(TransportDocumentService.prototype, 'releaseTransportDocument')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      void transportDocumentController.updateTransportDocument(
        undefined,
        undefined,
        PutHeader[PutHeader.release],
        '123'
      );
      expect(releaseTransportDocumentSpy).toHaveBeenCalled();
    });

    it('should call updateVisualInspectionCarrier', () => {
      const requestBody: UpdateRequestBodyType = {} as UpdateRequestBodyType;

      const updateVisualInspectionCarrierSpy = jest
        .spyOn(TransportDocumentService.prototype, 'updateVisualInspectionCarrier')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      jest.spyOn(TransportDocumentHandler.prototype, 'isUpdateOrderPositionCheckDto').mockReturnValue(true);
      void transportDocumentController.updateTransportDocument(
        requestBody,
        undefined,
        PutHeader[PutHeader.updateVisualInspectionCarrier]
      );
      expect(updateVisualInspectionCarrierSpy).toHaveBeenCalled();
    });

    it('should call updateVisualInspectionConsignee', () => {
      const requestBody: UpdateRequestBodyType = {} as UpdateRequestBodyType;

      const updateVisualInspectionConsigneeSpy = jest
        .spyOn(TransportDocumentService.prototype, 'updateVisualInspectionConsignee')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      jest.spyOn(TransportDocumentHandler.prototype, 'isUpdateVisualInspectionCheckDto').mockReturnValue(true);
      void transportDocumentController.updateTransportDocument(
        requestBody,
        undefined,
        PutHeader[PutHeader.updateVisualInspectionConsignee]
      );
      expect(updateVisualInspectionConsigneeSpy).toHaveBeenCalled();
    });

    it('should call processVehicleInspection', () => {
      const requestBody: UpdateRequestBodyType = {} as UpdateRequestBodyType;

      const processVehicleInspectionSpy = jest
        .spyOn(TransportDocumentService.prototype, 'processVehicleInspection')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      jest.spyOn(TransportDocumentHandler.prototype, 'isTransportVehicleCriteria').mockReturnValue(true);
      void transportDocumentController.updateTransportDocument(
        requestBody,
        undefined,
        PutHeader[PutHeader.vehicleInspection],
        '123'
      );
      expect(processVehicleInspectionSpy).toHaveBeenCalled();
    });

    it('should call processCarrierConfirmation', () => {
      const processCarrierConfirmationSpy = jest
        .spyOn(TransportDocumentService.prototype, 'processCarrierConfirmation')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      void transportDocumentController.updateTransportDocument(
        undefined,
        mockJwtTokenConsignor,
        PutHeader[PutHeader.confirmCarrier],
        '123'
      );
      expect(processCarrierConfirmationSpy).toHaveBeenCalled();
    });

    it('should call nothing', () => {
      expect(() =>
        transportDocumentController.updateTransportDocument(
          undefined,
          undefined,
          PutHeader[PutHeader.confirmCarrier],
          '123'
        )
      ).toThrow(HttpException);
    });

    it('should call acceptDangerousGoodRegistration', () => {
      const requestBody: UpdateRequestBodyType = {} as UpdateRequestBodyType;

      const acceptDangerousGoodRegistrationSpy = jest
        .spyOn(TransportDocumentService.prototype, 'acceptDangerousGoodRegistration')
        .mockReturnValue({} as Observable<SaveGoodsDataDto>);
      jest.spyOn(TransportDocumentHandler.prototype, 'isAcceptDangerousGoodRegistrationDto').mockReturnValue(true);
      void transportDocumentController.updateTransportDocument(
        requestBody,
        undefined,
        PutHeader[PutHeader.acceptDangerousGoodRegistration]
      );
      expect(acceptDangerousGoodRegistrationSpy).toHaveBeenCalled();
    });
  });
});
