/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Body, Controller, Get, Headers, Param, Post, Put } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiHeader, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { DtoParser } from '../../../shared/dto-parser';
import { TransportDocumentService } from '../service/transport-document.service';
import * as acceptDangerousGoodRegistration from './dto/dummies/accept-dangerous-good-registration.dummy';
import * as carrierCheckDtoDummy from './dto/dummies/carrier-check-dto.dummy';
import * as createGoodsDataDtoDummy from './dto/dummies/create-goods-data-dto.dummy';
import * as updateVisualInspectionCarrierDummy from './dto/dummies/update-visual-inspection-carrier.dummy';
import * as updateVisualInspectionConsigneeDummy from './dto/dummies/update-visual-inspection-consignee.dummy';
import * as vehicleInspectionDtoDummy from './dto/dummies/vehicle-inspection-dto.dummy';
import { PostHeader } from './enum/post-header.enum';
import { PutHeader } from './enum/put-header.enum';
import { CreateRequestBodyType } from './types/create-request-body-type';
import { UpdateRequestBodyType } from './types/update-request-body.type';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { Observable } from 'rxjs';
import { TransportDocumentHandler } from '../handler/transport-document.handler';
import { JwtDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto';
import { AuthUser } from './decorators/jwt-dto.decorator';

@ApiTags('Transport Document')
@ApiBearerAuth('JWT-auth')
@Controller()
@ApiTags('v1')
export class TransportDocumentController {
  constructor(
    private transportDocumentService: TransportDocumentService,
    private dtoParser: DtoParser,
    private transportDocumentHandler: TransportDocumentHandler
  ) {}

  /**
   * Get a transport document by given id from the blockchain
   *
   * @param transportDocumentId - Transport document id
   * @param jwtDto - User information
   * @returns Observable\<SaveGoodsDataDto[]\>
   */
  @Get(':transportDocumentId')
  @ApiOperation({
    summary: 'Get transport document by Id.',
    description: 'Gets a transport document by its id from the blockchain.',
  })
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  @ApiParam({ name: 'transportDocumentId', type: 'string', description: 'Transport document Id' })
  @ApiHeader({
    name: 'consigneeInformation',
    description: 'If the call was made by a consignee, enter his information here',
    required: false,
  })
  getTransportDocumentById(
    @Param('transportDocumentId') transportDocumentId: string,
    @AuthUser() jwtDto: JwtDto
  ): Observable<SaveGoodsDataDto> {
    return this.transportDocumentHandler.handleGetTransportDocumentById(transportDocumentId, jwtDto);
  }

  /**
   * Get all transport documents from the blockchain
   *
   * @param jwtDto - User information
   * @returns Observable\<SaveGoodsDataDto[]\>
   */
  @Get('')
  @ApiOperation({
    summary: 'Get all transport documents.',
    description: 'Gets all transport documents from the blockchain.',
  })
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  @ApiHeader({
    name: 'consigneeInformation',
    description: 'If the call was made by a consignee, enter his information here',
    required: false,
  })
  getAllTransportDocuments(@AuthUser() jwtDto: JwtDto): Observable<SaveGoodsDataDto[]> {
    return this.transportDocumentHandler.handleGetAllTransportDocuments(jwtDto);
  }


  /**
   * Gets a transport document id by order position id
   *
   * @param orderPositionId - Order position id
   * @returns Observable<string>
   */
  @Get('transportDocumentIdByOrderPositionId/:orderPositionId')
  @ApiOperation({
    summary: 'Get transport document Id by order position Id.',
    description: 'Gets transport document Id by order position Id.',
  })
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  @ApiParam({ name: 'orderPositionId', description: 'Order position Id' })
  getTransportDocumentIdByOrderPositionId(@Param('orderPositionId') orderPositionId: number): Observable<string> {
    return this.transportDocumentService.getTransportDocumentIdByOrderPositionId(orderPositionId);
  }

  /**
   * Save a transport document to the blockchain
   *
   * @param requestBody - Request body
   * @param typeOfAction - Type of action: create and release, set transport status or delete DGR
   * @returns Observable<SaveGoodsDataDto>
   */
  @Post('')
  @ApiOperation({
    summary: 'Save a transport document.',
    description: 'Saves a transport document to the blockchain.',
  })
  @ApiHeader({
    name: 'typeOfAction',
    enum: PostHeader,
    description:
      `Type of action other than to just create the transport document.\n
   - **` +
      PostHeader[PostHeader.createAndRelease] +
      `:** Creates a Transport Document and releases it at the same time.\n
   - **` +
      PostHeader[PostHeader.createAndTransport] +
      `:** Creates a Transport Document from a Dangerous Good Registration and brings it into status transport.`,
    required: false,
  })
  @ApiBody({
    required: false,
    examples: {
      a: {
        summary: 'Request for create',
        description: 'Request for createTransportDocument',
        value: createGoodsDataDtoDummy.createGoodsDataDto,
      },
      b: {
        summary: 'Request for createAndRelease',
        description: 'Request for createAndRelease',
        value: createGoodsDataDtoDummy.createGoodsDataDto,
      },
      c: {
        summary: 'Request for createAndTransport',
        description: 'Dangerous Good Registration will be extracted into a new Transport Document.',
        value: acceptDangerousGoodRegistration.acceptDangerousGoodRegistration,
      },
    },
  })
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  createTransportDocument(
    @Body() requestBody: CreateRequestBodyType,
    @Headers('typeOfAction') typeOfAction?: string
  ): Observable<SaveGoodsDataDto> {
    return this.transportDocumentHandler.handleCreateTransportDocument(requestBody, typeOfAction);
  }

  /**
   * Update a transport document in the blockchain
   *
   * @param requestBody - Request body
   * @param jwtDto - User information
   * @param typeOfAction - Type of action to execute
   * @param transportDocumentId - Transport document id
   * @returns Observable\<SaveGoodsDataDto | void\>
   */
  @Put(':transportDocumentId?')
  @ApiOperation({
    summary: 'Update a transport document.',
    description: 'Updates a transport document in the blockchain.',
  })
  @ApiHeader({
    name: 'typeOfAction',
    enum: PutHeader,
    description:
      `Type of action other than to just create the transport document.\n
   - **` +
      PutHeader[PutHeader.carrierCheck] +
      `:** The carrier verifies the document.\n
   - **` +
      PutHeader[PutHeader.updateVisualInspectionCarrier] +
      `:** The carrier performs a visual inspection of all shipping units before transportation.\n
   - **` +
      PutHeader[PutHeader.updateVisualInspectionConsignee] +
      `:** The consignee performs a visual inspection of all received shipping units at arrival.\n
   - **` +
      PutHeader[PutHeader.disable] +
      `:** Disables a Transport Document. I.e. it is deleted from the world state of the blockchain.\n
   - **` +
      PutHeader[PutHeader.release] +
      `:** The consignor releases a transport document.\n
   - **` +
      PutHeader[PutHeader.vehicleInspection] +
      `:** The consignor performs an inspection of the vehicle intended to be used for the transport.\n
   - **` +
      PutHeader[PutHeader.confirmCarrier] +
      `:** The Consignee confirms the carrier at arrival.\n
   - **` +
      PutHeader[PutHeader.acceptDangerousGoodRegistration] +
      `:** A dangerous good registration is either partially or completely transferred into an existing transport document`,
    required: false,
  })
  @ApiBody({
    required: false,
    examples: {
      a: {
        summary: 'Request for UpdateTransportDocument',
        description: 'Request for UpdateTransportDocument',
        value: createGoodsDataDtoDummy.createGoodsDataDto,
      },
      b: {
        summary: 'Request for CarrierCheck',
        description:
          'A dto containing information about what group positions (of an already created transport document) the carrier accepts. Executes a carrier check for a given transport document, updates its status, adds a new log entry and saves it to the blockchain.',
        value: carrierCheckDtoDummy.carrierCheckCriteria,
      },
      c: {
        summary: 'Request for UpdateVisualInspectionCarrier',
        description: 'Updates order position check, adds a new log entry `transport` and saves it to the blockchain.',
        value: updateVisualInspectionCarrierDummy.updateVisualInspectionCarrierDto,
      },
      d: {
        summary: 'Request for VehicleInspection',
        description:
          'Executes a vehicle inspection for a given transport document, updates its status, adds a new log entry and saves it to the blockchain.',
        value: vehicleInspectionDtoDummy.vehicleInspectionDto,
      },
      e: {
        summary: 'Request for UpdateVisualInspectionConsignee',
        description:
          'Updates visual inspection check, adds a new log entry to order and orderposition and saves it to the blockchain.',
        value: updateVisualInspectionConsigneeDummy.updateVisualInspectionConsigneeDto,
      },
      f: {
        summary: 'Request for AcceptDangerousGoodRegistration.',
        description: 'Dangerous Good Registration will be extracted into an already existing Transport Document.',
        value: acceptDangerousGoodRegistration.acceptDangerousGoodRegistration,
      },
    },
  })
  @ApiParam({
    name: 'transportDocumentId',
    description: 'Id of the transport document',
    required: false,
    allowEmptyValue: true,
  })
  @ApiHeader({
    name: 'consigneeInformation',
    description: 'If the call was made by a consignee, enter his information here',
    required: false,
  })
  @ApiResponse({ status: 201 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  updateTransportDocument(
    @Body()
    requestBody?: UpdateRequestBodyType,
    @AuthUser() jwtDto?: JwtDto,
    @Headers('typeOfAction') typeOfAction?: string,
    @Param('transportDocumentId') transportDocumentId?: string
  ): Observable<SaveGoodsDataDto | void> {
    return this.transportDocumentHandler.handleUpdateTransportDocument(
      requestBody,
      jwtDto,
      typeOfAction,
      transportDocumentId
    );
  }
}
