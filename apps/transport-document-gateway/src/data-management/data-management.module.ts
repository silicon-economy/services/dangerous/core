/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { DangerousGoodRegistrationModule } from './dangerous-good-registration/dangerous-good-registration.module';
import { TransportDocumentModule } from './transport-document/transport-document.module';

@Module({
  controllers: [],
  providers: [],
  imports: [TransportDocumentModule, DangerousGoodRegistrationModule],
  exports: [TransportDocumentModule, DangerousGoodRegistrationModule],
})
export class DataManagementModule {}
