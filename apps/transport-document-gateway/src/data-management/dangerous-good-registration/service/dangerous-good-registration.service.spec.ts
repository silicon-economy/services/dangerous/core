/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { DtoParser } from '../../../shared/dto-parser';
import { DangerousGoodRegistrationController } from '../controller/dangerous-good-registration.controller';
import { DangerousGoodRegistrationService } from './dangerous-good-registration.service';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { DangerousGoodRegistrationHandler } from '../handler/dangerous-good-registration.handler';
import { CreateDangerousGoodRegistrationDto } from "@core/api-interfaces/lib/dtos/data-management";
import { Order } from "@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order";
import { Freight } from "@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/freight";
import {
  mockDangerousGoodRegistration
} from "@core/api-interfaces/lib/mocks/dangerous-good-registration/dangerous-good-registration.mock";

describe('DangerousGoodRegistrationService', () => {
  let dangerousGoodRegistrationService: DangerousGoodRegistrationService;
  let amqpClientSpy;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [DangerousGoodRegistrationController],
      providers: [DangerousGoodRegistrationService, DangerousGoodRegistrationHandler, DtoParser],
      imports: [HttpModule, ConfigModule, AmqpModule.register(Queues.DATA_MANAGEMENT)],
    }).compile();

    dangerousGoodRegistrationService = moduleRef.get<DangerousGoodRegistrationService>(
      DangerousGoodRegistrationService
    );
    amqpClientSpy = jest.spyOn(dangerousGoodRegistrationService.client,'send')
  });

  it('should be defined', () => {
    expect(dangerousGoodRegistrationService).toBeDefined();
    expect(dangerousGoodRegistrationService.getDangerousGoodRegistration).toBeDefined();
    expect(dangerousGoodRegistrationService.getAllDangerousGoodRegistrations).toBeDefined();
    expect(dangerousGoodRegistrationService.saveDangerousGoodRegistration).toBeDefined();
    expect(dangerousGoodRegistrationService.updateDangerousGoodRegistration).toBeDefined();
    expect(dangerousGoodRegistrationService.deleteDangerousGoodRegistrationFromWorldState).toBeDefined();
  });

  it('should getDangerousGoodRegistration', ()=>{
    dangerousGoodRegistrationService.getDangerousGoodRegistration('')
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should getAllDangerousGoodRegistrations', ()=>{
    dangerousGoodRegistrationService.getAllDangerousGoodRegistrations()
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should saveDangerousGoodRegistration', ()=>{
    const createDangerousGoodRegistrationDto =  new CreateDangerousGoodRegistrationDto()
    createDangerousGoodRegistrationDto.freight = {orders: [new Order(undefined, undefined)]} as Freight
    dangerousGoodRegistrationService.saveDangerousGoodRegistration(createDangerousGoodRegistrationDto)
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should updateDangerousGoodRegistration', ()=>{
    dangerousGoodRegistrationService.updateDangerousGoodRegistration(mockDangerousGoodRegistration)
    expect(amqpClientSpy).toHaveBeenCalled()
  })

  it('should deleteDangerousGoodRegistrationFromWorldState', ()=>{
    dangerousGoodRegistrationService.deleteDangerousGoodRegistrationFromWorldState('')
    expect(amqpClientSpy).toHaveBeenCalled()
  })
});
