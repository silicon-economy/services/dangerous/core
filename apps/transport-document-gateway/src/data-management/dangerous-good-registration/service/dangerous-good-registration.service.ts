/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { defaultIfEmpty, Observable } from 'rxjs';
import { MessagePatternsDangerousGoodRegistration } from '@core/api-interfaces/lib/amqp/message-patterns.enum';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import {
  CreateDangerousGoodRegistrationDto,
  SaveDangerousGoodRegistrationDto,
} from '@core/api-interfaces/lib/dtos/data-management';
import { v4 as uuid } from 'uuid';

@Injectable()
export class DangerousGoodRegistrationService {
  private readonly logger = new Logger(DangerousGoodRegistrationService.name);

  constructor(
    public readonly configService: ConfigService,
    @Inject(Queues.DATA_MANAGEMENT) public readonly client: ClientProxy
  ) {}

  /**
   * Returns the dangerous good registration with the Id `dangerousGoodRegistrationId`.
   *
   * @param dangerousGoodRegistrationId - Dangerous good registration Id
   * @returns Dangerous good registration
   */
  getDangerousGoodRegistration(dangerousGoodRegistrationId: string): Observable<SaveDangerousGoodRegistrationDto> {
    this.logger.log('Get dangerous good registration with id ' + dangerousGoodRegistrationId);
    return this.client.send(
      MessagePatternsDangerousGoodRegistration.GET_DANGEROUS_GOOD_REGISTRATION,
      dangerousGoodRegistrationId
    );
  }

  /**
   * Returns all dangerous good registrations in descending order with 'lastUpdate' as a sortKey, i.e. most recently updated
   * transport document = first array entry).
   *
   * @returns Sorted list of all dangerous good registrations
   */
  getAllDangerousGoodRegistrations(): Observable<SaveDangerousGoodRegistrationDto[]> {
    this.logger.log('Get all dangerous good registrations');
    return this.client.send(MessagePatternsDangerousGoodRegistration.GET_ALL_DANGEROUS_GOOD_REGISTRATIONS, {});
  }

  /**
   * Saves a dangerous good registration to the blockchain.
   * After saving, the dangerous good registration Id is set to the Id given by the tendermint service.
   *
   * @param createDangerousGoodRegistrationDto - Save dangerous good registration Dto
   * @returns The saved dangerous good registration with its Id
   */
  saveDangerousGoodRegistration(
    createDangerousGoodRegistrationDto: CreateDangerousGoodRegistrationDto
  ): Observable<SaveDangerousGoodRegistrationDto> {
    this.logger.log('Create dangerous good registration');
    const saveDangerousGoodRegistrationDto = createDangerousGoodRegistrationDto as SaveDangerousGoodRegistrationDto;
    const date = new Date().getTime();
    saveDangerousGoodRegistrationDto.createdDate = date;
    saveDangerousGoodRegistrationDto.lastUpdate = date;
    saveDangerousGoodRegistrationDto.freight.orders[0].id = uuid();
    return this.client.send(
      MessagePatternsDangerousGoodRegistration.CREATE_DANGEROUS_GOOD_REGISTRATION,
      saveDangerousGoodRegistrationDto
    );
  }

  /**
   * Updates a dangerous good registration in the blockchain.
   *
   * @param createDangerousGoodRegistrationDto - Save dangerous good registration Dto
   * @returns The updated dangerous good registration
   */
  updateDangerousGoodRegistration(
    createDangerousGoodRegistrationDto: SaveDangerousGoodRegistrationDto
  ): Observable<void> {
    this.logger.log('Update dangerous good registration');
    return this.client.send(
      MessagePatternsDangerousGoodRegistration.UPDATE_DANGEROUS_GOOD_REGISTRATION,
      createDangerousGoodRegistrationDto
    );
  }

  /**
   * Deletes a dangerous good registration from the world state of the blockchain.
   *
   * @param dangerousGoodRegistrationId - Dangerous good registration Id
   * @returns Promise<void>
   */
  deleteDangerousGoodRegistrationFromWorldState(dangerousGoodRegistrationId: string): Observable<void> {
    this.logger.log('Delete dangerous good registration from world state');
    return this.client
      .send(MessagePatternsDangerousGoodRegistration.DELETE_DANGEROUS_GOOD_REGISTRATION, dangerousGoodRegistrationId)
      .pipe(defaultIfEmpty(new Observable<void>())) as Observable<void>;
  }
}
