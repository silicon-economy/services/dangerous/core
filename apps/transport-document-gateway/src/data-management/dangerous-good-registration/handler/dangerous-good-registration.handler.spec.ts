/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { DangerousGoodRegistrationHandler } from './dangerous-good-registration.handler';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { DangerousGoodRegistrationService } from '../service/dangerous-good-registration.service';
import { PutHeader } from '../controller/enum/put-header.enum';
import { mockDangerousGoodRegistration } from '@core/api-interfaces/lib/mocks/dangerous-good-registration/dangerous-good-registration.mock';
import { HttpException, HttpStatus } from '@nestjs/common';
import { BadRequestException } from "@nestjs/common";
import { CreateDangerousGoodRegistrationDto } from "@core/api-interfaces/lib/dtos/data-management";

describe('DangerousGoodRegistrationHandler', () => {
  let dangerousGoodRegistrationHandler: DangerousGoodRegistrationHandler;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [DangerousGoodRegistrationHandler, DangerousGoodRegistrationService],
      imports: [HttpModule, ConfigModule, AmqpModule.register(Queues.DATA_MANAGEMENT)],
    }).compile();

    dangerousGoodRegistrationHandler = moduleRef.get<DangerousGoodRegistrationHandler>(
      DangerousGoodRegistrationHandler
    );
  });

  it('should be defined', () => {
    expect(dangerousGoodRegistrationHandler).toBeDefined();
  });

  it('should delete a dangerous good registration from blockchain world state', async () => {
    const deleteDangerousGoodRegistrationFromWorldStateSpy = jest.spyOn(
      dangerousGoodRegistrationHandler['dangerousGoodRegistrationService'],
      'deleteDangerousGoodRegistrationFromWorldState'
    );
    await dangerousGoodRegistrationHandler.handleUpdateDangerousGoodRegistration(
      undefined,
      PutHeader[PutHeader.disableRegistration],
      'foobar'
    );

    expect(deleteDangerousGoodRegistrationFromWorldStateSpy).toHaveBeenCalledWith('foobar');
  });

  it('should update a dangerous good registration', async () => {
    const updateDangerousGoodRegistrationSpy = jest.spyOn(
      dangerousGoodRegistrationHandler['dangerousGoodRegistrationService'],
      'updateDangerousGoodRegistration'
    );
    await dangerousGoodRegistrationHandler.handleUpdateDangerousGoodRegistration(
      mockDangerousGoodRegistration,
      undefined,
      'foobar'
    );

    expect(updateDangerousGoodRegistrationSpy).toHaveBeenCalled();
  });

  it('should throw an http exception if provided action is not supported', async () => {
    await expect(async () => {
      await dangerousGoodRegistrationHandler.handleUpdateDangerousGoodRegistration(
        mockDangerousGoodRegistration,
        'foobar',
        'foobar'
      );
    }).rejects.toThrow(new HttpException('Action not supported', HttpStatus.BAD_REQUEST));
  });

  it('should throw an http exception if validation failed', async () => {
    await expect(async () => {
      await dangerousGoodRegistrationHandler.handleUpdateDangerousGoodRegistration({}, undefined, 'foobar');
    }).rejects.toThrow(new HttpException('Validation failed', HttpStatus.BAD_REQUEST));
  });

  it('should handleUpdateDangerousGoodRegistration and throw a bad request exception', async ()=>{
    await expect(async() => { await dangerousGoodRegistrationHandler.handleUpdateDangerousGoodRegistration()}).rejects.toThrow(new BadRequestException())
  })
});
