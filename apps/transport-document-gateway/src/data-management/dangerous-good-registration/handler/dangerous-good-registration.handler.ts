/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { DangerousGoodRegistrationService } from '../service/dangerous-good-registration.service';
import { SaveDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/data-management';
import { PutHeader } from '../controller/enum/put-header.enum';
import { Observable } from 'rxjs';
import { validate, ValidationError } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class DangerousGoodRegistrationHandler {
  constructor(private dangerousGoodRegistrationService: DangerousGoodRegistrationService) {}

  /**
   * Handles the update of a dangerous good registration based on the provided parameters.
   *
   * @param requestBody - The request body containing the data for updating the dangerous good registration. Optional.
   * @param typeOfAction - The type of action to perform. Optional.
   * @param dangerousGoodRegistrationId - The id of the dangerous good registration. Optional.
   * @returns An Observable that emits either a SaveDangerousGoodRegistrationDto or void.
   * @throws HttpException with status code 400 if the request is invalid.
   */
  async handleUpdateDangerousGoodRegistration(
    requestBody?: SaveDangerousGoodRegistrationDto | Partial<SaveDangerousGoodRegistrationDto>,
    typeOfAction?: string,
    dangerousGoodRegistrationId?: string
  ): Promise<Observable<void>> {
    switch (typeOfAction) {
      case undefined: {
        if (requestBody != undefined) {
          const requestBodyObject: SaveDangerousGoodRegistrationDto = plainToClass(
            SaveDangerousGoodRegistrationDto,
            requestBody
          );
          const errors: ValidationError[] = await validate(requestBodyObject);
          if (errors.length > 0) {
            throw new HttpException('Validation failed', HttpStatus.BAD_REQUEST);
          }
          return this.dangerousGoodRegistrationService.updateDangerousGoodRegistration(requestBodyObject);
        }
        break;
      }
      case PutHeader[PutHeader.disableRegistration]: {
        if (dangerousGoodRegistrationId != undefined) {
          return this.dangerousGoodRegistrationService.deleteDangerousGoodRegistrationFromWorldState(
            dangerousGoodRegistrationId
          );
        }
        break;
      }
      default: {
        throw new HttpException('Action not supported', HttpStatus.BAD_REQUEST);
      }
    }
    throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
  }
}
