/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { DtoParser } from '../../shared/dto-parser';
import { DangerousGoodRegistrationController } from './controller/dangerous-good-registration.controller';
import { DangerousGoodRegistrationService } from './service/dangerous-good-registration.service';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { DangerousGoodRegistrationHandler } from './handler/dangerous-good-registration.handler';

const amqpModule = AmqpModule.register(Queues.DATA_MANAGEMENT);

@Module({
  controllers: [DangerousGoodRegistrationController],
  providers: [DangerousGoodRegistrationHandler, DangerousGoodRegistrationService, DtoParser],
  imports: [HttpModule, amqpModule, DangerousGoodRegistrationModule],
  exports: [amqpModule],
})
export class DangerousGoodRegistrationModule {}
