/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { DtoParser } from '../../../shared/dto-parser';
import { DangerousGoodRegistrationService } from '../service/dangerous-good-registration.service';
import { DangerousGoodRegistrationController } from './dangerous-good-registration.controller';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { PutHeader } from './enum/put-header.enum';
import { HttpException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { DangerousGoodRegistrationHandler } from '../handler/dangerous-good-registration.handler';
import { mockDangerousGoodRegistration } from '@core/api-interfaces/lib/mocks/dangerous-good-registration/dangerous-good-registration.mock';

describe('DangerousGoodRegistrationController', () => {
  let dangerousGoodRegistrationController: DangerousGoodRegistrationController;
  let dangerousGoodRegistrationService: DangerousGoodRegistrationService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [DangerousGoodRegistrationController],
      providers: [DangerousGoodRegistrationService, DangerousGoodRegistrationHandler, DtoParser],
      imports: [HttpModule, ConfigModule, AmqpModule.register(Queues.DATA_MANAGEMENT)],
    }).compile();

    dangerousGoodRegistrationController = moduleRef.get<DangerousGoodRegistrationController>(
      DangerousGoodRegistrationController
    );
    dangerousGoodRegistrationService = moduleRef.get<DangerousGoodRegistrationService>(
      DangerousGoodRegistrationService
    );
  });

  it('should be defined', () => {
    expect(dangerousGoodRegistrationController).toBeDefined();
    expect(dangerousGoodRegistrationService).toBeDefined();
    expect(dangerousGoodRegistrationController.getDangerousGoodRegistration).toBeDefined();
    expect(dangerousGoodRegistrationController.getAllDangerousGoodRegistrations).toBeDefined();
    expect(dangerousGoodRegistrationController.createDangerousGoodRegistration).toBeDefined();
    expect(dangerousGoodRegistrationController.updateDangerousGoodRegistration).toBeDefined();
  });

  describe('updateDangerousGoodRegistration', () => {
    it('should call updateDangerousGoodRegistration', async () => {
      const updateDangerousGoodRegistrationSpy = jest
        .spyOn(DangerousGoodRegistrationService.prototype, 'updateDangerousGoodRegistration')
        .mockReturnValue({} as Observable<void>);
      await dangerousGoodRegistrationController.updateDangerousGoodRegistration(mockDangerousGoodRegistration);
      expect(updateDangerousGoodRegistrationSpy).toHaveBeenCalled();
    });

    it('should call deleteDangerousGoodRegistrationFromWorldState', async () => {
      const updateDangerousGoodRegistrationSpy = jest
        .spyOn(DangerousGoodRegistrationService.prototype, 'deleteDangerousGoodRegistrationFromWorldState')
        .mockReturnValue({} as Observable<void>);

      await dangerousGoodRegistrationController.updateDangerousGoodRegistration(
        null,
        PutHeader[PutHeader.disableRegistration],
        '1'
      );

      expect(updateDangerousGoodRegistrationSpy).toHaveBeenCalled();
      expect(dangerousGoodRegistrationService.deleteDangerousGoodRegistrationFromWorldState).toHaveBeenCalled();
    });

    it('should call nothing', async () => {
      await expect(
        async () => await dangerousGoodRegistrationController.updateDangerousGoodRegistration(undefined, '')
      ).rejects.toThrow(HttpException);
    });
  });
});
