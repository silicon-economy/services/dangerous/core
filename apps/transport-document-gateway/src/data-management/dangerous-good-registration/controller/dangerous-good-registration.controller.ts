/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Body, Controller, Get, Headers, Param, Post, Put } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiHeader, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { DangerousGoodRegistrationService } from '../service/dangerous-good-registration.service';
import { PutHeader } from './enum/put-header.enum';
import {
  CreateDangerousGoodRegistrationDto,
  SaveDangerousGoodRegistrationDto,
} from '@core/api-interfaces/lib/dtos/data-management';
import { Observable } from 'rxjs';
import { DangerousGoodRegistrationHandler } from '../handler/dangerous-good-registration.handler';

@ApiTags('Dangerous Good Registration')
@ApiBearerAuth('JWT-auth')
@Controller()
@ApiTags('v1')
export class DangerousGoodRegistrationController {
  constructor(
    private dangerousGoodRegistrationService: DangerousGoodRegistrationService,
    private dangerousGoodRegistrationHandler: DangerousGoodRegistrationHandler
  ) {}

  /**
   * Get dangerous good registration by id from the blockchain
   *
   * @param dangerousGoodRegistrationId - Dangerous good registration id
   * @returns Observable<SaveDangerousGoodRegistrationDto>
   */
  @Get(':dangerousGoodRegistrationId')
  @ApiOperation({
    summary: 'Get dangerous good registration by Id.',
    description: 'Gets a dangerous good registration by its id from the blockchain.',
  })
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  @ApiParam({ name: 'dangerousGoodRegistrationId', description: 'Dangerous good registration Id' })
  getDangerousGoodRegistration(
    @Param('dangerousGoodRegistrationId') dangerousGoodRegistrationId: string
  ): Observable<SaveDangerousGoodRegistrationDto> {
    return this.dangerousGoodRegistrationService.getDangerousGoodRegistration(dangerousGoodRegistrationId);
  }

  /**
   * Returns all dangerous good registrations from the blockchain.
   *
   * @returns Promise\<SaveDangerousGoodRegistrationDto[]\>
   */
  @Get('')
  @ApiOperation({
    summary: 'Get all dangerous good registrations.',
    description: 'Gets all dangerous good registrations from the blockchain.',
  })
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  getAllDangerousGoodRegistrations(): Observable<SaveDangerousGoodRegistrationDto[]> {
    return this.dangerousGoodRegistrationService.getAllDangerousGoodRegistrations();
  }

  /**
   * Save dangerous good registration to the blockchain
   *
   * @param createDangerousGoodRegistrationDto - dangerous good registration dto
   * @returns Observable<SaveDangerousGoodRegistrationDto>
   */
  @Post('')
  @ApiOperation({
    summary: 'Save a dangerous good registration.',
    description: 'Saves a dangerous good registration to the blockchain.',
  })
  @ApiBody({ description: 'The dangerous good registration to be saved', type: CreateDangerousGoodRegistrationDto })
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  createDangerousGoodRegistration(
    @Body() createDangerousGoodRegistrationDto: CreateDangerousGoodRegistrationDto
  ): Observable<SaveDangerousGoodRegistrationDto> {
    return this.dangerousGoodRegistrationService.saveDangerousGoodRegistration(createDangerousGoodRegistrationDto);
  }

  /**
   * Update a dangerous good registration in the blockchain
   *
   * @param requestBody - Request for UpdateDangerousGoodRegistration
   * @param typeOfAction - Type of action
   * @param dangerousGoodRegistrationId - Dangerous good registration id
   * @returns Observable\<SaveDangerousGoodRegistrationDto | void\>
   */
  @Put(':dangerousGoodRegistrationId?')
  @ApiOperation({
    summary: 'Update a dangerous good registration.',
    description: 'Updates a dangerous good registration in the blockchain.',
  })
  @ApiHeader({
    name: 'typeOfAction',
    enum: PutHeader,
    description:
      `Type of action other than to just update the dangerous good registration.\n
   - **` +
      PutHeader[PutHeader.disableRegistration] +
      `:** Disables a dangerous good registration. I.e. it is deleted from the world state of the blockchain.`,
    required: false,
  })
  @ApiBody({
    required: false,
    examples: {
      a: {
        summary: 'Request for UpdateDangerousGoodRegistration',
        description: 'Request for UpdateDangerousGoodRegistration',
        value: {} as SaveDangerousGoodRegistrationDto,
      },
    },
  })
  @ApiParam({
    name: 'dangerousGoodRegistrationId',
    description: 'Id of the dangerous good registration',
    required: false,
    allowEmptyValue: true,
  })
  @ApiResponse({ status: 201 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  async updateDangerousGoodRegistration(
    @Body() requestBody?: SaveDangerousGoodRegistrationDto | Partial<SaveDangerousGoodRegistrationDto>,
    @Headers('typeOfAction') typeOfAction?: string,
    @Param('dangerousGoodRegistrationId') dangerousGoodRegistrationId?: string
  ): Promise<Observable<void>> {
    return await this.dangerousGoodRegistrationHandler.handleUpdateDangerousGoodRegistration(
      requestBody,
      typeOfAction,
      dangerousGoodRegistrationId
    );
  }
}
