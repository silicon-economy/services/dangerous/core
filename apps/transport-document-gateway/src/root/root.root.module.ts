/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* istanbul ignore file */

import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { AppController } from './controller/root.controller';
import { AppService } from './service/root.service';

@Module({ controllers: [AppController], providers: [AppService], imports: [TerminusModule] })
export class RootModule {}
