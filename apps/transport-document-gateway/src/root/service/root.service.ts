/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* istanbul ignore file */

import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  HealthCheckResult,
  HealthCheckService,
  HealthIndicatorResult,
  HttpHealthIndicator,
  MemoryHealthIndicator,
} from '@nestjs/terminus';

@Injectable()
export class AppService {
  constructor(
    private health: HealthCheckService,
    private memory: MemoryHealthIndicator,
    private http: HttpHealthIndicator,
    public readonly configService: ConfigService
  ) {}

  /**
   * Get current status
   *
   * @returns Health check
   */
  getStatus(): Promise<HealthCheckResult> {
    return this.health.check([
      async (): Promise<HealthIndicatorResult> => this.memory.checkHeap('memory_heap', 200 * 1024 * 1024),
      async (): Promise<HealthIndicatorResult> => this.memory.checkRSS('memory_rss', 3000 * 1024 * 1024),
      async (): Promise<HealthIndicatorResult> =>
        this.http.pingCheck('Internet connection', 'https://www.google.de', {}),
    ]);
  }
}
