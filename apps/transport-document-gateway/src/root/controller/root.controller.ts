/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* istanbul ignore file */

import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { HealthCheck, HealthCheckResult } from '@nestjs/terminus';
import { AppService } from '../service/root.service';

@Controller()
@ApiTags('Misc')
export class AppController {
  constructor(private readonly appService: AppService) {}

  /**
   * Shows a current status of connections, and general infrastructure status and serves as a health check for kubernetes'
   *
   * @returns Status contained in app service
   */
  @ApiOperation({
    summary: 'Health Check.',
    description:
      'Shows a current status of connections, and general infrastructure status and serves as a health check for kubernetes',
  })
  @Get('/')
  @HealthCheck()
  getStatus(): Promise<HealthCheckResult> {
    return this.appService.getStatus();
  }
}
