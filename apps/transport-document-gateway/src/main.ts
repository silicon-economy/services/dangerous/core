/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* istanbul ignore file */

import { LogLevel, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { Config } from './config/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: process.env.logLevel ? [<LogLevel>process.env.logLevel] : ['error', 'warn', 'log', 'debug', 'verbose'],
    cors: true,
  });
  const configService = app.get(ConfigService);

  const options = new DocumentBuilder()
    .setTitle(Config.APPLICATION_TITLE)
    .setDescription(Config.APPLICATION_DESCRIPTION)
    .setVersion(Config.APPLICATION_VERSION)
    .addBearerAuth(
      {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
        name: 'JWT',
        description: 'Enter JWT token:',
        in: 'header',
      },
      'JWT-auth'
    )
    .build();
  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup(Config.SWAGGER_PATH, app, document, {
    swaggerOptions: {
      tagsSorter: 'method',
      operationsSorter: 'method',
    },
  });

  app.enableCors();

  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  await app.listen(configService.get<string>('httpPort'), () => console.log('Microservice is listening'));
}

void bootstrap();
