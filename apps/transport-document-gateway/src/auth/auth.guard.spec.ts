/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { AuthGuard } from '@authModule/auth.guard';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { APP_GUARD } from '@nestjs/core';
import { ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { createMock } from '@golevelup/ts-jest';
import jwt from 'jsonwebtoken';

describe('AuthGuard', () => {
  let authGuardRef: AuthGuard;
  let configService: ConfigService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      providers: [
        JwtService,
        {
          provide: APP_GUARD,
          useClass: AuthGuard,
        },
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn((key: string) => {
              if (key === 'dangerousPublicKey') {
                return 'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlHYk1CQUdCeXFHU000OUFnRUdCU3VCQkFBakE0R0dBQVFBYWdhYXhNQ1ZpaHMzSXZuemM1OE5KS2N3UExYMQo5SCtoanAyOTB0Z1BIeHhWWmxwQ0lqZEozTmRPc1Z4eVFQUGg0V1BsVUs5NTl0SUpyOVAxR2pRVERHSUJYYm9PClg0YkxmSXhsQnNveVFwR1d4RTd5MXpNYURURE11RkVBZU8ycEpLcHRqd253SVJucFJrWlRJYThibWZlU2ZCcEwKMEhYc3hzSURVcEU3NS8wUVZrbz0KLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0t';
              } else if (key === 'dangerousPrivateKey') {
                return 'LS0tLS1CRUdJTiBFQyBQUklWQVRFIEtFWS0tLS0tCk1JSGNBZ0VCQkVJQnM4a1FQKzEyT2FmYXpVSW5Lay93ZUxaaXN1RTd4ZmwybEYyaTBsUWxTUTB3SzNiUFRxTzcKWUFNdkw4d2ZJU2ZVQ1NHUU05NWdHUEJwUEY0QWx2NW9vTHFnQndZRks0RUVBQ09oZ1lrRGdZWUFCQUJxQnByRQp3SldLR3pjaStmTnpudzBrcHpBOHRmWDBmNkdPbmIzUzJBOGZIRlZtV2tJaU4wbmMxMDZ4WEhKQTgrSGhZK1ZRCnIzbjIwZ212MC9VYU5CTU1ZZ0ZkdWc1ZmhzdDhqR1VHeWpKQ2taYkVUdkxYTXhvTk1NeTRVUUI0N2Fra3FtMlAKQ2ZBaEdlbEdSbE1ocnh1Wjk1SjhHa3ZRZGV6R3dnTlNrVHZuL1JCV1NnPT0KLS0tLS1FTkQgRUMgUFJJVkFURSBLRVktLS0tLQ==';
              }
            }),
          },
        },
      ],
    }).compile();

    configService = moduleRef.get<ConfigService>(ConfigService);
    authGuardRef = new AuthGuard(configService);
  });

  it('should be defined', () => {
    expect(authGuardRef).toBeDefined();
  });

  it('should test canActivate with a context without a token', () => {
    const contextWithoutToken = createMock<ExecutionContext>();
    expect(() => {
      authGuardRef.canActivate(contextWithoutToken);
    }).toThrow(new UnauthorizedException());
  });

  it('should test canActivate with a context with an invalid token', () => {
    const contextWithInvalidToken = createMock<ExecutionContext>();
    contextWithInvalidToken.switchToHttp().getRequest.mockReturnValue({
      headers: {
        authorization: 'Bearer tokenxd',
      },
    });
    expect(() => {
      authGuardRef.canActivate(contextWithInvalidToken);
    }).toThrow(new UnauthorizedException());
  });

  it('should test canActivate with a context with a valid token', () => {
    const userPayload = { userId: 1, username: 'testuser' };
    const privateKey = Buffer.from(configService.get<string>('dangerousPrivateKey'), 'base64').toString('utf8');
    const validToken = jwt.sign(userPayload, privateKey, { algorithm: 'ES512' });
    console.log(validToken);
    const contextWithValidToken = createMock<ExecutionContext>();
    contextWithValidToken.switchToHttp().getRequest.mockReturnValue({
      headers: {
        authorization: `Bearer ${validToken}`,
      },
    });
    expect(authGuardRef.canActivate(contextWithValidToken)).toBeTruthy();
  });
});
