/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* istanbul ignore file */
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { WebSocket, WebSocketServer } from 'ws';
import { SensingPuckService } from '../service/sensing-puck.service';
import { AlertDto } from '@core/api-interfaces/lib/dtos/data-management';

@Injectable()
export class SensingPuckWebSocket {
  // For testing purposes, connect to this with "wscat -c ws://localhost:8086/alerting"
  private wss = new WebSocketServer({ port: this.configService.get('wsPort'), path: '/alerting' });
  private ws = new WebSocket(this.configService.get('wsUrl'));
  private sensingPuckWebSocket: SensingPuckWebSocket = this;

  constructor(public readonly configService: ConfigService, private sensingPuckService: SensingPuckService) {
    this.ws.on('message', (response: Buffer) => {
      const alert = this.sensingPuckWebSocket.parseAlertEventResponse(response);
      if (alert != undefined) {
        this.sensingPuckWebSocket.wss.clients.forEach(function each(client) {
          if (client !== this && client.readyState === WebSocket.OPEN) {
            client.send(Buffer.from(JSON.stringify(alert)));
          }
        });
      }
    });

    // Websocket Server
    // Sends history of alerts on connection
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    this.wss.on('connection', async (wsServer: WebSocket) => {
      wsServer.send(Buffer.from(JSON.stringify(await sensingPuckService.getAllPastEvents())));
    });
  }

  /**
   * Parse alert event response
   *
   * @param response - Response data
   * @returns Parsed altert dto
   */
  private parseAlertEventResponse(response: Buffer): AlertDto {
    let result: AlertDto;
    if (response != undefined) {
      result = JSON.parse(response.toLocaleString()) as AlertDto;
    }
    return result;
  }
}
