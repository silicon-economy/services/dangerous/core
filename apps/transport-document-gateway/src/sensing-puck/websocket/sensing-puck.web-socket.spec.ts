/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import config from '../../config/config';
import { SensingPuckModule } from '../sensing-puck.module';
import { SensingPuckWebSocket } from './sensing-puck.web-socket';

describe('SensingPuckWebSocket', () => {
  let service: SensingPuckWebSocket;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [SensingPuckModule, ConfigModule.forRoot({ isGlobal: true, load: [config] })],
    }).compile();

    service = module.get<SensingPuckWebSocket>(SensingPuckWebSocket);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
