/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* istanbul ignore file */
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom, Observable } from 'rxjs';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { MessagePatternsTendermintSensingPuck } from '@core/api-interfaces/lib/amqp/message-patterns.enum';
import { UploadSensingPuckDataDto } from '@core/api-interfaces/lib/dtos/data-management';
import {
  MsgFetchAllDeviceJobDataResponse,
  MsgFetchDeviceJobDataResponse,
  MsgUploadDeviceDataResponse,
} from '@core/api-interfaces/lib/dtos/blockchain/tx';
import { EventTypeAlert } from '@core/api-interfaces/lib/dtos/blockchain/eventType_alert';

@Injectable()
export class SensingPuckService {
  private readonly logger = new Logger(SensingPuckService.name);

  constructor(@Inject(Queues.TENDERMINT_SERVICE) public readonly client: ClientProxy) {}

  private apiBlockchainNode = process.env.API_BLOCKCHAIN_NODE || 'http://localhost:1317';
  private apiBlockchainRPC = process.env.API_BLOCKCHAIN_RPC || 'http://localhost:26657/';

  /**
   * Gets sensing puck data by job Id.
   *
   * @param orderPositionId - Order Position Id / job Id
   * @returns Sensing puck data
   */
  public getSensingPuckDataByOrderPositionId(orderPositionId: number): Observable<MsgFetchDeviceJobDataResponse> {
    this.logger.log('Get sensing puck data by job id/order position id ' + orderPositionId.toString());
    return this.client.send(MessagePatternsTendermintSensingPuck.GET_PUCK_DATA_BY_JOB_ID, orderPositionId);
  }

  /**
   * Get all sensing puck data
   *
   * @returns Array including all sensing puck data
   */
  public getAllSensingPuckData(): Observable<MsgFetchAllDeviceJobDataResponse> {
    this.logger.log('Get all sensing puck data');
    return this.client.send(MessagePatternsTendermintSensingPuck.GET_ALL_PUCK_DATA, {});
  }

  /**
   * Gets all previous events.
   *
   * @returns Array including all sensing puck data
   */
  public async getAllPastEvents(): Promise<EventTypeAlert> {
    this.logger.log('Get all past events');
    // TODO_LS: add dto
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const result = await firstValueFrom(this.client.send(MessagePatternsTendermintSensingPuck.GET_ALL_PAST_EVENTS, {}));
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/no-unsafe-return
    if (result.length < 1) return result;
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return,@typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
    return result.PastEvent.map((response) => response.eventTypeAlert);
  }

  /**
   * Upload sensing puck data
   *
   * @param uploadSensingPuckDataDto - Upload sensing puck data dto
   * @returns Sensing puck data dto
   */
  public uploadSensingPuckData(
    uploadSensingPuckDataDto: UploadSensingPuckDataDto
  ): Observable<MsgUploadDeviceDataResponse> {
    this.logger.log('Upload sensing puck data');
    return this.client.send(MessagePatternsTendermintSensingPuck.UPLOAD_SENSING_PUCK_DATA, uploadSensingPuckDataDto);
  }
}
