/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import config from '../../config/config';
import { SensingPuckModule } from '../sensing-puck.module';
import { SensingPuckWebSocket } from '../websocket/sensing-puck.web-socket';
import { SensingPuckService } from './sensing-puck.service';

describe('SensingPuckService', () => {
  let service: SensingPuckService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [SensingPuckModule, ConfigModule.forRoot({ isGlobal: true, load: [config] })],
    })
      .overrideProvider(SensingPuckWebSocket)
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      .useValue(() => {})
      .compile();

    service = module.get<SensingPuckService>(SensingPuckService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(service.getSensingPuckDataByOrderPositionId).toBeDefined();
    expect(service.getAllSensingPuckData).toBeDefined();
    expect(service.uploadSensingPuckData).toBeDefined();
  });
});
