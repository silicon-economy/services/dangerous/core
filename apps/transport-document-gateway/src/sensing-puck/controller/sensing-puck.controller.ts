/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* istanbul ignore file */
import { Body, Controller, Get, Logger, Param, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { SensingPuckService } from '../service/sensing-puck.service';
import { UploadSensingPuckDataDto } from '@core/api-interfaces/lib/dtos/data-management';
import { Observable } from 'rxjs';
import {
  MsgFetchAllDeviceJobDataResponse,
  MsgFetchDeviceJobDataResponse,
  MsgUploadDeviceDataResponse,
} from '@core/api-interfaces/lib/dtos/blockchain/tx';

@ApiTags('Sensing Puck')
@ApiBearerAuth('JWT-auth')
@Controller()
export class SensingPuckController {
  private logger = new Logger('SensingPuckController');

  constructor(private sensingPuckService: SensingPuckService) {}

  /**
   * Get sensing puck data by job id
   *
   * @param orderPositionId - Order position id
   * @returns Sensing puck data
   */
  @Get(':orderPositionId')
  @ApiOperation({
    summary: 'Get sensing puck data by job Id.',
    description: 'Gets sensing puck data by job Id.',
  })
  @ApiParam({
    name: 'orderPositionId',
    description: 'The orderPositionId/jobId of the device. OrderPositionId and jobId are equivalent.',
    required: true,
    type: Number,
  })
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  getSensingPuckDataByOrderPositionId(
    @Param('orderPositionId') orderPositionId?: number
  ): Observable<MsgFetchDeviceJobDataResponse> {
    return this.sensingPuckService.getSensingPuckDataByOrderPositionId(orderPositionId);
  }

  /**
   * Returns all sensing puck data.
   *
   * @returns  Promise\<any\>
   */
  @Get()
  @ApiOperation({
    summary: 'Get all sensing puck data.',
    description: 'Gets all sensing puck data.',
  })
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  getallSensingPuckData(): Observable<MsgFetchAllDeviceJobDataResponse> {
    return this.sensingPuckService.getAllSensingPuckData();
  }

  /**
   * Upload sensing puck data
   *
   * @param uploadSensingPuckDataDto - Sensing puck data dto
   * @returns Sensing puck data dto
   */
  @Post()
  @ApiOperation({
    summary: 'Upload sensing puck data.',
    description: 'Uploads sensing puck data.',
  })
  @ApiResponse({ status: 200 })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  uploadSensingPuckData(
    @Body() uploadSensingPuckDataDto: UploadSensingPuckDataDto
  ): Observable<MsgUploadDeviceDataResponse> {
    return this.sensingPuckService.uploadSensingPuckData(uploadSensingPuckDataDto);
  }
}
