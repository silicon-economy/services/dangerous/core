/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import config from '../../config/config';
import { SensingPuckModule } from '../sensing-puck.module';
import { SensingPuckService } from '../service/sensing-puck.service';
import { SensingPuckWebSocket } from '../websocket/sensing-puck.web-socket';
import { SensingPuckController } from './sensing-puck.controller';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';

describe('SensingPuckController', () => {
  let controller: SensingPuckController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        SensingPuckModule,
        HttpModule,
        AmqpModule.register(Queues.DATA_MANAGEMENT),
        ConfigModule.forRoot({ isGlobal: true, load: [config] }),
      ],
      controllers: [SensingPuckController],
      providers: [SensingPuckService],
    })
      .overrideProvider(SensingPuckWebSocket)
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      .useValue(() => {})
      .compile();

    controller = module.get<SensingPuckController>(SensingPuckController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(controller.getSensingPuckDataByOrderPositionId).toBeDefined();
    expect(controller.getallSensingPuckData).toBeDefined();
    expect(controller.uploadSensingPuckData).toBeDefined();
  });
});
