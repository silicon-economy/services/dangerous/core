/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { SensingPuckController } from './controller/sensing-puck.controller';
import { SensingPuckService } from './service/sensing-puck.service';
import { SensingPuckWebSocket } from './websocket/sensing-puck.web-socket';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

const amqpModule = AmqpModule.register(Queues.TENDERMINT_SERVICE);

@Module({
  imports: [HttpModule, amqpModule],
  providers: [SensingPuckService, SensingPuckWebSocket],
  controllers: [SensingPuckController],
  exports: [amqpModule],
})
export class SensingPuckModule {}
