/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* istanbul ignore file */

import { Inject, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { AuthModule } from './auth/auth.module';
import config from './config/config';
import { DangerousGoodRegistrationModule } from './data-management/dangerous-good-registration/dangerous-good-registration.module';
import { DataManagementModule } from './data-management/data-management.module';
import { TransportDocumentModule } from './data-management/transport-document/transport-document.module';
import { MediaManagementModule } from './media-management/media-management.module';
import { RootModule } from './root/root.root.module';
import { SensingPuckModule } from './sensing-puck/sensing-puck.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { RouterModule, Routes } from '@nestjs/core';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '/mediaManagement', module: MediaManagementModule },
      {
        path: '/transportDocument',
        module: TransportDocumentModule,
      },
      {
        path: '/dangerousGoodRegistration',
        module: DangerousGoodRegistrationModule,
      },
      {
        path: '/sensingPuck',
        module: SensingPuckModule,
      },
      {
        path: '',
        module: RootModule,
      },
    ],
  },
];

@Module({
  imports: [
    RouterModule.register(routes),
    DataManagementModule,
    MediaManagementModule,
    ConfigModule.forRoot({ isGlobal: true, load: [config] }),
    SensingPuckModule,
    AuthModule,
    RootModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {
  constructor(
    @Inject(Queues.DATA_MANAGEMENT) public readonly client: ClientProxy,
    @Inject(Queues.MEDIA_MANAGEMENT) public readonly client2: ClientProxy,
    @Inject(Queues.TENDERMINT_SERVICE) public readonly client3: ClientProxy
  ) {}

  async onApplicationBootstrap() {
    await this.client.connect();
    await this.client2.connect();
    await this.client3.connect();
  }
}
