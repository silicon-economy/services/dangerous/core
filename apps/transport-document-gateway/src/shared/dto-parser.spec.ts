/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DtoParser } from "./dto-parser";
import { CreateGoodsDataDto } from "@core/api-interfaces/lib/dtos/frontend";
import { mockTransportDocumentTransport } from "@core/api-interfaces/lib/mocks/transport-document/transport-document.mock";
import {BadRequestException} from "@nestjs/common";


describe('DtoParser', (): void => {
  let dtoParser: DtoParser;

  beforeEach(() => {
    dtoParser = new DtoParser();
  });

  it('should be defined', (): void => {
    expect(dtoParser).toBeDefined();
  });

  it('should parse createGoodsDataDto to saveGoodsDataDto', (): void => {
    const saveGoodsDataDto: CreateGoodsDataDto = dtoParser.parseDto(mockTransportDocumentTransport);

    expect(saveGoodsDataDto).toBeDefined();
    expect(saveGoodsDataDto.freight.orders).toHaveLength(1);
    expect(saveGoodsDataDto.freight.additionalInformation).toBe('Beförderung ohne Freistellung nach ADR 1.1.3.6');
  });
});
