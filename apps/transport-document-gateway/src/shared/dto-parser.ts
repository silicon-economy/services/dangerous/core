/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { BadRequestException, Injectable } from '@nestjs/common';
import * as ErrorStrings from '../resources/error.strings.json';
import { TransportDocumentStatus } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/status.enum';
import { CreateGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/create-goods-data.dto';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { FreightIdsIncluded } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/freight-ids-included';
import { Order } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order-with-id';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order-position-with-id';
import { OrderPosition } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order-position';
import { v4 as uuid } from 'uuid';

@Injectable()
export class DtoParser {
  /**
   * Takes a createGoodsDataDto and makes it saveGoodsDataDto. A saveGoodsDataDto holds more information, like ids for example.
   * The Id's are going to be set by the blockchain though.
   *
   * @param createGoodsDataDto - Create goods data dto
   * @returns The save goods data dto for the blockchain to supplement the Ids
   */
  parseDto(createGoodsDataDto: CreateGoodsDataDto): SaveGoodsDataDto {
    try {
      const dateAsUnixTimestamp: number = new Date().getTime();
      return new SaveGoodsDataDto(
        createGoodsDataDto.id,
        createGoodsDataDto.consignor,
        createGoodsDataDto.carrier,
        new FreightIdsIncluded(
          createGoodsDataDto.freight.orders.map((order: Order) => {
            return new OrderWithId(
              order.consignee,
              order.orderPositions.map((orderPosition: OrderPosition) => {
                return new OrderPositionWithId(
                  orderPosition.dangerousGood,
                  orderPosition.package,
                  orderPosition.packagingCode,
                  orderPosition.quantity,
                  orderPosition.unit,
                  orderPosition.individualAmount,
                  orderPosition.polluting,
                  orderPosition.transportPoints,
                  orderPosition.totalAmount,
                  undefined,
                  undefined,
                  undefined,
                  undefined
                );
              }),
              // eslint-disable-next-line @typescript-eslint/no-unsafe-call
              uuid(),
              undefined,
              undefined
            );
          }),
          createGoodsDataDto.freight.additionalInformation,
          createGoodsDataDto.freight.transportationInstructions,
          createGoodsDataDto.freight.totalTransportPoints
        ),
        [{ status: TransportDocumentStatus.created, date: dateAsUnixTimestamp, author: createGoodsDataDto.createdBy }],
        TransportDocumentStatus.created,
        dateAsUnixTimestamp,
        dateAsUnixTimestamp
      );
    } catch {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      throw new BadRequestException(ErrorStrings.ERROR_SAVE);
    }
  }
}
