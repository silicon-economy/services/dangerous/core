/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { DirectSecp256k1HdWallet, EncodeObject, Registry } from '@cosmjs/proto-signing';
import { BroadcastTxResponse, isBroadcastTxFailure, SigningStargateClient } from '@cosmjs/stargate';
import { Injectable, Logger } from '@nestjs/common';
import { ADDRESS_PREFIX, BLOCKCHAIN_RPC_ENDPOINT, DEFAULT_FEE } from '@config';
import { ConfigService } from '@nestjs/config';
import { stringify } from 'ts-jest';

@Injectable()
export class TendermintService {
  private TRANSACTION_COUNTER = 0;

  private readonly logger = new Logger('Tendermint Service');
  /** PART OF THE HOTFIX FOR VERSION 0.24 */
  private maxTimeout = 10;
  private timeoutMs = 1000;

  constructor(public readonly configService: ConfigService) {}

  /**
   * Sign broadcast with given params
   *
   * @param txs - Transactions
   * @param registry - Transaction registry with tx types
   */
  async signBroadcast(txs: EncodeObject[], registry: Registry) {
    const wallet = await DirectSecp256k1HdWallet.fromMnemonic(
      this.configService.get<string>('mnemonic'),
      undefined,
      ADDRESS_PREFIX
    );
    const stargate = SigningStargateClient;
    const client = await stargate.connectWithSigner(BLOCKCHAIN_RPC_ENDPOINT, wallet, {
      registry,
    });
    const [account] = await wallet.getAccounts();
    txs.forEach((tx) => {
      tx.value.creator = account.address;
    });

    // Retry sending the message and handle errors.
    for (let i = 0; i < this.maxTimeout; i++) {
      try {
        this.logger.debug('Transaction input:' + JSON.stringify(txs));
        const response: BroadcastTxResponse = await client.signAndBroadcast(account.address, txs, DEFAULT_FEE, '');

        if (await this.checkTxResult(response)) {
          try {
            return response.data[0].data;
          } catch (e) {
            this.logger.warn(e);
            this.logger.warn('Transaction OK, But nothing to decode.');
            return null;
          }
        }
      } catch (e) {
        this.logger.error(e);
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        await this.checkTxResult(e);
      }
    }
    throw new Error('Unexpected Error');
  }

  /**
   * Rudimentary Error Handling method based on Chain Responses.
   * Includes a bandaid fix in case we send two messages from the same wallet at the same time.
   *
   * @param response - Blockchain response
   * @returns Promise<boolean>
   */
  private async checkTxResult(response: BroadcastTxResponse): Promise<boolean> {
    this.TRANSACTION_COUNTER++;
    console.log(this.TRANSACTION_COUNTER);
    if (isBroadcastTxFailure(response)) {
      switch (response.code) {
        case 0: {
          return true;
        }
        case 1: {
          Logger.error(response);
          throw new Error('Misc Error: ' + stringify(response));
        }
        case 32: {
          this.logger.warn('Sequence mismatch: Retry in ' + stringify(this.timeoutMs) + ' Seconds');
          await this.delay(this.timeoutMs);
          return false;
        }
        case -32603: {
          this.logger.warn('Tx already in Cache: Retry in ' + stringify(this.timeoutMs) + ' Seconds');
          await this.delay(this.timeoutMs);
          return false;
        }
        // Authorization Errors
        case 4: {
          this.logger.warn('Authentication Error. Code: ' + stringify(response.code));
          throw new Error('Auth Error');
        }
        // Illegal TransportDocumentStatus Transaction
        case 18: {
          this.logger.warn('Attempted to perform an unauthorized transaction. Code: ' + stringify(response.code));
          throw new Error('Auth Transaction');
        }
        case 7: {
          this.logger.warn(
            'Attempted to perform an transaction with an invalid address. Code: ' + stringify(response.code)
          );
          throw new Error('Invalid Address');
        }
      }
    }

    return true;
  }

  //** PART OF THE HOTFIX FOR VERSION 0.24 */
  // eslint-disable-next-line jsdoc/require-jsdoc
  private async delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
}
