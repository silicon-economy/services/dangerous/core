/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TendermintService } from '@tendermintModule/service/tendermint.service';

@Module({
  imports: [HttpModule],
  providers: [TendermintService],
})
export class TendermintModule {}
