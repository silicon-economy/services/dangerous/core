/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AppModule } from '@appModule';
import config, { Queues } from '@config';
import { INestMicroservice, LogLevel } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import Long from 'long';
import protobufjs from 'protobufjs';

/**
 * Bootstraps the application with mqtt transport protocol.
 */
async function bootstrap() {
  // TODO: Remove, when cosmjs/blockchain is updated (Fixed by using https://github.com/protobufjs/protobuf.js/issues/1745#issuecomment-1200319399)
  protobufjs.util.Long = Long;
  protobufjs.configure();
  // TODO: Remove when the following is fixed https://github.com/nestjs/nest/issues/2343
  const appContext = await NestFactory.createApplicationContext(
    ConfigModule.forRoot({
      load: [config],
    }),
    { logger: process.env.logLevel ? [<LogLevel>process.env.logLevel] : ['error', 'warn', 'log', 'debug', 'verbose'] }
  );
  const configService = appContext.get(ConfigService);
  // TODO End

  const tendermintService: INestMicroservice = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: [configService.get('amqpUrl' as never)],
      queue: configService.get<string>('queuePrefix' as never) + Queues.TENDERMINT_SERVICE,
      queueOptions: {
        durable: true,
      },
    },
  });

  // TODO: Remove when the following is fixed https://github.com/nestjs/nest/issues/2343
  // Close the temporary app context since we no longer need it
  await appContext.close();
  // TODO End
  await tendermintService.listen();
}

// eslint-disable-next-line @typescript-eslint/no-floating-promises
void bootstrap();
