/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DangerousGoodRegistrationController } from '@dangerousGoodRegistrationModule/controller/dangerous-good-registration.controller';
import { DangerousGoodRegistrationModule } from '@dangerousGoodRegistrationModule/dangerous-good-registration.module';
import { DangerousGoodRegistrationService } from '@dangerousGoodRegistrationModule/service/dangerous-good-registration.service';
import { HttpModule } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { TendermintService } from '@tendermintModule/service/tendermint.service';
import { TendermintModule } from '@tendermintModule/tendermint.module';
import { ConfigModule } from '@nestjs/config';
import config from '@config';

describe('DangerousGoodRegistrationController', () => {
  let controller: DangerousGoodRegistrationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        DangerousGoodRegistrationModule,
        TendermintModule,
        HttpModule,
        ConfigModule.forRoot({ isGlobal: true, load: [config] }),
      ],
      controllers: [DangerousGoodRegistrationController],
      providers: [DangerousGoodRegistrationService, TendermintService],
    }).compile();

    controller = module.get<DangerousGoodRegistrationController>(DangerousGoodRegistrationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
