/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DangerousGoodRegistrationService } from '@dangerousGoodRegistrationModule/service/dangerous-good-registration.service';
import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { MessagePatternsTendermintDangerousGoodRegistration } from '@core/api-interfaces/lib/amqp/message-patterns.enum';
import { SaveDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/data-management';

@Controller()
export class DangerousGoodRegistrationController {
  constructor(private dangerousGoodRegistrationService: DangerousGoodRegistrationService) {}

  /**
   * Returns all dangerous goods registrations.
   *
   * @returns All dangerous goods registrations
   */
  @MessagePattern(MessagePatternsTendermintDangerousGoodRegistration.GET_ALL_DANGEROUS_GOOD_REGISTRATIONS)
  async findAll() {
    return this.dangerousGoodRegistrationService.findAll();
  }

  /**
   * Returns the corresponding dangerous goods registration of a given Id
   *
   * @param Id - Dangerous goods registration Id
   * @returns The dangerous goods registration corresponding to the given Id
   */
  @MessagePattern(MessagePatternsTendermintDangerousGoodRegistration.GET_DANGEROUS_GOOD_REGISTRATION)
  async findOne(Id: string) {
    return this.dangerousGoodRegistrationService.findOne(Id);
  }

  /**
   * Creates/Saves a given dangerous goods registration.
   *
   * @param saveDangerousGoodRegistration - Dangerous goods registration to be saved
   * @returns The saved dangerous goods registration
   */
  @MessagePattern(MessagePatternsTendermintDangerousGoodRegistration.CREATE_DANGEROUS_GOOD_REGISTRATION)
  async createOne(saveDangerousGoodRegistration: SaveDangerousGoodRegistrationDto) {
    return this.dangerousGoodRegistrationService.createOne(saveDangerousGoodRegistration);
  }

  /**
   * Deletes, i.e. it gets deleted in the blockchain's current world state only, a dangerous good registration from the blockchain.
   *
   * @param dangerousGoodRegistrationId - Dangerous goods registration Id
   * @returns cosmos response with empty data
   */
  @MessagePattern(MessagePatternsTendermintDangerousGoodRegistration.DELETE_DANGEROUS_GOOD_REGISTRATION)
  async deleteOne(dangerousGoodRegistrationId: string) {
    return this.dangerousGoodRegistrationService.deleteOne(dangerousGoodRegistrationId);
  }

  /**
   * Updates a dangerous good registration in the blockchain.
   *
   * @param saveDangerousGoodRegistration - Dangerous goods registration to be updated
   * @returns Updated transport document
   */
  @MessagePattern(MessagePatternsTendermintDangerousGoodRegistration.UPDATE_DANGEROUS_GOOD_REGISTRATION)
  async updateOne(saveDangerousGoodRegistration: SaveDangerousGoodRegistrationDto) {
    return this.dangerousGoodRegistrationService.updateOne(saveDangerousGoodRegistration);
  }
}
