/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TendermintService } from '@tendermintModule/service/tendermint.service';
import { DangerousGoodRegistrationController } from './controller/dangerous-good-registration.controller';
import { DangerousGoodRegistrationService } from './service/dangerous-good-registration.service';

@Module({
  imports: [HttpModule],
  providers: [DangerousGoodRegistrationService, TendermintService],
  controllers: [DangerousGoodRegistrationController],
})
export class DangerousGoodRegistrationModule {}
