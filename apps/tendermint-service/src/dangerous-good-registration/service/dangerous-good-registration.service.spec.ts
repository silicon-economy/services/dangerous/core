/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DangerousGoodRegistrationModule } from '@dangerousGoodRegistrationModule/dangerous-good-registration.module';
import { DangerousGoodRegistrationService } from '@dangerousGoodRegistrationModule/service/dangerous-good-registration.service';
import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import config from '@config';

describe('DangerousGoodRegistrationService', () => {
  let service: DangerousGoodRegistrationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DangerousGoodRegistrationModule, ConfigModule.forRoot({ isGlobal: true, load: [config] })],
    }).compile();

    service = module.get<DangerousGoodRegistrationService>(DangerousGoodRegistrationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
