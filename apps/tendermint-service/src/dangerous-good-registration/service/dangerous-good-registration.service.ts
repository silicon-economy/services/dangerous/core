/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-call */
import { EncodeObject, Registry } from '@cosmjs/proto-signing';
import { Injectable, Logger } from '@nestjs/common';
import { TendermintService } from '@tendermintModule/service/tendermint.service';
import { Reader } from 'protobufjs';
import { SaveDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/data-management';
import {
  MsgCreateDangerousGoodRegistration,
  MsgCreateDangerousGoodRegistrationResponse,
  MsgDeleteDangerousGoodRegistration,
  MsgDeleteDangerousGoodRegistrationResponse,
  MsgFetchAllDangerousGoodRegistrations,
  MsgFetchAllDangerousGoodRegistrationsResponse,
  MsgFetchDangerousGoodRegistration,
  MsgFetchDangerousGoodRegistrationResponse,
  MsgUpdateDangerousGoodRegistration,
  MsgUpdateDangerousGoodRegistrationResponse,
} from '@core/api-interfaces/lib/dtos/blockchain/tx';
import { MsgDeviceData } from '@core/api-interfaces/lib/dtos/blockchain/deviceJobData.dto';
import { DeleteGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/delete-save-goods-data.dto';
import { Config } from '@config';

@Injectable()
export class DangerousGoodRegistrationService {
  private readonly logger = new Logger(DangerousGoodRegistrationService.name);
  constructor(private tendermintService: TendermintService) {}

  types = [
    // Messages
    [Config.TENDERMINT_TYPE_URL + '.MsgUpdateDangerousGoodRegistration', MsgUpdateDangerousGoodRegistration],
    [Config.TENDERMINT_TYPE_URL + '.MsgDeleteDangerousGoodRegistration', MsgDeleteDangerousGoodRegistration],
    [Config.TENDERMINT_TYPE_URL + '.MsgCreateDangerousGoodRegistration', MsgCreateDangerousGoodRegistration],
    [Config.TENDERMINT_TYPE_URL + '.MsgDeviceData', MsgDeviceData],

    // Queries
    [Config.TENDERMINT_TYPE_URL + '.MsgFetchDangerousGoodRegistration', MsgFetchDangerousGoodRegistration],
    [Config.TENDERMINT_TYPE_URL + '.MsgFetchAllDangerousGoodRegistrations', MsgFetchAllDangerousGoodRegistrations],
  ];

  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-explicit-any
  registry = new Registry(<any>this.types);

  /**
   * Returns all dangerous goods registrations.
   *
   * @returns All dangerous goods registrations
   */
  public async findAll() {
    this.logger.debug('Fetch all dangerous good registrations from the blockchain');
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgFetchAllDangerousGoodRegistrations',
      value: { creator: '' },
    };

    return this.tendermintService.signBroadcast([tx], this.registry).then((data) => {
      return data == undefined
        ? { dangerousGoodRegistrations: [] }
        : MsgFetchAllDangerousGoodRegistrationsResponse.decode(new Reader(data));
    });
  }

  /**
   * Returns the corresponding dangerous goods registration of a given Id
   *
   * @param id - Dangerous goods registration Id
   * @returns The dangerous goods registration corresponding to the given Id
   */
  public findOne(id: string) {
    this.logger.debug('Fetch dangerous good registration with id ' + id + ' from the blockchain');
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgFetchDangerousGoodRegistration',
      value: { creator: '', id: id },
    };
    return this.tendermintService.signBroadcast([tx], this.registry).then((data) => {
      return MsgFetchDangerousGoodRegistrationResponse.decode(new Reader(data));
    });
  }

  /**
   * Creates/Saves a given dangerous goods registration.
   *
   * @param saveDangerousGoodRegistration - Dangerous goods registration to be saved
   * @returns The saved dangerous goods registration
   */
  public createOne(saveDangerousGoodRegistration: SaveDangerousGoodRegistrationDto) {
    this.logger.debug('Create a new dangerous good registration');
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgCreateDangerousGoodRegistration',
      value: saveDangerousGoodRegistration,
    };
    return this.tendermintService
      .signBroadcast([tx], this.registry)
      .then((data) => MsgCreateDangerousGoodRegistrationResponse.decode(new Reader(data)));
  }

  /**
   * Deletes, i.e. it gets deleted in the blockchain's current world state only, a dangerous good registration from the blockchain.
   *
   * @param dangerousGoodRegistrationId - Dangerous goods registration Id
   * @returns cosmos response with empty data
   */
  public deleteOne(dangerousGoodRegistrationId: string) {
    this.logger.debug(
      'Delete dangerous good registration with id ' + dangerousGoodRegistrationId + ' from the blockchain world state'
    );
    const deleteGoodsDataDTO: DeleteGoodsDataDto = {
      id: dangerousGoodRegistrationId,
      creator: '',
    };
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgDeleteDangerousGoodRegistration',
      value: deleteGoodsDataDTO,
    };
    return this.tendermintService.signBroadcast([tx], this.registry).then((data) => {
      if (data != undefined) {
        MsgDeleteDangerousGoodRegistrationResponse.decode(new Reader(data));
      }
    });
  }

  /**
   * Updates a dangerous good registration in the blockchain.
   *
   * @param saveDangerousGoodRegistration - Dangerous goods registration to be updated
   * @returns Updated transport document
   */
  public updateOne(saveDangerousGoodRegistration: SaveDangerousGoodRegistrationDto) {
    this.logger.debug('Update dangerous good registration with id ' + saveDangerousGoodRegistration.id);
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgUpdateDangerousGoodRegistration',
      value: saveDangerousGoodRegistration,
    };
    return this.tendermintService
      .signBroadcast([tx], this.registry)
      .then((data) => MsgUpdateDangerousGoodRegistrationResponse.decode(new Reader(data)));
  }
}
