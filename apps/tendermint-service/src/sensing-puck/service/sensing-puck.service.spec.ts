/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import config from '@config';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { SensingPuckModule } from '@sensingPuckModule/sensing-puck.module';
import { SensingPuckService } from '@sensingPuckModule/service/sensing-puck.service';

describe.skip('SensingPuckService', () => {
  let service: SensingPuckService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [SensingPuckModule, ConfigModule.forRoot({ isGlobal: true, load: [config] })],
      providers: [ConfigService],
    }).compile();

    service = module.get<SensingPuckService>(SensingPuckService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(service.findAllData).toBeDefined();
    expect(service.findDataByOrderPositionId).toBeDefined();
    expect(service.uploadDeviceData).toBeDefined();
  });
});
