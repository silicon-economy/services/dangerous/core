/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { EncodeObject, Registry } from '@cosmjs/proto-signing';
import { Injectable, Logger } from '@nestjs/common';
import { TendermintService } from '@tendermintModule/service/tendermint.service';
import { Reader } from 'protobufjs';
import {
  MsgFetchAllDeviceJobData,
  MsgFetchAllDeviceJobDataResponse,
  MsgFetchAllPastEvents,
  MsgFetchAllPastEventsResponse,
  MsgFetchDeviceJobData,
  MsgFetchDeviceJobDataResponse,
  MsgUploadDeviceData,
  MsgUploadDeviceDataResponse,
} from '@core/api-interfaces/lib/dtos/blockchain/tx';
import { UploadSensingPuckDataDto } from '@core/api-interfaces/lib/dtos/data-management';
import { Config } from '@config';

@Injectable()
export class SensingPuckService {
  private readonly logger = new Logger(SensingPuckService.name);
  constructor(private tendermintService: TendermintService) {}

  types = [
    // Messages
    [Config.TENDERMINT_TYPE_URL + '.MsgUploadDeviceData', MsgUploadDeviceData],

    //Queries
    [Config.TENDERMINT_TYPE_URL + '.MsgFetchDeviceJobData', MsgFetchDeviceJobData],
    [Config.TENDERMINT_TYPE_URL + '.MsgFetchAllDeviceJobData', MsgFetchAllDeviceJobData],
    [Config.TENDERMINT_TYPE_URL + '.MsgFetchAllPastEvents', MsgFetchAllPastEvents],
  ];

  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-explicit-any
  registry = new Registry(<any>this.types);

  /**
   * Gets sensing puck data by a given order position Id.
   *
   * @param orderPositionId - Order position Id
   * @returns Sensing puck data corresponding to the order position Id
   */
  public findDataByOrderPositionId(orderPositionId: number) {
    this.logger.debug('Get sensing puck data for order position id' + orderPositionId.toString());
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgFetchDeviceJobData',
      value: { creator: '', id: orderPositionId },
    };

    return this.tendermintService.signBroadcast([tx], this.registry).then((data) => {
      return data == undefined ? { deviceData: [] } : MsgFetchDeviceJobDataResponse.decode(new Reader(data));
    });
  }

  /**
   * Gets all sensing puck data.
   *
   * @returns All sensing puck data
   */
  public findAllData() {
    this.logger.debug('Get all sensing puck data');
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgFetchAllDeviceJobData',
      value: { creator: '' },
    };

    return this.tendermintService.signBroadcast([tx], this.registry).then((data) => {
      return data == undefined ? [] : MsgFetchAllDeviceJobDataResponse.decode(new Reader(data));
    });
  }

  /**
   * Uploads sensing puck data for a specified puck given in the Dto.
   *
   * @param uploadSensingPuckDataDTO - Dto for uploading sensing puck data
   * @returns Cosmos response with empty data field
   */
  public uploadDeviceData(uploadSensingPuckDataDTO: UploadSensingPuckDataDto) {
    this.logger.debug('Upload sensing puck data for device with id' + uploadSensingPuckDataDTO.deviceId.toString());
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgUploadDeviceData',
      value: uploadSensingPuckDataDTO,
    };
    return this.tendermintService
      .signBroadcast([tx], this.registry)
      .then((data) => MsgUploadDeviceDataResponse.decode(new Reader(data)));
  }

  /**
   * Gets all past events.
   *
   * @returns All past events
   */
  public getAllPastEvents() {
    this.logger.debug('Get all past events');
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgFetchAllPastEvents',
      value: { creator: '' },
    };

    return this.tendermintService.signBroadcast([tx], this.registry).then((data) => {
      return data == undefined ? [] : MsgFetchAllPastEventsResponse.decode(new Reader(data));
    });
  }
}
