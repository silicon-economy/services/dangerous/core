/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SensingPuckService } from '@sensingPuckModule/service/sensing-puck.service';
import * as alertSubscription from '@sensingPuckModule/websocket/alertSubscription/alertSubscription.json';
import { WebSocket, WebSocketServer } from 'ws';
import { AlertDto } from '@core/api-interfaces/lib/dtos/data-management';
import { SensingPuckConfig } from '@config';

@Injectable()
export class SensingPuckWebSocket {
  // For testing purposes, connect to this with "wscat -c ws://localhost:8080/alerting"
  private wss = new WebSocketServer({ port: this.configService.get<number>('wsPort'), path: '/alerting' });
  private ws = new WebSocket(this.configService.get('wsUrl'));
  private sensingPuckWebSocket: SensingPuckWebSocket = this;

  constructor(public readonly configService: ConfigService, private sensingPuckService: SensingPuckService) {
    this.ws.on('open', () => {
      this.ws.send(JSON.stringify(alertSubscription));
    });
    this.ws.on('message', (response: Buffer) => {
      const alert = this.sensingPuckWebSocket.parseAlertEventResponse(response);
      if (alert != undefined) {
        this.wss.clients.forEach(function each(client) {
          if (client !== this && client.readyState === WebSocket.OPEN) {
            client.send(Buffer.from(JSON.stringify(alert)));
          }
        });
      }
    });
  }

  /**
   * Parses an alert event response.
   *
   * @param response - response containing websocket event fired from blockchain
   * @returns response parsed into alertDto
   */
  private parseAlertEventResponse(response: Buffer): AlertDto {
    let result: AlertDto;
    if (response != undefined) {
      const event = (JSON.parse(response.toLocaleString()) as { result: { events: AlertDto } }).result?.events;
      if (event != undefined) {
        const className = SensingPuckConfig.ALERT_EVENT_CLASS_NAME;
        result = {
          deviceId: parseInt(String(event[className + 'deviceId']).replace('"', '')),
          jobId: parseInt(String(event[className + 'jobId']).replace('"', '')),
          temperature: parseFloat(event[className + 'temperature'] as string),
          upperTemperatureThreshold: parseFloat(event[className + 'upperTemperatureThreshold'] as string),
          lowerTemperatureThreshold: parseFloat(event[className + 'lowerTemperatureThreshold'] as string),
          humidity: parseFloat(event[className + 'humidity'] as string),
          upperHumidityThreshold: parseFloat(event[className + 'upperHumidityThreshold'] as string),
          lowerHumidityThreshold: parseFloat(event[className + 'lowerHumidityThreshold'] as string),
          timestamp: Date.parse(String(event[className + 'timestamp']).replace(/"/g, '')),
        };
      }
    }
    return result;
  }
}
