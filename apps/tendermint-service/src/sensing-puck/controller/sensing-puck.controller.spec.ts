/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import config from '@config';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { SensingPuckController } from '@sensingPuckModule/controller/sensing-puck.controller';
import { SensingPuckModule } from '@sensingPuckModule/sensing-puck.module';
import { SensingPuckService } from '@sensingPuckModule/service/sensing-puck.service';
import { TendermintService } from '@tendermintModule/service/tendermint.service';
import { TendermintModule } from '@tendermintModule/tendermint.module';

describe.skip('SensingPuckController', () => {
  let controller: SensingPuckController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        SensingPuckModule,
        TendermintModule,
        HttpModule,
        ConfigModule.forRoot({ isGlobal: true, load: [config] }),
      ],
      controllers: [SensingPuckController],
      providers: [SensingPuckService, TendermintService],
    }).compile();

    controller = module.get<SensingPuckController>(SensingPuckController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(controller.getAllSensingPuckData).toBeDefined();
    expect(controller.getSensingPuckDataByOrderPosition).toBeDefined();
    expect(controller.uploadData).toBeDefined();
  });
});
