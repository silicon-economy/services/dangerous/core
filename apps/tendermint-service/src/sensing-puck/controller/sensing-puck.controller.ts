/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-return */
import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { SensingPuckService } from '@sensingPuckModule/service/sensing-puck.service';
import { MessagePatternsTendermintSensingPuck } from '@core/api-interfaces/lib/amqp/message-patterns.enum';
import { UploadSensingPuckDataDto } from '@core/api-interfaces/lib/dtos/data-management';

@Controller('sensing-puck')
export class SensingPuckController {
  constructor(private sensingPuckService: SensingPuckService) {}

  /**
   * Gets sensing puck data by a given order position Id.
   *
   * @param orderPositionId - Order position Id
   * @returns Sensing puck data corresponding to the order position Id
   */
  @MessagePattern(MessagePatternsTendermintSensingPuck.GET_PUCK_DATA_BY_JOB_ID)
  async getSensingPuckDataByOrderPosition(orderPositionId: number) {
    return this.sensingPuckService.findDataByOrderPositionId(orderPositionId);
  }

  /**
   * Gets all sensing puck data.
   *
   * @returns All sensing puck data
   */
  @MessagePattern(MessagePatternsTendermintSensingPuck.GET_ALL_PUCK_DATA)
  async getAllSensingPuckData() {
    return this.sensingPuckService.findAllData();
  }

  /**
   * Gets all previous events.
   *
   * @returns All previous events
   */
  @MessagePattern(MessagePatternsTendermintSensingPuck.GET_ALL_PAST_EVENTS)
  async getAllPastEvents() {
    return this.sensingPuckService.getAllPastEvents();
  }

  /**
   * Uploads sensing puck data for a specified puck given in the Dto.
   *
   * @param uploadSensingPuckDataDTO - dto for uploading sensing puck data
   * @returns cosmos response with empty data field
   */
  @MessagePattern(MessagePatternsTendermintSensingPuck.UPLOAD_SENSING_PUCK_DATA)
  async uploadData(uploadSensingPuckDataDTO: UploadSensingPuckDataDto) {
    return this.sensingPuckService.uploadDeviceData(uploadSensingPuckDataDTO);
  }
}
