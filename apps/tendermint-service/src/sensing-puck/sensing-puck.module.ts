/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { SensingPuckController } from '@sensingPuckModule/controller/sensing-puck.controller';
import { SensingPuckService } from '@sensingPuckModule/service/sensing-puck.service';
import { SensingPuckWebSocket } from '@sensingPuckModule/websocket/sensing-puck.web-socket';
import { TendermintService } from '@tendermintModule/service/tendermint.service';

@Module({
  imports: [HttpModule],
  providers: [SensingPuckService, TendermintService, SensingPuckWebSocket],
  controllers: [SensingPuckController],
})
export class SensingPuckModule {}
