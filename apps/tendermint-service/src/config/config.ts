/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export const ADDRESS_PREFIX = 'cosmos';
export const BLOCKCHAIN_RPC_ENDPOINT = process.env.API_BLOCKCHAIN_RPC || 'http://localhost:26657/';
export const DEFAULT_FEE = {
  amount: [
    {
      denom: 'token',
      amount: '0',
    },
  ],
  gas: '1800000',
};

/**
 * Tendermint link pattern
 */
export enum Config {
  TENDERMINT_CHAIN_ID = '/org/businesslogic/businesslogic/',
  TENDERMINT_TYPE_URL = '/git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic',
}

/**
 * Sensing puck configuration
 */
export enum SensingPuckConfig {
  ALERT_EVENT_CLASS_NAME = 'git.openlogisticsfoundation.org.silicon_economy.services.dangerous.core.apps.blockchain.businesslogic.EventTypeAlert.',
}

/**
 * Tendermint service pattern
 */
export enum Queues {
  TENDERMINT_SERVICE = 'tendermintService',
}

export default () => ({
  mnemonic: process.env.DANGEROUS_USER_MNEMONIC || '',
  queuePrefix: process.env.DEPLOYMENT_ENVIRONMENT || '',
  amqpUrl: process.env.AMQP_URL || 'amqp://guest:guest@0.0.0.0:5672',
  wsPort: Number(process.env.WS_PORT) || 8080,
  wsUrl: process.env.WS_URL || 'ws://0.0.0.0:26657/websocket',
});
