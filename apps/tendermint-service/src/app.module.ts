/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import config from '@config';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { SensingPuckModule } from '@sensingPuckModule/sensing-puck.module';
import { TendermintModule } from '@tendermintModule/tendermint.module';
import { TransportDocumentModule } from '@transportDocumentModule/transport-document.module';
import { DangerousGoodRegistrationModule } from './dangerous-good-registration/dangerous-good-registration.module';
import { RouterModule, Routes } from '@nestjs/core';

const routes: Routes = [
  {
    path: 'DangerousBlockchain/v1',
    module: TransportDocumentModule,
  },
];

@Module({
  imports: [
    RouterModule.register(routes),
    TransportDocumentModule,
    TendermintModule,
    SensingPuckModule,
    ConfigModule.forRoot({ isGlobal: true, load: [config] }),
    DangerousGoodRegistrationModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
