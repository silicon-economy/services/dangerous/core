/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { TendermintService } from '@tendermintModule/service/tendermint.service';
import { TendermintModule } from '@tendermintModule/tendermint.module';
import { TransportDocumentController } from '@transportDocumentModule/controller/transport-document.controller';
import { TransportDocumentService } from '@transportDocumentModule/service/transport-document.service';
import { TransportDocumentModule } from '@transportDocumentModule/transport-document.module';
import { ConfigModule } from '@nestjs/config';
import config from '@config';

describe('TransportDocumentController', () => {
  let controller: TransportDocumentController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TransportDocumentModule,
        TendermintModule,
        HttpModule,
        ConfigModule.forRoot({ isGlobal: true, load: [config] }),
      ],
      controllers: [TransportDocumentController],
      providers: [TransportDocumentService, TendermintService],
    }).compile();

    controller = module.get<TransportDocumentController>(TransportDocumentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
