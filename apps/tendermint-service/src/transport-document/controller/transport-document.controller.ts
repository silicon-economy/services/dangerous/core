/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { TransportDocumentService } from '@transportDocumentModule/service/transport-document.service';
import { MessagePatternsTendermintTransportDocument } from '@core/api-interfaces/lib/amqp/message-patterns.enum';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';

@Controller()
export class TransportDocumentController {
  constructor(private transportDocumentService: TransportDocumentService) {}

  /**
   * Returns all transport documents.
   *
   * @returns All transport documents
   */
  @MessagePattern(MessagePatternsTendermintTransportDocument.GET_ALL_TRANSPORT_DOCUMENTS)
  async findAll() {
    return this.transportDocumentService.findAll();
  }

  /**
   * Returns the corresponding transport document of a given Id
   *
   * @param Id - Transport document Id
   * @returns The transport document corresponding to the given Id
   */
  @MessagePattern(MessagePatternsTendermintTransportDocument.GET_TRANSPORT_DOCUMENT)
  async findOne(Id: string) {
    return this.transportDocumentService.findOne(Id);
  }

  /**
   * Gets the corresponding transport document Id for a given order position Id.
   *
   * @param orderPositionId - order position Id
   * @returns the transport document Id containing the given order position
   */
  @MessagePattern(MessagePatternsTendermintTransportDocument.GET_TRANSPORT_DOCUMENT_ID_BY_ORDER_POSITION_ID)
  async getTransportDocumentIdByOrderPositionId(orderPositionId: number) {
    return this.transportDocumentService.getTransportDocumentIdByOrderPositionId(orderPositionId);
  }

  /**
   * Creates/Saves a given transport document.
   *
   * @param saveGoodsDataDto - Transport document to be saved
   * @returns The saved transport document
   */
  @MessagePattern(MessagePatternsTendermintTransportDocument.CREATE_TRANSPORT_DOCUMENT)
  async createOne(saveGoodsDataDto: SaveGoodsDataDto) {
    return this.transportDocumentService.createOne(saveGoodsDataDto);
  }

  /**
   * Deletes, i.e. it gets deleted in the blockchain's current world state only, a transport document from the blockchain.
   *
   * @param transportDocumentId - Transport document Id
   * @returns cosmos response with empty data
   */
  @MessagePattern(MessagePatternsTendermintTransportDocument.DELETE_TRANSPORT_DOCUMENT)
  async deleteOne(transportDocumentId: string) {
    return this.transportDocumentService.deleteOne(transportDocumentId);
  }

  /**
   * Updates a transport document in the blockchain.
   *
   * @param saveGoodsDataDto - Transport document to be updated
   * @returns Updated transport document
   */
  @MessagePattern(MessagePatternsTendermintTransportDocument.UPDATE_TRANSPORT_DOCUMENT)
  async updateTransportDocument(saveGoodsDataDto: SaveGoodsDataDto) {
    return this.transportDocumentService.updateTransportDocument(saveGoodsDataDto);
  }
}
