/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { TransportDocumentService } from '@transportDocumentModule/service/transport-document.service';
import { TransportDocumentModule } from '@transportDocumentModule/transport-document.module';
import { ConfigModule } from '@nestjs/config';
import config from '@config';

describe('TransportDocumentService', () => {
  let service: TransportDocumentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TransportDocumentModule, ConfigModule.forRoot({ isGlobal: true, load: [config] })],
    }).compile();

    service = module.get<TransportDocumentService>(TransportDocumentService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
