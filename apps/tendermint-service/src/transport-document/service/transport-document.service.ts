/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-call */
import { EncodeObject, Registry } from '@cosmjs/proto-signing';
import { Injectable, Logger } from '@nestjs/common';
import { TendermintService } from '@tendermintModule/service/tendermint.service';
import { Reader } from 'protobufjs';
import {
  MsgAddDeviceToOrderPosition,
  MsgCreateTransportDocument,
  MsgCreateTransportDocumentResponse,
  MsgDeleteDangerousGoodRegistration,
  MsgDeleteTransportDocument,
  MsgDeleteTransportDocumentResponse,
  MsgFetchAllTransportDocuments,
  MsgFetchAllTransportDocumentsResponse,
  MsgFetchTransportDocument,
  MsgFetchTransportDocumentIdByOrderPositionId,
  MsgFetchTransportDocumentIdByOrderPositionIdResponse,
  MsgFetchTransportDocumentResponse,
  MsgFinishIoTBrokerJob,
  MsgRemoveDeviceFromOrderPosition,
  MsgRevertToGenesis,
  MsgUpdateTransportDocument,
  MsgUpdateTransportDocumentResponse,
} from '@core/api-interfaces/lib/dtos/blockchain/tx';
import { MsgDeviceData } from '@core/api-interfaces/lib/dtos/blockchain/deviceJobData.dto';
import { DeleteGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/delete-save-goods-data.dto';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { Config } from '@config';

import * as util from 'util';

@Injectable()
export class TransportDocumentService {
  private readonly logger = new Logger(TransportDocumentService.name);

  constructor(private tendermintService: TendermintService) {}

  types = [
    // Messages
    [Config.TENDERMINT_TYPE_URL + '.MsgDeleteTransportDocument', MsgDeleteTransportDocument],
    [Config.TENDERMINT_TYPE_URL + '.MsgAddDeviceToOrderPosition', MsgAddDeviceToOrderPosition],
    [Config.TENDERMINT_TYPE_URL + '.MsgDeviceData', MsgDeviceData],
    [Config.TENDERMINT_TYPE_URL + '.MsgFinishIoTBrokerJob', MsgFinishIoTBrokerJob],
    [Config.TENDERMINT_TYPE_URL + '.MsgUpdateTransportDocument', MsgUpdateTransportDocument],
    [Config.TENDERMINT_TYPE_URL + '.MsgCreateTransportDocument', MsgCreateTransportDocument],
    [Config.TENDERMINT_TYPE_URL + '.MsgRemoveDeviceFromOrderPosition', MsgRemoveDeviceFromOrderPosition],
    [Config.TENDERMINT_TYPE_URL + '.MsgRevertToGenesis', MsgRevertToGenesis],
    [Config.TENDERMINT_TYPE_URL + '.MsgDeleteDangerousGoodRegistration', MsgDeleteDangerousGoodRegistration],

    // Queries
    [Config.TENDERMINT_TYPE_URL + '.MsgFetchTransportDocument', MsgFetchTransportDocument],
    [
      Config.TENDERMINT_TYPE_URL + '.MsgFetchTransportDocumentIdByOrderPositionId',
      MsgFetchTransportDocumentIdByOrderPositionId,
    ],
    [Config.TENDERMINT_TYPE_URL + '.MsgFetchAllTransportDocuments', MsgFetchAllTransportDocuments],
  ];

  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-explicit-any
  registry = new Registry(<any>this.types);


  /**
   * Returns all transport documents.
   *
   * @returns all transport documents
   */
  public async findAll() {
    this.logger.debug('Fetch all transport documents from the blockchain');
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgFetchAllTransportDocuments',
      value: { creator: '' },
    };

    return this.tendermintService.signBroadcast([tx], this.registry).then((data) => {
      return data == undefined
        ? { transportDocument: [] }
        : MsgFetchAllTransportDocumentsResponse.decode(new Reader(data));
    });
  }


  /**
   * Returns the corresponding transport document of a given Id
   *
   * @param id - transport document id
   * @returns the transport document corresponding to the given id
   */
  public async findOne(id: string) {
    this.logger.debug('Fetch transport document with id ' + id + ' from the blockchain');
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgFetchTransportDocument',
      value: { creator: '', id: id },
    };

    return this.tendermintService.signBroadcast([tx], this.registry).then((data) => {
      return MsgFetchTransportDocumentResponse.decode(new Reader(data));
    });
  }


  /**
   * Gets the corresponding transport document Id for a given order position Id.
   *
   * @param orderPositionId - order position id
   * @returns the transport document id containing the given order position
   */
  public async getTransportDocumentIdByOrderPositionId(orderPositionId: number) {
    this.logger.debug('Fetch transport document id by orderPositionId ' + orderPositionId.toString());
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgFetchTransportDocumentIdByOrderPositionId',
      value: { creator: '', orderPositionId: orderPositionId.toString() },
    };

    return this.tendermintService.signBroadcast([tx], this.registry).then((data) => {
      return MsgFetchTransportDocumentIdByOrderPositionIdResponse.decode(new Reader(data));
    });
  }


  /**
   * Creates/Saves a given transport document.
   *
   * @param saveGoodsDataDto - transport document to be saved
   * @returns the saved transport document
   */
  public async createOne(saveGoodsDataDto: SaveGoodsDataDto) {
    this.logger.debug('Create a new transport document');
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgCreateTransportDocument',
      value: saveGoodsDataDto,
    };
    return this.tendermintService
      .signBroadcast([tx], this.registry)
      .then((data) => MsgCreateTransportDocumentResponse.decode(new Reader(data)));
  }


  /**
   * Deletes, i.e. it gets deleted in the blockchain's current world state only, a transport document from the blockchain.
   *
   * @param transportDocumentId - Transport document Id
   * @returns cosmos response with empty data
   */
  public deleteOne(transportDocumentId: string) {
    this.logger.debug('Delete transport document with id ' + transportDocumentId + ' from the blockchain world state');
    const deleteGoodsDataDto: DeleteGoodsDataDto = { id: transportDocumentId, creator: '' };
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgDeleteTransportDocument',
      value: deleteGoodsDataDto,
    };
    return this.tendermintService.signBroadcast([tx], this.registry).then((data) => {
      if (data != undefined) {
        MsgDeleteTransportDocumentResponse.decode(new Reader(data));
      }
    });
  }


  /**
   * Updates a transport document in the blockchain.
   *
   * @param saveGoodsDataDto - transport document to be updated
   * @returns updated transport document
   */
  public updateTransportDocument(saveGoodsDataDto: SaveGoodsDataDto) {
    this.logger.debug('Update transport document with id ' + saveGoodsDataDto.id);
    const tx: EncodeObject = {
      typeUrl: Config.TENDERMINT_TYPE_URL + '.MsgUpdateTransportDocument',
      value: saveGoodsDataDto,
    };
    console.log(util.inspect(saveGoodsDataDto, { showHidden: false, depth: null, colors: true }));
    return this.tendermintService
      .signBroadcast([tx], this.registry)
      .then((data) => MsgUpdateTransportDocumentResponse.decode(new Reader(data)));
  }
}
