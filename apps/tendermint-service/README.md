# dangerous.backend.tendermint.service

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

# Using

Uses Port :8085 , which is also defined in src/config/config.json
Swagger at http://localhost:8085/api
OpenAPI JSON at http://localhost:8085/api-json

This client is used for querying and posting dangerous transactions from/in the blockchain.

For the authentification the mnemonic and the user address need to be deposited in the config.json. The mnemonic and the address will be received after registering a new participant in the blockchain network. You have to contact the responsible blockchain admin for the information.

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## License

See attached File LICENSE
