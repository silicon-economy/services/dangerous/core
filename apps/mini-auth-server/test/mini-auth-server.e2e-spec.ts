/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import axios from 'axios';

describe('GET /app/healthCheck', () => {
  it('should return a message', async () => {
    const res = await axios.get(`/app/healthCheck`);

    expect(res.status).toBe(200);
    expect(res.data).toEqual({
      details: {
        memory_heap: {
          status: 'up',
        },
        memory_rss: {
          status: 'up',
        },
      },
      error: {},
      info: {
        memory_heap: {
          status: 'up',
        },
        memory_rss: {
          status: 'up',
        },
      },
      status: 'ok',
    });
  });
});
