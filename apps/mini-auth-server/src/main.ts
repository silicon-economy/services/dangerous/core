/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Logger, LogLevel } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { Config } from '@config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: process.env.logLevel ? [<LogLevel>process.env.logLevel] : ['error', 'warn', 'log'],
    cors: true
  });
  const configService = app.get(ConfigService);
  const port = configService.get<string>('httpPort');

  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle(Config.APPLICATION_TITLE.toString())
    .setDescription(Config.APPLICATION_DESCRIPTION)
    .setVersion(Config.APPLICATION_VERSION)
    .addSecurity('basic', { type: 'http', scheme: 'basic' })
    .addBasicAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup(Config.SWAGGER_PATH, app, document);
  await app.listen(port, () => Logger.log(`🚀 Application is running on: http://localhost:${port}`));
}

void bootstrap().then();
