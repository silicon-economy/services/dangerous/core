/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpException,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { UserService } from './user.service';
import {
  ApiBearerAuth,
  ApiBody,
  ApiHeader,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import {
  createUserDtoCarrierDummy,
  createUserDtoConsigneeDummy,
  createUserDtoConsignorAndConsigneeDummy,
  createUserDtoDummy
} from "@miniAuthServer/user/dto/create-user-dto.dummy";
import { CreateUserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/create-user.dto';
import { UpdateUserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/update-user.dto';
import { UserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/user.dto';
import { User } from '@miniAuthServer/user/entities/user.entity';

@ApiTags('UserController')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  /**
   * Creates a new user.
   *
   * @param createUserDto - Create user dto
   */
  @Post()
  @ApiOperation({
    summary: 'Create a new user.',
    description: 'Creates a new user.',
    deprecated: true,
  })
  @ApiBody({
    required: true,
    examples: {
      a: {
        summary: 'Consignor',
        description: 'Example request body for a consignor',
        value: createUserDtoDummy
      },
      b: {
        summary: 'Carrier',
        description: 'Example request body for a carrier',
        value: createUserDtoCarrierDummy
      },
      c: {
        summary: 'Consignee',
        description: 'Example request body for a consignee',
        value: createUserDtoConsigneeDummy
      },
      d: {
        summary: 'Consignor + Consignee',
        description: 'Example request body for a consignor, who is also a consignee',
        value: createUserDtoConsignorAndConsigneeDummy
      },
    },
  })
  @ApiInternalServerErrorResponse({ description: 'New user creation not allowed temporarily!' })
  // Creation of users disabled momentarily.
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  create(@Body() createUserDto: CreateUserDto): string | void {
    throw new HttpException('New user creation not allowed temporarily!', HttpStatus.FORBIDDEN);
  }

  /**
   * If "Token" is specified, the token is validated and the user data is returned.
   * If `authHeader` is provided, the `login` method is called to authenticate
   * the user and the jwt token is returned.
   * If neither `token` nor `authHeader` is provided,
   * it calls the `convertAllUserToDto` method to convert all the user objects
   * to DTOs and returns them.
   *
   * @param authHeader - Authentication header
   * @param token - Authentication token
   * @returns Returns a User object, a string, or an array of UserDto objects.
   */
  @Get('authenticateAndGetUserDetails')
  @ApiBearerAuth('basic')
  @ApiOperation({
    summary: 'Authenticate or get user details.',
    description:
      '# Use Cases \n' +
      '1. If `Token` is specified, the token is validated and the user data is returned.\n' +
      '2. If `authorization` is provided, the `login` method is called to authenticate ' +
      'the user and the jwt token is returned.\n' +
      '3. If neither `token` nor `authorization` is provided, ' +
      'it calls the `convertAllUserToDto` method to convert all the user objects ' +
      'to DTOs and returns them.\n' +
      '4. If swagger basic auth is used and neither `token` nor `authorization` is provided, ' +
      'the `login` method is called to authenticate the user and the jwt token is returned.',
  })
  @ApiHeader({
    name: 'authorization',
    description:
      'If `authorization` is provided, the `login` method is called to authenticate ' +
      'the user and get the user details.',
    required: false,
  })
  @ApiQuery({
    name: 'token',
    type: String,
    description: 'If `Token` is specified, the token is validated and the user data is returned.',
    required: false,
  })
  authenticateAndGetUserDetails(
    @Headers('authorization') authHeader?: string,
    @Query('token') token?: string
  ): string | User | UserDto[] {
    return this.userService.authenticateAndGetUserDetails(authHeader, token);
  }

  /**
   * Returns user information for a given user id.
   *
   * @param id - User id of user to be requested
   * @returns User information
   */
  @Get(':id')
  @ApiOperation({
    summary: 'Get user data for a given user id.',
    description: 'Gets user data for a given user id.',
  })
  @ApiParam({
    name: 'id',
    type: String,
    description: 'User Id of requested user',
    required: true,
  })
  getUser(@Param('id') id: string): UserDto {
    return this.userService.getUserDetails(id);
  }

  /**
   * Updates an existing user and returns the updated user.
   *
   * @param id - User id of user to be modified
   * @param updateUserDto - Update user dto with new changes
   */
  @Patch(':id')
  @ApiOperation({
    summary: 'Update an existing user and returns the updated user.',
    description: 'Updates an existing user and returns the updated user.',
    deprecated: true,
  })
  @ApiBody({
    required: true,
    examples: {
      a: {
        summary: 'Consignor',
        description: 'Example request body for a consignor',
        value: createUserDtoDummy
      },
    },
  })
  @ApiParam({
    name: 'id',
    type: String,
    description: 'User Id of user to be updated',
    required: true,
  })
  @ApiInternalServerErrorResponse({ description: 'User update not allowed temporarily!' })
  // Update of users disabled momentarily.
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto): UserDto {
    throw new HttpException('User update not allowed temporarily!', HttpStatus.FORBIDDEN);
  }

  /**
   * Deletes an existing user given by its id.
   *
   * @param id - User id of user to be deleted
   */
  @Delete(':id')
  @ApiOperation({
    summary: 'Delete an existing user given by its id.',
    description: 'Deletes an existing user given by its id.',
    deprecated: true,
  })
  @ApiParam({
    name: 'id',
    type: String,
    description: 'User Id of user to be deleted',
    required: true,
  })
  @ApiInternalServerErrorResponse({ description: 'User deletion not allowed temporarily!' })
  // Removal of users disabled momentarily.
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  remove(@Param('id') id: string): void | string {
    throw new HttpException('User deletion not allowed temporarily!', HttpStatus.FORBIDDEN);
  }
}
