/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserController } from '@miniAuthServer/user/user.controller';

describe('UserController', () => {
  let controller: UserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      controllers: [UserController],
      providers: [
        UserService,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn((key: string) => {
              if (key === 'dangerousUsers') {
                return '[{"username":"chemIndustries","password":"1234","roles":["consignor"],"company":{"name":"Chemical Industries Germany Inc.","address":{"street":"Ressestraße","number":"50","postalCode":"45894","city":"Gelsenkirchen","country":"Deutschland"},"contact":{"name":"Fritz Hazard","phone":"0049 123 456 789 0","mail":"fritz.hazard@ChemIndInc.de","department":"OE 3 II"}}},{"username":"gefahrgutlogistik","password":"1234","roles":["carrier"],"company":{"name":"Gefahrgutlogistik","address":{"street":"Rathausplatz","number":"1","postalCode":"83209","city":"Prien am Chiemsee","country":"Deutschland"},"contact":{"name":"Max Mustermann","phone":"0049 123 456 789 1","mail":"max.mustermann@iml.fraunhofer.de","department":"OE 340"}}},{"username":"funkeAg","password":"1234","roles":["consignee"],"company":{"name":"Funke AG - Lacke und Farben","address":{"street":"Columbiadamm","number":"194","postalCode":"10965","city":"Berlin","country":"Deutschland"},"contact":{"name":"Dr. Martina Zünd","phone":"0049 741 852 063 9","mail":"Zuend@Funke.de","department":"Abt. 4A"}}}]';
              } else if (key === 'dangerousPrivateKey') {
                return 'LS0tLS1CRUdJTiBFQyBQUklWQVRFIEtFWS0tLS0tCk1JSGNBZ0VCQkVJQnFzZlVGTmx6aDZrUGU4ZkhJNFJpNkJOK0NvbDZJaDNERndHdW5rVWhUZjVRVitvdTNzTGsKUndsTkxHVzl6b0VYcHgxNGplNWdLNCtPOEErN09Ic0NDNUtnQndZRks0RUVBQ09oZ1lrRGdZWUFCQUh3d3FtTQpZRG9lYjRHc0VxR3huQjBzcHNZYldnQjl5SEtSR3V4eGNVeWpqblM1N3F4Tm9tNUxZKzdSQ3B4S3VHd1ExZWlPCkNJL3IvaXY1UUxwMkNXeDY2Z0dkUlA0NnhlRGExcnEyY2V4QzRtbWwwdjE4RFNHQU10OG9TL3dNbGdsMTZoYTUKVGxSVWd4U2pramNmeUNVWDVLcVRHcDlHLzZVM1cxZk1Na0t1K0Foa1FRPT0KLS0tLS1FTkQgRUMgUFJJVkFURSBLRVktLS0tLQ==';
              } else if (key === 'dangerousPublicKey') {
                return 'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlHYk1CQUdCeXFHU000OUFnRUdCU3VCQkFBakE0R0dBQVFCOE1LcGpHQTZIbStCckJLaHNad2RMS2JHRzFvQQpmY2h5a1Jyc2NYRk1vNDUwdWU2c1RhSnVTMlB1MFFxY1NyaHNFTlhvamdpUDYvNHIrVUM2ZGdsc2V1b0JuVVQrCk9zWGcydGE2dG5Ic1F1SnBwZEw5ZkEwaGdETGZLRXY4REpZSmRlb1d1VTVVVklNVW81STNIOGdsRitTcWt4cWYKUnYrbE4xdFh6REpDcnZnSVpFRT0KLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0t';
              }
              return null;
            }),
          },
        },
      ],
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
