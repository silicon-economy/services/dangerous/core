/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class Address {
  street: string;
  zip: string;
  place: string;
  country: string;

  constructor(street: string, zip: string, place: string, country: string) {
    this.street = street;
    this.zip = zip;
    this.place = place;
    this.country = country;
  }
}
