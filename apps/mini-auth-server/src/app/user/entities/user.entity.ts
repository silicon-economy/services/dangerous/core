/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { v4 as uuid } from 'uuid';
import { UserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/user.dto';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { CompanyDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/company.dto';
import { AddressDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/address.dto';
import { ContactPersonDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/contact-person.dto';

export class User {
  id: string;
  username: string;
  password: string;
  roles: UserRole[];
  company: CompanyDto;

  constructor(username: string, password: string, roles: UserRole[], company: CompanyDto, jsonUserString?: string) {
    this.id = uuid();
    if (jsonUserString) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      const { company, ...userData }: { company: CompanyDto } = JSON.parse(jsonUserString);
      Object.assign(this, userData);
      this.password = Buffer.from(this.password).toString('base64');
      this.company = new CompanyDto(
        company.name,
        new AddressDto(
          company.address.street,
          company.address.number,
          company.address.postalCode,
          company.address.city,
          company.address.country
        ),
        new ContactPersonDto(
          company.contact.name,
          company.contact.phone,
          company.contact.mail,
          company.contact.department
        )
      );
    } else {
      this.username = username;
      this.password = Buffer.from(password).toString('base64');
      this.roles = roles;
      this.company = company;
    }
  }

  /**
   * Validates the given username and password against the user's credentials.
   *
   * @param username - The username to validate.
   * @param password - The password to validate.
   * @returns A boolean indicating whether the username and password are valid.
   */
  public validate(username: string, password: string): boolean {
    return Buffer.from(password).toString('base64') === this.password && username === this.username;
  }

  /**
   * Converts a User object to a UserDto object.
   *
   * @param user - The User object to convert.
   * @returns A UserDto object representing the User.
   */
  static toDto(user: User): UserDto {
    return new UserDto(user.id, user.company);
  }
}
