/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { BadRequestException, Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { User } from './entities/user.entity';
import * as jwt from 'jsonwebtoken';
import { UsernameJwtPayload } from 'jsonwebtoken';
import { ConfigService } from '@nestjs/config';
import { CreateUserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/create-user.dto';
import { UserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/user.dto';
import { UpdateUserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/update-user.dto';
import { JwtDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto';

declare module 'jsonwebtoken' {
  export interface UsernameJwtPayload extends jwt.JwtPayload {
    username: string | (() => string);
  }
}

@Injectable()
export class UserService {
  private users: User[] = [];
  private privateKey = '';
  private publicKey = '';
  private logger = new Logger(UserService.name);

  constructor(private configService: ConfigService) {
    this.logger.debug('initializing user');
    const dangerousUsers: string = configService.get<string>('dangerousUsers');

    if (dangerousUsers) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      const jsonUserArray: [] = JSON.parse(dangerousUsers);
      jsonUserArray.forEach((jsonUser) => {
        this.users.push(new User(undefined, undefined, undefined, undefined, JSON.stringify(jsonUser)));
      });
      this.logger.debug('user generated');
    } else {
      throw Error('No user data provided! Make sure, env variable `dangerousUsers` is set.');
    }

    this.logger.log('read JWT secret key pair');
    this.privateKey = Buffer.from(configService.get<string>('dangerousPrivateKey'), 'base64').toString('utf8');
    this.publicKey = Buffer.from(configService.get<string>('dangerousPublicKey'), 'base64').toString('utf8');
    this.logger.log('secret key pair has been read successfully');
  }

  /**
   * Creates a new user.
   *
   * @param createUserDto - Create user dto
   * @returns User id
   */
  createUser(createUserDto: CreateUserDto): string {
    const user = new User(createUserDto.username, createUserDto.password, createUserDto.roles, createUserDto.company);
    if (user) this.logger.log('user created', user);
    this.users.push(user);
    return user.id;
  }

  /**
   * If "Token" is specified, the token is validated and the user data is returned.
   * If `authHeader` is provided, the `login` method is called to authenticate
   * the user and get the user details.
   * If neither `token` nor `authHeader` is provided,
   * it calls the `convertAllUserToDto` method to convert all the user objects
   * to DTOs and returns them.
   *
   * @param authHeader - Authentication header
   * @param token - Authentication token
   * @returns Returns a User object, a string, or an array of UserDto objects.
   */
  authenticateAndGetUserDetails(authHeader?: string, token?: string): string | User | UserDto[] {
    let retVal: User | string | UserDto[];
    if (token) retVal = this.validateTokenAndGetUser(token);
    else if (authHeader) retVal = this.login(authHeader);
    else retVal = this.convertAllUserToDto();
    return retVal;
  }

  /**
   * Returns user information for a given user id.
   *
   * @param id - User id of user to be requested
   * @returns User information
   */
  getUserDetails(id: string): UserDto {
    const user: User = this.users.find((user) => user.id === id);
    if (!user) throw new BadRequestException('User not found');
    return User.toDto(user);
  }

  /**
   * Updates an existing user and returns the updated user
   *
   * @param id - User id of user to be modified
   * @param updateUserDto - Update user dto with new changes
   * @returns Updated user
   */
  updateUserDetails(id: string, updateUserDto: UpdateUserDto): UserDto {
    return User.toDto(
      this.users.find((user) => {
        if (user.id === id) {
          return this.updateUser(updateUserDto, user);
        }
      })
    );
  }

  /**
   * Deletes a user given by its id.
   *
   * @param id - User id of user to be deleted
   */
  removeUser(id: string): void {
    this.users = this.users.filter((user) => user.id != id);
  }

  /**
   * Updates the properties of a User object based on the provided UpdateUserDto.
   *
   * @param updateUserDto - The UpdateUserDto containing the properties to update.
   * @param user - The User object to update.
   * @returns The updated User object.
   */
  private updateUser(updateUserDto: UpdateUserDto, user: User): User {
    Object.keys(updateUserDto).forEach((key: string) => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      if (user[key]) user[key] = updateUserDto[key];
    });
    return user;
  }

  /**
   * Converts all User objects to their corresponding DTO representations.
   *
   * @returns An array of UserDto objects representing all the User objects.
   */
  private convertAllUserToDto(): UserDto[] {
    const retVal: UserDto[] = [];
    this.users.forEach((user: User) => retVal.push(User.toDto(user)));
    return retVal;
  }

  /**
   * Validates a JWT and retrieves the corresponding User object.
   *
   * @param token - The JWT to validate.
   * @returns The User object associated with the JWT if valid, otherwise returns null.
   */
  private validateTokenAndGetUser(token: string): User {
    const jwtUser: UsernameJwtPayload | string = this.validateToken(token) as UsernameJwtPayload;
    return this.users.find((user: User) => user.username == jwtUser.username);
  }

  /**
   * Validates a JWT.
   *
   * @param token - The JWT to validate.
   * @returns The decoded payload of the JWT if valid, otherwise returns null.
   */
  private validateToken(token: string): string | jwt.JwtPayload {
    this.logger.log('validating JWT');
    this.logger.debug(token);
    const retVal: string | jwt.JwtPayload = jwt.verify(token, this.publicKey, { algorithms: ['ES512'] });
    this.logger.debug(retVal ? 'JWT valid' : 'JWT invalid');
    this.logger.verbose(retVal);
    return retVal;
  }

  /**
   * Performs the login process based on the provided Basic Authentication header.
   *
   * @param authHeader - The Basic Authentication header containing the encoded credentials.
   * @returns The generated JWT for the authenticated User.
   */
  private login(authHeader: string): string {
    const user = this.getUserFromBasicAuth(authHeader);
    return this.buildJWT(user);
  }

  /**
   * Builds a JWT for the provided User object.
   *
   * @param user - The User object for which to generate the JWT.
   * @returns The generated JWT.
   */
  private buildJWT(user: User): string {
    this.logger.log('Generating JWT');
    const retVal: string = jwt.sign(
      JSON.stringify(
        new JwtDto(user.username, user.roles, new UserDto(user.id, user.company), Math.floor(Date.now() / 1000) + 3600)
      ),
      this.privateKey,
      { algorithm: 'ES512' }
    );
    this.logger.debug('JWT:', retVal);
    return retVal;
  }

  /**
   * Retrieves a User object based on the provided Basic Authentication header.
   *
   * @param authHeader - The Basic Authentication header containing the encoded credentials.
   * @returns The User object matching the provided credentials.
   * @throws UnauthorizedException if the User is not found.
   */
  private getUserFromBasicAuth(authHeader: string): User {
    this.logger.debug('interpreting auth header', authHeader);
    authHeader = authHeader.split('Basic ')[1];
    authHeader = Buffer.from(authHeader, 'base64').toString();
    const auth: string[] = authHeader.split(':');
    this.logger.debug(auth);
    const user: User = this.users.find((user: User) => user.validate(auth[0], auth[1]));
    if (!user) throw new UnauthorizedException('User not found');
    this.logger.verbose(`found ${user.id} for ${authHeader}`);
    return user;
  }
}
