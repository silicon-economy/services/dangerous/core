/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CreateUserDto } from "@core/api-interfaces/lib/dtos/mini-auth-server/create-user.dto";
import { UserRole } from "@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum";
import { CompanyDto } from "@core/api-interfaces/lib/dtos/mini-auth-server/company.dto";
import { AddressDto } from "@core/api-interfaces/lib/dtos/mini-auth-server/address.dto";
import { ContactPersonDto } from "@core/api-interfaces/lib/dtos/mini-auth-server/contact-person.dto";

export const createUserDtoDummy: CreateUserDto  = new CreateUserDto(
  'chemIndustries',
  '1234',
  [UserRole.consignor],
  new CompanyDto(
    'Chemical Industries Germany Inc.',
    new AddressDto('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
    new ContactPersonDto('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
  )
);

export const createUserDtoCarrierDummy: CreateUserDto = new CreateUserDto(
  'gefahrgutlogistik',
  '1234',
  [UserRole.carrier],
  new CompanyDto(
    'Gefahrgutlogistik',
    new AddressDto('Rathausplatz', '1', '83209', 'Prien am Chiemsee', 'Deutschland'),
    new ContactPersonDto('Max Mustermann', '0049 123 456 789 1', 'max.mustermann@iml.fraunhofer.de', 'OE 340')
  )
);

export const createUserDtoConsigneeDummy: CreateUserDto =  new CreateUserDto(
  'funkeAg',
  '1234',
  [UserRole.consignee],
  new CompanyDto(
    'Funke AG - Lacke und Farben',
    new AddressDto('Columbiadamm', '194', '10965', 'Berlin', 'Deutschland'),
    new ContactPersonDto('Dr. Martina Zünd', '0049 741 852 063 9', 'Zuend@Funke.de', 'Abt. 4A')
  )
);

export const createUserDtoConsignorAndConsigneeDummy: CreateUserDto = new CreateUserDto(
  'funkeAg',
  '1234',
  [UserRole.consignor, UserRole.consignee],
  new CompanyDto(
    'Funke AG - Lacke und Farben',
    new AddressDto('Columbiadamm', '194', '10965', 'Berlin', 'Deutschland'),
    new ContactPersonDto('Dr. Martina Zünd', '0049 741 852 063 9', 'Zuend@Funke.de', 'Abt. 4A')
  )
);
