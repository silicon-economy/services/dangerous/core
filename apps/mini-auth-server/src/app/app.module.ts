/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TerminusModule } from '@nestjs/terminus';
import { ConfigModule } from '@nestjs/config';
import config from '../config/config';
import { UserModule } from '@miniAuthServer/user/user.module';

@Module({
  imports: [
    TerminusModule,
    UserModule,
    ConfigModule.forRoot({
      load: [config],
    }),
  ],
  controllers: [AppController],
})
export class AppModule {}
