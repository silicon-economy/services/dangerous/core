/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller, Get } from '@nestjs/common';
import {
  HealthCheck,
  HealthCheckResult,
  HealthCheckService,
  HealthIndicatorResult,
  MemoryHealthIndicator,
} from '@nestjs/terminus';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('AppController')
@Controller('app')
export class AppController {
  constructor(
    private health: HealthCheckService,
    private memory: MemoryHealthIndicator
  ) {}

  /**
   * Performs a health check and returns the result.
   *
   * @returns A Promise that resolves to a HealthCheckResult object.
   */
  @Get('healthCheck')
  @HealthCheck()
  checkHealth(): Promise<HealthCheckResult> {
    return this.health.check([
      (): Promise<HealthIndicatorResult> => this.memory.checkHeap('memory_heap', 150 * 1024 * 1024),
      (): Promise<HealthIndicatorResult> => this.memory.checkRSS('memory_rss', 150 * 1024 * 1024),
    ]);
  }
}
