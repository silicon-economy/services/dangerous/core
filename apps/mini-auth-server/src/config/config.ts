/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Main Config
 */
export enum Config {
  // Application configuration
  APPLICATION_TITLE = 'dangerous.mini.auth.service',
  APPLICATION_DESCRIPTION = 'The Mini Auth API description',
  APPLICATION_VERSION = '0.1',
  SWAGGER_PATH = 'api',
}

export default () => ({
  secretKey: process.env.secretKey || 'HelloWorld!',
  httpPort: process.env.HTTP_PORT || 3001,
  dangerousPublicKey: process.env.DANGEROUS_PUBLIC_KEY,
  dangerousPrivateKey: process.env.DANGEROUS_PRIVATE_KEY,

  // dangerous users
  dangerousUsers: process.env.DANGEROUS_USERS,
});
