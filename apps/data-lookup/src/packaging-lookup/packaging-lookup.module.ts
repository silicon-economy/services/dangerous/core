/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { PackagingLookupController } from '@packagingLookupModule/controller/packaging-lookup.controller';
import { PackagingLookupService } from '@packagingLookupModule/service/packaging-lookup.service';

@Module({
  controllers: [PackagingLookupController],
  providers: [PackagingLookupService],
})
export class PackagingLookupModule {}
