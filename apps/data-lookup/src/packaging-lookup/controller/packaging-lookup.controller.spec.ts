/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { PackagingLookupController } from '@packagingLookupModule/controller/packaging-lookup.controller';
import { PackagingLookupService } from '@packagingLookupModule/service/packaging-lookup.service';

describe('DangerousGoodLookupController', () => {
  let controller: PackagingLookupController;
  let service: PackagingLookupService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PackagingLookupController],
      providers: [PackagingLookupService],
    }).compile();

    controller = module.get<PackagingLookupController>(PackagingLookupController);
    service = module.get<PackagingLookupService>(PackagingLookupService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
    expect(controller.lookupPackaging).toBeDefined();
    expect(controller.lookupPackagingByCode).toBeDefined();
  });
});
