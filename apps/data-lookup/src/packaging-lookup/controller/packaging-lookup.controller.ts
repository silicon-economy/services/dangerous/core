/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { PackagingLookupRequestDto } from '@core/api-interfaces/lib/dtos/packaging-lookup/packaging-lookup.request.dto';
import { PackagingLookupResponseDto } from '@core/api-interfaces/lib/dtos/packaging-lookup/packaging-lookup.response.dto';
import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { PackagingLookupService } from '@packagingLookupModule/service/packaging-lookup.service';
import { MessagePatternsPackagingLookup } from '@core/api-interfaces/lib/amqp/message-patterns.enum';

@Controller()
export class PackagingLookupController {
  constructor(private readonly packagingLookupService: PackagingLookupService) {}

  /**
   * Looks up packaging information for a given kind or code of packaging.
   *
   * @param packagingLookupRequestDto - Packaging lookup request dto
   * @returns Promise\<PackagingLookupResponseDto[]\>
   */
  @MessagePattern(MessagePatternsPackagingLookup.GET)
  async lookupPackaging(packagingLookupRequestDto: PackagingLookupRequestDto): Promise<PackagingLookupResponseDto[]> {
    return this.packagingLookupService.lookupPackaging(packagingLookupRequestDto);
  }

  /**
   * Looks up packaging information for a given code of packaging.
   *
   * @param code - Packaging code
   * @returns Promise<PackagingLookupResponseDto>
   */
  @MessagePattern(MessagePatternsPackagingLookup.GET_BY_CODE)
  async lookupPackagingByCode(code: string): Promise<PackagingLookupResponseDto> {
    return this.packagingLookupService.lookupPackagingByCode(code);
  }
}
