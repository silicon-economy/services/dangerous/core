/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Config } from '@config';
import { Injectable, Logger } from '@nestjs/common';
import * as JSONStream from 'jsonstream';
import fs from 'fs';
import { PackagingLookupRequestDto, PackagingLookupResponseDto } from '@core/api-interfaces';

@Injectable()
export class PackagingLookupService {
  private readonly logger = new Logger(PackagingLookupService.name);

  /**
   * Filter packaging types by given packagingCode.
   *
   * @param packagingCode - Packaging code
   * @returns Promise\<PackagingLookupResponseDto[]\>
   */
  async lookupPackagingByCode(packagingCode: string): Promise<PackagingLookupResponseDto> {
    this.logger.log('Read out packaging information from the packaging JSON mock-db for a given packaging code');
    this.logger.verbose('packagingCode: ' + packagingCode);
    const results: PackagingLookupResponseDto[] = await this.parseJson();
    return results.find((result) => result.code.toLowerCase() === packagingCode.toLowerCase());
  }

  /**
   * Reads the data from a database based on given requestDto.
   *
   * @param packagingLookupRequestDto - Packaging lookup request dto
   * @returns Promise\<PackagingLookupResponseDto[]\>
   */
  async lookupPackaging(packagingLookupRequestDto: PackagingLookupRequestDto): Promise<PackagingLookupResponseDto[]> {
    this.logger.log('Read out packaging information from the packaging JSON mock-db based on given requestDto');
    this.logger.verbose('packagingLookupRequestDto: ' + JSON.stringify(packagingLookupRequestDto));
    if (packagingLookupRequestDto.query == undefined) {
      packagingLookupRequestDto.query = ' ';
    }
    return this.filterPackagings(packagingLookupRequestDto);
  }

  /**
   * Filter packagings by given filter.
   *
   * @param filter - The filter to be applied.
   * @returns Promise\<PackagingLookupResponseDto[]\>
   */
  public async filterPackagings(filter: PackagingLookupRequestDto): Promise<PackagingLookupResponseDto[]> {
    const results: PackagingLookupResponseDto[] = await this.parseJson();
    this.logger.debug('Apply filter to the packagingLookupResponseDto[]');
    return results.filter((p) => this.applyFilter(filter, p));
  }

  /**
   * Checks if the given filter information is valid and calls filterQuery to filter the input string.
   *
   * @param filter - Packaging lookup request dto
   * @param packaging - Packaging lookup response dto
   * @returns Boolean
   */
  private applyFilter(filter: PackagingLookupRequestDto, packaging: PackagingLookupResponseDto): boolean {
    const kindFilter = !filter.kind || packaging.kind === filter.kind;
    const typesOfMaterialFilter = !filter.typesOfMaterial || packaging.typesOfMaterial === filter.typesOfMaterial;
    const materialFilter = !filter.material || packaging.material === filter.material;
    const categoryFilter =
      !filter.category || packaging.category.toLowerCase().search(filter.category.toLowerCase()) > -1;
    return (
      this.filterQuery(filter.query, packaging) &&
      kindFilter &&
      typesOfMaterialFilter &&
      materialFilter &&
      categoryFilter
    );
  }

  /**
   * Returns true if every word of the given query string is contained in the given packaging
   *
   * @param query - Query string
   * @param packaging - Packaging lookup response dto
   * @returns Boolean
   */
  private filterQuery(query: string, packaging: PackagingLookupResponseDto): boolean {
    const combinedString = [
      packaging.kind,
      packaging.typesOfMaterial,
      packaging.material,
      packaging.category,
      packaging.code,
    ]
      .join(' ')
      .toLowerCase();
    const wordFilters: boolean[] = query
      .toLowerCase()
      .split(' ')
      .map((word: string) => combinedString.search(word) > -1);
    return wordFilters.reduce((a: boolean, b: boolean) => a && b, true);
  }

  /**
   * Returns all entries of the dummy database.
   *
   * @returns Promise\<PackagingLookupResponseDto[]\>
   */
  private async parseJson(): Promise<PackagingLookupResponseDto[]> {
    const results: PackagingLookupResponseDto[] = [];
    return new Promise(function (resolve) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
      fs.createReadStream(__dirname + '/../..' + Config.DUMMY_PACKAGING_DATABASE_PATH)
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
        .pipe(JSONStream.parse('*'))
        .on('data', (data: PackagingLookupResponseDto) => results.push(data))
        .on('end', () => {
          resolve(results);
        });
    });
  }
}
