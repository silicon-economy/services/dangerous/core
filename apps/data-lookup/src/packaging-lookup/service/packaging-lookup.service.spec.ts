/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';

import { PackagingLookupController } from '@packagingLookupModule/controller/packaging-lookup.controller';
import { PackagingLookupService } from '@packagingLookupModule/service/packaging-lookup.service';
import { PackagingLookupResponseDto } from '@core/api-interfaces';
import { PackagingKind } from '@core/api-interfaces/lib/dtos/packaging-lookup/enums/packaging-kind.enum';
import { TypesOfMaterial } from '@core/api-interfaces/lib/dtos/packaging-lookup/enums/types-of-material.enum';
import { Material } from '@core/api-interfaces/lib/dtos/packaging-lookup/enums/material.enum';

describe('PackagingLookupService', () => {
  let controller: PackagingLookupController;
  let service: PackagingLookupService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PackagingLookupController],
      providers: [PackagingLookupService],
    }).compile();

    controller = module.get<PackagingLookupController>(PackagingLookupController);
    service = module.get<PackagingLookupService>(PackagingLookupService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
    expect(service.lookupPackaging).toBeDefined();
  });

  describe('lookupPackagingByCode', () => {
    it('should output packaging info for given (partial) code', async () => {
      const packagingLookupResponseDTORef = {
        kind: PackagingKind[PackagingKind.Kombinationsverpackungen],
        typesOfMaterial: TypesOfMaterial[TypesOfMaterial.H],
        material: Material[Material.Kunststoffgefäß],
        category: 'in einem Fass aus Stahl',
        code: '6HA1',
      };
      expect(await controller.lookupPackagingByCode('6hA1')).toEqual(packagingLookupResponseDTORef);
    });
  });
  describe('lookupPackaging', () => {
    it('should output packaging info for given (partial) code', async () => {
      const packagingLookupResponseDTORef = {
        kind: PackagingKind[PackagingKind.Kombinationsverpackungen],
        typesOfMaterial: TypesOfMaterial[TypesOfMaterial.H],
        material: Material[Material.Kunststoffgefäß],
        category: 'in einem Fass aus Stahl',
        code: '6HA1',
      };
      expect(await controller.lookupPackaging({ query: '6hA1' })).toContainEqual(packagingLookupResponseDTORef);
    });

    it('should output packaging info for given (partial) kind', async () => {
      const packagingLookupResponseDTORef = {
        kind: PackagingKind[PackagingKind.Kombinationsverpackungen],
        typesOfMaterial: TypesOfMaterial[TypesOfMaterial.H],
        material: Material[Material.Kunststoffgefäß],
        category: 'in einem Fass aus Stahl',
        code: '6HA1',
      };
      expect(await controller.lookupPackaging({ query: 'KoMbInAt' })).toContainEqual(packagingLookupResponseDTORef);
    });

    it('should output all packaging info', async () => {
      const packagingLookupResponseDTOLength = 97;
      const response = await controller.lookupPackaging({ query: '' });
      expect(response.length).toEqual(packagingLookupResponseDTOLength);
    });

    it('should filter packagings without any filter results', async () => {
      const packagingLookupResult: PackagingLookupResponseDto[] = await service.filterPackagings({
        query: 'dieser string existiert nicht',
      });
      expect(packagingLookupResult.length).toEqual(0);
    });

    it('should filter packagings with filter results', async () => {
      const packagingLookupResult: PackagingLookupResponseDto[] = await service.filterPackagings({
        query: '',
        category: 'Deckel',
        kind: PackagingKind.Fässer,
        typesOfMaterial: TypesOfMaterial.A,
        material: Material.Stahl,
      });
      expect(packagingLookupResult.length).toBeGreaterThan(0);
    });
  });
});
