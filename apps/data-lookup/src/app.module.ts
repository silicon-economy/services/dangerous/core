/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import config from '@config';
import { DangerousGoodLookupModule } from '@dangerousGoodLookupModule/dangerous-good-lookup.module';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PackagingLookupModule } from '@packagingLookupModule/packaging-lookup.module';

@Module({
  imports: [DangerousGoodLookupModule, PackagingLookupModule, ConfigModule.forRoot({ isGlobal: true, load: [config] })],
})
export class AppModule {}
