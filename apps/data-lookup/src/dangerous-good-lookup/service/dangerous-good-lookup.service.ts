/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Config } from '@config';
import { Injectable, Logger } from '@nestjs/common';
import * as fs from 'fs';
import { DangerousGoodLookupResponseDto } from '@core/api-interfaces';
import * as JSONStream from 'jsonstream';

@Injectable()
export class DangerousGoodLookupService {
  private readonly logger = new Logger(DangerousGoodLookupService.name);

  /**
   * Reads the data from a database based on given inputString.
   *
   * @param inputString - Input string
   * @returns Promise\<DangerousGoodLookupResponseDto[]\>
   */
  async lookupDangerousGood(inputString: string): Promise<DangerousGoodLookupResponseDto[]> {
    this.logger.log('Read out dangerous good information from the dangerous good JSON mock-db for a given inputString');
    this.logger.verbose('inputString: ' + inputString);
    return this.parseJson(inputString, this.filterDangerousGoodLookupResponseDtos);
  }

  /**
   * Returns all entries in the dummy database fitting to the given inputString.
   *
   * @param inputString - Input string
   * @param filterFunction - Filter rule
   * @returns Promise\<DangerousGoodLookupResponseDto[]\>
   */
  private async parseJson(
    inputString: string,
    filterFunction: (
      dangerousGoodLookupResponseDtos: DangerousGoodLookupResponseDto[],
      inputString: string
    ) => DangerousGoodLookupResponseDto[]
  ): Promise<DangerousGoodLookupResponseDto[]> {
    let results: DangerousGoodLookupResponseDto[] = [];
    return new Promise((resolve) => {
      this.logger.debug('Read dummy database file');
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
      fs.createReadStream(__dirname + '/../..' + Config.DUMMY_DATABASE_PATH)
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
        .pipe(JSONStream.parse('*'))
        .on('data', (data: DangerousGoodLookupResponseDto) => results.push(data))
        .on('end', () => {
          this.logger.debug('Filter results');
          results.forEach((dangerousGoodLookupResponseDto: DangerousGoodLookupResponseDto) => {
            dangerousGoodLookupResponseDto.limitedQuantity = Number(dangerousGoodLookupResponseDto.limitedQuantity);
            dangerousGoodLookupResponseDto.polluting = String(dangerousGoodLookupResponseDto.polluting) == 'true';
          });
          results = filterFunction(results, inputString);
          resolve(results);
        });
    });
  }

  /**
   * Filters dangerous good lookup response dtos for a given input string.
   *
   * @param dangerousGoodLookupResponseDtos - All dangerous good lookup response dtos read from the dummy database
   * @param inputString - Input string
   * @returns dangerousGoodLookupResponseDtos that match the input string
   */
  private filterDangerousGoodLookupResponseDtos(
    dangerousGoodLookupResponseDtos: DangerousGoodLookupResponseDto[],
    inputString: string
  ): DangerousGoodLookupResponseDto[] {
    return dangerousGoodLookupResponseDtos.filter(
      (result: DangerousGoodLookupResponseDto) =>
        // Check if given unNumber is contained in dummy database entry
        result.unNumber.startsWith(inputString) ||
        // Check if given inputString is (partially) contained in dummy database entry
        result.description.toLowerCase().includes(inputString.toLowerCase())
    );
  }
}
