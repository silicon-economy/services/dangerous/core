/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DangerousGoodLookupController } from '@dangerousGoodLookupModule/controller/dangerous-good-lookup.controller';
import { DangerousGoodLookupService } from '@dangerousGoodLookupModule/service/dangerous-good-lookup.service';
import { Test, TestingModule } from '@nestjs/testing';
import { DangerousGoodLookupResponseDto } from '@core/api-interfaces';

describe('DangerousGoodLookupService', () => {
  let controller: DangerousGoodLookupController;
  let service: DangerousGoodLookupService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DangerousGoodLookupController],
      providers: [DangerousGoodLookupService],
    }).compile();

    controller = module.get<DangerousGoodLookupController>(DangerousGoodLookupController);
    service = module.get<DangerousGoodLookupService>(DangerousGoodLookupService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  describe('lookupData', () => {
    it('should return data for given unNumber', async () => {
      const dangerousGoodLookupResponseDtoRef: DangerousGoodLookupResponseDto[] = [
        {
          unNumber: '1090',
          casNumber: '67-64-1',
          description: 'ACETON',
          label1: '3',
          label2: '',
          label3: '',
          packingGroup: 'II',
          tunnelRestrictionCode: '(D/E)',
          transportCategory: '2',
          polluting: false,
          limitedQuantity: 1,
        },
      ];

      expect(await controller.lookupDangerousGood('1090')).toMatchObject(dangerousGoodLookupResponseDtoRef);
    });

    it('should return data for given description', async () => {
      const dangerousGoodLookupResponseDtoRef: DangerousGoodLookupResponseDto = {
        unNumber: '1090',
        casNumber: '67-64-1',
        description: 'ACETON',
        label1: '3',
        label2: '',
        label3: '',
        packingGroup: 'II',
        tunnelRestrictionCode: '(D/E)',
        transportCategory: '2',
        polluting: false,
        limitedQuantity: 1,
      };
      expect(await controller.lookupDangerousGood('AcEtOn')).toContainEqual(dangerousGoodLookupResponseDtoRef);
    });
  });
});
