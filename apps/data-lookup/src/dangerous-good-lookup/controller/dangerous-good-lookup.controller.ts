/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DangerousGoodLookupService } from '@dangerousGoodLookupModule/service/dangerous-good-lookup.service';
import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { DangerousGoodLookupResponseDto } from '@core/api-interfaces';
import { MessagePatternsDangerousGoodLookup } from '@core/api-interfaces/lib/amqp/message-patterns.enum';

@Controller()
export class DangerousGoodLookupController {
  constructor(private readonly dangerousGoodLookupService: DangerousGoodLookupService) {}

  /**
   * Looks up data of a good based on given inputString.
   *
   * @param inputString - Input string
   * @returns Dangerous good
   */
  @MessagePattern(MessagePatternsDangerousGoodLookup.GET)
  async lookupDangerousGood(inputString: string): Promise<DangerousGoodLookupResponseDto[]> {
    return this.dangerousGoodLookupService.lookupDangerousGood(inputString);
  }
}
