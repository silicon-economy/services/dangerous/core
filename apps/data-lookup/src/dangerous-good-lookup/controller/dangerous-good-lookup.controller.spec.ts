/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DangerousGoodLookupController } from '@dangerousGoodLookupModule/controller/dangerous-good-lookup.controller';
import { DangerousGoodLookupService } from '@dangerousGoodLookupModule/service/dangerous-good-lookup.service';
import { Test, TestingModule } from '@nestjs/testing';

describe('DangerousGoodLookupController', () => {
  let controller: DangerousGoodLookupController;
  let service: DangerousGoodLookupService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DangerousGoodLookupController],
      providers: [DangerousGoodLookupService],
    }).compile();

    controller = module.get<DangerousGoodLookupController>(DangerousGoodLookupController);
    service = module.get<DangerousGoodLookupService>(DangerousGoodLookupService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
    expect(controller.lookupDangerousGood).toBeDefined();
  });
});
