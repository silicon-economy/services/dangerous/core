/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DangerousGoodLookupController } from '@dangerousGoodLookupModule/controller/dangerous-good-lookup.controller';
import { DangerousGoodLookupService } from '@dangerousGoodLookupModule/service/dangerous-good-lookup.service';
import { Module } from '@nestjs/common';

@Module({
  controllers: [DangerousGoodLookupController],
  providers: [DangerousGoodLookupService],
})
export class DangerousGoodLookupModule {}
