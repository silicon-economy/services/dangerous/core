/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Main config
 */
export enum Config {
  // Application configuration
  DUMMY_DATABASE_PATH = '/assets/Dummy-Stoffdatenbank_V5.json',
  DUMMY_PACKAGING_DATABASE_PATH = '/assets/Dummy-Verpackungstypdatenbank_V7.json',
}

export default () => ({
  queuePrefix: process.env.DEPLOYMENT_ENVIRONMENT || '',
  amqpUrl: process.env.AMQP_URL || 'amqp://guest:guest@0.0.0.0:5672',
});
