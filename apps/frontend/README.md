# Dangerous

Angular 16 Frontend for the dangerous project for the transportation of dangerous goods.

Can be run via npm or Docker.

## NPM

### Prerequisite

For DragonPuck integration, please refer to its [repository](https://git.openlogisticsfoundation.org/silicon-economy/base/iotbroker/device-integration/dragonpuck).

### Install

Run `npm i` in the project's directory.

### Run

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

### Build

Run `ng build --prod` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests and code coverage

Run `ng test --code-coverage` to run the test suite and view the code coverage.

## Docker

### Build the image

Run `docker build -t dangerous .` to build a Docker image.

### Start the container

#### Interactive

Run `docker run --name dangerous-container --rm -it -p 8080:8080 dangerous` to start the Docker container. Navigate to `http://localhost:8080/`.

#### As a background service

Run `docker run --name dangerous-container --rm -d -p 8080:8080 dangerous` to start the Docker container. Navigate to `http://localhost:8080/`.

### Stop the container

Run `docker stop dangerous-container` to stop the execution of the container.

### Remove the container

Run `docker rm dangerous-container` to remove container. Required if you did not include the `--rm` flag and want to test a new build of the image.
