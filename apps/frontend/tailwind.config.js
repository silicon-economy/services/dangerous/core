/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['apps/frontend/src/**/*.{html,ts}'],
  theme: {
    extend: {},
    screens: {
      // Phone portrait
      phone: '320px',
      // Tablet portrait
      tablet: '768px',
      // Laptops and Desktops (Low-Res), tablet landscape
      laptop: '1024px',
      // Laptops and Desktops (High-Res)
      desktop: '1440px',
    },
    colors: {
      backgroundLight: '#127C621A',
      backgroundDark: '#303030',
      textLight: '#545454',
      textDark: '#FFFFFFF7',
      primary: '#6DC1AC',
      accent: '#CB007E',
    },
  },
  plugins: [],
};
