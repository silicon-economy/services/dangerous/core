/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

(function (window) {
  window.env = window.env || {};
  // Environment variables

  // Transport document detail page map source
  window['env']['accessToken'] = '${ACCESS_TOKEN}';
  window['env']['apiEndpointNominatim'] = '${API_ENDPOINT_NOMINATIM}';
  window['env']['apiEndpointTiles'] = '${API_ENDPOINT_TILES}';

  // Dangerous Goods Gateway
  window['env']['apiEndpoint'] = '${API_ENDPOINT}';

  // Transport Document Gateway
  window['env']['apiEndpointDocuments'] = '${API_ENDPOINT_DOCUMENTS}';

  // Websocket url (Transport Document Gateway)
  window['env']['apiAlertWebsocket'] = '${API_ALERT_WEBSOCKET}';

  // Blockchain
  window['env']['creatorAddress'] = '${CREATOR_ADDRESS}';
  window['env']['mnemonic'] = '${DANGEROUS_USER_MNEMONIC}';
  window['env']['blockchainTypeURL'] = '${BLOCKCHAIN_TYPE_URL}';
  window['env']['blockchainGasLimit'] = '${BLOCKCHAIN_GAS_LIMIT}';
  window['env']['apiEndpointBlockchainRPC'] = '${API_ENDPOINT_BLOCKCHAIN_RPC}';
  window['env']['apiEndpointBroker'] = '${API_ENDPOINT_BROKER}';
})(this);
