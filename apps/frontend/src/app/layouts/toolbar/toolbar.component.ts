/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { Language } from '@base/src/app/models/language.interface';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { ThemeService } from '@shared/services/theme-service/theme.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
})
export class ToolbarComponent implements OnInit {
  readonly languages: Language[] = [
    { id: 'de', label: 'Deutsch', img: '../../../assets/i18n/de.svg' },
    { id: 'en', label: 'English', img: '../../../assets/i18n/en.svg' },
  ];

  @Output() public toggleDrawer: EventEmitter<void> = new EventEmitter<void>();
  public currentLanguage: BehaviorSubject<Language> = new BehaviorSubject<Language>(this.languages[0]);
  public isLoggedIn: boolean;

  constructor(
    private translateService: TranslateService,
    private readonly userService: UserLoginService,
    private readonly themeService: ThemeService
  ) {}

  ngOnInit(): void {
    this.currentLanguage.next(
      this.languages.find((language: Language) => {
        if (localStorage.getItem('language')) {
          return language.id == localStorage.getItem('language');
        } else {
          return language.id == 'de';
        }
      })
    );

    this.userService.isLoggedIn$.subscribe((isLoggedIn) => {
      this.isLoggedIn = isLoggedIn;
    });
  }

  get themeToggleState(): Observable<boolean> {
    return this.themeService.getThemeValue();
  }

  /**
   * Handles the change event of the theme toggle switch.
   *
   * @param event - The change event object.
   */
  onThemeToggleChange(event: MatSlideToggleChange): void {
    this.themeService.setThemeValue(event.checked);
  }

  /**
   * Handles the click event of the drawer toggle button.
   */
  onToggleDrawer(): void {
    if (this.userService.isLoggedIn$.getValue()) {
      this.toggleDrawer.emit();
    }
  }

  /**
   * Handles the click event of a language menu item.
   *
   * @param menuItem - The selected language menu item.
   */
  clickLanguageMenuItem(menuItem: Language): void {
    this.translateService.use(menuItem.id);
    localStorage.setItem('language', menuItem.id);
    this.currentLanguage.next(menuItem);
  }

  /**
   * Logs out the user.
   */
  public logout(): void {
    this.userService.logout();
  }
}
