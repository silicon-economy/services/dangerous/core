/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterTestingModule } from '@angular/router/testing';
import { ToolbarComponent } from './toolbar.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ThemeService } from '@shared/services/theme-service/theme.service';
import { MatSlideToggleChange, MatSlideToggleModule } from '@angular/material/slide-toggle';
import { Language } from '@base/src/app/models/language.interface';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let userService: UserLoginService;
  let fixture: ComponentFixture<ToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ToolbarComponent],
      imports: [
        HttpClientTestingModule,
        MatToolbarModule,
        MatMenuModule,
        MatIconModule,
        MatSlideToggleModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
      ],
      providers: [UserLoginService, UserAuthenticationService, ThemeService],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;
    userService = TestBed.inject(UserLoginService);
    fixture.detectChanges();
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should read English language from local storage', () => {
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('en');
    component.ngOnInit();
    expect(component.currentLanguage.getValue()).toEqual({
      id: 'en',
      label: 'English',
      img: '../../../assets/i18n/en.svg',
    });
  });

  it('should call setThemeValue method of theme service', () => {
    const themeServiceSpy = jest.spyOn(ThemeService.prototype, 'setThemeValue').mockImplementation();
    component.onThemeToggleChange(new MatSlideToggleChange(undefined, true));
    expect(themeServiceSpy).toHaveBeenCalled();
  });

  it('should emit toggle drawer event', () => {
    jest.spyOn(userService.isLoggedIn$, 'getValue').mockReturnValue(true);
    const toggleDrawerSpy = jest.spyOn(component.toggleDrawer, 'emit');
    component.onToggleDrawer();
    expect(toggleDrawerSpy).toHaveBeenCalled();
  });

  it('should handles click event of a language menu item', () => {
    const translateServiceSpy = jest.spyOn(TranslateService.prototype, 'use');
    const localStorageSpy = jest.spyOn(Storage.prototype, 'setItem');
    const currentLanguageSpy = jest.spyOn(component.currentLanguage, 'next');
    const mockLanguage: Language = { id: 'en', label: 'English', img: '../../../assets/i18n/en.svg' };

    component.clickLanguageMenuItem(mockLanguage);

    expect(translateServiceSpy).toHaveBeenCalledWith(mockLanguage.id);
    expect(localStorageSpy).toHaveBeenCalledWith('language', mockLanguage.id);
    expect(currentLanguageSpy).toHaveBeenCalledWith(mockLanguage);
  });

  it('should logout the current user', () => {
    const userServiceSpy = jest.spyOn(userService, 'logout');
    component.logout();
    expect(userServiceSpy).toHaveBeenCalledWith();
  });
});
