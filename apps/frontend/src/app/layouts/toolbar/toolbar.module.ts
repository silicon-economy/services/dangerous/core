/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { ToolbarComponent } from '@base/src/app/layouts/toolbar/toolbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FlexModule } from '@angular/flex-layout';
import { AsyncPipe, NgForOf, NgIf } from '@angular/common';
import { MatMenuModule } from '@angular/material/menu';
import { EventService } from '@shared/event.service';
import { TranslateModule } from '@ngx-translate/core';
import { MatListModule } from '@angular/material/list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ReactiveFormsModule } from '@angular/forms';
import { ThemeService } from '@shared/services/theme-service/theme.service';

@NgModule({
  declarations: [ToolbarComponent],
  exports: [ToolbarComponent],
  imports: [
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    RouterLink,
    MatRippleModule,
    MatFormFieldModule,
    MatSelectModule,
    FlexModule,
    NgForOf,
    MatMenuModule,
    TranslateModule,
    MatListModule,
    RouterLinkActive,
    NgIf,
    MatSlideToggleModule,
    ReactiveFormsModule,
    AsyncPipe,
  ],
  providers: [EventService, ThemeService],
})
export class ToolbarModule {}
