/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-dangerous-good-registration.dto';

@Component({
  selector: 'app-sensing-puck-data-view [document]',
  templateUrl: './sensing-puck-data-view.component.html',
  styleUrls: ['./sensing-puck-data-view.component.scss'],
})
export class SensingPuckDataViewComponent {
  @Input() document!: SaveGoodsDataDto | SaveDangerousGoodRegistrationDto;

  get relevantOrderPositions(): OrderPositionWithId[] {
    const allOrderPositions = this.document?.freight?.orders
      .map((order) => order.orderPositions)
      .reduce((a, b) => a.concat(b), []);
    return allOrderPositions?.filter((orderPosition) => orderPosition.deviceId != '0');
  }
}
