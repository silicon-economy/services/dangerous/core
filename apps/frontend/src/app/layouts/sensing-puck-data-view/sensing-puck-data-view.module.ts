/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SensingPuckDataViewComponent
} from '@base/src/app/layouts/sensing-puck-data-view/sensing-puck-data-view.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [SensingPuckDataViewComponent],
  imports: [CommonModule, TranslateModule, MatIconModule, MatCardModule],
  exports: [SensingPuckDataViewComponent],
})
export class SensingPuckDataViewModule {}
