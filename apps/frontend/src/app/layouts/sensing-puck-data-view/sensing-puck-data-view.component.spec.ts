/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SensingPuckDataViewComponent } from './sensing-puck-data-view.component';

describe('SensorDataViewComponent', () => {
  let component: SensingPuckDataViewComponent;
  let fixture: ComponentFixture<SensingPuckDataViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SensingPuckDataViewComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SensingPuckDataViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
