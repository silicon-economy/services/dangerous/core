/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { Company } from '@core/api-interfaces/lib/dtos/frontend';
import { FreightIdsIncluded } from '@core/api-interfaces/lib/dtos/frontend/transport-document/freight-ids-included';

@Component({
  selector: 'app-consignor-and-freight-view [consignor] [freight]',
  templateUrl: './consignor-and-freight-view.component.html',
})
export class ConsignorAndFreightViewComponent {
  @Input() consignor!: Company;
  @Input() freight!: FreightIdsIncluded;
}
