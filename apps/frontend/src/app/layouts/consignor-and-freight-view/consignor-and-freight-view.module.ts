/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsignorAndFreightViewComponent } from './consignor-and-freight-view.component';
import { CompanyViewModule } from '@base/src/app/layouts/company-view/company-view.module';
import { FreightBaseDataViewModule } from '@base/src/app/layouts/freight-base-data-view/freight-base-data-view.module';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [ConsignorAndFreightViewComponent],
  imports: [CommonModule, CompanyViewModule, FreightBaseDataViewModule, MatIconModule, TranslateModule],
  exports: [ConsignorAndFreightViewComponent],
})
export class ConsignorAndFreightViewModule {}
