/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsignorAndFreightViewComponent } from './consignor-and-freight-view.component';
import { TranslateModule } from '@ngx-translate/core';
import {
  mockTransportDocumentCreated
} from "@core/api-interfaces/lib/mocks/transport-document/transport-document.mock";

describe('ConsignorAndFreightViewComponent', () => {
  let component: ConsignorAndFreightViewComponent;
  let fixture: ComponentFixture<ConsignorAndFreightViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ConsignorAndFreightViewComponent],
      imports: [TranslateModule.forRoot()],
    });
    fixture = TestBed.createComponent(ConsignorAndFreightViewComponent);
    component = fixture.componentInstance;
    component.consignor = mockTransportDocumentCreated.consignor;
    component.freight = mockTransportDocumentCreated.freight;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
