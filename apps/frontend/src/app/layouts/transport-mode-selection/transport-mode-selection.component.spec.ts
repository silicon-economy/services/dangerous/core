/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportModeSelectionComponent } from './transport-mode-selection.component';
import { TranslateModule } from '@ngx-translate/core';

describe('TransportModeSelectionComponent', () => {
  let component: TransportModeSelectionComponent;
  let fixture: ComponentFixture<TransportModeSelectionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TransportModeSelectionComponent],
      imports: [TranslateModule.forRoot()],
    });
    fixture = TestBed.createComponent(TransportModeSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
