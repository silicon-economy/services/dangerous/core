/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderViewComponent } from '@base/src/app/layouts/order-view/order-view.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { StatusModule } from '@base/src/app/layouts/status/status.module';
import { CompanyViewModule } from '@base/src/app/layouts/company-view/company-view.module';
import { DangerousGoodLabelModule } from '@base/src/app/layouts/dangerous-good-label/dangerous-good-label.module';
import { DescriptionDialogModule } from '@base/src/app/dialogs/description-dialog/description-dialog.module';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { OrderTableViewComponent } from './components/order-table-view/order-table-view.component';
import {
  ShowStatusHistoryButtonModule
} from '@base/src/app/layouts/show-status-history-button/show-status-history-button.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  declarations: [OrderViewComponent, OrderTableViewComponent],
  imports: [
    CommonModule,
    MatExpansionModule,
    MatIconModule,
    StatusModule,
    CompanyViewModule,
    DangerousGoodLabelModule,
    DescriptionDialogModule,
    MatButtonModule,
    MatBadgeModule,
    ShowStatusHistoryButtonModule,
    TranslateModule,
    MatTableModule,
  ],
  exports: [OrderViewComponent],
})
export class OrderViewModule {}
