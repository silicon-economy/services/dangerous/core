/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-order-view[order][orderIndex]',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.scss'],
})
export class OrderViewComponent implements OnInit {
  @Input() order!: OrderWithId;
  @Input() orderIndex = 0;
  @Input() isMatExpansionPanelExpanded? = false;
  public matPanelTitleIcon: string;
  public matPanelTitleInput: string;
  private userRoles: UserRole[];

  constructor(
    private readonly dialog: MatDialog,
    userLoginService: UserLoginService,
    private translateService: TranslateService
  ) {
    userLoginService.userRoles.subscribe((userRoles) => (this.userRoles = userRoles));
  }

  ngOnInit(): void {
    if (this.userRoles.some((userRole) => userRole == UserRole.consignee)) {
      this.isMatExpansionPanelExpanded = true;
    } else if (this.isMatExpansionPanelExpanded == undefined) {
      this.isMatExpansionPanelExpanded = false;
    }
    if (!this.isMatExpansionPanelExpanded) {
      this.matPanelTitleIcon = 'list_alt';
      this.matPanelTitleInput =
        (this.translateService.instant('transportDocumentDetailsPage.stage') as string) +
        ' ' +
        String(this.orderIndex + 1);
      this.translateService.onLangChange.subscribe((): void => {
        this.translateService.get('transportDocumentDetailsPage.stage').subscribe((translation: string) => {
          this.matPanelTitleInput = translation + ' ' + String(this.orderIndex + 1);
        });
      });
    } else {
      this.matPanelTitleIcon = 'person';
      this.matPanelTitleInput = this.translateService.instant('roles.consignee') as string;
      this.translateService.onLangChange.subscribe((): void => {
        this.translateService.get('roles.consignee').subscribe((translation: string) => {
          this.matPanelTitleInput = translation;
        });
      });
    }
  }
}
