/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import { OrderViewComponent } from "@base/src/app/layouts/order-view/order-view.component";
import { DescriptionDialogComponent } from "@base/src/app/dialogs/description-dialog/description-dialog.component";
import { UserLoginService } from "@core/services/user-service/user-login.service";
import { UserAuthenticationService } from "@core/services/user-service/user-authentication.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TranslateModule } from "@ngx-translate/core";
import { MatExpansionModule } from "@angular/material/expansion";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { CompanyViewModule } from "@base/src/app/layouts/company-view/company-view.module";
import {
  OrderTableViewComponent
} from "@base/src/app/layouts/order-view/components/order-table-view/order-table-view.component";
import { MatBadgeModule } from "@angular/material/badge";
import { DangerousGoodLabelModule } from "@base/src/app/layouts/dangerous-good-label/dangerous-good-label.module";
import { mockOrderWithId } from "@core/api-interfaces/lib/mocks/order/order.mock";
import { UserRole } from "@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum";
import { BehaviorSubject } from "rxjs";

describe('OrderViewComponent', () => {
  let component: OrderViewComponent;
  let fixture: ComponentFixture<OrderViewComponent>;

  beforeEach(async () => {
    registerLocaleData(localeDe, 'de-DE', localeDeExtra);
    const mockref: Partial<MatDialogRef<DescriptionDialogComponent>> = {};
    await TestBed.configureTestingModule({
      declarations: [OrderViewComponent, DescriptionDialogComponent, OrderTableViewComponent],
      imports: [
        MatDialogModule,
        MatIconModule,
        MatExpansionModule,
        MatBadgeModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        CompanyViewModule,
        DangerousGoodLabelModule,
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockref,
        },
        UserLoginService,
        UserAuthenticationService,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderViewComponent);
    component = fixture.componentInstance;
    component.order = mockOrderWithId;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize correctly when consignee is not present', (): void => {
    const userLoginServiceMock = {
      userRoles: new BehaviorSubject<UserRole[]>([])
    };

    userLoginServiceMock.userRoles.next([]);
    component.ngOnInit();
    expect(component.isMatExpansionPanelExpanded).toBe(false);
    expect(component.matPanelTitleIcon).toBe('list_alt');
  });
});
