/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import {
  OrderTableViewComponent
} from "@base/src/app/layouts/order-view/components/order-table-view/order-table-view.component";
import { DescriptionDialogComponent } from "@base/src/app/dialogs/description-dialog/description-dialog.component";
import { TranslateModule } from '@ngx-translate/core';
import { DangerousGoodLabelModule } from '@base/src/app/layouts/dangerous-good-label/dangerous-good-label.module';
import { mockOrderPositionWithId } from "@core/api-interfaces/lib/mocks/order-position/orderPosition.mock";
import { mockOrderWithId } from "@core/api-interfaces/lib/mocks/order/order.mock";

describe('OrderTableViewComponent', () => {
  let component: OrderTableViewComponent;
  let fixture: ComponentFixture<OrderTableViewComponent>;

  beforeEach(async () => {
    registerLocaleData(localeDe, 'de-DE', localeDeExtra);
    const mockref: Partial<MatDialogRef<DescriptionDialogComponent>> = {};
    await TestBed.configureTestingModule({
      declarations: [OrderTableViewComponent, DescriptionDialogComponent],
      imports: [
        MatDialogModule,
        MatIconModule,
        TranslateModule.forRoot(),
        DangerousGoodLabelModule,
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockref,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderTableViewComponent);
    component = fixture.componentInstance;
    component.order = mockOrderWithId;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate the total quantity correctly', () => {
    expect(component.totalQuantity).toEqual(mockOrderPositionWithId.quantity * component.order.orderPositions.length);
  });

  it('should calculate the total amount correctly', () => {
    expect(component.totalAmount).toEqual(mockOrderPositionWithId.totalAmount * component.order.orderPositions.length);
  });

  it('should calculate the total transport points correctly', () => {
    expect(component.totalTransportPoints).toEqual(
      mockOrderPositionWithId.transportPoints * component.order.orderPositions.length
    );
  });

  it('should open the description dialog', inject([MatDialog], (dialog: MatDialog) => {
    const dialogSpy = jest.spyOn(dialog, 'open').mockImplementation();
    component.onDisplayDescription('Testtitel', 'Testbeschreibung');
    expect(dialogSpy).toHaveBeenNthCalledWith(1, DescriptionDialogComponent, {
      data: { title: 'Testtitel', description: 'Testbeschreibung' },
      width: '600px',
    });
  }));
});
