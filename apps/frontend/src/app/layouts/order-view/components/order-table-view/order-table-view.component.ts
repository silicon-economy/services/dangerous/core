/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DescriptionDialogComponent } from '@base/src/app/dialogs/description-dialog/description-dialog.component';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-order-table-view [order]',
  templateUrl: './order-table-view.component.html',
  styleUrls: ['./order-table-view.component.scss'],
})
export class OrderTableViewComponent {
  @Input() order!: OrderWithId;
  public displayedColumns: string[] = [
    'orderPositionNumber',
    'unNumber',
    'description',
    'hazardLabel',
    'polluting',
    'packagingGroup',
    'tunnelRestrictionCode',
    'packagingCode',
    'package',
    'quantity',
    'individualAmount',
    'totalAmount',
    'transportPoints',
  ];

  constructor(
    private readonly dialog: MatDialog,
    private translateService: TranslateService
  ) {}

  get totalQuantity(): number {
    if (this.order.orderPositions.length > 0) {
      return this.order.orderPositions
        .map((orderPosition) => orderPosition.quantity)
        .reduce(function (a, b) {
          return a + b;
        });
    }
    return 0;
  }

  get totalAmount(): number {
    if (this.order.orderPositions.length > 0) {
      return this.order.orderPositions
        .map((orderPosition) => orderPosition.totalAmount)
        .reduce(function (a, b) {
          return a + b;
        });
    }
    return 0;
  }

  get totalTransportPoints(): number {
    if (this.order.orderPositions.length > 0) {
      return this.order.orderPositions
        .map((orderPosition) => orderPosition.transportPoints)
        .reduce(function (a, b) {
          return a + b;
        });
    }
    return 0;
  }

  /**
   * Trigger to display a description dialog with given title and description
   *
   * @param titleKey - The description title key.
   * @param descriptionKey - The description text key.
   */
  public onDisplayDescription(titleKey: string, descriptionKey: string): void {
    this.translateService.get(titleKey).subscribe((titleTranslation: string) => {
      this.translateService.get(descriptionKey).subscribe((descriptionTranslation: string) => {
        this.dialog.open(DescriptionDialogComponent, {
          data: {
            title: this.translateService.instant(titleTranslation) as string,
            description: this.translateService.instant(descriptionTranslation) as string,
          },
          width: '600px',
        });
      });
    });
  }
}
