/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';

import { CreateGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend';
import {
  CreateDangerousGoodRegistrationDto
} from "@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/create-dangerous-good-registration.dto";


@Component({
  selector: 'app-create-document-summary',
  templateUrl: './create-document-summary.component.html',
  styleUrls: ['./create-document-summary.component.scss'],
})
export class CreateDocumentSummaryComponent {
  @Input() createDocumentRequestDto: CreateGoodsDataDto | CreateDangerousGoodRegistrationDto
}
