/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateDocumentSummaryComponent } from './create-document-summary.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { TranslateModule } from "@ngx-translate/core";
import { ConsigneeListDialogModule } from "@base/src/app/dialogs/consignee-list-dialog/consignee-list-dialog.module";
import { CompanyViewModule } from "@base/src/app/layouts/company-view/company-view.module";
import { CarrierViewModule } from "@base/src/app/layouts/carrier-view/carrier-view.module";
import { DangerousGoodLabelModule } from "@base/src/app/layouts/dangerous-good-label/dangerous-good-label.module";
import { OrderPositionViewModule } from "@shared/components/order-position-view/order-position-view.module";

@NgModule({
  declarations: [CreateDocumentSummaryComponent],
  exports: [CreateDocumentSummaryComponent],
  imports: [
    CommonModule,
    MatDividerModule,
    MatCardModule,
    MatButtonModule,
    TranslateModule,
    ConsigneeListDialogModule,
    CompanyViewModule,
    CarrierViewModule,
    DangerousGoodLabelModule,
    OrderPositionViewModule
  ],
  providers: [
    UserLoginService,
  ],
})
export class CreateDocumentSummaryModule {}
