/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDocumentSummaryComponent } from './create-document-summary.component';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { TranslateModule } from '@ngx-translate/core';
import {
  mockCreateTransportDocumentRequestDto
} from "@core/api-interfaces/lib/mocks/transport-document/create-transport-document-request-dto.mock";


describe('SummaryInformationPageComponent', () => {
  let component: CreateDocumentSummaryComponent;
  let fixture: ComponentFixture<CreateDocumentSummaryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateDocumentSummaryComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        HttpClient,
        HttpHandler,
        UserLoginService,
        UserAuthenticationService,
      ],
      imports: [TranslateModule.forRoot()],
    });
    fixture = TestBed.createComponent(CreateDocumentSummaryComponent);
    component = fixture.componentInstance;
    component.createDocumentRequestDto = mockCreateTransportDocumentRequestDto;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
