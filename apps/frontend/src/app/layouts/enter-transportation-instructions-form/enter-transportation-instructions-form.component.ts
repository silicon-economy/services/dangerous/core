/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-enter-transportation-instructions-form',
  templateUrl: './enter-transportation-instructions-form.component.html',
})
export class EnterTransportationInstructionsFormComponent {
  public transportationInstructionsFormControl: FormControl = new FormControl();

  /**
   * Emitter used for providing the current value of the transportation instructions to the parent component
   */
  @Output() transportationInstructionsEmitter: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
    this.transportationInstructionsFormControl.valueChanges.subscribe((transportationInstructions: string) =>
      this.transportationInstructionsEmitter.emit(transportationInstructions)
    );
  }
}
