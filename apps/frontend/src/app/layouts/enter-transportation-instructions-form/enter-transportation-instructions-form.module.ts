/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  EnterTransportationInstructionsFormComponent
} from '@base/src/app/layouts/enter-transportation-instructions-form/enter-transportation-instructions-form.component';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [EnterTransportationInstructionsFormComponent],
  imports: [CommonModule, MatInputModule, TranslateModule, ReactiveFormsModule],
  exports: [EnterTransportationInstructionsFormComponent],
})
export class EnterTransportationInstructionsFormModule {}
