/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterTransportationInstructionsFormComponent } from './enter-transportation-instructions-form.component';
import { TranslateModule } from '@ngx-translate/core';

describe('EnterTransportationInstructionsFormComponent', () => {
  let component: EnterTransportationInstructionsFormComponent;
  let fixture: ComponentFixture<EnterTransportationInstructionsFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EnterTransportationInstructionsFormComponent],
      imports: [TranslateModule.forRoot()],
    });
    fixture = TestBed.createComponent(EnterTransportationInstructionsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
