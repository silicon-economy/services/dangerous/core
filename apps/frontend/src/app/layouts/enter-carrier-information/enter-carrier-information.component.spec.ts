/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterCarrierInformationComponent } from './enter-carrier-information.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { EventService } from "@shared/event.service";
import { TranslateModule } from "@ngx-translate/core";

describe('EnterCarrierInformationComponent', () => {
  let component: EnterCarrierInformationComponent;
  let fixture: ComponentFixture<EnterCarrierInformationComponent>;

  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [EnterCarrierInformationComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [EventService],
      imports: [ReactiveFormsModule, TranslateModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(EnterCarrierInformationComponent);
    component = fixture.componentInstance
    fixture.detectChanges();
  });

  it('should create', (): void => {
    expect(component).toBeTruthy();
  });

  it('should set the transportation instructions', ()=>{
    const validityEmitterSpy = jest.spyOn(component.carrierInformationIsValidEmitter, 'emit')
    const carrierInformationEmitterSpy  = jest.spyOn(component.carrierInformationEmitter, 'emit')
    component.setTransportationInstructions('Just a mock. Yo clean up the damn vehicle after dropping off the load!')
    expect(validityEmitterSpy).toHaveBeenCalled()
    expect(carrierInformationEmitterSpy).toHaveBeenCalled()
  })
});
