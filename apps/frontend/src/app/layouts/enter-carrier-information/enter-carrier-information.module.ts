/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnterCarrierInformationComponent } from './enter-carrier-information.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { A11yModule } from '@angular/cdk/a11y';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import {
  EnterTransportationInstructionsFormModule
} from "@base/src/app/layouts/enter-transportation-instructions-form/enter-transportation-instructions-form.module";

@NgModule({
  declarations: [EnterCarrierInformationComponent],
  exports: [EnterCarrierInformationComponent],
    imports: [
        CommonModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        A11yModule,
        MatInputModule,
        TranslateModule,
        EnterTransportationInstructionsFormModule,
    ],
})
export class EnterCarrierInformationModule {}
