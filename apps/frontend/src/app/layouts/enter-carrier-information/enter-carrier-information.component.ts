/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CarrierInformation } from '@base/src/app/models/carrierInformation';

@Component({
  selector: 'app-enter-carrier-information',
  templateUrl: './enter-carrier-information.component.html',
  styleUrls: ['./enter-carrier-information.component.scss'],
})
export class EnterCarrierInformationComponent {
  /**
   * Emitter used for providing the current value of the entered carrier information to the parent component
   */
  @Output() carrierInformationEmitter: EventEmitter<CarrierInformation> = new EventEmitter<CarrierInformation>();

  /**
   * Emitter used for providing the current validity of the entered carrier information to the parent component
   */
  @Output() carrierInformationIsValidEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  public carrierForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    driver: new FormControl('', [Validators.required]),
    licensePlate: new FormControl('', [Validators.required]),
    transportationInstructions: new FormControl(),
  });

  constructor() {
    this.carrierForm.valueChanges.subscribe((carrierInformation: CarrierInformation) => {
      this.carrierInformationEmitter.emit(carrierInformation);
      this.carrierInformationIsValidEmitter.emit(this.carrierForm.valid);
    });
  }

  /**
   * A simple setter for transportationInstructions. Used for Adapting the current value from the child component.
   *
   * @param transportationInstructions - the transportationInstructions to be set
   */
  setTransportationInstructions(transportationInstructions: string): void {
    this.carrierForm.get('transportationInstructions').setValue(transportationInstructions)
  }
}
