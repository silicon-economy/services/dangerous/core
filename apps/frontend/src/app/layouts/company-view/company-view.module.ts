/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyViewComponent } from '@base/src/app/layouts/company-view/company-view.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [CompanyViewComponent],
  imports: [CommonModule, MatIconModule, MatButtonModule],
  exports: [CompanyViewComponent],
})
export class CompanyViewModule {}
