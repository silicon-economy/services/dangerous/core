/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { DangerousGood } from '@core/api-interfaces/lib/dtos/frontend';

@Component({
  selector: 'app-dangerous-good-label[dangerousGood]',
  templateUrl: './dangerous-good-label.component.html',
  styleUrls: ['./dangerous-good-label.component.scss'],
})
export class DangerousGoodLabelComponent {
  @Input() public dangerousGood!: DangerousGood;
  @Input() public customClass?: string = "defaultIconStyle";
}
