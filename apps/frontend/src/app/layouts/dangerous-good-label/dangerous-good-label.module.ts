/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DangerousGoodLabelComponent } from '@base/src/app/layouts/dangerous-good-label/dangerous-good-label.component';

@NgModule({
  declarations: [DangerousGoodLabelComponent],
  imports: [CommonModule],
  exports: [DangerousGoodLabelComponent],
})
export class DangerousGoodLabelModule {}
