/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DangerousGoodLabelComponent } from '@base/src/app/layouts/dangerous-good-label/dangerous-good-label.component';
import { mockDangerousGood } from "@core/api-interfaces/lib/mocks/dangerous-good/dangerous-good.mock";

describe('LabelSymbolsComponent', () => {
  let component: DangerousGoodLabelComponent;
  let fixture: ComponentFixture<DangerousGoodLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DangerousGoodLabelComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DangerousGoodLabelComponent);
    component = fixture.componentInstance;
    component.dangerousGood = mockDangerousGood
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
