/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CarrierViewComponent } from './carrier-view.component';
import { TranslateModule } from '@ngx-translate/core';
import { mockCarrier } from "@core/api-interfaces/lib/mocks/carrier/carrier.mock";

describe('CarrierViewComponent', () => {
  let component: CarrierViewComponent;
  let fixture: ComponentFixture<CarrierViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CarrierViewComponent],
      imports: [TranslateModule.forRoot()],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrierViewComponent);
    component = fixture.componentInstance;
    component.carrier = mockCarrier;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
