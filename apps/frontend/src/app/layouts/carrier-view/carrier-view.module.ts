/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarrierViewComponent } from '@base/src/app/layouts/carrier-view/carrier-view.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [CarrierViewComponent],
  imports: [CommonModule, TranslateModule],
  exports: [CarrierViewComponent],
})
export class CarrierViewModule {}
