/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { Carrier } from '@core/api-interfaces/lib/dtos/frontend';

@Component({
  selector: 'app-carrier-view[carrier]',
  templateUrl: './carrier-view.component.html',
})
export class CarrierViewComponent {
  @Input() carrier!: Carrier;
}
