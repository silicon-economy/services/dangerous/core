/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { Company } from '@core/api-interfaces/lib/dtos/frontend';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-side-navigation',
  templateUrl: './side-navigation.component.html',
  styleUrls: ['./side-navigation.component.scss'],
})
export class SideNavigationComponent {
  protected currentUserRoles: UserRole[];
  protected currentCompany: Company;
  protected currentUserProfilePicturePath = '';

  constructor(readonly userService: UserLoginService) {
    this.userService.userCompany.subscribe((companyDto: Company) => (this.currentCompany = companyDto));
    this.userService.userRoles.subscribe((userRoles: UserRole[]) => {
      this.currentUserRoles = userRoles;
      this.getProfilePicturePath(userRoles);
    });
  }

  /**
   * Get the profile picture path.
   *
   * @param userRoles - Array of user roles
   */
  private getProfilePicturePath(userRoles: UserRole[]): void {
    if (userRoles) {
      if (this.currentUserRoles.length != 0) {
        this.currentUserProfilePicturePath =
          `../../../${environment.ASSETS.USER_PROFILE_PICTURES.PATH}` +
          this.currentUserRoles[0] +
          `${environment.ASSETS.USER_PROFILE_PICTURES.FILE_TYPE}`;
      }
    }
  }
}
