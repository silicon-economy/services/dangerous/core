/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavigationComponent } from './side-navigation.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { Observable, of } from 'rxjs';
import { Address, Company, ContactPerson } from '@core/api-interfaces/lib/dtos/frontend';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';

class UserLoginServiceMock {
  public userRoles: Observable<UserRole[]> = of([UserRole.consignor]);
  public userCompany: Observable<Company> = of(
    new Company(
      'foobar',
      new Address('foobar', 'foobar', 'foobar', 'foobar', 'foobar'),
      new ContactPerson('foobar', 'foobar', 'foobar', 'foobar')
    )
  );
}

describe('SideNavigationComponent', () => {
  let component: SideNavigationComponent;
  let fixture: ComponentFixture<SideNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot()],
      declarations: [SideNavigationComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: UserLoginService,
          useClass: UserLoginServiceMock,
        },
        UserAuthenticationService,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SideNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should subscribe to observables in constructor', () => {
    expect(component['currentCompany']).toEqual(
      new Company(
        'foobar',
        new Address('foobar', 'foobar', 'foobar', 'foobar', 'foobar'),
        new ContactPerson('foobar', 'foobar', 'foobar', 'foobar')
      )
    );
    expect(component['currentUserRoles']).toEqual([UserRole.consignor]);
  });
});
