/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';

import { SideNavigationComponent } from './side-navigation.component';
import { MatListModule } from '@angular/material/list';
import { MainNavigationModule } from './components/main-navigation/main-navigation.module';
import { MatLineModule } from '@angular/material/core';
import { FooterNavigationModule } from './components/footer-navigation/footer-navigation.module';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [SideNavigationComponent],
  exports: [SideNavigationComponent],
  imports: [
    CommonModule,
    MatListModule,
    MainNavigationModule,
    MatLineModule,
    FooterNavigationModule,
    MatIconModule,
    TranslateModule,
    NgOptimizedImage,
  ],
  providers: [],
})
export class SideNavigationModule {}
