/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainNavigationRoutingModule } from './main-navigation-routing.module';
import { MainNavigationComponent } from './main-navigation.component';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';
import { FlexModule } from '@angular/flex-layout';
import { MatLineModule } from '@angular/material/core';
import {
  ShippingLabelQrCodeScannerDialogModule
} from '@base/src/app/dialogs/shipping-label-qr-code-scanner-dialog/shipping-label-qr-code-scanner-dialog.module';
import {
  HandoverQrCodeScannerDialogModule
} from '@base/src/app/dialogs/handover-qr-code-scanner-dialog/handover-qr-code-scanner-dialog.module';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';

@NgModule({
  declarations: [MainNavigationComponent],
  exports: [MainNavigationComponent],
  imports: [
    CommonModule,
    MainNavigationRoutingModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    TranslateModule,
    FlexModule,
    MatLineModule,
    ShippingLabelQrCodeScannerDialogModule,
    HandoverQrCodeScannerDialogModule,
  ],
  providers: [TransportDocumentService],
})
export class MainNavigationModule {}
