/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EventService } from '@shared/event.service';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-footer-navigation',
  templateUrl: './footer-navigation.component.html',
})
export class FooterNavigationComponent {
  public version = '0.9.0';
  public environment = environment;

  constructor(
    private router: Router,
    private eventService: EventService,
    private userService: UserLoginService
  ) {}

  /**
   * Logs out the user.
   */
  public logout(): void {
    this.userService.logout();
  }
}
