/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterNavigationComponent } from './footer-navigation.component';
import { EventService } from '@shared/event.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { UserServiceModule } from '@core/services/user-service/user-service.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('FooterNavigationComponent', () => {
  let component: FooterNavigationComponent;
  let fixture: ComponentFixture<FooterNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UserServiceModule, HttpClientTestingModule, TranslateModule.forRoot()],
      declarations: [FooterNavigationComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [EventService],
    }).compileComponents();

    fixture = TestBed.createComponent(FooterNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
