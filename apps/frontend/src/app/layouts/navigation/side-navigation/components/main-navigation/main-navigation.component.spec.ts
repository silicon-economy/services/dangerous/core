/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainNavigationComponent } from './main-navigation.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { TranslateModule } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ScanDialogService } from '@shared/services/scan-dialog-service/scan-dialog.service';
import { MatDialogModule } from '@angular/material/dialog';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';

describe('MainNavigationComponent', () => {
  let component: MainNavigationComponent;
  let fixture: ComponentFixture<MainNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        RouterTestingModule.withRoutes([
          {
            path: 'transport-documents/create',
            redirectTo: '',
          },
          {
            path: 'dangerous-good-registrations/create',
            redirectTo: '',
          },
        ]),
        FeedbackDialogServiceModule,
        NotificationServiceModule,
      ],
      declarations: [MainNavigationComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: UserLoginService,
          useValue: {
            userRoles: of([UserRole.carrier]),
          },
        },
        UserAuthenticationService,
        ScanDialogService,
        TransportDocumentHttpService,
        TransportDocumentService,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MainNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to "/transport-documents/create" when openCreateTransportDocument is called', async () => {
    const navigateSpy = jest.spyOn(Router.prototype, 'navigateByUrl');
    await component.openCreateTransportDocument();
    expect(navigateSpy).toHaveBeenCalledWith('/transport-documents/create');
  });

  it('should navigate to "/create-dangerous-good-registration" when openCreateDangerousGoodRegistration is called', async () => {
    const navigateSpy = jest.spyOn(Router.prototype, 'navigateByUrl');
    await component.openCreateDangerousGoodRegistration();
    expect(navigateSpy).toHaveBeenCalledWith('/dangerous-good-registrations/create');
  });

  it('should handle error when navigation to "/transport-documents/create" fails', async () => {
    const navigateByUrlSpy = jest
      .spyOn(Router.prototype, 'navigateByUrl')
      .mockRejectedValueOnce(new Error('Navigation error'));
    const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();

    await expect(component.openCreateTransportDocument()).rejects.toThrow('Navigation error');
    expect(consoleErrorSpy).toHaveBeenCalledWith(
      'Error navigating to /transport-documents/create: ',
      new Error('Navigation error')
    );

    navigateByUrlSpy.mockRestore();
    consoleErrorSpy.mockRestore();
  });

  it('should handle error when navigation to "/create-dangerous-good-registration" fails', async () => {
    const navigateByUrlSpy = jest
      .spyOn(Router.prototype, 'navigateByUrl')
      .mockRejectedValueOnce(new Error('Navigation error'));
    const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();

    await expect(component.openCreateDangerousGoodRegistration()).rejects.toThrow('Navigation error');
    expect(consoleErrorSpy).toHaveBeenCalledWith(
      'Error navigating to /dangerous-good-registrations/create:',
      new Error('Navigation error')
    );

    navigateByUrlSpy.mockRestore();
    consoleErrorSpy.mockRestore();
  });
});
