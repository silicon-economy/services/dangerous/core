/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { ScanDialogService } from '@shared/services/scan-dialog-service/scan-dialog.service';
import {
  ShippingLabelQrCodeScannerDialogComponent
} from '@base/src/app/dialogs/shipping-label-qr-code-scanner-dialog/shipping-label-qr-code-scanner-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import { PermissionService } from '@shared/services/permission-service/permission.service';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
})
export class MainNavigationComponent {
  protected readonly UserRole = UserRole;

  constructor(
    private readonly scanDialogService: ScanDialogService,
    private router: Router,
    private readonly userService: UserLoginService,
    private ngZone: NgZone,
    private readonly translateService: TranslateService,
    protected readonly transportDocumentService: TransportDocumentService,
    protected readonly permissionService: PermissionService
  ) {}

  /**
   * Navigates to the '/create-transport-document' route asynchronously.
   *
   * @returns A promise that resolves when the navigation is successful.
   * @throws If an error occurs during navigation.
   */
  public async openCreateTransportDocument(): Promise<void> {
    await this.ngZone.run(async () => {
      await this.router.navigateByUrl('/transport-documents/create').catch((error) => {
        console.error('Error navigating to /transport-documents/create: ', error);
        throw error;
      });
    });
  }

  /**
   * Navigates to the '/create-dangerous-good-registration' route asynchronously.
   *
   * @returns A promise that resolves when the navigation is successful.
   * @throws If an error occurs during navigation.
   */
  public async openCreateDangerousGoodRegistration(): Promise<void> {
    await this.ngZone.run(async () => {
      await this.router.navigateByUrl('/dangerous-good-registrations/create').catch((error) => {
        console.error('Error navigating to /dangerous-good-registrations/create:', error);
        throw error;
      });
    });
  }

  /**
   * Show a dialog window with ScanQrCodePageComponent
   */
  public openScanDialogVisualInspectionCarrier(): void {
    this.translateService.get('dialogs.visualInspectionCarrier.title').subscribe((translation: string) => {
      this.scanDialogService.createDialog(ShippingLabelQrCodeScannerDialogComponent, [
        { attributeName: 'titleHeader', attribute: translation },
      ]);
    });
  }
}
