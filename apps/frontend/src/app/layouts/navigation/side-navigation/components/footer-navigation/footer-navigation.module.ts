/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterNavigationComponent } from './footer-navigation.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatDividerModule } from '@angular/material/divider';
import { TranslateModule } from '@ngx-translate/core';
import { FlexModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatLineModule } from '@angular/material/core';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { RouterLink, RouterLinkActive } from '@angular/router';

@NgModule({
  declarations: [FooterNavigationComponent],
  exports: [FooterNavigationComponent],
  imports: [
    CommonModule,
    MatMenuModule,
    MatDividerModule,
    TranslateModule,
    FlexModule,
    MatIconModule,
    MatLineModule,
    MatListModule,
    MatButtonModule,
    RouterLink,
    RouterLinkActive,
  ],
})
export class FooterNavigationModule {}
