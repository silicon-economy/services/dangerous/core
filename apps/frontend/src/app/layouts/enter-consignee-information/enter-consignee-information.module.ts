/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnterConsigneeInformationComponent } from './enter-consignee-information.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule } from '@angular/material/button';
import {
  EnterCompanyInformationFormGroupModule
} from "@base/src/app/layouts/enter-company-information-form-group/enter-company-information-form-group.module";

@NgModule({
  declarations: [EnterConsigneeInformationComponent],
  exports: [EnterConsigneeInformationComponent],
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        MatCardModule,
        MatIconModule,
        TranslateModule,
        MatButtonModule,
        EnterCompanyInformationFormGroupModule,
    ],
})
export class EnterConsigneeInformationModule {}
