/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterConsigneeInformationComponent } from './enter-consignee-information.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import {
  EnterCompanyInformationFormGroupComponent
} from "@base/src/app/layouts/enter-company-information-form-group/enter-company-information-form-group.component";
import { mockCompany } from "@core/api-interfaces/lib/mocks/company/company.mock";

describe('EnterConsigneeInformation', () => {
  let component: EnterConsigneeInformationComponent;
  let fixture: ComponentFixture<EnterConsigneeInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EnterConsigneeInformationComponent, EnterCompanyInformationFormGroupComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [ReactiveFormsModule, TranslateModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(EnterConsigneeInformationComponent);
    component = fixture.componentInstance;
    component.consigneeIndex = 0;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit consignee when value changes in companyFormGroup', (): void => {
    const emitSpy = jest.spyOn(component.consigneeEmitter, 'emit');
    component.companyFormGroupComponent.companyEmitter.emit(mockCompany)
    expect(emitSpy).toHaveBeenCalledWith(mockCompany);
  });


  it('should reset companyFormGroup', (): void => {
    const resetSpy = jest.spyOn(component.companyFormGroupComponent, 'resetCompanyFormGroup');
    component.resetConsigneeFormGroup();
    expect(resetSpy).toHaveBeenCalled();
  });

  it('should remove the consignee', ()=>{
    const resetSpy = jest.spyOn(component.destroyMeEmitter, 'emit');
    component.removeConsignee()
    expect(resetSpy).toHaveBeenCalled();
  })

  it('should get the displayed consignee index', () => {
    expect(component.getDisplayedConsigneeIndex()).toEqual(1)
  })
});
