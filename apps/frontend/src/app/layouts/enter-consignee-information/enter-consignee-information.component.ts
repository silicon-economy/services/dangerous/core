/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AfterViewInit, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Company } from '@core/api-interfaces/lib/dtos/frontend';
import {
  EnterCompanyInformationFormGroupComponent
} from '@base/src/app/layouts/enter-company-information-form-group/enter-company-information-form-group.component';

@Component({
  selector: 'app-enter-consignee-information',
  templateUrl: './enter-consignee-information.component.html',
  styleUrls: ['./enter-consignee-information.component.scss'],
})
export class EnterConsigneeInformationComponent implements AfterViewInit {
  /**
   * Used to display the index of the consignee and emit the index to the parent component if the user tries to remove this very consignee
   */
  @Input() public consigneeIndex?: number;

  @ViewChild('companyFormGroupComponent') companyFormGroupComponent: EnterCompanyInformationFormGroupComponent;

  /**
   * Emitter used for providing the current state of the entered company information to the parent component.
   */
  @Output() consigneeEmitter: EventEmitter<Company> = new EventEmitter<Company>();

  /**
   * Emitter used for providing the current validity of the formGroup to the parent component.
   */
  @Output() consigneeInformationIsValidEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Emitter used to trigger the removal of this component in the parent component.
   * It is used when the remove-consignee-button is clicked.
   */
  @Output() destroyMeEmitter: EventEmitter<number> = new EventEmitter<number>();

  ngAfterViewInit(): void {
    this.companyFormGroupComponent.companyEmitter.subscribe((consignee: Company) =>
      this.consigneeEmitter.emit(consignee)
    );
    this.companyFormGroupComponent.companyFormGroupIsValidEmitter.subscribe((isValid: boolean) =>
      this.consigneeInformationIsValidEmitter.emit(isValid)
    );
  }

  /**
   * Resets the company form group.
   */
  public resetConsigneeFormGroup(): void {
    this.companyFormGroupComponent.resetCompanyFormGroup();
  }

  /**
   * Executed when the remove-consignee button is clicked
   * Emits the components consigneeIndex to the parent component, so it can be removed
   */
  public removeConsignee(): void {
    this.destroyMeEmitter.emit(this.consigneeIndex);
  }

  /**
   * The consigneeIndex to be displayed is the saved consigneeIndex incremented by one
   * This method returns that value
   *
   * @returns number consigneeIndex + 1
   */
  public getDisplayedConsigneeIndex(): number {
    if (this.consigneeIndex != undefined) {
      return this.consigneeIndex + 1;
    }
  }
}
