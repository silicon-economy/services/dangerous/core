/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DescriptionDialogComponent } from '@base/src/app/dialogs/description-dialog/description-dialog.component';
import {
  OrderPositionStatus,
  OrderStatus,
  TransportDocumentStatus,
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/status.enum';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { UserLoginService } from '@core/services/user-service/user-login.service';

@Component({
  selector: 'app-status [status]',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss'],
})
export class StatusComponent implements OnInit {
  @Input() status!: TransportDocumentStatus | OrderStatus | OrderPositionStatus;
  protected readonly TransportDocumentStatus = TransportDocumentStatus;
  protected readonly OrderStatus = OrderStatus;
  mainUserRole: UserRole;

  constructor(
    private readonly dialog: MatDialog,
    private readonly userService: UserLoginService
  ) {}

  ngOnInit(): void {
    this.userService.userRoles.subscribe((currentUserRole: UserRole[]) => {
      this.mainUserRole = currentUserRole[0];
    });
  }

  /**
   * Trigger to display a description dialog with the status as title and its description
   *
   * @param event - Mouse event
   */
  public onDisplayStatusDescription(event: MouseEvent): void {
    event.stopPropagation();
    const title: string = 'status.' + this.mainUserRole + '.' + this.status + '.short';
    const description: string = 'status.' + this.mainUserRole + '.' + this.status + '.description';
    this.dialog.open(DescriptionDialogComponent, {
      data: {
        title,
        description,
      },
      width: '600px',
    });
  }
}
