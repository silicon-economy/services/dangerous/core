/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { LogEntry } from '@core/api-interfaces/lib/dtos/frontend/transport-document/log-entry';

@Component({
  selector: 'app-document-history-dialog',
  templateUrl: './status-history-dialog.component.html',
  styleUrls: ['./status-history-dialog.component.scss'],
})
export class StatusHistoryDialogComponent {
  logEntries: LogEntry[];

  constructor(
    public dialogRef: MatDialogRef<StatusHistoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { logEntries: LogEntry[] }
  ) {
    this.logEntries = data.logEntries;
    this.logEntries.reverse();
  }
}
