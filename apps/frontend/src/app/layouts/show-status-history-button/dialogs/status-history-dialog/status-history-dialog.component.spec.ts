/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { StatusHistoryDialogComponent } from './status-history-dialog.component';
import {
  ShowStatusHistoryButtonComponent
} from '@base/src/app/layouts/show-status-history-button/show-status-history-button.component';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { mockLogEntries } from "@core/api-interfaces/lib/mocks/log-entries/log-entries.mock";

describe('DocumentHistoryComponent', () => {
  let showStatusHistoryButtonComponent: ShowStatusHistoryButtonComponent;
  let statusHistoryDialogComponent: StatusHistoryDialogComponent;
  let fixtureButton: ComponentFixture<ShowStatusHistoryButtonComponent>;

  beforeEach(async () => {
    const mockref: Partial<MatDialogRef<StatusHistoryDialogComponent>> = {};
    await TestBed.configureTestingModule({
      imports: [MatDialogModule, MatIconModule, TranslateModule.forRoot()],
      declarations: [ShowStatusHistoryButtonComponent, StatusHistoryDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockref,
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            logEntries: [],
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixtureButton = TestBed.createComponent(ShowStatusHistoryButtonComponent);
    showStatusHistoryButtonComponent = fixtureButton.componentInstance;
    showStatusHistoryButtonComponent.logEntries = mockLogEntries
    const mockEvent = {
      stopPropagation: jest.fn(),
    };
    const fixtureDialogRef = showStatusHistoryButtonComponent.openStatusHistoryDialog(
      mockEvent as unknown as MouseEvent
    );

    statusHistoryDialogComponent = fixtureDialogRef.componentInstance;
  });

  it('should create', () => {
    expect(statusHistoryDialogComponent).toBeTruthy();
  });

  it('should get all log entries', () => {
    const logEntries = statusHistoryDialogComponent.logEntries;
    expect(logEntries.length).toEqual(4);
    expect(logEntries[0].status).toEqual('created');
    expect(logEntries[1].status).toEqual('accepted');
    expect(logEntries[2].status).toEqual('transport');
    expect(logEntries[3].status).toEqual('transport_denied');
  });
});
