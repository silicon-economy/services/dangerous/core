/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { LogEntry } from '@core/api-interfaces/lib/dtos/frontend/transport-document/log-entry';
import {
  StatusHistoryDialogComponent
} from '@base/src/app/layouts/show-status-history-button/dialogs/status-history-dialog/status-history-dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-show-status-history-button [logEntries]',
  templateUrl: './show-status-history-button.component.html',
})
export class ShowStatusHistoryButtonComponent {
  @Input() logEntries!: LogEntry[];

  constructor(private readonly dialog: MatDialog) {}

  /**
   * Open dialog window containing the status history
   *
   * @param event - Event whose propagation is to be stopped
   * @returns Material dialog reference
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public openStatusHistoryDialog(event: MouseEvent): MatDialogRef<StatusHistoryDialogComponent, any> {
    event.stopPropagation();
    return this.dialog.open(StatusHistoryDialogComponent, {
      data: { logEntries: this.logEntries },
      width: '500px',
    });
  }
}
