/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { ShowStatusHistoryButtonComponent } from './show-status-history-button.component';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import {
  StatusHistoryDialogComponent
} from '@base/src/app/layouts/show-status-history-button/dialogs/status-history-dialog/status-history-dialog.component';
import { TranslateModule } from '@ngx-translate/core';
import { mockLogEntries } from '@core/api-interfaces/lib/mocks/log-entries/log-entries.mock';

describe('ShowStatusHistoryButtonComponent', () => {
  let component: ShowStatusHistoryButtonComponent;
  let fixture: ComponentFixture<ShowStatusHistoryButtonComponent>;

  beforeEach(() => {
    const mockref: Partial<MatDialogRef<StatusHistoryDialogComponent>> = {};
    TestBed.configureTestingModule({
      declarations: [ShowStatusHistoryButtonComponent],
      imports: [MatDialogModule, MatIconModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockref,
        },
      ],
    });
    fixture = TestBed.createComponent(ShowStatusHistoryButtonComponent);
    component = fixture.componentInstance;
    component.logEntries = mockLogEntries;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open the status history dialog', inject([MatDialog], (dialog: MatDialog) => {
    const dialogSpy = jest.spyOn(dialog, 'open').mockImplementation();
    const mockEvent = {
      stopPropagation: jest.fn(),
    };
    component.openStatusHistoryDialog(mockEvent as unknown as MouseEvent);
    expect(dialogSpy).toHaveBeenNthCalledWith(1, StatusHistoryDialogComponent, {
      data: { logEntries: mockLogEntries },
      width: '500px',
    });
  }));
});
