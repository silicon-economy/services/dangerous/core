/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowStatusHistoryButtonComponent } from './show-status-history-button.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [ShowStatusHistoryButtonComponent],
  exports: [ShowStatusHistoryButtonComponent],
  imports: [CommonModule, MatIconModule, MatButtonModule, TranslateModule],
})
export class ShowStatusHistoryButtonModule {}
