/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Company } from '@core/api-interfaces/lib/dtos/frontend';

@Component({
  selector: 'app-enter-company-information-form-group',
  templateUrl: './enter-company-information-form-group.component.html',
})
export class EnterCompanyInformationFormGroupComponent implements OnInit {
  /**
   * The Input is used for providing a default company value that is set on initialization
   * It is used when the consignor information is shown, because these are extracted from the JWT-Token
   */
  @Input() companyDefaultValue?: Company;

  /**
   * The Input is used to disable all FormControls of the FormGroup
   * It is used when the consignor information is shown
   */
  @Input() interActionsAreDisabled?: boolean = false;

  companyFormGroup: FormGroup = this.initializeCompanyFormGroup();

  /**
   * Emitter used for providing the current state of the entered company information to the parent component.
   */
  @Output() companyEmitter: EventEmitter<Company> = new EventEmitter<Company>();

  /**
   * Emitter used for providing the current validity of the formGroup to the parent component.
   */
  @Output() companyFormGroupIsValidEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private formBuilder: FormBuilder) {
    this.companyFormGroup.valueChanges.subscribe((company: Company) => {
      this.companyEmitter.emit(company);
      this.companyFormGroupIsValidEmitter.emit(this.companyFormGroup.valid);
    });
  }

  ngOnInit(): void {
    this.companyFormGroup.patchValue(this.companyDefaultValue);
    if (this.interActionsAreDisabled) {
      this.companyFormGroup.disable();
    }
  }

  /**
   * Resets the company form group.
   */
  public resetCompanyFormGroup(): void {
    this.companyFormGroup.reset('');
  }

  /**
   * Auxiliary function used to build the companyFormGroup
   *
   * @returns FormGroup the empty companyFormGroup
   */
  private initializeCompanyFormGroup(): FormGroup {
    return this.formBuilder.group(
      {
        name: ['', Validators.required],
        address: this.formBuilder.group({
          street: [''],
          number: [''],
          postalCode: [''],
          city: [''],
          country: [''],
        }),
        contact: this.formBuilder.group({
          name: [''],
          department: [''],
          phone: ['', Validators.required],
          mail: ['', Validators.email],
        }),
      },
      { validators: Validators.required }
    );
  }
}
