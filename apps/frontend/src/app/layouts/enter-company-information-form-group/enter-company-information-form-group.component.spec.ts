/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterCompanyInformationFormGroupComponent } from './enter-company-information-form-group.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { mockCompany } from '@core/api-interfaces/lib/mocks/company/company.mock';

describe('CompanyFormGroupComponent', () => {
  let component: EnterCompanyInformationFormGroupComponent;
  let fixture: ComponentFixture<EnterCompanyInformationFormGroupComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EnterCompanyInformationFormGroupComponent],
      imports: [
        TranslateModule.forRoot(),
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
      ],
    });
    fixture = TestBed.createComponent(EnterCompanyInformationFormGroupComponent);
    component = fixture.componentInstance;
    component.companyDefaultValue = mockCompany;
    component.interActionsAreDisabled = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reset the form group', () => {
    jest.spyOn(component.companyFormGroup, 'reset');
    component.resetCompanyFormGroup();
    expect(component.companyFormGroup.reset).toHaveBeenCalled();
  });
});
