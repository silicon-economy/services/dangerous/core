/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import { registerLocaleData } from '@angular/common';
import {
  FreightBaseDataViewComponent
} from '@base/src/app/layouts/freight-base-data-view/freight-base-data-view.component';
import { TranslateModule } from '@ngx-translate/core';
import { mockFreightIdsIncluded } from "@core/api-interfaces/lib/mocks/freight/freight.mock";

describe('FreightBaseDataViewComponent', () => {
  let component: FreightBaseDataViewComponent;
  let fixture: ComponentFixture<FreightBaseDataViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FreightBaseDataViewComponent],
      imports: [TranslateModule.forRoot()],
    }).compileComponents();
    registerLocaleData(localeDe, 'de-DE', localeDeExtra);
    fixture = TestBed.createComponent(FreightBaseDataViewComponent);
    component = fixture.componentInstance;
    component.freight = mockFreightIdsIncluded;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
