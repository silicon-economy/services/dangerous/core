/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { FreightIdsIncluded } from '@core/api-interfaces/lib/dtos/frontend/transport-document/freight-ids-included';

@Component({
  selector: 'app-freight-base-data-view[freight]',
  templateUrl: './freight-base-data-view.component.html',
})
export class FreightBaseDataViewComponent {
  @Input() freight!: FreightIdsIncluded;
}
