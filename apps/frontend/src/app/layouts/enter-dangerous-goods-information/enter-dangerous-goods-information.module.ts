/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnterDangerousGoodsInformationComponent } from './enter-dangerous-goods-information.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCardModule } from '@angular/material/card';
import { TranslateModule } from '@ngx-translate/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { MatDialogModule } from '@angular/material/dialog';
import {
  OrderPositionFormGroupGenerator
} from "@base/src/app/layouts/enter-dangerous-goods-information/component/order-position-input-form/services/order-position-form-group-generator";
import { DangerousGoodLabelModule } from "@base/src/app/layouts/dangerous-good-label/dangerous-good-label.module";
import {
  OrderPositionInputFormComponent
} from "@base/src/app/layouts/enter-dangerous-goods-information/component/order-position-input-form/order-position-input-form.component";
import { MatStepperModule } from "@angular/material/stepper";
import {
  SelectedDangerousGoodPreviewComponent
} from './component/order-position-input-form/components/selected-dangerous-good-preview/selected-dangerous-good-preview.component';
import {
  SelectPackagingDialogComponent
} from "@base/src/app/layouts/enter-dangerous-goods-information/dialogs/select-packaging-dialog/select-packaging-dialog.component";

@NgModule({
  declarations: [EnterDangerousGoodsInformationComponent, OrderPositionInputFormComponent, SelectedDangerousGoodPreviewComponent, SelectPackagingDialogComponent],
  exports: [EnterDangerousGoodsInformationComponent],
    imports: [
        CommonModule,
        MatExpansionModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatInputModule,
        MatButtonModule,
        MatBadgeModule,
        MatCardModule,
        TranslateModule,
        MatAutocompleteModule,
        MatDialogModule,
        DangerousGoodLabelModule,
        MatStepperModule,
    ],
  providers: [
    OrderPositionFormGroupGenerator,
  ],
})
export class EnterDangerousGoodsInformationModule {}
