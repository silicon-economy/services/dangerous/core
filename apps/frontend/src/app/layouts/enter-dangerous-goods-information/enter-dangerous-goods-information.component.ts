/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Freight, OrderPosition } from '@core/api-interfaces/lib/dtos/frontend';
import { TransportCategoryPoints } from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/responce/transportCategoryPoints';
import { CalculationRequest } from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/request/calculationRequest';
import { SingleCalculationRequestWrapper } from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/request/singleCalculationRequestWrapper';
import { SingleCalculationRequest } from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/request/singleCalculationRequest';
import { DangerousGoodsGatewayHttpService } from '@shared/httpRequests/dangerous-goods-gateway-http.service';

@Component({
  selector: 'app-enter-dangerous-goods-information',
  templateUrl: './enter-dangerous-goods-information.component.html',
  styleUrls: ['./enter-dangerous-goods-information.component.scss'],
})
export class EnterDangerousGoodsInformationComponent {
  @Input() freight: Freight;

  /**
   * Emitter used for providing the current value of the entered freight information to the parent component
   */
  @Output() freightEmitter: EventEmitter<Freight> = new EventEmitter<Freight>();

  /**
   * Emitter used for providing the current validity of the entered freight information to the parent component
   */
  @Output() allOrderPositionsAreValidEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  public isFirstOrderExpanded = false;

  /**
   * Variable used for storing the response of the calculated transport points and exemption status, so they can be
   * inputted into the order-position-input-form-components
   */
  public calculateTransportPointsAndExemptionStatusResponse: TransportCategoryPoints;

  /**
   * Variable used for storing the validity of all order-position-input-form-components
   * the first index represents the orderIndex
   * the second index represents the orderpositionIndex
   */
  public validityOfOrderPositionInputFormComponent: boolean[][] = [[]];

  constructor(private dangerousGoodsGatewayHttpService: DangerousGoodsGatewayHttpService) {}

  /**
   * Emits the freight, and if all orderPositions of all orders of that freight are validly entered to the parent component
   * If all orderPositionInputs are valid, the transportPoints and exemptionStatus are updated
   */
  public emitValidityAndFreightAndUpdateTransportPoints(): void {
    const allOrderPositionInputFormComponentsAreValid =
      this.allOrdersHaveOrderPositionsAndAllOrderPositionInputFormComponentsAreValid();
    this.allOrderPositionsAreValidEmitter.emit(allOrderPositionInputFormComponentsAreValid);
    if (allOrderPositionInputFormComponentsAreValid) {
      this.updateTransportPointsAndExemptionStatus();
      this.freightEmitter.emit(this.freight);
    }
  }

  /**
   * Checks if all orders have associated order positions and if all order position input form components are valid.
   *
   * @returns Returns `true` if every order has at least one order position and all order positions are valid, otherwise `false`.
   */
  public allOrdersHaveOrderPositionsAndAllOrderPositionInputFormComponentsAreValid(): boolean {
    return this.validityOfOrderPositionInputFormComponent.every(
      (validityOfOrder: boolean[]) =>
        validityOfOrder.length > 0 &&
        validityOfOrder.every((validityOfOrderPosition: boolean) => validityOfOrderPosition === true)
    );
  }

  /**
   * This method is used to update the validityOfOrderPositionInputFormComponent array when a consignee is added, because
   * ngOnChanges doesn't recognize changes in nested objects by itself
   */
  public addOrderValidityStorage(): void {
    this.validityOfOrderPositionInputFormComponent.push([]);
    this.emitValidityAndFreightAndUpdateTransportPoints();
  }

  /**
   * This method is used to update the validityOfOrderPositionInputFormComponent array when a consignee is removed, because
   * ngOnChanges doesn't recognize changes in nested objects by itself
   *
   * @param orderIndex - index of the order which was removed and needs its validityStorage removed
   */
  public removeOrderValidityStorage(orderIndex: number): void {
    this.validityOfOrderPositionInputFormComponent.splice(orderIndex, 1);
    this.emitValidityAndFreightAndUpdateTransportPoints();
  }

  /**
   * Updates the Validity of an orderPositionInputFormComponent. Triggered by its emitter
   *
   * @param orderIndex - the index of the order
   * @param orderPositionIndex - the index of the order-position
   * @param validityValue - a boolean indicating whether the form is filled out validly
   */
  public updateOrderPositionInputFormComponentsValidities(
    orderIndex: number,
    orderPositionIndex: number,
    validityValue: boolean
  ): void {
    this.validityOfOrderPositionInputFormComponent[orderIndex][orderPositionIndex] = validityValue;
    this.emitValidityAndFreightAndUpdateTransportPoints();
  }

  /**
   * Updates the value of the order-position without changing the reference pointer, so child components aren't reinitialized.
   *
   * @param orderIndex - the index of the order
   * @param orderPositionIndex - the index of the order-position
   * @param orderPosition - the new value of the order-position
   */
  public setOrderPositionOfOrder(orderIndex: number, orderPositionIndex: number, orderPosition: OrderPosition): void {
    // Object.assign is used instead of the = operator, because it would trigger the unnecessary reinitialization of child components, which caused problems.
    Object.assign(this.freight.orders[orderIndex].orderPositions[orderPositionIndex], orderPosition);
  }

  /**
   * Adds an order position for a given order.
   *
   * @param event - The event object.
   * @param orderIndex - The orders index.
   */
  public addOrderPosition(event: Event, orderIndex: number): void {
    this.isFirstOrderExpanded = true;
    event.stopPropagation();

    this.freight.orders[orderIndex].orderPositions.push(new OrderPosition());
    this.validityOfOrderPositionInputFormComponent[orderIndex].push(false);
    this.emitValidityAndFreightAndUpdateTransportPoints();
  }

  /**
   * Removes an order-position and its validity value.
   *
   * @param orderIndex - order index
   * @param orderPositionIndex - order-position index
   */
  public removeOrderPosition(orderIndex: number, orderPositionIndex: number): void {
    this.freight.orders[orderIndex].orderPositions.splice(orderPositionIndex, 1);
    this.validityOfOrderPositionInputFormComponent[orderIndex].splice(orderPositionIndex, 1);
    this.emitValidityAndFreightAndUpdateTransportPoints();
  }

  /**
   * Updates the TransportPoints and Exemption Status for the whole freight
   */
  public updateTransportPointsAndExemptionStatus(): void {
    this.dangerousGoodsGatewayHttpService
      .calculateTransportCategoryPoints(this.buildAdrCalculationRequestDto())
      .subscribe((transportCategoryPoints: TransportCategoryPoints): void => {
        this.calculateTransportPointsAndExemptionStatusResponse = transportCategoryPoints;
        this.freight.totalTransportPoints =
          this.calculateTransportPointsAndExemptionStatusResponse.totalTransportCategoryPoints;
        this.freight.additionalInformation = this.calculateTransportPointsAndExemptionStatusResponse.exemptionStatus;
      });
  }

  /**
   * Constructs a new `CalculationRequest` Dto for ADR calculations.
   *
   * @returns A new instance of `CalculationRequest` with all the mapped calculation requests.
   */
  private buildAdrCalculationRequestDto(): CalculationRequest {
    return new CalculationRequest(
      this.freight.orders.map(
        (order) =>
          new SingleCalculationRequestWrapper(
            order.orderPositions.map(
              (orderPosition) =>
                new SingleCalculationRequest(
                  orderPosition.dangerousGood.unNumber,
                  orderPosition.unit,
                  orderPosition.quantity,
                  orderPosition.individualAmount
                )
            )
          )
      )
    );
  }
}
