/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Enumeration for unit labels.
 */
export enum PackagingLabelUnitsEnum {
  valueKg = 'kg',
  valueLiter = 'L',
  displayedValueKg = 'Kilogramm',
  displayedValueLiter = 'Liter',
}
