/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { DangerousGood, OrderPosition } from '@core/api-interfaces/lib/dtos/frontend';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DangerousGoodsGatewayHttpService } from '@shared/httpRequests/dangerous-goods-gateway-http.service';
import {
  SelectPackagingDialogComponent
} from '@base/src/app/layouts/enter-dangerous-goods-information/dialogs/select-packaging-dialog/select-packaging-dialog.component';
import {
  Packaging
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingTypes/packaging';
import {
  TransportCategoryPointsResponse
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/responce/transportCategoryPointsResponse';
import {
  OrderPositionFormGroupGenerator
} from '@base/src/app/layouts/enter-dangerous-goods-information/component/order-position-input-form/services/order-position-form-group-generator';
import {
  PackagingLabelUnitsEnum
} from "@base/src/app/layouts/enter-dangerous-goods-information/component/order-position-input-form/enum/packagingLabelUnits.enum";
import {
  SelectPackagingLabelUnitElement
} from "@base/src/app/layouts/enter-dangerous-goods-information/component/order-position-input-form/model/selectPackagingLabelUnitElement";

@Component({
  selector: 'app-order-position-input-form',
  templateUrl: './order-position-input-form.component.html',
  styleUrls: ['./order-position-input-form.component.scss'],
})
export class OrderPositionInputFormComponent implements OnChanges {
  /**
   * Input property representing the index of the order position
   */
  @Input() orderPositionIndex: number;

  /**
   * Input property representing the transport points and exemption status of the order position
   */
  @Input() orderPositionTransportPointsAndExemptionStatus: TransportCategoryPointsResponse;

  /**
   * Emitter used for providing the current value of the entered order position information to the parent component
   */
  @Output() orderPositionEmitter: EventEmitter<OrderPosition> = new EventEmitter<OrderPosition>();

  /**
   * Emitter used for providing the current validity of the entered order position information to the parent component
   */
  @Output() orderPositionFormGroupIsValidEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Emitter used for telling the parent component that the user wants to remove the order position and the component
   * instance should therefore be destroyed
   */
  @Output() destroyMeEmitter: EventEmitter<void> = new EventEmitter<void>();

  /**
   * Represents a FormGroup for the order position
   */
  public orderPositionFormGroup: FormGroup = this.orderPositionFormGroupGenerator.buildEmptyOrderPositionFormGroup();

  /**
   * An array of dangerous goods fitting to the input string of the user. The array is used to provide autocomplete
   * options
   */
  public dangerousGoodsSearchFilterResults: DangerousGood[];

  /**
   * Used for saving the entered query in case the user accidentally selects the wrong dangerous good.
   */
  private dangerousGoodSearchQuery: string;

  public units: SelectPackagingLabelUnitElement[] = [
    new SelectPackagingLabelUnitElement(PackagingLabelUnitsEnum.valueKg, PackagingLabelUnitsEnum.displayedValueKg),
    new SelectPackagingLabelUnitElement(PackagingLabelUnitsEnum.valueLiter, PackagingLabelUnitsEnum.displayedValueLiter),
  ];

  constructor(
    public dialog: MatDialog,
    private dangerousGoodsGatewayHttpService: DangerousGoodsGatewayHttpService,
    private orderPositionFormGroupGenerator: OrderPositionFormGroupGenerator,
  ) {
    this.orderPositionFormGroup.valueChanges.subscribe(() => {
      // The raw value of the form group is needed, so that also the values of disabled form controls
      // (packaging and packaging description) will be included in the freight
      const orderPosition: OrderPosition = this.orderPositionFormGroup.getRawValue() as OrderPosition;
      this.orderPositionEmitter.emit(orderPosition);
      this.orderPositionFormGroupIsValidEmitter.emit(this.orderPositionFormGroup.valid);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.orderPositionTransportPointsAndExemptionStatus !== undefined &&
      changes.orderPositionTransportPointsAndExemptionStatus.currentValue !== null &&
      JSON.stringify(changes.orderPositionTransportPointsAndExemptionStatus.currentValue) !==
        JSON.stringify(changes.orderPositionTransportPointsAndExemptionStatus.previousValue)
    ) {
      this.orderPositionFormGroup.patchValue({
        totalAmount: this.orderPositionTransportPointsAndExemptionStatus.totalAmount,
        exemptionStatus: this.orderPositionTransportPointsAndExemptionStatus.exemptionStatus,
        transportPoints: this.orderPositionTransportPointsAndExemptionStatus.transportCategoryPoints,
      });
    }
  }

  /**
   * Emits to the parent component that the user once to remove the order position.
   * The parent component will then proceed to destroy this component.
   */
  public removeOrderPosition(): void {
    this.destroyMeEmitter.emit();
  }

  /**
   * Filters Dangerous goods based on the value of the 'dangerousGood' form control in an order position form group.
   *
   * @param orderPositionFormGroup - The form group of the order position.
   */
  public updateDangerousGoodSearchFilterResults(orderPositionFormGroup: FormGroup): void {
    this.dangerousGoodSearchQuery = orderPositionFormGroup?.get('dangerousGood').value as string;
    if (this.dangerousGoodSearchQuery) {
      this.dangerousGoodsGatewayHttpService
        .lookupUnNumber(this.dangerousGoodSearchQuery)
        .subscribe((goods: DangerousGood[]): void => {
          this.dangerousGoodsSearchFilterResults = goods;
        });
    }
  }

  /**
   * Opens the select packaging dialog and sets the selected package as the formControls value
   */
  public onShowPackagingDialog(): void {
    const dialogRef: MatDialogRef<SelectPackagingDialogComponent> = this.dialog.open(SelectPackagingDialogComponent, {
      width: '90%',
      height: '80vh',
    });
    dialogRef.afterClosed().subscribe((packaging: Packaging): void => {
      if (packaging) {
        this.updatePackagingFormControls(packaging);
      }
    });
  }

  /**
   * Updates the dangerous good.
   *
   * @param dangerousGood - The new dangerous good.
   */
  public onSelectDangerousGood(dangerousGood: DangerousGood): void {
    this.orderPositionFormGroupGenerator.setDangerousGoodHasBeenPicked(true);
    this.orderPositionFormGroup.get('dangerousGood').setValue(dangerousGood);
  }

  /**
   * Updates the packaging formControls.
   *
   * @param packaging - The newly selected packaging.
   */
  private updatePackagingFormControls(packaging: Packaging): void {
    this.orderPositionFormGroupGenerator.setPackagingHasBeenPicked(true);
    this.orderPositionFormGroup.patchValue({
      package: `${packaging.kind} aus ${packaging.material} (${packaging.typesOfMaterial}) ${packaging.category}`,
      packagingCode: packaging.code,
    });
  }

  /**
   * Resets the dangerousGood FormControl back to its status before the autocomplete option was selected
   */
  public unselectDangerousGood(): void {
    this.orderPositionFormGroupGenerator.setDangerousGoodHasBeenPicked(false);
    this.orderPositionFormGroup.get('dangerousGood').setValue(this.dangerousGoodSearchQuery);
  }
}
