/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DangerousGood } from '@core/api-interfaces/lib/dtos/frontend';

@Component({
  selector: 'app-selected-dangerous-good-preview',
  templateUrl: './selected-dangerous-good-preview.component.html',
  styleUrls: ['./selected-dangerous-good-preview.component.css'],
})
export class SelectedDangerousGoodPreviewComponent {
  @Input() selectedDangerousGood: DangerousGood;
  @Output() unselectDangerousGoodEmitter: EventEmitter<void> = new EventEmitter<void>();

  /**
   * Returns a formatted information of the selected dangerous good
   *
   * @param dangerousGood - The dangerous good to be previewed
   * @returns string formatted information
   */
  public getFormattedDangerousGoodDescription(dangerousGood: DangerousGood | null): string {
    if (!dangerousGood || !dangerousGood.label1) {
      return '';
    } else {
      return `${dangerousGood.unNumber} ${dangerousGood.description}, ${this.getFormattedLabelText(dangerousGood)}, ${
        dangerousGood.packingGroup
      }, ${dangerousGood.tunnelRestrictionCode}`;
    }
  }

  /**
   * Returns a formatted output of the labels of a dangerous good
   *
   * @param dangerousGood - The dangerous good giving information about present labels
   * @returns string formatted label text
   */
  public getFormattedLabelText(dangerousGood: DangerousGood | null): string {
    if (!dangerousGood || !dangerousGood.label1) {
      return '';
    } else if (dangerousGood.label3) {
      return `${dangerousGood.label1} (${dangerousGood.label2} + ${dangerousGood.label3})`;
    } else if (dangerousGood.label2) {
      return `${dangerousGood.label1} (${dangerousGood.label2})`;
    } else {
      return dangerousGood.label1;
    }
  }
}
