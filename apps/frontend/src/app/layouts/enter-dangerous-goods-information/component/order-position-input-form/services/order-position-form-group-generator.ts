/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable()
export class OrderPositionFormGroupGenerator {
  /**
   * Stores information about whether a packaging has been picked yet or not.
   * It is needed because the formControl only checks whether it holds a value, not if that value is a dangerous good.
   */
  private dangerousGoodHasBeenPicked = false;

  /**
   * Stores information about whether a packaging has been picked yet or not.
   * It is needed because disabled formControls (which are used for packaging information) are always valid.
   */
  private packagingHasBeenPicked = false;

  constructor(private formBuilder: FormBuilder) {}

  /**
   * Builds and returns a FormGroup for an empty order position.
   *
   * @returns FormGroup
   */
  public buildEmptyOrderPositionFormGroup(): FormGroup {
    return this.formBuilder.group({
      dangerousGood: [null, [Validators.required, this.dangerousGoodHasBeenPickedValidator()]],
      package: [null, [Validators.required, this.packagingHasBeenPickedValidator()]],
      packagingCode: [null, [Validators.required, this.packagingHasBeenPickedValidator()]],
      quantity: [null, [Validators.required, Validators.min(1)]],
      unit: [null, [Validators.required]],
      individualAmount: [null, [Validators.required, Validators.min(1)]],
      polluting: [false],
      transportPoints: [0, [Validators.required, Validators.min(0)]],
      totalAmount: [null, [Validators.min(0)]],
      exemptionStatus: [''],
    });
  }

  /**
   * Sets the status of whether a dangerous good has been picked.
   *
   * @param dangerousGoodHasBeenPicked -
   */
  public setDangerousGoodHasBeenPicked(dangerousGoodHasBeenPicked: boolean): void {
    this.dangerousGoodHasBeenPicked = dangerousGoodHasBeenPicked;
  }

  /**
   * Sets the status of whether packaging has been picked.
   *
   * @param packagingHasBeenPicked - Packaging to be set as picked.
   */
  public setPackagingHasBeenPicked(packagingHasBeenPicked: boolean): void {
    this.packagingHasBeenPicked = packagingHasBeenPicked;
  }

  /**
   * Validator function to check if a dangerous good has been picked.
   *
   * @returns Result of the validation.
   */
  private dangerousGoodHasBeenPickedValidator(): (() => { [key: string]: boolean } | null) {
    return () => {
      return this.dangerousGoodHasBeenPicked
        ? null
        : { dangerousGoodHasNotBeenPicked: true };
    };
  }

  /**
   * Validator function to check if packaging has been picked.
   *
   * @returns Result of the validation.
   */
  private packagingHasBeenPickedValidator(): (() => { [key: string]: boolean } | null) {
    return () => {
      return this.packagingHasBeenPicked
        ? null
        : { packagingHasNotBeenPicked: true };
    };
  }
}
