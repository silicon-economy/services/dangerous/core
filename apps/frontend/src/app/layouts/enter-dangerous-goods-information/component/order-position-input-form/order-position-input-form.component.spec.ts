/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderPositionInputFormComponent } from './order-position-input-form.component';
import { MatDialogModule, MatDialogRef } from "@angular/material/dialog";
import { DangerousGoodsGatewayHttpService } from "@shared/httpRequests/dangerous-goods-gateway-http.service";
import { HttpClient, HttpHandler } from "@angular/common/http";
import { FormControl, FormGroup, ReactiveFormsModule } from "@angular/forms";
import {
  SelectPackagingDialogComponent
} from "@base/src/app/layouts/enter-dangerous-goods-information/dialogs/select-packaging-dialog/select-packaging-dialog.component";
import { TranslateModule } from "@ngx-translate/core";
import { SimpleChange, SimpleChanges } from "@angular/core";
import {
  OrderPositionFormGroupGenerator
} from "@base/src/app/layouts/enter-dangerous-goods-information/component/order-position-input-form/services/order-position-form-group-generator";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import {
  TransportCategoryPointsResponse
} from "@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/responce/transportCategoryPointsResponse";
import { ExemptionStatus } from "@core/api-interfaces";
import {
  Packaging
} from "@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingTypes/packaging";
import { of } from "rxjs";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatSelectModule } from "@angular/material/select";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { MatCardModule } from "@angular/material/card";
import { MatIconModule } from "@angular/material/icon";
import { mockDangerousGood } from "@core/api-interfaces/lib/mocks/dangerous-good/dangerous-good.mock";

describe('PositionInputFormComponent', () => {
  let component: OrderPositionInputFormComponent;
  let fixture: ComponentFixture<OrderPositionInputFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderPositionInputFormComponent, SelectPackagingDialogComponent],
      imports: [
        MatDialogModule,
        TranslateModule.forRoot(),
        MatAutocompleteModule,
        ReactiveFormsModule,
        MatCheckboxModule,
        MatSelectModule,
        MatFormFieldModule,
        MatInputModule,
        NoopAnimationsModule,
        MatCardModule,
        MatIconModule
      ],
      providers: [
        DangerousGoodsGatewayHttpService,
        HttpClient,
        HttpHandler,
        OrderPositionFormGroupGenerator,
      ],
    });
    fixture = TestBed.createComponent(OrderPositionInputFormComponent);
    component = fixture.componentInstance;
    component.orderPositionIndex = 0;
    component.orderPositionTransportPointsAndExemptionStatus = new TransportCategoryPointsResponse()
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should filter dangerous goods', (): void => {
    const mockFormGroup = new FormGroup({
        'dangerousGood': new FormControl('DangerousGood')
      });
    mockFormGroup.get('dangerousGood').setValue('');

    const dangerousGoodsService = jest.spyOn(component, 'updateDangerousGoodSearchFilterResults');
    component.updateDangerousGoodSearchFilterResults(mockFormGroup);
    expect(dangerousGoodsService).toHaveBeenCalled();
    expect(component.dangerousGoodsSearchFilterResults).toBeUndefined();
  });

  it('should update exemption points', () =>{
    const changes: SimpleChanges = {
      orderPositionTransportPointsAndExemptionStatus: new SimpleChange(component.orderPositionTransportPointsAndExemptionStatus, {unNumber: '1090',
        totalAmount: 1,
        transportCategoryPoints: 1,
        exemptionStatus: ExemptionStatus.transportWith1kp} as TransportCategoryPointsResponse, false),
    };
    jest.spyOn(component.orderPositionFormGroup, 'patchValue')
    component.ngOnChanges(changes);
    expect(component.orderPositionFormGroup.patchValue).toHaveBeenCalled()
  })

  it('should remove the orderPositionInputFormComponent', ()=>{
    jest.spyOn(component.destroyMeEmitter, 'emit')
    component.removeOrderPosition();
    expect(component.destroyMeEmitter.emit).toHaveBeenCalled()
  })

  it('should react to the results of the selectPackagingDialog', ()=>{
    jest.spyOn(component.dialog, 'open').mockReturnValue({
        afterClosed: () => of(new Packaging())
      } as MatDialogRef<SelectPackagingDialogComponent>);
    component.onShowPackagingDialog()
  })

  it('should test the selection and unSelection of dangerous goods', ()=>{
    const setDangerousGoodHasBeenPickedSpy = jest.spyOn(
      component['orderPositionFormGroupGenerator'], 'setDangerousGoodHasBeenPicked'
    );

    component.onSelectDangerousGood(mockDangerousGood);
    expect(setDangerousGoodHasBeenPickedSpy).toHaveBeenCalledWith(true);
    expect(component.orderPositionFormGroup.get('dangerousGood').value).toBe(mockDangerousGood)

    component.unselectDangerousGood();
    expect(setDangerousGoodHasBeenPickedSpy).toHaveBeenCalledWith(false);
    expect(component.orderPositionFormGroup.get('dangerousGood').value).not.toBe(mockDangerousGood)
  })
});
