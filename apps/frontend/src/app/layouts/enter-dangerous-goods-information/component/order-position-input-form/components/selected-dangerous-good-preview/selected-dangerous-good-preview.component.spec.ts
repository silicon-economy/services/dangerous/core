/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedDangerousGoodPreviewComponent } from './selected-dangerous-good-preview.component';
import { DangerousGoodLabelModule } from '@base/src/app/layouts/dangerous-good-label/dangerous-good-label.module';
import { MatIconModule } from '@angular/material/icon';
import { mockDangerousGood } from '@core/api-interfaces/lib/mocks/dangerous-good/dangerous-good.mock';

describe('SelectedDangerousGoodPreviewComponent', () => {
  let component: SelectedDangerousGoodPreviewComponent;
  let fixture: ComponentFixture<SelectedDangerousGoodPreviewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SelectedDangerousGoodPreviewComponent],
      imports: [DangerousGoodLabelModule, MatIconModule],
    });
    fixture = TestBed.createComponent(SelectedDangerousGoodPreviewComponent);
    component = fixture.componentInstance;
    component.selectedDangerousGood = mockDangerousGood;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the FormattedDangerousGoodDescription', () => {
    component.getFormattedDangerousGoodDescription(component.selectedDangerousGood);
    component.getFormattedLabelText(component.selectedDangerousGood);
  });
});
