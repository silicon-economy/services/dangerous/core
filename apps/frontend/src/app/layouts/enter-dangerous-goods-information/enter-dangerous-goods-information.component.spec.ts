/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Order } from '@core/api-interfaces/lib/dtos/frontend';

import { EnterDangerousGoodsInformationComponent } from './enter-dangerous-goods-information.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import {
  OrderPositionFormGroupGenerator
} from '@base/src/app/layouts/enter-dangerous-goods-information/component/order-position-input-form/services/order-position-form-group-generator';
import { DangerousGoodsGatewayHttpService } from '@shared/httpRequests/dangerous-goods-gateway-http.service';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import {
  mockTransportDocumentCreated
} from "@core/api-interfaces/lib/mocks/transport-document/transport-document.mock";

describe('EnterDangerousGoodsInformationComponent', (): void => {
  let component: EnterDangerousGoodsInformationComponent;
  let fixture: ComponentFixture<EnterDangerousGoodsInformationComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EnterDangerousGoodsInformationComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        MatCheckboxModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatExpansionModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        MatDialogModule,
        HttpClientTestingModule
      ],
      providers: [OrderPositionFormGroupGenerator, DangerousGoodsGatewayHttpService],
    }).compileComponents();

    fixture = TestBed.createComponent(EnterDangerousGoodsInformationComponent);
    component = fixture.componentInstance;
    component.freight = mockTransportDocumentCreated.freight;
    component.validityOfOrderPositionInputFormComponent = component.freight.orders.map((order) =>
      order.orderPositions.map(() => true)
    );
    fixture.detectChanges();
  });

  it('should create', (): void => {
    expect(component).toBeTruthy();
  });

  it('should emit the current freight', () => {
    jest.spyOn(component.freightEmitter, 'emit');
    component.emitValidityAndFreightAndUpdateTransportPoints();
    fixture.detectChanges();

    expect(component.freightEmitter.emit).toHaveBeenCalled();
  });

  it('should add an order and its orderValidityStorage, then update it and then remove it', () => {
    // the default state of the mock transport document is valid
    expect(component.allOrdersHaveOrderPositionsAndAllOrderPositionInputFormComponentsAreValid()).toBe(true)

    // a new order is added, that isn't valid yet, so the validity should be false
    component.freight.orders.push(new Order(undefined,[]))
    component.addOrderValidityStorage();
    const orderIndex = component.freight.orders.length-1;

    const clickEvent = new MouseEvent('click');
    Object.assign(clickEvent, {stopPropagation: jest.fn()});
    component.addOrderPosition(clickEvent, orderIndex)
    expect(component.allOrdersHaveOrderPositionsAndAllOrderPositionInputFormComponentsAreValid()).toBe(false)


    // change the order-position to a valid one and update its validity
    component.setOrderPositionOfOrder(orderIndex, 0, mockTransportDocumentCreated.freight.orders[0].orderPositions[0])
    component.updateOrderPositionInputFormComponentsValidities(orderIndex, 0, true)
    expect(component.allOrdersHaveOrderPositionsAndAllOrderPositionInputFormComponentsAreValid()).toBe(true)

    // the invalid order is getting removed again, so that only the valid orders remain
    component.removeOrderPosition(orderIndex,0)
    component.freight.orders.splice(orderIndex)
    component.removeOrderValidityStorage(orderIndex)
    expect(component.allOrdersHaveOrderPositionsAndAllOrderPositionInputFormComponentsAreValid()).toBe(true)
  });

  it('should updateTransportPointsAndExemptionStatus', ()=> {
    component.updateTransportPointsAndExemptionStatus();
  });

});
