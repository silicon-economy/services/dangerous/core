/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPackagingDialogComponent } from './select-packaging-dialog.component';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { DangerousGoodsGatewayHttpService } from "@shared/httpRequests/dangerous-goods-gateway-http.service";
import { TranslateModule } from "@ngx-translate/core";
import {
  Packaging
} from "@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingTypes/packaging";
import {
  PackagingFilter
} from "@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingFilter";
import { of } from "rxjs";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { mockPackaging } from "@core/api-interfaces/lib/mocks/packaging/packaging.mock";

describe('PackagingDialogComponent', (): void => {
  let component: SelectPackagingDialogComponent;
  let fixture: ComponentFixture<SelectPackagingDialogComponent>;

  const packagingFilter: PackagingFilter = {
    query: ''
  };

  beforeEach((): void => {
    TestBed.configureTestingModule({
      declarations: [SelectPackagingDialogComponent],
      providers: [
        { provide: MatDialogRef, useValue: {close: jest.fn()} },
        DangerousGoodsGatewayHttpService
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ],
      imports: [
        TranslateModule.forRoot(),
        MatDialogModule,
        HttpClientTestingModule
      ]
    });
    fixture = TestBed.createComponent(SelectPackagingDialogComponent);
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', (): void => {
    expect(component).toBeTruthy();
  });

  it('should not call updatePackaging() correctly', (): void => {
    jest.spyOn(component, 'onSelectPackaging');
    expect(component.onSelectPackaging).not.toHaveBeenCalled();
  });

  it('should update packaging correctly when updatePackaging is called', (): void => {
    const mockFilterPackagingResponse: Packaging[] = [mockPackaging];
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
    jest.spyOn(component.dangerousGoodsGatewayHttpService, 'getPackagingDetails').mockReturnValueOnce(of(mockFilterPackagingResponse));
    component.updatePackaging(packagingFilter);
    expect(component.packagingFilterResults).toEqual(mockFilterPackagingResponse);
  });

  it('should close dialog when a package is selected', ()=> {
    jest.spyOn(component.dialogRef, 'close')
    component.onSelectPackaging(mockPackaging)
    expect(component.dialogRef.close).toHaveBeenCalled()
  })
});
