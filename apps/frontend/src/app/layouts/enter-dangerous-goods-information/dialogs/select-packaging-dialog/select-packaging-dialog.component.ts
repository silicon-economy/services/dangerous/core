/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import {
  Packaging
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingTypes/packaging';
import {
  PACKAGING_KINDS
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingTypes/packaging-kind.type';
import {
  MATERIALS
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingTypes/packaging-material.type';
import { DangerousGoodsGatewayHttpService } from '@shared/httpRequests/dangerous-goods-gateway-http.service';
import {
  PackagingFilter
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingFilter';
import {
  PackagingFormGroup
} from '@base/src/app/layouts/enter-dangerous-goods-information/dialogs/select-packaging-dialog/interfaces/packaging-form-group.interface';

@Component({
  selector: 'app-select-packaging-dialog',
  templateUrl: './select-packaging-dialog.component.html',
})
export class SelectPackagingDialogComponent implements OnInit {
  public packagingKindsFilterOptions = ['Alle'].concat(PACKAGING_KINDS);
  public materialsFilterOptions = ['Alle'].concat(MATERIALS);
  public packagingFilterResults: Packaging[] = [];
  public formGroupOfPackaging: FormGroup<PackagingFormGroup> = new FormGroup<PackagingFormGroup>({
    query: new FormControl('', { nonNullable: true }),
    kind: new FormControl(this.packagingKindsFilterOptions[0]),
    material: new FormControl(this.materialsFilterOptions[0]),
  });

  constructor(
    public dialogRef: MatDialogRef<SelectPackagingDialogComponent>,
    public dangerousGoodsGatewayHttpService: DangerousGoodsGatewayHttpService
  ) {
    this.formGroupOfPackaging.valueChanges.subscribe((res: PackagingFilter) => this.updatePackaging(res));
  }

  public ngOnInit(): void {
    this.updatePackaging(this.formGroupOfPackaging.getRawValue());
  }

  /**
   * Closes the dialog and returns the selected packaging.
   *
   * @param packaging - The selected packaging.
   */
  public onSelectPackaging(packaging: Packaging): void {
    this.dialogRef.close(packaging);
  }

  /**
   * Updates the packaging information and change html if packaging is empty or not
   *
   * @param packagingFilter - The filter to be applied
   */
  public updatePackaging(packagingFilter: PackagingFilter): void {
    if (packagingFilter.kind === 'Alle') {
      packagingFilter.kind = undefined;
    }
    if (packagingFilter.material === 'Alle') {
      packagingFilter.material = undefined;
    }
    this.dangerousGoodsGatewayHttpService
      .getPackagingDetails(packagingFilter)
      .subscribe((packagingFilterResults: Packaging[]) => (this.packagingFilterResults = packagingFilterResults));
  }
}
