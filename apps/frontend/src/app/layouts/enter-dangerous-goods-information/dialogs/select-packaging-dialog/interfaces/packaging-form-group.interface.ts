/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { FormControl } from '@angular/forms';
import {
  PackagingKind
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingTypes/packaging-kind.type';
import {
  PackagingMaterial
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingTypes/packaging-material.type';
import {
  PackagingMaterialType
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingTypes/packaging-material-type.type';

export interface PackagingFormGroup {
  query: FormControl<string>;
  kind?: FormControl<PackagingKind>;
  material?: FormControl<PackagingMaterial>;
  typesOfMaterial?: FormControl<PackagingMaterialType>;
  category?: FormControl<string>;
}
