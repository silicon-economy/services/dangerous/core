/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from '@environments/environment';
import { LatLng } from 'leaflet';
import { NominatimService } from './nominatim.service';
import { mockCompany } from "@core/api-interfaces/lib/mocks/company/company.mock";

describe('NominatimService', () => {
  let service: NominatimService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(NominatimService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should not emit a value if none were found', () => {
    service.getLatLong(mockCompany).subscribe(() => {
      // Expect this to be never called
      expect(false).toBe(true);
    });
    httpMock.expectOne(`${environment.apiEndpointNominatim}&street=${mockCompany.address.number} ${mockCompany.address.street}&city=${mockCompany.address.city}&country=${mockCompany.address.country}&postalCode=${mockCompany.address.postalCode}`).flush([]);
    expect(true).toEqual(true);
  });

  it('should return the first result', () => {
    service.getLatLong(mockCompany).subscribe((res: LatLng) => {
      expect(res.lat).toEqual(2);
      expect(res.lng).toEqual(3);
    });
    httpMock.expectOne(`${environment.apiEndpointNominatim}&street=${mockCompany.address.number} ${mockCompany.address.street}&city=${mockCompany.address.city}&country=${mockCompany.address.country}&postalCode=${mockCompany.address.postalCode}`).flush([
      {
        lat: 2,
        lon: 3,
      },
      {
        lat: 1,
        lon: 4,
      },
    ]);
  });
});
