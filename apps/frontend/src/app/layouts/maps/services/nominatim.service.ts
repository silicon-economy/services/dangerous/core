/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { LatLng } from 'leaflet';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Company } from '@core/api-interfaces/lib/dtos/frontend';

/**
 * Access a Nominatim server to map a given address to geolocation coordinates.
 */
@Injectable({
  providedIn: 'root',
})
export class NominatimService {
  constructor(private http: HttpClient) {}

  /**
   * Fetches coordinates
   *
   * @param company - The company
   * @returns Observable with latitude/longitude
   */
  getLatLong(company: Company): Observable<LatLng> {
    return this.http
      .get<string[]>(
        `${environment.apiEndpointNominatim}&street=${company?.address?.number} ${company?.address?.street}&city=${company?.address?.city}&country=${company?.address?.country}&postalCode=${company?.address?.postalCode}`
      )
      .pipe(
        filter((res: string[]) => res && res.length > 0),
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        map((nominatimResult: any[]) => {
          // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-member-access
          return new LatLng(nominatimResult[0].lat, nominatimResult[0].lon, 0);
        })
      );
  }
}
