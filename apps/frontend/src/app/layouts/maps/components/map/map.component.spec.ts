/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NominatimService } from '@maps/services/nominatim.service';
import { LatLng } from 'leaflet';
import { of } from 'rxjs';
import { MapComponent } from './map.component';
import {
  mockTransportDocumentCreated
} from "@core/api-interfaces/lib/mocks/transport-document/transport-document.mock";

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MapComponent],
      imports: [HttpClientTestingModule, LeafletModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add markers', inject([NominatimService], (service: NominatimService) => {
    const serviceSpy = jest.spyOn(service, 'getLatLong').mockReturnValue(of(new LatLng(0, 0, 0)));
    component.document = mockTransportDocumentCreated;
    expect(component.markers.length).toEqual(23);
    mockTransportDocumentCreated.freight.orders = [];
    component.document = mockTransportDocumentCreated;
    expect(serviceSpy).toHaveBeenCalledTimes(3);
    expect(component.markers.length).toEqual(22);
  }));
});
