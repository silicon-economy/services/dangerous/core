/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnDestroy } from '@angular/core';
import { environment } from '@environments/environment';
import { MarkerConfig } from '@maps/interfaces/marker-config.interface';
import { Tunnel } from '@maps/interfaces/tunnel.interface';
import { NominatimService } from '@maps/services/nominatim.service';
import { MAP_ICON_SIZE, MAP_ICON_SIZE_TUNNEL, MAP_X_ANCHOR } from '@maps/utils/map.utils';
import { tunnels } from '@maps/utils/tunnel.utils';
import { Circle, icon, latLng, LatLng, marker, Marker, tileLayer } from 'leaflet';
import { Subscription } from 'rxjs';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { Order } from '@core/api-interfaces/lib/dtos/frontend';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-dangerous-good-registration.dto';

/**
 * Display a map and set markers for restricted tunnels and relevant transport destinations and origins.
 */
@Component({
  selector: 'app-map[document]',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnDestroy {
  @Input()
  public set document(document: SaveGoodsDataDto | SaveDangerousGoodRegistrationDto) {
    this.markers = [];
    this.subscriptions.push(
      this.nominatimService.getLatLong(document.consignor).subscribe((coordinates: LatLng) => {
        this.addMarker({
          coordinates,
          icon: 'tour_primary',
          title: `Absender: ${document.consignor.name}`,
        });
      })
    );
    document.freight.orders.forEach((order: Order) => {
      if (order.consignee) {
        this.subscriptions.push(
          this.nominatimService.getLatLong(order.consignee).subscribe((coordinates: LatLng) => {
            this.addMarker({
              coordinates,
              icon: 'tour_accent',
              title: `Empfänger: ${order.consignee.name}`,
            });
          })
        );
      }
    });
    this.addTunnels();
  }

  constructor(private readonly nominatimService: NominatimService) {}

  private subscriptions: Subscription[] = [];
  public height = '100%';
  public markers: (Marker | Circle)[] = [];
  public center: LatLng = latLng(51.49401, 7.407107);

  public options = {
    layers: [
      tileLayer(`${environment.apiEndpointTiles}{z}/{x}/{y}.png`, {
        maxZoom: 18,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      }),
    ],
    zoom: 9,
    center: this.center,
  };

  /**
   * Unsubscribe all subscriptions
   */
  ngOnDestroy(): void {
    this.subscriptions.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }

  /**
   * Adds a marker to the map
   *
   * @param config - Marker config
   */
  private addMarker(config: MarkerConfig): void {
    const size = config.size ?? MAP_ICON_SIZE;
    const halfSize = Math.floor(size / 2);
    const newMarker = marker([config.coordinates.lat, config.coordinates.lng], {
      icon: icon({
        iconSize: [size, size],
        iconAnchor: [MAP_X_ANCHOR, size],
        popupAnchor: [halfSize - MAP_X_ANCHOR, -1 * size],
        iconUrl: `assets/map/${config.icon}.png`,
        className: config.icon,
        shadowUrl: config.hideShadow ? undefined : 'leaflet/marker-shadow.png',
      }),
    });
    if (config.title) {
      newMarker.bindPopup(config.title);
    }
    this.markers = [...this.markers, newMarker];
  }

  /**
   * Add markers for all possibly restricted tunnels
   */
  private addTunnels(): void {
    tunnels.forEach((tunnel: Tunnel) => {
      this.addMarker({
        coordinates: new LatLng(tunnel.lat, tunnel.long, 0),
        title: `${tunnel.code}: ${tunnel.title}`,
        size: MAP_ICON_SIZE_TUNNEL,
        hideShadow: true,
        icon: 'cancel',
      });
    });
  }
}
