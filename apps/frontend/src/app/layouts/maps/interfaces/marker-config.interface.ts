/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { MapIcon } from '@maps/types/map-icon.type';
import { LatLng } from 'leaflet';

/**
 * Configuration for map markers.
 */
export interface MarkerConfig {
  coordinates: LatLng;
  icon: MapIcon;
  title?: string;
  size?: number;
  hideShadow?: boolean;
}
