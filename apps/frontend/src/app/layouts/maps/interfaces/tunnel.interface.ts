/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  TunnelRestrictionCode
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

/**
 * Model for Tunnel.
 */
export interface Tunnel {
  lat: number;
  long: number;
  title: string;
  code: TunnelRestrictionCode;
}
