/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Tunnel } from '@maps/interfaces/tunnel.interface';
import {
  TunnelRestrictionCode
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

export const tunnels: Tunnel[] = [
  {
    lat: 49.5733,
    long: 8.6848,
    title: 'B 38 - Saukopftunnel',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 48.6897,
    long: 9.2183,
    title: 'B 312 - Bereich Flughafen Stuttgart',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 49.4091,
    long: 8.7074,
    title: 'Gemeindestraße - Schlossbergtunnel',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 48.39431,
    long: 9.98028,
    title: 'B10 - "Westringtunnel" Ulm',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 48.7807,
    long: 9.1885,
    title: 'Wagenburgtunnel',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 48.36674,
    long: 10.88389,
    title: 'Pferseer Unterführung',
    code: TunnelRestrictionCode['(B)'],
  },
  {
    lat: 49.7535,
    long: 9.9464,
    title: 'BAB A3',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 52.4752,
    long: 13.3068,
    title: 'BAB A 100 (AS Schmargendorf)',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 53.55176,
    long: 10.00588,
    title: 'Wallringtunnel',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 53.62364,
    long: 10.01107,
    title: 'Tunnel Alsterkrugchaussee',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 53.54398,
    long: 9.91272,
    title: 'A7- Elbtunnel',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 53.64802,
    long: 9.98192,
    title: 'Krohnstiegtunnel',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 53.2335,
    long: 7.4044,
    title: 'A31 - Emstunnel',
    code: TunnelRestrictionCode['(B)'],
  },
  {
    lat: 50.68679235810078,
    long: 7.154232280835804,
    title: 'B 9 - Tunnel Bad Godesberg',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 52.2527,
    long: 8.9183,
    title: 'B 61n - Streckenabschnitt 99.1 Weserauentunnel',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 50.94194,
    long: 6.96027,
    title: 'Am Bahndamm, Verlängerung Trankgasse zum Konrad-Adenauer-Ufer',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 53.901,
    long: 10.7743,
    title: 'B 104 - Herrentunnel',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 50.7167,
    long: 10.8059,
    title: 'A 71 - Tunnel Alte Burg',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 50.6952,
    long: 10.7355,
    title: 'A 71 - Tunnel Rennsteig',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 50.6586,
    long: 10.69,
    title: 'A 71 - Tunnel Hochwald',
    code: TunnelRestrictionCode['(E)'],
  },
  {
    lat: 50.63056,
    long: 10.67007,
    title: 'A 71 - Tunnel Berg Bock',
    code: TunnelRestrictionCode['(E)'],
  },
];
