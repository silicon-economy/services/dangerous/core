/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export const MAP_ICON_SIZE = 42;
export const MAP_ICON_SIZE_TUNNEL = 32;
export const MAP_X_ANCHOR = 10;
