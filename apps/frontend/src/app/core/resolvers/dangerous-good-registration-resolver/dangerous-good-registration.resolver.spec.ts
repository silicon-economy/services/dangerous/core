/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, ResolveFn } from '@angular/router';

import { dangerousGoodRegistrationResolver } from './dangerous-good-registration.resolver';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-dangerous-good-registration.dto';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';

describe('dangerousGoodRegistrationResolver', () => {
  let dangerousGoodRegistrationHttpService: DangerousGoodRegistrationHttpService;
  let route: ActivatedRoute;

  const executeResolver: ResolveFn<SaveDangerousGoodRegistrationDto> = (...resolverParameters) =>
    TestBed.runInInjectionContext(() => dangerousGoodRegistrationResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        DangerousGoodRegistrationHttpService,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({
                id: '1',
              }),
            },
          },
        },
      ],
    });
    dangerousGoodRegistrationHttpService = TestBed.inject(DangerousGoodRegistrationHttpService);
    route = TestBed.inject(ActivatedRoute);
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });

  it('should call http service', async () => {
    const getDangerousGoodRegistrationSpy = jest
      .spyOn(dangerousGoodRegistrationHttpService, 'getDangerousGoodRegistration')
      .mockReturnValue(of());
    await executeResolver(route.snapshot, undefined);
    expect(getDangerousGoodRegistrationSpy).toHaveBeenCalled();
    expect(getDangerousGoodRegistrationSpy).toHaveBeenCalledWith('1');
  });
});
