/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, ResolveFn } from '@angular/router';

import { transportDocumentResolver } from './transport-document.resolver';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { of } from 'rxjs';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('transportDocumentResolver', () => {
  let transportDocumentHttpService: TransportDocumentHttpService;
  let route: ActivatedRoute;

  const executeResolver: ResolveFn<SaveGoodsDataDto> = (...resolverParameters) =>
    TestBed.runInInjectionContext(() => transportDocumentResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        TransportDocumentHttpService,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({
                id: '1',
              }),
            },
          },
        },
      ],
    });
    transportDocumentHttpService = TestBed.inject(TransportDocumentHttpService);
    route = TestBed.inject(ActivatedRoute);
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });

  it('should call http service', async () => {
    const getTransportDocumentSpy = jest
      .spyOn(transportDocumentHttpService, 'getTransportDocument')
      .mockReturnValue(of());
    await executeResolver(route.snapshot, undefined);
    expect(getTransportDocumentSpy).toHaveBeenCalled();
    expect(getTransportDocumentSpy).toHaveBeenCalledWith('1');
  });
});
