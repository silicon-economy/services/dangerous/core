/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';
import {
  dangerousGoodRegistrationListResolver
} from '@core/resolvers/dangerous-good-registration-list-resolver/transport-document-list.resolver';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('transportDocumentListResolver', () => {
  let dangerousGoodRegistrationHttpService: DangerousGoodRegistrationHttpService;
  const executeResolver: ResolveFn<SaveDangerousGoodRegistrationDto[]> = (...resolverParameters) =>
    TestBed.runInInjectionContext(() => dangerousGoodRegistrationListResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DangerousGoodRegistrationHttpService],
    });
    dangerousGoodRegistrationHttpService = TestBed.inject(DangerousGoodRegistrationHttpService);
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });

  it('should call http service', async () => {
    const getAllDangerousGoodRegistrationsSpy = jest
      .spyOn(dangerousGoodRegistrationHttpService, 'getAllDangerousGoodRegistrations')
      .mockReturnValue(of());
    await executeResolver(undefined, undefined);
    expect(getAllDangerousGoodRegistrationsSpy).toHaveBeenCalled();
  });
});
