/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot } from '@angular/router';
import { inject } from '@angular/core';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';

/**
 * Resolves the list of dangerous good registrations
 *
 * @param route - The activated route snapshot
 * @param state - The router state snapshot
 * @param dangerousGoodRegistrationHttpService - The dangerous good registration HTTP service
 * @returns An observable of the dangerous good registration list
 */
export const dangerousGoodRegistrationListResolver: ResolveFn<SaveDangerousGoodRegistrationDto[]> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  dangerousGoodRegistrationHttpService: DangerousGoodRegistrationHttpService = inject(
    DangerousGoodRegistrationHttpService
  )
) => {
  return dangerousGoodRegistrationHttpService.getAllDangerousGoodRegistrations();
};
