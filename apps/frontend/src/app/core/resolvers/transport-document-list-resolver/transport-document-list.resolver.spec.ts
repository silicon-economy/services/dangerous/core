/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';

import { transportDocumentListResolver } from './transport-document-list.resolver';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { of } from 'rxjs';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('transportDocumentListResolver', () => {
  let transportDocumentHttpService: TransportDocumentHttpService;

  const executeResolver: ResolveFn<SaveGoodsDataDto[]> = (...resolverParameters) =>
    TestBed.runInInjectionContext(() => transportDocumentListResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TransportDocumentHttpService],
    });
    transportDocumentHttpService = TestBed.inject(TransportDocumentHttpService);
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });

  it('should call http service', async () => {
    const getAllTransportDocumentsSpy = jest
      .spyOn(transportDocumentHttpService, 'getAllTransportDocuments')
      .mockReturnValue(of());
    await executeResolver(undefined, undefined);
    expect(getAllTransportDocumentsSpy).toHaveBeenCalled();
  });
});
