/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot } from '@angular/router';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { inject } from '@angular/core';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';

/**
 * Resolves the list of transport documents
 *
 * @param route - The activated route snapshot
 * @param state - The router state snapshot
 * @param transportDocumentHttpService - The transport document HTTP service
 * @returns An observable of the transport document list
 */
export const transportDocumentListResolver: ResolveFn<SaveGoodsDataDto[]> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  transportDocumentHttpService: TransportDocumentHttpService = inject(TransportDocumentHttpService)
) => {
  return transportDocumentHttpService.getAllTransportDocuments();
};
