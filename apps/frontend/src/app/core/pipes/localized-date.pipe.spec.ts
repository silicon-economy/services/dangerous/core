/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { LocalizedDatePipe } from '@core/pipes/localized-date.pipe';
import { TranslateModule } from '@ngx-translate/core';
import { DatePipe, registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeEn from '@angular/common/locales/en';

describe('LocalizedDatePipe', () => {
  let pipe: LocalizedDatePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [LocalizedDatePipe, DatePipe],
    });
    pipe = TestBed.inject(LocalizedDatePipe);
  });

  it('should create', () => {
    expect(pipe).toBeTruthy();
  });

  it('should always be UTC', () => {
    expect(new Date().getTimezoneOffset()).toBe(0);
  });

  it('should transform date value to localized german variant', () => {
    registerLocaleData(localeDe);
    jest.spyOn(pipe['translateService'], 'currentLang', 'get').mockReturnValue('de');
    expect(pipe.transform('1', 'short')).toEqual('01.01.70, 00:00');
  });

  it('should transform date value to localized english variant', () => {
    registerLocaleData(localeEn);
    jest.spyOn(pipe['translateService'], 'currentLang', 'get').mockReturnValue('en');
    expect(pipe.transform('1', 'short')).toEqual('1/1/70, 12:00 AM');
  });
});
