/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'localizedDate',
  pure: false,
  standalone: true,
})
export class LocalizedDatePipe implements PipeTransform {
  constructor(
    private translateService: TranslateService,
    private datePipe: DatePipe
  ) {}

  /**
   * Returns a date value according to the current locale rules.
   *
   * @param value - Date value
   * @param format - Date format
   * @returns Localized date string
   */
  transform(value: string | number | Date, format = 'short'): string {
    return this.datePipe.transform(
      value,
      format,
      Intl.DateTimeFormat().resolvedOptions().timeZone,
      this.translateService.currentLang
    );
  }
}
