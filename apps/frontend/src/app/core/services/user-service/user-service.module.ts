/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { UserLoginService } from '@core/services/user-service/user-login.service';

@NgModule({
  imports: [CommonModule],
  providers: [UserAuthenticationService, UserLoginService],
})
export class UserServiceModule {}
