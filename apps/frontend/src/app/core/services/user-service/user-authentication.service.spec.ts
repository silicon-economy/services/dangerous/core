/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { first } from 'rxjs';

describe('AuthService', () => {
  let service: UserAuthenticationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserAuthenticationService],
    });
    service = TestBed.inject(UserAuthenticationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should perform user authenticateAndGetUserDetails', () => {
    service.authenticateAndGetUserDetails('abc', 'abc').pipe(first()).subscribe();

    const req = httpMock.expectOne(`http://localhost:3001/user/authenticateAndGetUserDetails`);
    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('text');
    expect(req.request.headers.keys()).toEqual(['Content-Type', 'Authorization']);
    expect(req.request.headers.get('Authorization')).toBe('Basic YWJjOmFiYw==');
  });
});
