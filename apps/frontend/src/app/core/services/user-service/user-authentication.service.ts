/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';

@Injectable()
export class UserAuthenticationService {
  constructor(private httpClient: HttpClient) {}

  /**
   * Authenticates the user and retrieves user details
   *
   * @param username - The username
   * @param password - The password
   * @returns An observable of the authentication response
   */
  authenticateAndGetUserDetails(username: string, password: string): Observable<string> {
    return this.httpClient.get(
      `${environment.ENDPOINTS.MINI_AUTH_SERVER.URL}${environment.ENDPOINTS.MINI_AUTH_SERVER.USER_PREFIX}${environment.ENDPOINTS.MINI_AUTH_SERVER.AUTHENTICATE_AND_GET_USER_DETAILS}`,
      {
        responseType: 'text',
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Basic ' + Buffer.from(username + ':' + password).toString('base64'),
        }),
      }
    );
  }
}
