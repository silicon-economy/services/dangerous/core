/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

describe('UserService', () => {
  let service: UserLoginService;
  let authService: UserAuthenticationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserLoginService, UserAuthenticationService],
    });
    service = TestBed.inject(UserLoginService);
    authService = TestBed.inject(UserAuthenticationService);
  });

  afterEach(() => {
    service.logout();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set access token in local storage and process authenticateAndGetUserDetails', () => {
    const mockJwt =
      'eyJhbGciOiJFUzUxMiJ9.eyJpZCI6ImU1Y2E5NjViLTFjZWEtNGY0Yy1hMDgzLWQ5ZjY4NzIyOThkNCIsImNvbXBhbnkiOnsibmFtZSI6ImZvbyIsImFkZHJlc3MiOnsic3RyZWV0IjoiYmFyIiwibnVtYmVyIjoiZm9vIiwicG9zdGFsQ29kZSI6IjEyMzQ1IiwiY2l0eSI6ImZvbyIsImNvdW50cnkiOiJiYXIifSwiY29udGFjdCI6eyJuYW1lIjoiZm9vIGJhciIsInBob25lIjoiMDA0OSAxMjMgNDU2IDc4OSAwIiwibWFpbCI6ImZvby5iYXJAZm9vYmFyLmNvbSIsImRlcGFydG1lbnQiOiJmb28ifX0sInVzZXJuYW1lIjoiZm9vIiwicm9sZXMiOlsiY29uc2lnbm9yIl0sImlhdCI6MTY5NjYwMTEzOX0.AUxCbgMG39rv5aScASmlT8OR7rvFh-dQtU3_M1RkJ_VZiUov6FVMPkh55osgpejpLxbXQlA4H_YAGdnagg2lG7PdAMFOmmBmPHaPtZbRKA2ktLBoUjXidn2BFQty_uqKv0fx5C6ZuXg5CKpo7eT2wNUBzVOuVFxsS1E2kW6MDiZnBktY';
    jest.spyOn(authService, 'authenticateAndGetUserDetails').mockReturnValue(of(mockJwt));
    expect(service.isLoggedIn$.getValue()).toBe(false);
    service.login('foo', 'bar').subscribe();
    expect(service.isLoggedIn$.getValue()).toBe(true);
    expect(localStorage.getItem('access_token')).toBe(mockJwt);
  });

  it('should fail to authenticateAndGetUserDetails', () => {
    const mockJwt = 'foobar';
    jest.spyOn(authService, 'authenticateAndGetUserDetails').mockReturnValue(of(mockJwt));
    expect(service.isLoggedIn$.getValue()).toBe(false);
    service.login('foo', 'bar').subscribe();
    expect(service.isLoggedIn$.getValue()).toBe(false);
    expect(localStorage.getItem('access_token')).toBe(mockJwt);
  });

  it('should return an observable of the user roles', (done) => {
    const expectedRoles = ['consignor'];
    const mockJwt =
      'eyJhbGciOiJFUzUxMiJ9.eyJpZCI6ImU1Y2E5NjViLTFjZWEtNGY0Yy1hMDgzLWQ5ZjY4NzIyOThkNCIsImNvbXBhbnkiOnsibmFtZSI6ImZvbyIsImFkZHJlc3MiOnsic3RyZWV0IjoiYmFyIiwibnVtYmVyIjoiZm9vIiwicG9zdGFsQ29kZSI6IjEyMzQ1IiwiY2l0eSI6ImZvbyIsImNvdW50cnkiOiJiYXIifSwiY29udGFjdCI6eyJuYW1lIjoiZm9vIGJhciIsInBob25lIjoiMDA0OSAxMjMgNDU2IDc4OSAwIiwibWFpbCI6ImZvby5iYXJAZm9vYmFyLmNvbSIsImRlcGFydG1lbnQiOiJmb28ifX0sInVzZXJuYW1lIjoiZm9vIiwicm9sZXMiOlsiY29uc2lnbm9yIl0sImlhdCI6MTY5NjYwMTEzOX0.AUxCbgMG39rv5aScASmlT8OR7rvFh-dQtU3_M1RkJ_VZiUov6FVMPkh55osgpejpLxbXQlA4H_YAGdnagg2lG7PdAMFOmmBmPHaPtZbRKA2ktLBoUjXidn2BFQty_uqKv0fx5C6ZuXg5CKpo7eT2wNUBzVOuVFxsS1E2kW6MDiZnBktY';
    jest.spyOn(authService, 'authenticateAndGetUserDetails').mockReturnValue(of(mockJwt));
    service.login('foo', 'bar').subscribe();
    service.userRoles.subscribe((roles) => {
      expect(roles).toEqual(expectedRoles);
      done();
    });
  });

  it('should return an observable of the user name', (done) => {
    const expectedName = 'foo';
    const mockJwt =
      'eyJhbGciOiJFUzUxMiJ9.eyJpZCI6ImU1Y2E5NjViLTFjZWEtNGY0Yy1hMDgzLWQ5ZjY4NzIyOThkNCIsImNvbXBhbnkiOnsibmFtZSI6ImZvbyIsImFkZHJlc3MiOnsic3RyZWV0IjoiYmFyIiwibnVtYmVyIjoiZm9vIiwicG9zdGFsQ29kZSI6IjEyMzQ1IiwiY2l0eSI6ImZvbyIsImNvdW50cnkiOiJiYXIifSwiY29udGFjdCI6eyJuYW1lIjoiZm9vIGJhciIsInBob25lIjoiMDA0OSAxMjMgNDU2IDc4OSAwIiwibWFpbCI6ImZvby5iYXJAZm9vYmFyLmNvbSIsImRlcGFydG1lbnQiOiJmb28ifX0sInVzZXJuYW1lIjoiZm9vIiwicm9sZXMiOlsiY29uc2lnbm9yIl0sImlhdCI6MTY5NjYwMTEzOX0.AUxCbgMG39rv5aScASmlT8OR7rvFh-dQtU3_M1RkJ_VZiUov6FVMPkh55osgpejpLxbXQlA4H_YAGdnagg2lG7PdAMFOmmBmPHaPtZbRKA2ktLBoUjXidn2BFQty_uqKv0fx5C6ZuXg5CKpo7eT2wNUBzVOuVFxsS1E2kW6MDiZnBktY';
    jest.spyOn(authService, 'authenticateAndGetUserDetails').mockReturnValue(of(mockJwt));
    service.login('foo', 'bar').subscribe();
    service.userName.subscribe((name) => {
      expect(name).toEqual(expectedName);
      done();
    });
  });

  it('should return an observable of the user company', (done) => {
    const expectedCompany = {
      address: {
        city: 'foo',
        country: 'bar',
        number: 'foo',
        postalCode: '12345',
        street: 'bar',
      },
      contact: { department: 'foo', mail: 'foo.bar@foobar.com', name: 'foo bar', phone: '0049 123 456 789 0' },
      name: 'foo',
    };
    const mockJwt =
      'eyJhbGciOiJFUzUxMiJ9.eyJpZCI6ImU1Y2E5NjViLTFjZWEtNGY0Yy1hMDgzLWQ5ZjY4NzIyOThkNCIsImNvbXBhbnkiOnsibmFtZSI6ImZvbyIsImFkZHJlc3MiOnsic3RyZWV0IjoiYmFyIiwibnVtYmVyIjoiZm9vIiwicG9zdGFsQ29kZSI6IjEyMzQ1IiwiY2l0eSI6ImZvbyIsImNvdW50cnkiOiJiYXIifSwiY29udGFjdCI6eyJuYW1lIjoiZm9vIGJhciIsInBob25lIjoiMDA0OSAxMjMgNDU2IDc4OSAwIiwibWFpbCI6ImZvby5iYXJAZm9vYmFyLmNvbSIsImRlcGFydG1lbnQiOiJmb28ifX0sInVzZXJuYW1lIjoiZm9vIiwicm9sZXMiOlsiY29uc2lnbm9yIl0sImlhdCI6MTY5NjYwMTEzOX0.AUxCbgMG39rv5aScASmlT8OR7rvFh-dQtU3_M1RkJ_VZiUov6FVMPkh55osgpejpLxbXQlA4H_YAGdnagg2lG7PdAMFOmmBmPHaPtZbRKA2ktLBoUjXidn2BFQty_uqKv0fx5C6ZuXg5CKpo7eT2wNUBzVOuVFxsS1E2kW6MDiZnBktY';
    jest.spyOn(authService, 'authenticateAndGetUserDetails').mockReturnValue(of(mockJwt));
    service.login('foo', 'bar').subscribe();
    service.userCompany.subscribe((company) => {
      expect(company).toEqual(expectedCompany);
      done();
    });
  });

  it('should return an observable of the user contact person', (done) => {
    const expectedContactPerson = {
      department: 'foo',
      mail: 'foo.bar@foobar.com',
      name: 'foo bar',
      phone: '0049 123 456 789 0',
    };
    const mockJwt =
      'eyJhbGciOiJFUzUxMiJ9.eyJpZCI6ImU1Y2E5NjViLTFjZWEtNGY0Yy1hMDgzLWQ5ZjY4NzIyOThkNCIsImNvbXBhbnkiOnsibmFtZSI6ImZvbyIsImFkZHJlc3MiOnsic3RyZWV0IjoiYmFyIiwibnVtYmVyIjoiZm9vIiwicG9zdGFsQ29kZSI6IjEyMzQ1IiwiY2l0eSI6ImZvbyIsImNvdW50cnkiOiJiYXIifSwiY29udGFjdCI6eyJuYW1lIjoiZm9vIGJhciIsInBob25lIjoiMDA0OSAxMjMgNDU2IDc4OSAwIiwibWFpbCI6ImZvby5iYXJAZm9vYmFyLmNvbSIsImRlcGFydG1lbnQiOiJmb28ifX0sInVzZXJuYW1lIjoiZm9vIiwicm9sZXMiOlsiY29uc2lnbm9yIl0sImlhdCI6MTY5NjYwMTEzOX0.AUxCbgMG39rv5aScASmlT8OR7rvFh-dQtU3_M1RkJ_VZiUov6FVMPkh55osgpejpLxbXQlA4H_YAGdnagg2lG7PdAMFOmmBmPHaPtZbRKA2ktLBoUjXidn2BFQty_uqKv0fx5C6ZuXg5CKpo7eT2wNUBzVOuVFxsS1E2kW6MDiZnBktY';
    jest.spyOn(authService, 'authenticateAndGetUserDetails').mockReturnValue(of(mockJwt));
    service.login('foo', 'bar').subscribe();
    service.userContactPerson.subscribe((company) => {
      expect(company).toEqual(expectedContactPerson);
      done();
    });
  });
});
