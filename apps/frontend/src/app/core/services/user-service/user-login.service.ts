/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';

import { BehaviorSubject, catchError, map, Observable, tap, throwError } from 'rxjs';
import { AuthenticationEnum } from '@base/src/app/models/authentication.enum';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import jwtDecode from 'jwt-decode';
import { UserDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/user.dto';
import { CompanyDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/company.dto';
import { ContactPersonDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/contact-person.dto';
import { JwtDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/jwt.dto';
import { AddressDto } from '@core/api-interfaces/lib/dtos/mini-auth-server/address.dto';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { UnauthorizedException } from '@nestjs/common';

@Injectable()
export class UserLoginService {
  isLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.hasAccessToken());
  currentUserInformation$: BehaviorSubject<JwtDto> = new BehaviorSubject<JwtDto>(undefined);

  constructor(private authService: UserAuthenticationService) {
    const accessToken: string = localStorage.getItem('access_token');
    if (accessToken) {
      this.updateCurrentUserInformation(accessToken);
    }
  }

  /**
   * Returns an observable of the user roles.
   *
   * @returns An observable of the user roles
   */
  get userRoles(): Observable<UserRole[]> {
    return this.currentUserInformation$.pipe(map((jwtDto: JwtDto) => jwtDto?.roles || []));
  }

  /**
   * Returns an observable of the username.
   *
   * @returns An observable of the username
   */
  get userName(): Observable<string> {
    return this.currentUserInformation$.pipe(map((jwtDto: JwtDto) => jwtDto?.username || ''));
  }

  /**
   * Returns an observable of the user's company.
   *
   * @returns An observable of the user's company
   */
  get userCompany(): Observable<CompanyDto> {
    return this.currentUserInformation$.pipe(map((jwtDto: JwtDto) => jwtDto?.company));
  }

  /**
   * Returns an observable of the user's contact person.
   *
   * @returns An observable of the user's contact person
   */
  get userContactPerson(): Observable<ContactPersonDto> {
    return this.currentUserInformation$.pipe(map((jwtDto: JwtDto) => jwtDto?.company.contact));
  }

  /**
   * Authenticates the user with the provided username and password.
   *
   * @param username - The username
   * @param password - The password
   * @returns An observable of the authentication response
   */
  public login(username: string, password: string): Observable<string> {
    return this.authService.authenticateAndGetUserDetails(username, password).pipe(
      tap((res: string) => {
        localStorage.setItem('access_token', res);
        this.isLoggedIn$.next(true);
        this.updateCurrentUserInformation(res);
      }),
      catchError((err: UnauthorizedException) => {
        this.isLoggedIn$.next(false);
        return throwError(() => err);
      })
    );
  }

  /**
   * Logs out the user.
   */
  public logout(): void {
    localStorage.removeItem(AuthenticationEnum.accessToken);
    this.isLoggedIn$.next(false);
  }

  /**
   * Checks if the user has an access token.
   *
   * @returns True if the user has an access token, false otherwise
   */
  public hasAccessToken(): boolean {
    return !!localStorage.getItem(AuthenticationEnum.accessToken);
  }

  /**
   * Updates the current user information based on the provided access token.
   *
   * @param accessToken - The access token
   */
  private updateCurrentUserInformation(accessToken: string): void {
    const jwtDecoded: JwtDto = jwtDecode(accessToken);
    if (jwtDecoded != undefined) {
      this.currentUserInformation$.next(
        new JwtDto(
          jwtDecoded.username,
          jwtDecoded.roles,
          new UserDto(
            jwtDecoded.id,
            new CompanyDto(
              jwtDecoded.company.name,
              new AddressDto(
                jwtDecoded.company.address.street,
                jwtDecoded.company.address.number,
                jwtDecoded.company.address.postalCode,
                jwtDecoded.company.address.city,
                jwtDecoded.company.address.country
              ),
              new ContactPersonDto(
                jwtDecoded.company.contact.name,
                jwtDecoded.company.contact.phone,
                jwtDecoded.company.contact.mail,
                jwtDecoded.company.contact.department
              )
            )
          ),
          jwtDecoded.iat
        )
      );
    }
  }
}
