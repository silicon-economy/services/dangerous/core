/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { CanActivateFn, Router } from '@angular/router';
import { authGuardIsConsignor, authGuardIsLoggedIn, authGuardIsLoggedOut } from '@core/guards/auth.guard';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {UserRole} from "@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum";
import {of} from "rxjs";

describe('authGuard', () => {
  let userService: UserLoginService;

  const executeAuthGuardIsLoggedIn: CanActivateFn = (...guardParameters) =>
    TestBed.runInInjectionContext(() => authGuardIsLoggedIn(...guardParameters));

  const executeAuthGuardIsLoggedOut: CanActivateFn = (...guardParameters) =>
    TestBed.runInInjectionContext(() => authGuardIsLoggedOut(...guardParameters));

  const executeAuthGuardIsConsignor: CanActivateFn = (...guardParameters) =>
    TestBed.runInInjectionContext(() => authGuardIsConsignor(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([{ path: 'transport-documents', redirectTo: '' }]),
      ],
      providers: [UserLoginService, UserAuthenticationService],
    });

    userService = TestBed.inject(UserLoginService);
  });

  it('should be created', () => {
    expect(executeAuthGuardIsLoggedIn).toBeTruthy();
    expect(executeAuthGuardIsLoggedOut).toBeTruthy();
  });

  it('should return true if the user is logged in', () => {
    const isLoggedInSpy = jest.spyOn(userService.isLoggedIn$, 'getValue').mockReturnValue(true);
    const result = executeAuthGuardIsLoggedIn(undefined, undefined);
    expect(result).toBe(true);
    expect(isLoggedInSpy).toHaveBeenCalled();
  });

  it('should return false if the user is not logged in', () => {
    const isLoggedInSpy = jest.spyOn(userService.isLoggedIn$, 'getValue').mockReturnValue(false);
    const result = executeAuthGuardIsLoggedIn(undefined, undefined);
    expect(result).toBe(false);
    expect(isLoggedInSpy).toHaveBeenCalled();
  });

  it('should return true if the user is not logged in', async () => {
    const isLoggedInSpy = jest.spyOn(userService.isLoggedIn$, 'getValue').mockReturnValue(false);
    const result = executeAuthGuardIsLoggedOut(undefined, undefined);
    expect(await result).toBe(true);
    expect(isLoggedInSpy).toHaveBeenCalled();
  });

  it('should return false if the user is logged in', async () => {
    const isLoggedInSpy = jest.spyOn(userService.isLoggedIn$, 'getValue').mockReturnValue(true);
    const result = executeAuthGuardIsLoggedOut(undefined, undefined);
    expect(await result).toBe(false);
    expect(isLoggedInSpy).toHaveBeenCalled();
  });

  it('should return true if user is logged in and it is a consignor', () => {
    jest.spyOn(UserLoginService.prototype, 'userRoles', 'get').mockReturnValue(of([UserRole.consignor]));
    jest.spyOn(userService.isLoggedIn$, 'getValue').mockReturnValue(true);
    const navigateByUrlSpy = jest.spyOn(Router.prototype, 'navigateByUrl');
    const result = executeAuthGuardIsConsignor(undefined, undefined);

    expect(result).toBe(true);
    expect(navigateByUrlSpy).not.toHaveBeenCalled();
  });

  it('should return true if user is logged in and it is not a consignor', () => {
    jest.spyOn(UserLoginService.prototype, 'userRoles', 'get').mockReturnValue(of([UserRole.carrier]));
    jest.spyOn(userService.isLoggedIn$, 'getValue').mockReturnValue(true);
    const navigateByUrlSpy = jest.spyOn(Router.prototype, 'navigateByUrl');

    void executeAuthGuardIsConsignor(undefined, undefined);
    expect(navigateByUrlSpy).toHaveBeenCalled();
  });
});
