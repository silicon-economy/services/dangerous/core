/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserRole } from "@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum";

/**
 * Checks if the user is logged in and returns the result
 *
 * @returns True if the user is logged in, false otherwise
 */
export const authGuardIsLoggedIn: CanActivateFn = (): boolean => {
  const userService: UserLoginService = inject(UserLoginService);
  return userService.isLoggedIn$.getValue();
};

/**
 * Checks if the user is a consignor and redirects them to the 'transport-documents-list-page' if the user holds a different role.
 *
 * @returns True if user is consignor, false otherwise
 */
export const authGuardIsConsignor: CanActivateFn = (): boolean => {
  const userService: UserLoginService = inject(UserLoginService);
  const router: Router = inject(Router);
  let allUserRoles: UserRole[];

  userService.userRoles.subscribe((userRoles: UserRole[]) => (allUserRoles = userRoles));

  if(allUserRoles.some((userRole: UserRole): boolean => userRole == UserRole.consignor)){
    return userService.isLoggedIn$.getValue();
  } else {
    void router.navigateByUrl('transport-documents');
  }
}

/**
 * Checks if the user is logged out and redirects to the 'transport-documents-list-page' if logged in
 *
 * @returns True if the user is logged out, false otherwise
 */
export const authGuardIsLoggedOut: CanActivateFn = async (): Promise<boolean> => {
  const userService: UserLoginService = inject(UserLoginService);
  const router: Router = inject(Router);
  if (userService.isLoggedIn$.getValue()) {
    await router.navigateByUrl('transport-documents');
  }
  return !userService.isLoggedIn$.getValue();
};
