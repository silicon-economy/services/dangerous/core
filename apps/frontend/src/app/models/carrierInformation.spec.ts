/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CarrierInformation } from "@base/src/app/models/carrierInformation";

describe('CarrierInformation', () => {
  let carrierInformation: CarrierInformation;

  beforeEach(() => {
    carrierInformation = new CarrierInformation(undefined, undefined, undefined, undefined);
  });

  it('should create an instance', () => {
    expect(carrierInformation).toBeTruthy();
  });

  it('should have initial properties as undefined or empty strings', (): void => {
    expect(carrierInformation.name).toBeUndefined();
    expect(carrierInformation.driver).toBeUndefined();
    expect(carrierInformation.licensePlate).toBeUndefined();
    expect(carrierInformation.transportationInstructions).toBeUndefined();
  });

  it('should set properties when values are assigned', (): void => {
    const name = 'Gefahrgutlogistik';
    const driver = 'Max Mustermann';
    const licensePlate = 'RO-AB123';
    const instructions = 'Bitte Beförderungseinheit nach der Entladung reinigen.';

    carrierInformation.name = name;
    carrierInformation.driver = driver;
    carrierInformation.licensePlate = licensePlate;
    carrierInformation.transportationInstructions = instructions;

    expect(carrierInformation.name).toEqual(name);
    expect(carrierInformation.driver).toEqual(driver);
    expect(carrierInformation.licensePlate).toEqual(licensePlate);
    expect(carrierInformation.transportationInstructions).toEqual(instructions);
  });
});
