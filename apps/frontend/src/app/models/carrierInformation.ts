/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class CarrierInformation {
  name: string;
  driver: string;
  licensePlate: string;
  transportationInstructions: string;


  constructor(name: string, driver: string, licensePlate: string, transportationInstructions: string) {
    this.name = name;
    this.driver = driver;
    this.licensePlate = licensePlate;
    this.transportationInstructions = transportationInstructions;
  }
}
