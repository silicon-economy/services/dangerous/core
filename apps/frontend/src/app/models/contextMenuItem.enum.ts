/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Represents the available context menu items for transport documents.
 */
export enum ContextMenuItem {
  viewTransportDocument = 'viewTransportDocument',
  importDangerousGoodRegistration = 'importDangerousGoodRegistration',
  downloadPackagingLabel = 'downloadPackagingLabel',
  showQrCode = 'showQrCode',
  checkTransportDocument = 'checkTransportDocument',
  disableTransportDocument = 'disableTransportDocument',
  releaseTransportDocument = 'releaseTransportDocument',
  checkTransportationUnit = 'checkTransportationUnit',
  performVisualInspection = 'performVisualInspection',
}
