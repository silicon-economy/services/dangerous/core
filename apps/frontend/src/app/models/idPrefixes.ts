/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Enumeration of prefixes used for different types of identification numbers.
 */
export enum IdPrefixes {
  dangerousGoodRegistrationIdPrefix = 'DGR',
  transportDocumentIdPrefix = 'BP',
}
