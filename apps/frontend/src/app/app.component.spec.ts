/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ThemeServiceModule } from '@shared/services/theme-service/theme-service.module';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { of } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

class MockRouterNavigationStart {
  public events = of(new NavigationStart(0, ''));
}

class MockRouterNavigationEnd {
  public events = of(new NavigationEnd(0, '', ''));
}

class NgxSpinnerServiceMock {
  public show = jest.fn();
  public hide = jest.fn();
}

describe('AppComponent', () => {
  let app: AppComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), ThemeServiceModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [AppComponent],
      providers: [
        { provide: Router, useValue: new MockRouterNavigationStart() },
        { provide: NgxSpinnerService, useClass: NgxSpinnerServiceMock },
      ],
    }).compileComponents();
    const fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it(`should have as title 'blockchain_europe_dangerous'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('blockchain_europe_dangerous');
  });

  it(`should add languages to translate service`, () => {
    const translateServiceSpy = jest.spyOn(TranslateService.prototype, 'addLangs').mockImplementation();
    app.ngOnInit();
    expect(translateServiceSpy).toHaveBeenCalledWith(['en', 'de']);
  });

  it('should use currentLanguage if valid', () => {
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('en');
    const translateServiceSpy = jest.spyOn(TranslateService.prototype, 'use').mockImplementation();
    app.ngOnInit();
    expect(translateServiceSpy).toHaveBeenCalledWith('en');
  });

  it('should show spinner on NavigationStart event', () => {
    app.ngOnInit();
    expect(app['spinner'].show).toHaveBeenCalled();
    expect(app['spinner'].hide).not.toHaveBeenCalled();
  });

  it('should hide spinner on specific events', () => {
    // Change provider mock instance with new testing module configuration
    TestBed.resetTestingModule();
    void TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), ThemeServiceModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [AppComponent],
      providers: [
        { provide: Router, useValue: new MockRouterNavigationEnd() },
        { provide: NgxSpinnerService, useClass: NgxSpinnerServiceMock },
      ],
    }).compileComponents();
    const fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

    app.ngOnInit();
    expect(app['spinner'].hide).toHaveBeenCalled();
  });
});
