/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class EventService {
  private subject: Subject<Event> = new Subject<Event>();
  private iconMenuSubject: Subject<boolean> = new Subject<boolean>();

  /**
   * Changes the toggle navigation state.
   *
   * @param toggle - The toggle event.
   */
  public changeToggleNavigation(toggle: Event): void {
    this.subject.next(toggle);
  }

  /**
   * Changes the icon menu state.
   *
   * @param icon - The icon value.
   */
  public changeIconMenu(icon: boolean): void {
    this.iconMenuSubject.next(icon);
  }
}
