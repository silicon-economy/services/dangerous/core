/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  CarrierCheckCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/carrier-check-criteria';
import { CreateGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend';
import {
  TransportVehicleCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/transport-vehicle-criteria';
import {
  AcceptDangerousGoodRegistrationDto
} from "@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/accept-dangerous-good-registration.dto";
import {
  UpdateVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import {
  UpdateVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';

@Injectable()
export class TransportDocumentHttpService {
  constructor(private http: HttpClient) {}

  /**
   * Retrieves a specific transport document by ID.
   *
   * @param id - The ID of the transport document.
   * @returns An observable of type `SaveGoodsDataDto` that emits the transport document.
   */
  public getTransportDocument(id: string): Observable<SaveGoodsDataDto> {
    return this.http.get<SaveGoodsDataDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/${id}`
    );
  }

  /**
   * Retrieves all transport documents.
   *
   * @returns An observable of type `SaveGoodsDataDto[]` that emits an array of transport documents.
   */
  public getAllTransportDocuments(): Observable<SaveGoodsDataDto[]> {
    return this.http.get<SaveGoodsDataDto[]>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}`
    );
  }

  /**
   * Retrieves the packaging labels for a specific transport document as a PDF.
   *
   * @param id - The ID of the transport document.
   * @returns An observable of type `Blob` that emits the packaging labels PDF as a blob.
   */
  public getPackagingLabels(id: string): Observable<Blob> {
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/pdf');
    return this.http.get(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.MEDIA_MANAGEMENT_PREFIX}/${id}`,
      { headers: headers, responseType: 'blob' }
    );
  }

  /**
   * Disables a transport document by ID.
   *
   * @param id - The ID of the transport document to disable.
   * @returns An observable that completes once the request is successful.
   */
  public disableTransportDocument(id: string): Observable<void> {
    let headers = new HttpHeaders();
    headers = headers.set('typeOfAction', 'disable');
    return this.http.put<void>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/${id}`,
      null,
      { headers: headers }
    );
  }

  /**
   * Releases a transport document by ID.
   *
   * @param id - The ID of the transport document to release.
   * @returns An observable of type `SaveGoodsDataDto` that emits the released transport document.
   */
  public releaseTransportDocument(id: string): Observable<SaveGoodsDataDto> {
    let headers = new HttpHeaders();
    headers = headers.set('typeOfAction', 'release');
    return this.http.put<SaveGoodsDataDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/${id}`,
      null,
      { headers: headers }
    );
  }

  /**
   * Performs a carrier check for a transport document using the specified acceptance criteria.
   *
   * @param id - The ID of the transport document.
   * @param acceptanceCriteria - The acceptance criteria for the carrier check.
   * @returns An observable of type `SaveGoodsDataDto` that emits the updated transport document.
   */
  public transportDocumentCarrierCheck(
    id: string,
    acceptanceCriteria: CarrierCheckCriteria
  ): Observable<SaveGoodsDataDto> {
    let headers = new HttpHeaders();
    headers = headers.set('typeOfAction', 'carrierCheck');
    return this.http.put<SaveGoodsDataDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/${id}`,
      acceptanceCriteria,
      { headers: headers }
    );
  }

  /**
   * Get transport document id for a given order position id
   *
   * @param orderPositionId - The ID of the order position
   * @returns String observable
   */
  public getTransportDocumentIdByOrderPositionId(orderPositionId: number): Observable<string> {
    return this.http.get(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_ID_BY_ORDER_POSITION_ID}/${orderPositionId}`,
      { responseType: 'text' }
    );
  }

  /**
   * Updates the Order Position Check in a Transport Document
   *
   * @param updateVisualInspectionCarrierDto - Transport document with all necessary ids and checks
   * @returns Observable<TransportDocument>
   */
  public updateVisualInspectionCarrier(
    updateVisualInspectionCarrierDto: UpdateVisualInspectionCarrierDto
  ): Observable<SaveGoodsDataDto> {
    let updateDocumentHeaders: HttpHeaders = new HttpHeaders();
    updateDocumentHeaders = updateDocumentHeaders.set('typeOfAction', 'updateVisualInspectionCarrier');
    return this.http.put<SaveGoodsDataDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/`,
      updateVisualInspectionCarrierDto,
      { headers: updateDocumentHeaders }
    );
  }

  /**
   * Updates the consignee visual inspection.
   *
   * @param updateVisualInspectionConsigneeDto - Transport document with all necessary ids and checks
   * @returns Observable<TransportDocument>
   */
  public updateVisualInspectionConsignee(
    updateVisualInspectionConsigneeDto: UpdateVisualInspectionConsigneeDto
  ): Observable<SaveGoodsDataDto> {
    let updateDocumentHeaders = new HttpHeaders();
    updateDocumentHeaders = updateDocumentHeaders.set('typeOfAction', 'updateVisualInspectionConsignee');
    return this.http.put<SaveGoodsDataDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/`,
      updateVisualInspectionConsigneeDto,
      { headers: updateDocumentHeaders }
    );
  }

  /**
   * Updates the transport vehicle criteria for a transport document using the provided transport vehicle criteria.
   *
   * @param id - The ID of the transport document.
   * @param transportVehicleCriteria - The transport vehicle criteria.
   * @returns An observable of type `SaveGoodsDataDto` that emits the updated transport document.
   */
  public updateTransportVehicleCriteria(
    id: string,
    transportVehicleCriteria: TransportVehicleCriteria
  ): Observable<SaveGoodsDataDto> {
    let headers = new HttpHeaders();
    headers = headers.set('typeOfAction', 'vehicleInspection');
    return this.http.put<SaveGoodsDataDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/${id}`,
      transportVehicleCriteria,
      { headers: headers }
    );
  }

  /**
   * Creates a transport document
   *
   * @param request - The transport document to be added to the blockchain
   * @returns Transport document observable
   */
  public createTransportDocument(request: CreateGoodsDataDto | null): Observable<SaveGoodsDataDto> {
    return this.http.post<SaveGoodsDataDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.BASEPATH}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}`,
      request
    );
  }

  /**
   * Creates a transport document from a dangerous good registration
   *
   * @param request - The transport document to be added to the blockchain
   * @returns SaveGoodsDataDto Transport document response observable
   */
  public transferDangerousGoodRegistrationToTransportDocument(request: AcceptDangerousGoodRegistrationDto | null): Observable<SaveGoodsDataDto> {
    let createDocumentHeaders = new HttpHeaders();
    createDocumentHeaders = createDocumentHeaders.set('typeOfAction', 'createAndTransport');

    return this.http.post<SaveGoodsDataDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.BASEPATH}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}`,
      request,
      { headers: createDocumentHeaders }
    );
  }

  /**
   * Confirm carrier identity
   *
   * @param id - The ID of the transport document
   * @returns An observable of type `SaveGoodsDataDto` that emits the updated transport document.
   */
  public confirmCarrierIdentity(id: string): Observable<SaveGoodsDataDto> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set('typeOfAction', 'confirmCarrier');
    return this.http.put<SaveGoodsDataDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.BASEPATH}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/${id}`,
      {},
      { headers: headers }
    );
  }

  /**
   * Merges accepted order-positions from a dangerous good registration into an existing transport document.
   *
   * @param request - The transport document to be added to the blockchain.
   *@returns Transport document observable.
   */
  public acceptDangerousGoodRegistration(
    request: AcceptDangerousGoodRegistrationDto | null
  ): Observable<SaveGoodsDataDto> {
    let headers = new HttpHeaders();
    headers = headers.set('typeOfAction', 'acceptDangerousGoodRegistration');
    return this.http.put<SaveGoodsDataDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.BASEPATH}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}`,
      request,
      { headers: headers }
    );
  }

  /**
   * Creates a transport document and sets its status to `transport` immediately.
   *
   * @param request - The transport document to be added to the blockchain.
   * @returns Transport document observable.
   */
  public createTransportDocumentAndImmediateTransport(
    request: AcceptDangerousGoodRegistrationDto | null
  ): Observable<SaveGoodsDataDto> {
    let headers = new HttpHeaders();
    headers = headers.set('typeOfAction', 'createAndTransport');
    return this.http.post<SaveGoodsDataDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.BASEPATH}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}`,
      request,
      { headers: headers }
    );
  }
}
