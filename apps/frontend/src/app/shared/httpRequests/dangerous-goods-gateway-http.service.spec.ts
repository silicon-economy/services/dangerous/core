/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '@environments/environment';
import { DangerousGoodsGatewayHttpService } from '@shared/httpRequests/dangerous-goods-gateway-http.service';

describe('DangerousGoodsGatewayHttpService', () => {
  let service: DangerousGoodsGatewayHttpService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DangerousGoodsGatewayHttpService],
    });
    service = TestBed.inject(DangerousGoodsGatewayHttpService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get a dangerous good by its input string', () => {
    service.getDangerousGoodByInputString('1234').subscribe();
    httpMock
      .expectOne(`${environment.ENDPOINTS.DANGEROUS_GOODS_GATEWAY.URL}/dangerousgoodlookup/1234`)
      .flush({ id: '1234' }, {});
  });
});
