/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '@environments/environment';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  CarrierCheckCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/carrier-check-criteria';
import {
  UpdateVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import {
  OrderVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-visual-inspection-carrier.dto';
import {
  OrderPositionVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import {
  UpdateVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';
import {
  OrderVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/order-visual-inspection-consignee.dto';
import {
  OrderPositionVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/order-position-visual-inspection-consignee.dto';
import {
  TransportVehicleCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/transport-vehicle-criteria';
import { Carrier, Company, CreateGoodsDataDto, Freight } from '@core/api-interfaces/lib/dtos/frontend';
import {
  AcceptDangerousGoodRegistrationDto
} from "@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/accept-dangerous-good-registration.dto";
import { mockCarrier } from "@core/api-interfaces/lib/mocks/carrier/carrier.mock";

describe('TransportDocumentHttpService', () => {
  let service: TransportDocumentHttpService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TransportDocumentHttpService],
    });
    service = TestBed.inject(TransportDocumentHttpService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get a transport document', () => {
    service.getTransportDocument('1234').subscribe((transportDocument: SaveGoodsDataDto) => {
      expect(transportDocument.id).toBe('1234');
    });
    httpMock
      .expectOne(
        `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/1234`
      )
      .flush({ id: '1234' }, {});
  });

  it('should get all transport documents', () => {
    service.getAllTransportDocuments().subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}`
    );
  });

  it('should get packaging labels for a transport document', () => {
    service.getPackagingLabels('1234').subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.MEDIA_MANAGEMENT_PREFIX}/1234`
    );
  });

  it('should disable a transport document given by its id', () => {
    service.disableTransportDocument('1234').subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/1234`
    );
  });

  it('should release a transport document given by its id', () => {
    service.releaseTransportDocument('1234').subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/1234`
    );
  });

  it('should get transport document id by order position id', () => {
    service.getTransportDocumentIdByOrderPositionId(1).subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_ID_BY_ORDER_POSITION_ID}/1`
    );
  });

  it('should update visual inspection carrier', () => {
    const updateVisualInspectionCarrierDto: UpdateVisualInspectionCarrierDto = new UpdateVisualInspectionCarrierDto(
      'foo',
      [new OrderVisualInspectionCarrierDto('bar', [new OrderPositionVisualInspectionCarrierDto(1, true, true, '')])]
    );
    service.updateVisualInspectionCarrier(updateVisualInspectionCarrierDto).subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/`
    );
  });

  it('should update visual inspection consignee', () => {
    const updateVisualInspectionConsigneeDto: UpdateVisualInspectionConsigneeDto =
      new UpdateVisualInspectionConsigneeDto(
        'foobar',
        new OrderVisualInspectionConsigneeDto('1', [new OrderPositionVisualInspectionConsigneeDto(1, true, true, '')])
      );
    service.updateVisualInspectionConsignee(updateVisualInspectionConsigneeDto).subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/`
    );
  });

  it('should update transport vehicle criteria', () => {
    const transportVehicleCriteria: TransportVehicleCriteria = new TransportVehicleCriteria(
      'foo-bar',
      true,
      true,
      'foo',
      true,
      true
    );
    service.updateTransportVehicleCriteria('foobar', transportVehicleCriteria).subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/foobar`
    );
  });

  it('should create transport document', () => {
    const createGoodsDataDto: CreateGoodsDataDto = new CreateGoodsDataDto(
      new Company(undefined, undefined, undefined),
      new Freight(undefined, undefined, undefined, undefined),
      new Carrier(undefined, undefined, undefined)
    );
    service.createTransportDocument(createGoodsDataDto).subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.BASEPATH}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}`
    );
  });

  it('should confirm carrier identity', () => {
    service.confirmCarrierIdentity('foobar').subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.BASEPATH}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/foobar`
    );
  });

  it('should perform a carrier check for the transport document given by its id', () => {
    service
      .transportDocumentCarrierCheck(
        '1234',
        new CarrierCheckCriteria(undefined, undefined, undefined, undefined, undefined)
      )
      .subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}/1234`
    );
  });

  it('should update accept dangerous good registration', () => {
    const acceptDangerousGoodRegistrationDto: AcceptDangerousGoodRegistrationDto = new AcceptDangerousGoodRegistrationDto(
      '7642978',
      mockCarrier,
      'Please clean the transport unit after unloading',
      []
    )

    service.
    acceptDangerousGoodRegistration(acceptDangerousGoodRegistrationDto
      )
      .subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.BASEPATH}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}`,
    );
  });

  it('should post transport document', () => {
    const acceptDangerousGoodRegistrationDto: AcceptDangerousGoodRegistrationDto = new AcceptDangerousGoodRegistrationDto(
      '7459465',
      mockCarrier,
      'Please clean the transport unit after unloading',
      []
    )

    service.
    createTransportDocumentAndImmediateTransport(acceptDangerousGoodRegistrationDto
    )
      .subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.BASEPATH}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.TRANSPORT_DOCUMENT_PREFIX}`,
    );
  });
});
