/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { Injectable } from '@angular/core';
import {
  DangerousGoodLookupDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/dangerous-good-lookup/dangerous-good-lookup.dto';
import {
  PackagingFilter
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingFilter';
import {
  Packaging
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/packaging-lookup/packagingTypes/packaging';
import {
  TransportCategoryPoints
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/responce/transportCategoryPoints';
import {
  CalculationRequest
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/request/calculationRequest';
import { DangerousGood } from '@core/api-interfaces/lib/dtos/frontend';

@Injectable()
export class DangerousGoodsGatewayHttpService {
  constructor(private http: HttpClient) {}

  /**
   * Looks up possible UN numbers for a given UN number or its prefix
   *
   * @param unNumber - The unNumber (prefix)
   * @returns unNumber
   */
  public lookupUnNumber(unNumber: string): Observable<DangerousGood[]> {
    return this.http.get<DangerousGood[]>(
      `${environment.ENDPOINTS.DANGEROUS_GOODS_GATEWAY.URL}/dangerousgoodlookup/${unNumber}`
    );
  }

  /**
   * Retrieves a dangerous good by input string.
   *
   * @param inputString - The input string used to search for the dangerous good.
   * @returns An observable of type `DangerousGoodLookupDto` that emits the dangerous good.
   */
  public getDangerousGoodByInputString(inputString: string): Observable<DangerousGoodLookupDto> {
    return this.http.get<DangerousGoodLookupDto>(
      `${environment.ENDPOINTS.DANGEROUS_GOODS_GATEWAY.URL}/dangerousgoodlookup/${inputString}`,
      {}
    );
  }

  /**
   * Filter packaging by given filter
   *
   * @param filter - The filter to be applied
   * @returns Filtered packaging
   */
  public getPackagingDetails(filter: PackagingFilter): Observable<Packaging[]> {
    return this.http.get<Packaging[]>(`${environment.ENDPOINTS.DANGEROUS_GOODS_GATEWAY.URL}/packaginglookup`, {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      params: this.mapFilterToHttpParams(filter),
    });
  }

  /**
   * Maps a filter to the specific HttpParams
   *
   * @param filter - The Filter to be map
   * @returns The mapped HttpParams object for the request
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private mapFilterToHttpParams(filter: PackagingFilter): any {
    const params: PackagingFilter = {
      query: filter.query,
    };
    if (filter.category) {
      params.category = filter.category;
    }
    if (filter.kind) {
      params.kind = filter.kind;
    }
    if (filter.material) {
      params.material = filter.material;
    }
    if (filter.typesOfMaterial) {
      params.typesOfMaterial = filter.typesOfMaterial;
    }
    return params;
  }

  /**
   * Calculate the Transport Category Point by sending a request to the backend
   *
   * @param calculationRequest - calculations
   * @returns Calculation
   */
  public calculateTransportCategoryPoints(calculationRequest: CalculationRequest): Observable<TransportCategoryPoints> {
    return this.http.put<TransportCategoryPoints>(
      `${environment.ENDPOINTS.DANGEROUS_GOODS_GATEWAY.URL}/businesslogic/adr`,
      calculationRequest
    );
  }
}
