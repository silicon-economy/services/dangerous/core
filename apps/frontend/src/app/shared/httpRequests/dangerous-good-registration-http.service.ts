/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { SaveDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { CreateDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/create-dangerous-good-registration.dto';

@Injectable()
export class DangerousGoodRegistrationHttpService {
  constructor(private http: HttpClient) {}

  /**
   * Retrieves a specific dangerous good registration by id.
   *
   * @param id - The id of the dangerous good registration.
   * @returns An observable that emits the dangerous good registration.
   */
  public getDangerousGoodRegistration(id: string): Observable<SaveDangerousGoodRegistrationDto> {
    return this.http.get<SaveDangerousGoodRegistrationDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.DANGEROUS_GOOD_REGISTRATION_PREFIX}/${id}`
    );
  }

  /**
   * Retrieves all dangerous good registrations.
   *
   * @returns An observable that emits an array of dangerous good registrations.
   */
  public getAllDangerousGoodRegistrations(): Observable<SaveDangerousGoodRegistrationDto[]> {
    return this.http.get<SaveDangerousGoodRegistrationDto[]>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.DANGEROUS_GOOD_REGISTRATION_PREFIX}`
    );
  }

  /**
   * Disables a dangerous good registration by id.
   *
   * @param id - The id of the dangerous good registration to disable.
   * @returns An observable that completes once the request is successful.
   */
  public disableDangerousGoodRegistration(id: string): Observable<void> {
    let headers = new HttpHeaders();
    headers = headers.set(
      `${environment.ENDPOINTS.DANGEROUS_GOODS_GATEWAY.HTTP_HEADERS.TYPE_OF_ACTION}`,
      `${environment.ENDPOINTS.DANGEROUS_GOODS_GATEWAY.HTTP_HEADER_VALUES.DISABLE_DANGEROUS_GOOD_REGISTRATION}`
    );
    return this.http.put<void>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.DANGEROUS_GOOD_REGISTRATION_PREFIX}/${id}`,
      null,
      { headers: headers }
    );
  }

  /**
   * Creates a new Dangerous Good Registration
   *
   * @param createDangerousGoodRegistration - The registration for Dangerous Good.
   * @returns the Observable CreateDangerousGoodRegistrationDto.
   */
  public createDangerousGoodRegistration(
    createDangerousGoodRegistration: CreateDangerousGoodRegistrationDto
  ): Observable<SaveDangerousGoodRegistrationDto> {
    return this.http.post<SaveDangerousGoodRegistrationDto>(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.DANGEROUS_GOOD_REGISTRATION_PREFIX}`,
      createDangerousGoodRegistration
    );
  }
}
