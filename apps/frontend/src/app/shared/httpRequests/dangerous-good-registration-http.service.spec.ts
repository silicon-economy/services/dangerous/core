/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { environment } from '@environments/environment';

describe('DangerousGoodRegistrationHttpService', () => {
  let service: DangerousGoodRegistrationHttpService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DangerousGoodRegistrationHttpService],
    });
    service = TestBed.inject(DangerousGoodRegistrationHttpService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get a dangerous good registration', () => {
    service.getDangerousGoodRegistration('1234').subscribe((registration: SaveDangerousGoodRegistrationDto) => {
      expect(registration.id).toBe('1234');
    });
    httpMock
      .expectOne(
        `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.DANGEROUS_GOOD_REGISTRATION_PREFIX}/1234`
      )
      .flush({ id: '1234' }, {});
  });

  it('should get all dangerous good registrations', () => {
    service.getAllDangerousGoodRegistrations().subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.DANGEROUS_GOOD_REGISTRATION_PREFIX}`
    );
  });

  it('should disable a dangerous good registration given by its id', () => {
    service.disableDangerousGoodRegistration('1234').subscribe();
    httpMock.expectOne(
      `${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.URL}${environment.ENDPOINTS.TRANSPORT_DOCUMENT_GATEWAY.DANGEROUS_GOOD_REGISTRATION_PREFIX}/1234`
    );
  });
});
