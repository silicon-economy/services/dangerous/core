/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatDialogModule } from '@angular/material/dialog';
import { AddressDetailsComponent } from '@shared/components/consignee-information/address-details.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [AddressDetailsComponent],
  exports: [AddressDetailsComponent],
  imports: [CommonModule, MatDialogModule, TranslateModule],
})
export class AddressDetailsModule {}
