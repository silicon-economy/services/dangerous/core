/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { Company } from '@core/api-interfaces/lib/dtos/frontend';

@Component({
  selector: 'app-address-details[_companies]',
  templateUrl: './address-details.component.html'
})
export class AddressDetailsComponent {
  @Input() _companies: Company[];

  get companies(): Company[] {
    return this._companies;
  }
}
