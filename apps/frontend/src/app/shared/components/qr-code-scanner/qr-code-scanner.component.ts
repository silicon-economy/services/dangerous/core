/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Output } from '@angular/core';
import { BarcodeFormat } from '@zxing/library';

@Component({
  selector: 'app-qr-code-scanner',
  templateUrl: './qr-code-scanner.component.html',
})
export class QrCodeScannerComponent {
  @Output() public scanned: EventEmitter<string> = new EventEmitter<string>();
  public allowedFormats: BarcodeFormat[] = [BarcodeFormat.CODE_128, BarcodeFormat.QR_CODE, BarcodeFormat.DATA_MATRIX];
  public cameras: MediaDeviceInfo[] = [];
  public deviceIndex = 0;
  public device!: MediaDeviceInfo;

  get displayToggleButton(): boolean {
    return this.cameras.length > 1;
  }

  /**
   * Output an error in console log
   *
   * @param event - The event that causes the error
   */
  public onError(event: Error): void {
    console.error(event);
  }

  /**
   * Switch available camera devices
   */
  public onToggleDevice(): void {
    if (this.deviceIndex === 0) {
      this.deviceIndex = this.cameras.length - 1;
    } else {
      this.deviceIndex--;
    }
    this.device = this.cameras[this.deviceIndex];
  }

  /**
   * Sets camera as current device w/ device info
   *
   * @param cameras - Available device
   */
  public onCamerasFound(cameras: MediaDeviceInfo[]): void {
    this.cameras = cameras;
    this.device = cameras[cameras.length - 1];
  }

  /**
   * Transfer barcode to event emitter
   *
   * @param barcode - Scanned barcode string
   */
  public onScanSuccess(barcode: string): void {
    this.scanned.emit(barcode);
  }
}
