/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QrCodeScannerComponent } from './qr-code-scanner.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';

describe('ShippingLabelScannerComponent', () => {
  let component: QrCodeScannerComponent;
  let fixture: ComponentFixture<QrCodeScannerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ZXingScannerModule],
      declarations: [QrCodeScannerComponent],
    });
    fixture = TestBed.createComponent(QrCodeScannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should output an error to the console', () => {
    const consoleSpy = jest.spyOn(console, 'error');
    const foobarError: Error = new Error('foobar');

    component.onError(foobarError);
    expect(consoleSpy).toHaveBeenCalledWith(foobarError);
  });

  it('should set the device index to cameras array length decremented by 1', () => {
    component.cameras.length = 1;
    component.deviceIndex = 0;

    component.onToggleDevice();
    expect(component.deviceIndex).toEqual(0);
  });

  it('should decrement the device index by 1', () => {
    component.deviceIndex = 1;

    component.onToggleDevice();
    expect(component.deviceIndex).toEqual(0);
  });

  it('should set camera as current device', () => {
    component.onCamerasFound([{} as MediaDeviceInfo]);
    expect(component.cameras).toEqual([{}]);
    expect(component.device).toEqual({});
  });

  it('should transfer barcode to event emitter', () => {
    const scannedSpy = jest.spyOn(component.scanned, 'emit');

    component.onScanSuccess('foobar');
    expect(scannedSpy).toHaveBeenCalledWith('foobar');
  });
});
