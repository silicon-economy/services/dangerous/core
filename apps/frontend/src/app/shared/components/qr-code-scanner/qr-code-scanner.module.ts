/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QrCodeScannerComponent } from './qr-code-scanner.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ZXingScannerModule } from '@zxing/ngx-scanner';

@NgModule({
  declarations: [QrCodeScannerComponent],
  exports: [QrCodeScannerComponent],
  imports: [CommonModule, MatIconModule, MatButtonModule, ZXingScannerModule],
})
export class QrCodeScannerModule {}
