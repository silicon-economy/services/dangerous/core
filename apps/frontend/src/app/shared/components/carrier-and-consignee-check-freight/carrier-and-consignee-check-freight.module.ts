/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarrierAndConsigneeCheckFreightComponent } from './carrier-and-consignee-check-freight.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { QrCodeScannerModule } from '@shared/components/qr-code-scanner/qr-code-scanner.module';
import {
  CarrierCheckOrderPositionModule
} from '@base/src/app/dialogs/carrier-check-freight-dialog/components/carrier-check-order-position/carrier-check-order-position.module';
import { OrderPositionResultsModule } from '@shared/components/order-position-results/order-position-results.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  CarrierAndConsigneeCheckFreightServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight-service.module';

@NgModule({
  declarations: [CarrierAndConsigneeCheckFreightComponent],
  imports: [
    CommonModule,
    MatProgressBarModule,
    MatIconModule,
    MatTabsModule,
    MatButtonModule,
    QrCodeScannerModule,
    CarrierCheckOrderPositionModule,
    OrderPositionResultsModule,
    TranslateModule,
    CarrierAndConsigneeCheckFreightServiceModule,
  ],
  exports: [CarrierAndConsigneeCheckFreightComponent],
})
export class CarrierAndConsigneeCheckFreightModule {}
