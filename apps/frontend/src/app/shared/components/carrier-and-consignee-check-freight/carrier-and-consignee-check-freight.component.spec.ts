/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrierAndConsigneeCheckFreightComponent } from './carrier-and-consignee-check-freight.component';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {
  mockTransportDocumentTransport
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import { MatTabsModule } from '@angular/material/tabs';
import { QrCodeScannerModule } from '@shared/components/qr-code-scanner/qr-code-scanner.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import {
  CarrierAndConsigneeCheckFreightServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight-service.module';
import { QrCodeLabel } from '@shared/interfaces/qr-code-label.interface';
import { of } from 'rxjs';
import {
  OrderPositionVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';

describe('CarrierCheckFreightComponent', () => {
  let component: CarrierAndConsigneeCheckFreightComponent;
  let fixture: ComponentFixture<CarrierAndConsigneeCheckFreightComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule,
        HttpClientTestingModule,
        MatDialogModule,
        MatProgressBarModule,
        MatTabsModule,
        QrCodeScannerModule,
        NoopAnimationsModule,
        MatIconModule,
        TranslateModule.forRoot(),
        CarrierAndConsigneeCheckFreightServiceModule,
      ],
      declarations: [CarrierAndConsigneeCheckFreightComponent],
      providers: [NotificationService, TransportDocumentHttpService, FeedbackDialogService],
    }).compileComponents();
    fixture = TestBed.createComponent(CarrierAndConsigneeCheckFreightComponent);
    component = fixture.componentInstance;
    component.document = mockTransportDocumentTransport;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select the order position to be evaluated', () => {
    const mockQrCodeLabel: QrCodeLabel = {
      orderId: 'foo',
      transportDocumentId: 'BP000001-20210819',
      orderPositionId: 1,
    };
    jest
      .spyOn(TransportDocumentHttpService.prototype, 'getTransportDocumentIdByOrderPositionId')
      .mockReturnValue(of('BP000001-20210819'));
    const selectedOrderPositionSpy = jest.spyOn(
      CarrierAndConsigneeCheckBehaviorSubjectService.prototype as never,
      'setDecideOrderPosition'
    );

    const checkedOrderPositions = [
      new OrderPositionVisualInspectionCarrierDto(undefined, undefined, undefined, undefined),
    ];
    component.onScan(JSON.stringify(mockQrCodeLabel), checkedOrderPositions);

    expect(selectedOrderPositionSpy).toHaveBeenCalledWith(mockQrCodeLabel.orderPositionId);
  });

  it('should output an qr code already scanned error', () => {
    const mockQrCodeLabel: QrCodeLabel = {
      orderId: 'foo',
      transportDocumentId: 'BP000001-20210819',
      orderPositionId: 1,
    };
    jest
      .spyOn(TransportDocumentHttpService.prototype, 'getTransportDocumentIdByOrderPositionId')
      .mockReturnValue(of('BP000001-20210819'));
    const selectedOrderPositionSpy = jest.spyOn(component, 'onOrderPositionButtonClicked');
    const notificationServiceErrorSpy = jest.spyOn(NotificationService.prototype, 'error');

    const checkedOrderPositions = [new OrderPositionVisualInspectionCarrierDto(1, true, true, 'foo')];
    component.onScan(JSON.stringify(mockQrCodeLabel), checkedOrderPositions);

    expect(selectedOrderPositionSpy).not.toHaveBeenCalled();
    expect(notificationServiceErrorSpy).toHaveBeenCalled();
  });

  it('should output an invalid qr code error', () => {
    const mockQrCodeLabel: QrCodeLabel = {
      orderId: 'foo',
      transportDocumentId: 'BP000002-20210819',
      orderPositionId: 1,
    };
    jest
      .spyOn(TransportDocumentHttpService.prototype, 'getTransportDocumentIdByOrderPositionId')
      .mockReturnValue(of('BP000002-20210819'));
    const selectedOrderPositionSpy = jest.spyOn(component, 'onOrderPositionButtonClicked');
    const notificationServiceErrorSpy = jest.spyOn(NotificationService.prototype, 'error');

    const checkedOrderPositions = [new OrderPositionVisualInspectionCarrierDto(1, true, true, 'foo')];
    component.onScan(JSON.stringify(mockQrCodeLabel), checkedOrderPositions);

    expect(selectedOrderPositionSpy).not.toHaveBeenCalled();
    expect(notificationServiceErrorSpy).toHaveBeenCalledWith(
      'dialogs.visualInspectionCarrier.notification.qrCodeAlreadyScanned'
    );
    expect(notificationServiceErrorSpy).toHaveBeenCalledWith('dialogs.visualInspectionConsignee.notification.error');
  });
});
