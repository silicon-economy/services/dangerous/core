/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  OrderPositionVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { QrCodeLabel } from '@shared/interfaces/qr-code-label.interface';
import {
  CarrierAndConsigneeCheckFreightService
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight.service';
import {
  OrderPositionVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/order-position-visual-inspection-consignee.dto';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';
import {
  CarrierAndConsigneeCheckProcessorService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-processor-service/carrier-and-consignee-check-processor.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-carrier-and-consignee-check-freight[document]',
  templateUrl: './carrier-and-consignee-check-freight.component.html',
  styleUrls: ['./carrier-and-consignee-check-freight.component.scss'],
})
export class CarrierAndConsigneeCheckFreightComponent {
  @Input() public document!: SaveGoodsDataDto;
  public selectedTabIndex = 0;

  constructor(
    private notificationService: NotificationService,
    private transportDocumentService: TransportDocumentHttpService,
    protected readonly carrierAndConsigneeCheckFreightService: CarrierAndConsigneeCheckFreightService,
    protected readonly carrierAndConsigneeCheckBehaviorSubjectService: CarrierAndConsigneeCheckBehaviorSubjectService,
    protected readonly carrierAndConsigneeCheckProcessorService: CarrierAndConsigneeCheckProcessorService,
    private readonly translateService: TranslateService
  ) {}

  /**
   * Set scanned orderPosition as to be checked order position.
   *
   * @param result - The scanned value from the QR code.
   * @param checkedOrderPositions - Checked order positions
   */
  public onScan(
    result: string,
    checkedOrderPositions: (OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto)[]
  ): void {
    this.translateService
      .get([
        'dialogs.visualInspectionConsignee.notification.error',
        'dialogs.visualInspectionCarrier.notification.qrCodeAlreadyScanned',
      ])
      .subscribe((translations: string[]) => {
        try {
          const qrObj: QrCodeLabel = JSON.parse(result) as QrCodeLabel;
          this.transportDocumentService
            .getTransportDocumentIdByOrderPositionId(qrObj.orderPositionId)
            .subscribe((transportDocumentId: string) => {
              if (
                transportDocumentId === this.document.id &&
                !checkedOrderPositions.find(
                  (orderPositionVisualInspectionCarrierDto: OrderPositionVisualInspectionCarrierDto): boolean =>
                    orderPositionVisualInspectionCarrierDto.orderPositionId === qrObj.orderPositionId
                )
              ) {
                this.onOrderPositionButtonClicked(qrObj.orderPositionId);
              } else if (transportDocumentId === this.document.id) {
                // Avoid double scanning of the same position
                this.notificationService.error(
                  String(translations['dialogs.visualInspectionCarrier.notification.qrCodeAlreadyScanned'])
                );
              } else {
                this.notificationService.error(
                  String(translations['dialogs.visualInspectionConsignee.notification.error'])
                );
              }
            });
        } catch (e) {
          this.notificationService.error(String(translations['dialogs.visualInspectionConsignee.notification.error']));
        }
      });
  }

  /**
   * Select the order position to be evaluated.
   *
   * @param orderPositionId - The id of the order position.
   */
  public onOrderPositionButtonClicked(orderPositionId: number): void {
    this.carrierAndConsigneeCheckBehaviorSubjectService.setDecideOrderPosition(orderPositionId);
  }
}
