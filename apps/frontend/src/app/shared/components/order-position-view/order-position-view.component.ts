/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { OrderPosition } from '@core/api-interfaces/lib/dtos/frontend';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';

@Component({
  selector: 'app-order-position-view[orderPosition][saveGoodsDataDto]',
  templateUrl: './order-position-view.component.html',
})
export class OrderPositionViewComponent {
  @Input() orderPosition!: OrderPosition | OrderPositionWithId;
  @Input() saveGoodsDataDto!: SaveGoodsDataDto;
}
