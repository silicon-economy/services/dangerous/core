/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderPositionViewRoutingModule } from './order-position-view-routing.module';
import { OrderPositionViewComponent } from './order-position-view.component';
import { YesNoPipe } from '@shared/components/order-position-view/pipes/yes-no.pipe';
import {
  LabelSymbolsComponent
} from '@shared/components/order-position-view/components/label-symbols/label-symbols.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [OrderPositionViewComponent, YesNoPipe, LabelSymbolsComponent],
  imports: [CommonModule, OrderPositionViewRoutingModule, TranslateModule, MatCardModule],
  exports: [OrderPositionViewComponent, LabelSymbolsComponent],
})
export class OrderPositionViewModule {}
