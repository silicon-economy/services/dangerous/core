/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { map, Observable } from 'rxjs';

/**
 * Converts a boolean value to a human-readable string.
 */
@Pipe({
  name: 'yesNo',
})
export class YesNoPipe implements PipeTransform {
  constructor(private readonly translateService: TranslateService) {}

  /**
   * Transforms a boolean value into a human-readable equivalent string 'yes' or 'no'.
   *
   * @param value - The boolean value to be transformed.
   * @returns An Observable that emits the translated string value.
   */
  public transform(value: boolean): Observable<string> {
    return this.translateService.get(['auxiliary.yes', 'auxiliary.no']).pipe(
      map((translations: string[]) => {
        return value ? String(translations['auxiliary.yes']) : String(translations['auxiliary.no']);
      })
    );
  }
}
