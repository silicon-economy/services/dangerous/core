/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { YesNoPipe } from './yes-no.pipe';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TestBed } from '@angular/core/testing';

describe('YesNoPipe', () => {
  let pipe: YesNoPipe;
  let translateService: TranslateService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [TranslateModule.forRoot()], providers: [TranslateService] });
    translateService = TestBed.inject(TranslateService);
    pipe = new YesNoPipe(translateService);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transforms "true" to "Ja" or "Yes', (done) => {
    pipe.transform(true).subscribe((translation: string) => {
      expect(translation).toBe('auxiliary.yes');
      done();
    });
  });

  it('transforms "false" to "Nein" or "No"', (done) => {
    pipe.transform(false).subscribe((translation: string) => {
      expect(translation).toBe('auxiliary.no');
      done();
    });
  });
});
