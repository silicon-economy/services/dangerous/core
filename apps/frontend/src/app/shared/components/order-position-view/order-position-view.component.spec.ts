/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderPositionViewComponent } from './order-position-view.component';

describe('OrderPositionViewComponent', () => {
  let component: OrderPositionViewComponent;
  let fixture: ComponentFixture<OrderPositionViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderPositionViewComponent],
    });
    fixture = TestBed.createComponent(OrderPositionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
