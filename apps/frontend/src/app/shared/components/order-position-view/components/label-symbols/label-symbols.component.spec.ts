/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  LabelSymbolsComponent
} from '@shared/components/order-position-view/components/label-symbols/label-symbols.component';
import {
  PackagingGroup
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/packaging-group.enum';
import {
  TransportCategory
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/transport-category.enum';

describe('LabelSymbolsComponent', () => {
  let component: LabelSymbolsComponent;
  let fixture: ComponentFixture<LabelSymbolsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LabelSymbolsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LabelSymbolsComponent);
    component = fixture.componentInstance;
    component.dangerousGood = {
      description: 'test',
      packingGroup: PackagingGroup.I,
      transportCategory: TransportCategory._0,
      tunnelRestrictionCode: undefined,
      unNumber: '2',
      casNumber: '1',
      label1: undefined,
      label2: undefined,
      label3: undefined,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
