/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { DangerousGood } from '@core/api-interfaces/lib/dtos/frontend';

@Component({
  selector: 'app-label-symbols[dangerousGood]',
  templateUrl: './label-symbols.component.html',
  styleUrls: ['./label-symbols.component.scss'],
})
export class LabelSymbolsComponent {
  @Input() public dangerousGood!: DangerousGood;
  @Input() public center = false;
}
