/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { CriterionCheckComponent } from '@shared/components/criterion-check/criterion-check.component';

@NgModule({
  declarations: [CriterionCheckComponent],
  imports: [CommonModule, MatExpansionModule, MatSlideToggleModule],
  exports: [CriterionCheckComponent],
})
export class CriterionCheckModule {}
