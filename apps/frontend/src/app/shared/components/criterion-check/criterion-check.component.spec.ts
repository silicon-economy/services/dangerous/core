/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CriterionCheckComponent } from '@shared/components/criterion-check/criterion-check.component';

describe('CriterionCheckComponent', () => {
  let component: CriterionCheckComponent;
  let fixture: ComponentFixture<CriterionCheckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CriterionCheckComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriterionCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
