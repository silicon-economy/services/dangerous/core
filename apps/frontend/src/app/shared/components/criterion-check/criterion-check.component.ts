/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-criterion-check[label][description]',
  templateUrl: './criterion-check.component.html',
  styleUrls: ['./criterion-check.component.scss'],
})
export class CriterionCheckComponent {
  @Input() label!: string;
  @Input() description!: string;
  @Input() hideToggle = false;
  @Input() accepted = false;
  @Output() toggleSwitched = new EventEmitter<boolean>();
  panelOpenState = false;

  /**
   * Emits the acceptance status of the given criterion.
   *
   * @param accepted - The new acceptance status of the given criterion.
   */
  public switchToggle(accepted: boolean): void {
    this.toggleSwitched.emit(accepted);
  }
}
