/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import {
  OrderPositionVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import {
  OrderPositionVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/order-position-visual-inspection-consignee.dto';
import {
  CarrierAndConsigneeCheckDistinctionService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-distinction-service/carrier-and-consignee-check-distinction.service';

@Component({
  selector: 'app-order-position-results',
  templateUrl: './order-position-results.component.html',
})
export class OrderPositionResultsComponent {
  @Input() orderPositions!: OrderPositionWithId[];
  @Input() orderPositionVisualInspectionDtos!: (
    | OrderPositionVisualInspectionCarrierDto
    | OrderPositionVisualInspectionConsigneeDto
  )[];
  public displayedColumns: string[] = [
    'spacer',
    'description',
    'package',
    'quantity',
    'individualAmount',
    'totalAmount',
    'transportPoints',
    'comment',
  ];

  constructor(
    private readonly carrierAndConsigneeCheckDistinctionService: CarrierAndConsigneeCheckDistinctionService
  ) {}

  /**
   * Calculates the total amount based on checked order positions.
   *
   * @returns The total amount.
   */
  get totalAmount(): number {
    return this.calculateTotalValue((orderPosition: OrderPositionWithId) => orderPosition.totalAmount);
  }

  /**
   * Calculates the total quantity based on checked order positions.
   *
   * @returns The total quantity.
   */
  get totalQuantity(): number {
    return this.calculateTotalValue((orderPosition: OrderPositionWithId) => orderPosition.quantity);
  }

  /**
   * Calculates the total transport points based on checked order positions.
   *
   * @returns The total transport points.
   */
  get totalTransportPoints(): number {
    return this.calculateTotalValue((orderPosition: OrderPositionWithId) => orderPosition.transportPoints);
  }

  /**
   * Returns the result of the check by the carrier for a given order position.
   *
   * @param orderPosition - The order position.
   * @returns The result for the given order position.
   */
  public getResult(orderPosition: OrderPositionWithId): boolean | undefined {
    const orderPositionCheck: OrderPositionVisualInspectionConsigneeDto | OrderPositionVisualInspectionCarrierDto =
      this.getOrderPositionCheck(orderPosition);
    if (this.carrierAndConsigneeCheckDistinctionService.isOrderPositionVisualInspectionCarrierDto(orderPositionCheck)) {
      const carrierCheck: OrderPositionVisualInspectionCarrierDto =
        orderPositionCheck as OrderPositionVisualInspectionCarrierDto;
      return carrierCheck.acceptLabeling && carrierCheck.acceptTransportability;
    } else if (
      this.carrierAndConsigneeCheckDistinctionService.isOrderPositionVisualInspectionConsigneeDto(orderPositionCheck)
    ) {
      const consigneeCheck: OrderPositionVisualInspectionConsigneeDto =
        orderPositionCheck as OrderPositionVisualInspectionConsigneeDto;
      return consigneeCheck.acceptIntactness && consigneeCheck.acceptQuantity;
    }
  }

  /**
   * Returns an order position check's comment (if it exists) or undefined otherwise.
   *
   * @param orderPosition - The order position.
   * @returns The comment for the given order position.
   */
  public getComment(orderPosition: OrderPositionWithId): string | undefined {
    const orderPositionCheck: OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto =
      this.getOrderPositionCheck(orderPosition);
    return orderPositionCheck?.comment;
  }

  /**
   * Retrieves the check for a given order position.
   *
   * @param orderPosition - The order position.
   * @returns The check for the given order position.
   */
  private getOrderPositionCheck(
    orderPosition: OrderPositionWithId
  ): OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto {
    return this.orderPositionVisualInspectionDtos.find(
      (check: OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto) =>
        check.orderPositionId === orderPosition.id
    );
  }

  /**
   * Calculates the total value based on checked order positions and a value selector function.
   *
   * @param valueSelector - A function to select the value from an order position.
   * @returns The total value.
   */
  private calculateTotalValue(valueSelector: (orderPosition: OrderPositionWithId) => number): number {
    return this.orderPositions
      .map((orderPosition: OrderPositionWithId) => {
        const orderPositionIndex: number = this.orderPositions.findIndex(
          (value: OrderPositionWithId) => value === orderPosition
        );
        if (this.orderPositionVisualInspectionDtos[orderPositionIndex]) {
          return valueSelector(orderPosition);
        } else {
          return 0;
        }
      })
      .reduce((a: number, b: number) => a + b);
  }
}
