/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderPositionResultsRoutingModule } from './order-position-results-routing.module';
import { OrderPositionResultsComponent } from './order-position-results.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [OrderPositionResultsComponent],
  imports: [
    CommonModule,
    OrderPositionResultsRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    TranslateModule,
  ],
  exports: [OrderPositionResultsComponent],
})
export class OrderPositionResultsModule {}
