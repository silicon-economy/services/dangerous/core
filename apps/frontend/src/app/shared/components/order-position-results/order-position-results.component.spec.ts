/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderPositionResultsComponent } from './order-position-results.component';
import { MatDialogModule } from '@angular/material/dialog';
import { TransportLabel } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/transport-label.enum';
import { PackagingGroup } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/packaging-group.enum';
import {
  TunnelRestrictionCode
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/tunnel-restriction-code.enum';
import {
  TransportCategory
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/transport-category.enum';
import { OrderPositionStatus } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/status.enum';
import { PackagingUnit } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/packaging-unit.enum';
import {
  OrderPositionVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import { TranslateModule } from '@ngx-translate/core';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import {
  CarrierAndConsigneeCheckDistinctionServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-distinction-service/carrier-and-consignee-check-distinction-service.module';

describe('OrderPositionResultsComponent', () => {
  let component: OrderPositionResultsComponent;
  let fixture: ComponentFixture<OrderPositionResultsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        TranslateModule.forRoot(),
        MatIconModule,
        MatTableModule,
        CarrierAndConsigneeCheckDistinctionServiceModule,
      ],
      declarations: [OrderPositionResultsComponent],
    });
    fixture = TestBed.createComponent(OrderPositionResultsComponent);
    component = fixture.componentInstance;
    component.orderPositions = [
      {
        package:
          'Großpackmittel (IBC) aus starrer Kunststoff (H) für flüssige Stoffe, mit äußerer Umhüllung aus Stahl (A)',
        packagingCode: '31HA1',
        quantity: 1,
        unit: PackagingUnit.L,
        individualAmount: 1000,
        polluting: false,
        transportPoints: 3000,
        totalAmount: 1000,
        id: 1,
        deviceId: '1',
        status: 'accepted_by_carrier',
        logEntries: [
          {
            status: OrderPositionStatus.accepted_by_carrier,
            date: 1629372601559,
            author: 'Bernd Beförderer',
            description: '',
            acceptanceCriteria: {
              comment: '',
              visualInspectionCarrierCriteria: {
                acceptTransportability: true,
                acceptLabeling: true,
              },
            },
          },
        ],
        dangerousGood: {
          unNumber: '1230',
          casNumber: '67-56-1',
          description: 'METHANOL',
          label1: TransportLabel._3,
          label2: TransportLabel._6_1,
          label3: TransportLabel._,
          packingGroup: PackagingGroup.II,
          tunnelRestrictionCode: TunnelRestrictionCode['(C/D)'],
          transportCategory: TransportCategory._0,
        },
      },
    ];
    component.orderPositionVisualInspectionDtos = [
      {
        orderPositionId: 1,
        acceptTransportability: true,
        acceptLabeling: true,
        comment: 'foo',
      },
      {
        orderPositionId: 2,
        acceptLabeling: true,
        acceptTransportability: true,
        comment: '',
      },
    ] as OrderPositionVisualInspectionCarrierDto[];
    registerLocaleData(localeDe, 'de-DE', localeDeExtra);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get a result', () => {
    const mock2 = { ...component.orderPositions[0] };
    mock2.id = 2;
    component.orderPositions.push(mock2);

    expect(component.getResult(component.orderPositions[0])).toBe(true);
    expect(component.getResult(mock2)).toBe(true);
  });

  it('should calculate the total amount correctly', () => {
    expect(component.totalAmount).toEqual(component.orderPositions[0].totalAmount * component.orderPositions.length);
  });

  it('should calculate the total quantity correctly', () => {
    expect(component.totalQuantity).toEqual(component.orderPositions[0].quantity * component.orderPositions.length);
  });

  it('should calculate the total transport points correctly', () => {
    expect(component.totalTransportPoints).toEqual(
      component.orderPositions[0].transportPoints * component.orderPositions.length
    );
  });
});
