/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentType } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';

@Injectable()
export class ScanDialogService {
  private dialogConfig: MatDialogConfig;

  constructor(private readonly matDialog: MatDialog) {
    this.dialogConfig = new MatDialogConfig();
    this.dialogConfig.disableClose = true;
    this.dialogConfig.panelClass = 'scan-dialog-container';
    this.dialogConfig.id = 'modal-scanner';
  }

  /**
   * Create dialog for dialog window
   *
   * @param component - The component which will be displayed
   * @param componentAttributes - The used attributes
   * @returns The dialog
   */
  public createDialog<T>(
    component: ComponentType<T>,
    componentAttributes?: { attributeName: string; attribute: SaveGoodsDataDto | OrderPositionWithId | string }[]
  ): MatDialogRef<T> {
    if (componentAttributes != undefined) {
      const data = { data: {} };
      componentAttributes.forEach((element) => {
        data.data = Object.assign(data.data, { [element.attributeName]: element.attribute });
      });
      this.dialogConfig = Object.assign(this.dialogConfig, data);
    }
    return this.matDialog.open<T>(component, this.dialogConfig);
  }
}
