/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { ScanDialogService } from './scan-dialog.service';

describe('ScanDialogService', () => {
  let service: ScanDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule],
      providers: [ScanDialogService],
    });
    service = TestBed.inject(ScanDialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
