/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  OrderPositionVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import {
  VisualInspectionCarrierCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/visual-inspection-carrier-criteria';
import { BehaviorSubject, map, Observable } from 'rxjs';
import {
  OrderPositionVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/order-position-visual-inspection-consignee.dto';
import {
  VisualInspectionConsigneeCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/visual-inspection-consignee-criteria';
import {
  CarrierAndConsigneeCheckDistinctionService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-distinction-service/carrier-and-consignee-check-distinction.service';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';
import {
  CarrierAndConsigneeCheckProcessorService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-processor-service/carrier-and-consignee-check-processor.service';

@Injectable()
export class CarrierAndConsigneeCheckFreightService {
  constructor(
    private readonly carrierAndConsigneeCheckIdentificationService: CarrierAndConsigneeCheckDistinctionService,
    private readonly carrierAndConsigneeCheckBehaviorSubjectService: CarrierAndConsigneeCheckBehaviorSubjectService,
    private readonly carrierAndConsigneeCheckProcessorService: CarrierAndConsigneeCheckProcessorService
  ) {}

  /**
   * Finds the order position based on the orderPositionId
   *
   * @param document - Transport document
   * @param orderPositionId - The order position id
   * @returns The found order position
   */
  public findOrderPosition(document: SaveGoodsDataDto, orderPositionId: number): OrderPositionWithId {
    return this.carrierAndConsigneeCheckProcessorService
      .getAllOrderPositions(document)
      .find((orderPosition: OrderPositionWithId): boolean => orderPosition.id == orderPositionId);
  }

  /**
   * Counts the total number of order positions in the given transport document.
   * Avoids division by zero.
   *
   * @param transportDocument - The transport document containing the freight.
   * @returns The count of order positions.
   */
  public getTotalOrderPositionsCount(transportDocument: SaveGoodsDataDto): number {
    return this.carrierAndConsigneeCheckProcessorService.getAllOrderPositions(transportDocument).length > 0
      ? this.carrierAndConsigneeCheckProcessorService.getAllOrderPositions(transportDocument).length
      : 1;
  }

  /**
   * Gets the unchecked order positions.
   *
   * @param transportDocument - The transport document containing the order positions.
   * @param checkedOrderPositions - The checked order positions.
   * @returns An array of unchecked order positions.
   */
  public getUncheckedOrderPositions(
    transportDocument: SaveGoodsDataDto,
    checkedOrderPositions: (OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto)[]
  ): OrderPositionWithId[] {
    const allOrderPositions: OrderPositionWithId[] =
      this.carrierAndConsigneeCheckProcessorService.getAllOrderPositions(transportDocument);
    return allOrderPositions.filter((orderPosition: OrderPositionWithId) => {
      return !this.isCheckedOrderPosition(orderPosition, checkedOrderPositions);
    });
  }

  /**
   * Checks if an order position is present in the checked order positions.
   *
   * @param orderPosition - The order position to be checked.
   * @param checkedOrderPositions - The checked order positions.
   * @returns True if the order position is checked, false otherwise.
   */
  private isCheckedOrderPosition(
    orderPosition: OrderPositionWithId,
    checkedOrderPositions: (OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto)[]
  ): boolean {
    return checkedOrderPositions.some(
      (checkedOrderPosition: OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto) => {
        return checkedOrderPosition.orderPositionId === orderPosition.id;
      }
    );
  }

  /**
   * Calculates the progress percentage based on the count of checked order positions.
   *
   * @param transportDocument - The transport document containing the freight.
   * @returns The progress percentage.
   */
  public calculateProgress(transportDocument: SaveGoodsDataDto): Observable<number> {
    return this.carrierAndConsigneeCheckBehaviorSubjectService.getCheckedOrderPositions().pipe(
      map(
        (
          checkedOrderPositions: (OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto)[]
        ) => {
          const totalOrderPositionsCount: number = this.getTotalOrderPositionsCount(transportDocument);
          return (100 * checkedOrderPositions.length) / totalOrderPositionsCount;
        }
      )
    );
  }

  /**
   * Checks if all the checked order positions have successful checks for labeling and transportability.
   *
   * @param checkedOrderPositions - The checked order positions.
   * @returns True if all checks are successful, false otherwise.
   */
  public areAllChecksSuccessful(
    checkedOrderPositions: (OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto)[]
  ): boolean {
    return checkedOrderPositions.every(
      (
        orderPositionVisualInspectionDto:
          | OrderPositionVisualInspectionCarrierDto
          | OrderPositionVisualInspectionConsigneeDto
      ): boolean => {
        if (
          this.carrierAndConsigneeCheckIdentificationService.isOrderPositionVisualInspectionCarrierDto(
            orderPositionVisualInspectionDto
          )
        ) {
          const carrierCheck: OrderPositionVisualInspectionCarrierDto =
            orderPositionVisualInspectionDto as OrderPositionVisualInspectionCarrierDto;
          return carrierCheck.acceptLabeling && carrierCheck.acceptTransportability;
        } else if (
          this.carrierAndConsigneeCheckIdentificationService.isOrderPositionVisualInspectionConsigneeDto(
            orderPositionVisualInspectionDto
          )
        ) {
          const carrierCheck: OrderPositionVisualInspectionConsigneeDto =
            orderPositionVisualInspectionDto as OrderPositionVisualInspectionConsigneeDto;
          return carrierCheck.acceptIntactness && carrierCheck.acceptQuantity;
        }
      }
    );
  }

  /**
   * Completes the visual inspection carrier check for the transport document,
   * stores the results into the blockchain, and performs navigation based on the outcome.
   *
   * @param transportDocument - The transport document to complete the checks for.
   */
  public completeVisualInspectionChecks(transportDocument: SaveGoodsDataDto): void {
    const checkedOrderPositionsBehaviourSubject: BehaviorSubject<
      (OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto)[]
    > = this.carrierAndConsigneeCheckBehaviorSubjectService.getCheckedOrderPositions();

    if (
      this.carrierAndConsigneeCheckIdentificationService.isOrderPositionVisualInspectionCarrierDtoArray(
        checkedOrderPositionsBehaviourSubject
      )
    ) {
      // Carrier check
      this.carrierAndConsigneeCheckProcessorService.completeVisualInspectionChecksForCarrier(transportDocument);
    } else if (
      this.carrierAndConsigneeCheckIdentificationService.isOrderPositionVisualInspectionConsigneeDtoArray(
        checkedOrderPositionsBehaviourSubject
      )
    ) {
      // Consignee check
      this.carrierAndConsigneeCheckProcessorService.completeVisualInspectionChecksForConsignee(transportDocument);
    }
  }

  /**
   * Handle the check (visual inspection) result of a specific order position.
   *
   * @param orderPositionId - Order position id
   * @param visualInspectionCarrierCriteria - Object containing the transport criteria
   */
  public onCheckedCarrier(
    orderPositionId: number,
    visualInspectionCarrierCriteria: VisualInspectionCarrierCriteria
  ): void {
    const orderPositionVisualInspectionCarrierDto: OrderPositionVisualInspectionCarrierDto =
      new OrderPositionVisualInspectionCarrierDto(
        orderPositionId,
        visualInspectionCarrierCriteria.acceptTransportability,
        visualInspectionCarrierCriteria.acceptLabeling,
        visualInspectionCarrierCriteria.comment
      );

    this.carrierAndConsigneeCheckBehaviorSubjectService.setCheckedOrderPositions(
      orderPositionVisualInspectionCarrierDto
    );
    this.carrierAndConsigneeCheckBehaviorSubjectService.setDecideOrderPosition(undefined);
  }

  /**
   * Handle the check (visual inspection) result of a specific order position.
   *
   * @param orderPositionId - Order position id
   * @param visualInspectionConsigneeCriteria - Object containing the transport criteria
   */
  public onCheckedConsignee(
    orderPositionId: number,
    visualInspectionConsigneeCriteria: VisualInspectionConsigneeCriteria
  ): void {
    const orderPositionVisualInspectionConsigneeDto: OrderPositionVisualInspectionConsigneeDto =
      new OrderPositionVisualInspectionConsigneeDto(
        orderPositionId,
        visualInspectionConsigneeCriteria.acceptQuantity,
        visualInspectionConsigneeCriteria.acceptIntactness,
        visualInspectionConsigneeCriteria.comment
      );

    this.carrierAndConsigneeCheckBehaviorSubjectService.setCheckedOrderPositions(
      orderPositionVisualInspectionConsigneeDto
    );
    this.carrierAndConsigneeCheckBehaviorSubjectService.setDecideOrderPosition(undefined);
  }
}
