/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { CarrierAndConsigneeCheckBehaviorSubjectService } from './carrier-and-consignee-check-behavior-subject.service';

describe('CarrierAndConsigneeCheckBehaviorSubjectService', () => {
  let service: CarrierAndConsigneeCheckBehaviorSubjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [CarrierAndConsigneeCheckBehaviorSubjectService] });
    service = TestBed.inject(CarrierAndConsigneeCheckBehaviorSubjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
