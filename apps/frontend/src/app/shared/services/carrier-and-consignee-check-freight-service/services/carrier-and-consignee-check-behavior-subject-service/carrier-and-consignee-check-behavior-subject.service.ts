/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import {
  OrderPositionVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import {
  OrderPositionVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/order-position-visual-inspection-consignee.dto';

@Injectable()
export class CarrierAndConsigneeCheckBehaviorSubjectService {
  public decideOrderPosition: Subject<number> = new Subject<number>();
  public checkedOrderPositions: BehaviorSubject<
    (OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto)[]
  > = new BehaviorSubject<(OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto)[]>([]);

  /**
   * Sets the decide order position.
   *
   * @param orderPositionId - The id of the order position.
   */
  public setDecideOrderPosition(orderPositionId: number): void {
    this.decideOrderPosition.next(orderPositionId);
  }

  /**
   * Gets the decide order position.
   *
   * @returns A Subject that emits the order position id.
   */
  public getDecideOrderPosition(): Subject<number> {
    return this.decideOrderPosition;
  }

  /**
   * Sets the checked order positions.
   *
   * @param orderPositionVisualInspectionCarrierDto - The OrderPositionVisualInspectionCarrierDto or
   * OrderPositionVisualInspectionConsigneeDto to be added to the checked order positions.
   */
  public setCheckedOrderPositions(
    orderPositionVisualInspectionCarrierDto:
      | OrderPositionVisualInspectionCarrierDto
      | OrderPositionVisualInspectionConsigneeDto
  ): void {
    const orderPositionVisualInspectionCarrierDtos = [
      ...this.getCheckedOrderPositions().getValue(),
      orderPositionVisualInspectionCarrierDto,
    ];
    this.checkedOrderPositions.next(orderPositionVisualInspectionCarrierDtos);
  }

  /**
   * Gets the checked order positions.
   *
   * @returns A BehaviorSubject that emits the checked order positions.
   */
  public getCheckedOrderPositions(): BehaviorSubject<
    (OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto)[]
  > {
    return this.checkedOrderPositions;
  }
}
