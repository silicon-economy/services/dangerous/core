/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import {
  OrderPositionVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import {
  OrderPositionVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/order-position-visual-inspection-consignee.dto';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class CarrierAndConsigneeCheckDistinctionService {
  /**
   * Checks if the given object is an instance of OrderPositionVisualInspectionCarrierDto.
   *
   * @param orderPositionVisualInspectionCarrierDto - The object to check.
   * @returns True if the object is an instance of OrderPositionVisualInspectionCarrierDto, false otherwise.
   */
  public isOrderPositionVisualInspectionCarrierDto(
    orderPositionVisualInspectionCarrierDto:
      | OrderPositionVisualInspectionCarrierDto
      | OrderPositionVisualInspectionConsigneeDto
  ): boolean {
    return (Object.prototype.hasOwnProperty.call(orderPositionVisualInspectionCarrierDto, 'acceptLabeling') &&
      Object.prototype.hasOwnProperty.call(
        orderPositionVisualInspectionCarrierDto,
        'acceptTransportability'
      )) as boolean;
  }

  /**
   * Checks if the given object is an instance of OrderPositionVisualInspectionConsigneeDto.
   *
   * @param orderPositionVisualInspectionDto - The object to check.
   * @returns True if the object is an instance of OrderPositionVisualInspectionConsigneeDto, false otherwise.
   */
  public isOrderPositionVisualInspectionConsigneeDto(
    orderPositionVisualInspectionDto:
      | OrderPositionVisualInspectionCarrierDto
      | OrderPositionVisualInspectionConsigneeDto
  ): boolean {
    return (Object.prototype.hasOwnProperty.call(orderPositionVisualInspectionDto, 'acceptIntactness') &&
      Object.prototype.hasOwnProperty.call(orderPositionVisualInspectionDto, 'acceptQuantity')) as boolean;
  }

  /**
   * Checks if all elements in the checked order positions array are instances of OrderPositionVisualInspectionCarrierDto.
   *
   * @param checkedOrderPositions - Checked order positions behavior subject
   * @returns True if all elements are instances of OrderPositionVisualInspectionCarrierDto, false otherwise.
   */
  public isOrderPositionVisualInspectionCarrierDtoArray(
    checkedOrderPositions: BehaviorSubject<
      (OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto)[]
    >
  ): boolean {
    return checkedOrderPositions
      .getValue()
      .every(
        (orderPositionCheck: OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto) => {
          return this.isOrderPositionVisualInspectionCarrierDto(orderPositionCheck);
        }
      );
  }

  /**
   * Checks if all elements in the checked order positions array are instances of OrderPositionVisualInspectionConsigneeDto.
   *
   * @param checkedOrderPositions - Checked order positions behavior subject
   * @returns True if all elements are instances of OrderPositionVisualInspectionConsigneeDto, false otherwise.
   */
  public isOrderPositionVisualInspectionConsigneeDtoArray(
    checkedOrderPositions: BehaviorSubject<
      (OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto)[]
    >
  ): boolean {
    return checkedOrderPositions
      .getValue()
      .every(
        (orderPositionCheck: OrderPositionVisualInspectionCarrierDto | OrderPositionVisualInspectionConsigneeDto) => {
          return this.isOrderPositionVisualInspectionConsigneeDto(orderPositionCheck);
        }
      );
  }
}
