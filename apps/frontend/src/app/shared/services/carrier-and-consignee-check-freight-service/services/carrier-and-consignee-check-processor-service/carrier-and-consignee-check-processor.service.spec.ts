/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { CarrierAndConsigneeCheckProcessorService } from './carrier-and-consignee-check-processor.service';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { MatDialogModule } from '@angular/material/dialog';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import {
  CarrierAndConsigneeCheckBehaviorSubjectServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject-service.module';
import {
  mockTransportDocumentTransport,
  mockTransportDocumentVehicleAccepted,
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import {
  OrderPositionVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import {
  UpdateVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';
import {
  OrderPositionVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/order-position-visual-inspection-consignee.dto';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';
import {
  UpdateVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import { of, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  OrderPositionStatus
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';
import { LogEntry } from '@core/api-interfaces/lib/dtos/frontend/transport-document/log-entry';
import {
  VisualInspectionCarrierCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/visual-inspection-carrier-criteria';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';

describe('CarrierAndConsigneeCheckProcessorService', () => {
  let service: CarrierAndConsigneeCheckProcessorService;
  let carrierAndConsigneeCheckBehaviorSubjectService: CarrierAndConsigneeCheckBehaviorSubjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule,
        HttpClientTestingModule,
        MatDialogModule,
        TranslateModule.forRoot(),
        CarrierAndConsigneeCheckBehaviorSubjectServiceModule,
        RouterTestingModule.withRoutes([
          {
            path: 'transport-documents/',
            redirectTo: '',
          },
        ]),
      ],
      providers: [
        NotificationService,
        CarrierAndConsigneeCheckProcessorService,
        TransportDocumentHttpService,
        FeedbackDialogService,
      ],
    });

    service = TestBed.inject(CarrierAndConsigneeCheckProcessorService);
    carrierAndConsigneeCheckBehaviorSubjectService = TestBed.inject(CarrierAndConsigneeCheckBehaviorSubjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all order positions', () => {
    expect(service.getAllOrderPositions(mockTransportDocumentVehicleAccepted)).toEqual(
      mockTransportDocumentVehicleAccepted.freight.orders[0].orderPositions
    );
  });

  it('should retrieve a specific order position from the given transport document', () => {
    expect(service.getOrderPositionById(mockTransportDocumentVehicleAccepted, 1)).toEqual(
      mockTransportDocumentVehicleAccepted.freight.orders[0].orderPositions[0]
    );
  });

  it('should throw error if the order position is not found', () => {
    expect(service.getOrderPositionById(mockTransportDocumentVehicleAccepted, 2)).toEqual(undefined);
  });

  it('should retrieve the index of the order that contains the specified order position in the transport document', () => {
    expect(
      service['getOrderIndex'](
        mockTransportDocumentVehicleAccepted,
        new OrderPositionVisualInspectionCarrierDto(1, true, true, '')
      )
    ).toEqual(0);
  });

  it('should retrieve the index of the order position within the specified order in the transport document', () => {
    expect(
      service['getOrderPositionIndex'](
        mockTransportDocumentVehicleAccepted,
        0,
        new OrderPositionVisualInspectionCarrierDto(1, true, true, '')
      )
    ).toEqual(0);
  });

  it('should complete visual inspection checks for consignee', () => {
    const updateVisualInspectionConsigneeSpy = jest.spyOn(service as never, 'updateVisualInspectionConsignee');
    const checkedOrderPositionsSpy = jest
      .spyOn(carrierAndConsigneeCheckBehaviorSubjectService.checkedOrderPositions, 'getValue')
      .mockReturnValue([
        new OrderPositionVisualInspectionConsigneeDto(
          mockTransportDocumentTransport.freight?.orders[0].orderPositions[0].id,
          true,
          true,
          ''
        ),
      ]);

    const updateVisualInspectionConsigneeDto: UpdateVisualInspectionConsigneeDto = {
      transportDocumentId: mockTransportDocumentTransport.id,
      order: {
        orderId: mockTransportDocumentTransport.freight.orders[0].id,
        orderPositions: [
          new OrderPositionVisualInspectionConsigneeDto(
            mockTransportDocumentTransport.freight?.orders[0].orderPositions[0].id,
            true,
            true,
            ''
          ),
        ],
      },
    };

    service.completeVisualInspectionChecksForConsignee(mockTransportDocumentTransport);
    expect(updateVisualInspectionConsigneeSpy).toHaveBeenCalledWith(updateVisualInspectionConsigneeDto);
    expect(checkedOrderPositionsSpy).toHaveBeenCalled();
  });

  it('should update visual inspection carrier and handle success and navigate to /transports-documents on next', () => {
    const updateVisualInspectionCarrierSpy = jest
      .spyOn(service['transportDocumentHttpService'], 'updateVisualInspectionCarrier')
      .mockReturnValue(of(mockTransportDocumentTransport));
    const handleSuccessSpy = jest.spyOn(FeedbackDialogService.prototype, 'handleSuccess');
    const translateServiceSpy = jest.spyOn(TranslateService.prototype, 'get').mockReturnValueOnce(of('foo'));
    const notificationServiceSuccessSpy = jest.spyOn(NotificationService.prototype, 'success');
    const routerNavigateSpy = jest.spyOn(Router.prototype, 'navigate');

    const updateVisualInspectionCarrierDto: UpdateVisualInspectionCarrierDto = {
      transportDocumentId: 'BP000006-202338',
      orders: [
        {
          orderId: '2a9779e1-e74e-4f1b-89e9-615bcfc051ff',
          orderPositions: [
            {
              orderPositionId: 11,
              acceptLabeling: true,
              acceptTransportability: true,
              comment: '',
            },
            {
              orderPositionId: 13,
              acceptLabeling: true,
              acceptTransportability: true,
              comment: '',
            },
          ],
        },
        {
          orderId: 'f08368f3-f050-4ff8-ab0b-4b2caabf6ad6',
          orderPositions: [
            {
              orderPositionId: 15,
              acceptLabeling: true,
              acceptTransportability: true,
              comment: '',
            },
          ],
        },
      ],
    };
    jest.spyOn(window, 'window', 'get');

    service['updateVisualInspectionCarrier'](updateVisualInspectionCarrierDto);

    expect(updateVisualInspectionCarrierSpy).toHaveBeenCalled();
    expect(handleSuccessSpy).toHaveBeenCalled();
    expect(translateServiceSpy).toHaveBeenCalledWith('dialogs.visualInspectionCarrier.notification.success');
    expect(notificationServiceSuccessSpy).toHaveBeenCalled();
    expect(routerNavigateSpy).toHaveBeenCalledWith(['/transport-documents']);
  });

  it('should throw an error when trying to update visual inspection carrier', () => {
    const updateVisualInspectionCarrierSpy = jest
      .spyOn(service['transportDocumentHttpService'], 'updateVisualInspectionCarrier')
      .mockReturnValue(
        throwError(() => {
          return new Error('');
        })
      );
    const handleErrorSpy = jest.spyOn(FeedbackDialogService.prototype, 'handleError');
    const translateServiceSpy = jest.spyOn(TranslateService.prototype, 'get').mockReturnValue(of('foo'));
    const notificationServiceErrorSpy = jest.spyOn(NotificationService.prototype, 'error');
    jest.spyOn(window, 'window', 'get');

    service['updateVisualInspectionCarrier'](undefined);

    expect(updateVisualInspectionCarrierSpy).toHaveBeenCalled();
    expect(handleErrorSpy).toHaveBeenCalled();
    expect(translateServiceSpy).toHaveBeenCalledWith('dialogs.visualInspectionCarrier.notification.error');
    expect(notificationServiceErrorSpy).toHaveBeenCalledWith('foo');
  });

  it('should update visual inspection consignee and handle success', () => {
    const updateVisualInspectionConsigneeSpy = jest
      .spyOn(service['transportDocumentHttpService'], 'updateVisualInspectionConsignee')
      .mockReturnValue(of(mockTransportDocumentTransport));
    const handleSuccessSpy = jest.spyOn(FeedbackDialogService.prototype, 'handleSuccess');
    const translateServiceSpy = jest.spyOn(TranslateService.prototype, 'get').mockReturnValue(of('foo'));
    const notificationServiceSuccessSpy = jest.spyOn(NotificationService.prototype, 'success');

    const updateVisualInspectionConsigneeDto: UpdateVisualInspectionConsigneeDto = {
      transportDocumentId: 'BP000006-202338',
      order: {
        orderId: '2a9779e1-e74e-4f1b-89e9-615bcfc051ff',
        orderPositions: [
          {
            orderPositionId: 11,
            acceptIntactness: true,
            acceptQuantity: true,
            comment: '',
          },
          {
            orderPositionId: 13,
            acceptIntactness: true,
            acceptQuantity: true,
            comment: '',
          },
        ],
      },
    };
    jest.spyOn(window, 'window', 'get');

    service['updateVisualInspectionConsignee'](updateVisualInspectionConsigneeDto);

    expect(updateVisualInspectionConsigneeSpy).toHaveBeenCalled();
    expect(handleSuccessSpy).toHaveBeenCalled();
    expect(translateServiceSpy).toHaveBeenCalledWith('dialogs.visualInspectionConsignee.notification.success');
    expect(notificationServiceSuccessSpy).toHaveBeenCalled();
  });

  it('should throw an error when trying to update visual inspection consignee', () => {
    const updateVisualInspectionConsigneeSpy = jest
      .spyOn(service['transportDocumentHttpService'], 'updateVisualInspectionConsignee')
      .mockReturnValue(
        throwError(() => {
          return new Error('');
        })
      );
    const handleErrorSpy = jest.spyOn(FeedbackDialogService.prototype, 'handleError');
    const translateServiceSpy = jest.spyOn(TranslateService.prototype, 'get').mockReturnValue(of('foo'));
    const notificationServiceErrorSpy = jest.spyOn(NotificationService.prototype, 'error');
    jest.spyOn(window, 'window', 'get');

    service['updateVisualInspectionConsignee'](undefined);

    expect(updateVisualInspectionConsigneeSpy).toHaveBeenCalled();
    expect(handleErrorSpy).toHaveBeenCalled();
    expect(translateServiceSpy).toHaveBeenCalledWith('dialogs.visualInspectionCarrier.notification.error');
    expect(notificationServiceErrorSpy).toHaveBeenCalled();
  });

  it('should merge order position check results into a transport document', () => {
    const mergeOrderPositionCheckResultsIntoTransportDocumentSpy = jest.spyOn(
      CarrierAndConsigneeCheckProcessorService.prototype as never,
      'mergeOrderPositionCheckResultsIntoTransportDocument'
    );
    const checkedOrderPositionsMock: OrderPositionVisualInspectionCarrierDto[] = [
      new OrderPositionVisualInspectionCarrierDto(1, true, true, ''),
    ];
    jest.useFakeTimers().setSystemTime(1705495282753);

    service['mergeOrderPositionCheckResultsIntoTransportDocument'](
      mockTransportDocumentVehicleAccepted,
      checkedOrderPositionsMock
    );

    const mockTransportDocument: SaveGoodsDataDto = structuredClone(mockTransportDocumentVehicleAccepted);
    mockTransportDocument.freight.orders[0].orderPositions[0].logEntries.push(
      new LogEntry(
        OrderPositionStatus.visual_inspection_carrier_accepted,
        new Date().valueOf(),
        undefined,
        undefined,
        new VisualInspectionCarrierCriteria(
          checkedOrderPositionsMock[0].acceptTransportability,
          checkedOrderPositionsMock[0].acceptLabeling,
          checkedOrderPositionsMock[0].comment
        )
      )
    );

    expect(mergeOrderPositionCheckResultsIntoTransportDocumentSpy).toHaveReturnedWith(mockTransportDocument);
  });

  it('should parse single order position visual inspection carrier dto', () => {
    const mergeOrderPositionCheckResultsIntoTransportDocumentSpy = jest.spyOn(
      CarrierAndConsigneeCheckProcessorService.prototype as never,
      'parseSingleOrderPositionVisualInspectionCarrierDto'
    );
    const mockOrderWithId: OrderWithId = mockTransportDocumentVehicleAccepted.freight.orders[0];
    const secondOrderPosition: OrderPositionWithId = structuredClone(mockOrderWithId.orderPositions[0]);
    secondOrderPosition.id = 3;
    mockOrderWithId.orderPositions.push(secondOrderPosition);
    jest.useFakeTimers().setSystemTime(1705495282753);

    service['parseSingleOrderPositionVisualInspectionCarrierDto'](mockOrderWithId);

    const mockOrderPositionVisualInspectionCarrierDtos: OrderPositionVisualInspectionCarrierDto[] = [
      new OrderPositionVisualInspectionCarrierDto(mockOrderWithId.orderPositions[0].id, false, false, undefined),
      new OrderPositionVisualInspectionCarrierDto(mockOrderWithId.orderPositions[1].id, false, false, undefined),
    ];

    expect(mergeOrderPositionCheckResultsIntoTransportDocumentSpy).toHaveReturnedWith(
      mockOrderPositionVisualInspectionCarrierDtos
    );
  });
});
