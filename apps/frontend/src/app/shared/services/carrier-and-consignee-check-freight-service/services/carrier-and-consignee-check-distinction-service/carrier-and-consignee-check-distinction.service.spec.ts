/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { CarrierAndConsigneeCheckDistinctionService } from './carrier-and-consignee-check-distinction.service';

describe('CarrierAndConsigneeCheckDistinctionService', () => {
  let service: CarrierAndConsigneeCheckDistinctionService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [CarrierAndConsigneeCheckDistinctionService] });
    service = TestBed.inject(CarrierAndConsigneeCheckDistinctionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
