/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import {
  OrderPositionVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  OrderVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-visual-inspection-carrier.dto';
import {
  UpdateVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/update-visual-inspection-consignee.dto';
import {
  OrderPositionVisualInspectionConsigneeDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-consignee/order-position-visual-inspection-consignee.dto';
import { LogEntry } from '@core/api-interfaces/lib/dtos/frontend/transport-document/log-entry';
import {
  VisualInspectionCarrierCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/visual-inspection-carrier-criteria';
import {
  UpdateVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/update-visual-inspection-carrier.dto';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { Router } from '@angular/router';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { TranslateService } from '@ngx-translate/core';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';
import {
  OrderPositionStatus
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

@Injectable()
export class CarrierAndConsigneeCheckProcessorService {
  constructor(
    private readonly transportDocumentHttpService: TransportDocumentHttpService,
    private dialogService: FeedbackDialogService,
    private router: Router,
    private readonly notificationService: NotificationService,
    private readonly translateService: TranslateService,
    private readonly carrierAndConsigneeCheckBehaviorSubjectService: CarrierAndConsigneeCheckBehaviorSubjectService,
    private feedbackDialogService: FeedbackDialogService
  ) {}

  /**
   * Completes the visual inspection checks for a carrier.
   *
   * @param transportDocument - The transport document.
   */
  public completeVisualInspectionChecksForCarrier(transportDocument: SaveGoodsDataDto): void {
    const modifiedTransportDocument: SaveGoodsDataDto = this.mergeOrderPositionCheckResultsIntoTransportDocument(
      transportDocument,
      this.carrierAndConsigneeCheckBehaviorSubjectService
        .getCheckedOrderPositions()
        .getValue() as OrderPositionVisualInspectionCarrierDto[]
    );
    this.dialogService.showDialog();
    const updateVisualInspectionCarrierDto: UpdateVisualInspectionCarrierDto =
      this.createUpdateVisualInspectionCarrierDto(transportDocument, modifiedTransportDocument);
    this.updateVisualInspectionCarrier(updateVisualInspectionCarrierDto);
  }

  /**
   * Completes the visual inspection checks for a consignee.
   *
   * @param transportDocument - The transport document.
   */
  public completeVisualInspectionChecksForConsignee(transportDocument: SaveGoodsDataDto): void {
    const updateVisualInspectionConsigneeDto: UpdateVisualInspectionConsigneeDto = {
      transportDocumentId: transportDocument.id,
      order: {
        orderId: transportDocument.freight?.orders[0].id,
        orderPositions:
          this.carrierAndConsigneeCheckBehaviorSubjectService.checkedOrderPositions.getValue() as OrderPositionVisualInspectionConsigneeDto[],
      },
    };
    this.updateVisualInspectionConsignee(updateVisualInspectionConsigneeDto);
  }

  /**
   * Creates the update visual inspection carrier dto.
   *
   * @param transportDocument - Transport document.
   * @param modifiedTransportDocument - The modified transport document.
   * @returns The update visual inspection carrier dto.
   */
  private createUpdateVisualInspectionCarrierDto(
    transportDocument: SaveGoodsDataDto,
    modifiedTransportDocument: SaveGoodsDataDto
  ): UpdateVisualInspectionCarrierDto {
    return {
      transportDocumentId: transportDocument.id,
      orders: this.parseOrderVisualInspectionCarrierDto(modifiedTransportDocument),
    };
  }

  /**
   * Updates the visual inspection carrier.
   *
   * @param updateVisualInspectionCarrierDto - The update visual inspection carrier dto.
   */
  private updateVisualInspectionCarrier(updateVisualInspectionCarrierDto: UpdateVisualInspectionCarrierDto): void {
    this.transportDocumentHttpService.updateVisualInspectionCarrier(updateVisualInspectionCarrierDto).subscribe({
      next: (): void => {
        this.dialogService.handleSuccess();
        this.translateService
          .get('dialogs.visualInspectionCarrier.notification.success')
          .subscribe((translation: string) => {
            void this.router.navigate(['/transport-documents']);
            window.location.reload();
            this.notificationService.success(translation);
          });
      },
      error: (): void => {
        this.dialogService.handleError();
        this.translateService
          .get('dialogs.visualInspectionCarrier.notification.error')
          .subscribe((translation: string) => {
            this.notificationService.error(translation);
          });
      },
    });
  }

  /**
   * Updates the visual inspection consignee.
   *
   * @param updateVisualInspectionConsigneeDto - The update visual inspection consignee dto.
   */
  private updateVisualInspectionConsignee(
    updateVisualInspectionConsigneeDto: UpdateVisualInspectionConsigneeDto
  ): void {
    this.feedbackDialogService.showDialog()
    this.transportDocumentHttpService.updateVisualInspectionConsignee(updateVisualInspectionConsigneeDto).subscribe({
      next: (): void => {
        this.dialogService.handleSuccess();
        this.translateService
          .get('dialogs.visualInspectionConsignee.notification.success')
          .subscribe((translation: string) => {
            this.notificationService.success(translation);
            window.location.reload();
          });
      },
      error: (): void => {
        this.dialogService.handleError();
        this.translateService
          .get('dialogs.visualInspectionCarrier.notification.error')
          .subscribe((translation: string) => {
            this.notificationService.error(translation);
          });
      },
    });
  }

  /**
   * Merges the order position check results into the transport document and returns the modified document.
   *
   * @param transportDocument - The original transport document.
   * @param checkedOrderPositions - The checked order positions from the visual inspection carrier.
   * @returns The modified transport document with updated order position check results.
   */
  private mergeOrderPositionCheckResultsIntoTransportDocument(
    transportDocument: SaveGoodsDataDto,
    checkedOrderPositions: OrderPositionVisualInspectionCarrierDto[]
  ): SaveGoodsDataDto {
    const modifiedTransportDocument: SaveGoodsDataDto = structuredClone(transportDocument);

    checkedOrderPositions.forEach(
      (orderPositionVisualInspectionCarrierDto: OrderPositionVisualInspectionCarrierDto): void => {
        const orderIndex: number = this.getOrderIndex(transportDocument, orderPositionVisualInspectionCarrierDto);

        if (orderIndex !== -1) {
          const orderPositionIndex: number = this.getOrderPositionIndex(
            transportDocument,
            orderIndex,
            orderPositionVisualInspectionCarrierDto
          );

          modifiedTransportDocument.freight.orders[orderIndex].orderPositions[orderPositionIndex].logEntries.push(
            new LogEntry(
              OrderPositionStatus.visual_inspection_carrier_accepted,
              new Date().valueOf(),
              undefined,
              undefined,
              new VisualInspectionCarrierCriteria(
                orderPositionVisualInspectionCarrierDto.acceptTransportability,
                orderPositionVisualInspectionCarrierDto.acceptLabeling,
                orderPositionVisualInspectionCarrierDto.comment
              )
            )
          );
        }
      }
    );
    return modifiedTransportDocument;
  }

  /**
   * Parses a single order and returns an array of OrderPositionVisualInspectionCarrierDto objects.
   *
   * @param singleOrder - The single order to parse.
   * @returns An array of OrderPositionVisualInspectionCarrierDto objects.
   */
  private parseSingleOrderPositionVisualInspectionCarrierDto(
    singleOrder: OrderWithId
  ): OrderPositionVisualInspectionCarrierDto[] {
    const orderPositionVisualInspectionCarrierDtos: OrderPositionVisualInspectionCarrierDto[] = [];

    singleOrder.orderPositions.forEach((singleOrderPosition: OrderPositionWithId): void => {
      const visualInspectionCarrierCriteria: VisualInspectionCarrierCriteria =
        this.getVisualInspectionCarrierCriteriaByOrderPosition(singleOrderPosition);
      const orderPositionVisualInspectionCarrierDto: OrderPositionVisualInspectionCarrierDto =
        new OrderPositionVisualInspectionCarrierDto(
          singleOrderPosition.id,
          visualInspectionCarrierCriteria.acceptTransportability,
          visualInspectionCarrierCriteria.acceptLabeling,
          visualInspectionCarrierCriteria.comment
        );
      orderPositionVisualInspectionCarrierDtos.push(orderPositionVisualInspectionCarrierDto);
    });
    return orderPositionVisualInspectionCarrierDtos;
  }

  // Order related functions
  /**
   * Parses the transport document and returns an array of OrderVisualInspectionCarrierDto objects.
   *
   * @param transportDocument - The transport document to parse.
   * @returns An array of OrderVisualInspectionCarrierDto objects.
   */
  private parseOrderVisualInspectionCarrierDto(transportDocument: SaveGoodsDataDto): OrderVisualInspectionCarrierDto[] {
    const orderVisualInspectionCarrierDtos: OrderVisualInspectionCarrierDto[] = [];

    transportDocument.freight.orders.forEach((singleOrder: OrderWithId) => {
      const orderPositionVisualInspectionCarrierDtos: OrderPositionVisualInspectionCarrierDto[] =
        this.parseSingleOrderPositionVisualInspectionCarrierDto(singleOrder);
      const orderVisualInspectionCarrierDto: OrderVisualInspectionCarrierDto = new OrderVisualInspectionCarrierDto(
        singleOrder.id,
        orderPositionVisualInspectionCarrierDtos
      );
      orderVisualInspectionCarrierDtos.push(orderVisualInspectionCarrierDto);
    });
    return orderVisualInspectionCarrierDtos;
  }

  /**
   * Retrieves the index of the order that contains the specified order position in the transport document.
   *
   * @param transportDocument - The transport document to search within.
   * @param orderPositionVisualInspectionCarrierDto - The order position DTO to find the corresponding order.
   * @returns The index of the order containing the order position, or -1 if not found.
   */
  private getOrderIndex(
    transportDocument: SaveGoodsDataDto,
    orderPositionVisualInspectionCarrierDto: OrderPositionVisualInspectionCarrierDto
  ): number {
    return transportDocument.freight.orders.findIndex((order: OrderWithId) =>
      order.orderPositions.some(
        (orderPosition: OrderPositionWithId): boolean =>
          orderPosition.id === orderPositionVisualInspectionCarrierDto.orderPositionId
      )
    );
  }

  // Order position related functions
  /**
   * Retrieves the visual inspection carrier criteria for the specified order position.
   *
   * @param orderPosition - The order position to retrieve the visual inspection carrier criteria for.
   * @returns The vehicle inspection carrier criteria for the order position.
   */
  private getVisualInspectionCarrierCriteriaByOrderPosition(
    orderPosition: OrderPositionWithId
  ): VisualInspectionCarrierCriteria {
    let visualInspectionCarrierCriteria: VisualInspectionCarrierCriteria = new VisualInspectionCarrierCriteria(
      false,
      false
    );

    if (orderPosition.logEntries.length > 0) {
      orderPosition.logEntries.forEach((element: LogEntry) => {
        if (
          element.status == OrderPositionStatus.visual_inspection_carrier_accepted ||
          element.status == OrderPositionStatus.visual_inspection_carrier_denied
        ) {
          visualInspectionCarrierCriteria = new VisualInspectionCarrierCriteria(
            (element.acceptanceCriteria as VisualInspectionCarrierCriteria).acceptTransportability,
            (element.acceptanceCriteria as VisualInspectionCarrierCriteria).acceptLabeling
          );
        }
      });
    }
    return visualInspectionCarrierCriteria;
  }

  /**
   * Retrieves the index of the order position within the specified order in the transport document.
   *
   * @param transportDocument - The transport document containing the order and order position.
   * @param orderIndex - The index of the order containing the order position.
   * @param orderPositionVisualInspectionCarrierDto - The order position DTO to find its index.
   * @returns The index of the order position within the order, or -1 if not found.
   */
  private getOrderPositionIndex(
    transportDocument: SaveGoodsDataDto,
    orderIndex: number,
    orderPositionVisualInspectionCarrierDto: OrderPositionVisualInspectionCarrierDto
  ): number {
    return transportDocument.freight.orders[orderIndex].orderPositions.findIndex(
      (orderPosition: OrderPositionWithId): boolean =>
        orderPosition.id === orderPositionVisualInspectionCarrierDto.orderPositionId
    );
  }

  /**
   * Retrieves a specific order position from the given transport document.
   *
   * @param transportDocument - The transport document containing the freight.
   * @param orderPositionId - The ID of the order position.
   * @returns The order position if it exists in the document, otherwise undefined.
   * @throws Error if the order position is not found.
   */
  public getOrderPositionById(transportDocument: SaveGoodsDataDto, orderPositionId: number): OrderPositionWithId {
    const orderPosition: OrderPositionWithId = this.getAllOrderPositions(transportDocument).find(
      (orderPositionWithId: OrderPositionWithId): boolean => orderPositionWithId.id === orderPositionId
    );
    if (orderPosition) {
      return orderPosition;
    }
    return undefined;
  }

  /**
   * Retrieves all order positions from the given transport document.
   *
   * @param transportDocument - The transport document containing the freight.
   * @returns An array of order positions.
   */
  public getAllOrderPositions(transportDocument: SaveGoodsDataDto): OrderPositionWithId[] {
    return transportDocument.freight.orders
      .map((order: OrderWithId) => order.orderPositions)
      .reduce((a: OrderPositionWithId[], b: OrderPositionWithId[]) => a.concat(b), []);
  }
}
