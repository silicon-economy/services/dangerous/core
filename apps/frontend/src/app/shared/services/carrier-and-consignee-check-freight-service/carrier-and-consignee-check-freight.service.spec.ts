/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import {
  CarrierAndConsigneeCheckFreightService
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight.service';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { MatDialogModule } from '@angular/material/dialog';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TranslateModule } from '@ngx-translate/core';
import {
  mockTransportDocumentVehicleAccepted
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  OrderPositionVisualInspectionCarrierDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/update-visual-inspection-carrier/order-position-visual-inspection-carrier.dto';
import {
  CarrierAndConsigneeCheckDistinctionService
} from './services/carrier-and-consignee-check-distinction-service/carrier-and-consignee-check-distinction.service';
import {
  CarrierAndConsigneeCheckDistinctionServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-distinction-service/carrier-and-consignee-check-distinction-service.module';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';
import {
  CarrierAndConsigneeCheckProcessorService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-processor-service/carrier-and-consignee-check-processor.service';

describe('CarrierCheckFreightService', () => {
  let service: CarrierAndConsigneeCheckFreightService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        MatSnackBarModule,
        TranslateModule.forRoot(),
        CarrierAndConsigneeCheckDistinctionServiceModule,
      ],
      providers: [
        CarrierAndConsigneeCheckFreightService,
        CarrierAndConsigneeCheckDistinctionService,
        CarrierAndConsigneeCheckBehaviorSubjectService,
        CarrierAndConsigneeCheckProcessorService,
        TransportDocumentHttpService,
        FeedbackDialogService,
        NotificationService,
      ],
    });
    service = TestBed.inject(CarrierAndConsigneeCheckFreightService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return 1 as total order position count', () => {
    const transportDocumentWithoutOrders: SaveGoodsDataDto = structuredClone(mockTransportDocumentVehicleAccepted);
    transportDocumentWithoutOrders.freight.orders = undefined;
    expect(service.getTotalOrderPositionsCount(mockTransportDocumentVehicleAccepted)).toEqual(1);
  });

  it('should retrieve the unchecked order positions', () => {
    let checkedOrderPositions: OrderPositionVisualInspectionCarrierDto[] = [
      new OrderPositionVisualInspectionCarrierDto(1, true, true, ''),
    ];
    expect(service.getUncheckedOrderPositions(mockTransportDocumentVehicleAccepted, checkedOrderPositions)).toEqual([]);

    checkedOrderPositions = [];
    expect(service.getUncheckedOrderPositions(mockTransportDocumentVehicleAccepted, checkedOrderPositions)).toEqual([
      mockTransportDocumentVehicleAccepted.freight.orders[0].orderPositions[0],
    ]);
  });

  it('should check if all the checked order positions have successful checks for labeling and transportability', () => {
    const checkedOrderPositions: OrderPositionVisualInspectionCarrierDto[] = [
      new OrderPositionVisualInspectionCarrierDto(1, true, true, ''),
    ];
    expect(service.areAllChecksSuccessful(checkedOrderPositions)).toEqual(true);
  });

  it('should complete visual inspection checks', () => {
    const mockTransportDocument: SaveGoodsDataDto = structuredClone(mockTransportDocumentVehicleAccepted);

    const dialogServiceSpy = jest.spyOn(FeedbackDialogService.prototype, 'showDialog');
    const mergeOrderPositionCheckResultsIntoTransportDocumentSpy = jest.spyOn(
      CarrierAndConsigneeCheckProcessorService.prototype as never,
      'completeVisualInspectionChecksForCarrier'
    );

    service.completeVisualInspectionChecks(mockTransportDocument);

    expect(mergeOrderPositionCheckResultsIntoTransportDocumentSpy).toHaveBeenCalledWith(mockTransportDocument);
    expect(dialogServiceSpy).toHaveBeenCalled();
  });
});
