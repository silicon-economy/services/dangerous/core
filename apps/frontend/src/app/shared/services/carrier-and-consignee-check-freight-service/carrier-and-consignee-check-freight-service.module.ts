/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  CarrierAndConsigneeCheckFreightService
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight.service';
import {
  CarrierAndConsigneeCheckDistinctionServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-distinction-service/carrier-and-consignee-check-distinction-service.module';
import {
  CarrierAndConsigneeCheckBehaviorSubjectServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject-service.module';
import {
  CarrierAndConsigneeCheckProcessorServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-processor-service/carrier-and-consignee-check-processor-service.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CarrierAndConsigneeCheckDistinctionServiceModule,
    CarrierAndConsigneeCheckBehaviorSubjectServiceModule,
    CarrierAndConsigneeCheckProcessorServiceModule,
  ],
  providers: [CarrierAndConsigneeCheckFreightService],
})
export class CarrierAndConsigneeCheckFreightServiceModule {}
