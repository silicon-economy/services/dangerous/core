/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { FeedbackDialogService } from './feedback-dialog.service';
import {
  BlockchainSaveFeedbackDialogComponent
} from '@base/src/app/dialogs/blockchain-save-feedback-dialog/blockchain-save-feedback-dialog.component';

describe('FeedbackDialogService', () => {
  let service: FeedbackDialogService;

  beforeEach(() => {
    const dialogRef: Partial<MatDialogRef<BlockchainSaveFeedbackDialogComponent>> = {};

    TestBed.configureTestingModule({
      providers: [
        FeedbackDialogService,
        {
          provide: MatDialogRef,
          useValue: dialogRef,
        },
      ],
      imports: [MatDialogModule],
    });
    service = TestBed.inject(FeedbackDialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should handle errors', () => {
    const emitSpy = jest.spyOn(service.dialogOpen$, 'next').mockImplementation();
    service.handleError();
    expect(emitSpy).toHaveBeenNthCalledWith(1, false);
  });

  it('should handle success', () => {
    const emitSpy = jest.spyOn(service.dialogOpen$, 'next').mockImplementation();
    service.handleSuccess();
    expect(emitSpy).toHaveBeenNthCalledWith(1, false);
  });

  it('should show the dialog', () => {
    const emitSpy = jest.spyOn(service.dialogOpen$, 'next').mockImplementation();
    service.showDialog();
    expect(emitSpy).toHaveBeenNthCalledWith(1, true);
  });
});
