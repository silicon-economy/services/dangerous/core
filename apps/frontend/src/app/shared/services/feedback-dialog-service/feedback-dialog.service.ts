/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import {
  BlockchainSaveFeedbackDialogComponent
} from '@base/src/app/dialogs/blockchain-save-feedback-dialog/blockchain-save-feedback-dialog.component';

/**
 * Display dialog while saving to blockchain is in progress since its duration is longer than normally expected
 */
@Injectable()
export class FeedbackDialogService {
  dialogOpen$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private dialog: MatDialog) {
    this.dialogOpen$.subscribe((showDialog: boolean) => {
      if (showDialog) {
        this.dialog.open(BlockchainSaveFeedbackDialogComponent, {
          disableClose: true,
        });
      } else {
        this.dialog.closeAll();
      }
    });
  }

  /**
   * Hide the dialog
   */
  public handleSuccess(): void {
    this.dialogOpen$.next(false);
  }

  /**
   * Hide the dialog and log the error
   *
   * @param $error - The thrown error
   */
  public handleError($error?: string): void {
    console.error($error);
    this.dialogOpen$.next(false);
  }

  /**
   * Show the Dialog
   */
  public showDialog(): void {
    this.dialogOpen$.next(true);
  }
}
