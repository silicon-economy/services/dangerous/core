/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [FeedbackDialogService],
})
export class FeedbackDialogServiceModule {}
