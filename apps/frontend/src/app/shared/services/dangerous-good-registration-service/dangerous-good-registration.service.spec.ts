/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { DangerousGoodRegistrationService } from './dangerous-good-registration.service';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  mockDangerousGoodRegistration
} from '@core/api-interfaces/lib/mocks/dangerous-good-registration/dangerous-good-registration.mock';
import { RouterTestingModule } from '@angular/router/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { of } from 'rxjs';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {
  DisableDocumentDialogComponent
} from '@base/src/app/dialogs/disable-document-dialog/disable-document-dialog.component';

describe('DangerousGoodRegistrationService', () => {
  let service: DangerousGoodRegistrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DisableDocumentDialogComponent],
      providers: [
        DangerousGoodRegistrationHttpService,
        DangerousGoodRegistrationService,
        FeedbackDialogService,
        NotificationService,
      ],
      imports: [
        TranslateModule.forRoot(),
        MatSnackBarModule,
        HttpClientTestingModule,
        MatDialogModule,
        RouterTestingModule.withRoutes([
          {
            path:
              'dangerous-good-registrations/' + mockDangerousGoodRegistration.id + '/transfer-to-transport-document',
            redirectTo: '',
          },
        ]),
      ],
    });
    service = TestBed.inject(DangerousGoodRegistrationService);
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should test onTransferToTransportDocumentButtonClicked', async () => {
    const routerSpy = jest.spyOn(service['router'], 'navigate');
    await service.onTransferToTransportDocumentButtonClicked(mockDangerousGoodRegistration);
    expect(routerSpy).toHaveBeenCalled();
  });

  it('should disable a dangerous good registration', () => {
    const dangerousGoodRegistrationIdMock = '123';

    jest
      .spyOn(service.matDialog, 'open')
      .mockReturnValue({ afterClosed: () => of(true) } as MatDialogRef<typeof DisableDocumentDialogComponent>);
    const feedbackDialogServiceHandleSuccessDialogSpy = jest.spyOn(FeedbackDialogService.prototype, 'handleSuccess');
    const dangerousGoodRegistrationHttpServiceSpy = jest
      .spyOn(DangerousGoodRegistrationHttpService.prototype, 'disableDangerousGoodRegistration')
      .mockReturnValue(of(undefined));
    const feedbackDialogServiceHandleSuccessSpy = jest.spyOn(FeedbackDialogService.prototype, 'handleSuccess');
    const translateServiceSpy = jest.spyOn(TranslateService.prototype, 'get');
    const notificationServiceSpy = jest.spyOn(NotificationService.prototype, 'success');

    service.onDisableDangerousGoodRegistrationButtonClicked(dangerousGoodRegistrationIdMock);

    expect(feedbackDialogServiceHandleSuccessDialogSpy).toHaveBeenCalled();
    expect(dangerousGoodRegistrationHttpServiceSpy).toHaveBeenCalledWith(dangerousGoodRegistrationIdMock);
    expect(feedbackDialogServiceHandleSuccessSpy).toHaveBeenCalled();
    expect(translateServiceSpy).toHaveBeenCalledWith(
      'dialogs.disableDocument.dangerousGoodRegistration.notification.success'
    );
    expect(notificationServiceSpy).toHaveBeenCalled();
  });
});
