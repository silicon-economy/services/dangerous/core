/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-dangerous-good-registration.dto';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {
  DisableDocumentDialogComponent
} from '@base/src/app/dialogs/disable-document-dialog/disable-document-dialog.component';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '@shared/services/notification-service/notification.service';

@Injectable()
export class DangerousGoodRegistrationService {
  constructor(
    private readonly dangerousGoodRegistrationHttpService: DangerousGoodRegistrationHttpService,
    private readonly router: Router,
    private readonly feedbackDialogService: FeedbackDialogService,
    private readonly translateService: TranslateService,
    private readonly notificationService: NotificationService,
    public matDialog: MatDialog
  ) {}

  /**
   * Transfers a dangerous good registration into a transport document.
   *
   * @param dangerousGoodRegistration - the dangerous good registration to be transferred into a transport document
   */
  public async onTransferToTransportDocumentButtonClicked(
    dangerousGoodRegistration: SaveDangerousGoodRegistrationDto
  ): Promise<void> {
    await this.router
      .navigate(['/dangerous-good-registrations', dangerousGoodRegistration.id, 'transfer-to-transport-document'])
      .catch((error) => {
        console.error('Error navigating:', error);
        throw error;
      });
  }

  /**
   * Opens the disable-transport-document-dialog.
   *
   * @param dangerousGoodRegistrationId - The ID of the dangerous good registration to be disabled.
   */
  public onDisableDangerousGoodRegistrationButtonClicked(dangerousGoodRegistrationId: string): void {
    const dialogRef: MatDialogRef<DisableDocumentDialogComponent> = this.matDialog.open(
      DisableDocumentDialogComponent,
      {
        data: dangerousGoodRegistrationId,
      }
    );
    dialogRef.afterClosed().subscribe((markedForDeletion: boolean): void => {
      if (markedForDeletion === true) {
        this.feedbackDialogService.showDialog();
        this.dangerousGoodRegistrationHttpService
          .disableDangerousGoodRegistration(dangerousGoodRegistrationId)
          .subscribe((): void => {
            this.feedbackDialogService.handleSuccess();
            this.translateService
              .get('dialogs.disableDocument.dangerousGoodRegistration.notification.success')
              .subscribe((translation: string): void => {
                this.notificationService.success(translation);
                this.router.navigate(['/dangerous-good-registrations']).then(() => window.location.reload()).catch((error) => {
                  console.error('Error navigating:', error);
                  throw error;
                });
              });
          });
      }
    });
  }
}
