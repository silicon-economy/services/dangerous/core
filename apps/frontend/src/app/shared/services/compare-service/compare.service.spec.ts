/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { CompareService } from './compare.service';
import { Company } from '@core/api-interfaces/lib/dtos/frontend';

describe('CompareService', () => {
  let service: CompareService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [CompareService] });
    service = TestBed.inject(CompareService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should sort numbers and strings', () => {
    expect(service.compare(1, 1, true)).toBe(1);
    expect(service.compare(1, 1, false)).toBe(-1);
    expect(service.compare(1, 2, true)).toBe(-1);
    expect(service.compare(1, 2, false)).toBe(1);
    expect(service.compare(2, 1, false)).toBe(-1);
    expect(service.compare(2, 1, true)).toBe(1);
    expect(service.compare('a', 'b', true)).toBe(-1);
    expect(service.compare('a', 'b', false)).toBe(1);
    expect(service.compare('b', 'a', false)).toBe(-1);
    expect(service.compare('b', 'a', true)).toBe(1);
  });

  it('should sort dates', () => {
    expect(service.compareDate(new Date(2000, 0, 1), new Date(2020, 0, 1), true)).toBeLessThan(0);
    expect(service.compareDate(undefined, new Date(2020, 0, 1), true)).toBeLessThan(0);
    expect(service.compareDate(new Date(2000, 0, 1), new Date(2020, 0, 1), false)).toBeGreaterThan(0);
    expect(service.compareDate(undefined, new Date(2020, 0, 1), false)).toBeGreaterThan(0);
    expect(service.compareDate(new Date(2020, 0, 1), new Date(2000, 0, 1), true)).toBeGreaterThan(0);
    expect(service.compareDate(new Date(2020, 0, 1), undefined, true)).toBeGreaterThan(0);
    expect(service.compareDate(new Date(2020, 0, 1), new Date(2000, 0, 1), false)).toBeLessThan(0);
    expect(service.compareDate(new Date(2020, 0, 1), undefined, false)).toBeLessThan(0);
  });

  it('should sort companies by name', () => {
    const a = new Company('foo', undefined, undefined);
    const b = new Company('bar', undefined, undefined);
    expect(service.compareCompany(a, b, true)).toBe(1);
    expect(service.compareCompany(a, b, false)).toBe(-1);
  });
});
