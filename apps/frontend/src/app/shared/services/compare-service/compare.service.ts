/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Carrier, Company } from '@core/api-interfaces/lib/dtos/frontend';

@Injectable()
export class CompareService {
  /**
   * Compare two numbers or strings
   *
   * @param a - A number or string
   * @param b - Another number or string
   * @param isAsc - If sorting is ascending
   * @returns The sorting information
   */
  compare(a: number | string, b: number | string, isAsc: boolean): number {
    return (a.toString().toLocaleLowerCase() < b.toString().toLocaleLowerCase() ? -1 : 1) * (isAsc ? 1 : -1);
  }

  /**
   * Compare two Dates
   *
   * @param a - A date
   * @param b - Another date
   * @param isAsc - If sorting is ascending
   * @returns - The sorting information
   */
  compareDate(a: Date | undefined, b: Date | undefined, isAsc: boolean): number {
    if (a === undefined) {
      a = new Date(1970, 0, 1);
    }
    if (b === undefined) {
      b = new Date(1970, 0, 1);
    }
    return (+a - +b) * (isAsc ? 1 : -1);
  }

  /**
   * Compare two Companies by comparing the names
   *
   * @param a - A company
   * @param b - Another company
   * @param isAsc - If sorting is ascending
   * @returns The sorting information
   */
  compareCompany(a: Company, b: Company, isAsc: boolean): number {
    return this.compare(a.name ?? '', b.name ?? '', isAsc);
  }

  /**
   * Compare two Carriers by comparing the names
   *
   * @param a - A company
   * @param b - Another company
   * @param isAsc - If sorting is ascending
   * @returns The sorting information
   */
  compareCarrier(a: Carrier, b: Carrier, isAsc: boolean): number {
    return this.compare(a.name ?? '', b.name ?? '', isAsc);
  }
}
