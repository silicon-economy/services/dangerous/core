/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompareService } from '@shared/services/compare-service/compare.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [CompareService],
})
export class CompareModule {}
