/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

/**
 * NotificationService helps to send notifications to provide feedback after an action has completed
 */
@Injectable()
export class NotificationService {
  constructor(private readonly snackBar: MatSnackBar) {}

  /**
   * Open a Material snack bar presenting the provided message with a green background
   *
   * @param message - Message to display
   * @param duration - Time of visibility (in milliseconds)
   */
  public success(message: string, duration = 5000): void {
    this.openSnackBar(message, 'x', duration, ['notification', 'success']);
  }

  /**
   * Open a Material snack bar presenting the provided message with a yellow background
   *
   * @param message - Message to display
   * @param duration - Optional time of visibility (in milliseconds)
   */
  public warn(message: string, duration = 5000): void {
    this.openSnackBar(message, 'x', duration, ['notification', 'warn']);
  }

  /**
   * Open a Material snack bar presenting the provided message with a red background
   *
   * @param message - Message to display
   * @param duration - Optional time of visibility (in milliseconds)
   */
  public error(message: string, duration = 5000): void {
    this.openSnackBar(message, 'x', duration, ['notification', 'error']);
  }

  /**
   * Open a Material snack bar presenting the provided message
   *
   * @param message - Message to display
   * @param action - Action text
   * @param duration - Optional time of visibility (in milliseconds)
   * @param panelClass - Panel class
   * @param horizontalPosition - Horizontal position
   * @param verticalPosition - Vertical position
   */
  private openSnackBar(
    message: string,
    action: string,
    duration = 2000,
    panelClass: string[] = [],
    horizontalPosition: MatSnackBarHorizontalPosition = 'center',
    verticalPosition: MatSnackBarVerticalPosition = 'top'
  ): void {
    this.snackBar.open(message, action, {
      duration,
      panelClass,
      horizontalPosition,
      verticalPosition,
    });
  }
}
