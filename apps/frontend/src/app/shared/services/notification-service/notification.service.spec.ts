/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { NotificationService } from './notification.service';

describe('NotificationService', () => {
  let service: NotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule],
      providers: [NotificationService],
    });
    service = TestBed.inject(NotificationService);
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should show success notification', () => {
    const notificationSpy = jest.spyOn(NotificationService.prototype as never, 'openSnackBar').mockImplementation();
    service.success('foobar');
    expect(notificationSpy).toHaveBeenCalledWith('foobar', 'x', 5000, ['notification', 'success']);
  });

  it('should show warn notification', () => {
    const notificationSpy = jest.spyOn(NotificationService.prototype as never, 'openSnackBar').mockImplementation();
    service.warn('foobar');
    expect(notificationSpy).toHaveBeenCalledWith('foobar', 'x', 5000, ['notification', 'warn']);
  });

  it('should show error notification', () => {
    const notificationSpy = jest.spyOn(NotificationService.prototype as never, 'openSnackBar').mockImplementation();
    service.error('foobar');
    expect(notificationSpy).toHaveBeenCalledWith('foobar', 'x', 5000, ['notification', 'error']);
  });

  it('should open snackBar', () => {
    const matSnackBarSpy = jest.spyOn(MatSnackBar.prototype, 'open').mockImplementation();
    service.success('foobar');
    expect(matSnackBarSpy).toHaveBeenCalled();
  });
});
