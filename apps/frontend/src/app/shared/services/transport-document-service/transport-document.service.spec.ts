/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { TransportDocumentService } from './transport-document.service';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import {
  mockTransportDocumentAccepted,
  mockTransportDocumentCreated,
  mockTransportDocumentReleased,
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import { of } from 'rxjs';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ScanDialogServiceModule } from '@shared/services/scan-dialog-service/scan-dialog-service.module';
import {
  CheckCarrierIdentityDialogComponent
} from '@base/src/app/dialogs/check-carrier-identity-dialog/check-carrier-identity-dialog.component';
import {
  DisableDocumentDialogComponent
} from "@base/src/app/dialogs/disable-document-dialog/disable-document-dialog.component";

describe('TransportDocumentService', () => {
  let service: TransportDocumentService;
  let transportDocumentHttpServiceMock: Partial<TransportDocumentHttpService>;
  let matDialog: MatDialog;
  let transportDocumentHttpService: TransportDocumentHttpService;
  let notificationService: NotificationService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CheckCarrierIdentityDialogComponent, DisableDocumentDialogComponent],
      imports: [
        HttpClientTestingModule,
        FeedbackDialogServiceModule,
        NotificationServiceModule,
        MatDialogModule,
        ScanDialogServiceModule,
        TranslateModule.forRoot(),
        RouterTestingModule.withRoutes([
          {
            path: 'transport-documents/vehicleInspection/' + mockTransportDocumentAccepted.id,
            redirectTo: '',
          },
          {
            path: 'transport-documents/check/' + mockTransportDocumentAccepted.id,
            redirectTo: '',
          },
          {
            path: 'transport-documents/' + mockTransportDocumentAccepted.id,
            redirectTo: '',
          },
        ]),
      ],
      providers: [TransportDocumentHttpService, TransportDocumentService],
    });

    service = TestBed.inject(TransportDocumentService);
    matDialog = TestBed.inject(MatDialog);
    transportDocumentHttpService = TestBed.inject(TransportDocumentHttpService);
    notificationService = TestBed.inject(NotificationService);
    router = TestBed.inject(Router);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // onReleaseTransportDocumentButtonClicked
  it(
    'should release the transport document when onCheckTransportVehicleButtonClicked is called with a ' +
      'valid transport document ID',
    () => {
      const releaseTransportDocumentSpy = jest
        .spyOn(TransportDocumentHttpService.prototype, 'releaseTransportDocument')
        .mockReturnValue(of(mockTransportDocumentReleased));
      const handleSuccessSpy = jest.spyOn(FeedbackDialogService.prototype, 'handleSuccess');
      const successSpy = jest.spyOn(NotificationService.prototype, 'success');

      delete window.location;
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      window.location = { reload: jest.fn() as unknown } as Location;

      service.onReleaseTransportDocumentButtonClicked(mockTransportDocumentAccepted.id);

      expect(releaseTransportDocumentSpy).toHaveBeenCalledWith(mockTransportDocumentAccepted.id);
      expect(handleSuccessSpy).toHaveBeenCalledTimes(1);
      expect(successSpy).toHaveBeenCalledTimes(1);
    }
  );

  // onCheckTransportVehicleButtonClicked
  it(
    'should navigate to the transport document vehicle inspection page when ' +
      'onCheckTransportVehicleButtonClicked is called with a valid transport document ID',
    async () => {
      const navigateSpy = jest.spyOn(Router.prototype, 'navigate');
      await service.onCheckTransportVehicleButtonClicked(mockTransportDocumentAccepted.id);
      expect(navigateSpy).toHaveBeenCalledWith([
        '/transport-documents/vehicleInspection/',
        mockTransportDocumentAccepted.id,
      ]);
    }
  );

  it(
    'should throw an error and log it to the console when onCheckTransportVehicleButtonClicked encounters ' +
      'a navigation error',
    async () => {
      jest.spyOn(Router.prototype, 'navigate').mockRejectedValueOnce(new Error('Navigation error'));
      const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();

      await expect(service.onCheckTransportVehicleButtonClicked('foo')).rejects.toThrow('Navigation error');
      expect(consoleErrorSpy).toHaveBeenCalledWith('Error navigating:', new Error('Navigation error'));
    }
  );

  // onCheckTransportDocumentButtonClicked
  it(
    'should navigate to the transport document check page when onCheckTransportDocumentButtonClicked is ' +
      'called with a valid transport document ID',
    async () => {
      const navigateSpy = jest.spyOn(Router.prototype, 'navigate');
      await service.onCheckTransportDocumentButtonClicked(mockTransportDocumentAccepted.id);
      expect(navigateSpy).toHaveBeenCalledWith(['/transport-documents/check/', mockTransportDocumentAccepted.id]);
    }
  );

  it(
    'should throw an error and log it to the console when onCheckTransportDocumentButtonClicked encounters ' +
      'a navigation error',
    async () => {
      jest.spyOn(Router.prototype, 'navigate').mockRejectedValueOnce(new Error('Navigation error'));
      const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();

      await expect(service.onCheckTransportDocumentButtonClicked('foo')).rejects.toThrow('Navigation error');
      expect(consoleErrorSpy).toHaveBeenCalledWith('Error navigating:', new Error('Navigation error'));
    }
  );

  // onViewTransportDocumentButtonClicked
  it(
    'should navigate to the transport document details page when onViewTransportDocumentButtonClicked is ' +
      'called with a valid transport document ID',
    async () => {
      const navigateSpy = jest.spyOn(Router.prototype, 'navigate');
      await service.onViewTransportDocumentButtonClicked(mockTransportDocumentAccepted.id);
      expect(navigateSpy).toHaveBeenCalledWith(['/transport-documents/', mockTransportDocumentAccepted.id]);
    }
  );

  it(
    'should throw an error and log it to the console when onViewTransportDocumentButtonClicked encounters ' +
      'a navigation error',
    async () => {
      jest.spyOn(Router.prototype, 'navigate').mockRejectedValueOnce(new Error('Navigation error'));
      const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();

      await expect(service.onViewTransportDocumentButtonClicked('foo')).rejects.toThrow('Navigation error');
      expect(consoleErrorSpy).toHaveBeenCalledWith('Error navigating:', new Error('Navigation error'));
    }
  );

  it('should confirm carrier identity with correct transport document ID', (): void => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const dialogRefMock = {
      afterClosed: jest.fn().mockReturnValue(of(true)),
    } as never;

    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/no-explicit-any
    (service as any).dialog = {
      open: jest.fn().mockReturnValue(dialogRefMock),
    };

    service.onCheckCarrierIdentityButtonClicked(mockTransportDocumentCreated);
    setTimeout((): void => {
      expect(transportDocumentHttpServiceMock.confirmCarrierIdentity).toHaveBeenCalledWith('BP000001-20210819');
    }, 0);
  });

  it('should disable transport document when user confirms', () => {
    const transportDocumentId = '1234';
    const dialogRefMock = {
      afterClosed: () => of(true)
    } as MatDialogRef<MatDialog>;

    jest.spyOn(matDialog, 'open').mockReturnValue(dialogRefMock);
    jest.spyOn(transportDocumentHttpService, 'disableTransportDocument').mockReturnValue(of(null));
    jest.spyOn(notificationService, 'success').mockImplementation();
    jest.spyOn(router, 'navigate').mockResolvedValue(true);

    service.onDisableTransportDocumentButtonClicked(transportDocumentId);

    expect(matDialog.open).toHaveBeenCalled();
    expect(transportDocumentHttpService.disableTransportDocument).toHaveBeenCalledWith(transportDocumentId);
    expect(notificationService.success).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/transport-documents']);
  });
});
