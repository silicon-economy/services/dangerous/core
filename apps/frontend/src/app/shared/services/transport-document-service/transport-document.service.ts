/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable, NgZone } from '@angular/core';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { Router } from '@angular/router';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { Company } from '@core/api-interfaces/lib/dtos/frontend';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import {
  CheckCarrierIdentityDialogComponent
} from '@base/src/app/dialogs/check-carrier-identity-dialog/check-carrier-identity-dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {
  CarrierCheckFreightDialogComponent
} from '@base/src/app/dialogs/carrier-check-freight-dialog/carrier-check-freight-dialog.component';
import { ScanDialogService } from '@shared/services/scan-dialog-service/scan-dialog.service';
import {
  DocumentQrCodeDialogComponent
} from '@base/src/app/dialogs/document-qr-code-dialog/document-qr-code-dialog.component';
import {
  HandoverQrCodeScannerDialogComponent
} from '@base/src/app/dialogs/handover-qr-code-scanner-dialog/handover-qr-code-scanner-dialog.component';
import {
  ConsigneeCheckOrderDialogComponent
} from '@base/src/app/dialogs/consignee-check-order-dialog/consignee-check-order-dialog.component';
import {
  AcceptDangerousGoodRegistrationQrCodeScannerDialogComponent
} from '@base/src/app/dialogs/accept-dangerous-good-registration-qr-code-scanner-dialog/accept-dangerous-good-registration-qr-code-scanner-dialog.component';
import {
  DisableDocumentDialogComponent
} from '@base/src/app/dialogs/disable-document-dialog/disable-document-dialog.component';

@Injectable()
export class TransportDocumentService {
  constructor(
    private readonly transportDocumentHttpService: TransportDocumentHttpService,
    private readonly scanDialogService: ScanDialogService,
    private readonly router: Router,
    private readonly feedbackDialogService: FeedbackDialogService,
    private readonly translateService: TranslateService,
    private readonly notificationService: NotificationService,
    private readonly ngZone: NgZone,
    private readonly matDialog: MatDialog
  ) {}

  /**
   * Retrieves the shipping labels for a transport document or dangerous good registration as a PDF and opens it in a new window.
   *
   * @param id - The id of the document for which the shipping labels are retrieved
   */
  public onDownloadShippingLabelsButtonClicked(id: string): void {
    this.transportDocumentHttpService.getPackagingLabels(id).subscribe((packagingLabelsPdf: Blob) => {
      const pdfBlob: Blob = new Blob([packagingLabelsPdf], { type: 'application/pdf' });
      const pdfBlobUrl: string = URL.createObjectURL(pdfBlob);
      window.open(pdfBlobUrl);
    });
  }

  /**
   * Navigates to the '/transport-documents/\{transportDocumentId\}' route with the specified transport document ID.
   *
   * @param transportDocumentId - The ID of the transport document.
   * @returns A promise that resolves when the navigation is successful or rejects if an error occurs.
   */
  public async onViewTransportDocumentButtonClicked(transportDocumentId: string): Promise<void> {
    await this.router
      .navigate(['/transport-documents/', transportDocumentId])
      .catch((error) => {
        console.error('Error navigating:', error);
        throw error;
      })
      .then();
  }

  /**
   * Navigates to the '/transport-documents/check/\{transportDocumentId\}' route with the specified transport document ID.
   *
   * @param transportDocumentId - The ID of the transport document.
   * @returns A promise that resolves when the navigation is successful or rejects if an error occurs.
   */
  public async onCheckTransportDocumentButtonClicked(transportDocumentId: string): Promise<void> {
    await this.router
      .navigate(['/transport-documents/check/', transportDocumentId])
      .catch((error) => {
        console.error('Error navigating:', error);
        throw error;
      })
      .then();
  }

  /**
   * Navigates to the '/transport-documents/vehicleInspection/\{transportDocumentId\}'
   * route with the specified transport document ID.
   *
   * @param transportDocumentId - The ID of the transport document.
   * @returns A promise that resolves when the navigation is successful or rejects if an error occurs.
   */
  public async onCheckTransportVehicleButtonClicked(transportDocumentId: string): Promise<void> {
    await this.router
      .navigate(['/transport-documents/vehicleInspection/', transportDocumentId])
      .catch((error) => {
        console.error('Error navigating:', error);
        throw error;
      })
      .then();
  }

  /**
   * Handles the click event of the "Start Visual Inspection" button initiated by the carrier.
   *
   * @param transportDocument - The transport document containing the dangerous goods data to be saved.
   */
  public onStartVisualInspectionButtonClickedByCarrier(transportDocument: SaveGoodsDataDto): void {
    const componentAttributes: [{ attributeName: string; attribute }] = [
      { attributeName: 'document', attribute: transportDocument },
    ];
    this.scanDialogService.createDialog(CarrierCheckFreightDialogComponent, componentAttributes);
  }

  /**
   * Handles the click event of the "Generate Handover QR Code" button.
   *
   * @param transportDocumentId - The id of the transport document.
   */
  public onGenerateHandoverQRCodeButtonClicked(transportDocumentId: string): void {
    this.matDialog.open(DocumentQrCodeDialogComponent, {
      data: { transportDocumentId: transportDocumentId },
    });
  }

  /**
   * Handles the click event of the "Scan Handover QR Code" button.
   */
  public onScanHandoverQRCodeButtonClicked(): void {
    this.translateService.get('transportDocument.actions.scanQrCode').subscribe((translation: string) => {
      this.scanDialogService.createDialog(HandoverQrCodeScannerDialogComponent, [
        { attributeName: 'titleHeader', attribute: translation },
      ]);
    });
  }

  /**
   * Opens a dialog to check the identity of the carrier.
   * If the identity is accepted, it confirms the carrier's identity via an HTTP call and displays a success notification.
   *
   * @param document - The transport document.
   */
  public onCheckCarrierIdentityButtonClicked(document: SaveGoodsDataDto): void {
    const dialogRef: MatDialogRef<CheckCarrierIdentityDialogComponent> = this.matDialog.open(
      CheckCarrierIdentityDialogComponent,
      {
        data: document.carrier,
      }
    );
    dialogRef.afterClosed().subscribe((acceptIdentity: boolean): void => {
      if (acceptIdentity === true) {
        this.feedbackDialogService.showDialog();
        this.transportDocumentHttpService.confirmCarrierIdentity(document.id).subscribe((): void => {
          this.feedbackDialogService.handleSuccess();
          this.translateService
            .get('transportDocumentDetailsPage.notification.carrierConfirmationSuccess')
            .subscribe((translation: string): void => {
              this.notificationService.success(translation);
              window.location.reload();
            });
        });
      }
    });
  }

  /**
   * Handles the click event of the "Start Visual Inspection" button initiated by the consignee.
   *
   * @param transportDocument - The transport document containing the goods data to be saved.
   */
  public onStartVisualInspectionButtonClickedByConsignee(transportDocument: SaveGoodsDataDto): void {
    const componentAttributes: [{ attributeName: string; attribute }] = [
      { attributeName: 'document', attribute: transportDocument },
    ];
    this.scanDialogService.createDialog(ConsigneeCheckOrderDialogComponent, componentAttributes);
  }

  /**
   * Called when the release transport document button is clicked.
   *
   * @param transportDocumentId - The ID of the transport document to be released.
   */
  public onReleaseTransportDocumentButtonClicked(transportDocumentId: string): void {
    this.transportDocumentHttpService.releaseTransportDocument(transportDocumentId).subscribe(() => {
      this.feedbackDialogService.handleSuccess();
      this.translateService
        .get('transportDocumentList.contextMenu.releaseTransportDocument.notification.success')
        .subscribe((translation: string) => {
          this.ngZone.run(() => {
            window.location.reload();
            this.notificationService.success(translation);
          });
        });
    });
  }

  /**
   * Opens the disable-transport-document-dialog.
   *
   * @param transportDocumentId - The ID of the transport document to be disabled.
   */
  public onDisableTransportDocumentButtonClicked(transportDocumentId: string): void {
    const dialogRef: MatDialogRef<DisableDocumentDialogComponent> = this.matDialog.open(
      DisableDocumentDialogComponent,
      {
        data: transportDocumentId,
      }
    );
    dialogRef.afterClosed().subscribe((markedForDeletion: boolean): void => {
      if (markedForDeletion === true) {
        this.feedbackDialogService.showDialog();
        this.transportDocumentHttpService.disableTransportDocument(transportDocumentId).subscribe((): void => {
          console.log('document was disabled')
          this.feedbackDialogService.handleSuccess();
          this.translateService
            .get('dialogs.disableDocument.transportDocument.notification.success')
            .subscribe((translation: string): void => {
              this.notificationService.success(translation);
              this.router.navigate(['/transport-documents']).then(() => window.location.reload()).catch((error) => {
                console.error('Error navigating:', error);
                throw error;
              });
            });
        });
      }
    });
  }

  /**
   * Check if a document contains a log entry with a comment.
   *
   * @param document - The document which will be checked.
   * @returns If the document has at least one log entry containing a comment.
   */
  public hasComment(document: SaveGoodsDataDto): boolean {
    return document.logEntries.filter((l) => !!l.acceptanceCriteria?.comment).length > 0;
  }

  /**
   * Gets the consignees of all orders. (In case of a dangerous good registration,
   * there is always only a single order and consignee present.)
   *
   * @param saveGoodsDataDto - The dangerous good document
   * @returns Array of companies
   */
  getConsignees(saveGoodsDataDto: SaveGoodsDataDto): Company[] {
    return saveGoodsDataDto.freight.orders.map((order: OrderWithId) => order.consignee);
  }

  /**
   * Returns the count of devices connected to order positions of the document.
   *
   * @param transportDocument - The document.
   * @returns The number of order positions with a device connected to them.
   */
  public getDeviceCount(transportDocument: SaveGoodsDataDto): number {
    const orderPositions: OrderPositionWithId[] = transportDocument.freight.orders.flatMap(
      (order: OrderWithId) => order.orderPositions
    );
    const orderPositionsWithDevice: OrderPositionWithId[] = orderPositions.filter(
      (orderPosition: OrderPositionWithId) => {
        return parseInt(orderPosition.deviceId) !== 0;
      }
    );
    return orderPositionsWithDevice.length;
  }

  /**
   * Returns whether an order position of the given document was ever linked to a dragon puck.
   *
   * @param transportDocument - A Transport Document.
   * @returns Boolean value
   */
  public wasLinkedBefore(transportDocument: SaveGoodsDataDto): boolean {
    return transportDocument.logEntries.find((logEntry) => logEntry.description.includes('Puck')) !== undefined;
  }

  /**
   * Opens the accept-dangerous-good-registration-qr-code-scanner-dialog.
   *
   * @param transportDocumentId - The ID of the transport document to be disabled.
   */
  public onImportDangerousGoodRegistrationButtonClicked(transportDocumentId?: string): void {
    this.matDialog.open(AcceptDangerousGoodRegistrationQrCodeScannerDialogComponent, {
      data: { transportDocumentId: transportDocumentId },
    });
  }
}
