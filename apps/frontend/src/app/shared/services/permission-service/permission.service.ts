/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { map } from 'rxjs/operators';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { Observable } from 'rxjs';
import {
  OrderStatus,
  TransportDocumentStatus,
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/status.enum';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';

@Injectable({
  providedIn: 'root',
})
export class PermissionService {
  constructor(private readonly userService: UserLoginService) {}

  /**
   * @param transportDocument - the corresponding transportDocument
   * @returns true if transport-document-status equals "created" and the logged-in user has the role of a "carrier"
   */
  userCanCheckTransportDocument(transportDocument: SaveGoodsDataDto): Observable<boolean> {
    return this.userHasRole(UserRole.carrier).pipe(
      map(
        (userHasRole) =>
          userHasRole && this.transportDocumentHasStatus(transportDocument, TransportDocumentStatus.created)
      )
    );
  }

  /**
   * @param transportDocument - the corresponding transportDocument
   * @returns true if transport-document-status equals "accepted" and the logged-in user has the role of a "consignor"
   */
  userCanReleaseTransportDocument(transportDocument: SaveGoodsDataDto): Observable<boolean> {
    return this.userHasRole(UserRole.consignor).pipe(
      map(
        (userHasRole) =>
          userHasRole && this.transportDocumentHasStatus(transportDocument, TransportDocumentStatus.accepted)
      )
    );
  }

  /**
   * @param transportDocument - the corresponding transportDocument
   * @returns true if transport-document-status equals "released" and the logged-in user has the role of a "consignor"
   */
  userCanCheckTransportVehicle(transportDocument: SaveGoodsDataDto): Observable<boolean> {
    return this.userHasRole(UserRole.consignor).pipe(
      map(
        (userHasRole) =>
          userHasRole && this.transportDocumentHasStatus(transportDocument, TransportDocumentStatus.released)
      )
    );
  }

  /**
   * @param transportDocument - the corresponding transportDocument
   * @returns true if transport-document-status equals "vehicle_accepted" and the logged-in user has the role of a "carrier"
   */
  userCanVisuallyInspectFreight(transportDocument: SaveGoodsDataDto): Observable<boolean> {
    return this.userHasRole(UserRole.carrier).pipe(
      map(
        (userHasRole) =>
          userHasRole && this.transportDocumentHasStatus(transportDocument, TransportDocumentStatus.vehicle_accepted)
      )
    );
  }

  /**
   * @param transportDocument - the corresponding transportDocument
   * @returns true if transport-document-status equals "transport" and the logged-in user has the role of a "carrier"
   */
  userCanShowTransportDocumentHandoverQRCode(transportDocument: SaveGoodsDataDto): Observable<boolean> {
    return this.userHasRole(UserRole.carrier).pipe(
      map(
        (userHasRole) =>
          userHasRole && this.transportDocumentHasStatus(transportDocument, TransportDocumentStatus.transport)
      )
    );
  }

  /**
   * @param transportDocument - the corresponding transportDocument
   * @returns true if transport-document-status equals "transport", the logged-in user has the role of a "consignee" and the status of the order is not set yet.
   */
  userCanCheckCarrierIdentity(transportDocument: SaveGoodsDataDto): Observable<boolean> {
    return this.userHasRole(UserRole.consignee).pipe(
      map(
        (userHasRole) =>
          userHasRole &&
          this.transportDocumentHasStatus(transportDocument, TransportDocumentStatus.transport) &&
          //here orders[0] is used, because consignees can only access and see their own order
          this.orderHasStatus(transportDocument.freight.orders[0], '')
      )
    );
  }

  /**
   * @param transportDocument - the corresponding transportDocument
   * @returns true if transport-document-status equals "transport", the logged-in user has the role of a "consignee" and the status of the order is not set yet.
   */
  userCanVisuallyInspectOrder(transportDocument: SaveGoodsDataDto): Observable<boolean> {
    return this.userHasRole(UserRole.consignee).pipe(
      map(
        (userHasRole) =>
          userHasRole &&
          this.transportDocumentHasStatus(transportDocument, TransportDocumentStatus.transport) &&
          //here orders[0] is used, because consignees can only access and see their own order
          this.orderHasStatus(transportDocument.freight.orders[0], OrderStatus.carrier_confirmed)
      )
    );
  }

  /**
   * @param transportDocument - the corresponding transportDocument
   * @returns true if transport-document-status equals "created" or "not_accepted" and the logged-in user has the role of a "consignor"
   */
  userCanDisableTransportDocument(transportDocument: SaveGoodsDataDto): Observable<boolean> {
    return this.userHasRole(UserRole.consignor).pipe(
      map(
        (userHasRole) =>
          (userHasRole && this.transportDocumentHasStatus(transportDocument, TransportDocumentStatus.created)) ||
          (userHasRole && this.transportDocumentHasStatus(transportDocument, TransportDocumentStatus.not_accepted))
      )
    );
  }

  /**
   * Checks if the user has the necessary role to disable dangerous good registration.
   *
   * @returns A boolean indicating whether the user has the Consignor role.
   */
  userCanDisableDangerousGoodRegistration(): Observable<boolean> {
    return this.userHasRole(UserRole.consignor);
  }

  /**
   * Checks if the user has the necessary role to transfer a dangerous good registration to a transport document.
   *
   * @returns A boolean indicating whether the user has the Consignor role.
   */
  userCanTransferDangerousGoodRegistrationToTransportDocument(): Observable<boolean> {
    return this.userHasRole(UserRole.consignor);
  }

  /**
   * Determines if the user can view dangerous good registrations.
   *
   * @returns An observable that emits a boolean value indicating whether the user can view dangerous good registrations.
   */
  userCanViewDangerousGoodRegistrations(): Observable<boolean> {
    return this.userHasRole(UserRole.consignor);
  }

  /**
   * Determines if the user can create a transport document and dangerous good registration.
   *
   * @returns An observable that emits a boolean value indicating whether the user can create a transport document and dangerous good registration.
   */
  userCanCreateTransportDocumentAndDangerousGoodRegistration(): Observable<boolean> {
    return this.userHasRole(UserRole.consignor);
  }

  /**
   * Determines if the user can scan a handover QR code.
   *
   * @returns An observable that emits a boolean value indicating whether the user can scan a handover QR code.
   */
  userCanScanHandoverQRCode(): Observable<boolean> {
    return this.userHasRole(UserRole.consignee);
  }

  /**
   * Determines if the user can perform visual inspection as a carrier.
   *
   * @returns An observable that emits a boolean value indicating whether the user can perform visual inspection as a carrier.
   */
  userCanPerformVisualInspectionCarrier(): Observable<boolean> {
    return this.userHasRole(UserRole.carrier);
  }

  /**
   * Determines if the user can import an existing dangerous good registration into an existing transport document.
   *
   * @param transportDocument - the transport document into which the dangerous good registration will be imported in
   * @returns An observable that emits a boolean value indicating whether the user can perform the action.
   */
  userCanImportDangerousGoodRegistrationIntoExistingTransportDocument(transportDocument: SaveGoodsDataDto): Observable<boolean> {
    return this.userHasRole(UserRole.carrier).pipe(
      map(
        (userHasRole) =>
          (userHasRole && this.transportDocumentHasStatus(transportDocument, TransportDocumentStatus.transport))
      )
    );
  }

  /**
   * @param role - the checked userRole;
   * @returns true if the currently logged-in user has the given role
   */
  private userHasRole(role: UserRole): Observable<boolean> {
    return this.userService.userRoles.pipe(
      map((userRoles: UserRole[]) => {
        return userRoles.some((userRole) => userRole === role);
      })
    );
  }

  /**
   * @param transportDocument - the transportDocument to be Checked
   * @param status - the TransportDocumentStatus to be checked
   * @returns true if the transportDocument has the given status
   */
  private transportDocumentHasStatus(transportDocument: SaveGoodsDataDto, status: TransportDocumentStatus): boolean {
    return transportDocument.status === status;
  }

  /**
   * @param order - the order to be Checked
   * @param status - the OrderStatus to be checked
   * @returns true if the order has the given status
   */
  private orderHasStatus(order: OrderWithId, status: OrderStatus | string): boolean {
    return order.status === status;
  }
}
