/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { PermissionService } from './permission.service';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { Observable, of } from 'rxjs';
import {
  mockTransportDocumentCreated
} from "@core/api-interfaces/lib/mocks/transport-document/transport-document.mock";

describe('PermissionService', () => {
  let service: PermissionService;

  class mockUserLoginService {
    userRoles: Observable<UserRole[]> = of([UserRole.consignee]);
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: UserLoginService,
          useValue: new mockUserLoginService(),
        },
      ],
    });
    service = TestBed.inject(PermissionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should test userCanCheckTransportDocument', () => {
    let result: boolean;
    service.userCanCheckTransportDocument(mockTransportDocumentCreated).subscribe((response) => (result = response));
    expect(result).toBe(false);
  });

  it('should test userCanReleaseTransportDocument', () => {
    let result: boolean;
    service.userCanReleaseTransportDocument(mockTransportDocumentCreated).subscribe((response) => (result = response));
    expect(result).toBe(false);
  });

  it('should test userCanCheckTransportVehicle', () => {
    let result: boolean;
    service.userCanCheckTransportVehicle(mockTransportDocumentCreated).subscribe((response) => (result = response));
    expect(result).toBe(false);
  });

  it('should test userCanVisuallyInspectFreight', () => {
    let result: boolean;
    service.userCanVisuallyInspectFreight(mockTransportDocumentCreated).subscribe((response) => (result = response));
    expect(result).toBe(false);
  });

  it('should test userCanShowTransportDocumentHandoverQRCode', () => {
    let result: boolean;
    service
      .userCanShowTransportDocumentHandoverQRCode(mockTransportDocumentCreated)
      .subscribe((response) => (result = response));
    expect(result).toBe(false);
  });

  it('should test userCanCheckCarrierIdentity', () => {
    let result: boolean;
    service.userCanCheckCarrierIdentity(mockTransportDocumentCreated).subscribe((response) => (result = response));
    expect(result).toBe(false);
  });

  it('should test userCanVisuallyInspectOrder', () => {
    let result: boolean;
    service.userCanVisuallyInspectOrder(mockTransportDocumentCreated).subscribe((response) => (result = response));
    expect(result).toBe(false);
  });

  it('should test userCanVisuallyInspectOrder', () => {
    let result: boolean;
    service.userCanVisuallyInspectOrder(mockTransportDocumentCreated).subscribe((response) => (result = response));
    expect(result).toBe(false);
  });

  it('should test userCanDisableTransportDocument', () => {
    let result: boolean;
    service.userCanDisableTransportDocument(mockTransportDocumentCreated).subscribe((response) => (result = response));
    expect(result).toBe(false);
  });

  it('should test userCanCheckCarrierIdentity', () => {
    let result: boolean;
    service.userCanCheckCarrierIdentity(mockTransportDocumentCreated).subscribe((response) => (result = response));
    expect(result).toBe(false);
  });

  it('should test userCanTransferDangerousGoodRegistrationToTransportDocument', () => {
    let result: boolean;
    service.userCanTransferDangerousGoodRegistrationToTransportDocument().subscribe((response) => (result = response));
    expect(result).toBe(false);
  });
});
