/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class ThemeService {
  private toggleValue = new BehaviorSubject<boolean>(false);

  constructor() {
    this.toggleValue.next(JSON.parse(localStorage.getItem('dark-theme')) as boolean);
  }

  /**
   * Returns an observable of the current theme value
   *
   * @returns An observable of the current theme value
   */
  getThemeValue(): Observable<boolean> {
    return this.toggleValue.asObservable();
  }

  /**
   * Sets the theme value and persists it in the local storage.
   *
   * @param themeValue - The new theme value
   */
  setThemeValue(themeValue: boolean): void {
    this.toggleValue.next(themeValue);
    localStorage.setItem('dark-theme', themeValue.toString());
  }
}
