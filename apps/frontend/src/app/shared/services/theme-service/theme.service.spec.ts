/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { ThemeService } from './theme.service';

describe('ThemeService', () => {
  let service: ThemeService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [ThemeService] });
    service = TestBed.inject(ThemeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the current theme value', (done) => {
    service['toggleValue'].next(true);
    service.getThemeValue().subscribe((value: boolean) => {
      expect(value).toBe(true);
      done();
    });
  });

  it('should set the current theme value', () => {
    service.setThemeValue(true);
    expect(service['toggleValue'].getValue()).toBe(true);
    service.setThemeValue(false);
    expect(service['toggleValue'].getValue()).toBe(false);
  });
});
