/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { ListSortService } from './list-sort.service';
import {
  TransportDocumentListFilterService
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-filter-service/transport-document-list-filter.service';
import {
  TransportDocumentListNextActionService
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-next-action-service/transport-document-list-next-action.service';
import { UserServiceModule } from '@core/services/user-service/user-service.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { Address, Company, ContactPerson } from '@core/api-interfaces/lib/dtos/frontend';
import { FreightIdsIncluded } from '@core/api-interfaces/lib/dtos/frontend/transport-document/freight-ids-included';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import { Sort } from '@angular/material/sort';
import { CompareService } from '@shared/services/compare-service/compare.service';
import { TransportDocumentStatus } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/status.enum';
import {
  mockTransportDocumentTransport
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';

describe('ListSortService', () => {
  let service: ListSortService;

  const saveGoodsDataDtosMock = [mockTransportDocumentTransport, structuredClone(mockTransportDocumentTransport)];

  const saveDangerousGoodRegistrationsDtoMock: SaveDangerousGoodRegistrationDto[] = [
    new SaveDangerousGoodRegistrationDto(
      'DGR000001-20220310',
      'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
      new Company(
        'Chemical Industries Germany Inc.',
        new Address('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
        new ContactPerson('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
      ),
      new FreightIdsIncluded(
        [
          new OrderWithId(
            new Company(
              'Chemical Industries Germany Inc.',
              new Address('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
              new ContactPerson('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
            ),
            [
              new OrderPositionWithId(
                undefined,
                '',
                '',
                0,
                undefined,
                0,
                false,
                0,
                0,
                0,
                undefined,
                undefined,
                undefined
              ),
            ],
            ''
          ),
        ],
        'Beförderung ohne Freistellung nach ADR 1.1.3.6',
        '"Bitte Beförderungseinheit nach letzter Entladung reinigen."',
        780
      ),
      1646917164906,
      1646917164906
    ),
    new SaveDangerousGoodRegistrationDto(
      'DGR000001-20220310',
      'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
      new Company(
        'Chemical Industries Germany Inc.',
        new Address('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
        new ContactPerson('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
      ),
      new FreightIdsIncluded(
        [
          new OrderWithId(
            new Company(
              'Chemical Industries Germany Inc.',
              new Address('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
              new ContactPerson('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
            ),
            [
              new OrderPositionWithId(
                undefined,
                '',
                '',
                0,
                undefined,
                0,
                false,
                0,
                0,
                0,
                undefined,
                undefined,
                undefined
              ),
            ],
            ''
          ),
          new OrderWithId(
            new Company(
              'Chemical Industries Germany Inc.',
              new Address('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
              new ContactPerson('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
            ),
            [
              new OrderPositionWithId(
                undefined,
                '',
                '',
                0,
                undefined,
                0,
                false,
                0,
                0,
                0,
                undefined,
                undefined,
                undefined
              ),
            ],
            ''
          ),
        ],
        'Beförderung ohne Freistellung nach ADR 1.1.3.6',
        '"Bitte Beförderungseinheit nach letzter Entladung reinigen."',
        780
      ),
      1646917164907,
      1646917164907
    ),
  ];

  const compareServiceMock = {
    compare: jest.fn(),
    compareDate: jest.fn(),
    compareCompany: jest.fn(),
    compareCarrier: jest.fn(),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [UserServiceModule, HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [
        ListSortService,
        TransportDocumentListFilterService,
        TransportDocumentListNextActionService,
        {
          provide: CompareService,
          useValue: compareServiceMock,
        },
      ],
    });
    service = TestBed.inject(ListSortService);
    saveGoodsDataDtosMock[1].lastUpdate = 1629372601560;
    saveGoodsDataDtosMock[1].createdDate = 1629372434572;
    saveGoodsDataDtosMock[1].id = 'BP000002-20210819';
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should sort dangerous good registrations by last update date and created date', () => {
    const sortMockLastUpdate: Sort = {
      active: 'last_update',
      direction: 'asc',
    };
    const sortMockCreatedDate: Sort = {
      active: 'created_date',
      direction: 'asc',
    };

    service.sortDangerousGoodRegistrations(sortMockLastUpdate, saveDangerousGoodRegistrationsDtoMock);
    expect(compareServiceMock.compareDate).toHaveBeenCalledWith(new Date(1646917164907), new Date(1646917164906), true);
    compareServiceMock.compareDate.mockClear();
    service.sortDangerousGoodRegistrations(sortMockCreatedDate, saveDangerousGoodRegistrationsDtoMock);
    expect(compareServiceMock.compareDate).toHaveBeenCalledWith(new Date(1646917164907), new Date(1646917164906), true);

    const sortMockConsignee: Sort = {
      active: 'consignee',
      direction: 'asc',
    };
    const sortMockId: Sort = {
      active: 'id',
      direction: 'asc',
    };

    service.sortDangerousGoodRegistrations(sortMockConsignee, saveDangerousGoodRegistrationsDtoMock);
    expect(compareServiceMock.compare).toHaveBeenCalledWith(2, 1, true);
    compareServiceMock.compare.mockClear();
    service.sortDangerousGoodRegistrations(sortMockId, saveDangerousGoodRegistrationsDtoMock);
    expect(compareServiceMock.compare).toHaveBeenCalledWith('DGR000001-20220310', 'DGR000001-20220310', true);
  });

  it('should sort transport documents by last update date, created date, id, consignee, status, sender, and carrier', () => {
    const sortMockLastUpdate: Sort = {
      active: 'last_update',
      direction: 'asc',
    };
    const sortMockCreatedDate: Sort = {
      active: 'created_date',
      direction: 'asc',
    };

    const allStatus: TransportDocumentStatus[] = [
      TransportDocumentStatus.created,
      TransportDocumentStatus.accepted,
      TransportDocumentStatus.not_accepted,
      TransportDocumentStatus.released,
      TransportDocumentStatus.transport,
      TransportDocumentStatus.transport_denied,
      TransportDocumentStatus.vehicle_accepted,
      TransportDocumentStatus.vehicle_denied,
      TransportDocumentStatus.transport_completed,
      // TransportDocumentStatus.unknown,
      // 'carrier_confirmed',
    ];

    const displayStatus: Record<TransportDocumentStatus, boolean> = {
      accepted: true,
      created: true,
      not_accepted: true,
      released: true,
      transport: true,
      transport_denied: true,
      vehicle_accepted: true,
      vehicle_denied: true,
      transport_completed: true,
      unknown: true,
      // carrier_confirmed: true,
    };

    const myActions = 'Meine Aktionen';

    const allRequiredActionFilters: string[] = [myActions];

    const requiredActionFilters: Record<string, boolean> = {
      'Meine Aktionen': false,
    };

    compareServiceMock.compareDate.mockClear();
    service.sortTransportDocuments(
      sortMockLastUpdate,
      saveGoodsDataDtosMock,
      allStatus,
      displayStatus,
      allRequiredActionFilters,
      requiredActionFilters,
      myActions
    );
    expect(compareServiceMock.compareDate).toHaveBeenCalledWith(new Date(1629372601560), new Date(1629372601559), true);
    compareServiceMock.compareDate.mockClear();
    service.sortTransportDocuments(
      sortMockCreatedDate,
      saveGoodsDataDtosMock,
      allStatus,
      displayStatus,
      allRequiredActionFilters,
      requiredActionFilters,
      myActions
    );
    expect(compareServiceMock.compareDate).toHaveBeenCalledWith(new Date(1629372434572), new Date(1629372434571), true);

    const sortMockId: Sort = {
      active: 'id',
      direction: 'asc',
    };

    const sortMockConsignee: Sort = {
      active: 'consignee',
      direction: 'asc',
    };

    const sortMockStatus: Sort = {
      active: 'status',
      direction: 'asc',
    };

    compareServiceMock.compare.mockClear();
    service.sortTransportDocuments(
      sortMockId,
      saveGoodsDataDtosMock,
      allStatus,
      displayStatus,
      allRequiredActionFilters,
      requiredActionFilters,
      myActions
    );
    expect(compareServiceMock.compare).toHaveBeenCalledWith('BP000002-20210819', 'BP000001-20210819', true);
    compareServiceMock.compare.mockClear();
    service.sortTransportDocuments(
      sortMockConsignee,
      saveGoodsDataDtosMock,
      allStatus,
      displayStatus,
      allRequiredActionFilters,
      requiredActionFilters,
      myActions
    );
    expect(compareServiceMock.compare).toHaveBeenCalledWith(1, 1, true);
    compareServiceMock.compare.mockClear();
    service.sortTransportDocuments(
      sortMockStatus,
      saveGoodsDataDtosMock,
      allStatus,
      displayStatus,
      allRequiredActionFilters,
      requiredActionFilters,
      myActions
    );
    expect(compareServiceMock.compare).toHaveBeenCalledWith('transport', 'transport', true);

    const sortMockSender: Sort = {
      active: 'sender',
      direction: 'asc',
    };

    service.sortTransportDocuments(
      sortMockSender,
      saveGoodsDataDtosMock,
      allStatus,
      displayStatus,
      allRequiredActionFilters,
      requiredActionFilters,
      myActions
    );
    expect(compareServiceMock.compareCompany).toHaveBeenCalledWith(
      saveGoodsDataDtosMock[0].consignor,
      saveGoodsDataDtosMock[1].consignor,
      true
    );

    const sortMockCarrier: Sort = {
      active: 'carrier',
      direction: 'asc',
    };

    service.sortTransportDocuments(
      sortMockCarrier,
      saveGoodsDataDtosMock,
      allStatus,
      displayStatus,
      allRequiredActionFilters,
      requiredActionFilters,
      myActions
    );
    expect(compareServiceMock.compareCarrier).toHaveBeenCalledWith(
      saveGoodsDataDtosMock[0].carrier,
      saveGoodsDataDtosMock[1].carrier,
      true
    );
  });
});
