/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListSortService } from '@shared/services/list-sort-service/list-sort.service';
import { CompareModule } from '@shared/services/compare-service/compare.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, CompareModule],
  providers: [ListSortService],
})
export class ListSortModule {}
