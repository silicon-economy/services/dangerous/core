/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import {
  TransportDocumentListFilterService
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-filter-service/transport-document-list-filter.service';
import { Sort } from '@angular/material/sort';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { TransportDocumentStatus } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/status.enum';
import { CompareService } from '@shared/services/compare-service/compare.service';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';

@Injectable()
export class ListSortService {
  constructor(
    private readonly transportDocumentListFilterService: TransportDocumentListFilterService,
    private readonly compareService: CompareService
  ) {}

  /**
   * Sorts the transport documents based on the provided sorting state and filters.
   *
   * @param sortState - The sorting state object.
   * @param sortedData - The array of transport documents to be sorted.
   * @returns The sorted array of transport documents.
   */
  public sortDangerousGoodRegistrations(
    sortState: Sort,
    sortedData: SaveDangerousGoodRegistrationDto[]
  ): SaveDangerousGoodRegistrationDto[] {
    const data: SaveDangerousGoodRegistrationDto[] = [...sortedData];

    if (sortState.active && sortState.direction !== '') {
      data.sort((a: SaveDangerousGoodRegistrationDto, b: SaveDangerousGoodRegistrationDto) => {
        const isAsc = sortState.direction === 'asc';
        switch (sortState.active) {
          case 'id':
            return this.compareService.compare(a.id, b.id, isAsc);
          case 'consignee':
            return this.compareService.compare(a.freight.orders.length, b.freight.orders.length, isAsc);
          case 'created_date':
            return this.compareService.compareDate(new Date(a.createdDate), new Date(b.createdDate), isAsc);
          case 'last_update':
            return this.compareService.compareDate(new Date(a.lastUpdate), new Date(b.lastUpdate), isAsc);
          default:
            return 0;
        }
      });
    }
    return data;
  }

  /**
   * Sorts the transport documents based on the provided sorting state and filters.
   *
   * @param sortState - The sorting state object.
   * @param sortedData - The array of transport documents to be sorted.
   * @param allStatus - The array of all possible transport document statuses.
   * @param displayStatus - The object representing the display status filters.
   * @param allRequiredActionFilters - The array of all possible required action filters.
   * @param requiredActionFilters - The object representing the required action filters.
   * @param myActions - The string representing the user's actions.
   * @returns The sorted array of transport documents.
   */
  public sortTransportDocuments(
    sortState: Sort,
    sortedData: SaveGoodsDataDto[],
    allStatus: TransportDocumentStatus[],
    displayStatus: Record<TransportDocumentStatus, boolean>,
    allRequiredActionFilters: string[],
    requiredActionFilters: Record<string, boolean>,
    myActions: string
  ): SaveGoodsDataDto[] {
    const data: SaveGoodsDataDto[] = [...sortedData]
      .filter((saveGoodsDataDto: SaveGoodsDataDto) =>
        this.transportDocumentListFilterService.applyRequiredActionFilter(
          saveGoodsDataDto,
          allRequiredActionFilters,
          requiredActionFilters,
          myActions
        )
      )
      .filter((saveGoodsDataDto: SaveGoodsDataDto) =>
        this.transportDocumentListFilterService.filterByStatus(saveGoodsDataDto, allStatus, displayStatus)
      );

    if (sortState.active && sortState.direction !== '') {
      data.sort((a: SaveGoodsDataDto, b: SaveGoodsDataDto) => {
        const isAsc = sortState.direction === 'asc';
        switch (sortState.active) {
          case 'id':
            return this.compareService.compare(a.id, b.id, isAsc);
          case 'created_date':
            return this.compareService.compareDate(new Date(a.createdDate), new Date(b.createdDate), isAsc);
          case 'last_update':
            return this.compareService.compareDate(new Date(a.lastUpdate), new Date(b.lastUpdate), isAsc);
          //   TODO-DM: Rename sender to consignor for all occurrences
          case 'sender':
            return this.compareService.compareCompany(a.consignor, b.consignor, isAsc);
          case 'consignee':
            return this.compareService.compare(a.freight.orders.length, b.freight.orders.length, isAsc);
          case 'carrier':
            return this.compareService.compareCarrier(a.carrier, b.carrier, isAsc);
          case 'status':
            return this.compareService.compare(a.status, b.status, isAsc);
          default:
            return 0;
        }
      });
    }
    return data;
  }
}
