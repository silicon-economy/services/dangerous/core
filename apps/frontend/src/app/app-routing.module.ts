/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { authGuardIsLoggedIn, authGuardIsLoggedOut } from '@core/guards/auth.guard';

const routes: Routes = [
  {
    path: 'login',
    canActivate: [authGuardIsLoggedOut],
    loadChildren: () => import('./pages/login/login-page.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'transport-documents',
    canActivate: [authGuardIsLoggedIn],
    loadChildren: () =>
      import('@base/src/app/pages/transport-document/transport-documents-routing.module').then(
        (m) => m.TransportDocumentsRoutingModule
      ),
  },
  {
    path: 'dangerous-good-registrations',
    loadChildren: () =>
      import('@base/src/app/pages/dangerous-good-registration/dangerous-good-registrations-routing.module').then(
        (m) => m.DangerousGoodRegistrationsRoutingModule
      ),
  },
  {
    path: 'dangerous-good-registration-details-page-module',
    loadChildren: () =>
      import(
        './pages/dangerous-good-registration/dangerous-good-registration-details-page/dangerous-good-registration-details-page.module'
      ).then((m) => m.DangerousGoodRegistrationDetailsPageModule),
  },
  {
    path: 'legal-information',
    loadChildren: () =>
      import('@base/src/app/pages/legal-information/legal-information-routing.module').then(
        (m) => m.LegalInformationRoutingModule
      ),
  },
  { path: '**', redirectTo: 'login' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
