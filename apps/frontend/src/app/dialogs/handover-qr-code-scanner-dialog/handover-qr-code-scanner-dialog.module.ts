/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HandoverQrCodeScannerDialogComponent } from './handover-qr-code-scanner-dialog.component';
import { ScannerDialogModule } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.module';
import { QrCodeScannerModule } from '@shared/components/qr-code-scanner/qr-code-scanner.module';

@NgModule({
  declarations: [HandoverQrCodeScannerDialogComponent],
  imports: [CommonModule, ScannerDialogModule, QrCodeScannerModule],
})
export class HandoverQrCodeScannerDialogModule {}
