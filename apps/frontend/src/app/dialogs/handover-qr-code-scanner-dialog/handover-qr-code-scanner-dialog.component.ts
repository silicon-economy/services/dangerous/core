/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject, OnInit } from '@angular/core';
import { ScannerDialogComponent } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.component';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {
  ShippingLabelQrCodeScannerDialogComponent
} from '@base/src/app/dialogs/shipping-label-qr-code-scanner-dialog/shipping-label-qr-code-scanner-dialog.component';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-handover-shipping-label-qr-code-scanner-dialog',
  templateUrl: './handover-qr-code-scanner-dialog.component.html',
})
export class HandoverQrCodeScannerDialogComponent extends ScannerDialogComponent implements OnInit {
  titleHeader = '';

  constructor(
    private readonly notificationService: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: { titleHeader: string },
    dialogRef: MatDialogRef<ShippingLabelQrCodeScannerDialogComponent>,
    private readonly transportDocumentService: TransportDocumentService,
    private readonly translateService: TranslateService
  ) {
    super(dialogRef);
  }

  ngOnInit(): void {
    if (this.data.titleHeader !== undefined) this.titleHeader = this.data.titleHeader;
  }

  /**
   * Handles the scan event of a qr code for a transport document.
   *
   * @param transportDocumentId - The ID of the transport document to scan.
   */
  public async onScan(transportDocumentId: string): Promise<void> {
    try {
      if (transportDocumentId.startsWith('BP')) {
        await this.transportDocumentService.onViewTransportDocumentButtonClicked(transportDocumentId);

        this.notificationService.success(
          this.translateService.instant('dialogs.visualInspectionConsignee.notification.success') as string
        );
        this.closeDialog();
      } else {
        this.translateService
          .get('dialogs.visualInspectionConsignee.notification.unauthorized')
          .subscribe((translation: string) => this.notificationService.error(translation));
      }
    } catch (e) {
      this.notificationService.error(
        this.translateService.instant('dialogs.visualInspectionConsignee.notification.error') as string
      );
    }
  }
}
