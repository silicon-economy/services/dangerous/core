/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HandoverQrCodeScannerDialogComponent } from './handover-qr-code-scanner-dialog.component';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ScanDialogServiceModule } from '@shared/services/scan-dialog-service/scan-dialog-service.module';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { TranslateModule } from '@ngx-translate/core';
import { ScannerDialogModule } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.module';
import { QrCodeScannerModule } from '@shared/components/qr-code-scanner/qr-code-scanner.module';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import {
  mockTransportDocumentVehicleAccepted
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import {
  CarrierAndConsigneeCheckFreightServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight-service.module';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';

describe('HandoverQrCodeScannerDialogComponent', () => {
  let component: HandoverQrCodeScannerDialogComponent;
  let fixture: ComponentFixture<HandoverQrCodeScannerDialogComponent>;

  const route = {
    data: of({
      document: mockTransportDocumentVehicleAccepted,
    }),
    paramMap: of({
      get: jest.fn().mockReturnValue('1'),
    }),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NotificationServiceModule,
        HttpClientTestingModule,
        ScanDialogServiceModule,
        MatDialogModule,
        FeedbackDialogServiceModule,
        TranslateModule.forRoot(),
        QrCodeScannerModule,
        ScannerDialogModule,
        CarrierAndConsigneeCheckFreightServiceModule,
      ],
      declarations: [HandoverQrCodeScannerDialogComponent],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        TransportDocumentHttpService,
        { provide: ActivatedRoute, useValue: route },
        TransportDocumentService,
      ],
    });
    fixture = TestBed.createComponent(HandoverQrCodeScannerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
