/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-document-qr-code-dialog',
  templateUrl: './document-qr-code-dialog.component.html',
})
export class DocumentQrCodeDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: {
      dangerousGoodRegistrationId?: string;
      transportDocumentId?: string;
    }
  ) {}
}
