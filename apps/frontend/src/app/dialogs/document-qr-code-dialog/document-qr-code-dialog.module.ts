/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import {
  DocumentQrCodeDialogComponent
} from '@base/src/app/dialogs/document-qr-code-dialog/document-qr-code-dialog.component';
import { QRCodeModule } from 'angularx-qrcode';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';
import { NgIf } from '@angular/common';

@NgModule({
  declarations: [DocumentQrCodeDialogComponent],
  imports: [QRCodeModule, MatDialogModule, MatButtonModule, TranslateModule, NgIf],
})
export class DocumentQrCodeDialogModule {}
