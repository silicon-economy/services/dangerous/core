/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannerDialogComponent } from './scanner-dialog.component';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { MatIconModule } from '@angular/material/icon';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import {
  mockTransportDocumentVehicleAccepted
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import {
  CarrierAndConsigneeCheckFreightServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight-service.module';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { TranslateModule } from '@ngx-translate/core';

describe('ScannerDialogComponent', () => {
  let component: ScannerDialogComponent;
  let fixture: ComponentFixture<ScannerDialogComponent>;

  const route = {
    data: of({
      document: mockTransportDocumentVehicleAccepted,
    }),
    paramMap: of({
      get: jest.fn().mockReturnValue('1'),
    }),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        TitleBarModule,
        MatIconModule,
        CarrierAndConsigneeCheckFreightServiceModule,
        HttpClientTestingModule,
        FeedbackDialogServiceModule,
        NotificationServiceModule,
        TranslateModule.forRoot(),
      ],
      declarations: [ScannerDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {},
        },
        { provide: ActivatedRoute, useValue: route },
        TransportDocumentHttpService,
      ],
    });
    fixture = TestBed.createComponent(ScannerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
