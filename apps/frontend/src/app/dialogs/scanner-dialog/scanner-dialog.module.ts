/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { ScannerDialogComponent } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.component';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [ScannerDialogComponent],
  imports: [CommonModule, MatButtonModule, TitleBarModule, MatDialogModule, MatIconModule],
  exports: [ScannerDialogComponent],
})
export class ScannerDialogModule {}
