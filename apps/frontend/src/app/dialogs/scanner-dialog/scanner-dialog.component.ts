/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { tap } from 'rxjs';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import {
  CarrierAndConsigneeCheckFreightService
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight.service';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';
import {
  CarrierAndConsigneeCheckProcessorService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-processor-service/carrier-and-consignee-check-processor.service';

@Component({
  selector: 'app-scanner-dialog [title]',
  templateUrl: './scanner-dialog.component.html',
})
export class ScannerDialogComponent {
  @Input() public title!: string;
  public document!: SaveGoodsDataDto;
  public orderPosition!: OrderPositionWithId | undefined;
  public decideOrderPosition: OrderPositionWithId | undefined = undefined;

  constructor(
    protected readonly dialogRef: MatDialogRef<ScannerDialogComponent>,
    protected readonly route?: ActivatedRoute,
    protected readonly carrierAndConsigneeCheckFreightService?: CarrierAndConsigneeCheckFreightService,
    protected readonly carrierAndConsigneeCheckBehaviorSubjectService?: CarrierAndConsigneeCheckBehaviorSubjectService,
    protected readonly carrierAndConsigneeCheckProcessorService?: CarrierAndConsigneeCheckProcessorService
  ) {}

  /**
   * Closes the dialog
   */
  closeDialog(): void {
    this.dialogRef.close();
  }

  /**
   * Subscribes to the route data and updates the document if available
   */
  protected subscribeToRouteData(): void {
    this.route.data
      .pipe(
        tap((data) => {
          if (data.document !== undefined) {
            this.document = data.document as SaveGoodsDataDto;
          }
        })
      )
      .subscribe();
  }

  /**
   * Subscribes to the route paramMap and retrieves the order position
   */
  protected subscribeToParamMap(): void {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      this.retrieveOrderPosition(paramMap);
    });
  }

  /**
   * Retrieves the order position based on the orderPositionId
   *
   * @param paramMap - The route paramMap
   */
  protected retrieveOrderPosition(paramMap: ParamMap): void {
    const orderPositionId: number = +paramMap.get('orderPositionId');
    if (orderPositionId && this.document && this.document.freight) {
      // Use == because the backend sends the id as a string
      this.orderPosition = this.carrierAndConsigneeCheckFreightService.findOrderPosition(
        this.document,
        orderPositionId
      );
    }
  }

  /**
   * Subscribes to changes in the selected order position.
   */
  public subscribeToSelectedOrderPosition(): void {
    this.carrierAndConsigneeCheckBehaviorSubjectService
      .getDecideOrderPosition()
      .subscribe((orderPositionId: number) => {
        this.decideOrderPosition = this.carrierAndConsigneeCheckProcessorService.getOrderPositionById(
          this.document,
          orderPositionId
        );
      });
  }
}
