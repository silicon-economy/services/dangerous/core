/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { CommentDialogComponent } from './comment-dialog.component';
import {
  TransportDocumentStatus
} from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums';

describe('CommentDialogComponent', () => {
  let component: CommentDialogComponent;
  let fixture: ComponentFixture<CommentDialogComponent>;

  beforeEach(async () => {
    const mockRef: Partial<MatDialogRef<CommentDialogComponent>> = {};
    await TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [CommentDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockRef,
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            document: {
              logEntries: [],
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should filter the comment log entries', () => {
    const mockDate = new Date();
    expect(component.commentEntries).toEqual([]);
    component.data.document.logEntries.push(
      {
        date: mockDate.valueOf(),
        status: TransportDocumentStatus.accepted,
        acceptanceCriteria: {
          comment: 'foo',
        },
        author: 'bar',
      },
      {
        date: mockDate.valueOf(),
        status: TransportDocumentStatus.created,
      },
      {
        date: mockDate.valueOf(),
        status: TransportDocumentStatus.not_accepted,
        acceptanceCriteria: {
          comment: 'foo',
        },
      }
    );
    expect(component.commentEntries).toEqual([
      {
        date: mockDate.valueOf(),
        status: TransportDocumentStatus.accepted,
        acceptanceCriteria: {
          comment: 'foo',
        },
        author: 'bar',
      },
      {
        acceptanceCriteria: {
          comment: 'foo',
        },
        date: mockDate.valueOf(),
        status: 'not_accepted',
      },
    ]);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
