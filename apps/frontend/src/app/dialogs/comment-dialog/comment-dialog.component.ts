/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { LogEntry } from '@core/api-interfaces/lib/dtos/frontend/transport-document/log-entry';

@Component({
  selector: 'app-comment-dialog',
  templateUrl: './comment-dialog.component.html',
})
export class CommentDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: { document: SaveGoodsDataDto }) {}

  /**
   * Get all log entries with a comment
   *
   * @returns An array of log entries
   */
  get commentEntries(): LogEntry[] {
    const hasLogEntries: boolean = this.data?.document?.logEntries?.length > 0;

    if (hasLogEntries) {
      return this.data.document.logEntries.filter((logEntry: LogEntry) => !!logEntry.acceptanceCriteria?.comment);
    } else {
      return [];
    }
  }
}
