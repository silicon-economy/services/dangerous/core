/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentDialogComponent } from '@base/src/app/dialogs/comment-dialog/comment-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { StatusModule } from '@base/src/app/layouts/status/status.module';

@NgModule({
  declarations: [CommentDialogComponent],
  imports: [CommonModule, MatDialogModule, MatButtonModule, StatusModule],
})
export class CommentDialogModule {}
