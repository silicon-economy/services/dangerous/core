/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-write-to-blockchain-confirmation-dialog',
  templateUrl: './write-to-blockchain-confirmation-dialog.component.html',
})
export class WriteToBlockchainConfirmationDialogComponent {
  protected readonly location: Location = location;

  constructor(public readonly dialogRef: MatDialogRef<WriteToBlockchainConfirmationDialogComponent>) {}

  /**
   * Closes the dialog when the confirmation action is triggered.
   */
  public onConfirm(): void {
    this.dialogRef.close(true);
  }
}
