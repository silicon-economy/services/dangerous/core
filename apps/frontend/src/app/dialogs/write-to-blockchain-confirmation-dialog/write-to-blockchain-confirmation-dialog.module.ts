/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  WriteToBlockchainConfirmationDialogComponent
} from "@base/src/app/dialogs/write-to-blockchain-confirmation-dialog/write-to-blockchain-confirmation-dialog.component";
import { TranslateModule } from "@ngx-translate/core";
import { MatDialogModule } from "@angular/material/dialog";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";


@NgModule({
  declarations: [WriteToBlockchainConfirmationDialogComponent],
  imports: [
    CommonModule,
    TranslateModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [WriteToBlockchainConfirmationDialogComponent]
})
export class WriteToBlockchainConfirmationDialogModule { }
