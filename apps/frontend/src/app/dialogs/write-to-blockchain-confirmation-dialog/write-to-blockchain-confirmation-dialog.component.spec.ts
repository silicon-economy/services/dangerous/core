/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WriteToBlockchainConfirmationDialogComponent } from './write-to-blockchain-confirmation-dialog.component';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { TranslateModule } from '@ngx-translate/core';

describe('ConfirmationDialogComponent', (): void => {
  let component: WriteToBlockchainConfirmationDialogComponent;
  let fixture: ComponentFixture<WriteToBlockchainConfirmationDialogComponent>;

  beforeEach((): void => {
    TestBed.configureTestingModule({
      declarations: [WriteToBlockchainConfirmationDialogComponent],
      providers: [
        { provide: MatDialogRef, useValue: { close: jest.fn() } },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        TransportDocumentHttpService,
        HttpClient,
        HttpHandler,
      ],
      imports: [MatDialogModule, TranslateModule.forRoot()],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(WriteToBlockchainConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', (): void => {
    expect(component).toBeTruthy();
  });

  it('should close the dialog with true on confirmation', (): void => {
    const closeDialogSpy = jest.spyOn(component.dialogRef, 'close');
    component.onConfirm();
    expect(closeDialogSpy).toHaveBeenCalledWith(true);
  });
});
