/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarrierCheckFreightDialogComponent } from './carrier-check-freight-dialog.component';
import {
  CarrierAndConsigneeCheckFreightModule
} from '@shared/components/carrier-and-consignee-check-freight/carrier-and-consignee-check-freight.module';
import { ScannerDialogModule } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  CarrierCheckOrderPositionModule
} from '@base/src/app/dialogs/carrier-check-freight-dialog/components/carrier-check-order-position/carrier-check-order-position.module';
import {
  CarrierAndConsigneeCheckFreightServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight-service.module';

@NgModule({
  declarations: [CarrierCheckFreightDialogComponent],
  imports: [
    CommonModule,
    CarrierAndConsigneeCheckFreightModule,
    ScannerDialogModule,
    TranslateModule,
    CarrierCheckOrderPositionModule,
  ],
  providers: [CarrierAndConsigneeCheckFreightServiceModule],
})
export class CarrierCheckFreightDialogModule {}
