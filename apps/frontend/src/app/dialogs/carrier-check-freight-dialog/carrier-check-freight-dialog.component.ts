/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject, OnInit } from '@angular/core';
import { ScannerDialogComponent } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.component';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import { ActivatedRoute } from '@angular/router';
import {
  CarrierAndConsigneeCheckFreightService
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight.service';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';
import {
  CarrierAndConsigneeCheckProcessorService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-processor-service/carrier-and-consignee-check-processor.service';

@Component({
  selector: 'app-carrier-check-freight-dialog',
  templateUrl: './carrier-check-freight-dialog.component.html',
})
export class CarrierCheckFreightDialogComponent extends ScannerDialogComponent implements OnInit {
  constructor(
    protected readonly carrierAndConsigneeCheckFreightService: CarrierAndConsigneeCheckFreightService,
    protected readonly carrierAndConsigneeCheckBehaviorSubjectService: CarrierAndConsigneeCheckBehaviorSubjectService,
    protected readonly carrierAndConsigneeCheckProcessorService: CarrierAndConsigneeCheckProcessorService,
    protected dialogRef: MatDialogRef<CarrierCheckFreightDialogComponent>,
    protected route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA)
    private readonly data?: {
      document: SaveGoodsDataDto;
      orderPosition: OrderPositionWithId;
    }
  ) {
    super(
      dialogRef,
      route,
      carrierAndConsigneeCheckFreightService,
      carrierAndConsigneeCheckBehaviorSubjectService,
      carrierAndConsigneeCheckProcessorService
    );
    this.document = this.data.document ?? this.document;
    this.orderPosition = this.data.orderPosition ?? this.orderPosition;
  }

  ngOnInit(): void {
    this.subscribeToRouteData();
    this.subscribeToParamMap();
    this.subscribeToSelectedOrderPosition();
  }
}
