/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrierCheckFreightDialogComponent } from './carrier-check-freight-dialog.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ScannerDialogModule } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.module';
import {
  CarrierAndConsigneeCheckFreightModule
} from '@shared/components/carrier-and-consignee-check-freight/carrier-and-consignee-check-freight.module';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import {
  mockTransportDocumentTransport,
  mockTransportDocumentVehicleAccepted,
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';

describe('CarrierCheckFreightDialogComponent', () => {
  let component: CarrierCheckFreightDialogComponent;
  let fixture: ComponentFixture<CarrierCheckFreightDialogComponent>;

  const route = {
    data: of({
      document: mockTransportDocumentVehicleAccepted,
    }),
    paramMap: of({
      get: jest.fn().mockReturnValue('1'),
    }),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ScannerDialogModule,
        CarrierAndConsigneeCheckFreightModule,
        NotificationServiceModule,
        HttpClientTestingModule,
        FeedbackDialogServiceModule,
        NoopAnimationsModule,
        TranslateModule.forRoot(),
      ],
      declarations: [CarrierCheckFreightDialogComponent],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        TransportDocumentHttpService,
        { provide: ActivatedRoute, useValue: route },
      ],
    });
    fixture = TestBed.createComponent(CarrierCheckFreightDialogComponent);
    registerLocaleData(localeDe, 'de-DE', localeDeExtra);

    component = fixture.componentInstance;
    component.document = mockTransportDocumentTransport;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should find order position with specific id in all orders of a transport document', () => {
    component.ngOnInit();
    expect(component.orderPosition).toEqual(mockTransportDocumentVehicleAccepted.freight.orders[0].orderPositions[0]);
  });
});
