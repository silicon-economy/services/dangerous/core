/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarrierCheckOrderPositionComponent } from './carrier-check-order-position.component';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CriterionCheckModule } from '@shared/components/criterion-check/criterion-check.module';
import { OrderPositionViewModule } from '@shared/components/order-position-view/order-position-view.module';

@NgModule({
  declarations: [CarrierCheckOrderPositionComponent],
  imports: [
    CommonModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatExpansionModule,
    CriterionCheckModule,
    MatButtonModule,
    FormsModule,
    TranslateModule,
    OrderPositionViewModule,
  ],
  exports: [CarrierCheckOrderPositionComponent],
})
export class CarrierCheckOrderPositionModule {}
