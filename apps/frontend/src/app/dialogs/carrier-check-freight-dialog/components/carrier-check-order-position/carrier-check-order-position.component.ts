/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  VisualInspectionCarrierCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/visual-inspection-carrier-criteria';
import {
  OrderPositionCriterionCarrier
} from '@base/src/app/dialogs/carrier-check-freight-dialog/types/order-position-criterion-carrier.type';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';

@Component({
  selector: 'app-carrier-check-order-position[orderPosition][document]',
  templateUrl: './carrier-check-order-position.component.html',
  styleUrls: ['./carrier-check-order-position.component.scss'],
})
export class CarrierCheckOrderPositionComponent implements OnInit {
  @Input() public orderPosition!: OrderPositionWithId;
  @Input() public document!: SaveGoodsDataDto;
  @Output() checked: EventEmitter<VisualInspectionCarrierCriteria> =
    new EventEmitter<VisualInspectionCarrierCriteria>();
  public visualInspectionCarrierCriteria: VisualInspectionCarrierCriteria;

  constructor(
    private readonly carrierAndConsigneeCheckBehaviorSubjectService: CarrierAndConsigneeCheckBehaviorSubjectService
  ) {
    this.visualInspectionCarrierCriteria = new VisualInspectionCarrierCriteria(false, false, '');
  }

  ngOnInit(): void {
    this.carrierAndConsigneeCheckBehaviorSubjectService.getDecideOrderPosition().subscribe(() => {
      this.visualInspectionCarrierCriteria = new VisualInspectionCarrierCriteria(false, false, '');
    });
  }

  /**
   * Emit the result of the check by the carrier.
   */
  public onCheck(): void {
    if (this.orderPositionIsAccepted) {
      this.visualInspectionCarrierCriteria.comment = '';
    }
    this.checked.emit(this.visualInspectionCarrierCriteria);
  }

  /**
   * Sets the acceptance status of the given order position criterion to either true or false.
   *
   * @param orderPositionCriterion  - The order position criterion which is to be changed.
   * @param accepted  - The new status of the order position criterion.
   */
  public setOrderPositionCriterion(orderPositionCriterion: OrderPositionCriterionCarrier, accepted: boolean): void {
    this.visualInspectionCarrierCriteria[orderPositionCriterion] = accepted;
  }

  /**
   * Returns if the given order position would be accepted.
   *
   * @returns true if all order position criteria are set to true.
   */
  get orderPositionIsAccepted(): boolean {
    return (
      this.visualInspectionCarrierCriteria != null &&
      this.visualInspectionCarrierCriteria.acceptLabeling &&
      this.visualInspectionCarrierCriteria.acceptTransportability
    );
  }
}
