/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { Carrier } from '@core/api-interfaces/lib/dtos/frontend';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-core-check-carrier-identity-dialog',
  templateUrl: './check-carrier-identity-dialog.component.html',
})
export class CheckCarrierIdentityDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Carrier,
    private readonly dialogRef: MatDialogRef<CheckCarrierIdentityDialogComponent>
  ) {}

  /**
   * Closes the dialog.
   */
  public acceptIdentityOfCarrier(): void {
    this.dialogRef.close(true);
  }
}
