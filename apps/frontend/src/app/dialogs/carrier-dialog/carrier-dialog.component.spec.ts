/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrierDialogComponent } from './carrier-dialog.component';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { CarrierViewComponent } from '@base/src/app/layouts/carrier-view/carrier-view.component';

describe('CarrierDialogComponent', (): void => {
  let component: CarrierDialogComponent;
  let fixture: ComponentFixture<CarrierDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CarrierDialogComponent, CarrierViewComponent],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ],
      imports: [MatDialogModule, TranslateModule.forRoot()],
    });
    fixture = TestBed.createComponent(CarrierDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
