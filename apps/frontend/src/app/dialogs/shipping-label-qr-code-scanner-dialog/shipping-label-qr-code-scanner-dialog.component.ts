/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject, OnInit } from '@angular/core';
import { ScannerDialogComponent } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.component';
import { QrCodeLabel } from '@shared/interfaces/qr-code-label.interface';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ScanDialogService } from '@shared/services/scan-dialog-service/scan-dialog.service';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import {
  CarrierCheckFreightDialogComponent
} from '@base/src/app/dialogs/carrier-check-freight-dialog/carrier-check-freight-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-shipping-label-qr-code-scanner-dialog',
  templateUrl: './shipping-label-qr-code-scanner-dialog.component.html',
})
export class ShippingLabelQrCodeScannerDialogComponent extends ScannerDialogComponent implements OnInit {
  titleHeader = '';

  constructor(
    private readonly transportDocumentHttpService: TransportDocumentHttpService,
    private readonly notificationService: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: { titleHeader: string },
    dialogRef: MatDialogRef<ShippingLabelQrCodeScannerDialogComponent>,
    private readonly scanDialogService: ScanDialogService,
    protected readonly route: ActivatedRoute,
    private readonly translateService: TranslateService
  ) {
    super(dialogRef, route);
  }

  ngOnInit(): void {
    if (this.data.titleHeader !== undefined) {
      this.titleHeader = this.data.titleHeader;
    }
  }

  /**
   * Set scanned orderPosition as to be checked order position.
   *
   * @param result - The scanned value from the QR code.
   */
  public onScan(result: string): void {
    this.translateService
      .get('dialogs.visualInspectionConsignee.notification.error')
      .subscribe((translation: string) => {
        try {
          const qrObj: QrCodeLabel = JSON.parse(result) as QrCodeLabel;
          if (qrObj.orderPositionId) {
            this.transportDocumentHttpService.getTransportDocumentIdByOrderPositionId(qrObj.orderPositionId).subscribe({
              next: (transportDocumentId: string): void => {
                this.switchToCarrierCheckFreightDialog(transportDocumentId, qrObj.orderPositionId);
              },
              error: (): void => {
                this.notificationService.error(translation);
              },
            });
          } else {
            this.notificationService.error(translation);
          }
        } catch (e) {
          this.notificationService.error(translation);
        }
      });
  }

  /**
   * After scanning the QR code, the CarrierCheckFreightPageComponent will be shown in dialog
   *
   * @param transportDocumentId - Current document id
   * @param orderPositionId - Current order position id
   */
  public switchToCarrierCheckFreightDialog(transportDocumentId: string, orderPositionId: number): void {
    this.closeDialog();
    this.dialogRef.afterClosed().subscribe(() => {
      this.getTransportDocumentAndSwitchToCarrierCheckFreightDialog(transportDocumentId, orderPositionId);
    });
  }

  /**
   * Retrieves the transport document and performs the necessary operations.
   *
   * @param transportDocumentId - Current document id
   * @param orderPositionId - Current order position id
   */
  private getTransportDocumentAndSwitchToCarrierCheckFreightDialog(transportDocumentId: string, orderPositionId: number): void {
    this.transportDocumentHttpService
      .getTransportDocument(transportDocumentId)
      .subscribe((transportDocument: SaveGoodsDataDto): void => {
        const orderPosition: OrderPositionWithId = this.carrierAndConsigneeCheckFreightService.findOrderPosition(
          transportDocument,
          orderPositionId
        );
        const componentAttributes: {
          attributeName: string;
          attribute: SaveGoodsDataDto | OrderPositionWithId;
        }[] = this.createComponentAttributes(transportDocument, orderPosition);
        this.scanDialogService.createDialog(CarrierCheckFreightDialogComponent, componentAttributes);
      });
  }

  /**
   * Creates the component attributes for the dialog.
   *
   * @param transportDocument - The transport document
   * @param orderPosition - The order position
   * @returns The component attributes
   */
  private createComponentAttributes(
    transportDocument: SaveGoodsDataDto,
    orderPosition: OrderPositionWithId
  ): {
    attributeName: string;
    attribute: SaveGoodsDataDto | OrderPositionWithId;
  }[] {
    return [
      { attributeName: 'document', attribute: transportDocument },
      { attributeName: 'orderPosition', attribute: orderPosition },
    ];
  }
}
