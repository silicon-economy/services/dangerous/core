/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippingLabelQrCodeScannerDialogComponent } from './shipping-label-qr-code-scanner-dialog.component';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { ScannerDialogModule } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.module';
import { QrCodeScannerModule } from '@shared/components/qr-code-scanner/qr-code-scanner.module';
import { ScanDialogServiceModule } from '@shared/services/scan-dialog-service/scan-dialog-service.module';
import { QrCodeLabel } from '@shared/interfaces/qr-code-label.interface';
import { of, throwError } from 'rxjs';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  mockTransportDocumentVehicleAccepted
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import {
  CarrierAndConsigneeCheckFreightService
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight.service';
import {
  CarrierAndConsigneeCheckDistinctionService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-distinction-service/carrier-and-consignee-check-distinction.service';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';
import {
  CarrierAndConsigneeCheckProcessorService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-processor-service/carrier-and-consignee-check-processor.service';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { TranslateModule } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

describe('QrCodeScannerDialogComponent', () => {
  let component: ShippingLabelQrCodeScannerDialogComponent;
  let fixture: ComponentFixture<ShippingLabelQrCodeScannerDialogComponent>;

  const dialogMock = {
    close: () => {
      jest.fn();
    },
    afterClosed: () => of('Dialog closed'),
  };

  const route = {
    data: of({
      document: mockTransportDocumentVehicleAccepted,
    }),
    paramMap: of({
      get: jest.fn().mockReturnValue('1'),
    }),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule,
        MatDialogModule,
        ScannerDialogModule,
        QrCodeScannerModule,
        ScanDialogServiceModule,
        NoopAnimationsModule,
        FeedbackDialogServiceModule,
        TranslateModule.forRoot(),
      ],
      declarations: [ShippingLabelQrCodeScannerDialogComponent],
      providers: [
        TransportDocumentHttpService,
        CarrierAndConsigneeCheckFreightService,
        CarrierAndConsigneeCheckDistinctionService,
        CarrierAndConsigneeCheckBehaviorSubjectService,
        CarrierAndConsigneeCheckProcessorService,
        NotificationService,
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
        { provide: MatDialogRef, useValue: dialogMock },
        { provide: ActivatedRoute, useValue: route },
      ],
    });
    fixture = TestBed.createComponent(ShippingLabelQrCodeScannerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set scanned order position as to be checked order position', () => {
    const mockQrCodeLabel: QrCodeLabel = {
      orderId: 'foo',
      transportDocumentId: 'BP000001-20210819',
      orderPositionId: 1,
    };
    const transportDocumentHttpServiceSpy = jest
      .spyOn(TransportDocumentHttpService.prototype, 'getTransportDocumentIdByOrderPositionId')
      .mockReturnValue(of('BP000001-20210819'));
    const switchToNextDialogSpy = jest.spyOn(component, 'switchToCarrierCheckFreightDialog');

    component.onScan(JSON.stringify(mockQrCodeLabel));
    expect(transportDocumentHttpServiceSpy).toHaveBeenCalledWith(1);
    expect(switchToNextDialogSpy).toHaveBeenCalledWith(
      mockQrCodeLabel.transportDocumentId,
      mockQrCodeLabel.orderPositionId
    );
  });

  it('should output error due to invalid qr code', () => {
    const mockQrCodeLabel: QrCodeLabel = {
      orderId: 'foo',
      transportDocumentId: 'BP000001-20210819',
      orderPositionId: 1,
    };
    jest.spyOn(TransportDocumentHttpService.prototype, 'getTransportDocumentIdByOrderPositionId').mockReturnValue(
      throwError(() => {
        return new Error();
      })
    );
    const notificationServiceErrorSpy = jest.spyOn(NotificationService.prototype, 'error');

    component.onScan(JSON.stringify(mockQrCodeLabel));
    expect(notificationServiceErrorSpy).toHaveBeenCalledWith('dialogs.visualInspectionConsignee.notification.error');
  });

  it('should output error due to missing order position id', () => {
    const mockQrCodeLabel: Partial<QrCodeLabel> = {
      orderId: 'foo',
      transportDocumentId: 'BP000001-20210819',
    };
    const notificationServiceErrorSpy = jest.spyOn(NotificationService.prototype, 'error');

    component.onScan(JSON.stringify(mockQrCodeLabel));
    expect(notificationServiceErrorSpy).toHaveBeenCalledWith('dialogs.visualInspectionConsignee.notification.error');
  });

  it('should switch to next order position dialog', () => {
    const transportDocumentHttpServiceSpy = jest
      .spyOn(TransportDocumentHttpService.prototype, 'getTransportDocument')
      .mockReturnValue(of(mockTransportDocumentVehicleAccepted));
    const scanDialogServiceSpy = jest.spyOn(ShippingLabelQrCodeScannerDialogComponent.prototype as never, 'getTransportDocumentAndSwitchToCarrierCheckFreightDialog');

    component.switchToCarrierCheckFreightDialog('foo', 1);

    expect(transportDocumentHttpServiceSpy).toHaveBeenCalledWith('foo');
    expect(scanDialogServiceSpy).toHaveBeenCalledWith('foo', 1);
  });
});
