/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { IdPrefixes } from "@base/src/app/models/idPrefixes";

@Component({
  selector: 'app-accept-dangerous-good-registration-qr-code-scanner-dialog',
  templateUrl: './accept-dangerous-good-registration-qr-code-scanner-dialog.component.html',
})
export class AcceptDangerousGoodRegistrationQrCodeScannerDialogComponent {
  constructor(
    private readonly notificationService: NotificationService,
    @Inject(MAT_DIALOG_DATA) private data: { transportDocumentId: string },
    private dialogRef: MatDialogRef<AcceptDangerousGoodRegistrationQrCodeScannerDialogComponent>,
    private readonly translateService: TranslateService,
    private router: Router
  ) {}

  /**
   * Handles the scan event of a qr code for a dangerous good registration.
   *
   * @param dangerousGoodRegistrationId - The ID of the dangerous good registration to scan.
   */
  public async onScan(dangerousGoodRegistrationId: string): Promise<void> {
    try {
      if (dangerousGoodRegistrationId.startsWith(IdPrefixes.dangerousGoodRegistrationIdPrefix) && this.data?.transportDocumentId?.startsWith(IdPrefixes.transportDocumentIdPrefix)) {
        await this.router
          .navigateByUrl(
            'dangerous-good-registrations/' + dangerousGoodRegistrationId + '/accept/' + this.data.transportDocumentId
          )
          .then(() => this.dialogRef.close());
      } else if (dangerousGoodRegistrationId.startsWith(IdPrefixes.dangerousGoodRegistrationIdPrefix)) {
        await this.router
          .navigateByUrl('dangerous-good-registrations/' + dangerousGoodRegistrationId + '/accept')
          .then(() => this.dialogRef.close());
      }
    } catch (e) {
      this.translateService
        .get('dialogs.visualInspectionConsignee.notification.error')
        .subscribe((translation: string) => this.notificationService.error(translation));
    }
  }
}
