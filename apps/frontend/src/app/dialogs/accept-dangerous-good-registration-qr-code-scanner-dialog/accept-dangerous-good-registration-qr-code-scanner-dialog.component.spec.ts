/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import {
  AcceptDangerousGoodRegistrationQrCodeScannerDialogComponent
} from './accept-dangerous-good-registration-qr-code-scanner-dialog.component';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { ScannerDialogModule } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.module';
import { RouterTestingModule } from '@angular/router/testing';
import {
  CarrierAndConsigneeCheckFreightServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight-service.module';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { QrCodeScannerModule } from '@shared/components/qr-code-scanner/qr-code-scanner.module';
import { Router } from '@angular/router';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  mockTransportDocumentCreated
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import {
  mockDangerousGoodRegistration
} from "@core/api-interfaces/lib/mocks/dangerous-good-registration/dangerous-good-registration.mock";

describe('AcceptDangerousGoodRegistrationQrCodeScannerDialogComponent', () => {
  let component: AcceptDangerousGoodRegistrationQrCodeScannerDialogComponent;
  let fixture: ComponentFixture<AcceptDangerousGoodRegistrationQrCodeScannerDialogComponent>;

  const dialogMock = {
    close: jest.fn(),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NotificationServiceModule,
        TranslateModule.forRoot(),
        ScannerDialogModule,
        CarrierAndConsigneeCheckFreightServiceModule,
        HttpClientTestingModule,
        FeedbackDialogServiceModule,
        QrCodeScannerModule,
        RouterTestingModule.withRoutes([
          {
            path: 'dangerous-good-registrations/'+mockDangerousGoodRegistration.id+'/accept',
            redirectTo: '',
          },
          {
            path: 'dangerous-good-registrations/'+mockDangerousGoodRegistration.id+'/accept/'+mockTransportDocumentCreated.id,
            redirectTo: '',
          },
        ]),
        NoopAnimationsModule,
      ],
      declarations: [AcceptDangerousGoodRegistrationQrCodeScannerDialogComponent],
      providers: [
        TransportDocumentHttpService,
        { provide: MatDialogRef, useValue: dialogMock },
        { provide: MAT_DIALOG_DATA, useValue: [] },
      ],
    });
    fixture = TestBed.createComponent(AcceptDangerousGoodRegistrationQrCodeScannerDialogComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to "dangerous-good-registrations/<<dangerousGoodRegistrationId>>/accept" when onScan is called', async () => {
    const mockDangerousGoodRegistrationId = mockDangerousGoodRegistration.id;
    component['data'].transportDocumentId = '';

    const navigateSpy = jest.spyOn(Router.prototype, 'navigateByUrl');
    await component.onScan(mockDangerousGoodRegistrationId);

    expect(navigateSpy).toHaveBeenCalledWith('dangerous-good-registrations/'+mockDangerousGoodRegistrationId+'/accept');
  });

  it('should navigate to "dangerous-good-registrations/<<dangerousGoodRegistrationId>>/accept/<<this.data.transportDocumentId>>" when onScan is called', async () => {
    const mockDangerousGoodRegistrationId = mockDangerousGoodRegistration.id;
    component['data'].transportDocumentId = mockTransportDocumentCreated.id;

    const navigateSpy = jest.spyOn(Router.prototype, 'navigateByUrl');
    const dialogSpy = jest.spyOn(component['dialogRef'], 'close')
    await component.onScan(mockDangerousGoodRegistrationId);

    expect(navigateSpy).toHaveBeenCalledWith(
      'dangerous-good-registrations/'+mockDangerousGoodRegistrationId+'/accept/' + mockTransportDocumentCreated.id
    );
    expect(dialogSpy).toHaveBeenCalled();
  });

  it('should test the error case', async ()=>{
    const notificationServiceSpy = jest.spyOn(component['notificationService'],'error')
    await component.onScan('DGR-invalidId');
    expect(notificationServiceSpy).toHaveBeenCalled()
  })
});
