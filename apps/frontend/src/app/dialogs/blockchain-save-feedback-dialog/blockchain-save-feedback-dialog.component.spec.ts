/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import {
  BlockchainSaveFeedbackDialogComponent
} from '@base/src/app/dialogs/blockchain-save-feedback-dialog/blockchain-save-feedback-dialog.component';
import { TranslateModule } from '@ngx-translate/core';

describe('FeedbackDialogComponent', () => {
  let component: BlockchainSaveFeedbackDialogComponent;
  let fixture: ComponentFixture<BlockchainSaveFeedbackDialogComponent>;

  beforeEach(async () => {
    const mockRef: Partial<MatDialogRef<BlockchainSaveFeedbackDialogComponent>> = {};
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockRef,
        },
      ],
      declarations: [BlockchainSaveFeedbackDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockchainSaveFeedbackDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
