/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

/**
 * Give user feedback when saving to blockchain.
 */
@Component({
  selector: 'app-blockchain-save-feedback-dialog',
  templateUrl: './blockchain-save-feedback-dialog.component.html',
})
export class BlockchainSaveFeedbackDialogComponent {
  constructor(public dialogRef: MatDialogRef<BlockchainSaveFeedbackDialogComponent>) {}
}
