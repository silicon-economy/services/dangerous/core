/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import {
  BlockchainSaveFeedbackDialogComponent
} from '@base/src/app/dialogs/blockchain-save-feedback-dialog/blockchain-save-feedback-dialog.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [BlockchainSaveFeedbackDialogComponent],
  imports: [MatProgressBarModule, MatDialogModule, TranslateModule],
})
export class BlockchainSaveFeedbackDialogModule {}
