/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisableDocumentDialogComponent } from './disable-document-dialog.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import {
  BlockchainSaveFeedbackDialogModule
} from '@base/src/app/dialogs/blockchain-save-feedback-dialog/blockchain-save-feedback-dialog.module';
import { TranslateModule } from '@ngx-translate/core';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';

@NgModule({
  declarations: [DisableDocumentDialogComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    BlockchainSaveFeedbackDialogModule,
    TranslateModule,
    NotificationServiceModule,
  ],
})
export class DisableDocumentDialogModule {}
