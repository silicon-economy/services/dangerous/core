/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Company } from '@core/api-interfaces/lib/dtos/frontend';

@Component({
  selector: 'app-consignee-list-dialog',
  templateUrl: './consignee-list-dialog.component.html',
})
export class ConsigneeListDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: Company[]) {}
}
