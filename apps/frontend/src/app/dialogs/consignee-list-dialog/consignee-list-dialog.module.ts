/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ConsigneeListDialogComponent
} from '@base/src/app/dialogs/consignee-list-dialog/consignee-list-dialog.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { AddressDetailsModule } from '@shared/components/consignee-information/address-details.module';

@NgModule({
  declarations: [ConsigneeListDialogComponent],
  imports: [CommonModule, MatButtonModule, MatDialogModule, TranslateModule, AddressDetailsModule],
})
export class ConsigneeListDialogModule {}
