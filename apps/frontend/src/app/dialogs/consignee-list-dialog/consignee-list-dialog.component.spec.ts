/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { ConsigneeListDialogComponent } from './consignee-list-dialog.component';
import { TranslateModule } from '@ngx-translate/core';

describe('ConsigneeListDialogComponent', () => {
  let component: ConsigneeListDialogComponent;
  let fixture: ComponentFixture<ConsigneeListDialogComponent>;

  beforeEach(async () => {
    const mockRef: Partial<MatDialogRef<ConsigneeListDialogComponent>> = {};

    await TestBed.configureTestingModule({
      imports: [MatDialogModule, TranslateModule.forRoot()],
      declarations: [ConsigneeListDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockRef,
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: [],
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsigneeListDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
