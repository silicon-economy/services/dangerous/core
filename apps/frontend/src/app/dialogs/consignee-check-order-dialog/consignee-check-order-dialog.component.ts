/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject, OnInit } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ScannerDialogComponent } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.component';
import {
  CarrierAndConsigneeCheckFreightService
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight.service';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';
import {
  CarrierAndConsigneeCheckProcessorService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-processor-service/carrier-and-consignee-check-processor.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-consignee-check-order-dialog',
  templateUrl: './consignee-check-order-dialog.component.html',
})
export class ConsigneeCheckOrderDialogComponent extends ScannerDialogComponent implements OnInit {
  constructor(
    protected readonly carrierAndConsigneeCheckFreightService: CarrierAndConsigneeCheckFreightService,
    protected readonly carrierAndConsigneeCheckBehaviorSubjectService: CarrierAndConsigneeCheckBehaviorSubjectService,
    protected readonly carrierAndConsigneeCheckProcessorService: CarrierAndConsigneeCheckProcessorService,
    dialogRef: MatDialogRef<ConsigneeCheckOrderDialogComponent>,
    protected route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA)
    private readonly data?: {
      document: SaveGoodsDataDto;
      orderPosition: OrderPositionWithId;
    }
  ) {
    super(
      dialogRef,
      route,
      carrierAndConsigneeCheckFreightService,
      carrierAndConsigneeCheckBehaviorSubjectService,
      carrierAndConsigneeCheckProcessorService
    );
    this.document = this.data.document ?? this.document;
    this.orderPosition = this.data.orderPosition ?? this.orderPosition;
  }

  ngOnInit(): void {
    this.subscribeToRouteData();
    this.subscribeToParamMap();
    this.subscribeToSelectedOrderPosition();
  }
}
