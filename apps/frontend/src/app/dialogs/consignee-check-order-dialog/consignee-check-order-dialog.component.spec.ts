/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsigneeCheckOrderDialogComponent } from './consignee-check-order-dialog.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ScannerDialogModule } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.module';
import {
  CarrierAndConsigneeCheckFreightModule
} from '@shared/components/carrier-and-consignee-check-freight/carrier-and-consignee-check-freight.module';
import {
  ConsigneeCheckOrderModule
} from '@base/src/app/dialogs/consignee-check-order-dialog/components/consignee-check-order/consignee-check-order.module';
import {
  CarrierAndConsigneeCheckProcessorServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-processor-service/carrier-and-consignee-check-processor-service.module';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { of } from 'rxjs';
import {
  mockTransportDocumentVehicleAccepted
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import { ActivatedRoute } from '@angular/router';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('ConsigneeCheckOrderDialogComponent', () => {
  let component: ConsigneeCheckOrderDialogComponent;
  let fixture: ComponentFixture<ConsigneeCheckOrderDialogComponent>;

  const route = {
    data: of({
      document: mockTransportDocumentVehicleAccepted,
    }),
    paramMap: of({
      get: jest.fn().mockReturnValue('1'),
    }),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        FeedbackDialogServiceModule,
        MatDialogModule,
        NotificationServiceModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        ScannerDialogModule,
        CarrierAndConsigneeCheckFreightModule,
        ConsigneeCheckOrderModule,
        CarrierAndConsigneeCheckProcessorServiceModule,
        NoopAnimationsModule,
      ],
      declarations: [ConsigneeCheckOrderDialogComponent],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        {
          provide: MatDialogRef,
          useValue: {},
        },
        TransportDocumentHttpService,
        { provide: ActivatedRoute, useValue: route },
      ],
    });
    fixture = TestBed.createComponent(ConsigneeCheckOrderDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
