/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import {
  VisualInspectionConsigneeCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/visual-inspection-consignee-criteria';
import {
  OrderPositionCriterionConsignee
} from '@base/src/app/dialogs/consignee-check-order-dialog/types/order-position-criterion-consignee.type';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';

@Component({
  selector: 'app-consignee-check-order[orderPosition][document]',
  templateUrl: './consignee-check-order.component.html',
  styleUrls: ['./consignee-check-order.component.scss'],
})
export class ConsigneeCheckOrderComponent implements OnInit {
  @Input() public orderPosition!: OrderPositionWithId;
  @Input() public document!: SaveGoodsDataDto;
  public visualInspectionConsigneeCriteria: VisualInspectionConsigneeCriteria;
  @Output() checked: EventEmitter<VisualInspectionConsigneeCriteria> =
    new EventEmitter<VisualInspectionConsigneeCriteria>();

  constructor(
    private readonly carrierAndConsigneeCheckBehaviorSubjectService: CarrierAndConsigneeCheckBehaviorSubjectService
  ) {
    this.visualInspectionConsigneeCriteria = new VisualInspectionConsigneeCriteria(false, false, '');
  }

  ngOnInit(): void {
    this.carrierAndConsigneeCheckBehaviorSubjectService.getDecideOrderPosition().subscribe((): void => {
      this.visualInspectionConsigneeCriteria = new VisualInspectionConsigneeCriteria(false, false, '');
    });
  }

  /**
   * Emit the result of the check by the carrier.
   */
  public onCheck(): void {
    // Reset comment if order position is accepted
    if (this.orderPositionIsAccepted) {
      this.visualInspectionConsigneeCriteria.comment = '';
    }

    this.checked.emit(this.visualInspectionConsigneeCriteria);
  }

  /**
   * Sets the acceptance status of the given order position criterion to either true or false.
   *
   * @param orderPositionCriterion  - The order position criterion which is to be changed.
   * @param accepted  - The new status of the order position criterion.
   */
  public setOrderPositionCriterion(orderPositionCriterion: OrderPositionCriterionConsignee, accepted: boolean): void {
    this.visualInspectionConsigneeCriteria[orderPositionCriterion] = accepted;
  }

  /**
   * Returns if the given order position would be accepted.
   *
   * @returns true if all order position criteria are set to true.
   */
  get orderPositionIsAccepted(): boolean {
    return (
      this.visualInspectionConsigneeCriteria != null &&
      this.visualInspectionConsigneeCriteria.acceptIntactness &&
      this.visualInspectionConsigneeCriteria.acceptQuantity
    );
  }
}
