/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsigneeCheckOrderComponent } from './consignee-check-order.component';
import { TranslateModule } from '@ngx-translate/core';
import {
  CarrierAndConsigneeCheckFreightService
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight.service';
import {
  CarrierAndConsigneeCheckDistinctionService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-distinction-service/carrier-and-consignee-check-distinction.service';
import {
  CarrierAndConsigneeCheckBehaviorSubjectService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-behavior-subject-service/carrier-and-consignee-check-behavior-subject.service';
import {
  CarrierAndConsigneeCheckProcessorService
} from '@shared/services/carrier-and-consignee-check-freight-service/services/carrier-and-consignee-check-processor-service/carrier-and-consignee-check-processor.service';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { MatDialogModule } from '@angular/material/dialog';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { MatCardModule } from '@angular/material/card';
import { OrderPositionViewModule } from '@shared/components/order-position-view/order-position-view.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('CarrierCheckOrderPositionComponent', () => {
  let component: ConsigneeCheckOrderComponent;
  let fixture: ComponentFixture<ConsigneeCheckOrderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        MatDialogModule,
        NotificationServiceModule,
        MatCardModule,
        OrderPositionViewModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        FormsModule,
        NoopAnimationsModule,
      ],
      declarations: [ConsigneeCheckOrderComponent],
      providers: [
        CarrierAndConsigneeCheckFreightService,
        CarrierAndConsigneeCheckDistinctionService,
        CarrierAndConsigneeCheckBehaviorSubjectService,
        CarrierAndConsigneeCheckProcessorService,
        TransportDocumentService,
        TransportDocumentHttpService,
        FeedbackDialogService,
      ],
    });
    fixture = TestBed.createComponent(ConsigneeCheckOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
