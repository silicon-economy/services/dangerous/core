/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsigneeCheckOrderDialogComponent } from './consignee-check-order-dialog.component';
import { ScannerDialogModule } from '@base/src/app/dialogs/scanner-dialog/scanner-dialog.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  CarrierAndConsigneeCheckFreightModule
} from '@shared/components/carrier-and-consignee-check-freight/carrier-and-consignee-check-freight.module';
import {
  ConsigneeCheckOrderModule
} from '@base/src/app/dialogs/consignee-check-order-dialog/components/consignee-check-order/consignee-check-order.module';
import {
  CarrierAndConsigneeCheckFreightServiceModule
} from '@shared/services/carrier-and-consignee-check-freight-service/carrier-and-consignee-check-freight-service.module';
import {
  CarrierCheckOrderPositionModule
} from '@base/src/app/dialogs/carrier-check-freight-dialog/components/carrier-check-order-position/carrier-check-order-position.module';

@NgModule({
  declarations: [ConsigneeCheckOrderDialogComponent],
  imports: [
    CommonModule,
    ScannerDialogModule,
    TranslateModule,
    CarrierAndConsigneeCheckFreightModule,
    ConsigneeCheckOrderModule,
    CarrierCheckOrderPositionModule,
  ],
  providers: [CarrierAndConsigneeCheckFreightServiceModule],
})
export class ConsigneeCheckOrderDialogModule {}
