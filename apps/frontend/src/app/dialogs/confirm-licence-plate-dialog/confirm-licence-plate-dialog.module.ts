/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  ConfirmLicencePlateDialogComponent
} from '@base/src/app/dialogs/confirm-licence-plate-dialog/confirm-licence-plate-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [ConfirmLicencePlateDialogComponent],
  imports: [CommonModule, MatDialogModule, MatButtonModule, TranslateModule],
})
export class ConfirmLicencePlateDialogModule {}
