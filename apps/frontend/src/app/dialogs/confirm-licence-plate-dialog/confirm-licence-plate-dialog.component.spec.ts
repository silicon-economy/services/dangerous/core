/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { ConfirmLicencePlateDialogComponent } from './confirm-licence-plate-dialog.component';
import { TranslateModule } from '@ngx-translate/core';

describe('ConfirmLicencePlateDialogComponent', () => {
  let component: ConfirmLicencePlateDialogComponent;
  let fixture: ComponentFixture<ConfirmLicencePlateDialogComponent>;

  beforeEach(async () => {
    const mockref: Partial<MatDialogRef<ConfirmLicencePlateDialogComponent>> = {};
    await TestBed.configureTestingModule({
      imports: [MatDialogModule, TranslateModule.forRoot()],
      declarations: [ConfirmLicencePlateDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockref,
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data: {
              licencePlate: 'RO-AB123',
              accepted: true,
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmLicencePlateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
