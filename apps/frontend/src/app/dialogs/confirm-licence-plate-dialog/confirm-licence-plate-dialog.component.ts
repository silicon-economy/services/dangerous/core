/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-licence-plate-dialog',
  templateUrl: './confirm-licence-plate-dialog.component.html',
})
export class ConfirmLicencePlateDialogComponent {
  constructor(
    private readonly dialogRef: MatDialogRef<ConfirmLicencePlateDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      licencePlate: string;
      accepted: boolean;
    }
  ) {}

  /**
   * Confirm the action by closing the dialog and returning true.
   */
  public onConfirm(): void {
    this.dialogRef.close(true);
  }
}
