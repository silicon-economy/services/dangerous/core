/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, HostBinding, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { ThemeService } from '@shared/services/theme-service/theme.service';
import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'blockchain_europe_dangerous';
  @HostBinding('class') className = '';

  constructor(
    private readonly translateService: TranslateService,
    private readonly router: Router,
    private readonly spinner: NgxSpinnerService,
    private readonly themeService: ThemeService,
    private overlay: OverlayContainer
  ) {}

  ngOnInit(): void {
    this.initializeLanguageSettings();
    this.subscribeToRouterEvents();
    this.updateThemeCssClass();
  }

  /**
   * Updates the class and appearance of an element based on the value of
   * the theme retrieved from the ThemeService. Subscribes to the theme value
   * and applies the appropriate class and styling to the element.
   */
  private updateThemeCssClass(): void {
    this.themeService.getThemeValue().subscribe((isDarkTheme) => {
      const darkClassName = 'darkTheme';
      this.className = isDarkTheme ? darkClassName : '';
      if (isDarkTheme) {
        this.overlay.getContainerElement().classList.add(darkClassName);
      } else {
        this.overlay.getContainerElement().classList.remove(darkClassName);
      }
    });
  }

  /**
   * Initializes the language settings for translation and locale data.
   * Adds supported languages, sets the default language, and uses the
   * language stored in localStorage if valid. Registers locale data for
   * the default language.
   */
  private initializeLanguageSettings(): void {
    this.translateService.addLangs(['en', 'de']);
    this.translateService.setDefaultLang('de');
    const currentLanguage: string = localStorage.getItem('language');
    if (currentLanguage === ('en' || 'de')) {
      this.translateService.use(currentLanguage);
    }
    registerLocaleData(localeDe, 'de');
  }

  /**
   * Subscribes to router events and shows/hides the spinner based on the event type.
   */
  private subscribeToRouterEvents(): void {
    const showSpinnerEvents: (typeof NavigationStart)[] = [NavigationStart];
    const hideSpinnerEvents: (typeof NavigationEnd | typeof NavigationCancel | typeof NavigationError)[] = [
      NavigationEnd,
      NavigationCancel,
      NavigationError,
    ];

    this.router.events.subscribe((event) => {
      if (showSpinnerEvents.some((eventType) => event instanceof eventType)) {
        void this.spinner.show();
      } else if (hideSpinnerEvents.some((eventType) => event instanceof eventType)) {
        void this.spinner.hide();
      }
    });
  }
}
