/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { NavbarModule } from '@base/src/app/layouts/navigation/navbar/navbar.module';
import { LoginPageModule } from '@base/src/app/pages/login/login-page.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthenticationEnum } from '@base/src/app/models/authentication.enum';
import { MatButtonModule } from '@angular/material/button';
import { SideNavigationModule } from '@base/src/app/layouts/navigation/side-navigation/side-navigation.module';
import { ToolbarModule } from '@base/src/app/layouts/toolbar/toolbar.module';
import { DangerousGoodsGatewayHttpService } from '@shared/httpRequests/dangerous-goods-gateway-http.service';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule } from '@angular/material/form-field';
import { DangerousGoodRegistrationListPageModule } from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/dangerous-good-registration-list-page.module';
import { NgOptimizedImage } from '@angular/common';
import { UserServiceModule } from '@core/services/user-service/user-service.module';
import { TransportDocumentCheckModule } from '@base/src/app/pages/transport-document/transport-document-check-page/components/transport-document-check/transport-document-check.module';
import { VehicleInspectionPageModule } from '@base/src/app/pages/transport-document/vehicle-inspection-page/vehicle-inspection-page.module';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { environment } from '@environments/environment';

/**
 * Retrieves the access token from the local storage.
 *
 * @returns The access token as a string, or null if it is not found.
 */
export function tokenGetter() {
  return localStorage.getItem(AuthenticationEnum.accessToken);
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    NavbarModule,
    LoginPageModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: [environment.JWT_MODULE_ALLOWED_DOMAINS],
        disallowedRoutes: [],
      },
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, '../assets/i18n/', '.json'),
        deps: [HttpClient, HttpHandler],
      },
    }),
    MatButtonModule,
    SideNavigationModule,
    ToolbarModule,
    MatProgressBarModule,
    NgxSpinnerModule,
    TransportDocumentCheckModule,
    DangerousGoodRegistrationListPageModule,
    NgOptimizedImage,
    UserServiceModule,
    VehicleInspectionPageModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
  ],
  providers: [
    DangerousGoodsGatewayHttpService,
    { provide: LOCALE_ID, useValue: 'de' },
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { subscriptSizing: 'dynamic' } },
  ],
  bootstrap: [AppComponent],
  exports: [],
})
export class AppModule {}
