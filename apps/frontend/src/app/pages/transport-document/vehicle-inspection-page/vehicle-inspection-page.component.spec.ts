/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleInspectionPageComponent } from './vehicle-inspection-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { TranslateModule } from '@ngx-translate/core';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { Component, Input } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import {
  mockTransportDocumentTransport
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';

@Component({
  selector: 'app-vehicle-inspection[_transportDocument]',
  template: '',
})
class VehicleInspectionMockComponent {
  @Input() _transportDocument!: SaveGoodsDataDto;
}

describe('VehicleInspectionPageComponent', () => {
  let component: VehicleInspectionPageComponent;
  let fixture: ComponentFixture<VehicleInspectionPageComponent>;

  const saveGoodsDataDtoMock: SaveGoodsDataDto = mockTransportDocumentTransport;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TitleBarModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        FeedbackDialogServiceModule,
        NotificationServiceModule,
        MatDialogModule,
      ],
      declarations: [VehicleInspectionPageComponent, VehicleInspectionMockComponent],
      providers: [TransportDocumentHttpService],
    });
    fixture = TestBed.createComponent(VehicleInspectionPageComponent);
    component = fixture.componentInstance;
    component['_transportDocument'] = saveGoodsDataDtoMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
