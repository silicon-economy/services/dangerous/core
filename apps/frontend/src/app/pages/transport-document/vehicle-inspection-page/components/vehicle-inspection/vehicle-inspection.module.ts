/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VehicleInspectionComponent } from './vehicle-inspection.component';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';
import {
  ConfirmLicencePlateDialogModule
} from '@base/src/app/dialogs/confirm-licence-plate-dialog/confirm-licence-plate-dialog.module';
import { CriterionCheckModule } from '@shared/components/criterion-check/criterion-check.module';

@NgModule({
  declarations: [VehicleInspectionComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatInputModule,
    FormsModule,
    MatIconModule,
    MatExpansionModule,
    MatButtonModule,
    TranslateModule,
    ConfirmLicencePlateDialogModule,
    CriterionCheckModule,
  ],
  exports: [VehicleInspectionComponent],
})
export class VehicleInspectionModule {}
