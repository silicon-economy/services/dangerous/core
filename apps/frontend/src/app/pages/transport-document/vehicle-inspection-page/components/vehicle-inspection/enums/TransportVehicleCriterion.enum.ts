/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Available transport vehicle criteria
 */
export enum TransportVehicleCriterion {
  acceptVehicleCondition = 'acceptVehicleCondition',
  acceptVehicleSafetyEquipment = 'acceptVehicleSafetyEquipment',
  acceptCarrierInformation = 'acceptCarrierInformation',
  acceptCarrierSafetyEquipment = 'acceptCarrierSafetyEquipment',
}
