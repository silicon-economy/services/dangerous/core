/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, NgZone, OnDestroy, OnInit } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  TransportVehicleCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/transport-vehicle-criteria';
import {
  TransportVehicleCriterion
} from '@base/src/app/pages/transport-document/vehicle-inspection-page/components/vehicle-inspection/enums/TransportVehicleCriterion.enum';
import { Router } from '@angular/router';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { filter, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import {
  ConfirmLicencePlateDialogComponent
} from '@base/src/app/dialogs/confirm-licence-plate-dialog/confirm-licence-plate-dialog.component';

@Component({
  selector: 'app-vehicle-inspection[_transportDocument]',
  templateUrl: './vehicle-inspection.component.html',
  styleUrls: ['./vehicle-inspection.component.scss'],
})
export class VehicleInspectionComponent implements OnInit, OnDestroy {
  comment = '';
  protected readonly TransportVehicleCriterion = TransportVehicleCriterion;
  private subscriptions: Subscription[] = [];

  constructor(
    private readonly dialog: MatDialog,
    private readonly router: Router,
    private readonly transportDocumentHttpService: TransportDocumentHttpService,
    private readonly feedbackDialogService: FeedbackDialogService,
    private readonly notificationService: NotificationService,
    private readonly translateService: TranslateService,
    private readonly ngZone: NgZone
  ) {}

  @Input() _transportDocument!: SaveGoodsDataDto;

  get transportDocument(): SaveGoodsDataDto {
    return this._transportDocument;
  }

  private _transportVehicleCriteria: TransportVehicleCriteria;

  get transportVehicleCriteria(): TransportVehicleCriteria {
    return this._transportVehicleCriteria;
  }

  set transportVehicleCriteria(transportVehicleCriteria: TransportVehicleCriteria) {
    this._transportVehicleCriteria = transportVehicleCriteria;
  }

  /**
   * Returns if the given transport vehicle would be accepted
   *
   * @returns true if all transport vehicle criteria are set to true
   */
  get vehicleIsAccepted(): boolean {
    return (
      this.transportDocument != null &&
      this.transportVehicleCriteria != null &&
      this.transportVehicleCriteria.acceptVehicleCondition &&
      this.transportVehicleCriteria.acceptVehicleSafetyEquipment &&
      this.transportVehicleCriteria.acceptCarrierInformation &&
      this.transportVehicleCriteria.acceptCarrierSafetyEquipment
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  ngOnInit(): void {
    this._transportVehicleCriteria = new TransportVehicleCriteria(
      this.transportDocument.carrier?.licensePlate,
      false,
      false,
      this.transportDocument.carrier?.driver,
      false,
      false,
      ''
    );
  }

  /**
   * Sets the acceptance status of the given transport vehicle criterion to either true or false.
   *
   * @param transportVehicleCriterion - The transport vehicle criterion which is to be changed.
   * @param accepted - The new status of the transport vehicle criterion.
   */
  public setTransportVehicleCriterion(transportVehicleCriterion: TransportVehicleCriterion, accepted: boolean): void {
    this.transportVehicleCriteria[transportVehicleCriterion] = accepted;
  }

  /**
   * Display the confirmation license plate dialog
   */
  public onShowConfirmLicencePlate(): void {
    const licencePlate = this.transportVehicleCriteria.licencePlate;
    const vehicleIsAccepted = this.vehicleIsAccepted;

    this.subscriptions.push(
      this.dialog
        .open(ConfirmLicencePlateDialogComponent, {
          data: {
            licencePlate,
            accepted: vehicleIsAccepted,
          },
        })
        .afterClosed()
        .pipe(filter((result) => result === true))
        .subscribe(() => {
          this.updateTransportVehicleCriteria();
        })
    );
  }

  /**
   * Updates the transport vehicle criteria of the given transport document
   */
  public updateTransportVehicleCriteria(): void {
    this.transportVehicleCriteria.comment = this.vehicleIsAccepted ? '' : this.comment;
    this.feedbackDialogService.showDialog();

    this.subscriptions.push(
      this.transportDocumentHttpService
        .updateTransportVehicleCriteria(this.transportDocument.id, this.transportVehicleCriteria)
        .subscribe(() => {
          this.feedbackDialogService.handleSuccess();
          this.subscriptions.push(
            this.translateService
              .get('dialogs.confirmLicencePlate.notification.success')
              .subscribe((translation: string) => {
                this.notificationService.success(translation);
                this.ngZone.run(() => {
                  void this.router.navigate(['/documents']);
                });
              })
          );
        })
    );
  }
}
