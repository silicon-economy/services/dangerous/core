/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleInspectionComponent } from './vehicle-inspection.component';
import { MatDialogModule } from '@angular/material/dialog';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { CriterionCheckModule } from '@shared/components/criterion-check/criterion-check.module';
import {
  mockTransportDocumentReleased,
  mockTransportDocumentTransport,
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { of } from 'rxjs';
import {
  TransportVehicleCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/transport-vehicle-criteria';

describe('VehicleInspectionComponent', () => {
  let component: VehicleInspectionComponent;
  let fixture: ComponentFixture<VehicleInspectionComponent>;

  const saveGoodsDataDtoMock: SaveGoodsDataDto = mockTransportDocumentTransport;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        HttpClientTestingModule,
        FeedbackDialogServiceModule,
        NotificationServiceModule,
        TranslateModule.forRoot(),
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        CriterionCheckModule,
        NoopAnimationsModule,
        MatIconModule,
        MatExpansionModule,
        FormsModule,
      ],
      declarations: [VehicleInspectionComponent],
      providers: [TransportDocumentHttpService],
    });
    fixture = TestBed.createComponent(VehicleInspectionComponent);
    component = fixture.componentInstance;
    component['_transportDocument'] = saveGoodsDataDtoMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the necessary methods and services when updating transport vehicle criteria', () => {
    component.transportVehicleCriteria = new TransportVehicleCriteria(
      mockTransportDocumentReleased.carrier?.licensePlate,
      false,
      false,
      mockTransportDocumentReleased.carrier?.driver,
      false,
      false,
      ''
    );

    const feedbackDialogServiceShowDialogSpy = jest.spyOn(FeedbackDialogService.prototype, 'showDialog');
    const transportDocumentHttpServiceSpy = jest
      .spyOn(TransportDocumentHttpService.prototype, 'updateTransportVehicleCriteria')
      .mockReturnValue(of(undefined));
    const feedbackDialogServiceHandleSuccessSpy = jest.spyOn(FeedbackDialogService.prototype, 'handleSuccess');
    const translateServiceSpy = jest.spyOn(TranslateService.prototype, 'get');
    const notificationServiceSpy = jest.spyOn(NotificationService.prototype, 'success');

    component.updateTransportVehicleCriteria();

    expect(feedbackDialogServiceShowDialogSpy).toHaveBeenCalled();
    expect(transportDocumentHttpServiceSpy).toHaveBeenCalledWith(
      component.transportDocument.id,
      component.transportVehicleCriteria
    );
    expect(feedbackDialogServiceHandleSuccessSpy).toHaveBeenCalled();
    expect(translateServiceSpy).toHaveBeenCalledWith('dialogs.confirmLicencePlate.notification.success');
    expect(notificationServiceSpy).toHaveBeenCalled();
  });
});
