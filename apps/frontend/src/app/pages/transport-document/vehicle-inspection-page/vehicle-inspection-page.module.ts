/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VehicleInspectionPageRoutingModule } from './vehicle-inspection-page-routing.module';
import { VehicleInspectionPageComponent } from './vehicle-inspection-page.component';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import {
  VehicleInspectionModule
} from '@base/src/app/pages/transport-document/vehicle-inspection-page/components/vehicle-inspection/vehicle-inspection.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [VehicleInspectionPageComponent],
  imports: [CommonModule, VehicleInspectionPageRoutingModule, TitleBarModule, VehicleInspectionModule, TranslateModule],
})
export class VehicleInspectionPageModule {}
