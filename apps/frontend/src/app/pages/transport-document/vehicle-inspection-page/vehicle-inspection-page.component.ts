/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { ActivatedRoute, Data } from '@angular/router';

@Component({
  selector: 'app-vehicle-inspection-page',
  templateUrl: './vehicle-inspection-page.component.html'
})
export class VehicleInspectionPageComponent implements OnInit {
  constructor(private activatedRoute: ActivatedRoute) {}

  public _transportDocument!: SaveGoodsDataDto;

  get transportDocument(): SaveGoodsDataDto {
    return this._transportDocument;
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: Data) => {
      this._transportDocument = <SaveGoodsDataDto>data._transportDocument;
    });
  }
}
