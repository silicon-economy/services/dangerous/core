/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransportDocumentDetailsPageComponent } from './transport-document-details-page.component';
import { transportDocumentResolver } from '@core/resolvers/transport-document-resolver/transport-document.resolver';

const routes: Routes = [
  {
    path: '',
    component: TransportDocumentDetailsPageComponent,
    resolve: { transportDocument: transportDocumentResolver },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransportDocumentDetailsPageRoutingModule {}
