/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportDocumentDetailsPageComponent } from './transport-document-details-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import {
  TransportDocumentBaseDataViewComponent
} from '@base/src/app/pages/transport-document/transport-document-details-page/components/transport-document-base-data-view/transport-document-base-data-view.component';
import {
  ConsignorAndFreightViewModule
} from '@base/src/app/layouts/consignor-and-freight-view/consignor-and-freight-view.module';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { MatExpansionModule } from '@angular/material/expansion';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CarrierViewModule } from '@base/src/app/layouts/carrier-view/carrier-view.module';
import { MapModule } from '@maps/map.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SensingPuckDataViewModule } from '@base/src/app/layouts/sensing-puck-data-view/sensing-puck-data-view.module';
import { StatusModule } from '@base/src/app/layouts/status/status.module';
import {
  ShowStatusHistoryButtonModule
} from '@base/src/app/layouts/show-status-history-button/show-status-history-button.module';
import { OrderViewModule } from '@base/src/app/layouts/order-view/order-view.module';
import { TitleBarComponent } from '@base/src/app/layouts/title-bar/title-bar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import {
  TransportDocumentDetailsPageButtonBarComponent
} from '@base/src/app/pages/transport-document/transport-document-details-page/components/transport-document-details-page-button-bar/transport-document-details-page-button-bar.component';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import {
  mockTransportDocumentCreated
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import { ScanDialogServiceModule } from '@shared/services/scan-dialog-service/scan-dialog-service.module';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';

describe('TransportDocumentDetailsPageComponent', () => {
  let component: TransportDocumentDetailsPageComponent;
  let fixture: ComponentFixture<TransportDocumentDetailsPageComponent>;

  registerLocaleData(localeDe);
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        MatIconModule,
        MatCardModule,
        ConsignorAndFreightViewModule,
        MatExpansionModule,
        NoopAnimationsModule,
        CarrierViewModule,
        MapModule,
        HttpClientTestingModule,
        SensingPuckDataViewModule,
        StatusModule,
        ShowStatusHistoryButtonModule,
        OrderViewModule,
        RouterTestingModule,
        HttpClientTestingModule,
        FeedbackDialogServiceModule,
        NotificationServiceModule,
        ScanDialogServiceModule,
      ],
      declarations: [
        TransportDocumentDetailsPageComponent,
        TitleBarComponent,
        TransportDocumentBaseDataViewComponent,
        TransportDocumentDetailsPageButtonBarComponent,
      ],
      providers: [UserLoginService, UserAuthenticationService, TransportDocumentHttpService, TransportDocumentService],
    });
    fixture = TestBed.createComponent(TransportDocumentDetailsPageComponent);
    component = fixture.componentInstance;
    component.transportDocument = mockTransportDocumentCreated;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
