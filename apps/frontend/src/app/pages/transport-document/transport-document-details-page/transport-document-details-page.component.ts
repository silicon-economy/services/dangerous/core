/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { ActivatedRoute, Data } from '@angular/router';

@Component({
  selector: 'app-transport-document-details-page',
  templateUrl: './transport-document-details-page.component.html',
})
export class TransportDocumentDetailsPageComponent implements OnInit, OnChanges {
  @Input() showButtonBar = true;
  @Input() transportDocument: SaveGoodsDataDto;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: Data) => {
      if (!this.transportDocument) {
        this.transportDocument = <SaveGoodsDataDto>data.transportDocument;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.transportDocument) {
      this.transportDocument = changes.transportDocument.currentValue as SaveGoodsDataDto;
    }
  }
}
