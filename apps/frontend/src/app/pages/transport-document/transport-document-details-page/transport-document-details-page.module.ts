/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransportDocumentDetailsPageRoutingModule } from './transport-document-details-page-routing.module';
import { TransportDocumentDetailsPageComponent } from './transport-document-details-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import {
  TransportDocumentBaseDataViewComponent
} from '@base/src/app/pages/transport-document/transport-document-details-page/components/transport-document-base-data-view/transport-document-base-data-view.component';
import {
  StatusHistoryDialogComponent
} from '@base/src/app/layouts/show-status-history-button/dialogs/status-history-dialog/status-history-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MzdTimelineModule } from 'ngx-rend-timeline';
import { StatusModule } from '@base/src/app/layouts/status/status.module';
import { CompanyViewModule } from '@base/src/app/layouts/company-view/company-view.module';
import { FreightBaseDataViewModule } from '@base/src/app/layouts/freight-base-data-view/freight-base-data-view.module';
import { CarrierViewModule } from '@base/src/app/layouts/carrier-view/carrier-view.module';
import { MapModule } from '@maps/map.module';
import { SensingPuckDataViewModule } from '@base/src/app/layouts/sensing-puck-data-view/sensing-puck-data-view.module';
import { OrderViewModule } from '@base/src/app/layouts/order-view/order-view.module';
import { MatButtonModule } from '@angular/material/button';
import {
  ConsignorAndFreightViewModule
} from '@base/src/app/layouts/consignor-and-freight-view/consignor-and-freight-view.module';
import {
  ShowStatusHistoryButtonModule
} from '@base/src/app/layouts/show-status-history-button/show-status-history-button.module';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import {
  TransportDocumentDetailsPageButtonBarComponent
} from './components/transport-document-details-page-button-bar/transport-document-details-page-button-bar.component';
import {
  CheckCarrierIdentityDialogComponent
} from '@base/src/app/dialogs/check-carrier-identity-dialog/check-carrier-identity-dialog.component';
import { TransportDocumentService } from "@shared/services/transport-document-service/transport-document.service";

@NgModule({
    declarations: [
        TransportDocumentDetailsPageComponent,
        TransportDocumentBaseDataViewComponent,
        StatusHistoryDialogComponent,
        TransportDocumentDetailsPageButtonBarComponent,
        CheckCarrierIdentityDialogComponent,
    ],
    imports: [
        CommonModule,
        TransportDocumentDetailsPageRoutingModule,
        TitleBarModule,
        TranslateModule,
        MatIconModule,
        MatCardModule,
        MatExpansionModule,
        MatDialogModule,
        MzdTimelineModule,
        StatusModule,
        CompanyViewModule,
        FreightBaseDataViewModule,
        CarrierViewModule,
        MapModule,
        SensingPuckDataViewModule,
        OrderViewModule,
        MatButtonModule,
        ConsignorAndFreightViewModule,
        ShowStatusHistoryButtonModule,
    ],
    exports: [
        TransportDocumentDetailsPageComponent
    ],
    providers: [
        TransportDocumentService
    ]
})
export class TransportDocumentDetailsPageModule {}
