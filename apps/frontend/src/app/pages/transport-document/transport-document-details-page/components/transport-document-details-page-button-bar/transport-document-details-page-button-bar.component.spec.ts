/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportDocumentDetailsPageButtonBarComponent } from './transport-document-details-page-button-bar.component';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material/icon';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { MatDialogModule } from '@angular/material/dialog';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { ScanDialogServiceModule } from '@shared/services/scan-dialog-service/scan-dialog-service.module';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';

describe('TransportDocumentDetailsPageButtonBarComponent', () => {
  let component: TransportDocumentDetailsPageButtonBarComponent;
  let fixture: ComponentFixture<TransportDocumentDetailsPageButtonBarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TransportDocumentDetailsPageButtonBarComponent],
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        MatIconModule,
        FeedbackDialogServiceModule,
        MatDialogModule,
        NotificationServiceModule,
        ScanDialogServiceModule,
      ],
      providers: [UserLoginService, UserAuthenticationService, TransportDocumentHttpService, TransportDocumentService],
    });
    fixture = TestBed.createComponent(TransportDocumentDetailsPageButtonBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
