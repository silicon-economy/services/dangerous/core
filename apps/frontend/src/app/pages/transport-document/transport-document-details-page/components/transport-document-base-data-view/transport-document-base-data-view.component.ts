/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';

@Component({
  selector: 'app-transport-document-base-data-view[transportDocument]',
  templateUrl: './transport-document-base-data-view.component.html',
  styleUrls: ['./transport-document-base-data-view.component.scss'],
})
export class TransportDocumentBaseDataViewComponent {
  @Input() transportDocument!: SaveGoodsDataDto;
}
