/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';

import { TransportDocumentBaseDataViewComponent } from './transport-document-base-data-view.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import {
  ConsignorAndFreightViewModule
} from '@base/src/app/layouts/consignor-and-freight-view/consignor-and-freight-view.module';
import { StatusModule } from '@base/src/app/layouts/status/status.module';
import {
  ShowStatusHistoryButtonModule
} from '@base/src/app/layouts/show-status-history-button/show-status-history-button.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  mockTransportDocumentCreated
} from "@core/api-interfaces/lib/mocks/transport-document/transport-document.mock";

describe('TransportDocumentBaseDataViewComponent', () => {
  let component: TransportDocumentBaseDataViewComponent;
  let fixture: ComponentFixture<TransportDocumentBaseDataViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        TranslateModule.forRoot(),
        MatIconModule,
        MatCardModule,
        ShowStatusHistoryButtonModule,
        ConsignorAndFreightViewModule,
        MatExpansionModule,
        StatusModule,
        HttpClientTestingModule,
      ],
      declarations: [TransportDocumentBaseDataViewComponent],
      providers: [UserLoginService, UserAuthenticationService],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportDocumentBaseDataViewComponent);
    component = fixture.componentInstance;
    component.transportDocument = mockTransportDocumentCreated;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
