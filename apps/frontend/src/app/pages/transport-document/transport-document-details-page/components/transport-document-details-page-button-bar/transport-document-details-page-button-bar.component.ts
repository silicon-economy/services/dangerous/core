/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { PermissionService } from '@shared/services/permission-service/permission.service';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';

@Component({
  selector: 'app-transport-document-details-page-button-bar [transportDocument]',
  templateUrl: './transport-document-details-page-button-bar.component.html',
  styleUrls: ['./transport-document-details-page-button-bar.component.scss'],
})
export class TransportDocumentDetailsPageButtonBarComponent {
  @Input() transportDocument!: SaveGoodsDataDto;

  constructor(
    public permissionService: PermissionService,
    public transportDocumentService: TransportDocumentService
  ) {}
}
