/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateTransportDocumentPageRoutingModule } from './create-transport-document-page-routing.module';
import {
  TransportModeSelectionModule
} from '@base/src/app/layouts/transport-mode-selection/transport-mode-selection.module';
import {
  CreateTransportDocumentPageComponent
} from '@base/src/app/pages/transport-document/create-transport-document-page/create-transport-document-page.component';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import {
  EnterCarrierInformationModule
} from '@base/src/app/layouts/enter-carrier-information/enter-carrier-information.module';
import { MatIconModule } from '@angular/material/icon';
import {
  CreateDocumentSummaryModule
} from '@base/src/app/layouts/create-document-summary/create-document-summary.module';
import {
  EnterDangerousGoodsInformationModule
} from '@base/src/app/layouts/enter-dangerous-goods-information/enter-dangerous-goods-information.module';
import {
  EnterConsigneeInformationModule
} from '@base/src/app/layouts/enter-consignee-information/enter-consignee-information.module';
import {
  EnterCompanyInformationFormGroupModule
} from '@base/src/app/layouts/enter-company-information-form-group/enter-company-information-form-group.module';
import {
  WriteToBlockchainConfirmationDialogModule
} from '@base/src/app/dialogs/write-to-blockchain-confirmation-dialog/write-to-blockchain-confirmation-dialog.module';
import {
  CreateTransportDocumentService
} from '@base/src/app/pages/transport-document/create-transport-document-page/services/create-transport-document.service';

@NgModule({
  declarations: [CreateTransportDocumentPageComponent],
  imports: [
    CommonModule,
    TransportModeSelectionModule,
    CreateTransportDocumentPageRoutingModule,
    TitleBarModule,
    TranslateModule,
    MatCardModule,
    MatStepperModule,
    FormsModule,
    MatButtonModule,
    EnterCarrierInformationModule,
    MatIconModule,
    CreateDocumentSummaryModule,
    EnterDangerousGoodsInformationModule,
    EnterConsigneeInformationModule,
    EnterCompanyInformationFormGroupModule,
    WriteToBlockchainConfirmationDialogModule,
  ],
  providers: [CreateTransportDocumentService],
})
export class CreateTransportDocumentPageModule {}
