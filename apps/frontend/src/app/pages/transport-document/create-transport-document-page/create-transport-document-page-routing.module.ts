/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  CreateTransportDocumentPageComponent
} from '@base/src/app/pages/transport-document/create-transport-document-page/create-transport-document-page.component';

const routes: Routes = [
  {
    path: '',
    component: CreateTransportDocumentPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateTransportDocumentPageRoutingModule {}
