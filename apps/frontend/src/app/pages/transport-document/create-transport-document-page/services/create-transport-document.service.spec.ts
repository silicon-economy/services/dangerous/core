/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { CreateTransportDocumentService } from './create-transport-document.service';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { CarrierInformation } from '@base/src/app/models/carrierInformation';
import { of } from 'rxjs';
import {
  WriteToBlockchainConfirmationDialogComponent
} from '@base/src/app/dialogs/write-to-blockchain-confirmation-dialog/write-to-blockchain-confirmation-dialog.component';
import {
  mockTransportDocumentTransport
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import {
  mockCreateTransportDocumentRequestDto
} from '@core/api-interfaces/lib/mocks/transport-document/create-transport-document-request-dto.mock';
import { FeedbackDialogService } from "@shared/services/feedback-dialog-service/feedback-dialog.service";

describe('CreateTransportDocumentService', () => {
  let service: CreateTransportDocumentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatDialogModule],
      providers: [
        UserLoginService,
        UserAuthenticationService,
        TransportDocumentHttpService,
        CreateTransportDocumentService,
        FeedbackDialogService
      ],
    });
    service = TestBed.inject(CreateTransportDocumentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the createTransportDocumentRequestDto', () => {
    expect(service.getCreateTransportDocumentDto()).toEqual(service['createTransportDocumentDto']);
  });

  it('should set and then get the freight', () => {
    service.setFreight(mockCreateTransportDocumentRequestDto.freight);
    expect(service.getFreight()).toEqual(mockCreateTransportDocumentRequestDto.freight);
  });

  it('should set and get orders', () => {
    const ordersLength = service.getOrders().length;

    service.addEmptyOrder();
    expect(service.getOrders().length).toEqual(ordersLength + 1);

    const indexOfOrderToBeSet = service.getOrders().length - 1;
    const mockConsignee = mockCreateTransportDocumentRequestDto.freight.orders[0].consignee;
    service.setConsigneeOfOrder(mockConsignee, indexOfOrderToBeSet);
    expect(service.getOrders()[indexOfOrderToBeSet].consignee).toEqual(mockConsignee);

    service.removeOrder(indexOfOrderToBeSet);
    expect(service.getOrders().length).toEqual(ordersLength);
  });

  it('should set carrierInformation', () => {
    const carrierInformation = new CarrierInformation(
      mockCreateTransportDocumentRequestDto.carrier.name,
      mockCreateTransportDocumentRequestDto.carrier.driver,
      mockCreateTransportDocumentRequestDto.carrier.licensePlate,
      mockCreateTransportDocumentRequestDto.freight.transportationInstructions
    );
    service.setCarrierInformation(carrierInformation);
    expect(service.getCreateTransportDocumentDto().carrier).toEqual(mockCreateTransportDocumentRequestDto.carrier);
    expect(service.getCreateTransportDocumentDto().freight.transportationInstructions).toEqual(
      mockCreateTransportDocumentRequestDto.freight.transportationInstructions
    );
  });

  it('should set and get consignor', () => {
    service.setConsignor(mockCreateTransportDocumentRequestDto.consignor);
    expect(service.getConsignor()).toEqual(mockCreateTransportDocumentRequestDto.consignor);
  });

  it('should save transport document and navigate to its details page', () => {
    const matDialogSpy = jest.spyOn(service['matDialog'], 'open').mockReturnValue({
      afterClosed: () => of(true),
    } as MatDialogRef<typeof WriteToBlockchainConfirmationDialogComponent>);
    const transportDocumentHttpServiceSpy = jest
      .spyOn(service['transportDocumentHttpService'], 'createTransportDocument')
      .mockReturnValue(of(mockTransportDocumentTransport));
    const routerSpy = jest.spyOn(service['router'], 'navigateByUrl');

    service.saveTransportDocumentInBlockchain();
    expect(matDialogSpy).toHaveBeenCalled();
    expect(transportDocumentHttpServiceSpy).toHaveBeenCalled();
    expect(routerSpy).toHaveBeenCalledWith('/transport-documents/' + mockTransportDocumentTransport.id);
  });
});
