/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import {
  Carrier,
  Company,
  CreateGoodsDataDto,
  Freight,
  Order,
} from '@core/api-interfaces/lib/dtos/frontend';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { CarrierInformation } from '@base/src/app/models/carrierInformation';
import { MatDialog } from '@angular/material/dialog';
import { WriteToBlockchainConfirmationDialogComponent } from '@base/src/app/dialogs/write-to-blockchain-confirmation-dialog/write-to-blockchain-confirmation-dialog.component';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { Router } from '@angular/router';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { environment } from '@environments/environment';

@Injectable()
export class CreateTransportDocumentService {
  private createTransportDocumentDto: CreateGoodsDataDto;

  constructor(
    private userService: UserLoginService,
    private matDialog: MatDialog,
    private transportDocumentHttpService: TransportDocumentHttpService,
    private router: Router,
    private feedbackDialogService: FeedbackDialogService
  ) {
    this.userService.userCompany.subscribe(
      (consignor: Company) =>
        (this.createTransportDocumentDto = new CreateGoodsDataDto(
          consignor,
          new Freight([new Order(Company.createEmptyCompany(), [])], undefined, undefined, undefined),
          undefined
        ))
    );
  }

  /**
   * Retrieves the CreateGoodsDataDto object containing transport document.
   *
   * @returns CreateGoodsDataDto
   */
  public getCreateTransportDocumentDto(): CreateGoodsDataDto {
    return this.createTransportDocumentDto;
  }

  /**
   * Retrieves the freight information from the transport document data.
   *
   * @returns Freight
   */
  public getFreight(): Freight {
    return this.createTransportDocumentDto.freight;
  }

  /**
   * Sets the updated freight in the transport document.
   *
   * @param updatedFreight - Freight.
   */
  public setFreight(updatedFreight: Freight): void {
    this.createTransportDocumentDto.freight = updatedFreight;
  }

  /**
   * Retrieves the array of orders from the freight information.
   *
   * @returns Order[].
   */
  public getOrders(): Order[] {
    return this.createTransportDocumentDto.freight.orders;
  }

  /**
   * Adds an empty order to the list of orders in the transport document.
   */
  public addEmptyOrder(): void {
    this.createTransportDocumentDto.freight.orders.push(new Order(Company.createEmptyCompany(), []));
  }

  /**
   * Updates the consignee information of a specific order.
   *
   * @param updatedConsignee - Company.
   * @param orderIndex - Number of one order.
   */
  public setConsigneeOfOrder(updatedConsignee: Company, orderIndex: number): void {
    this.createTransportDocumentDto.freight.orders[orderIndex].consignee = updatedConsignee;
  }

  /**
   * Removes the order at the specified index from the list of orders in the transport document.
   *
   * @param orderIndex - Number of one order.
   */
  public removeOrder(orderIndex: number): void {
    this.createTransportDocumentDto.freight.orders.splice(orderIndex, 1);
  }

  /**
   * Sets the carrier information in the transport document data.
   *
   * @param carrierInformation - CarrierInformation.
   */
  public setCarrierInformation(carrierInformation: CarrierInformation): void {
    this.createTransportDocumentDto.freight.transportationInstructions = carrierInformation.transportationInstructions;
    this.createTransportDocumentDto.carrier = new Carrier(
      carrierInformation.name,
      carrierInformation.driver,
      carrierInformation.licensePlate
    );
  }

  /**
   * Retrieves the consignor.
   *
   * @returns Company
   */
  public getConsignor(): Company {
    return this.createTransportDocumentDto.consignor;
  }

  /**
   * Sets the consignor.
   *
   * @param consignor - Company.
   */
  public setConsignor(consignor: Company): void {
    this.createTransportDocumentDto.consignor = consignor;
  }

  /**
   * Creates a new transport document after user confirmation.
   */
  public saveTransportDocumentInBlockchain(): void {
    this.matDialog
      .open(WriteToBlockchainConfirmationDialogComponent)
      .afterClosed()
      .subscribe((accept): void => {
        if (accept === true) {
          this.feedbackDialogService.showDialog();
          this.transportDocumentHttpService
            .createTransportDocument(this.createTransportDocumentDto)
            .subscribe((response: SaveGoodsDataDto): void => {
              this.feedbackDialogService.handleSuccess();
              this.router
                .navigateByUrl(`${environment.ROUTER_LINKS.TRANSPORT_DOCUMENT_LIST}` + response.id)
                .catch((error): void => {
                  throw error;
                });
            });
        }
      });
  }
}
