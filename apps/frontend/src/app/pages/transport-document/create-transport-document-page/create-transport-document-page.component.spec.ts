/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTransportDocumentPageComponent } from './create-transport-document-page.component';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { TranslateModule } from '@ngx-translate/core';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import {
  TransportModeSelectionModule
} from '@base/src/app/layouts/transport-mode-selection/transport-mode-selection.module';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import {
  EnterDangerousGoodsInformationModule
} from '@base/src/app/layouts/enter-dangerous-goods-information/enter-dangerous-goods-information.module';
import { DangerousGoodsGatewayHttpService } from '@shared/httpRequests/dangerous-goods-gateway-http.service';
import {
  EnterCarrierInformationModule
} from '@base/src/app/layouts/enter-carrier-information/enter-carrier-information.module';
import {
  EnterCompanyInformationFormGroupModule
} from '@base/src/app/layouts/enter-company-information-form-group/enter-company-information-form-group.module';
import {
  CreateTransportDocumentService
} from '@base/src/app/pages/transport-document/create-transport-document-page/services/create-transport-document.service';
import {
  CreateDocumentSummaryModule
} from '@base/src/app/layouts/create-document-summary/create-document-summary.module';
import {
  EnterDangerousGoodsInformationComponent
} from '@base/src/app/layouts/enter-dangerous-goods-information/enter-dangerous-goods-information.component';
import { FeedbackDialogService } from "@shared/services/feedback-dialog-service/feedback-dialog.service";

describe('CreateTransportDocumentPageComponent', () => {
  let component: CreateTransportDocumentPageComponent;
  let fixture: ComponentFixture<CreateTransportDocumentPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateTransportDocumentPageComponent],
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        TranslateModule.forRoot(),
        TitleBarModule,
        TransportModeSelectionModule,
        MatCardModule,
        MatStepperModule,
        NoopAnimationsModule,
        MatIconModule,
        EnterDangerousGoodsInformationModule,
        EnterCarrierInformationModule,
        EnterCompanyInformationFormGroupModule,
        CreateDocumentSummaryModule,
      ],
      providers: [
        CreateTransportDocumentService,
        UserLoginService,
        UserAuthenticationService,
        TransportDocumentHttpService,
        DangerousGoodsGatewayHttpService,
        FeedbackDialogService
      ],
    });
    fixture = TestBed.createComponent(CreateTransportDocumentPageComponent);
    component = fixture.componentInstance;
    component.enterDangerousGoodsInformationComponent = {
      addOrderValidityStorage: jest.fn(),
      removeOrderValidityStorage() {
        jest.fn();
      },
    } as unknown as EnterDangerousGoodsInformationComponent;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add and remove a consignee and check the validity', () => {
    component.addConsignee();
    component.removeConsignee(1);
    expect(component.allEnterConsigneeInformationComponentsAreFilledOutValidly()).toEqual(false);
  });
});
