/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, ViewChild } from '@angular/core';
import {
  CreateTransportDocumentService
} from '@base/src/app/pages/transport-document/create-transport-document-page/services/create-transport-document.service';
import {
  EnterDangerousGoodsInformationComponent
} from '@base/src/app/layouts/enter-dangerous-goods-information/enter-dangerous-goods-information.component';

@Component({
  selector: 'app-create-transport-document-page',
  templateUrl: './create-transport-document-page.component.html'
})
export class CreateTransportDocumentPageComponent {
  /**
   * Because ngOnChanges doesn't recognize changes in nested objects by itself, the wanted changes are triggered manually by referencing the viewChild
   */
  @ViewChild('enterDangerousGoodsInformationComponent')
  public enterDangerousGoodsInformationComponent: EnterDangerousGoodsInformationComponent;

  /**
   * An array holding information about which enterConsigneeInformationComponents have valid inputs.
   * If it only holds true values, step 1 has been filled out is validly.
   */
  public validityOfEnterConsigneeInformationComponent = [false];

  /**
   * A variable holding information about whether step 2 (dangerous goods) was filled out validly.
   */
  public allEnteredDangerousGoodInformationAreValid = false;

  /**
   * A variable holding information about whether step 3 (carrier information) was filled out validly.
   */
  public carrierInformationIsValid = false;

  /**
   * A variable holding information about whether step 4 (consignor) was filled out validly.
   */
  public consignorInformationIsValid = false;

  constructor(public createTransportDocumentService: CreateTransportDocumentService) {}

  /**
   * Adds a new consignee and creates the associated form component reference.
   */
  public addConsignee(): void {
    this.createTransportDocumentService.addEmptyOrder();
    this.validityOfEnterConsigneeInformationComponent.push(false);
    // Unfortunately ngOnChanges doesn't recognize changes in nested objects by itself and this line is more performant than the workarounds
    this.enterDangerousGoodsInformationComponent.addOrderValidityStorage();
  }

  /**
   * Removes a consignee from the list of consignees.
   *
   * @param index - The unique index of the consignee to remove.
   */
  public removeConsignee(index: number): void {
    this.createTransportDocumentService.removeOrder(index);
    this.validityOfEnterConsigneeInformationComponent.splice(index, 1);
    // Unfortunately ngOnChanges doesn't recognize changes in nested objects by itself and this line is more performant than the workarounds
    this.enterDangerousGoodsInformationComponent.removeOrderValidityStorage(index);
  }

  /**
   * @returns true if all enterConsigneeInformationComponents are filled out validly
   */
  public allEnterConsigneeInformationComponentsAreFilledOutValidly(): boolean {
    return this.validityOfEnterConsigneeInformationComponent.every((valid) => valid);
  }
}
