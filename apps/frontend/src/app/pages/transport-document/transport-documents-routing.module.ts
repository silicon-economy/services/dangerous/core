/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {authGuardIsConsignor} from "@core/guards/auth.guard";

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import(
        '@base/src/app/pages/transport-document/transport-document-list-page/transport-document-list-page.module'
      ).then((m) => m.TransportDocumentListPageModule),
  },
  {
    path: 'create',
    canActivate: [authGuardIsConsignor],
    loadChildren: () =>
      import(
        '@base/src/app/pages/transport-document/create-transport-document-page/create-transport-document-page.module'
        ).then((m) => m.CreateTransportDocumentPageModule),
  },
  {
    path: 'check/:id',
    loadChildren: () =>
      import(
        '@base/src/app/pages/transport-document/transport-document-check-page/transport-document-check-page.module'
      ).then((m) => m.TransportDocumentCheckPageModule),
  },
  {
    path: 'vehicleInspection/:id',
    loadChildren: () =>
      import('@base/src/app/pages/transport-document/vehicle-inspection-page/vehicle-inspection-page.module').then(
        (m) => m.VehicleInspectionPageModule
      ),
  },
  {
    path: ':id',
    loadChildren: () =>
      import(
        '@base/src/app/pages/transport-document/transport-document-details-page/transport-document-details-page.module'
      ).then((m) => m.TransportDocumentDetailsPageModule),
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransportDocumentsRoutingModule {}
