/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FreightBaseDataViewRoutingModule } from './freight-base-data-view-routing.module';
import {
  FreightBaseDataViewComponent
} from '@base/src/app/pages/transport-document/transport-document-check-page/components/transport-document-check/components/freight-base-data-view/freight-base-data-view.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [FreightBaseDataViewComponent],
  imports: [CommonModule, FreightBaseDataViewRoutingModule, TranslateModule],
  exports: [FreightBaseDataViewComponent],
})
export class FreightBaseDataViewModule {}
