/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Available acceptance criteria
 */
export enum AcceptanceCriterion {
  acceptConsignor = 'acceptConsignor',
  acceptConsignees = 'acceptConsignees',
  acceptFreight = 'acceptFreight',
  acceptTransportationInstructions = 'acceptTransportationInstructions',
  acceptAdditionalInformation = 'acceptAdditionalInformation',
}
