/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportDocumentCheckComponent } from './transport-document-check.component';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { MatDialogModule } from '@angular/material/dialog';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  CarrierCheckCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/carrier-check-criteria';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { of } from 'rxjs';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import {
  mockTransportDocumentTransport
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';

describe('TransportDocumentCheckComponent', () => {
  let component: TransportDocumentCheckComponent;
  let fixture: ComponentFixture<TransportDocumentCheckComponent>;

  const mockTransportDocument: SaveGoodsDataDto = mockTransportDocumentTransport;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        FeedbackDialogServiceModule,
        MatDialogModule,
        NotificationServiceModule,
        TranslateModule.forRoot(),
        MatIconModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        NoopAnimationsModule,
      ],
      declarations: [TransportDocumentCheckComponent],
      providers: [TransportDocumentHttpService],
    });
    fixture = TestBed.createComponent(TransportDocumentCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update acceptance criteria of a transport document', () => {
    const carrierCheckCriteriaMock: CarrierCheckCriteria = new CarrierCheckCriteria(
      true,
      [true, true],
      true,
      true,
      true
    );

    component['_transportDocument'] = mockTransportDocument;
    component.acceptanceCriteria = carrierCheckCriteriaMock;

    const feedbackDialogServiceShowDialogSpy = jest.spyOn(FeedbackDialogService.prototype, 'showDialog');
    const transportDocumentHttpServiceSpy = jest
      .spyOn(TransportDocumentHttpService.prototype, 'transportDocumentCarrierCheck')
      .mockReturnValue(of(undefined));
    const feedbackDialogServiceHandleSuccessSpy = jest.spyOn(FeedbackDialogService.prototype, 'handleSuccess');
    const translateServiceSpy = jest.spyOn(TranslateService.prototype, 'get');
    const notificationServiceSpy = jest.spyOn(NotificationService.prototype, 'success');

    component.updateAcceptanceCriteria();

    expect(feedbackDialogServiceShowDialogSpy).toHaveBeenCalled();
    expect(transportDocumentHttpServiceSpy).toHaveBeenCalledWith(
      component.transportDocument.id,
      component.acceptanceCriteria
    );
    expect(feedbackDialogServiceHandleSuccessSpy).toHaveBeenCalled();
    expect(translateServiceSpy).toHaveBeenCalledWith('dialogs.checkTransportDocument.notification.success');
    expect(notificationServiceSpy).toHaveBeenCalled();
  });
});
