/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransportDocumentCheckRoutingModule } from './transport-document-check-routing.module';
import { TransportDocumentCheckComponent } from './transport-document-check.component';
import { MatIconModule } from '@angular/material/icon';
import { CriterionCheckModule } from '@shared/components/criterion-check/criterion-check.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { AddressDetailsModule } from '@shared/components/consignee-information/address-details.module';
import { TranslateModule } from '@ngx-translate/core';
import { OrderPositionViewModule } from '@shared/components/order-position-view/order-position-view.module';
import { MatCardModule } from '@angular/material/card';
import {
  FreightBaseDataViewModule
} from '@base/src/app/pages/transport-document/transport-document-check-page/components/transport-document-check/components/freight-base-data-view/freight-base-data-view.module';

@NgModule({
  declarations: [TransportDocumentCheckComponent],
  exports: [TransportDocumentCheckComponent],
  imports: [
    CommonModule,
    TransportDocumentCheckRoutingModule,
    MatIconModule,
    CriterionCheckModule,
    MatExpansionModule,
    MatListModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    AddressDetailsModule,
    TranslateModule,
    OrderPositionViewModule,
    MatCardModule,
    FreightBaseDataViewModule,
  ],
})
export class TransportDocumentCheckModule {}
