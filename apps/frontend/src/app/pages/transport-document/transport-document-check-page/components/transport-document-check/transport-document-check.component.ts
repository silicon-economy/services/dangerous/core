/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, NgZone } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  CarrierCheckCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/carrier-check-criteria';
import {
  AcceptanceCriterion
} from '@base/src/app/pages/transport-document/transport-document-check-page/components/transport-document-check/types/acceptance-criterion.type';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-transport-document-check[_transportDocument]',
  templateUrl: './transport-document-check.component.html',
})
export class TransportDocumentCheckComponent {
  public acceptanceCriteria: CarrierCheckCriteria;
  protected readonly AcceptanceCriterion = AcceptanceCriterion;

  constructor(
    private readonly router: Router,
    private readonly transportDocumentHttpService: TransportDocumentHttpService,
    private readonly feedbackDialogService: FeedbackDialogService,
    private readonly notificationService: NotificationService,
    private readonly translateService: TranslateService,
    private readonly ngZone: NgZone
  ) {
    this.acceptanceCriteria = {
      comment: '',
      acceptAdditionalInformation: false,
      acceptConsignees: [],
      acceptFreight: false,
      acceptConsignor: false,
      acceptTransportationInstructions: false,
    };
  }

  @Input() private _transportDocument!: SaveGoodsDataDto;

  get transportDocument(): SaveGoodsDataDto {
    return this._transportDocument;
  }

  private _comment = '';

  get comment(): string {
    return this._comment;
  }

  set comment(value: string) {
    this._comment = value;
  }

  /**
   * Returns if the given transport document would be accepted
   *
   * @returns true if all acceptance criteria are set to true
   */
  get documentIsAccepted(): boolean {
    return (
      this.transportDocument != null &&
      this.acceptanceCriteria != null &&
      this.acceptanceCriteria.acceptConsignor &&
      this.acceptanceCriteria.acceptConsignees.every((consignee) => consignee === true) &&
      this.acceptanceCriteria.acceptFreight &&
      this.acceptanceCriteria.acceptTransportationInstructions &&
      this.acceptanceCriteria.acceptAdditionalInformation
    );
  }

  /**
   * Sets the acceptance status of the given acceptance criterion to either true or false
   *
   * @param acceptanceCriterion  - The acceptance criterion which is to be changed
   * @param accepted - The new status of the acceptance criterion
   * @param index - The index of the consignee in the case that the acceptance criterion is of type `acceptConsignees`
   */
  public setAcceptCriterion(acceptanceCriterion: AcceptanceCriterion, accepted: boolean, index = -1): void {
    if (acceptanceCriterion !== AcceptanceCriterion.acceptConsignees) {
      this.acceptanceCriteria[acceptanceCriterion] = accepted;
    } else {
      this.acceptanceCriteria.acceptConsignees[index] = accepted;
    }
  }

  /**
   * Updates the acceptance criteria of the given transport document
   */
  public updateAcceptanceCriteria(): void {
    // Set comment if document is not accepted
    this.acceptanceCriteria.comment = this.documentIsAccepted ? '' : this.comment;
    this.feedbackDialogService.showDialog();

    this.transportDocumentHttpService
      .transportDocumentCarrierCheck(this.transportDocument.id, this.acceptanceCriteria)
      .subscribe(() => {
        this.feedbackDialogService.handleSuccess();
        this.translateService
          .get('dialogs.checkTransportDocument.notification.success')
          .subscribe((translation: string) => {
            this.notificationService.success(translation);
            this.ngZone.run(() => {
              void this.router.navigate(['/documents']);
            });
          });
      });
  }
}
