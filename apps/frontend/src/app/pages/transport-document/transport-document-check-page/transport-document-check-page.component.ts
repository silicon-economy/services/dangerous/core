/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';

@Component({
  selector: 'app-transport-document-check-page',
  templateUrl: './transport-document-check-page.component.html',
})
export class TransportDocumentCheckPageComponent implements OnInit {
  constructor(private activatedRoute: ActivatedRoute) {}

  private _transportDocument: SaveGoodsDataDto;

  get transportDocument(): SaveGoodsDataDto {
    return this._transportDocument;
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: Data) => {
      this._transportDocument = <SaveGoodsDataDto>data._transportDocument;
    });
  }
}
