/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransportDocumentCheckPageRoutingModule } from './transport-document-check-page-routing.module';
import { TransportDocumentCheckPageComponent } from './transport-document-check-page.component';
import {
  TransportDocumentCheckModule
} from '@base/src/app/pages/transport-document/transport-document-check-page/components/transport-document-check/transport-document-check.module';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [TransportDocumentCheckPageComponent],
  imports: [
    CommonModule,
    TransportDocumentCheckPageRoutingModule,
    TransportDocumentCheckModule,
    TitleBarModule,
    TranslateModule,
  ],
})
export class TransportDocumentCheckPageModule {}
