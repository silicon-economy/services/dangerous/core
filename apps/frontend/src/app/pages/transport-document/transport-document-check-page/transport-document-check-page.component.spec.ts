/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportDocumentCheckPageComponent } from './transport-document-check-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  TransportDocumentCheckModule
} from '@base/src/app/pages/transport-document/transport-document-check-page/components/transport-document-check/transport-document-check.module';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('TransportDocumentCheckPageComponent', () => {
  let component: TransportDocumentCheckPageComponent;
  let fixture: ComponentFixture<TransportDocumentCheckPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        TitleBarModule,
        TranslateModule.forRoot(),
        TransportDocumentCheckModule,
        FeedbackDialogServiceModule,
        NotificationServiceModule,
        NoopAnimationsModule,
      ],
      declarations: [TransportDocumentCheckPageComponent],
      providers: [TransportDocumentHttpService],
    });
    fixture = TestBed.createComponent(TransportDocumentCheckPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
