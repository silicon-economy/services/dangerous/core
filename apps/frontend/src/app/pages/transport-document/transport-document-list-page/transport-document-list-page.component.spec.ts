/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportDocumentListPageComponent } from './transport-document-list-page.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { UserServiceModule } from '@core/services/user-service/user-service.module';
import { ScanDialogServiceModule } from '@shared/services/scan-dialog-service/scan-dialog-service.module';
import { MatDialogModule } from '@angular/material/dialog';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';

describe('TransportDocumentListPageComponent', () => {
  let component: TransportDocumentListPageComponent;
  let fixture: ComponentFixture<TransportDocumentListPageComponent>;

  const route = {
    data: of({
      _transportDocuments: [
        new SaveGoodsDataDto('123', undefined, undefined, undefined, undefined, undefined, undefined, undefined),
      ],
    }),
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TransportDocumentListPageComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        UserServiceModule,
        ScanDialogServiceModule,
        MatDialogModule,
        FeedbackDialogServiceModule,
        NotificationServiceModule,
      ],
      providers: [
        TransportDocumentHttpService,
        {
          provide: ActivatedRoute,
          useValue: route,
        },
        TransportDocumentService,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(TransportDocumentListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
