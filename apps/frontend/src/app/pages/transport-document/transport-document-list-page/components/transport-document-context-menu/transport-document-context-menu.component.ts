/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import { PermissionService } from '@shared/services/permission-service/permission.service';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';

@Component({
  selector: 'app-transport-document-context-menu[document]',
  templateUrl: './transport-document-context-menu.component.html',
})
export class TransportDocumentContextMenuComponent {
  @Input() document!: SaveGoodsDataDto;

  constructor(
    public transportDocumentHttpService: TransportDocumentHttpService,
    readonly permissionService: PermissionService,
    readonly transportDocumentService: TransportDocumentService
  ) {}
}
