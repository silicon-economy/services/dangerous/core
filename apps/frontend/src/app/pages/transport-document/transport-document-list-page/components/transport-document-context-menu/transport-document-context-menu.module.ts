/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  TransportDocumentContextMenuComponent
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-context-menu/transport-document-context-menu.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { TranslateModule } from '@ngx-translate/core';
import { ScanDialogServiceModule } from '@shared/services/scan-dialog-service/scan-dialog-service.module';
import {
  CarrierCheckFreightDialogModule
} from '@base/src/app/dialogs/carrier-check-freight-dialog/carrier-check-freight-dialog.module';
import {
  ConsigneeCheckOrderDialogModule
} from '@base/src/app/dialogs/consignee-check-order-dialog/consignee-check-order-dialog.module';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';

@NgModule({
  declarations: [TransportDocumentContextMenuComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    TranslateModule,
    ScanDialogServiceModule,
    CarrierCheckFreightDialogModule,
    ConsigneeCheckOrderDialogModule,
  ],
  exports: [TransportDocumentContextMenuComponent],
  providers: [TransportDocumentHttpService, FeedbackDialogService],
})
export class TransportDocumentContextMenuModule {}
