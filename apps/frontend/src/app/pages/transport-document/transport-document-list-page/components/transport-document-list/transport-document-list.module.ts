/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransportDocumentListComponent } from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list.component';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { TransportDocumentContextMenuModule } from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-context-menu/transport-document-context-menu.module';
import { MatMenuModule } from '@angular/material/menu';
import { ConsigneeListDialogModule } from '@base/src/app/dialogs/consignee-list-dialog/consignee-list-dialog.module';
import { MatDividerModule } from '@angular/material/divider';
import { CompareModule } from '@shared/services/compare-service/compare.module';
import { TransportDocumentListFilterModule } from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-filter-service/transport-document-list-filter.module';
import { ListSortModule } from '@shared/services/list-sort-service/list-sort.module';
import { CommentDialogModule } from '@base/src/app/dialogs/comment-dialog/comment-dialog.module';
import { MatBadgeModule } from '@angular/material/badge';
import { TranslateModule } from '@ngx-translate/core';
import { TransportDocumentListNextActionServiceModule } from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-next-action-service/transport-document-list-next-action-service.module';
import { StatusModule } from '@base/src/app/layouts/status/status.module';
import { RouterLink } from '@angular/router';
import { LocalizedDatePipe } from '@core/pipes/localized-date.pipe';

@NgModule({
  declarations: [TransportDocumentListComponent],
  imports: [
    CommonModule,
    MatSortModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    TransportDocumentContextMenuModule,
    MatMenuModule,
    ConsigneeListDialogModule,
    MatDividerModule,
    CompareModule,
    TransportDocumentListFilterModule,
    ListSortModule,
    CommentDialogModule,
    MatBadgeModule,
    TranslateModule,
    TransportDocumentListNextActionServiceModule,
    StatusModule,
    RouterLink,
    LocalizedDatePipe,
  ],
  exports: [TransportDocumentListComponent],
})
export class TransportDocumentListModule {}
