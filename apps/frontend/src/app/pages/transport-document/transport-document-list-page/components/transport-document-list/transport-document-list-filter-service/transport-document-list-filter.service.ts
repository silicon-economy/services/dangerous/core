/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { TransportDocumentStatus } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/status.enum';
import {
  TransportDocumentListNextActionService
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-next-action-service/transport-document-list-next-action.service';

@Injectable()
export class TransportDocumentListFilterService {
  constructor(private readonly transportDocumentListNextActionService: TransportDocumentListNextActionService) {}

  /**
   * Applies a filter to the data based on the provided filter string.
   *
   * @param filterString - The string to filter the data by.
   * @param entries - The entries to filter.
   * @returns An array of SaveGoodsDataDto objects that match the filter.
   */
  public applyFilterByString(filterString: string, entries: SaveGoodsDataDto[]): SaveGoodsDataDto[] {
    const lowercaseFilterString = filterString.toLowerCase();

    return entries.filter((saveGoodsDataDto: SaveGoodsDataDto) => {
      return (
        this.filterById(saveGoodsDataDto, lowercaseFilterString) ||
        this.filterByConsignorName(saveGoodsDataDto, lowercaseFilterString) ||
        this.filterByCarrierName(saveGoodsDataDto, lowercaseFilterString) ||
        this.filterByConsigneeNames(saveGoodsDataDto, lowercaseFilterString)
      );
    });
  }

  /**
   * Filter documents by the selected status.
   *
   * @param document - A transport document.
   * @param allStatus - All existing transport document statuses.
   * @param displayStatus - Statuses that get displayed in the current list view.
   * @returns Boolean value if transport documents with this status are visible
   */
  public filterByStatus(
    document: SaveGoodsDataDto,
    allStatus: TransportDocumentStatus[],
    displayStatus: Record<TransportDocumentStatus, boolean>
  ): boolean {
    for (const status of allStatus) {
      if (displayStatus[status] === false && document.status === status) {
        return false;
      }
    }
    return true;
  }

  /**
   * Applies the required action filter to the given document.
   *
   * @param document - The document to apply the required action filter to.
   * @param allRequiredActionFilters - An array of all available required action filters.
   * @param requiredActionFilters - An object representing the required action filters and their enabled status.
   * @param myActions - The action to filter for.
   * @returns A boolean indicating whether the transport documents having the respective status should be displayed.
   */
  public applyRequiredActionFilter(
    document: SaveGoodsDataDto,
    allRequiredActionFilters: string[],
    requiredActionFilters: Record<string, boolean>,
    myActions: string
  ): boolean {
    for (const requiredActionFilter of allRequiredActionFilters) {
      if (requiredActionFilters[requiredActionFilter] === true && requiredActionFilter === myActions) {
        return this.isRequiredAction(document.status);
      }
    }
    return true;
  }

  /**
   * Check if status contains 'wait' in string and return true
   *
   * @param status - The status that has to be checked
   * @returns True, if status contains 'wait' in string
   */
  public isRequiredAction(status: TransportDocumentStatus): boolean {
    return this.transportDocumentListNextActionService.getNextActionAsString(status).isRequired;
  }

  /**
   * Checks the visibility of a specific status in relation to a transport document status.
   *
   * @param transportDocumentStatus - The current transport document status.
   * @param statusToCheckVisibilityFor - The status to check visibility for.
   * @returns A boolean indicating whether the status is required and matches the status to check visibility for.
   */
  public checkVisibility(
    transportDocumentStatus: TransportDocumentStatus,
    statusToCheckVisibilityFor: TransportDocumentStatus
  ): boolean {
    return this.isRequiredAction(transportDocumentStatus) && transportDocumentStatus === statusToCheckVisibilityFor;
  }

  /**
   * Filters the data by the ID property of the SaveGoodsDataDto object.
   *
   * @param saveGoodsDataDto - The SaveGoodsDataDto object to filter.
   * @param filterString - The string to filter by.
   * @returns boolean - True if the filter matches, false otherwise.
   */
  private filterById(saveGoodsDataDto: SaveGoodsDataDto, filterString: string): boolean {
    return saveGoodsDataDto.id.toLowerCase().includes(filterString);
  }

  /**
   * Filters the data by the consignor name property of the SaveGoodsDataDto object.
   *
   * @param saveGoodsDataDto - The SaveGoodsDataDto object to filter.
   * @param filterString - The string to filter by.
   * @returns boolean - True if the filter matches, false otherwise.
   */
  private filterByConsignorName(saveGoodsDataDto: SaveGoodsDataDto, filterString: string): boolean {
    return saveGoodsDataDto.consignor.name.toLowerCase().includes(filterString);
  }

  /**
   * Filters the data by the carrier name property of the SaveGoodsDataDto object.
   *
   * @param saveGoodsDataDto - The SaveGoodsDataDto object to filter.
   * @param filterString - The string to filter by.
   * @returns boolean - True if the filter matches, false otherwise.
   */
  private filterByCarrierName(saveGoodsDataDto: SaveGoodsDataDto, filterString: string): boolean {
    return saveGoodsDataDto.carrier.name.toLowerCase().includes(filterString);
  }

  /**
   * Filters the data by the consignee names property of the SaveGoodsDataDto object.
   *
   * @param saveGoodsDataDto - The SaveGoodsDataDto object to filter.
   * @param filterString - The string to filter by.
   * @returns boolean - True if the filter matches, false otherwise.
   */
  private filterByConsigneeNames(saveGoodsDataDto: SaveGoodsDataDto, filterString: string): boolean {
    return saveGoodsDataDto.freight.orders.some((order: OrderWithId) => {
      return order.consignee.name.toLowerCase().includes(filterString);
    });
  }
}
