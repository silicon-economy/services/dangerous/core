/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { TransportDocumentListNextActionService } from './transport-document-list-next-action.service';
import {
  TransportDocumentListNextActionServiceModule
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-next-action-service/transport-document-list-next-action-service.module';
import { UserServiceModule } from '@core/services/user-service/user-service.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('TransportDocumentListNextActionService', () => {
  let service: TransportDocumentListNextActionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TransportDocumentListNextActionServiceModule,
        UserServiceModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
      ],
    });
    service = TestBed.inject(TransportDocumentListNextActionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
