/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { TransportDocumentListFilterService } from './transport-document-list-filter.service';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  TransportDocumentListNextActionService
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-next-action-service/transport-document-list-next-action.service';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import {
  mockTransportDocumentTransport
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';

describe('ListFilterService', () => {
  let service: TransportDocumentListFilterService;
  const mockTransportDocument: SaveGoodsDataDto = mockTransportDocumentTransport;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [
        TransportDocumentListFilterService,
        TransportDocumentListNextActionService,
        UserLoginService,
        UserAuthenticationService,
      ],
    });
    service = TestBed.inject(TransportDocumentListFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should apply a filter to the data based on the provided filter string', () => {
    const mockTransportDocument2: SaveGoodsDataDto = structuredClone(mockTransportDocument);
    mockTransportDocument2.id = 'BP000002-20210819';
    const mockTransportDocuments: SaveGoodsDataDto[] = [mockTransportDocument, mockTransportDocument2];

    const filteredTransportDocuments: SaveGoodsDataDto[] = service.applyFilterByString('001', mockTransportDocuments);
    expect(filteredTransportDocuments).toEqual([mockTransportDocuments[0]]);
  });
});
