/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  TransportDocumentListFilterService
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-filter-service/transport-document-list-filter.service';
import {
  TransportDocumentListNextActionServiceModule
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-next-action-service/transport-document-list-next-action-service.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, TransportDocumentListNextActionServiceModule],
  providers: [TransportDocumentListFilterService],
})
export class TransportDocumentListFilterModule {}
