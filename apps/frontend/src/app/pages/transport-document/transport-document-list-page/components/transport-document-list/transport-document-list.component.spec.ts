/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportDocumentListComponent } from './transport-document-list.component';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { TransportDocumentListFilterService } from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-filter-service/transport-document-list-filter.service';
import { TransportDocumentListNextActionService } from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-next-action-service/transport-document-list-next-action.service';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ListSortService } from '@shared/services/list-sort-service/list-sort.service';
import { CompareService } from '@shared/services/compare-service/compare.service';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatDividerModule } from '@angular/material/divider';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { TransportDocumentContextMenuModule } from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-context-menu/transport-document-context-menu.module';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { MatBadgeModule } from '@angular/material/badge';
import {
  mockTransportDocumentAccepted,
  mockTransportDocumentCreated,
  mockTransportDocumentReleased,
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import { StatusModule } from '@base/src/app/layouts/status/status.module';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import { CarrierDialogComponent } from '@base/src/app/dialogs/carrier-dialog/carrier-dialog.component';
import { mockCarrier } from '@core/api-interfaces/lib/mocks/carrier/carrier.mock';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

describe('TransportDocumentListComponent', () => {
  let component: TransportDocumentListComponent;
  let fixture: ComponentFixture<TransportDocumentListComponent>;
  let matDialog: MatDialog;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        MatTableModule,
        MatSortModule,
        MatIconModule,
        MatMenuModule,
        NoopAnimationsModule,
        MatDividerModule,
        TransportDocumentContextMenuModule,
        RouterTestingModule,
        MatBadgeModule,
        StatusModule,
        FeedbackDialogServiceModule,
        NotificationServiceModule,
      ],
      declarations: [TransportDocumentListComponent, CarrierDialogComponent],
      providers: [
        TransportDocumentListFilterService,
        TransportDocumentListNextActionService,
        TransportDocumentService,
        {
          provide: UserLoginService,
          useValue: {
            userRoles: of([UserRole.carrier]),
          },
        },
        UserAuthenticationService,
        ListSortService,
        CompareService,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(TransportDocumentListComponent);
    matDialog = TestBed.inject(MatDialog);
    component = fixture.componentInstance;

    const mockTransportDocument1: SaveGoodsDataDto = structuredClone(mockTransportDocumentCreated);
    const mockTransportDocument2: SaveGoodsDataDto = structuredClone(mockTransportDocumentAccepted);
    const mockTransportDocument3: SaveGoodsDataDto = structuredClone(mockTransportDocumentReleased);

    mockTransportDocument2.id = 'BP000002-20210819';
    mockTransportDocument3.id = 'BP000003-20210819';

    component.entries = [mockTransportDocument1, mockTransportDocument2, mockTransportDocument3];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle all status', () => {
    component.displayStatus = {
      accepted: true,
      created: true,
      not_accepted: true,
      released: true,
      transport: false,
      transport_denied: true,
      vehicle_accepted: true,
      vehicle_denied: true,
      transport_completed: true,
      unknown: true,
    };
    component.onToggleAllStatus();

    expect(component.displayStatus).toStrictEqual({
      accepted: true,
      created: true,
      not_accepted: true,
      released: true,
      transport: true,
      transport_denied: true,
      vehicle_accepted: true,
      vehicle_denied: true,
      transport_completed: true,
      unknown: true,
    });
  });

  it('should open CarrierDialogComponent with given carrier data', () => {
    const openSpy = jest.spyOn(matDialog, 'open');
    component.onDisplayCarrier(mockCarrier);

    expect(openSpy).toHaveBeenCalledWith(CarrierDialogComponent, { data: mockCarrier });
  });
});
