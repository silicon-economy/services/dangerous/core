/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { TransportDocumentStatus } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/status.enum';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { TranslateService } from '@ngx-translate/core';
import {
  ActionByRole
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-next-action-service/interfaces/action-by-role.interface';

@Injectable()
export class TransportDocumentListNextActionService {
  private userRoles: UserRole[];

  constructor(
    private readonly userService: UserLoginService,
    private readonly translateService: TranslateService
  ) {
    this.userService.userRoles.subscribe((userRoles: UserRole[]) => {
      this.userRoles = userRoles;
    });
  }

  get nextActionByRole(): Map<UserRole, Map<TransportDocumentStatus, ActionByRole>> {
    return new Map<UserRole, Map<TransportDocumentStatus, ActionByRole>>([
      [
        UserRole.consignor,
        new Map<TransportDocumentStatus, ActionByRole>([
          [
            TransportDocumentStatus.created,
            {
              action: this.translateService.instant(
                'transportDocumentList.columns.requiredAction.consignor.created'
              ) as string,
              isRequired: false,
            },
          ],
          [
            TransportDocumentStatus.accepted,
            {
              action: this.translateService.instant(
                'transportDocumentList.columns.requiredAction.consignor.accepted'
              ) as string,
              isRequired: true,
            },
          ],
          [
            TransportDocumentStatus.released,
            {
              action: this.translateService.instant(
                'transportDocumentList.columns.requiredAction.consignor.released'
              ) as string,
              isRequired: true,
            },
          ],
          [
            TransportDocumentStatus.vehicle_accepted,
            {
              action: this.translateService.instant(
                'transportDocumentList.columns.requiredAction.consignor.vehicle_accepted'
              ) as string,
              isRequired: false,
            },
          ],
          [
            TransportDocumentStatus.transport,
            {
              action: this.translateService.instant(
                'transportDocumentList.columns.requiredAction.consignor.transport'
              ) as string,
              isRequired: false,
            },
          ],
        ]),
      ],
      [
        UserRole.carrier,
        new Map<TransportDocumentStatus, ActionByRole>([
          [
            TransportDocumentStatus.created,
            {
              action: this.translateService.instant(
                'transportDocumentList.columns.requiredAction.carrier.created'
              ) as string,
              isRequired: true,
            },
          ],
          [
            TransportDocumentStatus.accepted,
            {
              action: this.translateService.instant(
                'transportDocumentList.columns.requiredAction.carrier.accepted'
              ) as string,
              isRequired: false,
            },
          ],
          [
            TransportDocumentStatus.released,
            {
              action: this.translateService.instant(
                'transportDocumentList.columns.requiredAction.carrier.released'
              ) as string,
              isRequired: false,
            },
          ],
          [
            TransportDocumentStatus.vehicle_accepted,
            {
              action: this.translateService.instant(
                'transportDocumentList.columns.requiredAction.carrier.vehicle_accepted'
              ) as string,
              isRequired: true,
            },
          ],
          [
            TransportDocumentStatus.transport,
            {
              action: this.translateService.instant(
                'transportDocumentList.columns.requiredAction.carrier.transport'
              ) as string,
              isRequired: true,
            },
          ],
        ]),
      ],
      [
        UserRole.consignee,
        new Map<TransportDocumentStatus, ActionByRole>([
          [TransportDocumentStatus.created, { action: '', isRequired: false }],
          [TransportDocumentStatus.accepted, { action: '', isRequired: false }],
          [TransportDocumentStatus.released, { action: '', isRequired: false }],
          [TransportDocumentStatus.vehicle_accepted, { action: '', isRequired: false }],
          [TransportDocumentStatus.transport, { action: '', isRequired: false }],
        ]),
      ],
    ]);
  }

  /**
   * Retrieves the next action and its required status for a given transport document status.
   *
   * @param status - The transport document status.
   * @returns An object containing the next action and its required status.
   */
  public getNextActionAsString(status: TransportDocumentStatus): ActionByRole {
    return (
      this.nextActionByRole.get(this.userRoles[0]).get(status) ??
      ({
        action: '',
        isRequired: false,
      } satisfies ActionByRole)
    );
  }
}
