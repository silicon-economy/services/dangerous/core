/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  TransportDocumentContextMenuComponent
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-context-menu/transport-document-context-menu.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { RouterTestingModule } from '@angular/router/testing';
import { UserServiceModule } from '@core/services/user-service/user-service.module';
import {
  mockTransportDocumentTransport
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { TranslateModule } from '@ngx-translate/core';
import { FeedbackDialogServiceModule } from '@shared/services/feedback-dialog-service/feedback-dialog-service.module';
import { MatDialogModule } from '@angular/material/dialog';
import { NotificationServiceModule } from '@shared/services/notification-service/notification-service.module';
import { ScanDialogServiceModule } from '@shared/services/scan-dialog-service/scan-dialog-service.module';

describe('DocumentContextMenuComponent', () => {
  let component: TransportDocumentContextMenuComponent;
  let fixture: ComponentFixture<TransportDocumentContextMenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatIconModule,
        MatMenuModule,
        RouterTestingModule,
        UserServiceModule,
        TranslateModule.forRoot(),
        FeedbackDialogServiceModule,
        MatDialogModule,
        NotificationServiceModule,
        ScanDialogServiceModule,
      ],
      declarations: [TransportDocumentContextMenuComponent],
      providers: [TransportDocumentService, TransportDocumentHttpService],
    });
    fixture = TestBed.createComponent(TransportDocumentContextMenuComponent);
    component = fixture.componentInstance;
    component.document = mockTransportDocumentTransport;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
