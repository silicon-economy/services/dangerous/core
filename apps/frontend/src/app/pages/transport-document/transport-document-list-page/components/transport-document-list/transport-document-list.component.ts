/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnInit } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { TransportDocumentStatus } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/status.enum';
import { MatDialog } from '@angular/material/dialog';
import {
  ConsigneeListDialogComponent
} from '@base/src/app/dialogs/consignee-list-dialog/consignee-list-dialog.component';
import { Sort } from '@angular/material/sort';
import {
  TransportDocumentListFilterService
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-filter-service/transport-document-list-filter.service';
import { CommentDialogComponent } from '@base/src/app/dialogs/comment-dialog/comment-dialog.component';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import {
  TransportDocumentListNextActionService
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-next-action-service/transport-document-list-next-action.service';
import { ListSortService } from '@shared/services/list-sort-service/list-sort.service';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import { Carrier } from "@core/api-interfaces/lib/dtos/frontend";
import { CarrierDialogComponent } from "@base/src/app/dialogs/carrier-dialog/carrier-dialog.component";

@Component({
  selector: 'app-transport-document-list[entries]',
  templateUrl: './transport-document-list.component.html',
  styleUrls: ['./transport-document-list.component.scss'],
})
// TODO-DM: This file looks really cluttered! Outsource single methods to external service if possible.
export class TransportDocumentListComponent implements OnInit {
  @Input() entries: SaveGoodsDataDto[] = [];
  columnsToDisplay: string[] = [
    'menu',
    'id',
    'consignee',
    'carrier',
    'created_date',
    'last_update',
    'status',
    'required_action',
    'comment',
  ];
  public allStatus: TransportDocumentStatus[] = [
    TransportDocumentStatus.created,
    TransportDocumentStatus.accepted,
    TransportDocumentStatus.not_accepted,
    TransportDocumentStatus.released,
    TransportDocumentStatus.transport,
    TransportDocumentStatus.transport_denied,
    TransportDocumentStatus.vehicle_accepted,
    TransportDocumentStatus.vehicle_denied,
    TransportDocumentStatus.transport_completed,
    // TransportDocumentStatus.unknown,
    // 'carrier_confirmed',
  ];
  public displayStatus: Record<TransportDocumentStatus, boolean> = {
    accepted: true,
    created: true,
    not_accepted: true,
    released: true,
    transport: true,
    transport_denied: true,
    vehicle_accepted: true,
    vehicle_denied: true,
    transport_completed: true,
    unknown: true,
    // carrier_confirmed: true,
  };
  public lastSort: Sort = {
    active: 'last_update',
    direction: 'desc',
  };
  // TODO-DM: Refactor string
  public myActions = 'Meine Aktionen';
  public allRequiredActionFilters: string[] = [this.myActions];
  public requiredActionFilters: Record<string, boolean> = {
    // TODO-DM: Refactor string
    'Meine Aktionen': false,
  };
  userRoles: UserRole[] = [];
  protected readonly TransportDocumentStatus = TransportDocumentStatus;
  protected sortedData: SaveGoodsDataDto[] = [];
  private dataFilteredByString: SaveGoodsDataDto[] = [];

  constructor(
    private readonly matDialog: MatDialog,
    readonly transportDocumentListFilterService: TransportDocumentListFilterService,
    private readonly userService: UserLoginService,
    readonly transportDocumentListNextActionService: TransportDocumentListNextActionService,
    private readonly listSortService: ListSortService,
    readonly transportDocumentService: TransportDocumentService
  ) {}

  /**
   * Return the number filtered by active statuses.
   *
   * @returns Active status number
   */
  get numActiveStatus(): number {
    return this.allStatus
      .map((status: TransportDocumentStatus) => {
        return this.displayStatus[status];
      })
      .filter((isStatusSelected: boolean) => isStatusSelected).length;
  }

  ngOnInit(): void {
    this.sortedData = this.dataFilteredByString = this.entries;
    this.userService.userRoles.subscribe((userRoles: UserRole[]) => {
      this.userRoles = userRoles;
    });
  }

  /**
   * Trigger to display the consignee for a given document.
   *
   * @param saveGoodsDataDto - The document to which the consignees will be displayed.
   */
  public onDisplayConsignees(saveGoodsDataDto: SaveGoodsDataDto): void {
    this.matDialog.open(ConsigneeListDialogComponent, {
      data: this.transportDocumentService.getConsignees(saveGoodsDataDto),
    });
  }

  /**
   * Trigger to display the carrier for a given document.
   *
   * @param carrier - Carrier will be displayed.
   */
  public onDisplayCarrier(carrier: Carrier): void {
    this.matDialog.open(CarrierDialogComponent, {
      data: carrier
    });
  }

  /**
   * Trigger to display the consignee for a given document.
   *
   * @param document - The document to which the consignees will be displayed.
   */
  public onDisplayComment(document: SaveGoodsDataDto): void {
    this.matDialog.open(CommentDialogComponent, {
      data: {
        document,
      },
    });
  }

  /**
   * Toggle the display status of the given status.
   *
   * @param transportDocumentStatus - The status to be toggled.
   */
  public onToggleDisplayStatus(transportDocumentStatus: TransportDocumentStatus): void {
    this.displayStatus[transportDocumentStatus] = !this.displayStatus[transportDocumentStatus];
    this.sortData(this.lastSort);
  }

  /**
   * Toggle a required action filter.
   *
   * @param actionFilter - The status to be toggled.
   */
  public onToggleRequiredActionFilter(actionFilter: string): void {
    this.requiredActionFilters[actionFilter] = !this.requiredActionFilters[actionFilter];
    this.sortData(this.lastSort);
  }

  /**
   * Applies a filter to the data based on the provided filter string.
   *
   * @param filterString - The string to filter the data by.
   */
  public filterByString(filterString: string): void {
    this.dataFilteredByString = this.transportDocumentListFilterService.applyFilterByString(filterString, this.entries);
  }

  /**
   * Sort the Data via MatSort.
   *
   * @param sortState - The sort object containing information like active columns and direction.
   */
  sortData(sortState?: Sort): void {
    if (sortState) {
      this.lastSort = sortState;
    }

    this.sortedData = this.listSortService.sortTransportDocuments(
      this.lastSort,
      this.dataFilteredByString,
      this.allStatus,
      this.displayStatus,
      this.allRequiredActionFilters,
      this.requiredActionFilters,
      this.myActions
    );
  }

  /**
   * Toggle all statuses.
   * If all statuses are actively filtered, deactivate all filters, otherwise activate all filters.
   */
  public onToggleAllStatus(): void {
    const allActive: boolean = this.numActiveStatus === this.allStatus.length;
    this.allStatus.forEach((status: TransportDocumentStatus) => {
      this.displayStatus[status] = !allActive;
    });
    this.sortData(this.lastSort);
  }
}
