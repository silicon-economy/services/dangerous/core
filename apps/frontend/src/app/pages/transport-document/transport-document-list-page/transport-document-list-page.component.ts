/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import {
  TransportDocumentListComponent
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list.component';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import {
  ShippingLabelQrCodeScannerDialogComponent
} from '@base/src/app/dialogs/shipping-label-qr-code-scanner-dialog/shipping-label-qr-code-scanner-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { ScanDialogService } from '@shared/services/scan-dialog-service/scan-dialog.service';
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: 'app-transport-document-list-page',
  templateUrl: './transport-document-list-page.component.html',
})
export class TransportDocumentListPageComponent implements OnInit {
  filterString = '';
  @ViewChild('listComponent') listComponent: TransportDocumentListComponent;
  currentUserRoles: UserRole[];
  protected readonly UserRole = UserRole;

  constructor(
    private activatedRoute: ActivatedRoute,
    protected readonly transportDocumentService: TransportDocumentService,
    private readonly translateService: TranslateService,
    private readonly scanDialogService: ScanDialogService,
    private readonly userService: UserLoginService,
    private matDialog: MatDialog,
    private router: Router
  ) {
    this.userService.userRoles.subscribe((currentUserRoles: UserRole[]) => {
      this.currentUserRoles = currentUserRoles;
    });
  }

  private _transportDocuments: MatTableDataSource<SaveGoodsDataDto>;

  get transportDocuments(): MatTableDataSource<SaveGoodsDataDto> {
    return this._transportDocuments;
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: { _transportDocuments: SaveGoodsDataDto[] }) => {
      this._transportDocuments = new MatTableDataSource(data._transportDocuments.slice());
    });
  }

  /**
   * Applies a filter to the transport documents based on the input event.
   *
   * @param event - The input event that triggered the filter.
   */
  applyFilter(event: Event): void {
    const filterString = (event.target as HTMLInputElement).value;
    this.listComponent.filterByString(filterString);
    this.listComponent.sortData();
  }

  /**
   * Show a dialog window with ScanQrCodePageComponent
   */
  public openScanDialogVisualInspectionCarrier(): void {
    this.translateService.get('dialogs.visualInspectionCarrier.title').subscribe((translation: string) => {
      this.scanDialogService.createDialog(ShippingLabelQrCodeScannerDialogComponent, [
        { attributeName: 'titleHeader', attribute: translation },
      ]);
    });
  }
}
