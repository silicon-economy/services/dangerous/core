/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  TransportDocumentListPageComponent
} from '@base/src/app/pages/transport-document/transport-document-list-page/transport-document-list-page.component';
import {
  transportDocumentListResolver
} from '@core/resolvers/transport-document-list-resolver/transport-document-list.resolver';

const routes: Routes = [
  {
    path: '',
    resolve: { _transportDocuments: transportDocumentListResolver },
    component: TransportDocumentListPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransportDocumentListPageRoutingModule {}
