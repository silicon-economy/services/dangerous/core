/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { MatTableModule } from '@angular/material/table';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { MatCardModule } from '@angular/material/card';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule } from '@angular/material/button';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { TransportDocumentListPageComponent } from '@base/src/app/pages/transport-document/transport-document-list-page/transport-document-list-page.component';
import { TransportDocumentListPageRoutingModule } from '@base/src/app/pages/transport-document/transport-document-list-page/transport-document-list-page-routing.module';
import { TransportDocumentListModule } from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list.module';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { HandoverQrCodeScannerDialogModule } from '@base/src/app/dialogs/handover-qr-code-scanner-dialog/handover-qr-code-scanner-dialog.module';
import { MatListModule } from '@angular/material/list';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { CarrierViewModule } from '@base/src/app/layouts/carrier-view/carrier-view.module';
import { CarrierDialogComponent } from '@base/src/app/dialogs/carrier-dialog/carrier-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { AddressDetailsModule } from '@shared/components/consignee-information/address-details.module';

@NgModule({
  declarations: [TransportDocumentListPageComponent, CarrierDialogComponent],
  exports: [TransportDocumentListPageComponent],
  imports: [
    CommonModule,
    TransportDocumentListPageRoutingModule,
    MatTableModule,
    TitleBarModule,
    MatCardModule,
    TranslateModule,
    MatButtonModule,
    MatIconModule,
    MatTabsModule,
    TransportDocumentListModule,
    MatInputModule,
    FormsModule,
    HandoverQrCodeScannerDialogModule,
    MatListModule,
    CarrierViewModule,
    MatDialogModule,
    AddressDetailsModule,
  ],
  providers: [TransportDocumentHttpService, TransportDocumentService, FeedbackDialogService, DatePipe],
})
export class TransportDocumentListPageModule {}
