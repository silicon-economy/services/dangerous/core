/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  PrivacyPolicyPageComponent
} from '@base/src/app/pages/legal-information/privacy-policy-page/privacy-policy-page.component';

const routes: Routes = [
  {
    path: '',
    component: PrivacyPolicyPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivacyPolicyPageRoutingModule {}
