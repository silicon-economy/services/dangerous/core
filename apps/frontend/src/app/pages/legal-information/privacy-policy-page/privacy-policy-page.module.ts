/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivacyPolicyPageComponent } from './privacy-policy-page.component';
import {
  PrivacyPolicyPageRoutingModule
} from '@base/src/app/pages/legal-information/privacy-policy-page/privacy-policy-page-routing.module';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { MatCardModule } from '@angular/material/card';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [PrivacyPolicyPageComponent],
  imports: [CommonModule, PrivacyPolicyPageRoutingModule, TitleBarModule, MatCardModule, TranslateModule],
})
export class PrivacyPolicyPageModule {}
