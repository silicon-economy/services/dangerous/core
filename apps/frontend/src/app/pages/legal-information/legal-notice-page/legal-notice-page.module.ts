/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LegalNoticePageComponent } from './legal-notice-page.component';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import {
  LegalNoticePageRoutingModule
} from '@base/src/app/pages/legal-information/legal-notice-page/legal-notice-page-routing.module';
import { MatCardModule } from '@angular/material/card';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [LegalNoticePageComponent],
  imports: [CommonModule, TitleBarModule, LegalNoticePageRoutingModule, MatCardModule, TranslateModule],
})
export class LegalNoticePageModule {}
