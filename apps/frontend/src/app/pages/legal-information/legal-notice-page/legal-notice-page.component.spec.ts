/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalNoticePageComponent } from './legal-notice-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';

describe('LegalNoticePageComponent', () => {
  let component: LegalNoticePageComponent;
  let fixture: ComponentFixture<LegalNoticePageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), MatCardModule, TitleBarModule],
      declarations: [LegalNoticePageComponent],
    });
    fixture = TestBed.createComponent(LegalNoticePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
