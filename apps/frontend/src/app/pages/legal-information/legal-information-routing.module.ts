/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'privacy-policy',
    loadChildren: () =>
      import('@base/src/app/pages/legal-information/privacy-policy-page/privacy-policy-page.module').then(
        (m) => m.PrivacyPolicyPageModule
      ),
  },
  {
    path: 'legal-notice',
    loadChildren: () =>
      import('@base/src/app/pages/legal-information/legal-notice-page/legal-notice-page.module').then(
        (m) => m.LegalNoticePageModule
      ),
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LegalInformationRoutingModule {}
