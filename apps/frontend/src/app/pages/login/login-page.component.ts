/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EventService } from '@shared/event.service';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent {
  public loginFormGroup: FormGroup = this.fb.group({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(
    public userService: UserLoginService,
    private router: Router,
    private eventService: EventService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef
  ) {
    this.userService.isLoggedIn$.next(true);
  }

  /**
   * Performs user authenticateAndGetUserDetails
   * and navigates to the document list page if username and password are correct.
   */
  public login(): void {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const loginCredentials = this.loginFormGroup.value;

    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    if (loginCredentials.username && loginCredentials.password) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-member-access
      this.userService.login(loginCredentials.username, loginCredentials.password).subscribe({
        next: () => {
          this.cdr.detectChanges();
          void this.router.navigateByUrl('/transport-documents');
          this.eventService.changeIconMenu(true);
          this.eventService.changeToggleNavigation(new Event('addNavbar'));
        },
        error: (err: Error) => {
          throwError(() => err);
        },
      });
    }
  }
}
