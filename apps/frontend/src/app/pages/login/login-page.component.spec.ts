/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPageComponent } from './login-page.component';
import { EventService } from '@shared/event.service';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormBuilder, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

describe('LoginComponent', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;
  let fb: FormBuilder;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginPageComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [EventService, UserLoginService, UserAuthenticationService],
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        RouterTestingModule.withRoutes([{ path: 'documents', redirectTo: '' }]),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
    fb = TestBed.inject(FormBuilder);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should perform user authenticateAndGetUserDetails successfully', () => {
    const userServiceSpy = jest.spyOn(UserLoginService.prototype, 'login');
    component.loginFormGroup = fb.group({
      username: new FormControl('foo', Validators.required),
      password: new FormControl('bar', Validators.required),
    });
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const loginCredentials = component.loginFormGroup.value;
    component.login();
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    expect(userServiceSpy).toHaveBeenCalledWith(loginCredentials.username, loginCredentials.password);
  });

  it('should navigate to /transport-documents', () => {
    const navigateSpy = jest.spyOn(router, 'navigateByUrl');
    jest.spyOn(UserLoginService.prototype, 'login').mockReturnValue(of(''));
    component.loginFormGroup = fb.group({
      username: new FormControl('foo', Validators.required),
      password: new FormControl('bar', Validators.required),
    });
    component.login();
    expect(navigateSpy).toHaveBeenCalledWith('/transport-documents');
  });
});
