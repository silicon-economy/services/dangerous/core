/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import {
  Company,
  CreateGoodsDataDto,
  Freight,
  Order,
} from '@core/api-interfaces/lib/dtos/frontend';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { CreateDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/create-dangerous-good-registration.dto';
import { WriteToBlockchainConfirmationDialogComponent } from '@base/src/app/dialogs/write-to-blockchain-confirmation-dialog/write-to-blockchain-confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import { SaveDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/data-management';
import { Router } from '@angular/router';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';

@Injectable()
export class CreateDangerousGoodRegistrationService {
  private createDangerousGoodRegistrationDto: CreateDangerousGoodRegistrationDto;

  constructor(
    private userService: UserLoginService,
    private matDialog: MatDialog,
    private dangerousGoodRegistrationHttpService: DangerousGoodRegistrationHttpService,
    private router: Router,
    private feedbackDialogService: FeedbackDialogService
  ) {
    this.userService.userCompany.subscribe(
      (consignor: Company) =>
        (this.createDangerousGoodRegistrationDto = new CreateGoodsDataDto(
          consignor,
          new Freight([new Order(Company.createEmptyCompany(), [])], undefined, '', undefined),
          undefined
        ))
    );
  }

  /**
   * Gets the `createDangerousGoodRegistrationDto` object.
   *
   * @returns The `createDangerousGoodRegistrationDto` object currently stored.
   */
  public getCreateDangerousGoodRegistrationDto(): CreateDangerousGoodRegistrationDto {
    return this.createDangerousGoodRegistrationDto;
  }

  /**
   * Gets the `freight` object from the `createDangerousGoodRegistrationDto`.
   *
   * @returns The `freight` object.
   */
  public getFreight(): Freight {
    return this.createDangerousGoodRegistrationDto.freight;
  }

  /**
   * Updates the `freight` object in the `createDangerousGoodRegistrationDto`.
   *
   * @param updatedFreight - The new `Freight` object to set.
   */
  public setFreight(updatedFreight: Freight): void {
    this.createDangerousGoodRegistrationDto.freight = updatedFreight;
  }

  /**
   * Retrieves the `orders` array from the `freight` object within the `createDangerousGoodRegistrationDto`.
   *
   * @returns The array of `orders`.
   */
  public getOrders(): Order[] {
    return this.createDangerousGoodRegistrationDto.freight.orders;
  }

  /**
   * Sets the `consignee` of the `createDangerousGoodRegistrationDto`.
   *
   * @param updatedConsignee - The `Company` object representing the updated consignee.
   */
  public setConsignee(updatedConsignee: Company): void {
    this.createDangerousGoodRegistrationDto.freight.orders[0].consignee = updatedConsignee;
  }

  /**
   * Sets the `transportationInstructions` for the `freight` object in the `createDangerousGoodRegistrationDto`.
   *
   * @param transportationInstructions - The transportation instructions as a string.
   */
  public setTransportationInstructions(transportationInstructions: string): void {
    this.createDangerousGoodRegistrationDto.freight.transportationInstructions = transportationInstructions;
  }

  /**
   * Gets the `consignor` from the `createDangerousGoodRegistrationDto`.
   *
   * @returns The `consignor` object.
   */
  getConsignor(): Company {
    return this.createDangerousGoodRegistrationDto.consignor;
  }

  /**
   * Sets the `consignor` for the `createDangerousGoodRegistrationDto`.
   *
   * @param consignor - The `Company` object representing the updated consignor.
   */
  public setConsignor(consignor: Company): void {
    this.createDangerousGoodRegistrationDto.consignor = consignor;
  }

  /**
   * Initiates the process to save the dangerous good registration in the blockchain.
   */
  saveDangerousGoodRegistrationInBlockchain(): void {
    this.matDialog
      .open(WriteToBlockchainConfirmationDialogComponent)
      .afterClosed()
      .subscribe((accept): void => {
        if (accept === true) {
          this.feedbackDialogService.showDialog();
          this.dangerousGoodRegistrationHttpService
            .createDangerousGoodRegistration(this.createDangerousGoodRegistrationDto)
            .subscribe((response: SaveDangerousGoodRegistrationDto): void => {
              this.feedbackDialogService.handleSuccess();
              this.router.navigateByUrl('/dangerous-good-registrations/' + response.id).catch((error): void => {
                throw error;
              });
            });
        }
      });
  }
}
