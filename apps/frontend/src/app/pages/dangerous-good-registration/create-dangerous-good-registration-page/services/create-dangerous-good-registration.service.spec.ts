/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { of } from 'rxjs';
import {
  WriteToBlockchainConfirmationDialogComponent
} from '@base/src/app/dialogs/write-to-blockchain-confirmation-dialog/write-to-blockchain-confirmation-dialog.component';
import { CreateDangerousGoodRegistrationService } from './create-dangerous-good-registration.service';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import {
  mockDangerousGoodRegistration
} from '@core/api-interfaces/lib/mocks/dangerous-good-registration/dangerous-good-registration.mock';
import { FeedbackDialogService } from "@shared/services/feedback-dialog-service/feedback-dialog.service";

describe('CreateDangerousGoodRegistrationService', () => {
  let service: CreateDangerousGoodRegistrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatDialogModule],
      providers: [
        UserLoginService,
        UserAuthenticationService,
        DangerousGoodRegistrationHttpService,
        CreateDangerousGoodRegistrationService,
        FeedbackDialogService
      ],
    });
    service = TestBed.inject(CreateDangerousGoodRegistrationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the createTransportDocumentRequestDto', () => {
    expect(service.getCreateDangerousGoodRegistrationDto()).toEqual(service['createDangerousGoodRegistrationDto']);
  });

  it('should set and then get the freight', () => {
    service.setFreight(mockDangerousGoodRegistration.freight);
    expect(service.getFreight()).toEqual(mockDangerousGoodRegistration.freight);
  });

  it('should get orders', () => {
    expect(service.getOrders().length).toEqual(1);
  });

  it('should set and get consignee', () => {
    const mockConsignee = mockDangerousGoodRegistration.freight.orders[0].consignee;
    service.setConsignee(mockConsignee);
    expect(service.getOrders()[0].consignee).toEqual(mockConsignee);
  });

  it('should set transportationInstructions', () => {
    service.setTransportationInstructions(mockDangerousGoodRegistration.freight.transportationInstructions);
    expect(service.getFreight().transportationInstructions).toEqual(
      mockDangerousGoodRegistration.freight.transportationInstructions
    );
  });

  it('should set and get consignor', () => {
    service.setConsignor(mockDangerousGoodRegistration.consignor);
    expect(service.getConsignor()).toEqual(mockDangerousGoodRegistration.consignor);
  });

  it('should save dangerous good registration and navigate to its details page', () => {
    const matDialogSpy = jest.spyOn(service['matDialog'], 'open').mockReturnValue({
      afterClosed: () => of(true),
    } as MatDialogRef<typeof WriteToBlockchainConfirmationDialogComponent>);
    const dangerousGoodRegistrationHttpServiceSpy = jest
      .spyOn(service['dangerousGoodRegistrationHttpService'], 'createDangerousGoodRegistration')
      .mockReturnValue(of(mockDangerousGoodRegistration));
    const routerSpy = jest.spyOn(service['router'], 'navigateByUrl');

    service.saveDangerousGoodRegistrationInBlockchain();
    expect(matDialogSpy).toHaveBeenCalled();
    expect(dangerousGoodRegistrationHttpServiceSpy).toHaveBeenCalled();
    expect(routerSpy).toHaveBeenCalledWith('/dangerous-good-registrations/' + mockDangerousGoodRegistration.id);
  });
});
