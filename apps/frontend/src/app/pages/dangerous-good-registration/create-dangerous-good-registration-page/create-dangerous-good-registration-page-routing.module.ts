/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  CreateDangerousGoodRegistrationPageComponent
} from '@base/src/app/pages/dangerous-good-registration/create-dangerous-good-registration-page/create-dangerous-good-registration-page.component';

const routes: Routes = [
  {
    path: '',
    component: CreateDangerousGoodRegistrationPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateDangerousGoodRegistrationPageRoutingModule {}
