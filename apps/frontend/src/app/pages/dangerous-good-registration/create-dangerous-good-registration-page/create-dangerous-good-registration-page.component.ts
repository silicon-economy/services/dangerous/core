/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { CreateDangerousGoodRegistrationService } from '@base/src/app/pages/dangerous-good-registration/create-dangerous-good-registration-page/services/create-dangerous-good-registration.service';

@Component({
  selector: 'app-create-dangerous-good-registration-page',
  templateUrl: './create-dangerous-good-registration-page.component.html',
})
export class CreateDangerousGoodRegistrationPageComponent {
  public consigneeInformationIsValid = false;
  public allEnteredDangerousGoodInformationAreValid = false;
  public consignorInformationIsValid = false;

  constructor(public createDangerousGoodRegistrationService: CreateDangerousGoodRegistrationService) {}

  /**
   * Sets the validation state of consignee information.
   *
   * @param consigneeInformationIsValid - A boolean indicating whether the consignee information is valid or not.
   */
  setConsigneeInformationIsValid(consigneeInformationIsValid: boolean): void {
    this.consigneeInformationIsValid = consigneeInformationIsValid;
  }
}
