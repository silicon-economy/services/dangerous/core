/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  CreateDangerousGoodRegistrationPageRoutingModule
} from './create-dangerous-good-registration-page-routing.module';
import {
  TransportModeSelectionModule
} from '@base/src/app/layouts/transport-mode-selection/transport-mode-selection.module';
import {
  CreateDangerousGoodRegistrationPageComponent
} from '@base/src/app/pages/dangerous-good-registration/create-dangerous-good-registration-page/create-dangerous-good-registration-page.component';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {
  EnterConsigneeInformationModule
} from '@base/src/app/layouts/enter-consignee-information/enter-consignee-information.module';
import {
  EnterDangerousGoodsInformationModule
} from '@base/src/app/layouts/enter-dangerous-goods-information/enter-dangerous-goods-information.module';
import {
  EnterTransportationInstructionsFormModule
} from '@base/src/app/layouts/enter-transportation-instructions-form/enter-transportation-instructions-form.module';
import {
  EnterCompanyInformationFormGroupModule
} from '@base/src/app/layouts/enter-company-information-form-group/enter-company-information-form-group.module';
import {
  CreateDocumentSummaryModule
} from '@base/src/app/layouts/create-document-summary/create-document-summary.module';
import {
  WriteToBlockchainConfirmationDialogModule
} from '@base/src/app/dialogs/write-to-blockchain-confirmation-dialog/write-to-blockchain-confirmation-dialog.module';
import {
  CreateDangerousGoodRegistrationService
} from '@base/src/app/pages/dangerous-good-registration/create-dangerous-good-registration-page/services/create-dangerous-good-registration.service';

@NgModule({
  declarations: [CreateDangerousGoodRegistrationPageComponent],
  imports: [
    CommonModule,
    CreateDangerousGoodRegistrationPageRoutingModule,
    TransportModeSelectionModule,
    TitleBarModule,
    TranslateModule,
    MatCardModule,
    MatStepperModule,
    MatButtonModule,
    MatIconModule,
    EnterConsigneeInformationModule,
    EnterDangerousGoodsInformationModule,
    EnterTransportationInstructionsFormModule,
    EnterCompanyInformationFormGroupModule,
    CreateDocumentSummaryModule,
    WriteToBlockchainConfirmationDialogModule,
  ],
  providers: [CreateDangerousGoodRegistrationService],
})
export class CreateDangerousGoodRegistrationPageModule {}
