/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDangerousGoodRegistrationPageComponent } from './create-dangerous-good-registration-page.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import { TranslateModule } from '@ngx-translate/core';
import {
  TransportModeSelectionModule
} from '@base/src/app/layouts/transport-mode-selection/transport-mode-selection.module';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import {
  EnterDangerousGoodsInformationModule
} from '@base/src/app/layouts/enter-dangerous-goods-information/enter-dangerous-goods-information.module';
import {
  EnterCarrierInformationModule
} from '@base/src/app/layouts/enter-carrier-information/enter-carrier-information.module';
import {
  EnterCompanyInformationFormGroupModule
} from '@base/src/app/layouts/enter-company-information-form-group/enter-company-information-form-group.module';
import {
  CreateDocumentSummaryModule
} from '@base/src/app/layouts/create-document-summary/create-document-summary.module';
import { DangerousGoodsGatewayHttpService } from '@shared/httpRequests/dangerous-goods-gateway-http.service';
import {
  EnterConsigneeInformationModule
} from '@base/src/app/layouts/enter-consignee-information/enter-consignee-information.module';
import {
  EnterTransportationInstructionsFormModule
} from '@base/src/app/layouts/enter-transportation-instructions-form/enter-transportation-instructions-form.module';
import {
  CreateDangerousGoodRegistrationService
} from '@base/src/app/pages/dangerous-good-registration/create-dangerous-good-registration-page/services/create-dangerous-good-registration.service';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { FeedbackDialogService } from "@shared/services/feedback-dialog-service/feedback-dialog.service";

describe('CreateDangerousGoodRegistrationPageComponent', () => {
  let component: CreateDangerousGoodRegistrationPageComponent;
  let fixture: ComponentFixture<CreateDangerousGoodRegistrationPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateDangerousGoodRegistrationPageComponent],
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        TranslateModule.forRoot(),
        TitleBarModule,
        TransportModeSelectionModule,
        MatCardModule,
        MatStepperModule,
        NoopAnimationsModule,
        MatIconModule,
        EnterDangerousGoodsInformationModule,
        EnterCarrierInformationModule,
        EnterCompanyInformationFormGroupModule,
        CreateDocumentSummaryModule,
        EnterConsigneeInformationModule,
        EnterTransportationInstructionsFormModule,
      ],
      providers: [
        UserLoginService,
        UserAuthenticationService,
        DangerousGoodRegistrationHttpService,
        DangerousGoodsGatewayHttpService,
        CreateDangerousGoodRegistrationService,
        FeedbackDialogService
      ],
    });
    fixture = TestBed.createComponent(CreateDangerousGoodRegistrationPageComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set consigneeInformationIsValid', () => {
    component.setConsigneeInformationIsValid(true);
    expect(component.consigneeInformationIsValid).toEqual(true);
  });
});
