/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  dangerousGoodRegistrationResolver
} from '@core/resolvers/dangerous-good-registration-resolver/dangerous-good-registration.resolver';
import { authGuardIsConsignor } from "@core/guards/auth.guard";

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import(
        '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/dangerous-good-registration-list-page.module'
      ).then((m) => m.DangerousGoodRegistrationListPageModule),
  },
  {
    path: 'create',
    canActivate: [authGuardIsConsignor],
    loadChildren: () =>
      import(
        '@base/src/app/pages/dangerous-good-registration/create-dangerous-good-registration-page/create-dangerous-good-registration-page.module'
        ).then((m) => m.CreateDangerousGoodRegistrationPageModule),
  },
  {
    path: ':id',
    loadChildren: () =>
      import(
        '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-details-page/dangerous-good-registration-details-page.module'
      ).then((m) => m.DangerousGoodRegistrationDetailsPageModule),
  },
  {
    path: ':id/transfer-to-transport-document',
    loadChildren: () =>
      import(
        '@base/src/app/pages/dangerous-good-registration/transfer-dangerous-good-registration-to-transport-document-page/transfer-dangerous-good-registration-to-transport-document-page.module'
        ).then((m) => m.TransferDangerousGoodRegistrationToTransportDocumentPageModule),
  },
  {
    path: ':id/accept',
    loadChildren: () =>
      import(
        '@base/src/app/pages/dangerous-good-registration/accept-dangerous-good-registration-page/accept-dangerous-good-registration-page.module'
      ).then((m) => m.AcceptDangerousGoodRegistrationPageModule),
    resolve: {
      dangerousGoodRegistration: dangerousGoodRegistrationResolver,
    },
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DangerousGoodRegistrationsRoutingModule {}
