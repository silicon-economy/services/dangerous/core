/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { SaveDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { CarrierInformation } from '@base/src/app/models/carrierInformation';
import { Carrier, CreateGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend';
import { AcceptDangerousGoodRegistrationDto } from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/accept-dangerous-good-registration.dto';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { Router } from '@angular/router';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { environment } from '@environments/environment';

@Injectable()
export class TransferDangerousGoodRegistrationToTransportDocumentService {
  dangerousGoodRegistration: SaveDangerousGoodRegistrationDto;
  carrier: Carrier;
  transportInstructions: string;

  constructor(
    private transportDocumentHttpService: TransportDocumentHttpService,
    private router: Router,
    private feedbackDialogService: FeedbackDialogService
  ) {}

  /**
   * Sets the `dangerousGoodRegistration` with the provided Dto.
   *
   * @param saveDangerousGoodRegistrationDto - The Dto containing information about the dangerous good registration.
   */
  public setDangerousGoodRegistration(saveDangerousGoodRegistrationDto: SaveDangerousGoodRegistrationDto): void {
    this.dangerousGoodRegistration = saveDangerousGoodRegistrationDto;
  }

  /**
   * Sets the `carrier` and `transportInstructions` properties based on the provided `CarrierInformation`.
   *
   * @param carrierInformation - The `CarrierInformation` object containing carrier details and transportation instructions.
   */
  public setCarrierInformation(carrierInformation: CarrierInformation): void {
    this.carrier = new Carrier(carrierInformation.name, carrierInformation.driver, carrierInformation.licensePlate);
    this.transportInstructions = carrierInformation.transportationInstructions;
  }

  /**
   * Constructs and returns a `CreateTransportDocumentDto` from the current state of the `dangerousGoodRegistration`.
   *
   * @returns The `CreateGoodsDataDto` with the `carrier` information and `transportationInstructions` included.
   */
  public getCreateTransportDocumentDto(): CreateGoodsDataDto {
    const createTransportDocumentDto = this.dangerousGoodRegistration as unknown as CreateGoodsDataDto;
    createTransportDocumentDto.carrier = this.carrier;
    createTransportDocumentDto.freight.transportationInstructions = this.transportInstructions;
    return createTransportDocumentDto;
  }

  /**
   * Initiates the process to save the transport document in the blockchain.
   */
  public saveTransportDocumentInBlockchain(): void {
    const request: AcceptDangerousGoodRegistrationDto = new AcceptDangerousGoodRegistrationDto();
    request.dangerousGoodRegistrationId = this.dangerousGoodRegistration.id;
    request.carrier = this.carrier;
    request.transportationInstructions = this.transportInstructions;
    request.acceptedOrderPositionIds = this.dangerousGoodRegistration.freight.orders[0].orderPositions.map(
      (orderPosition) => orderPosition.id
    );

    this.feedbackDialogService.showDialog();
    this.transportDocumentHttpService
      .transferDangerousGoodRegistrationToTransportDocument(request)
      .subscribe((response: SaveGoodsDataDto): void => {
        this.feedbackDialogService.handleSuccess();
        this.router
          .navigateByUrl(`${environment.ROUTER_LINKS.TRANSPORT_DOCUMENT_LIST}` + response.id)
          .catch((error): void => {
            throw error;
          });
      });
  }
}
