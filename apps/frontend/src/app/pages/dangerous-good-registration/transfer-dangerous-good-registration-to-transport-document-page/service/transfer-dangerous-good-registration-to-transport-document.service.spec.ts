/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import {
  TransferDangerousGoodRegistrationToTransportDocumentService
} from './transfer-dangerous-good-registration-to-transport-document.service';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import {
  TransferDangerousGoodRegistrationToTransportDocumentPageComponent
} from '@base/src/app/pages/dangerous-good-registration/transfer-dangerous-good-registration-to-transport-document-page/transfer-dangerous-good-registration-to-transport-document-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CarrierInformation } from '@base/src/app/models/carrierInformation';
import {
  AcceptDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/accept-dangerous-good-registration.dto';
import { of } from 'rxjs';
import {
  mockTransportDocumentTransport
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import {
  mockDangerousGoodRegistration
} from '@core/api-interfaces/lib/mocks/dangerous-good-registration/dangerous-good-registration.mock';
import { mockCarrier } from '@core/api-interfaces/lib/mocks/carrier/carrier.mock';
import { FeedbackDialogService } from "@shared/services/feedback-dialog-service/feedback-dialog.service";
import { MatDialogModule } from "@angular/material/dialog";

describe('TransferDangerousGoodRegistrationToTransportDocumentService', () => {
  let service: TransferDangerousGoodRegistrationToTransportDocumentService;

  const mockTransportInstructions = 'Please clean the vehicle after dropping off the load.';
  const mockCarrierInformation = new CarrierInformation(
    mockCarrier.name,
    mockCarrier.driver,
    mockCarrier.licensePlate,
    mockTransportInstructions
  );

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TransferDangerousGoodRegistrationToTransportDocumentPageComponent],
      imports: [RouterTestingModule, HttpClientTestingModule, TranslateModule.forRoot(), MatDialogModule],
      providers: [TransferDangerousGoodRegistrationToTransportDocumentService, TransportDocumentHttpService, FeedbackDialogService],
    });
    service = TestBed.inject(TransferDangerousGoodRegistrationToTransportDocumentService);
    service.setDangerousGoodRegistration(mockDangerousGoodRegistration);
    service.setCarrierInformation(mockCarrierInformation);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should build the createTransportDocumentDto used for summary correctly', () => {
    const createTransportDocumentDTO = service.getCreateTransportDocumentDto();
    expect(createTransportDocumentDTO.freight).toEqual(mockDangerousGoodRegistration.freight);
  });

  it('should build and send the correct request', () => {
    const transportDocumentHttpServiceSpy = jest
      .spyOn(service['transportDocumentHttpService'], 'transferDangerousGoodRegistrationToTransportDocument')
      .mockReturnValue(of(mockTransportDocumentTransport));
    const routerSpy = jest.spyOn(service['router'], 'navigateByUrl');
    const expectedRequest: AcceptDangerousGoodRegistrationDto = {
      dangerousGoodRegistrationId: mockDangerousGoodRegistration.id,
      carrier: mockCarrier,
      transportationInstructions: mockTransportInstructions,
      acceptedOrderPositionIds: mockDangerousGoodRegistration.freight.orders[0].orderPositions.map(
        (orderPosition) => orderPosition.id
      ),
    };
    service.saveTransportDocumentInBlockchain();
    expect(transportDocumentHttpServiceSpy).toHaveBeenCalledWith(expectedRequest);
    expect(routerSpy).toHaveBeenCalledWith('/transport-documents/' + mockTransportDocumentTransport.id);
  });
});
