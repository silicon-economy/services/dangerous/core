/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { ActivatedRoute, Data } from '@angular/router';
import {
  TransferDangerousGoodRegistrationToTransportDocumentService
} from '@base/src/app/pages/dangerous-good-registration/transfer-dangerous-good-registration-to-transport-document-page/service/transfer-dangerous-good-registration-to-transport-document.service';

@Component({
  selector: 'app-transfer-dangerous-good-registration-to-transport-document-page',
  templateUrl: './transfer-dangerous-good-registration-to-transport-document-page.component.html',
})
export class TransferDangerousGoodRegistrationToTransportDocumentPageComponent implements OnInit {
  carrierInformationIsValid: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    public transferDangerousGoodRegistrationToTransportDocumentService: TransferDangerousGoodRegistrationToTransportDocumentService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: Data) => {
      this.transferDangerousGoodRegistrationToTransportDocumentService.setDangerousGoodRegistration(
        <SaveDangerousGoodRegistrationDto>data.dangerousGoodRegistration
      );
    });
  }
}
