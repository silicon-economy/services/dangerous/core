/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  TransferDangerousGoodRegistrationToTransportDocumentPageComponent
} from './transfer-dangerous-good-registration-to-transport-document-page.component';
import {
  TransferDangerousGoodRegistrationToTransportDocumentPageRoutingModule
} from '@base/src/app/pages/dangerous-good-registration/transfer-dangerous-good-registration-to-transport-document-page/transfer-dangerous-good-registration-to-transport-document-page-routing.module';
import {
  CreateDocumentSummaryModule
} from '@base/src/app/layouts/create-document-summary/create-document-summary.module';
import {
  EnterCarrierInformationModule
} from '@base/src/app/layouts/enter-carrier-information/enter-carrier-information.module';
import {
  EnterCompanyInformationFormGroupModule
} from '@base/src/app/layouts/enter-company-information-form-group/enter-company-information-form-group.module';
import {
  EnterConsigneeInformationModule
} from '@base/src/app/layouts/enter-consignee-information/enter-consignee-information.module';
import {
  EnterDangerousGoodsInformationModule
} from '@base/src/app/layouts/enter-dangerous-goods-information/enter-dangerous-goods-information.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatStepperModule } from '@angular/material/stepper';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  TransportModeSelectionModule
} from '@base/src/app/layouts/transport-mode-selection/transport-mode-selection.module';
import {
  TransferDangerousGoodRegistrationToTransportDocumentService
} from '@base/src/app/pages/dangerous-good-registration/transfer-dangerous-good-registration-to-transport-document-page/service/transfer-dangerous-good-registration-to-transport-document.service';
import {
  AcceptDangerousGoodRegistrationQrCodeScannerDialogModule
} from '@base/src/app/dialogs/accept-dangerous-good-registration-qr-code-scanner-dialog/accept-dangerous-good-registration-qr-code-scanner-dialog.module';

@NgModule({
  declarations: [TransferDangerousGoodRegistrationToTransportDocumentPageComponent],
  imports: [
    CommonModule,
    TransferDangerousGoodRegistrationToTransportDocumentPageRoutingModule,
    CreateDocumentSummaryModule,
    EnterCarrierInformationModule,
    EnterCompanyInformationFormGroupModule,
    EnterConsigneeInformationModule,
    EnterDangerousGoodsInformationModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatStepperModule,
    TitleBarModule,
    TranslateModule,
    TransportModeSelectionModule,
    AcceptDangerousGoodRegistrationQrCodeScannerDialogModule,
  ],
  providers: [TransferDangerousGoodRegistrationToTransportDocumentService],
})
export class TransferDangerousGoodRegistrationToTransportDocumentPageModule {}
