/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import {
  TransferDangerousGoodRegistrationToTransportDocumentPageComponent
} from './transfer-dangerous-good-registration-to-transport-document-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import {
  TransferDangerousGoodRegistrationToTransportDocumentService
} from '@base/src/app/pages/dangerous-good-registration/transfer-dangerous-good-registration-to-transport-document-page/service/transfer-dangerous-good-registration-to-transport-document.service';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import {
  TransportModeSelectionModule
} from '@base/src/app/layouts/transport-mode-selection/transport-mode-selection.module';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  EnterCarrierInformationModule
} from '@base/src/app/layouts/enter-carrier-information/enter-carrier-information.module';
import {
  CreateDocumentSummaryModule
} from '@base/src/app/layouts/create-document-summary/create-document-summary.module';
import { MatIconModule } from '@angular/material/icon';
import {
  mockCreateTransportDocumentRequestDto
} from '@core/api-interfaces/lib/mocks/transport-document/create-transport-document-request-dto.mock';
import { FeedbackDialogService } from "@shared/services/feedback-dialog-service/feedback-dialog.service";

describe('TransferDangerousGoodRegistrationToTransportDocumentPageComponent', () => {
  let component: TransferDangerousGoodRegistrationToTransportDocumentPageComponent;
  let fixture: ComponentFixture<TransferDangerousGoodRegistrationToTransportDocumentPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TransferDangerousGoodRegistrationToTransportDocumentPageComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        TitleBarModule,
        TransportModeSelectionModule,
        MatCardModule,
        MatStepperModule,
        NoopAnimationsModule,
        EnterCarrierInformationModule,
        CreateDocumentSummaryModule,
        MatIconModule,
      ],
      providers: [TransferDangerousGoodRegistrationToTransportDocumentService, TransportDocumentHttpService,
      FeedbackDialogService],
    });
    fixture = TestBed.createComponent(TransferDangerousGoodRegistrationToTransportDocumentPageComponent);
    component = fixture.componentInstance;
    jest
      .spyOn(component.transferDangerousGoodRegistrationToTransportDocumentService, 'getCreateTransportDocumentDto')
      .mockReturnValue(mockCreateTransportDocumentRequestDto);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
