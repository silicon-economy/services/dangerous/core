/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-dangerous-good-registration.dto';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import {
  DangerousGoodRegistrationService
} from '@shared/services/dangerous-good-registration-service/dangerous-good-registration.service';
import { PermissionService } from '@shared/services/permission-service/permission.service';

@Component({
  selector: 'app-dangerous-good-registration-details-page-button-bar [dangerousGoodRegistration]',
  templateUrl: './dangerous-good-registration-details-page-button-bar.component.html',
  styleUrls: ['./dangerous-good-registration-details-page-button-bar.component.scss'],
})
export class DangerousGoodRegistrationDetailsPageButtonBarComponent {
  @Input() dangerousGoodRegistration!: SaveDangerousGoodRegistrationDto;

  constructor(
    public transportDocumentService: TransportDocumentService,
    public dangerousGoodRegistrationService: DangerousGoodRegistrationService,
    public permissionService: PermissionService
  ) {}
}
