/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import {
  DangerousGoodRegistrationDetailsPageButtonBarComponent
} from './dangerous-good-registration-details-page-button-bar.component';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import {
  DangerousGoodRegistrationService
} from '@shared/services/dangerous-good-registration-service/dangerous-good-registration.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material/icon';
import { MatDialog } from '@angular/material/dialog';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ScanDialogServiceModule } from '@shared/services/scan-dialog-service/scan-dialog-service.module';

describe('DangerousGoodRegistrationDetailsPageButtonBarComponent', () => {
  let component: DangerousGoodRegistrationDetailsPageButtonBarComponent;
  let fixture: ComponentFixture<DangerousGoodRegistrationDetailsPageButtonBarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DangerousGoodRegistrationDetailsPageButtonBarComponent],
      imports: [HttpClientTestingModule, TranslateModule.forRoot(), MatIconModule, ScanDialogServiceModule],
      providers: [
        TransportDocumentHttpService,
        TransportDocumentService,
        DangerousGoodRegistrationService,
        DangerousGoodRegistrationHttpService,
        UserLoginService,
        UserAuthenticationService,
        { provide: MatDialog, useValue: {} },
        { provide: MatSnackBar, useValue: {} },
        FeedbackDialogService,
        NotificationService,
      ],
    });
    fixture = TestBed.createComponent(DangerousGoodRegistrationDetailsPageButtonBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
