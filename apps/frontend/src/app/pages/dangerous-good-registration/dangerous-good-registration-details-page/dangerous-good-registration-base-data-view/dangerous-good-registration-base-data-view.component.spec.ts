/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DangerousGoodRegistrationBaseDataViewComponent } from './dangerous-good-registration-base-data-view.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material/icon';
import {
  mockDangerousGoodRegistration
} from "@core/api-interfaces/lib/mocks/dangerous-good-registration/dangerous-good-registration.mock";

describe('DangerousGoodRegistrationBaseDataViewComponent', () => {
  let component: DangerousGoodRegistrationBaseDataViewComponent;
  let fixture: ComponentFixture<DangerousGoodRegistrationBaseDataViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DangerousGoodRegistrationBaseDataViewComponent],
      imports: [TranslateModule.forRoot(), MatIconModule],
    });
    fixture = TestBed.createComponent(DangerousGoodRegistrationBaseDataViewComponent);
    component = fixture.componentInstance;
    component.dangerousGoodRegistration = mockDangerousGoodRegistration;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
