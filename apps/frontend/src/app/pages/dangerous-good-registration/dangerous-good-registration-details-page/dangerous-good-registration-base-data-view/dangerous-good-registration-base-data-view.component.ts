/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-dangerous-good-registration.dto';

@Component({
  selector: 'app-dangerous-good-registration-base-data-view',
  templateUrl: './dangerous-good-registration-base-data-view.component.html',
  styleUrls: ['./dangerous-good-registration-base-data-view.component.css'],
})
export class DangerousGoodRegistrationBaseDataViewComponent {
  @Input() dangerousGoodRegistration: SaveDangerousGoodRegistrationDto;
}
