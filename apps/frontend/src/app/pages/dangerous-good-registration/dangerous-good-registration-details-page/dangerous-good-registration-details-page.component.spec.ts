/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DangerousGoodRegistrationDetailsPageComponent } from './dangerous-good-registration-details-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('DangerousGoodRegistrationDetailsPageComponent', () => {
  let component: DangerousGoodRegistrationDetailsPageComponent;
  let fixture: ComponentFixture<DangerousGoodRegistrationDetailsPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DangerousGoodRegistrationDetailsPageComponent],
      imports: [RouterTestingModule, TranslateModule.forRoot()],
    });
    fixture = TestBed.createComponent(DangerousGoodRegistrationDetailsPageComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
