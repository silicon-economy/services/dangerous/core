/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  DangerousGoodRegistrationDetailsPageRoutingModule
} from './dangerous-good-registration-details-page-routing.module';
import { DangerousGoodRegistrationDetailsPageComponent } from './dangerous-good-registration-details-page.component';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  DangerousGoodRegistrationBaseDataViewComponent
} from './dangerous-good-registration-base-data-view/dangerous-good-registration-base-data-view.component';
import { MatIconModule } from '@angular/material/icon';
import {
  ShowStatusHistoryButtonModule
} from '@base/src/app/layouts/show-status-history-button/show-status-history-button.module';
import { StatusModule } from '@base/src/app/layouts/status/status.module';
import { MatCardModule } from '@angular/material/card';
import {
  ConsignorAndFreightViewModule
} from '@base/src/app/layouts/consignor-and-freight-view/consignor-and-freight-view.module';
import { MapModule } from '@maps/map.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { OrderViewModule } from '@base/src/app/layouts/order-view/order-view.module';
import { SensingPuckDataViewModule } from '@base/src/app/layouts/sensing-puck-data-view/sensing-puck-data-view.module';
import {
  DangerousGoodRegistrationDetailsPageButtonBarComponent
} from './dangerous-good-registration-details-page-button-bar/dangerous-good-registration-details-page-button-bar.component';
import { MatButtonModule } from '@angular/material/button';
import { TransportDocumentService } from "@shared/services/transport-document-service/transport-document.service";

@NgModule({
  declarations: [
    DangerousGoodRegistrationDetailsPageComponent,
    DangerousGoodRegistrationBaseDataViewComponent,
    DangerousGoodRegistrationDetailsPageButtonBarComponent,
  ],
  exports: [
    DangerousGoodRegistrationDetailsPageComponent
  ],
  imports: [
    CommonModule,
    DangerousGoodRegistrationDetailsPageRoutingModule,
    TitleBarModule,
    TranslateModule,
    MatIconModule,
    ShowStatusHistoryButtonModule,
    StatusModule,
    MatCardModule,
    ConsignorAndFreightViewModule,
    MapModule,
    MatExpansionModule,
    OrderViewModule,
    SensingPuckDataViewModule,
    MatButtonModule,
  ],
  providers:[TransportDocumentService]
})
export class DangerousGoodRegistrationDetailsPageModule { }
