/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DangerousGoodRegistrationDetailsPageComponent } from './dangerous-good-registration-details-page.component';
import {
  dangerousGoodRegistrationResolver
} from '@core/resolvers/dangerous-good-registration-resolver/dangerous-good-registration.resolver';

const routes: Routes = [
  {
    path: '',
    component: DangerousGoodRegistrationDetailsPageComponent,
    resolve: { dangerousGoodRegistration: dangerousGoodRegistrationResolver },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DangerousGoodRegistrationDetailsPageRoutingModule {}
