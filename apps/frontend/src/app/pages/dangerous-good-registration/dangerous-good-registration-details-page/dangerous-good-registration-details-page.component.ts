/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';

@Component({
  selector: 'app-dangerous-good-registration-details-page',
  templateUrl: './dangerous-good-registration-details-page.component.html',
})
export class DangerousGoodRegistrationDetailsPageComponent implements OnInit {
  dangerousGoodRegistration: SaveDangerousGoodRegistrationDto;
  @Input() showButtonBar = true;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: Data) => {
      this.dangerousGoodRegistration = <SaveDangerousGoodRegistrationDto>data.dangerousGoodRegistration;
    });
  }
}
