/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { AcceptDangerousGoodRegistrationService } from './accept-dangerous-good-registration.service';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DangerousGoodsGatewayHttpService } from '@shared/httpRequests/dangerous-goods-gateway-http.service';
import {
  TransportDocumentDetailsPageModule
} from '@base/src/app/pages/transport-document/transport-document-details-page/transport-document-details-page.module';
import { MatGridListModule } from '@angular/material/grid-list';
import {
  mockDangerousGoodRegistration
} from '@core/api-interfaces/lib/mocks/dangerous-good-registration/dangerous-good-registration.mock';
import { mockCarrier } from '@core/api-interfaces/lib/mocks/carrier/carrier.mock';
import { CarrierInformation } from '@base/src/app/models/carrierInformation';
import {
  AcceptDangerousGoodRegistrationDto
} from "@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/accept-dangerous-good-registration.dto";
import {
  mockTransportDocumentCreated,
  mockTransportDocumentTransport
} from '@core/api-interfaces/lib/mocks/transport-document/transport-document.mock';
import { TranslateModule } from '@ngx-translate/core';
import {
  WriteToBlockchainConfirmationDialogModule
} from '@base/src/app/dialogs/write-to-blockchain-confirmation-dialog/write-to-blockchain-confirmation-dialog.module';
import { of } from 'rxjs';
import {
  WriteToBlockchainConfirmationDialogComponent
} from '@base/src/app/dialogs/write-to-blockchain-confirmation-dialog/write-to-blockchain-confirmation-dialog.component';
import { FeedbackDialogService } from "@shared/services/feedback-dialog-service/feedback-dialog.service";

describe('AcceptDangerousGoodRegistrationService', () => {
  let service: AcceptDangerousGoodRegistrationService;
  const mockCarrierInformation: CarrierInformation = new CarrierInformation(
    mockCarrier.name,
    mockCarrier.driver,
    mockCarrier.licensePlate,
    'mockTransportInstructions'
  );
  const mockAcceptDangerousGoodRegistrationDto: AcceptDangerousGoodRegistrationDto = new AcceptDangerousGoodRegistrationDto(
    '7459465',
    mockCarrier,
    'Please clean the transport unit after unloading',
    []
  );

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AcceptDangerousGoodRegistrationService,
        TransportDocumentHttpService,
        DangerousGoodsGatewayHttpService,
        FeedbackDialogService
      ],
      imports: [
        MatDialogModule,
        HttpClientTestingModule,
        TransportDocumentDetailsPageModule,
        MatGridListModule,
        TransportDocumentDetailsPageModule,
        TranslateModule.forRoot(),
        WriteToBlockchainConfirmationDialogModule,
      ],
    });
    service = TestBed.inject(AcceptDangerousGoodRegistrationService);
    service.setDangerousGoodRegistration(mockDangerousGoodRegistration);
    service.setImportedTransportDocument(mockTransportDocumentTransport);
    service.setAcceptedOrders(mockDangerousGoodRegistration.freight.orders);
    service.setCarrierInformation(mockCarrierInformation);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return the correct previewTransportDocument', () => {
    expect(service.getImportedTransportDocument()).toEqual(mockTransportDocumentTransport);
    expect(service.getDangerousGoodRegistration()).toEqual(mockDangerousGoodRegistration);
    expect(service.getPreviewTransportDocument().id).toEqual(mockTransportDocumentTransport.id);
  });

  it('should save it to the blockchain correctly', () => {
    const matDialogSpy = jest.spyOn(service['matDialog'], 'open').mockReturnValue({
      afterClosed: () => of(true),
    } as MatDialogRef<typeof WriteToBlockchainConfirmationDialogComponent>);
    const dangerousGoodRegistrationHttpServiceSpy = jest
      .spyOn(service['transportDocumentHttpService'], 'acceptDangerousGoodRegistration')
      .mockReturnValue(of(mockTransportDocumentTransport));
    const routerSpy = jest.spyOn(service['router'], 'navigateByUrl');
    service.saveIntoBlockchainConfirmation();
    expect(matDialogSpy).toHaveBeenCalled();
    expect(dangerousGoodRegistrationHttpServiceSpy).toHaveBeenCalled();
    expect(routerSpy).toHaveBeenCalledWith('/transport-documents/' + mockTransportDocumentTransport.id);
  });

  it('should navigate to the correct route after creating transport document', () => {
    jest.spyOn(service['transportDocumentHttpService'], 'createTransportDocumentAndImmediateTransport').mockReturnValueOnce(of(mockTransportDocumentCreated));
    const routerSpy = jest.spyOn(service['router'], 'navigateByUrl');

    service['createTransportDocumentFromAcceptedOrders'](mockAcceptDangerousGoodRegistrationDto);
    expect(routerSpy).toHaveBeenCalledWith('/transport-documents/' + mockTransportDocumentCreated.id);
  });
});
