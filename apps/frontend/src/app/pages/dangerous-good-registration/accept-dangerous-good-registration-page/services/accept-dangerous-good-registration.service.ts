/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { Carrier, Freight } from '@core/api-interfaces/lib/dtos/frontend';
import { CarrierInformation } from '@base/src/app/models/carrierInformation';
import {
  WriteToBlockchainConfirmationDialogComponent
} from '@base/src/app/dialogs/write-to-blockchain-confirmation-dialog/write-to-blockchain-confirmation-dialog.component';
import {
  AcceptDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/accept-dangerous-good-registration.dto';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { DangerousGoodsGatewayHttpService } from '@shared/httpRequests/dangerous-goods-gateway-http.service';
import {
  SingleCalculationRequest
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/request/singleCalculationRequest';
import {
  SingleCalculationRequestWrapper
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/request/singleCalculationRequestWrapper';
import {
  CalculationRequest
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/request/calculationRequest';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import {
  TransportCategoryPoints
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-goods-gateway/business-logic/responce/transportCategoryPoints';
import { TransportDocumentStatus } from '@core/api-interfaces/lib/dtos/frontend/transport-document/enums/status.enum';
import { FreightIdsIncluded } from '@core/api-interfaces/lib/dtos/frontend/transport-document/freight-ids-included';
import {
  VisualInspectionCarrierCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/visual-inspection-carrier-criteria';
import { firstValueFrom } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';

@Injectable()
export class AcceptDangerousGoodRegistrationService {
  private dangerousGoodRegistration: SaveDangerousGoodRegistrationDto;
  private importedTransportDocument?: SaveGoodsDataDto;
  private carrier: Carrier = new Carrier('', '', '');
  private transportationInstructions = '';
  private acceptedOrders: OrderWithId[] = [];
  private previewTransportDocument = new SaveGoodsDataDto();

  constructor(
    private matDialog: MatDialog,
    private router: Router,
    private transportDocumentHttpService: TransportDocumentHttpService,
    private dangerousGoodsGatewayHttpService: DangerousGoodsGatewayHttpService,
    private translateService: TranslateService,
    private feedbackDialogService: FeedbackDialogService
  ) {}

  /**
   * Set the dangerous-good-registration which is to be accepted
   *
   * @param dangerousGoodRegistration - the dangerousGoodRegistration to be accepted
   */
  public setDangerousGoodRegistration(dangerousGoodRegistration: SaveDangerousGoodRegistrationDto): void {
    this.dangerousGoodRegistration = dangerousGoodRegistration;
  }

  /**
   * Get the dangerous-good-registration which is to be accepted
   *
   * @returns SaveDangerousGoodRegistrationDto
   */
  public getDangerousGoodRegistration(): SaveDangerousGoodRegistrationDto {
    return this.dangerousGoodRegistration;
  }

  /**
   * Set the transport document into which the order positions of the dangerous-good-registration will be imported.
   * If it's undefined a new transport document will be created for the accepted orderPositions.
   *
   * @param importedTransportDocument - the transport document receiving the accepted orderPositions
   */
  public setImportedTransportDocument(importedTransportDocument: SaveGoodsDataDto): void {
    this.importedTransportDocument = importedTransportDocument;
  }

  /**
   * Get the transport document into which the order positions of the dangerous-good-registration will be imported.
   *
   * @returns SaveGoodsDataDto
   */
  public getImportedTransportDocument(): SaveGoodsDataDto {
    return this.importedTransportDocument;
  }

  /**
   * Sets the carrier and transportInstructions variables to the values entered by the user.
   *
   * @param carrierInformation - CarrierInformation
   */
  public setCarrierInformation(carrierInformation: CarrierInformation): void {
    this.carrier = new Carrier(carrierInformation.name, carrierInformation.driver, carrierInformation.licensePlate);
    this.transportationInstructions = carrierInformation.transportationInstructions;
    void this.generatePreviewTransportDocument();
  }

  /**
   * Takes all orders to be selected from and filters which have been selected. Then the result is taken and saved in
   * the acceptedOrders attribute.
   *
   * @param orders - all orders to be selected from, containing information whether they have been accepted or not
   */
  public setAcceptedOrders(orders: OrderWithId[]): void {
    let acceptedOrders = structuredClone(orders);
    acceptedOrders = acceptedOrders.map((order) => {
      order.orderPositions = order.orderPositions.filter((orderPosition: OrderPositionWithId) => {
        if (orderPosition.logEntries[0]) {
          return (orderPosition.logEntries[0].acceptanceCriteria as VisualInspectionCarrierCriteria).acceptLabeling;
        }
      });
      return order;
    });
    this.acceptedOrders = acceptedOrders;
    void this.generatePreviewTransportDocument();
  }

  /**
   * Get the accepted orders
   *
   * @returns OrderWithId[]
   */
  public getAcceptedOrders(): OrderWithId[] {
    return this.acceptedOrders;
  }

  /**
   * Get a preview transportDocument showing how the result of the acception of the dangerous good registration will look.
   *
   * @returns SaveGoodsDataDto
   */
  public getPreviewTransportDocument(): SaveGoodsDataDto {
    return this.previewTransportDocument;
  }

  /**
   * Generates the previewTransportDocument and saves it to the previewTransportDocument variable.
   *
   * @returns Promise<void>
   */
  public async generatePreviewTransportDocument(): Promise<void> {
    this.previewTransportDocument.freight = new FreightIdsIncluded();
    if (this.importedTransportDocument) {
      this.previewTransportDocument.id = this.importedTransportDocument.id;
      this.previewTransportDocument.freight.orders = this.importedTransportDocument.freight.orders.concat(
        this.acceptedOrders
      );
      this.previewTransportDocument.carrier = this.importedTransportDocument.carrier;
      this.previewTransportDocument.consignor = this.importedTransportDocument.consignor;
    } else {
      this.translateService
        .get('acceptDangerousGoodRegistrationPage.notGeneratedYet')
        .subscribe((translation: string) => (this.previewTransportDocument.id = translation));
      this.previewTransportDocument.freight.orders = this.acceptedOrders;
      this.previewTransportDocument.consignor = this.dangerousGoodRegistration.consignor;
      this.previewTransportDocument.carrier = this.carrier;
    }
    this.previewTransportDocument.freight.transportationInstructions = this.transportationInstructions;
    this.previewTransportDocument.status = TransportDocumentStatus.transport;

    if (this.previewTransportDocument.freight?.orders?.length > 0) {
      await this.updateTransportPointsAndExemptionStatus(this.previewTransportDocument.freight);
    }

    if (this.importedTransportDocument) {
      this.previewTransportDocument.freight.orders = this.importedTransportDocument.freight.orders;
    }
  }

  /**
   * Updates the TransportPoints and Exemption Status for the whole freight
   *
   * @param freight - the freight to calculate transportPoints and exemption status for
   * @returns Promise<void>
   */
  public async updateTransportPointsAndExemptionStatus(freight: Freight): Promise<void> {
    await firstValueFrom(
      this.dangerousGoodsGatewayHttpService.calculateTransportCategoryPoints(
        this.buildAdrCalculationRequestDto(freight)
      )
    ).then((transportCategoryPoints: TransportCategoryPoints) => {
      freight.totalTransportPoints = transportCategoryPoints.totalTransportCategoryPoints;
      freight.additionalInformation = transportCategoryPoints.exemptionStatus;
    });
  }

  /**
   * Build the request dto for performing the adr calculation
   *
   * @param freight - the freight to calculate transportPoints and exemption status for
   * @returns CalculationRequest
   */
  private buildAdrCalculationRequestDto(freight: Freight): CalculationRequest {
    return new CalculationRequest(
      freight.orders.map(
        (order) =>
          new SingleCalculationRequestWrapper(
            order.orderPositions.map(
              (orderPosition) =>
                new SingleCalculationRequest(
                  orderPosition.dangerousGood.unNumber,
                  orderPosition.unit,
                  orderPosition.quantity,
                  orderPosition.individualAmount
                )
            )
          )
      )
    );
  }

  /**
   * Opens a confirmation dialog for saving data into the blockchain.
   * If the dialog is accepted, the dangerousGoodRegistration is accepted as wanted.
   */
  public saveIntoBlockchainConfirmation(): void {
    this.matDialog
      .open(WriteToBlockchainConfirmationDialogComponent)
      .afterClosed()
      .subscribe((accept): void => {
        if (accept === true) {
          const acceptDangerousGoodRegistrationDto: AcceptDangerousGoodRegistrationDto =
            new AcceptDangerousGoodRegistrationDto(
              this.dangerousGoodRegistration.id,
              this.carrier,
              this.transportationInstructions,
              this.acceptedOrders[0].orderPositions.map((orderPosition: OrderPositionWithId) => orderPosition.id),
              this.importedTransportDocument?.id
            );
          if (this.importedTransportDocument !== undefined) {
            this.importAcceptedOrdersIntoExistingTransportDocument(acceptDangerousGoodRegistrationDto);
          } else {
            this.createTransportDocumentFromAcceptedOrders(acceptDangerousGoodRegistrationDto);
          }
        }
      });
  }

  /**
   * Creates a new Transport document from the accepted orders of the dangerousGoodRegistration and navigates to the
   * details page.
   *
   * @param acceptDangerousGoodRegistrationDto - the dto containing all necessary values
   */
  private createTransportDocumentFromAcceptedOrders(
    acceptDangerousGoodRegistrationDto: AcceptDangerousGoodRegistrationDto
  ): void {
    this.feedbackDialogService.showDialog();
    this.transportDocumentHttpService
      .createTransportDocumentAndImmediateTransport(acceptDangerousGoodRegistrationDto)
      .subscribe((saveGoodsDataDto: SaveGoodsDataDto): void => {
        this.feedbackDialogService.handleSuccess();
        this.router.navigateByUrl('/transport-documents/' + saveGoodsDataDto.id).catch((error): void => {
          throw error;
        });
      });
  }

  /**
   * Imports the accepted orders of the dangerousGoodRegistration into an existing transportDocument and navigates to the
   * details page.
   *
   * @param acceptDangerousGoodRegistrationDto - the dto containing all necessary values
   */
  private importAcceptedOrdersIntoExistingTransportDocument(
    acceptDangerousGoodRegistrationDto: AcceptDangerousGoodRegistrationDto
  ): void {
    this.feedbackDialogService.showDialog();
    this.transportDocumentHttpService
      .acceptDangerousGoodRegistration(acceptDangerousGoodRegistrationDto)
      .subscribe((saveGoodsDataDto: SaveGoodsDataDto): void => {
        this.feedbackDialogService.handleSuccess();
        this.router.navigateByUrl('/transport-documents/' + saveGoodsDataDto.id).catch((error): void => {
          throw error;
        });
      });
  }
}
