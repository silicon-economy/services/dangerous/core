/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  AcceptDangerousGoodRegistrationPageRoutingModule
} from './accept-dangerous-good-registration-page-routing.module';
import {
  AcceptDangerousGoodsRegistrationPageComponent
} from '@base/src/app/pages/dangerous-good-registration/accept-dangerous-good-registration-page/accept-dangerous-goods-registration-page.component';
import { MatButtonModule } from '@angular/material/button';
import { MatStepperModule } from '@angular/material/stepper';
import {
  OrderPositionsGridViewModule
} from '@base/src/app/pages/dangerous-good-registration/accept-dangerous-good-registration-page/components/order-positions-grid-view/order-positions-grid-view.module';
import { MatIconModule } from '@angular/material/icon';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import {
  DangerousGoodRegistrationDetailsPageModule
} from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-details-page/dangerous-good-registration-details-page.module';
import { TransportDocumentService } from '@shared/services/transport-document-service/transport-document.service';
import {
  EnterCarrierInformationModule
} from "@base/src/app/layouts/enter-carrier-information/enter-carrier-information.module";
import {
  EnterCompanyInformationFormGroupModule
} from "@base/src/app/layouts/enter-company-information-form-group/enter-company-information-form-group.module";
import { TranslateModule } from "@ngx-translate/core";
import {
  AcceptDangerousGoodRegistrationService
} from "@base/src/app/pages/dangerous-good-registration/accept-dangerous-good-registration-page/services/accept-dangerous-good-registration.service";
import {
  TransportDocumentDetailsPageModule
} from "@base/src/app/pages/transport-document/transport-document-details-page/transport-document-details-page.module";
import { CriterionCheckModule } from "@shared/components/criterion-check/criterion-check.module";
import { MatInputModule } from "@angular/material/input";
import {
  PreviewTransportDocumentDetailsViewComponent
} from './components/preview-transport-document-details-view/preview-transport-document-details-view.component';
import { MatExpansionModule } from "@angular/material/expansion";
import { OrderViewModule } from "@base/src/app/layouts/order-view/order-view.module";

@NgModule({
  declarations: [AcceptDangerousGoodsRegistrationPageComponent, PreviewTransportDocumentDetailsViewComponent],
  imports: [
    CommonModule,
    AcceptDangerousGoodRegistrationPageRoutingModule,
    MatButtonModule,
    MatStepperModule,
    OrderPositionsGridViewModule,
    MatIconModule,
    TitleBarModule,
    DangerousGoodRegistrationDetailsPageModule,
    EnterCarrierInformationModule,
    EnterCompanyInformationFormGroupModule,
    TranslateModule,
    TransportDocumentDetailsPageModule,
    CriterionCheckModule,
    MatInputModule,
    MatExpansionModule,
    OrderViewModule
  ],
  providers: [TransportDocumentService, AcceptDangerousGoodRegistrationService],
})
export class AcceptDangerousGoodRegistrationPageModule {}
