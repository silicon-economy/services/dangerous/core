/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  VisualInspectionCarrierCriteria
} from '@core/api-interfaces/lib/dtos/frontend/transport-document/acceptance-criteria/visual-inspection-carrier-criteria';
import { LogEntry } from '@core/api-interfaces/lib/dtos/frontend/transport-document/log-entry';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';

@Component({
  selector: 'app-order-positions-grid-view',
  templateUrl: './order-positions-grid-view.component.html',
  styleUrls: ['./order-positions-grid-view.component.scss'],
})
export class OrderPositionsGridViewComponent {
  @Input()
  orders: OrderWithId[];

  @Output() ordersChange: EventEmitter<OrderWithId[]> = new EventEmitter<OrderWithId[]>();

  /**
   * Switch status for orderIndex to accepted or denied
   *
   * @param selected - Selection state (boolean)
   * @param orderIndex - The order's index
   * @param orderPositionIndex - The position of the order
   */
  onSelectChange(selected: boolean, orderIndex: number, orderPositionIndex: number): void {
    if (selected) {
      this.orders[orderIndex].orderPositions[orderPositionIndex].logEntries = [
        {
          status: 'visual_inspection_carrier_accepted',
          acceptanceCriteria: {
            acceptLabeling: true,
            acceptTransportability: true,
            comment: '',
          } as VisualInspectionCarrierCriteria,
          date: new Date().getTime(),
        } as LogEntry,
      ];
    } else {
      this.orders[orderIndex].orderPositions[orderPositionIndex].logEntries = [
        {
          status: 'visual_inspection_carrier_denied',
          acceptanceCriteria: {
            acceptLabeling: false,
            acceptTransportability: false,
            comment: '',
          } as VisualInspectionCarrierCriteria,
          date: new Date().getTime(),
        } as LogEntry,
      ];
    }
    this.ordersChange.emit(this.orders);
  }
}
