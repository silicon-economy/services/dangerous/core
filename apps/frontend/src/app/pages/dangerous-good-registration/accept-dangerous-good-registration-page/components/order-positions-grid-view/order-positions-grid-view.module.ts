/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  OrderPositionsGridViewComponent
} from '@base/src/app/pages/dangerous-good-registration/accept-dangerous-good-registration-page/components/order-positions-grid-view/order-positions-grid-view.component';
import { MatIconModule } from '@angular/material/icon';
import {
  OrderPositionCardModule
} from '@base/src/app/pages/dangerous-good-registration/accept-dangerous-good-registration-page/components/order-positions-grid-view/components/order-position-card/order-position-card.module';
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [OrderPositionsGridViewComponent],
  imports: [CommonModule, MatIconModule, OrderPositionCardModule, TranslateModule],
  exports: [OrderPositionsGridViewComponent],
})
export class OrderPositionsGridViewModule {}
