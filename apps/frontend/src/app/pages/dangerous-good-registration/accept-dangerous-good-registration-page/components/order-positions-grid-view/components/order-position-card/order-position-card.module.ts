/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  OrderPositionCardComponent
} from '@base/src/app/pages/dangerous-good-registration/accept-dangerous-good-registration-page/components/order-positions-grid-view/components/order-position-card/order-position-card.component';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { OrderPositionViewModule } from "@shared/components/order-position-view/order-position-view.module";


@NgModule({
  declarations: [OrderPositionCardComponent],
  imports: [CommonModule, MatCardModule, MatGridListModule, OrderPositionViewModule],
  exports: [OrderPositionCardComponent],
})
export class OrderPositionCardModule {}
