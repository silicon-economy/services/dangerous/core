/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderPositionCardComponent } from './order-position-card.component';

describe('OrderPositionCardComponent', () => {
  let component: OrderPositionCardComponent;
  let fixture: ComponentFixture<OrderPositionCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OrderPositionCardComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderPositionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update `selected` and emit event', () => {
    jest.spyOn(component.changeSelectionEmitter, 'emit');
    component.selected = false;
    component.onSelected();
    expect(component.changeSelectionEmitter.emit).toHaveBeenCalledWith(true);
  });
});
