/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';

@Component({
  selector: 'app-order-position-card',
  templateUrl: './order-position-card.component.html',
  styleUrls: ['./order-position-card.component.scss'],
})
export class OrderPositionCardComponent {
  @Input()
  orderPosition: OrderPositionWithId;
  selected = false;
  @Output() changeSelectionEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  tiles: Tile[] = [
    { text: 'One', cols: 2, rows: 2 },
    { text: 'Two', cols: 4, rows: 1 },
    { text: 'Three', cols: 4, rows: 1 },
  ];

  /**
   * Switch selected status (boolean)
   */
  onSelected(): void {
    this.selected = !this.selected;
    this.changeSelectionEmitter.emit(this.selected);
  }
}

export interface Tile {
  rows: number;
  text: string;
  cols: number;
}
