/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewTransportDocumentDetailsViewComponent } from './preview-transport-document-details-view.component';

describe('PreviewTransportDocumentDetailsViewComponent', () => {
  let component: PreviewTransportDocumentDetailsViewComponent;
  let fixture: ComponentFixture<PreviewTransportDocumentDetailsViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PreviewTransportDocumentDetailsViewComponent],
    });
    fixture = TestBed.createComponent(PreviewTransportDocumentDetailsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
