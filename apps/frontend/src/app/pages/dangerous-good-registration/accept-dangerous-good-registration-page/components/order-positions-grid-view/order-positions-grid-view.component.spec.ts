/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderPositionsGridViewComponent } from './order-positions-grid-view.component';
import { mockOrderWithId } from "@core/api-interfaces/lib/mocks/order/order.mock";

describe('OrderPositionsGridViewComponent', () => {
  let component: OrderPositionsGridViewComponent;
  let fixture: ComponentFixture<OrderPositionsGridViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OrderPositionsGridViewComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderPositionsGridViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should complement selected orders with check attribute (true)', (): void => {
    const orderIndex = 0;
    const orderPositionIndex = 0;
    const emitSpy = jest.spyOn(component.ordersChange, 'emit');

    component.orders = [mockOrderWithId];
    component.onSelectChange(true, orderIndex, orderPositionIndex);

    expect(component.orders[orderIndex].orderPositions[orderPositionIndex].logEntries.length).toBe(1);
    expect(component.orders[orderIndex].orderPositions[orderPositionIndex].logEntries[0].status).toBe('visual_inspection_carrier_accepted');
    expect(emitSpy).toHaveBeenCalledWith([mockOrderWithId]);
  });

  it('should complement selected orders with check attribute (false)', (): void => {
    const orderIndex = 0;
    const orderPositionIndex = 0;
    const emitSpy = jest.spyOn(component.ordersChange, 'emit');

    component.orders = [mockOrderWithId];
    component.onSelectChange(false, orderIndex, orderPositionIndex);

    expect(component.orders[orderIndex].orderPositions[orderPositionIndex].logEntries.length).toBe(1);
    expect(component.orders[orderIndex].orderPositions[orderPositionIndex].logEntries[0].status).toBe('visual_inspection_carrier_denied');
    expect(emitSpy).toHaveBeenCalledWith([mockOrderWithId]);
  });
});
