/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';

@Component({
  selector: 'app-preview-transport-document-details-view [previewTransportDocument]',
  templateUrl: './preview-transport-document-details-view.component.html'
})
export class PreviewTransportDocumentDetailsViewComponent {
  /**
   * The preview transport document. It can either be created from the base of a dangerous good registration
   * to which carrierInformation has been added, or from the base of an already existing transport-document, which
   * will get the newly acceptedOrders merged into itself.
   */
  @Input() previewTransportDocument: SaveGoodsDataDto;

  /**
   * This variable is holding the acceptedOrders. It is only used when importing the accepted dangerous good registrations
   * into an already existing transport-document. This way the newly added orders can be shown seperated from the ones
   * already contained in the transport-document.
   */
  @Input() acceptedOrders: OrderWithId[];
}
