/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { BreakpointObserver } from '@angular/cdk/layout';
import { StepperOrientation } from '@angular/cdk/stepper';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import {
  SaveDangerousGoodRegistrationDto
} from "@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto";
import {
  AcceptDangerousGoodRegistrationService
} from "@base/src/app/pages/dangerous-good-registration/accept-dangerous-good-registration-page/services/accept-dangerous-good-registration.service";

@Component({
  selector: 'app-accept-dangerous-goods-registration-page',
  templateUrl: './accept-dangerous-goods-registration-page.component.html'
})
export class AcceptDangerousGoodsRegistrationPageComponent implements OnInit {
  public stepperOrientation: Observable<StepperOrientation>;
  public carrierInformationIsValid: boolean;
  public visualInspectionValidity: boolean[] = [false, false]

  /**
   * Determines whether the step for the visual inspection of the accepted orders was completed validly
   *
   * @returns boolean
   */
  public visualInspectionIsValid():boolean{
    return this.visualInspectionValidity.every(value => value===true)
  }

  /**
   * Determines whether the step for the selection of the accepted orders was completed validly
   *
   * @returns boolean
   */
  public orderPositionSelectionIsValid():boolean{
    return this.acceptDangerousGoodRegistrationService.getAcceptedOrders().some(order => order.orderPositions.length > 0)
  }

  constructor(
    breakpointObserver: BreakpointObserver,
    private activatedRoute: ActivatedRoute,
    public acceptDangerousGoodRegistrationService: AcceptDangerousGoodRegistrationService
  ) {
    this.stepperOrientation = breakpointObserver
      .observe('(min-width: 1025px)')
      .pipe(map(({ matches }) => (matches ? 'horizontal' : 'vertical')));
  }

  ngOnInit(): void {
    this.activatedRoute.data?.subscribe((data): void => {
      this.acceptDangerousGoodRegistrationService.setDangerousGoodRegistration(<SaveDangerousGoodRegistrationDto>data.dangerousGoodRegistration)
      this.acceptDangerousGoodRegistrationService.setImportedTransportDocument(<SaveGoodsDataDto>data.transportDocument)
    });
  }
}
