/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { AcceptDangerousGoodsRegistrationPageComponent } from './accept-dangerous-goods-registration-page.component';
import { MatStepperModule } from '@angular/material/stepper';
import { CriterionCheckComponent } from '@shared/components/criterion-check/criterion-check.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import { MapComponent } from '@maps/components/map/map.component';

import { MockComponent } from 'ng-mocks';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TitleBarComponent } from "@base/src/app/layouts/title-bar/title-bar.component";
import {
  OrderPositionsGridViewComponent
} from "@base/src/app/pages/dangerous-good-registration/accept-dangerous-good-registration-page/components/order-positions-grid-view/order-positions-grid-view.component";
import { CompanyViewComponent } from "@base/src/app/layouts/company-view/company-view.component";
import {
  FreightBaseDataViewComponent
} from "@base/src/app/layouts/freight-base-data-view/freight-base-data-view.component";
import { TransportDocumentService } from "@shared/services/transport-document-service/transport-document.service";
import { FeedbackDialogService } from "@shared/services/feedback-dialog-service/feedback-dialog.service";
import { NotificationService } from "@shared/services/notification-service/notification.service";
import { UserLoginService } from "@core/services/user-service/user-login.service";
import { UserAuthenticationService } from "@core/services/user-service/user-authentication.service";
import { TransportDocumentHttpService } from "@shared/httpRequests/transport-document-http.service";
import { DangerousGoodsGatewayHttpService } from "@shared/httpRequests/dangerous-goods-gateway-http.service";
import { TranslateModule } from "@ngx-translate/core";
import {
  AcceptDangerousGoodRegistrationService
} from "@base/src/app/pages/dangerous-good-registration/accept-dangerous-good-registration-page/services/accept-dangerous-good-registration.service";
import { MatGridListModule } from "@angular/material/grid-list";


describe('AcceptDangerousGoodsRegistrationPageComponent', () => {
  let component: AcceptDangerousGoodsRegistrationPageComponent;
  let fixture: ComponentFixture<AcceptDangerousGoodsRegistrationPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        MockComponent(MapComponent),
        AcceptDangerousGoodsRegistrationPageComponent,
        TitleBarComponent,
        OrderPositionsGridViewComponent,
        CriterionCheckComponent,
        CompanyViewComponent,
        FreightBaseDataViewComponent,
      ],
      imports: [
        ReactiveFormsModule,
        MatDialogModule,
        HttpClientTestingModule,
        NoopAnimationsModule,
        MatStepperModule,
        MatExpansionModule,
        MatFormFieldModule,
        FormsModule,
        MatInputModule,
        MatIconModule,
        MatCardModule,
        MatSlideToggleModule,
        TranslateModule.forRoot(),
        MatGridListModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useValue: {} },
        HttpClient,
        MatSnackBar,
        FormBuilder,
        { provide: MAT_DIALOG_DATA, useValue: {} },
        TransportDocumentService,
        FeedbackDialogService,
        NotificationService,
        UserLoginService,
        UserAuthenticationService,
        TransportDocumentHttpService,
        DangerousGoodsGatewayHttpService,
        AcceptDangerousGoodRegistrationService
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    registerLocaleData(localeDe, 'de-DE', localeDeExtra);
    fixture = TestBed.createComponent(AcceptDangerousGoodsRegistrationPageComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.orderPositionSelectionIsValid()).toEqual(false)
    expect(component.visualInspectionIsValid()).toEqual(false)
  });
});
