/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  AcceptDangerousGoodsRegistrationPageComponent
} from '@base/src/app/pages/dangerous-good-registration/accept-dangerous-good-registration-page/accept-dangerous-goods-registration-page.component';
import { transportDocumentResolver } from '@core/resolvers/transport-document-resolver/transport-document.resolver';

const routes: Routes = [
  {
    path: '',
    component: AcceptDangerousGoodsRegistrationPageComponent,
  },
  {
    path: ':id',
    component: AcceptDangerousGoodsRegistrationPageComponent,
    resolve: {
      transportDocument: transportDocumentResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcceptDangerousGoodRegistrationPageRoutingModule {}
