/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import {
  DangerousGoodRegistrationListComponent
} from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/components/dangerous-good-registration-list/dangerous-good-registration-list.component';

@Component({
  selector: 'app-dangerous-good-registration-list-page',
  templateUrl: './dangerous-good-registration-list-page.component.html',
})
export class DangerousGoodRegistrationListPageComponent implements OnInit {
  private _dangerousGoodRegistrations: MatTableDataSource<SaveDangerousGoodRegistrationDto>;
  @ViewChild('listComponent') listComponent: DangerousGoodRegistrationListComponent;

  constructor(
    private readonly dangerousGoodRegistrationHttpService: DangerousGoodRegistrationHttpService,
    private activatedRoute: ActivatedRoute
  ) {}

  get dangerousGoodRegistrations(): MatTableDataSource<SaveDangerousGoodRegistrationDto> {
    return this._dangerousGoodRegistrations;
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: { _dangerousGoodRegistrations: SaveDangerousGoodRegistrationDto[] }) => {
      this._dangerousGoodRegistrations = new MatTableDataSource(data._dangerousGoodRegistrations.slice());
    });
  }

  /**
   * Applies a filter to the list component based on the input event.
   *
   * @param event - The input event that triggered the filter.
   */
  applyFilter(event: Event): void {
    const filterString = (event.target as HTMLInputElement).value;
    this.listComponent.filterByString(filterString);
    this.listComponent.sortData();
  }
}
