/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { DangerousGoodRegistrationListPageRoutingModule } from './dangerous-good-registration-list-page-routing.module';
import { DangerousGoodRegistrationListPageComponent } from './dangerous-good-registration-list-page.component';
import { TransportDocumentListModule } from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import { TranslateModule } from '@ngx-translate/core';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import { DangerousGoodRegistrationListModule } from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/components/dangerous-good-registration-list/dangerous-good-registration-list.module';
import { DangerousGoodRegistrationListFilterService } from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/components/dangerous-good-registration-list/dangerous-good-registration-list-filter-service/dangerous-good-registration-list-filter.service';
import { DangerousGoodRegistrationService } from '@shared/services/dangerous-good-registration-service/dangerous-good-registration.service';

@NgModule({
  declarations: [DangerousGoodRegistrationListPageComponent],
  imports: [
    CommonModule,
    DangerousGoodRegistrationListPageRoutingModule,
    TransportDocumentListModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTabsModule,
    TitleBarModule,
    TranslateModule,
    DangerousGoodRegistrationListModule,
  ],
  exports: [DangerousGoodRegistrationListPageComponent],
  providers: [
    DangerousGoodRegistrationService,
    DangerousGoodRegistrationHttpService,
    DangerousGoodRegistrationListFilterService,
    DatePipe,
  ],
})
export class DangerousGoodRegistrationListPageModule {}
