/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DangerousGoodRegistrationListPageComponent } from './dangerous-good-registration-list-page.component';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import {
  DangerousGoodRegistrationListPageRoutingModule
} from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/dangerous-good-registration-list-page-routing.module';
import {
  TransportDocumentListModule
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { TitleBarModule } from '@base/src/app/layouts/title-bar/title-bar.module';
import {
  DangerousGoodRegistrationListModule
} from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/components/dangerous-good-registration-list/dangerous-good-registration-list.module';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import {
  DangerousGoodRegistrationListFilterService
} from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/components/dangerous-good-registration-list/dangerous-good-registration-list-filter-service/dangerous-good-registration-list-filter.service';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { Address, Company, ContactPerson } from '@core/api-interfaces/lib/dtos/frontend';
import { FreightIdsIncluded } from '@core/api-interfaces/lib/dtos/frontend/transport-document/freight-ids-included';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';

describe('DangerousGoodRegistrationListPageComponent', () => {
  let component: DangerousGoodRegistrationListPageComponent;
  let fixture: ComponentFixture<DangerousGoodRegistrationListPageComponent>;

  const saveDangerousGoodRegistrationsDtoMock: SaveDangerousGoodRegistrationDto[] = [
    new SaveDangerousGoodRegistrationDto(
      'DGR000001-20220310',
      'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
      new Company(
        'Chemical Industries Germany Inc.',
        new Address('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
        new ContactPerson('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
      ),
      new FreightIdsIncluded(
        [
          new OrderWithId(
            new Company(
              'Chemical Industries Germany Inc.',
              new Address('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
              new ContactPerson('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
            ),
            [
              new OrderPositionWithId(
                undefined,
                '',
                '',
                0,
                undefined,
                0,
                false,
                0,
                0,
                0,
                undefined,
                undefined,
                undefined
              ),
            ],
            ''
          ),
        ],
        'Beförderung ohne Freistellung nach ADR 1.1.3.6',
        '"Bitte Beförderungseinheit nach letzter Entladung reinigen."',
        780
      ),
      1646917164906,
      1646917164906
    ),
    new SaveDangerousGoodRegistrationDto(
      'DGR000001-20220310',
      'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
      new Company(
        'Chemical Industries Germany Inc.',
        new Address('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
        new ContactPerson('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
      ),
      new FreightIdsIncluded(
        [
          new OrderWithId(
            new Company(
              'Chemical Industries Germany Inc.',
              new Address('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
              new ContactPerson('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
            ),
            [
              new OrderPositionWithId(
                undefined,
                '',
                '',
                0,
                undefined,
                0,
                false,
                0,
                0,
                0,
                undefined,
                undefined,
                undefined
              ),
            ],
            ''
          ),
        ],
        'Beförderung ohne Freistellung nach ADR 1.1.3.6',
        '"Bitte Beförderungseinheit nach letzter Entladung reinigen."',
        780
      ),
      1646917164906,
      1646917164906
    ),
  ];

  const route = {
    data: of({
      _dangerousGoodRegistrations: saveDangerousGoodRegistrationsDtoMock,
    }),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        DangerousGoodRegistrationListPageRoutingModule,
        TransportDocumentListModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatTabsModule,
        TitleBarModule,
        TranslateModule,
        DangerousGoodRegistrationListModule,
        NoopAnimationsModule,
      ],
      declarations: [DangerousGoodRegistrationListPageComponent],
      providers: [
        DangerousGoodRegistrationHttpService,
        UserLoginService,
        UserAuthenticationService,
        DangerousGoodRegistrationListFilterService,
        {
          provide: ActivatedRoute,
          useValue: route,
        },
      ],
    });
    fixture = TestBed.createComponent(DangerousGoodRegistrationListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set dangerousGoodRegistrations', () => {
    component.ngOnInit();
    const matTableDataSourceMock = saveDangerousGoodRegistrationsDtoMock.slice();
    expect(component.dangerousGoodRegistrations.data).toStrictEqual(matTableDataSourceMock);
  });

  it('should apply filter', () => {
    const listComponentFilterByStringSpy = jest.spyOn(component.listComponent, 'filterByString');
    const listComponentSortDataSpy = jest.spyOn(component.listComponent, 'sortData');

    const eventMock: Event = <Event>(<never>{
      target: {
        value: 'foo',
      },
    });
    component.applyFilter(eventMock);
    expect(listComponentFilterByStringSpy).toHaveBeenCalledWith('foo');
    expect(listComponentSortDataSpy).toHaveBeenCalled();
  });
});
