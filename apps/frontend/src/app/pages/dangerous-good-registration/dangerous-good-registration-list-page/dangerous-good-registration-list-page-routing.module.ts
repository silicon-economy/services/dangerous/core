/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  DangerousGoodRegistrationListPageComponent
} from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/dangerous-good-registration-list-page.component';
import {
  dangerousGoodRegistrationListResolver
} from '@core/resolvers/dangerous-good-registration-list-resolver/transport-document-list.resolver';

const routes: Routes = [
  {
    path: '',
    resolve: { _dangerousGoodRegistrations: dangerousGoodRegistrationListResolver },
    component: DangerousGoodRegistrationListPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DangerousGoodRegistrationListPageRoutingModule {}
