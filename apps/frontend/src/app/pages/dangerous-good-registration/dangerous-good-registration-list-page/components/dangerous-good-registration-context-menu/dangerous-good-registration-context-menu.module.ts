/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import {
  DangerousGoodRegistrationContextMenuComponent
} from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/components/dangerous-good-registration-context-menu/dangerous-good-registration-context-menu.component';
import {
  DisableDocumentDialogModule
} from '@base/src/app/dialogs/disable-document-dialog/disable-document-dialog.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterLink } from '@angular/router';
import {
  DocumentQrCodeDialogModule
} from '@base/src/app/dialogs/document-qr-code-dialog/document-qr-code-dialog.module';

@NgModule({
  declarations: [DangerousGoodRegistrationContextMenuComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    DisableDocumentDialogModule,
    DocumentQrCodeDialogModule,
    TranslateModule,
    RouterLink,
  ],
  exports: [DangerousGoodRegistrationContextMenuComponent],
  providers: [TransportDocumentHttpService],
})
export class DangerousGoodRegistrationContextMenuModule {}
