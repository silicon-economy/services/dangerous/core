/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DangerousGoodRegistrationListComponent } from './dangerous-good-registration-list.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ListSortService } from '@shared/services/list-sort-service/list-sort.service';
import { CompareService } from '@shared/services/compare-service/compare.service';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserAuthenticationService } from '@core/services/user-service/user-authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  DangerousGoodRegistrationListFilterService
} from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/components/dangerous-good-registration-list/dangerous-good-registration-list-filter-service/dangerous-good-registration-list-filter.service';
import {
  TransportDocumentListFilterModule
} from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-filter-service/transport-document-list-filter.module';
import { TranslateModule } from '@ngx-translate/core';

describe('ListComponent', () => {
  let component: DangerousGoodRegistrationListComponent;
  let fixture: ComponentFixture<DangerousGoodRegistrationListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DangerousGoodRegistrationListComponent],
      imports: [MatDialogModule, HttpClientTestingModule, TransportDocumentListFilterModule, TranslateModule.forRoot()],
      providers: [
        ListSortService,
        CompareService,
        UserLoginService,
        UserAuthenticationService,
        DangerousGoodRegistrationListFilterService,
      ],
    });
    fixture = TestBed.createComponent(DangerousGoodRegistrationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
