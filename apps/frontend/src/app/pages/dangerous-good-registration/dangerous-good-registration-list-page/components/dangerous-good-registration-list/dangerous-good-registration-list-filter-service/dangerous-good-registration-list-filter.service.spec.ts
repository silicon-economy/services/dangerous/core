/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { DangerousGoodRegistrationListFilterService } from './dangerous-good-registration-list-filter.service';

describe('ListFilterService', () => {
  let service: DangerousGoodRegistrationListFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [DangerousGoodRegistrationListFilterService] });
    service = TestBed.inject(DangerousGoodRegistrationListFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
