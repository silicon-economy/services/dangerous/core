/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

// noinspection IdentifierGrammar

/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';

@Injectable()
export class DangerousGoodRegistrationListFilterService {
  /**
   * Applies a filter to the data based on the provided filter string.
   *
   * @param filterString - The string to filter the data by.
   * @param entries -
   * @returns SaveGoodsDataDto[]
   */
  public applyFilterByString(
    filterString: string,
    entries: SaveDangerousGoodRegistrationDto[]
  ): SaveDangerousGoodRegistrationDto[] {
    const lowercaseFilterString = filterString.toLowerCase();

    return entries.filter((saveGoodsDataDto: SaveDangerousGoodRegistrationDto) => {
      return (
        this.filterById(saveGoodsDataDto, lowercaseFilterString) ||
        this.filterByConsigneeNames(saveGoodsDataDto, lowercaseFilterString)
      );
    });
  }

  /**
   * Filters the data by the ID property of the SaveDangerousGoodRegistrationDto object.
   *
   * @param saveDangerousGoodRegistrationDto - The SaveDangerousGoodRegistrationDto object to filter.
   * @param filterString - The string to filter by.
   * @returns boolean - True if the filter matches, false otherwise.
   */
  private filterById(
    saveDangerousGoodRegistrationDto: SaveDangerousGoodRegistrationDto,
    filterString: string
  ): boolean {
    return saveDangerousGoodRegistrationDto.id.toLowerCase().includes(filterString);
  }

  /**
   * Filters the data by the consignee names property of the SaveDangerousGoodRegistrationDto object.
   *
   * @param saveDangerousGoodRegistrationDto - The SaveDangerousGoodRegistrationDto object to filter.
   * @param filterString - The string to filter by.
   * @returns boolean - True if the filter matches, false otherwise.
   */
  private filterByConsigneeNames(
    saveDangerousGoodRegistrationDto: SaveDangerousGoodRegistrationDto,
    filterString: string
  ): boolean {
    return saveDangerousGoodRegistrationDto.freight.orders.some((order: OrderWithId) => {
      return order.consignee.name.toLowerCase().includes(filterString);
    });
  }
}
