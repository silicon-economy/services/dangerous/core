/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserServiceModule } from '@core/services/user-service/user-service.module';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { Address, Company, ContactPerson } from '@core/api-interfaces/lib/dtos/frontend';
import { FreightIdsIncluded } from '@core/api-interfaces/lib/dtos/frontend/transport-document/freight-ids-included';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-position-with-id';
import {
  DangerousGoodRegistrationContextMenuComponent
} from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/components/dangerous-good-registration-context-menu/dangerous-good-registration-context-menu.component';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import {
  DisableDocumentDialogComponent
} from '@base/src/app/dialogs/disable-document-dialog/disable-document-dialog.component';
import {
  DocumentQrCodeDialogComponent
} from '@base/src/app/dialogs/document-qr-code-dialog/document-qr-code-dialog.component';
import { of } from 'rxjs';
import {
  DangerousGoodRegistrationService
} from '@shared/services/dangerous-good-registration-service/dangerous-good-registration.service';
import { DangerousGoodRegistrationHttpService } from '@shared/httpRequests/dangerous-good-registration-http.service';
import { FeedbackDialogService } from '@shared/services/feedback-dialog-service/feedback-dialog.service';
import { NotificationService } from '@shared/services/notification-service/notification.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('DangerousGoodRegistrationContextMenuComponent', () => {
  let component: DangerousGoodRegistrationContextMenuComponent;
  let fixture: ComponentFixture<DangerousGoodRegistrationContextMenuComponent>;

  const saveDangerousGoodRegistrationsDtoMock: SaveDangerousGoodRegistrationDto = new SaveDangerousGoodRegistrationDto(
    'DGR000001-20220310',
    'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
    new Company(
      'Chemical Industries Germany Inc.',
      new Address('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
      new ContactPerson('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
    ),
    new FreightIdsIncluded(
      [
        new OrderWithId(
          new Company(
            'Chemical Industries Germany Inc.',
            new Address('Ressestraße', '50', '45894', 'Gelsenkirchen', 'Deutschland'),
            new ContactPerson('Fritz Hazard', '0049 123 456 789 0', 'fritz.hazard@ChemIndInc.de', 'OE 3 II')
          ),
          [
            new OrderPositionWithId(
              undefined,
              '',
              '',
              0,
              undefined,
              0,
              false,
              0,
              0,
              0,
              undefined,
              undefined,
              undefined
            ),
          ],
          ''
        ),
      ],
      'Beförderung ohne Freistellung nach ADR 1.1.3.6',
      '"Bitte Beförderungseinheit nach letzter Entladung reinigen."',
      780
    ),
    1646917164906,
    1646917164906
  );

  const transportDocumentHttpServiceMock = {
    getPackagingLabels: jest.fn(() => {
      return of(new Blob());
    }),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterModule.forRoot([]),
        HttpClientTestingModule,
        UserServiceModule,
        MatIconModule,
        MatMenuModule,
        MatDialogModule,
        TranslateModule.forRoot(),
        MatSnackBarModule,
      ],
      providers: [
        {
          provide: TransportDocumentHttpService,
          useValue: transportDocumentHttpServiceMock,
        },
        DangerousGoodRegistrationService,
        DangerousGoodRegistrationHttpService,
        FeedbackDialogService,
        NotificationService,
      ],
      declarations: [DangerousGoodRegistrationContextMenuComponent],
    });
    fixture = TestBed.createComponent(DangerousGoodRegistrationContextMenuComponent);
    component = fixture.componentInstance;
    component.saveDangerousGoodRegistrationDto = saveDangerousGoodRegistrationsDtoMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open DisableDangerousGoodRegistrationDialogComponent dialog', () => {
    const matDialogMock = jest.spyOn(MatDialog.prototype, 'open').mockReturnValue({
      afterClosed: () => of(true),
    } as MatDialogRef<typeof component>);
    component.onDisableDangerousGoodRegistration(saveDangerousGoodRegistrationsDtoMock.id);
    expect(matDialogMock).toHaveBeenCalledWith(DisableDocumentDialogComponent, {
      data: saveDangerousGoodRegistrationsDtoMock.id,
    });
  });

  it('should open DangerousGoodRegistrationQrCodeDialogComponent dialog', () => {
    const matDialogMock = jest.spyOn(MatDialog.prototype, 'open').mockImplementation();
    component.onDangerousGoodRegistrationQrCode();
    expect(matDialogMock).toHaveBeenCalledWith(DocumentQrCodeDialogComponent, {
      data: { dangerousGoodRegistrationId: saveDangerousGoodRegistrationsDtoMock.id },
    });
  });

  it('should open a new window with packaging labels as PDF', () => {
    window.open = jest.fn();
    URL.createObjectURL = jest.fn(() => 'foobar');
    component.getPackagingLabelsAsPdf();
    expect(URL.createObjectURL).toHaveBeenCalledWith(new Blob());
    expect(window.open).toHaveBeenCalledWith('foobar');
  });
});
