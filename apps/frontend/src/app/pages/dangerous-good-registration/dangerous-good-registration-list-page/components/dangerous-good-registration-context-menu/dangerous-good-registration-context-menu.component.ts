/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import { TransportDocumentHttpService } from '@shared/httpRequests/transport-document-http.service';
import { MatDialog } from '@angular/material/dialog';
import {
  DocumentQrCodeDialogComponent
} from '@base/src/app/dialogs/document-qr-code-dialog/document-qr-code-dialog.component';
import {
  DangerousGoodRegistrationService
} from '@shared/services/dangerous-good-registration-service/dangerous-good-registration.service';

@Component({
  selector: 'app-dangerous-good-registration-context-menu[saveDangerousGoodRegistrationDto]',
  templateUrl: './dangerous-good-registration-context-menu.component.html',
})
export class DangerousGoodRegistrationContextMenuComponent {
  @Input() saveDangerousGoodRegistrationDto!: SaveDangerousGoodRegistrationDto;

  constructor(
    public transportDocumentHttpService: TransportDocumentHttpService,
    private readonly dangerousGoodRegistrationService: DangerousGoodRegistrationService,
    private readonly matDialog: MatDialog
  ) {}

  /**
   * Generates packaging labels as a PDF and opens it in a new tab.
   */
  public getPackagingLabelsAsPdf(): void {
    this.transportDocumentHttpService
      .getPackagingLabels(this.saveDangerousGoodRegistrationDto.id)
      .subscribe((packagingLabelsPdf: Blob) => {
        const pdfBlob = new Blob([packagingLabelsPdf], { type: 'application/pdf' });
        const pdfBlobUrl = URL.createObjectURL(pdfBlob);
        window.open(pdfBlobUrl);
      });
  }

  /**
   * Opens a dialogue to display the QR code for the dangerous good registration.
   */
  public onDangerousGoodRegistrationQrCode(): void {
    this.matDialog.open(DocumentQrCodeDialogComponent, {
      data: { dangerousGoodRegistrationId: this.saveDangerousGoodRegistrationDto.id },
    });
  }

  /**
   * Opens a dialogue to disable the dangerous good registration.
   *
   * @param dangerousGoodRegistrationId - Dangerous good registration id.
   */
  public onDisableDangerousGoodRegistration(dangerousGoodRegistrationId: string): void {
    this.dangerousGoodRegistrationService.onDisableDangerousGoodRegistrationButtonClicked(dangerousGoodRegistrationId);
  }
}
