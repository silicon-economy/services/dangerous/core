/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { TransportDocumentContextMenuModule } from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-context-menu/transport-document-context-menu.module';
import { MatMenuModule } from '@angular/material/menu';
import { ConsigneeListDialogModule } from '@base/src/app/dialogs/consignee-list-dialog/consignee-list-dialog.module';
import { MatDividerModule } from '@angular/material/divider';
import { CompareModule } from '@shared/services/compare-service/compare.module';
import { TransportDocumentListFilterModule } from '@base/src/app/pages/transport-document/transport-document-list-page/components/transport-document-list/transport-document-list-filter-service/transport-document-list-filter.module';
import { ListSortModule } from '@shared/services/list-sort-service/list-sort.module';
import { CommentDialogModule } from '@base/src/app/dialogs/comment-dialog/comment-dialog.module';
import { MatBadgeModule } from '@angular/material/badge';
import { DangerousGoodRegistrationListComponent } from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/components/dangerous-good-registration-list/dangerous-good-registration-list.component';
import { DangerousGoodRegistrationContextMenuModule } from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/components/dangerous-good-registration-context-menu/dangerous-good-registration-context-menu.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterLink } from '@angular/router';
import { LocalizedDatePipe } from '@core/pipes/localized-date.pipe';

@NgModule({
  declarations: [DangerousGoodRegistrationListComponent],
  imports: [
    CommonModule,
    MatSortModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    TransportDocumentContextMenuModule,
    MatMenuModule,
    ConsigneeListDialogModule,
    MatDividerModule,
    CompareModule,
    TransportDocumentListFilterModule,
    ListSortModule,
    CommentDialogModule,
    MatBadgeModule,
    DangerousGoodRegistrationContextMenuModule,
    TranslateModule,
    RouterLink,
    LocalizedDatePipe,
  ],
  exports: [DangerousGoodRegistrationListComponent],
})
export class DangerousGoodRegistrationListModule {}
