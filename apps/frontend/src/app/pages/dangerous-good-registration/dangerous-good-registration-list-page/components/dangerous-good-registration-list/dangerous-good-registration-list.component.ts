/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/frontend/transport-document/save-goods-data.dto';
import { MatDialog } from '@angular/material/dialog';
import {
  ConsigneeListDialogComponent
} from '@base/src/app/dialogs/consignee-list-dialog/consignee-list-dialog.component';
import { Company } from '@core/api-interfaces/lib/dtos/frontend';
import { OrderWithId } from '@core/api-interfaces/lib/dtos/frontend/transport-document/order-with-id';
import { Sort } from '@angular/material/sort';
import { ListSortService } from '@shared/services/list-sort-service/list-sort.service';
import { UserLoginService } from '@core/services/user-service/user-login.service';
import { UserRole } from '@core/api-interfaces/lib/dtos/mini-auth-server/user-role.enum';
import {
  SaveDangerousGoodRegistrationDto
} from '@core/api-interfaces/lib/dtos/frontend/dangerous-good-registration/save-dangerous-good-registration.dto';
import {
  DangerousGoodRegistrationListFilterService
} from '@base/src/app/pages/dangerous-good-registration/dangerous-good-registration-list-page/components/dangerous-good-registration-list/dangerous-good-registration-list-filter-service/dangerous-good-registration-list-filter.service';

@Component({
  selector: 'app-dangerous-good-registration-list[entries]',
  templateUrl: './dangerous-good-registration-list.component.html',
})
export class DangerousGoodRegistrationListComponent implements OnInit {
  @Input() entries: SaveDangerousGoodRegistrationDto[] = [];

  columnsToDisplay: string[] = ['menu', 'id', 'consignee', 'created_date', 'last_update'];

  public lastSort: Sort = {
    active: 'last_update',
    direction: 'desc',
  };
  userRoles: UserRole[] = [];
  protected sortedData: SaveDangerousGoodRegistrationDto[] = [];
  private dataFilteredByString: SaveDangerousGoodRegistrationDto[] = [];

  constructor(
    private readonly matDialog: MatDialog,
    readonly listSortService: ListSortService,
    private readonly listFilterService: DangerousGoodRegistrationListFilterService,
    private readonly userService: UserLoginService,
    private readonly cdRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.sortedData = this.dataFilteredByString = this.entries;
    this.userService.userRoles.subscribe((userRoles: UserRole[]) => {
      this.userRoles = userRoles;
    });
  }

  /**
   * Trigger to display the consignee for a given document.
   *
   * @param saveGoodsDataDto - The document to which the consignees will be displayed.
   */
  public onDisplayConsignees(saveGoodsDataDto: SaveGoodsDataDto): void {
    this.matDialog.open(ConsigneeListDialogComponent, {
      data: this.getConsignees(saveGoodsDataDto),
    });
  }

  /**
   * Gets the consignees of all orders. (In case of a dangerous good registration,
   * there is always only a single order and consignee present.)
   *
   * @param saveGoodsDataDto - The dangerous good document
   * @returns Array of companies
   */
  getConsignees(saveGoodsDataDto: SaveGoodsDataDto): Company[] {
    return saveGoodsDataDto.freight.orders.map((order: OrderWithId) => order.consignee);
  }

  /**
   * Applies a filter to the data based on the provided filter string.
   *
   * @param filterString - The string to filter the data by.
   */
  public filterByString(filterString: string): void {
    this.dataFilteredByString = this.listFilterService.applyFilterByString(filterString, this.entries);
  }

  /**
   * Sort the Data via MatSort.
   *
   * @param sortState - The sort object containing information like active columns and direction.
   */
  sortData(sortState?: Sort): void {
    if (sortState) {
      this.lastSort = sortState;
    }

    this.sortedData = this.listSortService.sortDangerousGoodRegistrations(this.lastSort, this.dataFilteredByString);
  }

  /**
   * Delete entry from list by given index.
   *
   * @param id - Entry id
   */
  public deleteEntryFromList(id: string): void {
    const index = this.sortedData.findIndex((entry: SaveDangerousGoodRegistrationDto) => entry.id === id);

    if (index != -1) {
      const entriesTemp: SaveDangerousGoodRegistrationDto[] = this.sortedData;
      entriesTemp.splice(index, 1);
      this.sortedData = [...entriesTemp];
    }
  }
}
