/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import 'jest-preset-angular/setup-jest';
import '@angular/localize/init';
import * as process from 'process';

window['process'] = process;

// global is undefined
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-explicit-any
(window as any)['global'] = window;

// eslint-disable-next-line @typescript-eslint/no-var-requires
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-var-requires, @typescript-eslint/no-explicit-any
(window as any).global.Buffer = (window as any).global.Buffer || require('buffer').Buffer;

afterEach(() => {
  if (global.gc) global.gc();
});
