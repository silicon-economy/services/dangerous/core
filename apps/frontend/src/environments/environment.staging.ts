/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export const environment = {
  production: false,
  VERSION: '0.9.0',
  EXTERNAL_ADDRESSES: {
    BLOCKCHAIN_EUROPE: 'https://blockchain-europe.nrw/',
    SILICON_ECONOMY: 'https://www.silicon-economy.com/',
  },
  ROUTER_LINKS: {
    TRANSPORT_DOCUMENT_LIST: '/transport-documents/',
  },
  ENDPOINTS: {
    DANGEROUS_GOODS_GATEWAY: {
      URL: 'https://dangerous-goods-gateway-staging.apps.sele.iml.fraunhofer.de',
      HTTP_HEADERS: {
        TYPE_OF_ACTION: 'typeOfAction',
      },
      HTTP_HEADER_VALUES: {
        DISABLE_DANGEROUS_GOOD_REGISTRATION: 'disableRegistration',
      },
    },
    TRANSPORT_DOCUMENT_GATEWAY: {
      BASEPATH: '',
      URL: 'https://transport-document-gateway-staging.apps.sele.iml.fraunhofer.de',
      TRANSPORT_DOCUMENT_PREFIX: '/transportDocument',
      DANGEROUS_GOOD_REGISTRATION_PREFIX: '/dangerousGoodRegistration',
      MEDIA_MANAGEMENT_PREFIX: '/mediaManagement',
      TRANSPORT_DOCUMENT_ID_BY_ORDER_POSITION_ID: '/transportDocumentIdByOrderPositionId',
    },
    MINI_AUTH_SERVER: {
      URL: 'https://mini-auth-server-staging.apps.sele.iml.fraunhofer.de',
      USER_PREFIX: '/user',
      AUTHENTICATE_AND_GET_USER_DETAILS: '/authenticateAndGetUserDetails',
    },
  },
  ASSETS: {
    USER_PROFILE_PICTURES: {
      PATH: 'assets/profile/',
      FILE_TYPE: '.png',
    },
  },
  apiEndpointTiles: 'https://osm-tiles.public.apps.sele.iml.fraunhofer.de/tile/',
  apiEndpointNominatim: 'https://osm-nominatim.public.apps.sele.iml.fraunhofer.de/?format=json',
  JWT_MODULE_ALLOWED_DOMAINS: /\S+.apps.sele.iml.fraunhofer.de/,
};
