/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { browser, by, element } from 'protractor';

export class AppPage {
  /**
   * Builds, signs and broadcasts a transaction to the blockchain.
   *
   * @returns css selector for an element
   */
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  /**
   * Builds, signs and broadcasts a transaction to the blockchain.
   *
   * @returns compute size of this element
   */
  async getTitleText(): Promise<string> {
    return element(by.css('app-root .content span')).getText();
  }
}
