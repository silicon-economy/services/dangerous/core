/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { PackagingLookupService } from '@packagingLookupModule/service/packaging-lookup.service';
import { PackagingLookupRequestDto, PackagingLookupResponseDto } from '@core/api-interfaces';
import { Observable } from 'rxjs';

@ApiTags('PackagingLookupController')
@ApiBearerAuth('JWT-auth')
@Controller('packaginglookup')
export class PackagingLookupController {
  constructor(private readonly packagingLookupService: PackagingLookupService) {}

  /**
   * Looks up packaging information.
   *
   * @param packagingLookupRequestDto - PackagingLookupRequestDto
   * @returns Promise\<PackagingLookupResponseDto[]\>
   */
  @Get()
  @ApiOperation({
    summary: 'Gets packaging details by a request defined as a packagingLookupRequestDto',
    description: 'Get packaging details by a request defined as a packagingLookupRequestDto',
  })
  public lookupPackaging(
    @Query() packagingLookupRequestDto: PackagingLookupRequestDto
  ): Observable<PackagingLookupResponseDto[]> {
    return this.packagingLookupService.lookupPackaging(packagingLookupRequestDto);
  }

  /**
   * Looks up packaging by its respective code.
   *
   * @param code - Packaging code
   * @returns Promise<PackagingLookupResponseDto>
   */
  @Get(':code')
  @ApiOperation({
    summary: 'Gets packaging details by packagingCode',
    description: 'Get packaging details by packagingCode',
  })
  public lookupPackagingByCode(@Param('code') code: string): Observable<PackagingLookupResponseDto> {
    return this.packagingLookupService.lookupPackagingByCode(code);
  }
}
