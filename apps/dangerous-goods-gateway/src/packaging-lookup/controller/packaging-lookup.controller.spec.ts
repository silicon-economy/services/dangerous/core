/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { PackagingLookupController } from '@packagingLookupModule/controller/packaging-lookup.controller';
import { PackagingLookupService } from '@packagingLookupModule/service/packaging-lookup.service';
import { PackagingLookupResponseDto } from '@core/api-interfaces';

describe('DangerousGoodLookupController', () => {
  let controller: PackagingLookupController;
  let service: PackagingLookupService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [PackagingLookupController],
      providers: [
        {
          provide: PackagingLookupService,
          useValue: {
            getDangerousGood: jest.fn(() => [{}] as PackagingLookupResponseDto[]),
            getDangerousGoodByCode: jest.fn(() => [{}] as PackagingLookupResponseDto[]),
          },
        },
      ],
    }).compile();

    controller = moduleRef.get<PackagingLookupController>(PackagingLookupController);
    service = moduleRef.get<PackagingLookupService>(PackagingLookupService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
    expect(controller.lookupPackaging).toBeDefined();
    expect(controller.lookupPackagingByCode).toBeDefined();
  });
});
