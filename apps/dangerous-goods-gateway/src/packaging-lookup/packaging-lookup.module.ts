/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { PackagingLookupController } from '@packagingLookupModule/controller/packaging-lookup.controller';
import { PackagingLookupService } from '@packagingLookupModule/service/packaging-lookup.service';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

const amqpModule = AmqpModule.register(Queues.DANGEROUS_LOOKUP);

@Module({
  imports: [amqpModule],
  controllers: [PackagingLookupController],
  providers: [PackagingLookupService],
  exports: [PackagingLookupService, amqpModule],
})
export class PackagingLookupModule {}
