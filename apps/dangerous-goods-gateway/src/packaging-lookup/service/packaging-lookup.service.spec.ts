/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { PackagingLookupService } from './packaging-lookup.service';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';

describe('DangerousGoodLookupService', () => {
  let packagingLookupServiceRef: PackagingLookupService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AmqpModule.register(Queues.DANGEROUS_LOOKUP)],
      providers: [PackagingLookupService],
    }).compile();

    packagingLookupServiceRef = module.get<PackagingLookupService>(PackagingLookupService);
  });

  it('should be defined', () => {
    expect(packagingLookupServiceRef).toBeDefined();
    expect(packagingLookupServiceRef.lookupPackaging).toBeDefined();
  });
});
