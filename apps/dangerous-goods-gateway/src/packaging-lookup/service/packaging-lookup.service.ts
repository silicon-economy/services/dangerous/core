/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { defaultIfEmpty, Observable } from 'rxjs';
import { PackagingLookupRequestDto, PackagingLookupResponseDto } from '@core/api-interfaces';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { MessagePatternsPackagingLookup } from '@core/api-interfaces/lib/amqp/message-patterns.enum';

@Injectable()
export class PackagingLookupService {
  private readonly logger = new Logger(PackagingLookupService.name);

  constructor(@Inject(Queues.DANGEROUS_LOOKUP) public readonly client: ClientProxy) {}

  /**
   * Looks up packaging information.
   *
   * @param packagingLookupRequestDto - Packaging Lookup Request Dto
   * @returns Observable\<PackagingLookupResponseDto[]\>
   */
  lookupPackaging(packagingLookupRequestDto: PackagingLookupRequestDto): Observable<PackagingLookupResponseDto[]> {
    this.logger.log('Send GET request for packaging information to the data-lookup microservice');
    this.logger.verbose('packagingLookupRequestDto: ' + JSON.stringify(packagingLookupRequestDto));
    return this.client.send(MessagePatternsPackagingLookup.GET, packagingLookupRequestDto);
  }

  /**
   * Looks up packaging by its respective packagingCode.
   *
   * @param packagingCode - Packaging packagingCode
   * @returns Promise<PackagingLookupResponseDto>
   */
  lookupPackagingByCode(packagingCode: string): Observable<PackagingLookupResponseDto> {
    this.logger.log('Send GET request for packaging information to the data-lookup microservice');
    this.logger.verbose('packagingCode: ' + JSON.stringify(packagingCode));
    return this.client
      .send<PackagingLookupResponseDto>(MessagePatternsPackagingLookup.GET_BY_CODE, packagingCode)
      .pipe(defaultIfEmpty(new PackagingLookupResponseDto()));
  }
}
