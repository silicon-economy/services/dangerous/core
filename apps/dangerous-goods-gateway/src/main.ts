/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { INestApplication, LogLevel, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';
import { Config } from '@config';
import { AppModule } from './app.module';

async function bootstrap() {
  const app: INestApplication = await NestFactory.create(AppModule, {
    logger: process.env.logLevel ? [<LogLevel>process.env.logLevel] : ['error', 'warn', 'log', 'debug', 'verbose'],
    cors: true,
  });
  const configService = app.get(ConfigService);

  const config: Pick<OpenAPIObject, 'openapi' | 'info' | 'servers' | 'security' | 'tags' | 'externalDocs'> =
    new DocumentBuilder()
      .setTitle(Config.APPLICATION_TITLE)
      .setDescription(Config.APPLICATION_DESCRIPTION)
      .setVersion(Config.APPLICATION_VERSION)
      .addBearerAuth(
        {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
          name: 'JWT',
          description: 'Enter JWT token:',
          in: 'header',
        },
        'JWT-auth'
      )
      .build();
  const document: OpenAPIObject = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(Config.SWAGGER_PATH, app, document);

  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.enableCors();
  await app.startAllMicroservices();
  await app.listen(configService.get<string>('httpPort'), () => console.log('Microservice is listening'));
}

void bootstrap();
