/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CanActivate, ExecutionContext, Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import jwt from 'jsonwebtoken';

import { Request } from 'express';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthGuard implements CanActivate {
  private readonly logger = new Logger(AuthGuard.name);
  private publicKey = '';

  constructor(public readonly configService: ConfigService) {}

  // eslint-disable-next-line jsdoc/require-jsdoc
  canActivate(context: ExecutionContext): boolean {
    const request: Request = context.switchToHttp().getRequest();
    const token = this.extractTokenFromHeader(request);
    if (!token) {
      this.logger.log('No JWT token was provided');
      throw new UnauthorizedException();
    }
    try {
      this.publicKey = Buffer.from(this.configService.get<string>('dangerousPublicKey'), 'base64').toString('utf8');
      request['user'] = jwt.verify(token, this.publicKey, { algorithms: ['ES512'] });
    } catch {
      this.logger.log('JWT token denied');
      throw new UnauthorizedException();
    }
    this.logger.log('JWT token accepted');
    return true;
  }

  // eslint-disable-next-line jsdoc/require-jsdoc
  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }
}
