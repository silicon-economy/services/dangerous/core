/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { DangerousGoodLookupModule } from '@dangerousGoodLookupModule/dangerous-good-lookup.module';
import { PackagingLookupModule } from '@packagingLookupModule/packaging-lookup.module';
import config from '@config';
import { BusinessLogicModule } from '@businessLogicModule/business-logic.module';
import { AuthModule } from '@authModule/auth.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

@Module({
  imports: [
    DangerousGoodLookupModule,
    PackagingLookupModule,
    ConfigModule.forRoot({ isGlobal: true, load: [config] }),
    BusinessLogicModule,
    AuthModule,
  ],
})
export class AppModule {
  constructor(
    @Inject(Queues.BUSINESS_LOGIC) public readonly client: ClientProxy,
    @Inject(Queues.DANGEROUS_LOOKUP) public readonly client2: ClientProxy
  ) {}

  async onApplicationBootstrap() {
    await this.client.connect();
    await this.client2.connect();
  }
}
