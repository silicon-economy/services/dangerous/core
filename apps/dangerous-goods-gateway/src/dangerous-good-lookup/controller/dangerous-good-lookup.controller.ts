/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller, Get, Param } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { DangerousGoodLookupService } from '@dangerousGoodLookupModule/service/dangerous-good-lookup.service';
import { DangerousGoodLookupResponseDto } from '@core/api-interfaces';
import { Observable } from 'rxjs';

@ApiTags('DangerousGoodLookupController')
@ApiBearerAuth('JWT-auth')
@Controller('dangerousgoodlookup')
export class DangerousGoodLookupController {
  constructor(private readonly dangerousGoodLookupService: DangerousGoodLookupService) {}

  /**
   * Gets the dangerous good details by an inputString.
   *
   * @param inputString - InputString to get dangerous good details
   * @returns Observable\<DangerousGoodLookupResponseDto[]\>
   */
  @Get('/:inputstring')
  @ApiOperation({
    summary: 'Gets a dangerous good by an inputString',
    description: 'Get a dangerous good by an inputString',
  })
  @ApiParam({
    name: 'inputstring',
    description:
      'the microservice will return all dangerous goods where the unNumber starts with this string and/or the description contains this string',
    examples: {
      1: {
        value: '1',
        description: "returns all dangerous goods where the unNumber starts with '1'",
      },
      Aceton: {
        value: 'Aceton',
        description: "returns all dangerous goods where the description contains 'Aceton'",
      },
    },
  })
  public getDangerousGood(@Param('inputstring') inputString: string): Observable<DangerousGoodLookupResponseDto[]> {
    return this.dangerousGoodLookupService.getDangerousGood(inputString);
  }
}
