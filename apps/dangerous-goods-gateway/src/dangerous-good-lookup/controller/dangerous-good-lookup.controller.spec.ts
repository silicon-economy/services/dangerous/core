/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { DangerousGoodLookupService } from '../service/dangerous-good-lookup.service';
import { DangerousGoodLookupController } from './dangerous-good-lookup.controller';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';

describe('DangerousGoodLookupController', () => {
  let dangerousGoodLookupControllerRef: DangerousGoodLookupController;
  let dangerousGoodLookupServiceRef: DangerousGoodLookupService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AmqpModule.register(Queues.DANGEROUS_LOOKUP)],
      controllers: [DangerousGoodLookupController],
      providers: [DangerousGoodLookupService],
    }).compile();

    dangerousGoodLookupControllerRef = moduleRef.get<DangerousGoodLookupController>(DangerousGoodLookupController);
    dangerousGoodLookupServiceRef = moduleRef.get<DangerousGoodLookupService>(DangerousGoodLookupService);
  });

  it('should be defined', () => {
    expect(dangerousGoodLookupControllerRef).toBeDefined();
    expect(dangerousGoodLookupServiceRef).toBeDefined();
    expect(dangerousGoodLookupControllerRef.getDangerousGood).toBeDefined();
  });
});
