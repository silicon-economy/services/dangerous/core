/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { DangerousGoodLookupService } from '@dangerousGoodLookupModule/service/dangerous-good-lookup.service';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';

describe('DangerousGoodLookupService', () => {
  let dangerousGoodLookupServiceRef: DangerousGoodLookupService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AmqpModule.register(Queues.DANGEROUS_LOOKUP)],
      providers: [DangerousGoodLookupService],
    }).compile();

    dangerousGoodLookupServiceRef = module.get<DangerousGoodLookupService>(DangerousGoodLookupService);
  });

  it('should be defined', () => {
    expect(dangerousGoodLookupServiceRef).toBeDefined();
    expect(dangerousGoodLookupServiceRef.getDangerousGood).toBeDefined();
  });

  it('should get dangerous good', () => {
    const amqpClientSpy = jest.spyOn(dangerousGoodLookupServiceRef.client, 'send')
    dangerousGoodLookupServiceRef.getDangerousGood('testxd')
    expect(amqpClientSpy).toHaveBeenCalled()
  })
});
