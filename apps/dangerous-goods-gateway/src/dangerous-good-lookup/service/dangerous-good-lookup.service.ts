/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy, RpcException } from '@nestjs/microservices';
import { Observable } from 'rxjs';
import { DangerousGoodLookupResponseDto } from '@core/api-interfaces';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { MessagePatternsDangerousGoodLookup } from '@core/api-interfaces/lib/amqp/message-patterns.enum';

@Injectable()
export class DangerousGoodLookupService {
  private readonly logger = new Logger(DangerousGoodLookupService.name);
  constructor(@Inject(Queues.DANGEROUS_LOOKUP) public readonly client: ClientProxy) {}

  /**
   * Returns a dangerous good by an inputString.
   *
   * @param inputString - Input string
   * @returns Observable\<DangerousGoodLookupResponseDto[]\>
   */
  getDangerousGood(inputString: string): Observable<DangerousGoodLookupResponseDto[]> {
    try {
      this.logger.log('Send GET request to data lookup service to get dangerous good information');
      this.logger.verbose('inputString: ' + JSON.stringify(inputString));
      return this.client.send(MessagePatternsDangerousGoodLookup.GET, inputString);
    } catch (error) {
      throw new RpcException(error as string);
    }
  }
}
