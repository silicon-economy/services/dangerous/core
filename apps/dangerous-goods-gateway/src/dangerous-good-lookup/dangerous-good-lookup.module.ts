/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { DangerousGoodLookupController } from '@dangerousGoodLookupModule/controller/dangerous-good-lookup.controller';
import { DangerousGoodLookupService } from '@dangerousGoodLookupModule/service/dangerous-good-lookup.service';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AuthModule } from '@authModule/auth.module';

const amqpModule = AmqpModule.register(Queues.DANGEROUS_LOOKUP);

@Module({
  imports: [amqpModule, AuthModule],
  controllers: [DangerousGoodLookupController],
  providers: [DangerousGoodLookupService],
  exports: [DangerousGoodLookupService, amqpModule],
})
export class DangerousGoodLookupModule {}
