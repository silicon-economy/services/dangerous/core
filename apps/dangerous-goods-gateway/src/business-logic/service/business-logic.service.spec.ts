/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { BusinessLogicService } from '@businessLogicModule/service/business-logic.service';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';

describe('BusinessLogicService', () => {
  let businessLogicServiceRef: BusinessLogicService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AmqpModule.register(Queues.BUSINESS_LOGIC)],
      providers: [BusinessLogicService],
    }).compile();

    businessLogicServiceRef = module.get<BusinessLogicService>(BusinessLogicService);
  });

  it('should be defined', () => {
    expect(businessLogicServiceRef).toBeDefined();
    expect(businessLogicServiceRef.calculateExemption).toBeDefined();
  });
});
