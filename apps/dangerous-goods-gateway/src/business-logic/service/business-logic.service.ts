/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import { CalculateExemptionRequestDto, CalculateExemptionResponseDto } from '@core/api-interfaces';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { MessagePatternsBusinessLogic } from '@core/api-interfaces/lib/amqp/message-patterns.enum';

@Injectable()
export class BusinessLogicService {
  private readonly logger = new Logger(BusinessLogicService.name);
  constructor(@Inject(Queues.BUSINESS_LOGIC) public readonly client: ClientProxy) {}

  /**
   * Returns a thousand-point value for given adrInformation.
   *
   * @param adrInformation - ADR information
   * @returns Promise<BusinessLogicCalculateExemptionResponseDto>
   */
  public async calculateExemption(
    adrInformation: CalculateExemptionRequestDto
  ): Promise<CalculateExemptionResponseDto> {
    this.logger.log('Send GET request to business logic service to get exemption status');
    this.logger.verbose('adrInformation: ' + JSON.stringify(adrInformation));
    return firstValueFrom(this.client.send(MessagePatternsBusinessLogic.CALCULATE_EXEMPTION, adrInformation));
  }
}
