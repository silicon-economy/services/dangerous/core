/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Body, Controller, Put } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BusinessLogicService } from '@businessLogicModule/service/business-logic.service';
import { CalculateExemptionRequestDto, CalculateExemptionResponseDto } from '@core/api-interfaces';

@ApiTags('BusinessLogicController')
@ApiBearerAuth('JWT-auth')
@Controller('businesslogic')
export class BusinessLogicController {
  constructor(private readonly businessLogicService: BusinessLogicService) {}

  /**
   * Calculates exemption status.
   *
   * @param adrInformation - ADR information
   * @returns Promise<BusinessLogicCalculateExemptionResponseDto>
   */
  @Put('adr')
  @ApiOperation({
    summary: 'Calculates the thousand point value and the exemption status',
    description: 'Gets the thousand point value and the exemption status for given adrInformation.',
  })
  async calculateExemption(
    @Body()
    adrInformation: CalculateExemptionRequestDto
  ): Promise<CalculateExemptionResponseDto> {
    return this.businessLogicService.calculateExemption(adrInformation);
  }
}
