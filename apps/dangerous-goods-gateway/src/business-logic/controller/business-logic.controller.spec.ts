/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { BusinessLogicController } from '@businessLogicModule/controller/business-logic.controller';
import { BusinessLogicService } from '@businessLogicModule/service/business-logic.service';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';

describe('BusinessLogicController', () => {
  let businessLogicControllerRef: BusinessLogicController;
  let businessLogicServiceRef: BusinessLogicService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AmqpModule.register(Queues.BUSINESS_LOGIC)],
      controllers: [BusinessLogicController],
      providers: [BusinessLogicService],
    }).compile();

    businessLogicControllerRef = moduleRef.get<BusinessLogicController>(BusinessLogicController);
    businessLogicServiceRef = moduleRef.get<BusinessLogicService>(BusinessLogicService);
  });

  it('should be defined', () => {
    expect(businessLogicControllerRef).toBeDefined();
    expect(businessLogicServiceRef).toBeDefined();
  });
});
