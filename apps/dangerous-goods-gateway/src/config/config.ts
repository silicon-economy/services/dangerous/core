/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Main config
 */
export enum Config {
  // Application configuration
  APPLICATION_TITLE = 'Dangerous Goods Gateway',
  APPLICATION_DESCRIPTION = 'dangerous - Digitalisierung und Automatisierung von Gefahrgut – Rechtskonform und Sicher',
  APPLICATION_VERSION = '1.0',
  SWAGGER_PATH = 'api',
}

export default () => ({
  queuePrefix: process.env.DEPLOYMENT_ENVIRONMENT || '',
  amqpUrl: process.env.AMQP_URL || 'amqp://guest:guest@0.0.0.0:5672',
  httpPort: process.env.HTTP_PORT || '3000',
  dangerousPublicKey: process.env.DANGEROUS_PUBLIC_KEY || '',
});
