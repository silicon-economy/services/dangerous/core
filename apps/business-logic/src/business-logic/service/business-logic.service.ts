/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

import { DangerousGoodLookupResponseDto, TransportCategory, transportCategoryMap } from '@core/api-interfaces';
import {
  CalculateExemptionRequestDto,
  CalculateExemptionResponseDto,
  ConsigneeResponse,
  ExemptionStatus,
  SingleRequest,
  SingleThousandPointValue,
} from '@core/api-interfaces/lib/dtos/business-logic';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { MessagePatternsDangerousGoodLookup } from '@core/api-interfaces/lib/amqp/message-patterns.enum';
import { AdrValues } from '@core/api-interfaces/lib/dtos/business-logic/business-logic.adr-values';

@Injectable()
export class BusinessLogicService {
  private readonly logger = new Logger(BusinessLogicService.name);
  constructor(@Inject(Queues.DANGEROUS_LOOKUP) public readonly client: ClientProxy) {}

  /**
   * Calculates thousand point value for given adrInformation
   *
   * @param adrInformation - CalculateExemptionRequestDto
   * @returns Promise<CalculateExemptionResponseDto>
   */
  async calculateExemption(adrInformation: CalculateExemptionRequestDto): Promise<CalculateExemptionResponseDto> {
    let responseDto: CalculateExemptionResponseDto = new CalculateExemptionResponseDto(
      [],
      0,
      ExemptionStatus.transportWithoutExemption
    );

    this.logger.debug('Start calculate exemption');
    for (const consignees of adrInformation.consignees) {
      const consigneeResponses: ConsigneeResponse = new ConsigneeResponse([]);

      for (const request of consignees.requests) {
        const dangerousGoodData: DangerousGoodLookupResponseDto[] = await this.lookupData(request.inputString);
        const singleThousandPointValueDto: SingleThousandPointValue = this.buildSingleThousandPointValueDto(
          request,
          dangerousGoodData
        );
        consigneeResponses.responses.push(singleThousandPointValueDto);
        responseDto.totalTransportCategoryPoints += singleThousandPointValueDto.transportCategoryPoints;
      }
      responseDto.consignees.push(consigneeResponses);
    }
    responseDto = this.setExemptionStatus(responseDto);
    this.logger.debug('Finish calculate exemption');
    this.logger.verbose('Calculate exemption response dto: ' + JSON.stringify(responseDto));
    return responseDto;
  }

  /**
   * Builds a SingleThousandPointValue from given SingleRequest and DangerousGoodLookupResponseDto[].
   *
   * @param request - SingleRequest
   * @param dangerousGoodData - DangerousGoodLookupResponseDto[]
   * @returns SingleThousandPointValue
   */
  private buildSingleThousandPointValueDto(
    request: SingleRequest,
    dangerousGoodData: DangerousGoodLookupResponseDto[]
  ): SingleThousandPointValue {
    this.logger.debug('Build single thousand point value dto');
    const totalAmount = request.quantity * request.individualAmount;
    const transportCategoryPoints =
      totalAmount * TransportCategory[transportCategoryMap.get(dangerousGoodData[0].transportCategory)];
    const exemptionStatus: ExemptionStatus = this.determineExemptionStatus(
      transportCategoryPoints,
      request,
      dangerousGoodData
    );
    return new SingleThousandPointValue(
      dangerousGoodData[0].unNumber,
      totalAmount,
      transportCategoryPoints,
      exemptionStatus
    );
  }

  /**
   * Determines the exemption status of dangerous goods.
   *
   * @param transportCategoryPoints - Transport points
   * @param request - Requested dangerous goods
   * @param dangerousGoodData - Data lookup result
   * @returns Exemption status
   */
  private determineExemptionStatus(
    transportCategoryPoints: number,
    request: SingleRequest,
    dangerousGoodData: DangerousGoodLookupResponseDto[]
  ): ExemptionStatus {
    this.logger.debug('Determine exemption status');
    if (transportCategoryPoints <= 1000 && request.individualAmount <= dangerousGoodData[0].limitedQuantity) {
      return ExemptionStatus.transportWith1kpAndLq;
    } else if (transportCategoryPoints <= 1000) {
      return ExemptionStatus.transportWith1kp;
    } else if (request.individualAmount <= dangerousGoodData[0].limitedQuantity) {
      return ExemptionStatus.transportWithLq;
    } else {
      return ExemptionStatus.transportWithoutExemption;
    }
  }

  /**
   * Sets the exemption status for a CalculateExemptionResponseDto.
   *
   * @param responseDto - CalculateExemptionResponseDto
   * @returns CalculateExemptionResponseDto
   */
  private setExemptionStatus(responseDto: CalculateExemptionResponseDto): CalculateExemptionResponseDto {
    this.logger.debug('Set exemption status');
    const determinedAdrValues: AdrValues = this.determineTotalAdr(responseDto, false, false);

    if (determinedAdrValues.adr34 && determinedAdrValues.adr1136) {
      responseDto.exemptionStatus = ExemptionStatus.transportWith1kpAndLq;
    } else if (determinedAdrValues.adr1136) {
      responseDto.exemptionStatus = ExemptionStatus.transportWith1kp;
    } else if (determinedAdrValues.adr34) {
      responseDto.exemptionStatus = ExemptionStatus.transportWithLq;
    } else {
      responseDto.exemptionStatus = ExemptionStatus.transportWithoutExemption;
    }
    this.logger.debug('Set exemption status to ' + responseDto.exemptionStatus);
    return responseDto;
  }

  /**
   * Determines the total transport points for a given request.
   *
   * @param responseDto - CalculateExemptionResponseDto
   * @param adr34 - States, if ADR 3.4 is violated, e.g., violation represents `false`
   * @param adr1136 - States, if ADR 1.1.3.6 is violated, e.g., violation represents `false`
   * @returns Updated values of adr34 and adr1136 for entire freight
   */
  private determineTotalAdr(responseDto: CalculateExemptionResponseDto, adr34: boolean, adr1136: boolean): AdrValues {
    this.logger.debug('Determine total adr values');
    let determinedAdrValues: AdrValues = new AdrValues(false, false, true);
    // Iterate through each consignee that is going to be supplied
    for (const consignees of responseDto.consignees) {
      // Iterate through each good that is being delivered to the consignee
      for (const response of consignees.responses) {
        determinedAdrValues = this.determineAdr(response, adr34, adr1136);
        if (determinedAdrValues.withoutExemption) {
          this.logger.verbose('Total adr result: ' + JSON.stringify(determinedAdrValues));
          return new AdrValues(false, false);
        }
      }
    }
    this.logger.verbose('Total adr result: ' + JSON.stringify(determinedAdrValues));
    return determinedAdrValues;
  }

  /**
   * Determines single ADR status compliance for an amount of a given dangerous good.
   *
   * @param response - SingleThousandPointValue
   * @param adr34 - States, if ADR 3.4 is violated, e.g., violation represents `false`
   * @param adr1136 - States, if ADR 1.1.3.6 is violated, e.g., violation represents `false`
   * @returns Boolean values for adr34, adr1136, withoutExemption variables
   */
  private determineAdr(response: SingleThousandPointValue, adr34: boolean, adr1136: boolean): AdrValues {
    this.logger.debug('Determine individual adr values');
    switch (response.exemptionStatus) {
      case ExemptionStatus.transportWithoutExemption: {
        return new AdrValues(false, false, true);
      }
      case ExemptionStatus.transportWith1kpAndLq: {
        return new AdrValues(true, true, false);
      }
      case ExemptionStatus.transportWith1kp: {
        return new AdrValues(adr34, true, false);
      }
      case ExemptionStatus.transportWithLq: {
        return new AdrValues(true, adr1136, false);
      }
    }
  }

  /**
   * Requests dangerous goods data for given Un number.
   *
   * @param inputString - Un number
   * @returns Promise\<DangerousGoodLookupResponseDto[]\>
   */
  private async lookupData(inputString: string): Promise<DangerousGoodLookupResponseDto[]> {
    this.logger.debug('Send AMQP request to data lookup service to lookup data');
    return lastValueFrom(this.client.send(MessagePatternsDangerousGoodLookup.GET, inputString));
  }
}
