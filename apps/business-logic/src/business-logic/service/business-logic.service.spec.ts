/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { BusinessLogicService } from '@businessLogicModule/service/business-logic.service';
import { Test, TestingModule } from '@nestjs/testing';
import {
  CalculateExemptionRequestDto,
  CalculateExemptionResponseDto,
  DangerousGoodLookupResponseDto,
  ExemptionStatus,
} from '@core/api-interfaces';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { PackagingUnit } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/packaging-unit.enum';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

describe('BusinessLogicService', () => {
  let service: BusinessLogicService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AmqpModule.register(Queues.DANGEROUS_LOOKUP)],
      providers: [BusinessLogicService],
    }).compile();

    service = module.get<BusinessLogicService>(BusinessLogicService);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    jest.spyOn(BusinessLogicService.prototype as any, 'lookupData').mockImplementation(
      () =>
        [
          {
            unNumber: '1090',
            description: 'ACETON',
            label1: '3',
            label2: '-',
            label3: '-',
            packingGroup: 'II',
            tunnelRestrictionCode: '(D/E)',
            transportCategory: '2',
            polluting: false,
            limitedQuantity: 1,
          },
        ] as DangerousGoodLookupResponseDto[]
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('calculateExemption', () => {
    it('should calculate thousand point value for given adrInformation and set exemption status accordingly', async () => {
      const requestDto: CalculateExemptionRequestDto = {
        consignees: [
          {
            requests: [
              {
                inputString: '1090',
                unit: PackagingUnit.kg,
                quantity: 541,
                individualAmount: 324,
              },
            ],
          },
        ],
      };

      const responseDto: CalculateExemptionResponseDto = {
        consignees: [
          {
            responses: [
              {
                unNumber: '1090',
                totalAmount: 175284,
                transportCategoryPoints: 525852,
                exemptionStatus: ExemptionStatus.transportWithoutExemption,
              },
            ],
          },
        ],
        totalTransportCategoryPoints: 525852,
        exemptionStatus: ExemptionStatus.transportWithoutExemption,
      };

      expect(await service.calculateExemption(requestDto)).toEqual(await Promise.resolve(responseDto));
    });
  });

  it('should calculate thousand point value for given adrInformation and set exemption status accordingly', async () => {
    const requestDto: CalculateExemptionRequestDto = {
      consignees: [
        {
          requests: [
            {
              inputString: '1090',
              unit: PackagingUnit.kg,
              quantity: 5,
              individualAmount: 2,
            },
          ],
        },
      ],
    };

    const responseDto: CalculateExemptionResponseDto = {
      consignees: [
        {
          responses: [
            {
              unNumber: '1090',
              totalAmount: 10,
              transportCategoryPoints: 30,
              exemptionStatus: ExemptionStatus.transportWith1kp,
            },
          ],
        },
      ],
      totalTransportCategoryPoints: 30,
      exemptionStatus: ExemptionStatus.transportWith1kp,
    };
    expect(await service.calculateExemption(requestDto)).toEqual(await Promise.resolve(responseDto));
  });

  it('should calculate thousand point value for given adrInformation and set exemption status accordingly', async () => {
    const requestDto: CalculateExemptionRequestDto = {
      consignees: [
        {
          requests: [
            {
              inputString: '1090',
              unit: PackagingUnit.kg,
              quantity: 334,
              individualAmount: 1,
            },
          ],
        },
      ],
    };

    const responseDto: CalculateExemptionResponseDto = {
      consignees: [
        {
          responses: [
            {
              unNumber: '1090',
              totalAmount: 334,
              transportCategoryPoints: 1002,
              exemptionStatus: ExemptionStatus.transportWithLq,
            },
          ],
        },
      ],
      totalTransportCategoryPoints: 1002,
      exemptionStatus: ExemptionStatus.transportWithLq,
    };
    expect(await service.calculateExemption(requestDto)).toEqual(await Promise.resolve(responseDto));
  });

  it('should calculate thousand point value for given adrInformation and set exemption status accordingly', async () => {
    const requestDto: CalculateExemptionRequestDto = {
      consignees: [
        {
          requests: [
            {
              inputString: '1090',
              unit: PackagingUnit.kg,
              quantity: 330,
              individualAmount: 1,
            },
          ],
        },
      ],
    };

    const responseDto: CalculateExemptionResponseDto = {
      consignees: [
        {
          responses: [
            {
              unNumber: '1090',
              totalAmount: 330,
              transportCategoryPoints: 990,
              exemptionStatus: ExemptionStatus.transportWith1kpAndLq,
            },
          ],
        },
      ],
      totalTransportCategoryPoints: 990,
      exemptionStatus: ExemptionStatus.transportWith1kpAndLq,
    };
    expect(await service.calculateExemption(requestDto)).toEqual(await Promise.resolve(responseDto));
  });
});
