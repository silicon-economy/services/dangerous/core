/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { BusinessLogicService } from '@businessLogicModule/service/business-logic.service';
import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { CalculateExemptionRequestDto, CalculateExemptionResponseDto } from '@core/api-interfaces';
import { MessagePatternsBusinessLogic } from '@core/api-interfaces/lib/amqp/message-patterns.enum';

@Controller()
export class BusinessLogicController {
  constructor(private readonly businessLogicService: BusinessLogicService) {}

  /**
   * Returns thousand point value for given adrInformation.
   *
   * @param adrInformation - CalculateExemptionRequestDto
   * @returns Promise<CalculateExemptionResponseDto>
   */
  @MessagePattern(MessagePatternsBusinessLogic.CALCULATE_EXEMPTION)
  async calculateExemption(adrInformation: CalculateExemptionRequestDto): Promise<CalculateExemptionResponseDto> {
    return this.businessLogicService.calculateExemption(adrInformation);
  }
}
