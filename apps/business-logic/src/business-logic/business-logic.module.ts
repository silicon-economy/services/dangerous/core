/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { BusinessLogicController } from '@businessLogicModule/controller/business-logic.controller';
import { BusinessLogicService } from '@businessLogicModule/service/business-logic.service';
import { Module } from '@nestjs/common';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

const amqpModule = AmqpModule.register(Queues.DANGEROUS_LOOKUP);

@Module({
  imports: [amqpModule],
  controllers: [BusinessLogicController],
  providers: [BusinessLogicService],
  exports: [BusinessLogicService, amqpModule],
})
export class BusinessLogicModule {}
