/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { BusinessLogicModule } from '@businessLogicModule/business-logic.module';
import config from '@config';
import { Inject, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

@Module({
  imports: [BusinessLogicModule, ConfigModule.forRoot({ isGlobal: true, load: [config] })],
})
export class AppModule {
  constructor(@Inject(Queues.DANGEROUS_LOOKUP) public readonly client: ClientProxy) {}

  // noinspection JSUnusedGlobalSymbols
  async onApplicationBootstrap() {
    await this.client.connect();
  }
}
