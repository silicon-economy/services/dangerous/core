/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import config from '@config';
import { INestMicroservice, LogLevel } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

async function bootstrap() {
  // TODO: Remove when the following is fixed https://github.com/nestjs/nest/issues/2343
  const appContext = await NestFactory.createApplicationContext(
    ConfigModule.forRoot({
      load: [config],
    }),
    { logger: process.env.logLevel ? [<LogLevel>process.env.logLevel] : ['error', 'warn', 'log', 'debug', 'verbose'] }
  );

  const configService = appContext.get(ConfigService);

  const businessLogicMicroservice: INestMicroservice = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: [configService.get('amqpUrl')],
      queue: configService.get<string>('queuePrefix') + Queues.BUSINESS_LOGIC,
      queueOptions: {
        durable: true,
      },
    },
  });

  // TODO: Remove when the following is fixed https://github.com/nestjs/nest/issues/2343
  // Close the temporary app context since we no longer need it
  await appContext.close();
  await businessLogicMicroservice.listen();
}

void bootstrap();
