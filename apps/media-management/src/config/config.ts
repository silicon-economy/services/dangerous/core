/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { QRCodeToFileOptions } from 'qrcode';

/**
 * Main config
 */
export enum Config {
  // File paths
  CAS_NUMBER_PDF_TEMPLATES_PATH = '/assets/pdfs/',
  CURRENT_QR_CODE_PATH = '/assets/pngs/currentQrCode.png',

  TEXT_FIELD_INDIVIDUAL_AMOUNT_NAME = 'individualAmountTextField',
  TEXT_FIELD_UNIT_NAME = 'unitTextField',
}

/**
 * QR code generator configuration
 */
export const qrCodeConfig: QRCodeToFileOptions = {
  type: 'png',
  color: {
    dark: '#000000', // Black dots
    light: '#0000', // Transparent background
  },
  errorCorrectionLevel: 'L',
};

export default () => ({
  queuePrefix: process.env.DEPLOYMENT_ENVIRONMENT || '',
  amqpUrl: process.env.AMQP_URL || 'amqp://guest:guest@0.0.0.0:5672',
});
