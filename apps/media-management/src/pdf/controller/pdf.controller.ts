/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { PdfService } from '@pdfModule/service/pdf.service';
import { MessagePatternsMediaManagement } from '@core/api-interfaces/lib/amqp/message-patterns.enum';

@Controller()
export class PdfController {
  constructor(private readonly pdfService: PdfService) {}

  /**
   * Generates a shipping label (PDF) for a given transport document and
   * returns it immediately without local saving.
   *
   * @param transportDocumentId - Transport document id
   */
  @MessagePattern(MessagePatternsMediaManagement.GET_PDF_BY_TRANSPORT_DOCUMENT_ID)
  public async getPdf(transportDocumentId: string): Promise<string> {
    return this.pdfService.generateShippingLabel(transportDocumentId);
  }
}
