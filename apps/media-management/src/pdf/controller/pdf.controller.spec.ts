/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { PdfService } from '@pdfModule/service/pdf.service';
import { QrCodeService } from '@qrCodeModule/service/qr-code.service';
import { QrCodeModule } from '@qrCodeModule/qr-code.module';
import { PdfController } from '@pdfModule/controller/pdf.controller';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

describe('PdfController', () => {
  let controller: PdfController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [QrCodeModule, AmqpModule.register(Queues.DATA_MANAGEMENT), ConfigModule],
      providers: [PdfService, QrCodeService],
      controllers: [PdfController],
    }).compile();

    controller = module.get<PdfController>(PdfController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(controller.getPdf).toBeDefined();
  });
});
