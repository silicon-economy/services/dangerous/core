/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { PdfService } from '@pdfModule/service/pdf.service';
import { QrCodeModule } from '@qrCodeModule/qr-code.module';
import { PdfController } from '@pdfModule/controller/pdf.controller';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

const amqpModule = AmqpModule.register(Queues.DATA_MANAGEMENT);

@Module({
  imports: [HttpModule, QrCodeModule, amqpModule],
  controllers: [PdfController],
  providers: [PdfService],
  exports: [amqpModule],
})
export class PdfModule {}
