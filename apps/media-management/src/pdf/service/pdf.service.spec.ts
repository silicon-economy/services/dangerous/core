/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { PdfService } from '@pdfModule/service/pdf.service';
import { QrCodeService } from '@qrCodeModule/service/qr-code.service';
import { QrCodeModule } from '@qrCodeModule/qr-code.module';
import { GoodsDataDtoMock, PdfStringPartialExpectedResult } from '@pdfModule/service/mock/goods-data.dto.mock';
import { AmqpModule } from '@core/api-interfaces/lib/amqp/amqp.module';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';

describe('PdfService', () => {
  let service: PdfService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [QrCodeModule, AmqpModule.register(Queues.DATA_MANAGEMENT), ConfigModule],
      providers: [QrCodeService, PdfService],
    }).compile();

    service = module.get<PdfService>(PdfService);
    jest.spyOn(PdfService.prototype, 'getTransportDocumentById').mockImplementation(() => {
      return GoodsDataDtoMock.getGoodsDataDtoMock();
    });
    jest.spyOn(PdfService.prototype, 'getDangerousGoodRegistrationById').mockImplementation(async () => {
      const document: SaveGoodsDataDto = await GoodsDataDtoMock.getGoodsDataDtoMock();
      document.id = 'DGR-0e0e8e58-f5ff-438c-a7ed-d86acf24beb9';
      return document;
    });
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(service.generateShippingLabel).toBeDefined();
    expect(service.getTransportDocumentById).toBeDefined();
    expect(service.getDangerousGoodRegistrationById).toBeDefined();
  });

  it('should output pdf string for given transport paper by id', async () => {
    expect(await service.generateShippingLabel('BP-0e0e8e58-f5ff-438c-a7ed-d86acf24beb9')).toContain(
      PdfStringPartialExpectedResult.PARTIAL_EXPECTED_RESULT
    );
  });

  it('should output pdf string for given dangerous good registration by id', async () => {
    expect(await service.generateShippingLabel('DGR-0e0e8e58-f5ff-438c-a7ed-d86acf24beb9')).toContain(
      PdfStringPartialExpectedResult.PARTIAL_EXPECTED_RESULT
    );
  });
});
