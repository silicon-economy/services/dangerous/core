/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TransportLabel } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/transport-label.enum';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { PackagingGroup } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/packaging-group.enum';
import { TunnelRestrictionCode } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/tunnel-restriction-code.enum';
import { TransportCategory } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/transport-category.enum';
import { PackagingUnit } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/packaging-unit.enum';
import { AdditionalInformation } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/additional-information.enum';
import { TransportDocumentStatus } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/enums/status.enum';

export class GoodsDataDtoMock {
  /**
   * Provides a mock transport document (Used for tests only)
   *
   * @returns Mock transport document
   */
  public static getGoodsDataDtoMock(): Promise<SaveGoodsDataDto> {
    return new Promise<SaveGoodsDataDto>((resolve) => {
      resolve({
        id: 'BP000001-20210819',
        consignor: {
          name: 'Chemical Industries Germany Inc.',
          address: {
            street: 'Ressestraße',
            number: '50',
            postalCode: '45894',
            city: 'Gelsenkirchen',
            country: 'Deutschland',
          },
          contact: {
            name: 'Fritz Hazard',
            phone: '0049 123 456 789 0',
            mail: 'fritz.hazard@ChemIndInc.de',
            department: 'OE 3 II',
          },
        },
        freight: {
          orders: [
            {
              id: 'db049a1b-ee64-42ae-9a4a-2b739f2f2bdb',
              consignee: {
                name: 'Funke AG - Lacke und Farben',
                address: {
                  street: 'Columbiadamm',
                  number: '194',
                  postalCode: '10965',
                  city: 'Berlin',
                  country: 'Deutschland',
                },
                contact: {
                  name: 'Dr. Martina Zünd',
                  phone: '0049 741 852 063 9',
                  mail: 'Zuend@Funke.de',
                  department: 'Abt. 4A',
                },
              },
              logEntries: [],
              status: '',
              orderPositions: [
                {
                  dangerousGood: {
                    unNumber: '1230',
                    casNumber: '67-56-1',
                    description: 'METHANOL',
                    label1: TransportLabel._3,
                    label2: TransportLabel._6_1,
                    label3: TransportLabel._,
                    packingGroup: PackagingGroup.II,
                    tunnelRestrictionCode: TunnelRestrictionCode['(D/E)'],
                    transportCategory: TransportCategory._2,
                  },
                  package: 'Fässer aus Stahl (A) abnehmbarer Deckel',
                  packagingCode: '1A2',
                  quantity: 1,
                  unit: PackagingUnit.L,
                  individualAmount: 200,
                  polluting: false,
                  transportPoints: 600,
                  totalAmount: 200,
                  id: 2,
                  deviceId: '0',
                  status: '',
                  logEntries: [],
                },
                {
                  dangerousGood: {
                    unNumber: '1090',
                    casNumber: '67-64-1',
                    description: 'ACETON',
                    label1: TransportLabel._3,
                    label2: TransportLabel._,
                    label3: TransportLabel._,
                    packingGroup: PackagingGroup.II,
                    tunnelRestrictionCode: TunnelRestrictionCode['(D/E)'],
                    transportCategory: TransportCategory._2,
                  },
                  package: 'Fässer aus Kunststoff (H) abnehmbarer Deckel',
                  packagingCode: '1H2',
                  quantity: 1,
                  unit: PackagingUnit.L,
                  individualAmount: 60,
                  polluting: false,
                  transportPoints: 180,
                  totalAmount: 60,
                  id: 3,
                  deviceId: '0',
                  status: '',
                  logEntries: [],
                },
              ],
            },
            {
              id: 'dc2f68db-6682-4bbd-ac62-914d7aedf105',
              consignee: {
                name: 'Gefahrstoffvertriebsgesellschaft Huber GmbH',
                address: {
                  street: 'Meraner Str.',
                  number: '47',
                  postalCode: '83024',
                  city: 'Rosenheim',
                  country: 'Deutschland',
                },
                contact: {
                  name: 'Alois Huber',
                  phone: '0049 098 765 432 1',
                  mail: 'alois.huber@geschaeftsfuehrung.GVG-Huber.de',
                  department: 'GL',
                },
              },
              logEntries: [],
              status: '',
              orderPositions: [
                {
                  dangerousGood: {
                    unNumber: '3105',
                    casNumber: '37187-22-7',
                    description: 'ORGANISCHES PEROXID TYP D, FLÜSSIG, N.A.G., (ENTHÄLT ACETYLACETONPEROXID)',
                    label1: TransportLabel._5_2,
                    label2: TransportLabel._,
                    label3: TransportLabel._,
                    packingGroup: PackagingGroup.II,
                    tunnelRestrictionCode: TunnelRestrictionCode['(D)'],
                    transportCategory: TransportCategory._2,
                  },
                  package:
                    'Großpackmittel (IBC) aus starrer Kunststoff (H) für flüssige Stoffe, mit äußerer Umhüllung aus Stahl (A)',
                  packagingCode: '31HA1',
                  quantity: 1,
                  unit: PackagingUnit.L,
                  individualAmount: 1000,
                  polluting: false,
                  transportPoints: 3000,
                  totalAmount: 1000,
                  id: 4,
                  deviceId: '0',
                  status: '',
                  logEntries: [],
                },
              ],
            },
          ],
          additionalInformation: AdditionalInformation['Beförderung ohne Freistellung nach ADR 1.1.3.6'],
          transportationInstructions: 'Bitte Beförderungseinheit nach letzter Entladung reinigen.',
          totalTransportPoints: 3780,
        },
        carrier: {
          name: 'Gefahrgutspedition',
          driver: 'Mustermann',
          licensePlate: 'DR-EI333',
        },
        logEntries: [
          {
            status: TransportDocumentStatus.created,
            date: 1629367025537,
            author: '',
            description: '',
            acceptanceCriteria: null,
          },
        ],
        status: TransportDocumentStatus.created,
        createdDate: 1629367025537,
        lastUpdate: 1629367025537,
      });
    });
  }
}

/**
 * Expected first string of the generated shipping label (Used for tests only)
 */
export enum PdfStringPartialExpectedResult {
  PARTIAL_EXPECTED_RESULT = 'data:application/pdf;base64',
}
