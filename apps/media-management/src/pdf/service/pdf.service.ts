/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import * as fs from 'fs';
import { PDFDocument, PDFFont, PDFForm, PDFPage, PDFTextField, StandardFonts } from 'pdf-lib';
import { QrCodeService } from '@qrCodeModule/service/qr-code.service';
import { Config } from '@config';
import { firstValueFrom } from 'rxjs';
import { PdfConfig } from '@pdfModule/config/pdf-config';
import { Queues } from '@amqp/queues.enum';
import { SaveGoodsDataDto } from '@core/api-interfaces/lib/dtos/data-management/transport-document/save-goods-data.dto';
import { MessagePatternsDangerousGoodRegistration, MessagePatternsDataManagement } from '@amqp/message-patterns.enum';
import { OrderPositionWithId } from '@core/api-interfaces/lib/dtos/data-management/transport-document/transport-document/order-position-with-id';

@Injectable()
export class PdfService {
  private readonly logger = new Logger(PdfService.name);

  constructor(
    private readonly qrCodeService: QrCodeService,
    @Inject(Queues.DATA_MANAGEMENT) public readonly client: ClientProxy,
    public readonly configService: ConfigService
  ) {}

  /**
   * Generates a (shipping) label for a given transport document or dangerous
   * good registration and returns it immediately without local saving.
   *
   * @param id - Transport document id | dangerous good registration id
   * @returns PDF-String (base64-encoded)
   */
  public async generateShippingLabel(id: string): Promise<string> {
    this.logger.debug('Generates a (shipping) label for a given transport document or dangerous good registration');
    this.logger.verbose('Transport document id | dangerous good registration id: ' + JSON.stringify(id));
    let document: SaveGoodsDataDto;

    if (id.startsWith('BP')) {
      this.logger.debug('Transport document id was provided');
      document = await this.getTransportDocumentById(id);
    } else if (id.startsWith('DGR')) {
      this.logger.debug('dangerous good registration id was provided');
      document = await this.getDangerousGoodRegistrationById(id);
    } else {
      throw Error('Id does not start with BP or DGR!');
    }

    let mergedShippingLabels: PDFDocument = await PDFDocument.create();

    mergedShippingLabels = await this.generateAllOrderPositionShippingLabel(document, mergedShippingLabels);
    // Serialize the PDFDocument to bytes (a Uint8Array)
    return mergedShippingLabels.saveAsBase64({ dataUri: true });
  }

  /**
   * Get transport document by transport document id.
   *
   * @param transportDocumentId - Transport document id
   * @returns Promise<TransportDocument>
   */
  async getTransportDocumentById(transportDocumentId: string): Promise<SaveGoodsDataDto> {
    try {
      this.logger.debug('Send AQMP request to data-management to GET the transport document by its id');
      return firstValueFrom(
        this.client.send(MessagePatternsDataManagement.GET_TRANSPORT_DOCUMENT, transportDocumentId)
      );
    } catch (e) {
      throw new NotFoundException(e);
    }
  }

  /**
   * Get dangerous good registration by dangerous good registration id.
   *
   * @param dangerousGoodRegistrationId - Dangerous good registration id
   * @returns Promise<DangerousGoodRegistration>
   */
  async getDangerousGoodRegistrationById(dangerousGoodRegistrationId: string): Promise<SaveGoodsDataDto> {
    try {
      this.logger.debug('Send AQMP request to data-management to GET the dangerous good registration by its id');
      return firstValueFrom(
        this.client.send(
          MessagePatternsDangerousGoodRegistration.GET_DANGEROUS_GOOD_REGISTRATION,
          dangerousGoodRegistrationId
        )
      );
    } catch (e) {
      throw new NotFoundException(e);
    }
  }

  /**
   * Generate shipping labels for all order positions of a transport document.
   *
   * @param transportDocument - Transport document
   * @param mergedShippingLabels - PDF document with several shipping labels
   * @returns PDF with shipping labels for all of a transport document's order positions
   */
  private async generateAllOrderPositionShippingLabel(
    transportDocument: SaveGoodsDataDto,
    mergedShippingLabels: PDFDocument
  ): Promise<PDFDocument> {
    this.logger.debug('Generate all order position shipping label');
    for (const order of transportDocument.freight.orders) {
      for (const orderPosition of order.orderPositions) {
        await this.generateOrderPositionShippingLabel(orderPosition, transportDocument, mergedShippingLabels);
      }
    }
    return mergedShippingLabels;
  }

  /**
   * Generate and append order position shipping label for a single order position.
   *
   * @param orderPosition - Order position with id of a transport document
   * @param transportDocument - Transport document
   * @param mergedShippingLabels - PDF document with several shipping labels
   * @returns Merged shipping labels PDF document
   */
  private async generateOrderPositionShippingLabel(
    orderPosition: OrderPositionWithId,
    transportDocument: SaveGoodsDataDto,
    mergedShippingLabels: PDFDocument
  ): Promise<PDFDocument> {
    const existingPdfBytes: Buffer = this.loadPdfTemplateBytes(orderPosition);
    const pdfDoc: PDFDocument = await PDFDocument.load(existingPdfBytes);
    const loadedPdfFonts: PdfFonts = await this.loadFonts(pdfDoc);

    const pages: PDFPage[] = pdfDoc.getPages();
    let workingPage: PDFPage = pages[0];
    const form: PDFForm = pdfDoc.getForm();

    const pdfTextFields: PdfTextFields = this.instantiateAllTextFields(form, transportDocument, orderPosition);

    const pdfTextFieldsWithSetAttributes: PdfTextFields = this.setAllTextFieldAttributes(
      pdfTextFields.consignorTextField,
      pdfTextFields.unitTextField,
      pdfTextFields.individualAmountTextField,
      form,
      loadedPdfFonts.helveticaFont,
      loadedPdfFonts.helveticaFontBold
    );

    workingPage = this.addTextFieldsToPage(
      workingPage,
      pdfTextFieldsWithSetAttributes.consignorTextField,
      pdfTextFieldsWithSetAttributes.individualAmountTextField,
      pdfTextFieldsWithSetAttributes.unitTextField,
      loadedPdfFonts.helveticaFont,
      loadedPdfFonts.helveticaFontBold
    );

    await this.addQrCodeToPage(orderPosition, workingPage, pdfDoc);

    form.flatten();
    // Add workingPage to mergedShippingLabels
    (await mergedShippingLabels.copyPages(pdfDoc, [0])).forEach((element) => {
      mergedShippingLabels.addPage(element);
    });

    return mergedShippingLabels;
  }

  /**
   * Load PDF templates of shipping labels.
   *
   * @param orderPosition - Order position with id of a transport document
   * @returns PDF template buffer
   */
  private loadPdfTemplateBytes(orderPosition: OrderPositionWithId): Buffer {
    this.logger.debug('Load pdf template bytes');
    return fs.readFileSync(
      __dirname + '/../..' + Config.CAS_NUMBER_PDF_TEMPLATES_PATH + orderPosition.dangerousGood.casNumber + '.pdf'
    );
  }

  /**
   * Add QR code to shipping label.
   *
   * @param orderPosition - Order position with id of a transport document
   * @param workingPage - Working PDF page
   * @param pdfDoc - PDF document
   * @returns Working PDF page
   */
  private async addQrCodeToPage(
    orderPosition: OrderPositionWithId,
    workingPage: PDFPage,
    pdfDoc: PDFDocument
  ): Promise<PDFPage> {
    console.debug('Add qr code to page');
    const qrCodeContent: string = JSON.stringify({
      orderPositionId: orderPosition.id,
    });
    const currentQrCodeUrl = __dirname + '/../..' + Config.CURRENT_QR_CODE_PATH;
    await this.qrCodeService.createQrCode(currentQrCodeUrl, qrCodeContent);
    const currentQrCodeBytes: Buffer = fs.readFileSync(currentQrCodeUrl);
    const qrCodeImage = await pdfDoc.embedPng(currentQrCodeBytes);
    workingPage.drawImage(qrCodeImage, {
      x: PdfConfig.qrCodeOptions.x,
      y: PdfConfig.qrCodeOptions.y,
      width: PdfConfig.qrCodeOptions.width,
      height: PdfConfig.qrCodeOptions.height,
    });
    return workingPage;
  }

  /**
   * Instantiate all text fields with given labels.
   *
   * @param form - PDF form
   * @param transportDocument - Transport document
   * @param orderPosition - Order position with id of a transport document
   * @returns Instantiated text fields of the shipping label
   */
  private instantiateAllTextFields(
    form: PDFForm,
    transportDocument: SaveGoodsDataDto,
    orderPosition: OrderPositionWithId
  ): PdfTextFields {
    const PdfTextFields: PdfTextFields = {
      consignorTextField: this.generateConsignorTextField(form, transportDocument),
      individualAmountTextField: form.createTextField(Config.TEXT_FIELD_INDIVIDUAL_AMOUNT_NAME),
      unitTextField: form.createTextField(Config.TEXT_FIELD_UNIT_NAME),
    };

    PdfTextFields.individualAmountTextField.setText(orderPosition.individualAmount.toString());
    PdfTextFields.unitTextField.setText(orderPosition.unit.toString());
    return PdfTextFields;
  }

  /**
   * Extract consignor from a given transport document and generate consignor text
   * field used for the shipping label.
   *
   * @param form - PDF form
   * @param transportDocument - Transport document
   * @returns Consignor text field
   */
  private generateConsignorTextField(form: PDFForm, transportDocument: SaveGoodsDataDto): PDFTextField {
    const consignorTextField = form.createTextField(PdfConfig.consignorTextFieldName);
    consignorTextField.enableMultiline();
    consignorTextField.setText(
      transportDocument.consignor.name +
        '\n' +
        transportDocument.consignor.address.street +
        ' ' +
        transportDocument.consignor.address.number +
        '\n' +
        transportDocument.consignor.address.postalCode +
        ' ' +
        transportDocument.consignor.address.city +
        '\n' +
        transportDocument.consignor.address.country +
        '\n' +
        'Tel.: ' +
        transportDocument.consignor.contact.phone
    );
    return consignorTextField;
  }

  /**
   * Add all text fields and set their positions in the shipping label.
   *
   * @param workingPage - Working PDF page
   * @param consignorTextField - Consignor text field
   * @param individualAmountTextField - Individual amount text field
   * @param unitTextField - Unit text field
   * @param helveticaFont - Helvetica font
   * @param helveticaFontBold - Helvetica bold font
   * @returns Working page with supplemented text fields
   */
  private addTextFieldsToPage(
    workingPage: PDFPage,
    consignorTextField: PDFTextField,
    individualAmountTextField: PDFTextField,
    unitTextField: PDFTextField,
    helveticaFont: PDFFont,
    helveticaFontBold: PDFFont
  ): PDFPage {
    consignorTextField.addToPage(workingPage, {
      x: PdfConfig.consignorTextFieldAppearance.x,
      y: PdfConfig.consignorTextFieldAppearance.y,
      width: PdfConfig.consignorTextFieldAppearance.width,
      height: PdfConfig.consignorTextFieldAppearance.height,
      borderWidth: PdfConfig.consignorTextFieldAppearance.borderWidth,
      font: helveticaFont,
    });

    individualAmountTextField.addToPage(workingPage, {
      x: PdfConfig.individualAmountTextFieldAppearance.x,
      y: PdfConfig.individualAmountTextFieldAppearance.y,
      width: PdfConfig.individualAmountTextFieldAppearance.width,
      height: PdfConfig.individualAmountTextFieldAppearance.height,
      borderWidth: PdfConfig.individualAmountTextFieldAppearance.borderWidth,
      font: helveticaFontBold,
    });

    unitTextField.addToPage(workingPage, {
      x: PdfConfig.unitTextFieldAppearance.x,
      y: PdfConfig.unitTextFieldAppearance.y,
      width: PdfConfig.unitTextFieldAppearance.width,
      height: PdfConfig.unitTextFieldAppearance.height,
      borderWidth: PdfConfig.unitTextFieldAppearance.borderWidth,
      font: helveticaFontBold,
    });
    return workingPage;
  }

  /**
   * Set all text field attributes of the shipping label.
   *
   * @param consignorTextField - Consignor text field
   * @param unitTextField - Unit text field
   * @param individualAmountTextField - Individual amount text field
   * @param form - PDF form
   * @param helveticaFont - Helvetica font
   * @param helveticaFontBold - Helvetica bold font
   * @returns All text fields with set attributes
   */
  private setAllTextFieldAttributes(
    consignorTextField: PDFTextField,
    unitTextField: PDFTextField,
    individualAmountTextField: PDFTextField,
    form: PDFForm,
    helveticaFont: PDFFont,
    helveticaFontBold: PDFFont
  ): PdfTextFields {
    return {
      consignorTextField: this.setConsignorTextFieldAttributes(consignorTextField, form, helveticaFont),
      individualAmountTextField: this.setIndividualAmountTextFieldAttributes(
        individualAmountTextField,
        form,
        helveticaFontBold
      ),
      unitTextField: this.setUnitTextFieldAttributes(unitTextField, form, helveticaFontBold),
    } satisfies PdfTextFields;
  }

  /**
   * Set the consignor text field attributes of the shipping label.
   *
   * @param consignorTextField - Consignor text field
   * @param form - PDF form
   * @param helveticaFont - Helvetica font
   * @returns Consignor text field
   */
  private setConsignorTextFieldAttributes(
    consignorTextField: PDFTextField,
    form: PDFForm,
    helveticaFont: PDFFont
  ): PDFTextField {
    consignorTextField.setAlignment(PdfConfig.consignorTextFieldAlignment);
    consignorTextField.acroField.setDefaultAppearance(PdfConfig.consignorTextFieldDefaultAppearance);
    form.updateFieldAppearances(helveticaFont);
    consignorTextField.setFontSize(PdfConfig.consignorTextFieldFontSize);
    return consignorTextField;
  }

  /**
   * Set the individual amount text field attributes of the shipping label.
   *
   * @param individualAmountTextField - Individual amount text field
   * @param form - PDF form
   * @param helveticaFontBold - Helvetica bold font
   * @returns Individual amount text field
   */
  private setIndividualAmountTextFieldAttributes(
    individualAmountTextField: PDFTextField,
    form: PDFForm,
    helveticaFontBold: PDFFont
  ): PDFTextField {
    individualAmountTextField.setAlignment(PdfConfig.setIndividualAmountTextFieldAlignment);
    individualAmountTextField.acroField.setDefaultAppearance(PdfConfig.setIndividualAmountTextFieldDefaultAppearance);
    form.updateFieldAppearances(helveticaFontBold);
    return individualAmountTextField;
  }

  /**
   * Set the unit text field attributes of the shipping label.
   *
   * @param unitTextField - Unit text field
   * @param form - PDF form
   * @param helveticaFontBold - Helvetica bold font
   * @returns Unit text field
   */
  private setUnitTextFieldAttributes(
    unitTextField: PDFTextField,
    form: PDFForm,
    helveticaFontBold: PDFFont
  ): PDFTextField {
    unitTextField.setAlignment(PdfConfig.unitTextFieldAlignment);
    unitTextField.acroField.setDefaultAppearance(PdfConfig.unitTextFieldDefaultAppearance);
    unitTextField.setFontSize(PdfConfig.unitTextFieldFontSize);
    form.updateFieldAppearances(helveticaFontBold);
    return unitTextField;
  }

  /**
   * Load necessary fonts used for the PDF generation.
   *
   * @param pdfDoc - PDF document
   */
  private async loadFonts(pdfDoc: PDFDocument) {
    return {
      helveticaFont: await pdfDoc.embedFont(StandardFonts.Helvetica),
      helveticaFontBold: await pdfDoc.embedFont(StandardFonts.HelveticaBold),
    } satisfies PdfFonts;
  }
}

type PdfFonts = {
  helveticaFont: PDFFont;
  helveticaFontBold: PDFFont;
};

type PdfTextFields = {
  consignorTextField: PDFTextField;
  individualAmountTextField: PDFTextField;
  unitTextField: PDFTextField;
};
