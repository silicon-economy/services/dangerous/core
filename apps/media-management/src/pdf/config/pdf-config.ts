/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { FieldAppearanceOptions } from 'pdf-lib/es/api/form/PDFField';
import { PDFPageDrawImageOptions, TextAlignment } from 'pdf-lib';

export class PdfConfig {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}
  static consignorTextFieldAppearance: FieldAppearanceOptions = {
    x: 14,
    y: 19,
    width: 535,
    height: 125,
    borderWidth: 0,
  };
  static individualAmountTextFieldAppearance: FieldAppearanceOptions = {
    x: 590,
    y: 64,
    width: 82,
    height: 32.5,
    borderWidth: 0,
  };
  static unitTextFieldAppearance: FieldAppearanceOptions = {
    x: 678,
    y: 55,
    width: 82,
    height: 50,
    borderWidth: 0,
  };
  static qrCodeOptions: PDFPageDrawImageOptions = {
    x: 750,
    y: -5,
    width: 170,
    height: 170,
  };

  static consignorTextFieldAlignment: TextAlignment = TextAlignment.Center;
  static consignorTextFieldDefaultAppearance = '/Helv Tf';
  static consignorTextFieldFontSize = 15;

  static setIndividualAmountTextFieldAlignment: TextAlignment = TextAlignment.Right;
  static setIndividualAmountTextFieldDefaultAppearance = '/HelvBo Tf';

  static unitTextFieldAlignment: TextAlignment = TextAlignment.Left;
  static unitTextFieldDefaultAppearance = '/HelvBo Tf';
  static unitTextFieldFontSize = 25;

  static consignorTextFieldName = 'consignorTextField';
}
