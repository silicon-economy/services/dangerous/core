/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { INestMicroservice, LogLevel, ValidationPipe } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from '@appModule';
import config from '@config';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

async function bootstrap() {
  // TODO: Remove when the following is fixed https://github.com/nestjs/nest/issues/2343
  const appContext = await NestFactory.createApplicationContext(
    ConfigModule.forRoot({
      load: [config],
    }),
    { logger: process.env.logLevel ? [<LogLevel>process.env.logLevel] : ['error', 'warn', 'log', 'debug', 'verbose'] }
  );
  const configService = appContext.get(ConfigService);
  // TODO End

  const mediaManagementMicroservice: INestMicroservice = await NestFactory.createMicroservice(AppModule, {
    logger: ['error', 'warn', 'debug', 'log'],
    cors: true,
    transport: Transport.RMQ,
    options: {
      urls: [configService.get('amqpUrl')],
      queue: configService.get<string>('queuePrefix') + Queues.MEDIA_MANAGEMENT,
      queueOptions: {
        durable: true,
      },
    },
  });

  // TODO: Remove when the following is fixed https://github.com/nestjs/nest/issues/2343
  // Close the temporary app context since we no longer need it
  await appContext.close();
  // TODO End

  mediaManagementMicroservice.useGlobalPipes(new ValidationPipe({ transform: true }));

  await mediaManagementMicroservice.listen();
}

void bootstrap().then();
