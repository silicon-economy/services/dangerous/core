/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { QrCodeModule } from '@qrCodeModule/qr-code.module';
import { PdfModule } from '@pdfModule/pdf.module';
import config from '@config';
import { Queues } from '@core/api-interfaces/lib/amqp/queues.enum';

@Module({
  imports: [QrCodeModule, PdfModule, ConfigModule.forRoot({ isGlobal: true, load: [config] })],
})
export class AppModule {
  constructor(@Inject(Queues.DATA_MANAGEMENT) public readonly client: ClientProxy) {}

  async onApplicationBootstrap() {
    await this.client.connect().catch((error) => {
      console.log(error);
    });
  }
}
