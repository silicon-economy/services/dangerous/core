/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable, Logger } from '@nestjs/common';
import QRCode from 'qrcode';
import { qrCodeConfig } from '@config';

@Injectable()
export class QrCodeService {
  private readonly logger = new Logger(QrCodeService.name);
  /**
   * Create a QR code for a given 'inputString'.
   *
   * @param filePath - File path for saving the generated QR code
   * @param inputString - Input which is going to be encoded into a QR code
   * @returns QR code string
   */
  public async createQrCode(filePath: string, inputString: string): Promise<void> {
    try {
      this.logger.debug('Create qr code');
      await QRCode.toFile(filePath, inputString, qrCodeConfig);
    } catch (e) {
      console.error(e);
    }
  }
}
