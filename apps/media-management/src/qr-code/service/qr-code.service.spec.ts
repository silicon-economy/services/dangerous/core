/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import * as fs from 'fs';
import qrCodeMockJson from './mock/qr-code.mock.json';
import { QrCodeService } from '@qrCodeModule/service/qr-code.service';
import { Config } from '@config';

describe('QrCodeService', () => {
  let service: QrCodeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QrCodeService],
    }).compile();

    service = module.get<QrCodeService>(QrCodeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should output QR code as svg path', async () => {
    const currentQrCodeUrl = __dirname + '/../..' + Config.CURRENT_QR_CODE_PATH;
    await service.createQrCode(currentQrCodeUrl, 'test');
    const currentQrCodeBytes: Buffer = fs.readFileSync(currentQrCodeUrl);
    expect(currentQrCodeBytes.toJSON()).toEqual(qrCodeMockJson);
  });
});
