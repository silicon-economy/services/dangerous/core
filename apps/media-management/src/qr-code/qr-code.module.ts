/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { QrCodeService } from '@qrCodeModule/service/qr-code.service';

@Module({
  controllers: [],
  providers: [QrCodeService],
  exports: [QrCodeService],
})
export class QrCodeModule {}
