== Chapter 10: Quality Requirements

"Quality is the totality of characteristics and values of a software product that relate to its ability to meet stated or implied requirements." [_Software Quality according to https://en.wikipedia.org/wiki/ISO/IEC_9126[ISO/IEC 9126; ISO 25010]]_

The most important quality goals have already been described in xref:01_introduction_and_goals.adoc#_quality_goals[Chapter 1: Quality Goals]. This section contains all quality requirements as quality tree with scenarios. We also capture quality requirements with lesser priority, which will not create high risks when they are not fully achieved. The quality scenarios depict the fundamental quality goals as well as other required quality properties. They allow the evaluation of decision alternatives.

=== Quality Tree

We use a tabular form of the quality tree. The following table lists the classifications (as https://en.wikipedia.org/wiki/ISO/IEC_9126[ISO/IEC 9126-1] characteristics and sub-characteristics with some _additional terms_) and the methods used to improve and to ensure software quality. _It is based on the "Bewertungsmatrix"._

[options="header"]
|===
|Quality Goal (ISO 9126-1) |Requirement (for Quality Improvement) |Test
of Requirement (Quality Assurance)
a|
*Maintainability*

(Testability, Stability, Analyzability, Changeability)

a|
* _Dangerous_ must be connected to the Silicon Economy infrastructures and Blockchain Networks of project partners
* _Dangerous_ must be designed in such a way that the crash of one component or one infrastructure does not lead to a crash of the other components.
* _Dangerous_ components are designed and implemented stateless. All states & configurations are stored in a database that is backed up regularly.
* On restart of _dangerous_ application, the current configuration is loaded from the database.
* _Dangerous_ respects all developer guidelines.

a|
* Test cases for putting into service and starting the application (e.g. DB available, interfaces).
* CI/CD pipeline, automatic compliance testing, code review, merge requests

a|
*Efficiency*

(Time behaviour,
Resource utilization, _Scalability_)

a|
* All components of _dangerous_ are designed to be scalable and redundant.
* If necessary, additional instances of the components can be started/stopped by the cluster to be able to react to load peaks.
+ The Blockchain network can be expanded and shrinked based on the number of participants.

a|
* No performance tests implemented yet

a|
*Usability*

(Understandability, Learnability, Operability, Attractiveness, _Simplicity_)

a|
*Web interfaces are only functions around the management of _dangerous_, its configuration, as well as for device management.
*Web interfaces visualize the current state of the _dangerous_ application, its components and connected devices.
*The _dangerous_ management frontend needs to appeal the administrators and for other stakeholders especially in demonstrations.
*For Admins, the effort required to integrate new functionalities must be minimal by documentation, guide and a SDK.
*The GUI Style guide is respected.
*Complete arc42 conformant software documentation and user guides
exist.
*Detailed documentation of public interfaces with JavaDoc &
OpenApi/Swagger

a|
* Document review

a|
*Reliability*

(Maturity, Fault tolerance/_Robustness_, +
Recoverability)

a|
* All components of _dangerous_ are designed to be scalable and redundant.
* If necessary, further instances of the components can be started/stopped by _dangerous_ in order to be able to react to load peaks.
* Appropriate unit tests exist, especially for error cases and boundary values.
* The architecture is designed such that methods depending on external services can be tested without having the external service available.

a|
* Unit tests (Jest, GoLang) following code package structure
* Using SonarCube during development and during the build process ensures a code coverage of at least 80%.
* All external dependencies are mockable, e.g. for unit tests.

a|
*Functionality*

(Suitability, Accuracy, Interoperability, +
Security, Completeness)

a|
* All functional requirements are fulfilled and implemented accurately and secure.
* Explicit, object-oriented domain model.
* Complete arc42 documentation, Jira tickets,
* The code is documented completely using JavaDoc conventions.

a|
* Unit tests
* High test coverage as a safety net
* Document Review
* Code Review

|===