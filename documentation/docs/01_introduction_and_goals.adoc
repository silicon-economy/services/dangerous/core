== *Chapter 1: Introduction and Goals*

=== *Introduction, Requirements, Goals & Stakeholders*

==== *Introduction*

===== *Introduction to Blockchain Europe*
The aim of the »Blockchain Europe« project is to establish the European Blockchain Institute in North-Rhine Westphalia (NRW), therefore we will be driving blockchain technology forwards together with companies and other research institutions. This sees a unique European institute being created to advance digitization in science and practice. Particularly in logistics and supply chain management many different, economically independent partners have to work closely together even though they do not inevitably have complete trust in each other. The focus of our research is therefore on open and integrated solutions that can be used by all players on the market. This will make it possible to digitally connect entire logistics chains from end to end. In our research and demonstration center we will show the opportunities blockchain provides live and oriented to applications while offering specific examples for practical uses.

===== *Silicon Economy - The big picture*
In a Silicon Economy (SE), services are executed and used via platforms. A Silicon Economy Platform consists of five different kinds of major components, namely one or more SE Services, one or more IDS connectors, a Logistics Broker, an optional IoT Broker and an optional Blockchain Full-Node. This platform is the environment of SE Services, its location does not matter (e.g., IT environment of a company, a cloud environment, etc.).

SE Services are professional (logistics) services, consisting of IT and/or physical services. They are part of a functional (logistics) process and can be linked and subsequently orchestrated in the sense of that process.

image::../images/chapter_1/1.1_Silicon-Economy-Overview.png[Overview silicon economy platform]


* **IDS Connector (not integrated):** Industrial Data Space Connector -Used for secure communication of various contents and management of access rights to various data.

* **Logistik Broker (not integrated):** Logistics brokers organize logistics services and their processing and connect the providers of logistics services with customers and users. This applies equally to internal and external logistics.

* **SE Service (use case related):** SE Services are professional (logistics) services, consisting of IT and/or physical services. They are part of a functional (logistics) process and can be linked and subsequently orchestrated in the sense of that process. Furthermore, SE Services can be integrated into existing structures, and must be able to be executed and monitored. SE Services are documented. SE Services must be able to be booked and accounted for and have a (technical) description that includes all information about the function of the service and the conditions of use. Finally, SE Services must be able to be supplied with data and must be able to be called via the IDS.

* **IoT Broker (integrated):** IoT brokers and their components are essential data sources for the Silicon Economy. They connect the cyberphysical systems (CPS) such as smart containers and pallets, as well as smart machines with the Silicon Economy - e.g., with cloud services and platforms. IoT brokers trade near-real-time data and information over fixed and (especially) mobile networks such as 5G and low power wide area networks (LPWAN such as NB-IoT...). The development of open source devices and open source software for IoT devices and CPS are also located here.

* **Blockchain (integrated):** The blockchain is a decentralized, distributed, cooperative data store. It enables the secure exchange of data in networks without recourse to an intermediary. The basis of blockchain technology is the so-called "distributed ledger", a distributed stored logbook, which contains entries with information. These entries are time-stamped and also contain a reference to the previous entry.  +

Furthermore, SE Services can be integrated into existing structures, and must be able to be executed and monitored. SE Services are documented. SE Services must be able to be booked and accounted for and have a (technical) description that includes all information about the function of the service and the conditions of use. Finally, SE Services must be able to be supplied with data and must be able to be called via the IDS.

===== *dangerous - A Blockchain Europe development project*
dangerous = Digitization and Automation for Dangerous Goods Management

**Logistical challenges & challenges regarding handling with sensitive data:**

* Dangerous goods management is legally regulated
* Approx. 149 million tons of dangerous goods transported on German streets (cf. Statistisches Bundesamt (2021) – https://www.destatis.de/DE/Themen/Branchen-Unternehmen/Transport-Verkehr/Publikationen/Downloads-Querschnitt/gefahrguttransporte-2080140187004.pdf?$$_$$$$_$$blob=publicationFile[Gefahrguttransporte 2018: Ergebnisse der Gefahrgutschätzung, p. 6 f.])
* Compliance: 2 thousand violations by 10 thousand inspections (cf. Bundesamt für Güterverkehr (2021) – https://www.balm.bund.de/SharedDocs/Downloads/DE/Statistik/Strassenkontrollstatistik/2021/Gefahrgutrecht$$_$$2021.pdf;jsessionid=CCD238503E9F0A43005A06388D3AA3A0.live11313?$$_$$$$_$$blob=publicationFile&v=1[Ergebnisse der Kontrollen im Gefahrgutrecht])
* Processing of sensitive data, e.g. contractual issues, type of dangerous goods, timestamp of the goods movement, sender, carrier and receiver data
* Trust is based on the actors, not in the system
* Continuous new legal and regulatory requirements
* Usage of current movement/transport as well as real-time data regarding the dangerous goods (traceability)
* Reliable protection of sensitive data while increasing data transparency at the same time
* Utilization of the generated data for an efficient auditing process and plausibility checks

**Vision: Blockchain User Portal for Dangerous Goods Management:**

* Blockchain User Portal, especially for transport-related dangerous goods information
* Development of open source modules for a complete and persistent documentation as well as traceability
* Electronic transport document according to ADR
  ** Less paperwork, conservation of resources, evidential value, more efficient preparation
* Provision of all relevant data and information
  ** Also for further actors, like the surveillance authorities and rescue and emergency services
* Bundling of all information in one place
  ** Replacement of various heterogenous systems
* Dangerous goods documentation in combination with tracking and tracing data will trigger Smart Contracts

**Objectives:**

* Automation of dangerous goods processes
* Digitized dangerous goods management and handling
* Smart Contracts to optimize and maximize process agility by compliance of the legal regulations (vgl. Gefahrgutverordnung Straße, Eisenbahn und Binnenschifffahrt – https://www.gesetze-im-internet.de/ggvseb/BJNR138900009.html[GGVSEB] / vgl. Gefahrgutbeförderungsgesetz – http://www.gesetze-im-internet.de/gefahrgutg/BJNR021210975.html[GGBefG] / vgl. Gefahrgut-Kontrollverordnung – http://www.gesetze-im-internet.de/ggkontrollv/BJNR130600997.html[GGKontrollV])

**Potentials:**

* Exchange of information and traceability due to the usage of blockchain technology within transport preparation
  ** Electronic transport document
  ** Additional dangerous goods specific documents
  ** Aim: Paperless dangerous goods handling
* Automation of processes by utilization of Smart Contracts during transportation process
  ** Track & Trace and rational/logical approaches
  ** Aim: Compliance to and proofing of legal conformity

image::../images/chapter_1/callenges.png[]

For more information, please see also: https://blockchain-europe.nrw/en/dangerous-goods/[Blockchain Europe]

For a more detailed view about the components and internal structure of the dangerous development project please also see chapter xref:05_building_block_view.adoc#_building_blocks_level_3[Chapter 5: Building Blocks - Level 3].

==== *Requirements Overview*

From an end user's point of view, dangerous improves the transparency of dangerous goods processes between companies, control bodies and emergency services. Furthermore the solution automates manual tasks and digitizes paper-based processes. The improvements are achieved for dispatchers, packers, and loaders in the consigning company, for drivers and operational logistic service providers in the carrying company and for gatekeepers, unloaders, purchasers in the consignee's company at the current release status:

* The dispatcher is assisted in providing all the required information regarding the dangerous goods that are to be transported and has tamper-proof documentation of the information under his responsibility. In addition, environmental gains through reduction of paper based documentation can be achieved.
* The packer receives information about the dangerous good that he has to prepare and is provided with a corresponding label for the package.
* The loader gets a tamper-proof confirmation of the driver that the dangerous goods were in good quality and of the right type and quantity when he loaded them onto the vehicle.
* The driver is provided with a digital and mobile accessible version of the transport document that can be updated in real time according to the actual process and thus minimizes his risk of penalties. Additionaly, he gets a tamper-proof confirmation that his vehicle fulfilled the quality requirements and had the required equipment when he left the consignor's company.
* The operational logistic service provider gains early insight into upcoming transport demands and thus increases his planning capabilities.
* The gatekeeper can control a driver's transport document and authorize his access to the company's property efficiently.
* The unloader can check and confirm tamper-proof that the dangerous goods were in good quality and of the right type and quantity when he unloaded them from the driver's vehicle.
* The purchaser gains transparency about the process status in real-time.

We summarize the requirements for *Blockchain Europe*, which are detailed in the Jira-Board.

=== What is the _dangerous_ Blockchain solution

* A solution to digitize and standardize the information sharing between companies in dangerous goods handling processes
  ** Avoiding paper use by digitising dangerous goods transport documents while complying with relevant regulations (ADR)
  ** Automatic allocation of information in the national supply chain
  ** Transfer and further processing of transport document data in visual inspections of prepared dangerous goods & corresponding transport vehicles
  ** Digitalised information transfer between companies with independent IT solutions
* A solution to improve transparency in dangerous goods handling processes
  ** Information sharing in all stages of the process to avoid contradictory and outdated data statuses
  ** Access to process information by other actors controlled via an authorisation module
* Tamper-Proof documentation of responsibilities and data provenance in dangerous goods handling

**Essential Features**

* Bridging information asymmetries and improve data integrity between supply chain partners through a shared truth on the Blockchain
* Mapping of information about dangerous goods into tokens on the blockchain
* Compliance with relevant laws and regulat while leveraging already existing digitalisation potentials

**Functional requirements**

* Data Sovereignity - Data access to corporate data can be controlled and restricted so that the value of corporate data can be disseminated in a controlled manner.
* Irreversibility - Written information and transactions are stored unchangeably and can only be updated via updates to the existing information but not removed.
* Interoperability - Easy integration into existing IT ecosystems in different companies and countries.
* Process-fit - The dangerous service fits to existing basic logistical and customs processes. It does not require major process changes, to be usable.




==== *Quality Goals*

The following table describes the goals of *Blockchain Europe* classified by https://en.wikipedia.org/wiki/ISO/IEC_9126[ISO/IEC 9126-1] characteristics.

[width="100%",cols="37%,63%",options="header",]
|===
|*Quality Goal* |*Motivation/Description*
|Reliability (Robustness) |Due to the decentralized nature of a blockchain, Reliability (Robustness) is inherent.

|Efficiency (Scalability) |Depending on the type of blockchain solution, additional nodes can join the blockchain network at will or after prior approval, e.g. by a committee.

|Usability (Simplicity) |For tech-savvy individuals of a Silicon Economy, the use case-specific use of blockchain solutions and functionalities of the dangerous system must be as simple as possible. Additionally, as the dangerous system interacts with, and is partly composed of Blockchain Europe basic components, an integration with these, as well as third-party services must be possible.

|Usability (Attractiveness) |The dangerous modules are generally not used on their own, but in the course of a use case. In the course of this, the modules must appeal to the blockchain administrators and other stakeholders especially in demonstrations.

|Functionality (Secure updates) |Minor updates for blockchain solutions and functionalities of the dangerous system must not lead to an error in the application. There might be some major releases that require an adaption of the application that uses the blockchain solutions and functionalities of the dangerous system.

|Functionality (Completeness) |Every transaction must be broadcasted and processed correctly in the blockchain network.

|Usability / Maintainability |The usage of any modules of the dangerous system must be clear and at the same time include all configuration options for a specific use case.

|Usability / User-Friendliness |For the end user, the visualized information as well as the required steps/actions have to be clear and easy to understand.

|Open Source Capability |Develop open source components with high logistic relevance in novel logistic applications.

|Independent System/Platform |Develop and deliver high-quality Silicon Economy-compatible open source code via an independent platform.

|Continuous further development (by industry) |Achieve usage and (further) development of open source components by industry stakeholders.

|Awareness |Achieve a high level of awareness in business, science, politics & society.

|Agility |With the blockchain and its associated processes and use cases, a complex system is be developed - with the aim to establish, an focus on, an agile organization.

|Reliabilty |The blockchain guarantees secure data traffic with sensitive data. And therefore protects the sensitive data.

|Transparency |All services must gather and process data fast to be efficient and also to obtain transparency.
|===

[#_stakeholders]
==== *Stakeholders*

The following table lists the most important stakeholders regarding the dangerous user portal (stakeholder classification and stakeholder cluster, involved stakeholders) and their respective descriptions, intentions as well as goals and benefits:

[options="header"]
|===
|Stakeholder Group Classification |Stakeholder Cluster |Involved Stakeholders |Description  |Intentions |Goals & Benefits

|Industry - Logistic Actors |Consignor |Stakeholder cluster "consignor" includes following actors: +
- Consignor +
- Packer +
- Loader +
- Filler +
- Tank-container / Portable tank operator +
- Seller +
- Declarant +
- Exporteur +
- Client of the consignor
|All kind of stakeholders and actors which are involved in preparation and sending of dangerous goods, hazardous commodities and hazardous raw materials.  |- More efficient operational processes +
- Reducing costs +
- Reducing time +
- Minimisation of errors
|- Simplified & consistent data exchange as well as collaboration between involved internal and external stakeholders and actors +
- Fewer paper printouts +
- Tamper-Proof documentation of all data related with dangerous goods transport and handling +
- Less time & effort for preparation and carrying out of dangerous goods transport and handling +
- Less time checking integrity of data +
- Increased internal compliance security aspects +
- Higher transparency of the current transport and handling status and the dangerous goods processes
|Industry - Logistic Actors |Carrier |Stakeholder cluster "carrier" includes following actors: +
- Carrier +
- Vehicle Operator +
- Transport service provider +
- Logistic service provider
|All kind of companies and actors which are involved in moving and transporting/shipping dangerous goods, hazardous commodities and hazardous raw materials.
|- More efficient operational processes +
- Reducing costs +
- Reducing time +
- Minimisation of errors
|- Simplified & consistent data exchange as well as collaboration between involved internal and external stakeholders and actors +
- Fewer paper printouts +
- Tamper-Proof documentation of all data related with dangerous goods transport and handling +
- Less time & effort for preparation and carrying out of dangerous goods transport and handling +
- Less time checking integrity of data +
- Increased internal compliance security aspects +
- Higher transparency of the current transport and handling status and the dangerous goods processes
|Industry - Logistic Actors |Consignee |Stakeholder cluster "consignee" includes following actors: +
- Consignee +
- Unloader +
- Buyer +
- Importeur
|All kind of stakeholders and actors which are involved in receiving of dangerous goods, hazardous commodities and hazardous raw materials.
|- More efficient operational processes +
- Improved predictability +
- Reducing costs +
- Reducing time +
- Minimisation of errors
|- Simplified & consistent data exchange as well as collaboration between involved internal and external stakeholders and actors +
- Fewer paper printouts +
- Tamper-Proof documentation of all data related with dangerous goods transport and handling +
- Less time & effort for preparation and carrying out of dangerous goods transport and handling +
- Less time checking integrity of data +
- Increased internal compliance security aspects +
- Higher transparency of the current transport and handling status and the dangerous goods processes
|Industry - Logistic Actors
|- Consignor +
- Carrier +
- Consignee |Dangerous Goods Commissioner (not integrated yet)
|Internal specific actors, from all of the different stakeholder clusters, who are the responsible dangerous goods safety advisors, on the companies' premisis.
|- More efficient operational processes +
- Improved documentation +
- Reducing time +
- Minimisation of errors
|- Simplified & consistent data exchange as well as collaboration between involved internal and external stakeholders and actors +
- Fewer paper printouts +
- Tamper-Proof documentation of all data related with dangerous goods transport and handling +
- Less time checking integrity of data +
- Increased internal compliance security aspects
|Authorities - Public Actors |Inspection & Control and Surveillance Authorities (partly integrated yet)
|Dangerous Goods Inspector: +
- Federal Office for Goods Transport AG (in Germany BAG (Bundesamt für Güterverkehr)) +
- Police (Federal and State Police) +
Dangerous Goods Control during border crossing: +
- Customs
|Public authorities and stakeholders which are responsible regarding the inspection, control and surveillance of dangerous goods transportation, dangerous goods storage and the export and import of dangerous goods.
|- Faster, improved and more transport controls
- More efficient operational processes
- Reducing time Minimisation of errors
- Higher detection of violations
|- Simplified as well as proper inspection and control of the transported dangerous goods +
- Reliable, truthful & real-time data +
- Less time checking integrity of data +
- Simplified & consistent data exchange as well as collaboration between further involved internal and external stakeholders and actors +
- Fewer paper printouts +
- Tamper-Proof documentation of all data related with dangerous goods transport and handling +
- Higher transparency of the current transport and handling status and the dangerous goods processes
|Authorities - Public Actors |Public Safety Authorities and Organizations (not integrated yet) |- Police (Federal and State Police) +
- Fire Brigade +
- Rescue and Emergency Services +
- Federal Agency for Technical Relief
|Public authorities and stakeholders which are responsible for the public safety and hazard prevention & management, for instance in case of dangerous goods transport accidents.
|- More efficient operational processes +
- Reducing time (response time and operating time) +
- Faster, improved and more secure rescue operations +
- Improved, faster collaboration with further actors and stakeholders
|- Reliable, truthful & real-time data of all of the transported hazardous goods (type and quantity) +
- Faster assistance and information exchange between further involved internal and external stakeholders and actors +
- Reducing risks (e.g. environmental, human) and enables a more efficient rescue process and overall safety +
- Tamper-Proof documentation of all data related with dangerous goods transport and handling
|===


