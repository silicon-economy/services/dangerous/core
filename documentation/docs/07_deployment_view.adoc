== Chapter 7: Deployment View (Developer)

Software does not run without hardware. This view describes the deployment of _dangerous_. It describes the technical infrastructure used to execute _dangerous_, with infrastructure elements like clouds, computing nodes, and net topologies as well as other infrastructure elements and the mapping/installation of compiled software building block instances to that infrastructure elements.

=== General Deployment Decisions

The _dangerous_ is part of an SE Platform Instance (i.e a K8s Cluster instance). In such an SE Platform Instance the following number of components may be deployed:

Components of an SE Platform

. One or more SE services → Project overview, and https://oe160.iml.fraunhofer.de/wiki/display/IOT/Chapter+4%3A+Solution+Strategy#Chapter4:SolutionStrategy-4.2CommonSEReferenceArchitecture[SE Reference Architecture]
. IoT Broker → IoT Broker
. Blockchain Full-Node → Blockchain Broker

An SE platform is

. The environment of SE services
. Location does not "matter" (e.g., enterprise IT environment, a cloud environment, ...)

=== Deployment of the _dangerous_

The _dangerous'_ sub components are deployed separately as containers into a Kubernetes cluster used as execution environment. Each sub component is configured via Kubernetes resources (i.e. YAML files) which are deployed alongside the sub component container. +

By design, those _dangerous_ sub component deployments may be scaled to an arbitrary number of instances to increase performance as well as redundancy. However, true redundancy can only be gained if the underlying execution platform comprises multiple independent failure domains such as multiple machines, data centers or geographic locations and sub component deployments span multiple such failure domains.

==== Current Deployment Addresses

[width="83%",cols="34%,32%,15%,19%",options="header",]
|===
|Microservice |Address |Interfaces |Swagger (if available)
|dangerous.frontend a|
* https://dangerous.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.frontend]
* https://dangerous-staging.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.frontend.staging]

|REST |-

|dangerous.backend.dangerous.goods.gateway a|
* https://dangerous-backend-dangerous-goods-gateway.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.backend.dangerous.goods.gateway]
* https://dangerous-backend-dangerous-goods-gateway-staging.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.backend.dangerous.goods.gateway.staging]

|REST/AMQP
|https://dangerous-backend-dangerous-goods-gateway-staging.apps.blockchain-europe.iml.fraunhofer.de/api/[Dangerous Goods Gateway Swagger]
https://dangerous-backend-dangerous-goods-gateway-staging.apps.blockchain-europe.iml.fraunhofer.de/api/[Dangerous Goods Gateway Staging Swagger]

|dangerous.backend.transport.paper.gateway a|
* https://dangerous-backend-transport-paper-gateway.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.backend.transport.paper.gateway]
* https://dangerous-backend-transport-paper-gateway-staging.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.backend.transport.paper.gateway.staging]

|REST/AMQP a|
https://dangerous-backend-transport-paper-gateway-staging.apps.blockchain-europe.iml.fraunhofer.de/api/#/Misc/AppController_getStatus[Transport Document Gateway Swagger]

https://dangerous-backend-transport-paper-gateway-staging.apps.blockchain-europe.iml.fraunhofer.de/api/#/Misc/AppController_getStatus[Transport Document Gateway Staging Swagger]

|dangerous.backend.data.management a|
* https://dangerous-backend-data-management.apps.blockchain-europe.iml.fraunhofer.de[dangerous.backend.data.management]
* https://dangerous-backend-data-management-staging.apps.blockchain-europe.iml.fraunhofer.de[dangerous.backend.data.management.staging]

|AMQP |-

|dangerous.backend.tendermint.service a|
* https://dangerous-backend-tendermint-service.apps.blockchain-europe.iml.fraunhofer.de[dangerous.backend.tendermint.service]
* https://dangerous-backend-tendermint-service-staging.apps.blockchain-europe.iml.fraunhofer.de[dangerous.backend.tendermint.service.staging]

|AMQP/REST |-

|dangerous.backend.blockchain a|
* https://dangerous-blockchain.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.backend.blockchain]
* https://dangerous-blockchain-staging.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.backend.blockchain.staging]

|REST
|http://dangerous-blockchain-service-dangerous-staging.apps.blockchain-europe.iml.fraunhofer.de/[Blockchain Staging Swagger]

|dangerous.backend.data.lookup a|
* https://dangerous-backend-data-lookup.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.backend.data.lookup]
* https://dangerous-backend-data-lookup-staging.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.backend.data.lookup.staging]

|AMQP |-

|dangerous.backend.business.logic a|
* https://dangerous-backend-business-logic.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.backend.business.logic]
* https://dangerous-backend-business-logic-staging.apps.blockchain-europe.iml.fraunhofer.de/[dangerous.backend.business.logic.staging]

|AMQP |-
|===

=== Template

* Deployed manually by operator ahead of other dangerous sub components
* Triple redundancy (no performance benefit, cannot be meaningfully scaled to more than four instances)
* Unique access credentials are created during deployment and provided to other sub components as Kubernetes resource inside the cluster
* Not accessible from outside the execution environment
* Uses large amounts of persistent storage (actual size may vary by use case) provided by the execution environment

==== Components

The following points apply to all components

* Not accessible from outside the execution environment
* Uses large amounts of persistent storage (actual size may vary by use case) provided by the execution environment

==== Blockchain Components

Currently in respect to data persistence a Cosmos module based on the Ewallet base component is used as an abstraction layer to provide a comprehensive access and permission layer. This means:

. A Blockchain instance must be configured and deployed. This instance could either be
.. A pre-configured blockchain with an existing transaction history and pre-configured Ewallet accounts.
.. A completely vanilla blockchain with a pre-generated genesis block.
. In either case it is recommended to provide services/endpoints with both wallet accounts and their passwords until the advanced base component "edocument" becomes available.
. Data can be persisted or requested through the Blockchain connector which extracts blockchain specific data and prepares them for processing through the following abstraction layers:
.. From Connector ~> Ewallet Microservice. The actual data is saved as part of a Token which belongs to its own segment ("DangerousSegment") and its parent Ewallet Address ("DangerousWallet")
.. Ewallet Microservice ~> CosmosLightClient. It is then passed as a Tx Msg as part of a message which is subsequently signed and passed to the Blockchain Rest API for broadcasting.
.. CosmosLightClient ~> CosmosEwalletModule. The module accepts signed messages, performs rudimentary tests and then broadcasts them to all known Blockchain Nodes and persists it in its own storage.
. All Data is as such saved for long term purposes. If a purge is desired, the blockchain may simply be re-started. If distribution is required then the new nodes require the genesis file and a connection to the existing dangerous chain.
.. As it stands, the connector must be restarted after such a purge, as it requires re-authentication with the instance. This work-around measure has been taken, as the implementation of "ewallet" is only temporary.

Data may be directly queried through light node access. This is can be achieved through a different means. The actual services communicate as per diagram through a series of controllers and microservices. However through the following steps a direct access is possible too:

. Save a Dangerous Goods Transport Paper through the front-end, or other means.
. Access the logs of the node instance. In respect to our deployment in the frame of _Dangerous,_ this may be achieved by choosing any running blockchain instance pod and clicking "logs".
. All data hashes in the pods are streamed as log statements, both the block height as well as the hash can be accessed. For easy access the block height is sufficient.
.. [2021-05-06|11:10:06.910] Executed blockmodule=state
*height=17271* validTxs=1 invalidTxs=0
.. [2021-05-06|11:10:06.911] Committed state module=state *height=17271 txs=1* appHash=13BEB18723AD03DEFC2AE3106633E0A14E454EDB6EB1E4C964DE5BBC3DC94952
. Taking the block number/height and querying the API directly provided through the CosmosSDK through the following endpoint:
.. https://dangerous-backend-blockchain.apps.blockchain-europe.iml.fraunhofer.de/blocks/17271
.. Yields the following JSON:
+
**block.json**

[source,syntaxhighlighter-pre]
----
{
"block_id": {
"hash": "F858DD47D6855353C4925F7310CFB442D6F2447CD8C8CFBCF730E13F9A83E3E9",
"parts": {
"total": "1",
"hash": "BD6C58F7F450865AB77E382B8EECD52A198195F598B1FA8858B1100F6056327D"
}
},
"block": {
"header": {
"version": {
"block": "10",
"app": "0"
},
"chain_id": "ewallet",
"height": "17271",
"time": "2021-05-06T11:10:01.893406788Z",
"last_block_id": {
"hash": "B2A2E6FBE559155F05028248FEA18F1E70F511FE648B2D2B34A8769CDEC31B40",
"parts": {
"total": "1",
"hash": "BDB47EED43D107CBCFD170A6E5870F87B8464629928F8F7813539A6303FAEA3F"
}
},
"last_commit_hash": "5807CD0DB2A9F3A6A709297FC75342C05A61E77378F87F05D5385B3DCD88FF49",
"data_hash": "BF0304B83F5C52AC9F1B7951822AEC9A0BBA40BFF702C1FD6687E6204E7240AE",
"validators_hash": "6A2C6593F29945A5DB1EE55B0B10BD96586DE0F1A44208F58965D8FB95C2CD07",
"next_validators_hash": "6A2C6593F29945A5DB1EE55B0B10BD96586DE0F1A44208F58965D8FB95C2CD07",
"consensus_hash": "048091BC7DDC283F77BFBF91D73C44DA58C3DF8A9CBC867405D8B7F3DAADA22F",
"app_hash": "232F28660DEC21E8B10C65B5DF74C7C19518DF4FAD97FF2F1E235D9CF5B768D7",
"last_results_hash": "6E340B9CFFB37A989CA544E6BB780A2C78901D3FB33738768511A30617AFA01D",
"evidence_hash": "",
"proposer_address": "2BFAB0191904FC2FA7503B564B8AB3909BDEA61A"
},
"data": {
"txs": [
"7xUoKBapCvAUv3v5IgokMjA4YTZmMGQtZDk5Yy00OGRkLTkxODktNmZiZjZiYjFjNTdkEhT9sc9/XVR+iFOh8RRlqcxo8VBzahokMTY1NDVkNDEtNmNlZC00YmVmLWIwN2UtZTBjNjlhY2ZlNmU2IAEqpRN7ImNhcnJpZXIiOnsibmFtZSI6ImRhbmdlcm91cyAtIEdlZmFocmd1dGxvZ2lzdGlrIG1pdCBCbG9ja2NoYWluIiwiZHJpdmVyIjoiQmVuamFtaW4gRGllaGFyZCIsImxpY2Vuc2VQbGF0ZSI6IkRSLUVJMzMzIn0sImZyZWlnaHQiOnsiYWRkaXRpb25hbEluZm9ybWF0aW9uIjoiQmVmw7ZyZGVydW5nIG9obmUgRnJlaXN0ZWxsdW5nIG5hY2ggQURSIDEuMS4zLjYiLCJsb2FkIjoiIiwib3JkZXJzIjpbeyJjb25zaWduZWUiOnsibmFtZSI6Ik1pbmlzdGVyaXVtIGbDvHIgV2lydHNjaGFmdCwgSW5ub3ZhdGlvbiwgRGlnaXRhbGlzaWVydW5nIHVuZCBFbmVyZ2llIGRlcyBMYW5kZXMgTm9yZHJoZWluLVdlc3RmYWxlbiAoTVdJREUpIiwiYWRkcmVzcyI6eyJzdHJlZXQiOiJCZXJnZXIgQWxsZWUiLCJudW1iZXIiOiIyNSIsInBvc3RhbENvZGUiOiI0MDIxMyIsImNpdHkiOiJEw7xzc2VsZG9yZiIsImNvdW50cnkiOiJEZXV0c2NobGFuZCJ9LCJjb250YWN0Ijp7Im5hbWUiOiJFcmlrYSBNdXN0ZXJtYW5uIiwiZGVwYXJ0bWVudCI6IkFCQyIsIm1haWwiOiJlcmlrYUBleGFtcGxlLmNvbSIsInBob25lIjoiKzQ5ICgwKSAyMTEvMDAwMDAtMCJ9fSwiaWQiOiIiLCJvcmRlclBvc2l0aW9ucyI6W3siZGFuZ2Vyb3VzR29vZCI6eyJkZXNjcmlwdGlvbiI6IkFDRVRPTiIsInVuTnVtYmVyIjoiMTA5MCIsImxhYmVsMSI6IjMiLCJwYWNraW5nR3JvdXAiOiJJSSIsInR1bm5lbFJlc3RyaWN0aW9uQ29kZSI6IihEL0UpIiwidHJhbnNwb3J0Q2F0ZWdvcnkiOiIyIiwicG9sbHV0aW5nIjpmYWxzZX0sInBhY2thZ2UiOiJJQkMiLCJjb2RlIjoiMzFBIiwicXVhbnRpdHkiOjIsInVuaXQiOiJMIiwiaW5kaXZpZHVhbEFtb3VudCI6NzUwLCJwb2xsdXRpbmciOmZhbHNlLCJ0cmFuc3BvcnRQb2ludHMiOjQ1MDAsInRvdGFsQW1vdW50IjoxNTAwfSx7ImRhbmdlcm91c0dvb2QiOnsiZGVzY3JpcHRpb24iOiJNRVRIQU5PTCIsInBvbGx1dGluZyI6ZmFsc2UsInVuTnVtYmVyIjoiMTIzMCIsImxhYmVsMSI6IjMiLCJsYWJlbDIiOiI2LjEiLCJsYWJlbDMiOiIiLCJwYWNraW5nR3JvdXAiOiJJSSIsInR1bm5lbFJlc3RyaWN0aW9uQ29kZSI6IihEL0UpIiwidHJhbnNwb3J0Q2F0ZWdvcnkiOiIyIn0sInBhY2thZ2UiOiJJQkMiLCJjb2RlIjoiMzFBIiwicXVhbnRpdHkiOjIsInVuaXQiOiJMIiwiaW5kaXZpZHVhbEFtb3VudCI6MTAwMCwicG9sbHV0aW5nIjpmYWxzZSwidHJhbnNwb3J0UG9pbnRzIjo2MDAwLCJ0b3RhbEFtb3VudCI6MjAwMH1dfSx7ImNvbnNpZ25lZSI6eyJuYW1lIjoiQnVuZGVzbWluaXN0ZXJpdW0gZsO8ciBWZXJrZWhyIHVuZCBkaWdpdGFsZSBJbmZyYXN0cnVrdHVyIChCTVZJKSIsImFkZHJlc3MiOnsic3RyZWV0IjoiUm9iZXJ0LVNjaHVtYW4tUGxhdHoiLCJudW1iZXIiOiIxIiwicG9zdGFsQ29kZSI6IjUzMTc1IiwiY2l0eSI6IkJvbm4iLCJjb3VudHJ5IjoiRGV1dHNjaGxhbmQifSwiY29udGFjdCI6eyJuYW1lIjoiTWF4IE11c3Rlcm1hbm4iLCJkZXBhcnRtZW50IjoiRzE2IiwibWFpbCI6Im1heEBleGFtcGxlLmNvbSIsInBob25lIjoiKzQ5ICgwKSAyMjgvMDAwMDAtMCJ9fSwiaWQiOiIiLCJvcmRlclBvc2l0aW9ucyI6W3siZGFuZ2Vyb3VzR29vZCI6eyJkZXNjcmlwdGlvbiI6Ik9SR0FOSVNDSEVTIFBFUk9YSUQgVFlQIEQsIEZMw5xTU0lHLCBOLkEuRy4sIChFTlRIw4RMVCBBQ0VUWUxBQ0VUT05QRVJPWElEKSIsInBvbGx1dGluZyI6ZmFsc2UsInVuTnVtYmVyIjoiMzEwNSIsImxhYmVsMSI6IjUuMiIsImxhYmVsMiI6IiIsImxhYmVsMyI6IiIsInBhY2tpbmdHcm91cCI6IklJIiwidHVubmVsUmVzdHJpY3Rpb25Db2RlIjoiKEQpIiwidHJhbnNwb3J0Q2F0ZWdvcnkiOiIyIn0sInBhY2thZ2UiOiJJQkMiLCJjb2RlIjoiMzFBIiwicXVhbnRpdHkiOjQsInVuaXQiOiJMIiwiaW5kaXZpZHVhbEFtb3VudCI6NzUwLCJwb2xsdXRpbmciOmZhbHNlLCJ0cmFuc3BvcnRQb2ludHMiOjkwMDAsInRvdGFsQW1vdW50IjozMDAwfV19XSwidG90YWxUcmFuc3BvcnRQb2ludHMiOjE5NTAwLCJ0cmFuc3BvcnRhdGlvbkluc3RydWN0aW9ucyI6IkJpdHRlIG5hY2ggVmVybGFkdW5nIHJlaW5pZ2VuLiJ9LCJzZW5kZXIiOnsibmFtZSI6IkZyYXVuaG9mZXItSW5zdGl0dXQgZsO8ciBNYXRlcmlhbGZsdXNzIHVuZCBMb2dpc3RpayBJTUwiLCJhZGRyZXNzIjp7InN0cmVldCI6Ikpvc2VwaC12b24tRnJhdW5ob2Zlci1TdHJhw59lIiwibnVtYmVyIjoiMi00IiwicG9zdGFsQ29kZSI6IjQ0MjI3IiwiY2l0eSI6IkRvcnRtdW5kIiwiY291bnRyeSI6IkRldXRzY2hsYW5kIn0sImNvbnRhY3QiOnsibmFtZSI6IkdpdXNlcHBlIFBlcmV6IiwiZGVwYXJ0bWVudCI6Ik9FLTExMCIsInBob25lIjoiMDA0OSAyMzEgOTc0MyAyNzgiLCJtYWlsIjoiZ2l1c2VwcGUucGVyZXpAaW1sLmZyYXVuaG9mZXIuZGUifX0sInN0YXR1cyI6ImNyZWF0ZWQiLCJjcmVhdGVkRGF0ZSI6MTYyMDI5OTM5NjE5Mn0yFP2xz39dVH6IU6HxFGWpzGjxUHNqOhxEYW5nZXJvdXNHb29kc1RyYW5zcG9ydFBhcGVyQipJbml0aWFsaXplZCBUcmFuc3BvcnQgUGFwZXIgb24gQmxvY2tjaGFpbi4SChD//4+7utat8A0aagom61rphyEDZofvznPdEOfvwnqRsq/pWvzgUCPL1HamJ+AKho40QgESQLWYUgcH9NVEKPsXLW4tEz+DhaXM3oPsKY9EaJayn67YZzK75CLvLCBg9IIj2rdxFayQnZhcp1aKIHEgLC4xiXU="
]
},
"evidence": {
"evidence": null
},
"last_commit": {
"height": "17270",
"round": "0",
"block_id": {
"hash": "B2A2E6FBE559155F05028248FEA18F1E70F511FE648B2D2B34A8769CDEC31B40",
"parts": {
"total": "1",
"hash": "BDB47EED43D107CBCFD170A6E5870F87B8464629928F8F7813539A6303FAEA3F"
}
},
"signatures": [
{
"block_id_flag": 2,
"validator_address": "2BFAB0191904FC2FA7503B564B8AB3909BDEA61A",
"timestamp": "2021-05-06T11:10:01.893406788Z",
"signature": "6MYxUpNv7eh+1H19OKvtF8Bz1SCXG3E8aouDx63AF7QkAQEUQg/hoyJJgrsedNv6I2riSQtcye/2Ql/NkcTpAQ=="
}
]
}
}
}
----
.. Where actual transaction related data is being stored in the "txs" field.
.. This Data may be decoded through any base64 decoder. As an example through the following command:
.. {blank}

**Decoding a String**

[source,syntaxhighlighter-pre]
----
echo 'QmxvY2tjaGFpbiE=' | base64 --decode
----
