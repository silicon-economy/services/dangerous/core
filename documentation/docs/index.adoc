:toc:
:toclevels: 3
:sectnumlevels: 5
:icons: font
:copyright: Blockchain Europe

= Software Documentation for dangerous

[[section-preface]]
== Preface

Motivation
The vision of a Silicon Economy (SE) is a data and platform economy, where people, companies, autonomous vehicles and IoT devices interact with each other to achieve logistics goals. It uses open, federal structures, AI, autonomous devices, distributed ledger technologies and ensures data sovereignty.

The reference architecture of the Silicon Economy consists of a macro architecture as a common framework in which SE services composed of micro services are loosely embedded. The macro architecture essentially consists of an International Data Spaces (IDS) infrastructure as well as three types of brokers.

The dangerous project is (an SE service) of a Silicon Economy Platform. This documentation contains the description of all dangerous  Components, their context, interaction, constraints and implementation. This document is written to be published under the Open Logistics Foundation License 1.3. Open Source License.

=== Documentation Approach

The documentation of the individual projects is in English and is based on the link:https://docs.arc42.org/home/[arc42 template].

Each project maintains the software documentation in its corresponding GitLab repository according to this template.
It is important that the risk assessment (see <<Risks and Technical Debts>>) and the quality criteria (see <<Introduction and Goals>> and <<Quality Requirements>>) are described before the start of each project.

The link:https://agilemanifesto.org/[Agile Manifesto] says: “Working software over comprehensive documentation”.
It does not say “no documentation”.
Even in lean, agile and iterative development approaches, an appropriate amount of documentation will help numerous stakeholders doing a better job.
arc42 is completely process-agnostic, and especially well-suited for lean and agile development approaches.
It is supposed to be used “on demand” - you provide only the type or amount of information required for your specific documentation purpose.
Documentation shall be part of your Definition-of-Done.
Therefore, arc42 is automatically included in Scrum processes.

This documentation is written in link:https://docs.asciidoctor.org/asciidoc/latest/[asciidoc].

=== Main Authors

[cols="1,2",options="header"]
|===
|Name
|E-Mail

|Lukas Nikelowski
|mailto:lukas.nikelowski@iml.fraunhofer.de[]

|Sebastian Brüning
|mailto:sebastian.bruening@iml.fraunhofer.de[]

|Ingo Völkel
|mailto:ingo.voelkel@iml.fraunhofer.de[]

|Holger Schulz
|mailto:holger.schulz@iml.fraunhofer.de[]

|Dario Mikolajczak
|mailto:dario.mikolajczak@iml.fraunhofer.de[]

|Liam Leander Szallies
|mailto:liam.leander.szallies@iml.fraunhofer.de[]

|Edgar Wilzer
|mailto:edgar.wilzer@iml.fraunhofer.de[]

|Kristin Bäßler
|mailto:kristin.baessler@iml.fraunhofer.de[]

|===

// Enable section numbering from here on
:sectnums:

<<<
// 1. Introduction and Goals
include::01_introduction_and_goals.adoc[]

<<<
// 2. Architecture Constraints
include::02_architecture_constraints.adoc[]

<<<
// 3. System Scope and Context
include::03_system_scope_and_context.adoc[]

<<<
// 4. Solution Strategy
include::04_solution_strategy.adoc[]

<<<
// 5. Building Block View
include::05_building_block_view.adoc[]

<<<
// 6. Runtime View
include::06_runtime_view.adoc[]

<<<
// 7. Deployment View
include::07_deployment_view.adoc[]

<<<
// 8. Concepts
include::08_concepts.adoc[]

<<<
// 9. Design Decisions
include::09_design_decisions.adoc[]

<<<
// 10. Quality Scenarios
include::10_quality_scenarios.adoc[]

<<<
// 11. Technical Risks
include::11_technical_risks.adoc[]

<<<
// 12. Tutorial
include::12_tutorial.adoc[]

<<<
// Glossary
include::glossary.adoc[]
