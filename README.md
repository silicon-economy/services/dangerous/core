> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.

# dangerous

This is the modular Blockchain Europe _dangerous_ repository which houses
the components of the _dangerous_ blockchain project.
They use the following technologies:

- Angular Frontend (Typescript)
- NestJS Backend Microservices (Typescript)
- Tendermint/Cosmos Blockchain (Golang)
- Rabbit MQ (AMQP)

As package manager we recommend using yarn 3.2.4 ([Installation instructions](https://yarnpkg.com/getting-started/install)).
This project was generated based on the [NX build system](https://nx.dev/).

## License

Dangerous is released under the Open Logistics Foundation License 1.3

#### Third Party Licenses

All third party licenses can be found [here](https://silicon-economy.pages.fraunhofer.de/services/dangerous/core/npm-licenses-list.txt).



## Prerequisites

This project is tested with the following frameworks:

- Node ver. 18.12.x LTS
- Ignite ver. v0.23.0

If required, we recommend referring the individual docker files included 
in each project for detailed installation instructions.

Install Ignite with the following command:
```shell
curl https://get.ignite.com/cli@v0.23.0! | bash
```

## Quick Start

1. As this project depends on the _TokenManager_ base component we recommend checking out the repositories recursively via:  
    ```shell
    git clone --recursive
    git pull --recursive
    ```

1. After pulling the software run (at the moment VPN access **is needed** for initial install):
    ```shell
    yarn install
    ```

1. To test all services run (default: `--maxParallel=2`, >10 GB RAM usage, lower this value to `1` to reduce memory consumption):  
    ```shell
    yarn test
    ```

1. Provide an `.env`-file, which follows the pattern below (if you are a member of the dangerous gitlab-project, you can access a valid `.env`-file [here](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/services/dangerous/core/-/snippets/733)):
    ```dotenv
    # Blockchain
    DANGEROUS_USER_MNEMONIC="<Insert value>"
    PUCK1_MNEMONIC="<Insert value>"
    PUCK2_MNEMONIC="<Insert value>"
    PUCK3_MNEMONIC="<Insert value>"
    PUCK4_MNEMONIC="<Insert value>"

    DANGEROUS_PRIVATE_KEY="<Insert value>"
    DANGEROUS_PUBLIC_KEY="<Insert value>"
    DANGEROUS_USERS="<Insert value>"
    ```

1. Start the Blockchain in the current terminal:  
    ```shell
    yarn blockchain:init
    yarn blockchain:start
    ```

1. Start the [RabbitMQ](https://www.rabbitmq.com/) broker in a second terminal:  
    ```
    yarn amqp:initial
    ```

1. To start the frontend and all microservices run each of the following commands in a 
separate terminal:  
    ```shell
    yarn all:service
    yarn all:gateway
    ```

Refer to the `package.json`-file in the project's root directory for an overview of the available nx based commands.

---
## Project Overview & Documentation
The extensive dangerous arc42 documentation can be found in the following directory: `documentation/docs/`.

### Custom Runtime Environments
For the following listed items, individual customization may be necessary:
- Helm Package Manager,
- CI/CD Pipelines,
- microservice-specific files.

For detailed information on the customizations, refer to the _Runtime View_ chapter of the arc42 documentation (cf. `documentation/docs/06_runtime_view.adoc`).

## Frontend
The dangerous.frontend is an Angular based Progressive Web App. It follows the recommended Best 
Practices by the Angular Community. It can write/read and sign blockchain transaction on its own 
with Light Client capabilities.

## Backend
The dangerous backend consists of multiple gateways, microservices and a blockchain.

### Gateways
The gateways are used to enable user friendly http based communication between frontend and microservices.
#### Dangerous Goods Gateway
This gateway communicates with all microservices responsible for looking up static data and 
calculating information required for the transport of dangerous goods.
#### Transport Document Gateway
This gateway communicates with all microservices that handle persisting data in(and requesting 
data from) the blockchain.

### Microservices
#### Media Management
Generates all shipping labels needed for a given transport document.

#### Tendermint Service
The Tendermint service is a nestjs typescript server developed to ease communication 
with the tendermint/cosmos blockchain. A user-friendly interface that communicates to the Cosmos-based 
blockchain, which protocols the history of electronic transport documents, by providing a tailored 
abstraction layer between the blockchain and the front-end.

#### Data Management
Offers create-, read- and update message patterns for handling transport documents in the blockchain.

#### Data Lookup
- Provides information about the with a given UN number associated dangerous good for a given UN number 
or partially given description. 
- Provides packaging information for a partially given packagingCode or other partially given packaging information (e.g. kind, typesOfMaterial, material, category).

#### Business Logic
Calculates the individual as well as total transport points for a given load and returns the 
exemption status.

#### Mini Auth Service
Provides basic JWT authentication capabilities for the _dangerous_ project.

#### Blockchain (_TokenManager_)
Consists of a Cosmos/Tendermint Blockchain instance complete with a full-node and a light-client 
server and provides the end-point which accepts signed Cosmos TX messages and propagates them 
in the Blockchain network. Can be substituted by any other instance of the related chain.
The blockchain _TokenManager_ serves as the backbone of the dangerous application.

Standard RPC port: http://localhost:26657  
Standard Node Address: http://localhost:1317

## Tools & Libs

The `tools` folder contains scripts to facilitate the maintainability of the project, like additional feature for yarn, or the license header.

The `libs` folder contains shared code wich is used by multiple projects at once, e.g. the AMQP-Patterns required for the communication between different microservices, or the models representing the user.
